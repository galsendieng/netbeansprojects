package com.orange.myabstract.web.rest;

import com.orange.myabstract.AbstractApp;

import com.orange.myabstract.domain.AudioTag;
import com.orange.myabstract.repository.AudioTagRepository;
import com.orange.myabstract.service.AudioTagService;
import com.orange.myabstract.repository.search.AudioTagSearchRepository;
import com.orange.myabstract.service.dto.AudioTagDTO;
import com.orange.myabstract.service.mapper.AudioTagMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the AudioTagResource REST controller.
 *
 * @see AudioTagResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = AbstractApp.class)
public class AudioTagResourceIntTest {

    private static final String DEFAULT_TYPE_STREAM = "AAAAAAAAAA";
    private static final String UPDATED_TYPE_STREAM = "BBBBBBBBBB";

    private static final String DEFAULT_VALEUR = "AAAAAAAAAA";
    private static final String UPDATED_VALEUR = "BBBBBBBBBB";

    private static final String DEFAULT_STREAMER = "AAAAAAAAAA";
    private static final String UPDATED_STREAMER = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_DATE_SUP = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_DATE_SUP = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final String DEFAULT_DATE_SUP_STR = DateTimeFormatter.ISO_INSTANT.format(DEFAULT_DATE_SUP);

    @Inject
    private AudioTagRepository audioTagRepository;

    @Inject
    private AudioTagMapper audioTagMapper;

    @Inject
    private AudioTagService audioTagService;

    @Inject
    private AudioTagSearchRepository audioTagSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Inject
    private EntityManager em;

    private MockMvc restAudioTagMockMvc;

    private AudioTag audioTag;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        AudioTagResource audioTagResource = new AudioTagResource();
        ReflectionTestUtils.setField(audioTagResource, "audioTagService", audioTagService);
        this.restAudioTagMockMvc = MockMvcBuilders.standaloneSetup(audioTagResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AudioTag createEntity(EntityManager em) {
        AudioTag audioTag = new AudioTag()
                .typeStream(DEFAULT_TYPE_STREAM)
                .valeur(DEFAULT_VALEUR)
                .streamer(DEFAULT_STREAMER)
                .dateSup(DEFAULT_DATE_SUP);
        return audioTag;
    }

    @Before
    public void initTest() {
        audioTagSearchRepository.deleteAll();
        audioTag = createEntity(em);
    }

    @Test
    @Transactional
    public void createAudioTag() throws Exception {
        int databaseSizeBeforeCreate = audioTagRepository.findAll().size();

        // Create the AudioTag
        AudioTagDTO audioTagDTO = audioTagMapper.audioTagToAudioTagDTO(audioTag);

        restAudioTagMockMvc.perform(post("/api/audio-tags")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(audioTagDTO)))
                .andExpect(status().isCreated());

        // Validate the AudioTag in the database
        List<AudioTag> audioTags = audioTagRepository.findAll();
        assertThat(audioTags).hasSize(databaseSizeBeforeCreate + 1);
        AudioTag testAudioTag = audioTags.get(audioTags.size() - 1);
        assertThat(testAudioTag.getTypeStream()).isEqualTo(DEFAULT_TYPE_STREAM);
        assertThat(testAudioTag.getValeur()).isEqualTo(DEFAULT_VALEUR);
        assertThat(testAudioTag.getStreamer()).isEqualTo(DEFAULT_STREAMER);
        assertThat(testAudioTag.getDateSup()).isEqualTo(DEFAULT_DATE_SUP);

        // Validate the AudioTag in ElasticSearch
        AudioTag audioTagEs = audioTagSearchRepository.findOne(testAudioTag.getId());
        assertThat(audioTagEs).isEqualToComparingFieldByField(testAudioTag);
    }

    @Test
    @Transactional
    public void getAllAudioTags() throws Exception {
        // Initialize the database
        audioTagRepository.saveAndFlush(audioTag);

        // Get all the audioTags
        restAudioTagMockMvc.perform(get("/api/audio-tags?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.[*].id").value(hasItem(audioTag.getId().intValue())))
                .andExpect(jsonPath("$.[*].typeStream").value(hasItem(DEFAULT_TYPE_STREAM.toString())))
                .andExpect(jsonPath("$.[*].valeur").value(hasItem(DEFAULT_VALEUR.toString())))
                .andExpect(jsonPath("$.[*].streamer").value(hasItem(DEFAULT_STREAMER.toString())))
                .andExpect(jsonPath("$.[*].dateSup").value(hasItem(DEFAULT_DATE_SUP_STR)));
    }

    @Test
    @Transactional
    public void getAudioTag() throws Exception {
        // Initialize the database
        audioTagRepository.saveAndFlush(audioTag);

        // Get the audioTag
        restAudioTagMockMvc.perform(get("/api/audio-tags/{id}", audioTag.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(audioTag.getId().intValue()))
            .andExpect(jsonPath("$.typeStream").value(DEFAULT_TYPE_STREAM.toString()))
            .andExpect(jsonPath("$.valeur").value(DEFAULT_VALEUR.toString()))
            .andExpect(jsonPath("$.streamer").value(DEFAULT_STREAMER.toString()))
            .andExpect(jsonPath("$.dateSup").value(DEFAULT_DATE_SUP_STR));
    }

    @Test
    @Transactional
    public void getNonExistingAudioTag() throws Exception {
        // Get the audioTag
        restAudioTagMockMvc.perform(get("/api/audio-tags/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAudioTag() throws Exception {
        // Initialize the database
        audioTagRepository.saveAndFlush(audioTag);
        audioTagSearchRepository.save(audioTag);
        int databaseSizeBeforeUpdate = audioTagRepository.findAll().size();

        // Update the audioTag
        AudioTag updatedAudioTag = audioTagRepository.findOne(audioTag.getId());
        updatedAudioTag
                .typeStream(UPDATED_TYPE_STREAM)
                .valeur(UPDATED_VALEUR)
                .streamer(UPDATED_STREAMER)
                .dateSup(UPDATED_DATE_SUP);
        AudioTagDTO audioTagDTO = audioTagMapper.audioTagToAudioTagDTO(updatedAudioTag);

        restAudioTagMockMvc.perform(put("/api/audio-tags")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(audioTagDTO)))
                .andExpect(status().isOk());

        // Validate the AudioTag in the database
        List<AudioTag> audioTags = audioTagRepository.findAll();
        assertThat(audioTags).hasSize(databaseSizeBeforeUpdate);
        AudioTag testAudioTag = audioTags.get(audioTags.size() - 1);
        assertThat(testAudioTag.getTypeStream()).isEqualTo(UPDATED_TYPE_STREAM);
        assertThat(testAudioTag.getValeur()).isEqualTo(UPDATED_VALEUR);
        assertThat(testAudioTag.getStreamer()).isEqualTo(UPDATED_STREAMER);
        assertThat(testAudioTag.getDateSup()).isEqualTo(UPDATED_DATE_SUP);

        // Validate the AudioTag in ElasticSearch
        AudioTag audioTagEs = audioTagSearchRepository.findOne(testAudioTag.getId());
        assertThat(audioTagEs).isEqualToComparingFieldByField(testAudioTag);
    }

    @Test
    @Transactional
    public void deleteAudioTag() throws Exception {
        // Initialize the database
        audioTagRepository.saveAndFlush(audioTag);
        audioTagSearchRepository.save(audioTag);
        int databaseSizeBeforeDelete = audioTagRepository.findAll().size();

        // Get the audioTag
        restAudioTagMockMvc.perform(delete("/api/audio-tags/{id}", audioTag.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate ElasticSearch is empty
        boolean audioTagExistsInEs = audioTagSearchRepository.exists(audioTag.getId());
        assertThat(audioTagExistsInEs).isFalse();

        // Validate the database is empty
        List<AudioTag> audioTags = audioTagRepository.findAll();
        assertThat(audioTags).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchAudioTag() throws Exception {
        // Initialize the database
        audioTagRepository.saveAndFlush(audioTag);
        audioTagSearchRepository.save(audioTag);

        // Search the audioTag
        restAudioTagMockMvc.perform(get("/api/_search/audio-tags?query=id:" + audioTag.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(audioTag.getId().intValue())))
            .andExpect(jsonPath("$.[*].typeStream").value(hasItem(DEFAULT_TYPE_STREAM.toString())))
            .andExpect(jsonPath("$.[*].valeur").value(hasItem(DEFAULT_VALEUR.toString())))
            .andExpect(jsonPath("$.[*].streamer").value(hasItem(DEFAULT_STREAMER.toString())))
            .andExpect(jsonPath("$.[*].dateSup").value(hasItem(DEFAULT_DATE_SUP_STR)));
    }
}
