package com.orange.myabstract.web.rest;

import com.orange.myabstract.AbstractApp;

import com.orange.myabstract.domain.MinorVersion;
import com.orange.myabstract.repository.MinorVersionRepository;
import com.orange.myabstract.service.MinorVersionService;
import com.orange.myabstract.repository.search.MinorVersionSearchRepository;
import com.orange.myabstract.service.dto.MinorVersionDTO;
import com.orange.myabstract.service.mapper.MinorVersionMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the MinorVersionResource REST controller.
 *
 * @see MinorVersionResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = AbstractApp.class)
public class MinorVersionResourceIntTest {

    private static final String DEFAULT_VALEUR = "AAAAAAAAAA";
    private static final String UPDATED_VALEUR = "BBBBBBBBBB";

    private static final String DEFAULT_STREAMER = "AAAAAAAAAA";
    private static final String UPDATED_STREAMER = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_DATE_SUP = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_DATE_SUP = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final String DEFAULT_DATE_SUP_STR = DateTimeFormatter.ISO_INSTANT.format(DEFAULT_DATE_SUP);

    @Inject
    private MinorVersionRepository minorVersionRepository;

    @Inject
    private MinorVersionMapper minorVersionMapper;

    @Inject
    private MinorVersionService minorVersionService;

    @Inject
    private MinorVersionSearchRepository minorVersionSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Inject
    private EntityManager em;

    private MockMvc restMinorVersionMockMvc;

    private MinorVersion minorVersion;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        MinorVersionResource minorVersionResource = new MinorVersionResource();
        ReflectionTestUtils.setField(minorVersionResource, "minorVersionService", minorVersionService);
        this.restMinorVersionMockMvc = MockMvcBuilders.standaloneSetup(minorVersionResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static MinorVersion createEntity(EntityManager em) {
        MinorVersion minorVersion = new MinorVersion()
                .valeur(DEFAULT_VALEUR)
                .streamer(DEFAULT_STREAMER)
                .dateSup(DEFAULT_DATE_SUP);
        return minorVersion;
    }

    @Before
    public void initTest() {
        minorVersionSearchRepository.deleteAll();
        minorVersion = createEntity(em);
    }

    @Test
    @Transactional
    public void createMinorVersion() throws Exception {
        int databaseSizeBeforeCreate = minorVersionRepository.findAll().size();

        // Create the MinorVersion
        MinorVersionDTO minorVersionDTO = minorVersionMapper.minorVersionToMinorVersionDTO(minorVersion);

        restMinorVersionMockMvc.perform(post("/api/minor-versions")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(minorVersionDTO)))
                .andExpect(status().isCreated());

        // Validate the MinorVersion in the database
        List<MinorVersion> minorVersions = minorVersionRepository.findAll();
        assertThat(minorVersions).hasSize(databaseSizeBeforeCreate + 1);
        MinorVersion testMinorVersion = minorVersions.get(minorVersions.size() - 1);
        assertThat(testMinorVersion.getValeur()).isEqualTo(DEFAULT_VALEUR);
        assertThat(testMinorVersion.getStreamer()).isEqualTo(DEFAULT_STREAMER);
        assertThat(testMinorVersion.getDateSup()).isEqualTo(DEFAULT_DATE_SUP);

        // Validate the MinorVersion in ElasticSearch
        MinorVersion minorVersionEs = minorVersionSearchRepository.findOne(testMinorVersion.getId());
        assertThat(minorVersionEs).isEqualToComparingFieldByField(testMinorVersion);
    }

    @Test
    @Transactional
    public void getAllMinorVersions() throws Exception {
        // Initialize the database
        minorVersionRepository.saveAndFlush(minorVersion);

        // Get all the minorVersions
        restMinorVersionMockMvc.perform(get("/api/minor-versions?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.[*].id").value(hasItem(minorVersion.getId().intValue())))
                .andExpect(jsonPath("$.[*].valeur").value(hasItem(DEFAULT_VALEUR.toString())))
                .andExpect(jsonPath("$.[*].streamer").value(hasItem(DEFAULT_STREAMER.toString())))
                .andExpect(jsonPath("$.[*].dateSup").value(hasItem(DEFAULT_DATE_SUP_STR)));
    }

    @Test
    @Transactional
    public void getMinorVersion() throws Exception {
        // Initialize the database
        minorVersionRepository.saveAndFlush(minorVersion);

        // Get the minorVersion
        restMinorVersionMockMvc.perform(get("/api/minor-versions/{id}", minorVersion.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(minorVersion.getId().intValue()))
            .andExpect(jsonPath("$.valeur").value(DEFAULT_VALEUR.toString()))
            .andExpect(jsonPath("$.streamer").value(DEFAULT_STREAMER.toString()))
            .andExpect(jsonPath("$.dateSup").value(DEFAULT_DATE_SUP_STR));
    }

    @Test
    @Transactional
    public void getNonExistingMinorVersion() throws Exception {
        // Get the minorVersion
        restMinorVersionMockMvc.perform(get("/api/minor-versions/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateMinorVersion() throws Exception {
        // Initialize the database
        minorVersionRepository.saveAndFlush(minorVersion);
        minorVersionSearchRepository.save(minorVersion);
        int databaseSizeBeforeUpdate = minorVersionRepository.findAll().size();

        // Update the minorVersion
        MinorVersion updatedMinorVersion = minorVersionRepository.findOne(minorVersion.getId());
        updatedMinorVersion
                .valeur(UPDATED_VALEUR)
                .streamer(UPDATED_STREAMER)
                .dateSup(UPDATED_DATE_SUP);
        MinorVersionDTO minorVersionDTO = minorVersionMapper.minorVersionToMinorVersionDTO(updatedMinorVersion);

        restMinorVersionMockMvc.perform(put("/api/minor-versions")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(minorVersionDTO)))
                .andExpect(status().isOk());

        // Validate the MinorVersion in the database
        List<MinorVersion> minorVersions = minorVersionRepository.findAll();
        assertThat(minorVersions).hasSize(databaseSizeBeforeUpdate);
        MinorVersion testMinorVersion = minorVersions.get(minorVersions.size() - 1);
        assertThat(testMinorVersion.getValeur()).isEqualTo(UPDATED_VALEUR);
        assertThat(testMinorVersion.getStreamer()).isEqualTo(UPDATED_STREAMER);
        assertThat(testMinorVersion.getDateSup()).isEqualTo(UPDATED_DATE_SUP);

        // Validate the MinorVersion in ElasticSearch
        MinorVersion minorVersionEs = minorVersionSearchRepository.findOne(testMinorVersion.getId());
        assertThat(minorVersionEs).isEqualToComparingFieldByField(testMinorVersion);
    }

    @Test
    @Transactional
    public void deleteMinorVersion() throws Exception {
        // Initialize the database
        minorVersionRepository.saveAndFlush(minorVersion);
        minorVersionSearchRepository.save(minorVersion);
        int databaseSizeBeforeDelete = minorVersionRepository.findAll().size();

        // Get the minorVersion
        restMinorVersionMockMvc.perform(delete("/api/minor-versions/{id}", minorVersion.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate ElasticSearch is empty
        boolean minorVersionExistsInEs = minorVersionSearchRepository.exists(minorVersion.getId());
        assertThat(minorVersionExistsInEs).isFalse();

        // Validate the database is empty
        List<MinorVersion> minorVersions = minorVersionRepository.findAll();
        assertThat(minorVersions).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchMinorVersion() throws Exception {
        // Initialize the database
        minorVersionRepository.saveAndFlush(minorVersion);
        minorVersionSearchRepository.save(minorVersion);

        // Search the minorVersion
        restMinorVersionMockMvc.perform(get("/api/_search/minor-versions?query=id:" + minorVersion.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(minorVersion.getId().intValue())))
            .andExpect(jsonPath("$.[*].valeur").value(hasItem(DEFAULT_VALEUR.toString())))
            .andExpect(jsonPath("$.[*].streamer").value(hasItem(DEFAULT_STREAMER.toString())))
            .andExpect(jsonPath("$.[*].dateSup").value(hasItem(DEFAULT_DATE_SUP_STR)));
    }
}
