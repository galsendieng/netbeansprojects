package com.orange.myabstract.web.rest;

import com.orange.myabstract.AbstractApp;

import com.orange.myabstract.domain.MaxWidth;
import com.orange.myabstract.repository.MaxWidthRepository;
import com.orange.myabstract.service.MaxWidthService;
import com.orange.myabstract.repository.search.MaxWidthSearchRepository;
import com.orange.myabstract.service.dto.MaxWidthDTO;
import com.orange.myabstract.service.mapper.MaxWidthMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the MaxWidthResource REST controller.
 *
 * @see MaxWidthResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = AbstractApp.class)
public class MaxWidthResourceIntTest {

    private static final String DEFAULT_TYPE_STREAM = "AAAAAAAAAA";
    private static final String UPDATED_TYPE_STREAM = "BBBBBBBBBB";

    private static final String DEFAULT_STREAMER = "AAAAAAAAAA";
    private static final String UPDATED_STREAMER = "BBBBBBBBBB";

    private static final String DEFAULT_VALEUR = "AAAAAAAAAA";
    private static final String UPDATED_VALEUR = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_DATE_SUP = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_DATE_SUP = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final String DEFAULT_DATE_SUP_STR = DateTimeFormatter.ISO_INSTANT.format(DEFAULT_DATE_SUP);

    @Inject
    private MaxWidthRepository maxWidthRepository;

    @Inject
    private MaxWidthMapper maxWidthMapper;

    @Inject
    private MaxWidthService maxWidthService;

    @Inject
    private MaxWidthSearchRepository maxWidthSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Inject
    private EntityManager em;

    private MockMvc restMaxWidthMockMvc;

    private MaxWidth maxWidth;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        MaxWidthResource maxWidthResource = new MaxWidthResource();
        ReflectionTestUtils.setField(maxWidthResource, "maxWidthService", maxWidthService);
        this.restMaxWidthMockMvc = MockMvcBuilders.standaloneSetup(maxWidthResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static MaxWidth createEntity(EntityManager em) {
        MaxWidth maxWidth = new MaxWidth()
                .typeStream(DEFAULT_TYPE_STREAM)
                .streamer(DEFAULT_STREAMER)
                .valeur(DEFAULT_VALEUR)
                .dateSup(DEFAULT_DATE_SUP);
        return maxWidth;
    }

    @Before
    public void initTest() {
        maxWidthSearchRepository.deleteAll();
        maxWidth = createEntity(em);
    }

    @Test
    @Transactional
    public void createMaxWidth() throws Exception {
        int databaseSizeBeforeCreate = maxWidthRepository.findAll().size();

        // Create the MaxWidth
        MaxWidthDTO maxWidthDTO = maxWidthMapper.maxWidthToMaxWidthDTO(maxWidth);

        restMaxWidthMockMvc.perform(post("/api/max-widths")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(maxWidthDTO)))
                .andExpect(status().isCreated());

        // Validate the MaxWidth in the database
        List<MaxWidth> maxWidths = maxWidthRepository.findAll();
        assertThat(maxWidths).hasSize(databaseSizeBeforeCreate + 1);
        MaxWidth testMaxWidth = maxWidths.get(maxWidths.size() - 1);
        assertThat(testMaxWidth.getTypeStream()).isEqualTo(DEFAULT_TYPE_STREAM);
        assertThat(testMaxWidth.getStreamer()).isEqualTo(DEFAULT_STREAMER);
        assertThat(testMaxWidth.getValeur()).isEqualTo(DEFAULT_VALEUR);
        assertThat(testMaxWidth.getDateSup()).isEqualTo(DEFAULT_DATE_SUP);

        // Validate the MaxWidth in ElasticSearch
        MaxWidth maxWidthEs = maxWidthSearchRepository.findOne(testMaxWidth.getId());
        assertThat(maxWidthEs).isEqualToComparingFieldByField(testMaxWidth);
    }

    @Test
    @Transactional
    public void getAllMaxWidths() throws Exception {
        // Initialize the database
        maxWidthRepository.saveAndFlush(maxWidth);

        // Get all the maxWidths
        restMaxWidthMockMvc.perform(get("/api/max-widths?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.[*].id").value(hasItem(maxWidth.getId().intValue())))
                .andExpect(jsonPath("$.[*].typeStream").value(hasItem(DEFAULT_TYPE_STREAM.toString())))
                .andExpect(jsonPath("$.[*].streamer").value(hasItem(DEFAULT_STREAMER.toString())))
                .andExpect(jsonPath("$.[*].valeur").value(hasItem(DEFAULT_VALEUR.toString())))
                .andExpect(jsonPath("$.[*].dateSup").value(hasItem(DEFAULT_DATE_SUP_STR)));
    }

    @Test
    @Transactional
    public void getMaxWidth() throws Exception {
        // Initialize the database
        maxWidthRepository.saveAndFlush(maxWidth);

        // Get the maxWidth
        restMaxWidthMockMvc.perform(get("/api/max-widths/{id}", maxWidth.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(maxWidth.getId().intValue()))
            .andExpect(jsonPath("$.typeStream").value(DEFAULT_TYPE_STREAM.toString()))
            .andExpect(jsonPath("$.streamer").value(DEFAULT_STREAMER.toString()))
            .andExpect(jsonPath("$.valeur").value(DEFAULT_VALEUR.toString()))
            .andExpect(jsonPath("$.dateSup").value(DEFAULT_DATE_SUP_STR));
    }

    @Test
    @Transactional
    public void getNonExistingMaxWidth() throws Exception {
        // Get the maxWidth
        restMaxWidthMockMvc.perform(get("/api/max-widths/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateMaxWidth() throws Exception {
        // Initialize the database
        maxWidthRepository.saveAndFlush(maxWidth);
        maxWidthSearchRepository.save(maxWidth);
        int databaseSizeBeforeUpdate = maxWidthRepository.findAll().size();

        // Update the maxWidth
        MaxWidth updatedMaxWidth = maxWidthRepository.findOne(maxWidth.getId());
        updatedMaxWidth
                .typeStream(UPDATED_TYPE_STREAM)
                .streamer(UPDATED_STREAMER)
                .valeur(UPDATED_VALEUR)
                .dateSup(UPDATED_DATE_SUP);
        MaxWidthDTO maxWidthDTO = maxWidthMapper.maxWidthToMaxWidthDTO(updatedMaxWidth);

        restMaxWidthMockMvc.perform(put("/api/max-widths")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(maxWidthDTO)))
                .andExpect(status().isOk());

        // Validate the MaxWidth in the database
        List<MaxWidth> maxWidths = maxWidthRepository.findAll();
        assertThat(maxWidths).hasSize(databaseSizeBeforeUpdate);
        MaxWidth testMaxWidth = maxWidths.get(maxWidths.size() - 1);
        assertThat(testMaxWidth.getTypeStream()).isEqualTo(UPDATED_TYPE_STREAM);
        assertThat(testMaxWidth.getStreamer()).isEqualTo(UPDATED_STREAMER);
        assertThat(testMaxWidth.getValeur()).isEqualTo(UPDATED_VALEUR);
        assertThat(testMaxWidth.getDateSup()).isEqualTo(UPDATED_DATE_SUP);

        // Validate the MaxWidth in ElasticSearch
        MaxWidth maxWidthEs = maxWidthSearchRepository.findOne(testMaxWidth.getId());
        assertThat(maxWidthEs).isEqualToComparingFieldByField(testMaxWidth);
    }

    @Test
    @Transactional
    public void deleteMaxWidth() throws Exception {
        // Initialize the database
        maxWidthRepository.saveAndFlush(maxWidth);
        maxWidthSearchRepository.save(maxWidth);
        int databaseSizeBeforeDelete = maxWidthRepository.findAll().size();

        // Get the maxWidth
        restMaxWidthMockMvc.perform(delete("/api/max-widths/{id}", maxWidth.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate ElasticSearch is empty
        boolean maxWidthExistsInEs = maxWidthSearchRepository.exists(maxWidth.getId());
        assertThat(maxWidthExistsInEs).isFalse();

        // Validate the database is empty
        List<MaxWidth> maxWidths = maxWidthRepository.findAll();
        assertThat(maxWidths).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchMaxWidth() throws Exception {
        // Initialize the database
        maxWidthRepository.saveAndFlush(maxWidth);
        maxWidthSearchRepository.save(maxWidth);

        // Search the maxWidth
        restMaxWidthMockMvc.perform(get("/api/_search/max-widths?query=id:" + maxWidth.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(maxWidth.getId().intValue())))
            .andExpect(jsonPath("$.[*].typeStream").value(hasItem(DEFAULT_TYPE_STREAM.toString())))
            .andExpect(jsonPath("$.[*].streamer").value(hasItem(DEFAULT_STREAMER.toString())))
            .andExpect(jsonPath("$.[*].valeur").value(hasItem(DEFAULT_VALEUR.toString())))
            .andExpect(jsonPath("$.[*].dateSup").value(hasItem(DEFAULT_DATE_SUP_STR)));
    }
}
