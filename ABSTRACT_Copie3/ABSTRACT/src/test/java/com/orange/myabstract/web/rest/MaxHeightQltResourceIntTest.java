package com.orange.myabstract.web.rest;

import com.orange.myabstract.AbstractApp;

import com.orange.myabstract.domain.MaxHeightQlt;
import com.orange.myabstract.repository.MaxHeightQltRepository;
import com.orange.myabstract.service.MaxHeightQltService;
import com.orange.myabstract.repository.search.MaxHeightQltSearchRepository;
import com.orange.myabstract.service.dto.MaxHeightQltDTO;
import com.orange.myabstract.service.mapper.MaxHeightQltMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the MaxHeightQltResource REST controller.
 *
 * @see MaxHeightQltResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = AbstractApp.class)
public class MaxHeightQltResourceIntTest {

    private static final String DEFAULT_TYPE_STREAM = "AAAAAAAAAA";
    private static final String UPDATED_TYPE_STREAM = "BBBBBBBBBB";

    private static final String DEFAULT_VALEUR = "AAAAAAAAAA";
    private static final String UPDATED_VALEUR = "BBBBBBBBBB";

    private static final String DEFAULT_STREAMER = "AAAAAAAAAA";
    private static final String UPDATED_STREAMER = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_DATE_SUP = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_DATE_SUP = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final String DEFAULT_DATE_SUP_STR = DateTimeFormatter.ISO_INSTANT.format(DEFAULT_DATE_SUP);

    @Inject
    private MaxHeightQltRepository maxHeightQltRepository;

    @Inject
    private MaxHeightQltMapper maxHeightQltMapper;

    @Inject
    private MaxHeightQltService maxHeightQltService;

    @Inject
    private MaxHeightQltSearchRepository maxHeightQltSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Inject
    private EntityManager em;

    private MockMvc restMaxHeightQltMockMvc;

    private MaxHeightQlt maxHeightQlt;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        MaxHeightQltResource maxHeightQltResource = new MaxHeightQltResource();
        ReflectionTestUtils.setField(maxHeightQltResource, "maxHeightQltService", maxHeightQltService);
        this.restMaxHeightQltMockMvc = MockMvcBuilders.standaloneSetup(maxHeightQltResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static MaxHeightQlt createEntity(EntityManager em) {
        MaxHeightQlt maxHeightQlt = new MaxHeightQlt()
                .typeStream(DEFAULT_TYPE_STREAM)
                .valeur(DEFAULT_VALEUR)
                .streamer(DEFAULT_STREAMER)
                .dateSup(DEFAULT_DATE_SUP);
        return maxHeightQlt;
    }

    @Before
    public void initTest() {
        maxHeightQltSearchRepository.deleteAll();
        maxHeightQlt = createEntity(em);
    }

    @Test
    @Transactional
    public void createMaxHeightQlt() throws Exception {
        int databaseSizeBeforeCreate = maxHeightQltRepository.findAll().size();

        // Create the MaxHeightQlt
        MaxHeightQltDTO maxHeightQltDTO = maxHeightQltMapper.maxHeightQltToMaxHeightQltDTO(maxHeightQlt);

        restMaxHeightQltMockMvc.perform(post("/api/max-height-qlts")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(maxHeightQltDTO)))
                .andExpect(status().isCreated());

        // Validate the MaxHeightQlt in the database
        List<MaxHeightQlt> maxHeightQlts = maxHeightQltRepository.findAll();
        assertThat(maxHeightQlts).hasSize(databaseSizeBeforeCreate + 1);
        MaxHeightQlt testMaxHeightQlt = maxHeightQlts.get(maxHeightQlts.size() - 1);
        assertThat(testMaxHeightQlt.getTypeStream()).isEqualTo(DEFAULT_TYPE_STREAM);
        assertThat(testMaxHeightQlt.getValeur()).isEqualTo(DEFAULT_VALEUR);
        assertThat(testMaxHeightQlt.getStreamer()).isEqualTo(DEFAULT_STREAMER);
        assertThat(testMaxHeightQlt.getDateSup()).isEqualTo(DEFAULT_DATE_SUP);

        // Validate the MaxHeightQlt in ElasticSearch
        MaxHeightQlt maxHeightQltEs = maxHeightQltSearchRepository.findOne(testMaxHeightQlt.getId());
        assertThat(maxHeightQltEs).isEqualToComparingFieldByField(testMaxHeightQlt);
    }

    @Test
    @Transactional
    public void getAllMaxHeightQlts() throws Exception {
        // Initialize the database
        maxHeightQltRepository.saveAndFlush(maxHeightQlt);

        // Get all the maxHeightQlts
        restMaxHeightQltMockMvc.perform(get("/api/max-height-qlts?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.[*].id").value(hasItem(maxHeightQlt.getId().intValue())))
                .andExpect(jsonPath("$.[*].typeStream").value(hasItem(DEFAULT_TYPE_STREAM.toString())))
                .andExpect(jsonPath("$.[*].valeur").value(hasItem(DEFAULT_VALEUR.toString())))
                .andExpect(jsonPath("$.[*].streamer").value(hasItem(DEFAULT_STREAMER.toString())))
                .andExpect(jsonPath("$.[*].dateSup").value(hasItem(DEFAULT_DATE_SUP_STR)));
    }

    @Test
    @Transactional
    public void getMaxHeightQlt() throws Exception {
        // Initialize the database
        maxHeightQltRepository.saveAndFlush(maxHeightQlt);

        // Get the maxHeightQlt
        restMaxHeightQltMockMvc.perform(get("/api/max-height-qlts/{id}", maxHeightQlt.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(maxHeightQlt.getId().intValue()))
            .andExpect(jsonPath("$.typeStream").value(DEFAULT_TYPE_STREAM.toString()))
            .andExpect(jsonPath("$.valeur").value(DEFAULT_VALEUR.toString()))
            .andExpect(jsonPath("$.streamer").value(DEFAULT_STREAMER.toString()))
            .andExpect(jsonPath("$.dateSup").value(DEFAULT_DATE_SUP_STR));
    }

    @Test
    @Transactional
    public void getNonExistingMaxHeightQlt() throws Exception {
        // Get the maxHeightQlt
        restMaxHeightQltMockMvc.perform(get("/api/max-height-qlts/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateMaxHeightQlt() throws Exception {
        // Initialize the database
        maxHeightQltRepository.saveAndFlush(maxHeightQlt);
        maxHeightQltSearchRepository.save(maxHeightQlt);
        int databaseSizeBeforeUpdate = maxHeightQltRepository.findAll().size();

        // Update the maxHeightQlt
        MaxHeightQlt updatedMaxHeightQlt = maxHeightQltRepository.findOne(maxHeightQlt.getId());
        updatedMaxHeightQlt
                .typeStream(UPDATED_TYPE_STREAM)
                .valeur(UPDATED_VALEUR)
                .streamer(UPDATED_STREAMER)
                .dateSup(UPDATED_DATE_SUP);
        MaxHeightQltDTO maxHeightQltDTO = maxHeightQltMapper.maxHeightQltToMaxHeightQltDTO(updatedMaxHeightQlt);

        restMaxHeightQltMockMvc.perform(put("/api/max-height-qlts")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(maxHeightQltDTO)))
                .andExpect(status().isOk());

        // Validate the MaxHeightQlt in the database
        List<MaxHeightQlt> maxHeightQlts = maxHeightQltRepository.findAll();
        assertThat(maxHeightQlts).hasSize(databaseSizeBeforeUpdate);
        MaxHeightQlt testMaxHeightQlt = maxHeightQlts.get(maxHeightQlts.size() - 1);
        assertThat(testMaxHeightQlt.getTypeStream()).isEqualTo(UPDATED_TYPE_STREAM);
        assertThat(testMaxHeightQlt.getValeur()).isEqualTo(UPDATED_VALEUR);
        assertThat(testMaxHeightQlt.getStreamer()).isEqualTo(UPDATED_STREAMER);
        assertThat(testMaxHeightQlt.getDateSup()).isEqualTo(UPDATED_DATE_SUP);

        // Validate the MaxHeightQlt in ElasticSearch
        MaxHeightQlt maxHeightQltEs = maxHeightQltSearchRepository.findOne(testMaxHeightQlt.getId());
        assertThat(maxHeightQltEs).isEqualToComparingFieldByField(testMaxHeightQlt);
    }

    @Test
    @Transactional
    public void deleteMaxHeightQlt() throws Exception {
        // Initialize the database
        maxHeightQltRepository.saveAndFlush(maxHeightQlt);
        maxHeightQltSearchRepository.save(maxHeightQlt);
        int databaseSizeBeforeDelete = maxHeightQltRepository.findAll().size();

        // Get the maxHeightQlt
        restMaxHeightQltMockMvc.perform(delete("/api/max-height-qlts/{id}", maxHeightQlt.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate ElasticSearch is empty
        boolean maxHeightQltExistsInEs = maxHeightQltSearchRepository.exists(maxHeightQlt.getId());
        assertThat(maxHeightQltExistsInEs).isFalse();

        // Validate the database is empty
        List<MaxHeightQlt> maxHeightQlts = maxHeightQltRepository.findAll();
        assertThat(maxHeightQlts).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchMaxHeightQlt() throws Exception {
        // Initialize the database
        maxHeightQltRepository.saveAndFlush(maxHeightQlt);
        maxHeightQltSearchRepository.save(maxHeightQlt);

        // Search the maxHeightQlt
        restMaxHeightQltMockMvc.perform(get("/api/_search/max-height-qlts?query=id:" + maxHeightQlt.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(maxHeightQlt.getId().intValue())))
            .andExpect(jsonPath("$.[*].typeStream").value(hasItem(DEFAULT_TYPE_STREAM.toString())))
            .andExpect(jsonPath("$.[*].valeur").value(hasItem(DEFAULT_VALEUR.toString())))
            .andExpect(jsonPath("$.[*].streamer").value(hasItem(DEFAULT_STREAMER.toString())))
            .andExpect(jsonPath("$.[*].dateSup").value(hasItem(DEFAULT_DATE_SUP_STR)));
    }
}
