package com.orange.myabstract.web.rest;

import com.orange.myabstract.AbstractApp;

import com.orange.myabstract.domain.ManifestData;
import com.orange.myabstract.repository.ManifestDataRepository;
import com.orange.myabstract.service.ManifestDataService;
import com.orange.myabstract.repository.search.ManifestDataSearchRepository;
import com.orange.myabstract.service.dto.ManifestDataDTO;
import com.orange.myabstract.service.mapper.ManifestDataMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ManifestDataResource REST controller.
 *
 * @see ManifestDataResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = AbstractApp.class)
public class ManifestDataResourceIntTest {

    private static final String DEFAULT_ATTRIBUT = "AAAAAAAAAA";
    private static final String UPDATED_ATTRIBUT = "BBBBBBBBBB";

    private static final String DEFAULT_VALEUR_ATTENDUE = "AAAAAAAAAA";
    private static final String UPDATED_VALEUR_ATTENDUE = "BBBBBBBBBB";

    private static final String DEFAULT_MESSAGE_ERREUR = "AAAAAAAAAA";
    private static final String UPDATED_MESSAGE_ERREUR = "BBBBBBBBBB";

    private static final String DEFAULT_DETAILS = "AAAAAAAAAA";
    private static final String UPDATED_DETAILS = "BBBBBBBBBB";

    @Inject
    private ManifestDataRepository manifestDataRepository;

    @Inject
    private ManifestDataMapper manifestDataMapper;

    @Inject
    private ManifestDataService manifestDataService;

    @Inject
    private ManifestDataSearchRepository manifestDataSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Inject
    private EntityManager em;

    private MockMvc restManifestDataMockMvc;

    private ManifestData manifestData;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        ManifestDataResource manifestDataResource = new ManifestDataResource();
        ReflectionTestUtils.setField(manifestDataResource, "manifestDataService", manifestDataService);
        this.restManifestDataMockMvc = MockMvcBuilders.standaloneSetup(manifestDataResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ManifestData createEntity(EntityManager em) {
        ManifestData manifestData = new ManifestData()
                .attribut(DEFAULT_ATTRIBUT)
                .valeurAttendue(DEFAULT_VALEUR_ATTENDUE)
                .messageErreur(DEFAULT_MESSAGE_ERREUR)
                .details(DEFAULT_DETAILS);
        return manifestData;
    }

    @Before
    public void initTest() {
        manifestDataSearchRepository.deleteAll();
        manifestData = createEntity(em);
    }

    @Test
    @Transactional
    public void createManifestData() throws Exception {
        int databaseSizeBeforeCreate = manifestDataRepository.findAll().size();

        // Create the ManifestData
        ManifestDataDTO manifestDataDTO = manifestDataMapper.manifestDataToManifestDataDTO(manifestData);

        restManifestDataMockMvc.perform(post("/api/manifest-data")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(manifestDataDTO)))
                .andExpect(status().isCreated());

        // Validate the ManifestData in the database
        List<ManifestData> manifestData = manifestDataRepository.findAll();
        assertThat(manifestData).hasSize(databaseSizeBeforeCreate + 1);
        ManifestData testManifestData = manifestData.get(manifestData.size() - 1);
        assertThat(testManifestData.getAttribut()).isEqualTo(DEFAULT_ATTRIBUT);
        assertThat(testManifestData.getValeurAttendue()).isEqualTo(DEFAULT_VALEUR_ATTENDUE);
        assertThat(testManifestData.getMessageErreur()).isEqualTo(DEFAULT_MESSAGE_ERREUR);
        assertThat(testManifestData.getDetails()).isEqualTo(DEFAULT_DETAILS);

        // Validate the ManifestData in ElasticSearch
        ManifestData manifestDataEs = manifestDataSearchRepository.findOne(testManifestData.getId());
        assertThat(manifestDataEs).isEqualToComparingFieldByField(testManifestData);
    }

    @Test
    @Transactional
    public void checkAttributIsRequired() throws Exception {
        int databaseSizeBeforeTest = manifestDataRepository.findAll().size();
        // set the field null
        manifestData.setAttribut(null);

        // Create the ManifestData, which fails.
        ManifestDataDTO manifestDataDTO = manifestDataMapper.manifestDataToManifestDataDTO(manifestData);

        restManifestDataMockMvc.perform(post("/api/manifest-data")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(manifestDataDTO)))
                .andExpect(status().isBadRequest());

        List<ManifestData> manifestData = manifestDataRepository.findAll();
        assertThat(manifestData).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkValeurAttendueIsRequired() throws Exception {
        int databaseSizeBeforeTest = manifestDataRepository.findAll().size();
        // set the field null
        manifestData.setValeurAttendue(null);

        // Create the ManifestData, which fails.
        ManifestDataDTO manifestDataDTO = manifestDataMapper.manifestDataToManifestDataDTO(manifestData);

        restManifestDataMockMvc.perform(post("/api/manifest-data")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(manifestDataDTO)))
                .andExpect(status().isBadRequest());

        List<ManifestData> manifestData = manifestDataRepository.findAll();
        assertThat(manifestData).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkMessageErreurIsRequired() throws Exception {
        int databaseSizeBeforeTest = manifestDataRepository.findAll().size();
        // set the field null
        manifestData.setMessageErreur(null);

        // Create the ManifestData, which fails.
        ManifestDataDTO manifestDataDTO = manifestDataMapper.manifestDataToManifestDataDTO(manifestData);

        restManifestDataMockMvc.perform(post("/api/manifest-data")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(manifestDataDTO)))
                .andExpect(status().isBadRequest());

        List<ManifestData> manifestData = manifestDataRepository.findAll();
        assertThat(manifestData).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllManifestData() throws Exception {
        // Initialize the database
        manifestDataRepository.saveAndFlush(manifestData);

        // Get all the manifestData
        restManifestDataMockMvc.perform(get("/api/manifest-data?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.[*].id").value(hasItem(manifestData.getId().intValue())))
                .andExpect(jsonPath("$.[*].attribut").value(hasItem(DEFAULT_ATTRIBUT.toString())))
                .andExpect(jsonPath("$.[*].valeurAttendue").value(hasItem(DEFAULT_VALEUR_ATTENDUE.toString())))
                .andExpect(jsonPath("$.[*].messageErreur").value(hasItem(DEFAULT_MESSAGE_ERREUR.toString())))
                .andExpect(jsonPath("$.[*].details").value(hasItem(DEFAULT_DETAILS.toString())));
    }

    @Test
    @Transactional
    public void getManifestData() throws Exception {
        // Initialize the database
        manifestDataRepository.saveAndFlush(manifestData);

        // Get the manifestData
        restManifestDataMockMvc.perform(get("/api/manifest-data/{id}", manifestData.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(manifestData.getId().intValue()))
            .andExpect(jsonPath("$.attribut").value(DEFAULT_ATTRIBUT.toString()))
            .andExpect(jsonPath("$.valeurAttendue").value(DEFAULT_VALEUR_ATTENDUE.toString()))
            .andExpect(jsonPath("$.messageErreur").value(DEFAULT_MESSAGE_ERREUR.toString()))
            .andExpect(jsonPath("$.details").value(DEFAULT_DETAILS.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingManifestData() throws Exception {
        // Get the manifestData
        restManifestDataMockMvc.perform(get("/api/manifest-data/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateManifestData() throws Exception {
        // Initialize the database
        manifestDataRepository.saveAndFlush(manifestData);
        manifestDataSearchRepository.save(manifestData);
        int databaseSizeBeforeUpdate = manifestDataRepository.findAll().size();

        // Update the manifestData
        ManifestData updatedManifestData = manifestDataRepository.findOne(manifestData.getId());
        updatedManifestData
                .attribut(UPDATED_ATTRIBUT)
                .valeurAttendue(UPDATED_VALEUR_ATTENDUE)
                .messageErreur(UPDATED_MESSAGE_ERREUR)
                .details(UPDATED_DETAILS);
        ManifestDataDTO manifestDataDTO = manifestDataMapper.manifestDataToManifestDataDTO(updatedManifestData);

        restManifestDataMockMvc.perform(put("/api/manifest-data")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(manifestDataDTO)))
                .andExpect(status().isOk());

        // Validate the ManifestData in the database
        List<ManifestData> manifestData = manifestDataRepository.findAll();
        assertThat(manifestData).hasSize(databaseSizeBeforeUpdate);
        ManifestData testManifestData = manifestData.get(manifestData.size() - 1);
        assertThat(testManifestData.getAttribut()).isEqualTo(UPDATED_ATTRIBUT);
        assertThat(testManifestData.getValeurAttendue()).isEqualTo(UPDATED_VALEUR_ATTENDUE);
        assertThat(testManifestData.getMessageErreur()).isEqualTo(UPDATED_MESSAGE_ERREUR);
        assertThat(testManifestData.getDetails()).isEqualTo(UPDATED_DETAILS);

        // Validate the ManifestData in ElasticSearch
        ManifestData manifestDataEs = manifestDataSearchRepository.findOne(testManifestData.getId());
        assertThat(manifestDataEs).isEqualToComparingFieldByField(testManifestData);
    }

    @Test
    @Transactional
    public void deleteManifestData() throws Exception {
        // Initialize the database
        manifestDataRepository.saveAndFlush(manifestData);
        manifestDataSearchRepository.save(manifestData);
        int databaseSizeBeforeDelete = manifestDataRepository.findAll().size();

        // Get the manifestData
        restManifestDataMockMvc.perform(delete("/api/manifest-data/{id}", manifestData.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate ElasticSearch is empty
        boolean manifestDataExistsInEs = manifestDataSearchRepository.exists(manifestData.getId());
        assertThat(manifestDataExistsInEs).isFalse();

        // Validate the database is empty
        List<ManifestData> manifestData = manifestDataRepository.findAll();
        assertThat(manifestData).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchManifestData() throws Exception {
        // Initialize the database
        manifestDataRepository.saveAndFlush(manifestData);
        manifestDataSearchRepository.save(manifestData);

        // Search the manifestData
        restManifestDataMockMvc.perform(get("/api/_search/manifest-data?query=id:" + manifestData.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(manifestData.getId().intValue())))
            .andExpect(jsonPath("$.[*].attribut").value(hasItem(DEFAULT_ATTRIBUT.toString())))
            .andExpect(jsonPath("$.[*].valeurAttendue").value(hasItem(DEFAULT_VALEUR_ATTENDUE.toString())))
            .andExpect(jsonPath("$.[*].messageErreur").value(hasItem(DEFAULT_MESSAGE_ERREUR.toString())))
            .andExpect(jsonPath("$.[*].details").value(hasItem(DEFAULT_DETAILS.toString())));
    }
}
