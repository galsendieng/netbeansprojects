package com.orange.myabstract.web.rest;

import com.orange.myabstract.AbstractApp;

import com.orange.myabstract.domain.LookAheadFragmentCount;
import com.orange.myabstract.repository.LookAheadFragmentCountRepository;
import com.orange.myabstract.service.LookAheadFragmentCountService;
import com.orange.myabstract.repository.search.LookAheadFragmentCountSearchRepository;
import com.orange.myabstract.service.dto.LookAheadFragmentCountDTO;
import com.orange.myabstract.service.mapper.LookAheadFragmentCountMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the LookAheadFragmentCountResource REST controller.
 *
 * @see LookAheadFragmentCountResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = AbstractApp.class)
public class LookAheadFragmentCountResourceIntTest {

    private static final String DEFAULT_VALEUR = "AAAAAAAAAA";
    private static final String UPDATED_VALEUR = "BBBBBBBBBB";

    private static final String DEFAULT_STREAMER = "AAAAAAAAAA";
    private static final String UPDATED_STREAMER = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_DATE_SUP = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_DATE_SUP = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final String DEFAULT_DATE_SUP_STR = DateTimeFormatter.ISO_INSTANT.format(DEFAULT_DATE_SUP);

    @Inject
    private LookAheadFragmentCountRepository lookAheadFragmentCountRepository;

    @Inject
    private LookAheadFragmentCountMapper lookAheadFragmentCountMapper;

    @Inject
    private LookAheadFragmentCountService lookAheadFragmentCountService;

    @Inject
    private LookAheadFragmentCountSearchRepository lookAheadFragmentCountSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Inject
    private EntityManager em;

    private MockMvc restLookAheadFragmentCountMockMvc;

    private LookAheadFragmentCount lookAheadFragmentCount;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        LookAheadFragmentCountResource lookAheadFragmentCountResource = new LookAheadFragmentCountResource();
        ReflectionTestUtils.setField(lookAheadFragmentCountResource, "lookAheadFragmentCountService", lookAheadFragmentCountService);
        this.restLookAheadFragmentCountMockMvc = MockMvcBuilders.standaloneSetup(lookAheadFragmentCountResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static LookAheadFragmentCount createEntity(EntityManager em) {
        LookAheadFragmentCount lookAheadFragmentCount = new LookAheadFragmentCount()
                .valeur(DEFAULT_VALEUR)
                .streamer(DEFAULT_STREAMER)
                .dateSup(DEFAULT_DATE_SUP);
        return lookAheadFragmentCount;
    }

    @Before
    public void initTest() {
        lookAheadFragmentCountSearchRepository.deleteAll();
        lookAheadFragmentCount = createEntity(em);
    }

    @Test
    @Transactional
    public void createLookAheadFragmentCount() throws Exception {
        int databaseSizeBeforeCreate = lookAheadFragmentCountRepository.findAll().size();

        // Create the LookAheadFragmentCount
        LookAheadFragmentCountDTO lookAheadFragmentCountDTO = lookAheadFragmentCountMapper.lookAheadFragmentCountToLookAheadFragmentCountDTO(lookAheadFragmentCount);

        restLookAheadFragmentCountMockMvc.perform(post("/api/look-ahead-fragment-counts")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(lookAheadFragmentCountDTO)))
                .andExpect(status().isCreated());

        // Validate the LookAheadFragmentCount in the database
        List<LookAheadFragmentCount> lookAheadFragmentCounts = lookAheadFragmentCountRepository.findAll();
        assertThat(lookAheadFragmentCounts).hasSize(databaseSizeBeforeCreate + 1);
        LookAheadFragmentCount testLookAheadFragmentCount = lookAheadFragmentCounts.get(lookAheadFragmentCounts.size() - 1);
        assertThat(testLookAheadFragmentCount.getValeur()).isEqualTo(DEFAULT_VALEUR);
        assertThat(testLookAheadFragmentCount.getStreamer()).isEqualTo(DEFAULT_STREAMER);
        assertThat(testLookAheadFragmentCount.getDateSup()).isEqualTo(DEFAULT_DATE_SUP);

        // Validate the LookAheadFragmentCount in ElasticSearch
        LookAheadFragmentCount lookAheadFragmentCountEs = lookAheadFragmentCountSearchRepository.findOne(testLookAheadFragmentCount.getId());
        assertThat(lookAheadFragmentCountEs).isEqualToComparingFieldByField(testLookAheadFragmentCount);
    }

    @Test
    @Transactional
    public void getAllLookAheadFragmentCounts() throws Exception {
        // Initialize the database
        lookAheadFragmentCountRepository.saveAndFlush(lookAheadFragmentCount);

        // Get all the lookAheadFragmentCounts
        restLookAheadFragmentCountMockMvc.perform(get("/api/look-ahead-fragment-counts?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.[*].id").value(hasItem(lookAheadFragmentCount.getId().intValue())))
                .andExpect(jsonPath("$.[*].valeur").value(hasItem(DEFAULT_VALEUR.toString())))
                .andExpect(jsonPath("$.[*].streamer").value(hasItem(DEFAULT_STREAMER.toString())))
                .andExpect(jsonPath("$.[*].dateSup").value(hasItem(DEFAULT_DATE_SUP_STR)));
    }

    @Test
    @Transactional
    public void getLookAheadFragmentCount() throws Exception {
        // Initialize the database
        lookAheadFragmentCountRepository.saveAndFlush(lookAheadFragmentCount);

        // Get the lookAheadFragmentCount
        restLookAheadFragmentCountMockMvc.perform(get("/api/look-ahead-fragment-counts/{id}", lookAheadFragmentCount.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(lookAheadFragmentCount.getId().intValue()))
            .andExpect(jsonPath("$.valeur").value(DEFAULT_VALEUR.toString()))
            .andExpect(jsonPath("$.streamer").value(DEFAULT_STREAMER.toString()))
            .andExpect(jsonPath("$.dateSup").value(DEFAULT_DATE_SUP_STR));
    }

    @Test
    @Transactional
    public void getNonExistingLookAheadFragmentCount() throws Exception {
        // Get the lookAheadFragmentCount
        restLookAheadFragmentCountMockMvc.perform(get("/api/look-ahead-fragment-counts/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateLookAheadFragmentCount() throws Exception {
        // Initialize the database
        lookAheadFragmentCountRepository.saveAndFlush(lookAheadFragmentCount);
        lookAheadFragmentCountSearchRepository.save(lookAheadFragmentCount);
        int databaseSizeBeforeUpdate = lookAheadFragmentCountRepository.findAll().size();

        // Update the lookAheadFragmentCount
        LookAheadFragmentCount updatedLookAheadFragmentCount = lookAheadFragmentCountRepository.findOne(lookAheadFragmentCount.getId());
        updatedLookAheadFragmentCount
                .valeur(UPDATED_VALEUR)
                .streamer(UPDATED_STREAMER)
                .dateSup(UPDATED_DATE_SUP);
        LookAheadFragmentCountDTO lookAheadFragmentCountDTO = lookAheadFragmentCountMapper.lookAheadFragmentCountToLookAheadFragmentCountDTO(updatedLookAheadFragmentCount);

        restLookAheadFragmentCountMockMvc.perform(put("/api/look-ahead-fragment-counts")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(lookAheadFragmentCountDTO)))
                .andExpect(status().isOk());

        // Validate the LookAheadFragmentCount in the database
        List<LookAheadFragmentCount> lookAheadFragmentCounts = lookAheadFragmentCountRepository.findAll();
        assertThat(lookAheadFragmentCounts).hasSize(databaseSizeBeforeUpdate);
        LookAheadFragmentCount testLookAheadFragmentCount = lookAheadFragmentCounts.get(lookAheadFragmentCounts.size() - 1);
        assertThat(testLookAheadFragmentCount.getValeur()).isEqualTo(UPDATED_VALEUR);
        assertThat(testLookAheadFragmentCount.getStreamer()).isEqualTo(UPDATED_STREAMER);
        assertThat(testLookAheadFragmentCount.getDateSup()).isEqualTo(UPDATED_DATE_SUP);

        // Validate the LookAheadFragmentCount in ElasticSearch
        LookAheadFragmentCount lookAheadFragmentCountEs = lookAheadFragmentCountSearchRepository.findOne(testLookAheadFragmentCount.getId());
        assertThat(lookAheadFragmentCountEs).isEqualToComparingFieldByField(testLookAheadFragmentCount);
    }

    @Test
    @Transactional
    public void deleteLookAheadFragmentCount() throws Exception {
        // Initialize the database
        lookAheadFragmentCountRepository.saveAndFlush(lookAheadFragmentCount);
        lookAheadFragmentCountSearchRepository.save(lookAheadFragmentCount);
        int databaseSizeBeforeDelete = lookAheadFragmentCountRepository.findAll().size();

        // Get the lookAheadFragmentCount
        restLookAheadFragmentCountMockMvc.perform(delete("/api/look-ahead-fragment-counts/{id}", lookAheadFragmentCount.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate ElasticSearch is empty
        boolean lookAheadFragmentCountExistsInEs = lookAheadFragmentCountSearchRepository.exists(lookAheadFragmentCount.getId());
        assertThat(lookAheadFragmentCountExistsInEs).isFalse();

        // Validate the database is empty
        List<LookAheadFragmentCount> lookAheadFragmentCounts = lookAheadFragmentCountRepository.findAll();
        assertThat(lookAheadFragmentCounts).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchLookAheadFragmentCount() throws Exception {
        // Initialize the database
        lookAheadFragmentCountRepository.saveAndFlush(lookAheadFragmentCount);
        lookAheadFragmentCountSearchRepository.save(lookAheadFragmentCount);

        // Search the lookAheadFragmentCount
        restLookAheadFragmentCountMockMvc.perform(get("/api/_search/look-ahead-fragment-counts?query=id:" + lookAheadFragmentCount.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(lookAheadFragmentCount.getId().intValue())))
            .andExpect(jsonPath("$.[*].valeur").value(hasItem(DEFAULT_VALEUR.toString())))
            .andExpect(jsonPath("$.[*].streamer").value(hasItem(DEFAULT_STREAMER.toString())))
            .andExpect(jsonPath("$.[*].dateSup").value(hasItem(DEFAULT_DATE_SUP_STR)));
    }
}
