package com.orange.myabstract.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.orange.myabstract.service.MaxWidthQltService;
import com.orange.myabstract.web.rest.util.HeaderUtil;
import com.orange.myabstract.web.rest.util.PaginationUtil;
import com.orange.myabstract.service.dto.MaxWidthQltDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing MaxWidthQlt.
 */
@RestController
@RequestMapping("/api")
public class MaxWidthQltResource {

    private final Logger log = LoggerFactory.getLogger(MaxWidthQltResource.class);
        
    @Inject
    private MaxWidthQltService maxWidthQltService;

    /**
     * POST  /max-width-qlts : Create a new maxWidthQlt.
     *
     * @param maxWidthQltDTO the maxWidthQltDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new maxWidthQltDTO, or with status 400 (Bad Request) if the maxWidthQlt has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/max-width-qlts")
    @Timed
    public ResponseEntity<MaxWidthQltDTO> createMaxWidthQlt(@RequestBody MaxWidthQltDTO maxWidthQltDTO) throws URISyntaxException {
        log.debug("REST request to save MaxWidthQlt : {}", maxWidthQltDTO);
        if (maxWidthQltDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("maxWidthQlt", "idexists", "A new maxWidthQlt cannot already have an ID")).body(null);
        }
        MaxWidthQltDTO result = maxWidthQltService.save(maxWidthQltDTO);
        return ResponseEntity.created(new URI("/api/max-width-qlts/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("maxWidthQlt", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /max-width-qlts : Updates an existing maxWidthQlt.
     *
     * @param maxWidthQltDTO the maxWidthQltDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated maxWidthQltDTO,
     * or with status 400 (Bad Request) if the maxWidthQltDTO is not valid,
     * or with status 500 (Internal Server Error) if the maxWidthQltDTO couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/max-width-qlts")
    @Timed
    public ResponseEntity<MaxWidthQltDTO> updateMaxWidthQlt(@RequestBody MaxWidthQltDTO maxWidthQltDTO) throws URISyntaxException {
        log.debug("REST request to update MaxWidthQlt : {}", maxWidthQltDTO);
        if (maxWidthQltDTO.getId() == null) {
            return createMaxWidthQlt(maxWidthQltDTO);
        }
        MaxWidthQltDTO result = maxWidthQltService.save(maxWidthQltDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("maxWidthQlt", maxWidthQltDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /max-width-qlts : get all the maxWidthQlts.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of maxWidthQlts in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @GetMapping("/max-width-qlts")
    @Timed
    public ResponseEntity<List<MaxWidthQltDTO>> getAllMaxWidthQlts(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of MaxWidthQlts");
        Page<MaxWidthQltDTO> page = maxWidthQltService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/max-width-qlts");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /max-width-qlts/:id : get the "id" maxWidthQlt.
     *
     * @param id the id of the maxWidthQltDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the maxWidthQltDTO, or with status 404 (Not Found)
     */
    @GetMapping("/max-width-qlts/{id}")
    @Timed
    public ResponseEntity<MaxWidthQltDTO> getMaxWidthQlt(@PathVariable Long id) {
        log.debug("REST request to get MaxWidthQlt : {}", id);
        MaxWidthQltDTO maxWidthQltDTO = maxWidthQltService.findOne(id);
        return Optional.ofNullable(maxWidthQltDTO)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /max-width-qlts/:id : delete the "id" maxWidthQlt.
     *
     * @param id the id of the maxWidthQltDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/max-width-qlts/{id}")
    @Timed
    public ResponseEntity<Void> deleteMaxWidthQlt(@PathVariable Long id) {
        log.debug("REST request to delete MaxWidthQlt : {}", id);
        maxWidthQltService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("maxWidthQlt", id.toString())).build();
    }

    /**
     * SEARCH  /_search/max-width-qlts?query=:query : search for the maxWidthQlt corresponding
     * to the query.
     *
     * @param query the query of the maxWidthQlt search 
     * @param pageable the pagination information
     * @return the result of the search
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @GetMapping("/_search/max-width-qlts")
    @Timed
    public ResponseEntity<List<MaxWidthQltDTO>> searchMaxWidthQlts(@RequestParam String query, Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to search for a page of MaxWidthQlts for query {}", query);
        Page<MaxWidthQltDTO> page = maxWidthQltService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/max-width-qlts");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
