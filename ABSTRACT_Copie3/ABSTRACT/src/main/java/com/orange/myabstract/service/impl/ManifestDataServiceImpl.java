package com.orange.myabstract.service.impl;

import com.orange.myabstract.service.ManifestDataService;
import com.orange.myabstract.domain.ManifestData;
import com.orange.myabstract.repository.ManifestDataRepository;
import com.orange.myabstract.repository.search.ManifestDataSearchRepository;
import com.orange.myabstract.service.dto.ManifestDataDTO;
import com.orange.myabstract.service.mapper.ManifestDataMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing ManifestData.
 */
@Service
@Transactional
public class ManifestDataServiceImpl implements ManifestDataService{

    private final Logger log = LoggerFactory.getLogger(ManifestDataServiceImpl.class);
    
    @Inject
    private ManifestDataRepository manifestDataRepository;

    @Inject
    private ManifestDataMapper manifestDataMapper;

    @Inject
    private ManifestDataSearchRepository manifestDataSearchRepository;

    /**
     * Save a manifestData.
     *
     * @param manifestDataDTO the entity to save
     * @return the persisted entity
     */
    public ManifestDataDTO save(ManifestDataDTO manifestDataDTO) {
        log.debug("Request to save ManifestData : {}", manifestDataDTO);
        ManifestData manifestData = manifestDataMapper.manifestDataDTOToManifestData(manifestDataDTO);
        manifestData = manifestDataRepository.save(manifestData);
        ManifestDataDTO result = manifestDataMapper.manifestDataToManifestDataDTO(manifestData);
        manifestDataSearchRepository.save(manifestData);
        return result;
    }

    /**
     *  Get all the manifestData.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true) 
    public Page<ManifestDataDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ManifestData");
        Page<ManifestData> result = manifestDataRepository.findAll(pageable);
        return result.map(manifestData -> manifestDataMapper.manifestDataToManifestDataDTO(manifestData));
    }

    /**
     *  Get one manifestData by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true) 
    public ManifestDataDTO findOne(Long id) {
        log.debug("Request to get ManifestData : {}", id);
        ManifestData manifestData = manifestDataRepository.findOne(id);
        ManifestDataDTO manifestDataDTO = manifestDataMapper.manifestDataToManifestDataDTO(manifestData);
        return manifestDataDTO;
    }

    /**
     *  Delete the  manifestData by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete ManifestData : {}", id);
        manifestDataRepository.delete(id);
        manifestDataSearchRepository.delete(id);
    }

    /**
     * Search for the manifestData corresponding to the query.
     *
     *  @param query the query of the search
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ManifestDataDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of ManifestData for query {}", query);
        Page<ManifestData> result = manifestDataSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(manifestData -> manifestDataMapper.manifestDataToManifestDataDTO(manifestData));
    }
}
