package com.orange.myabstract.service.impl;

import com.orange.myabstract.service.DVRWindowLengthService;
import com.orange.myabstract.domain.DVRWindowLength;
import com.orange.myabstract.repository.DVRWindowLengthRepository;
import com.orange.myabstract.repository.search.DVRWindowLengthSearchRepository;
import com.orange.myabstract.service.dto.DVRWindowLengthDTO;
import com.orange.myabstract.service.mapper.DVRWindowLengthMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing DVRWindowLength.
 */
@Service
@Transactional
public class DVRWindowLengthServiceImpl implements DVRWindowLengthService{

    private final Logger log = LoggerFactory.getLogger(DVRWindowLengthServiceImpl.class);
    
    @Inject
    private DVRWindowLengthRepository dVRWindowLengthRepository;

    @Inject
    private DVRWindowLengthMapper dVRWindowLengthMapper;

    @Inject
    private DVRWindowLengthSearchRepository dVRWindowLengthSearchRepository;

    /**
     * Save a dVRWindowLength.
     *
     * @param dVRWindowLengthDTO the entity to save
     * @return the persisted entity
     */
    public DVRWindowLengthDTO save(DVRWindowLengthDTO dVRWindowLengthDTO) {
        log.debug("Request to save DVRWindowLength : {}", dVRWindowLengthDTO);
        DVRWindowLength dVRWindowLength = dVRWindowLengthMapper.dVRWindowLengthDTOToDVRWindowLength(dVRWindowLengthDTO);
        dVRWindowLength = dVRWindowLengthRepository.save(dVRWindowLength);
        DVRWindowLengthDTO result = dVRWindowLengthMapper.dVRWindowLengthToDVRWindowLengthDTO(dVRWindowLength);
        dVRWindowLengthSearchRepository.save(dVRWindowLength);
        return result;
    }

    /**
     *  Get all the dVRWindowLengths.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true) 
    public Page<DVRWindowLengthDTO> findAll(Pageable pageable) {
        log.debug("Request to get all DVRWindowLengths");
        Page<DVRWindowLength> result = dVRWindowLengthRepository.findAll(pageable);
        return result.map(dVRWindowLength -> dVRWindowLengthMapper.dVRWindowLengthToDVRWindowLengthDTO(dVRWindowLength));
    }

    /**
     *  Get one dVRWindowLength by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true) 
    public DVRWindowLengthDTO findOne(Long id) {
        log.debug("Request to get DVRWindowLength : {}", id);
        DVRWindowLength dVRWindowLength = dVRWindowLengthRepository.findOne(id);
        DVRWindowLengthDTO dVRWindowLengthDTO = dVRWindowLengthMapper.dVRWindowLengthToDVRWindowLengthDTO(dVRWindowLength);
        return dVRWindowLengthDTO;
    }

    /**
     *  Delete the  dVRWindowLength by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete DVRWindowLength : {}", id);
        dVRWindowLengthRepository.delete(id);
        dVRWindowLengthSearchRepository.delete(id);
    }

    /**
     * Search for the dVRWindowLength corresponding to the query.
     *
     *  @param query the query of the search
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<DVRWindowLengthDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of DVRWindowLengths for query {}", query);
        Page<DVRWindowLength> result = dVRWindowLengthSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(dVRWindowLength -> dVRWindowLengthMapper.dVRWindowLengthToDVRWindowLengthDTO(dVRWindowLength));
    }
}
