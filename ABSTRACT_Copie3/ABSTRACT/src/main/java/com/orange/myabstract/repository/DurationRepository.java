package com.orange.myabstract.repository;

import com.orange.myabstract.domain.Duration;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the Duration entity.
 */
@SuppressWarnings("unused")
public interface DurationRepository extends JpaRepository<Duration,Long> {

}
