package com.orange.myabstract.repository;

import com.orange.myabstract.domain.DisplayHeight;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the DisplayHeight entity.
 */
@SuppressWarnings("unused")
public interface DisplayHeightRepository extends JpaRepository<DisplayHeight,Long> {

}
