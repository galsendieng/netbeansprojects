package com.orange.myabstract.repository;

import com.orange.myabstract.domain.Bitrate;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the Bitrate entity.
 */
@SuppressWarnings("unused")
public interface BitrateRepository extends JpaRepository<Bitrate,Long> {


}
