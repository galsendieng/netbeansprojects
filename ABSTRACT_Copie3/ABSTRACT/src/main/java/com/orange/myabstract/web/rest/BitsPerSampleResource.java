package com.orange.myabstract.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.orange.myabstract.service.BitsPerSampleService;
import com.orange.myabstract.web.rest.util.HeaderUtil;
import com.orange.myabstract.web.rest.util.PaginationUtil;
import com.orange.myabstract.service.dto.BitsPerSampleDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing BitsPerSample.
 */
@RestController
@RequestMapping("/api")
public class BitsPerSampleResource {

    private final Logger log = LoggerFactory.getLogger(BitsPerSampleResource.class);
        
    @Inject
    private BitsPerSampleService bitsPerSampleService;

    /**
     * POST  /bits-per-samples : Create a new bitsPerSample.
     *
     * @param bitsPerSampleDTO the bitsPerSampleDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new bitsPerSampleDTO, or with status 400 (Bad Request) if the bitsPerSample has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/bits-per-samples")
    @Timed
    public ResponseEntity<BitsPerSampleDTO> createBitsPerSample(@RequestBody BitsPerSampleDTO bitsPerSampleDTO) throws URISyntaxException {
        log.debug("REST request to save BitsPerSample : {}", bitsPerSampleDTO);
        if (bitsPerSampleDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("bitsPerSample", "idexists", "A new bitsPerSample cannot already have an ID")).body(null);
        }
        BitsPerSampleDTO result = bitsPerSampleService.save(bitsPerSampleDTO);
        return ResponseEntity.created(new URI("/api/bits-per-samples/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("bitsPerSample", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /bits-per-samples : Updates an existing bitsPerSample.
     *
     * @param bitsPerSampleDTO the bitsPerSampleDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated bitsPerSampleDTO,
     * or with status 400 (Bad Request) if the bitsPerSampleDTO is not valid,
     * or with status 500 (Internal Server Error) if the bitsPerSampleDTO couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/bits-per-samples")
    @Timed
    public ResponseEntity<BitsPerSampleDTO> updateBitsPerSample(@RequestBody BitsPerSampleDTO bitsPerSampleDTO) throws URISyntaxException {
        log.debug("REST request to update BitsPerSample : {}", bitsPerSampleDTO);
        if (bitsPerSampleDTO.getId() == null) {
            return createBitsPerSample(bitsPerSampleDTO);
        }
        BitsPerSampleDTO result = bitsPerSampleService.save(bitsPerSampleDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("bitsPerSample", bitsPerSampleDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /bits-per-samples : get all the bitsPerSamples.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of bitsPerSamples in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @GetMapping("/bits-per-samples")
    @Timed
    public ResponseEntity<List<BitsPerSampleDTO>> getAllBitsPerSamples(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of BitsPerSamples");
        Page<BitsPerSampleDTO> page = bitsPerSampleService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/bits-per-samples");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /bits-per-samples/:id : get the "id" bitsPerSample.
     *
     * @param id the id of the bitsPerSampleDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the bitsPerSampleDTO, or with status 404 (Not Found)
     */
    @GetMapping("/bits-per-samples/{id}")
    @Timed
    public ResponseEntity<BitsPerSampleDTO> getBitsPerSample(@PathVariable Long id) {
        log.debug("REST request to get BitsPerSample : {}", id);
        BitsPerSampleDTO bitsPerSampleDTO = bitsPerSampleService.findOne(id);
        return Optional.ofNullable(bitsPerSampleDTO)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /bits-per-samples/:id : delete the "id" bitsPerSample.
     *
     * @param id the id of the bitsPerSampleDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/bits-per-samples/{id}")
    @Timed
    public ResponseEntity<Void> deleteBitsPerSample(@PathVariable Long id) {
        log.debug("REST request to delete BitsPerSample : {}", id);
        bitsPerSampleService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("bitsPerSample", id.toString())).build();
    }

    /**
     * SEARCH  /_search/bits-per-samples?query=:query : search for the bitsPerSample corresponding
     * to the query.
     *
     * @param query the query of the bitsPerSample search 
     * @param pageable the pagination information
     * @return the result of the search
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @GetMapping("/_search/bits-per-samples")
    @Timed
    public ResponseEntity<List<BitsPerSampleDTO>> searchBitsPerSamples(@RequestParam String query, Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to search for a page of BitsPerSamples for query {}", query);
        Page<BitsPerSampleDTO> page = bitsPerSampleService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/bits-per-samples");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
