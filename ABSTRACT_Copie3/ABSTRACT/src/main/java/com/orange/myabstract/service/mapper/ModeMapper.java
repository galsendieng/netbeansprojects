package com.orange.myabstract.service.mapper;

import com.orange.myabstract.domain.*;
import com.orange.myabstract.service.dto.ModeDTO;

import org.mapstruct.*;
import java.util.List;

/**
 * Mapper for the entity Mode and its DTO ModeDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface ModeMapper {

    ModeDTO modeToModeDTO(Mode mode);

    List<ModeDTO> modesToModeDTOs(List<Mode> modes);

    @Mapping(target = "playings", ignore = true)
    Mode modeDTOToMode(ModeDTO modeDTO);

    List<Mode> modeDTOsToModes(List<ModeDTO> modeDTOs);
}
