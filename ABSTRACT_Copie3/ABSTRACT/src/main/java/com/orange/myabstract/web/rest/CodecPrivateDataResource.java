package com.orange.myabstract.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.orange.myabstract.service.CodecPrivateDataService;
import com.orange.myabstract.web.rest.util.HeaderUtil;
import com.orange.myabstract.web.rest.util.PaginationUtil;
import com.orange.myabstract.service.dto.CodecPrivateDataDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing CodecPrivateData.
 */
@RestController
@RequestMapping("/api")
public class CodecPrivateDataResource {

    private final Logger log = LoggerFactory.getLogger(CodecPrivateDataResource.class);
        
    @Inject
    private CodecPrivateDataService codecPrivateDataService;

    /**
     * POST  /codec-private-data : Create a new codecPrivateData.
     *
     * @param codecPrivateDataDTO the codecPrivateDataDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new codecPrivateDataDTO, or with status 400 (Bad Request) if the codecPrivateData has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/codec-private-data")
    @Timed
    public ResponseEntity<CodecPrivateDataDTO> createCodecPrivateData(@RequestBody CodecPrivateDataDTO codecPrivateDataDTO) throws URISyntaxException {
        log.debug("REST request to save CodecPrivateData : {}", codecPrivateDataDTO);
        if (codecPrivateDataDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("codecPrivateData", "idexists", "A new codecPrivateData cannot already have an ID")).body(null);
        }
        CodecPrivateDataDTO result = codecPrivateDataService.save(codecPrivateDataDTO);
        return ResponseEntity.created(new URI("/api/codec-private-data/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("codecPrivateData", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /codec-private-data : Updates an existing codecPrivateData.
     *
     * @param codecPrivateDataDTO the codecPrivateDataDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated codecPrivateDataDTO,
     * or with status 400 (Bad Request) if the codecPrivateDataDTO is not valid,
     * or with status 500 (Internal Server Error) if the codecPrivateDataDTO couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/codec-private-data")
    @Timed
    public ResponseEntity<CodecPrivateDataDTO> updateCodecPrivateData(@RequestBody CodecPrivateDataDTO codecPrivateDataDTO) throws URISyntaxException {
        log.debug("REST request to update CodecPrivateData : {}", codecPrivateDataDTO);
        if (codecPrivateDataDTO.getId() == null) {
            return createCodecPrivateData(codecPrivateDataDTO);
        }
        CodecPrivateDataDTO result = codecPrivateDataService.save(codecPrivateDataDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("codecPrivateData", codecPrivateDataDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /codec-private-data : get all the codecPrivateData.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of codecPrivateData in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @GetMapping("/codec-private-data")
    @Timed
    public ResponseEntity<List<CodecPrivateDataDTO>> getAllCodecPrivateData(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of CodecPrivateData");
        Page<CodecPrivateDataDTO> page = codecPrivateDataService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/codec-private-data");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /codec-private-data/:id : get the "id" codecPrivateData.
     *
     * @param id the id of the codecPrivateDataDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the codecPrivateDataDTO, or with status 404 (Not Found)
     */
    @GetMapping("/codec-private-data/{id}")
    @Timed
    public ResponseEntity<CodecPrivateDataDTO> getCodecPrivateData(@PathVariable Long id) {
        log.debug("REST request to get CodecPrivateData : {}", id);
        CodecPrivateDataDTO codecPrivateDataDTO = codecPrivateDataService.findOne(id);
        return Optional.ofNullable(codecPrivateDataDTO)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /codec-private-data/:id : delete the "id" codecPrivateData.
     *
     * @param id the id of the codecPrivateDataDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/codec-private-data/{id}")
    @Timed
    public ResponseEntity<Void> deleteCodecPrivateData(@PathVariable Long id) {
        log.debug("REST request to delete CodecPrivateData : {}", id);
        codecPrivateDataService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("codecPrivateData", id.toString())).build();
    }

    /**
     * SEARCH  /_search/codec-private-data?query=:query : search for the codecPrivateData corresponding
     * to the query.
     *
     * @param query the query of the codecPrivateData search 
     * @param pageable the pagination information
     * @return the result of the search
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @GetMapping("/_search/codec-private-data")
    @Timed
    public ResponseEntity<List<CodecPrivateDataDTO>> searchCodecPrivateData(@RequestParam String query, Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to search for a page of CodecPrivateData for query {}", query);
        Page<CodecPrivateDataDTO> page = codecPrivateDataService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/codec-private-data");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
