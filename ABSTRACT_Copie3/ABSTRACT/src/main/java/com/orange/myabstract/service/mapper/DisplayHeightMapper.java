package com.orange.myabstract.service.mapper;

import com.orange.myabstract.domain.*;
import com.orange.myabstract.service.dto.DisplayHeightDTO;

import org.mapstruct.*;
import java.util.List;

/**
 * Mapper for the entity DisplayHeight and its DTO DisplayHeightDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface DisplayHeightMapper {

    DisplayHeightDTO displayHeightToDisplayHeightDTO(DisplayHeight displayHeight);

    List<DisplayHeightDTO> displayHeightsToDisplayHeightDTOs(List<DisplayHeight> displayHeights);

    DisplayHeight displayHeightDTOToDisplayHeight(DisplayHeightDTO displayHeightDTO);

    List<DisplayHeight> displayHeightDTOsToDisplayHeights(List<DisplayHeightDTO> displayHeightDTOs);
}
