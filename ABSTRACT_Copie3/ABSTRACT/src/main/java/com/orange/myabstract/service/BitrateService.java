package com.orange.myabstract.service;

import com.orange.myabstract.service.dto.BitrateDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.LinkedList;
import java.util.List;

/**
 * Service Interface for managing Bitrate.
 */
public interface BitrateService {

    /**
     * Save a bitrate.
     *
     * @param bitrateDTO the entity to save
     * @return the persisted entity
     */
    BitrateDTO save(BitrateDTO bitrateDTO);

    /**
     *  Get all the bitrates.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<BitrateDTO> findAll(Pageable pageable);

    /**
     *  Get the "id" bitrate.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    BitrateDTO findOne(Long id);

    /**
    * Service fournissant des Bitrates par Streamer
    */

    
    /**
     *  Delete the "id" bitrate.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Search for the bitrate corresponding to the query.
     *
     *  @param query the query of the search
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<BitrateDTO> search(String query, Pageable pageable);
}
