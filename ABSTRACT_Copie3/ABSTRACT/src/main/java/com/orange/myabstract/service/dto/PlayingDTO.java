package com.orange.myabstract.service.dto;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;
import javax.persistence.Lob;


/**
 * A DTO for the Playing entity.
 */
public class PlayingDTO implements Serializable {

    private Long id;

    @NotNull
    private String link;

    @Lob
    private byte[] fileTest;

    private String fileTestContentType;

    private Set<ChannelsDTO> listchannels = new HashSet<>();

    private Set<CommandeDTO> listcommands = new HashSet<>();

    private Long theModeId;
    

    private String theModeName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
    public byte[] getFileTest() {
        return fileTest;
    }

    public void setFileTest(byte[] fileTest) {
        this.fileTest = fileTest;
    }

    public String getFileTestContentType() {
        return fileTestContentType;
    }

    public void setFileTestContentType(String fileTestContentType) {
        this.fileTestContentType = fileTestContentType;
    }

    public Set<ChannelsDTO> getListchannels() {
        return listchannels;
    }

    public void setListchannels(Set<ChannelsDTO> channels) {
        this.listchannels = channels;
    }

    public Set<CommandeDTO> getListcommands() {
        return listcommands;
    }

    public void setListcommands(Set<CommandeDTO> commandes) {
        this.listcommands = commandes;
    }

    public Long getTheModeId() {
        return theModeId;
    }

    public void setTheModeId(Long modeId) {
        this.theModeId = modeId;
    }


    public String getTheModeName() {
        return theModeName;
    }

    public void setTheModeName(String modeName) {
        this.theModeName = modeName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PlayingDTO playingDTO = (PlayingDTO) o;

        if ( ! Objects.equals(id, playingDTO.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "PlayingDTO{" +
            "id=" + id +
            ", link='" + link + "'" +
            ", fileTest='" + fileTest + "'" +
            '}';
    }
}
