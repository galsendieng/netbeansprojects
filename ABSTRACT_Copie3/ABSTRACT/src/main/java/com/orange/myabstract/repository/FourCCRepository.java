package com.orange.myabstract.repository;

import com.orange.myabstract.domain.FourCC;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the FourCC entity.
 */
@SuppressWarnings("unused")
public interface FourCCRepository extends JpaRepository<FourCC,Long> {

}
