package com.orange.myabstract.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.orange.myabstract.service.AudioTagService;
import com.orange.myabstract.web.rest.util.HeaderUtil;
import com.orange.myabstract.web.rest.util.PaginationUtil;
import com.orange.myabstract.service.dto.AudioTagDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing AudioTag.
 */
@RestController
@RequestMapping("/api")
public class AudioTagResource {

    private final Logger log = LoggerFactory.getLogger(AudioTagResource.class);
        
    @Inject
    private AudioTagService audioTagService;

    /**
     * POST  /audio-tags : Create a new audioTag.
     *
     * @param audioTagDTO the audioTagDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new audioTagDTO, or with status 400 (Bad Request) if the audioTag has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/audio-tags")
    @Timed
    public ResponseEntity<AudioTagDTO> createAudioTag(@RequestBody AudioTagDTO audioTagDTO) throws URISyntaxException {
        log.debug("REST request to save AudioTag : {}", audioTagDTO);
        if (audioTagDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("audioTag", "idexists", "A new audioTag cannot already have an ID")).body(null);
        }
        AudioTagDTO result = audioTagService.save(audioTagDTO);
        return ResponseEntity.created(new URI("/api/audio-tags/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("audioTag", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /audio-tags : Updates an existing audioTag.
     *
     * @param audioTagDTO the audioTagDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated audioTagDTO,
     * or with status 400 (Bad Request) if the audioTagDTO is not valid,
     * or with status 500 (Internal Server Error) if the audioTagDTO couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/audio-tags")
    @Timed
    public ResponseEntity<AudioTagDTO> updateAudioTag(@RequestBody AudioTagDTO audioTagDTO) throws URISyntaxException {
        log.debug("REST request to update AudioTag : {}", audioTagDTO);
        if (audioTagDTO.getId() == null) {
            return createAudioTag(audioTagDTO);
        }
        AudioTagDTO result = audioTagService.save(audioTagDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("audioTag", audioTagDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /audio-tags : get all the audioTags.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of audioTags in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @GetMapping("/audio-tags")
    @Timed
    public ResponseEntity<List<AudioTagDTO>> getAllAudioTags(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of AudioTags");
        Page<AudioTagDTO> page = audioTagService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/audio-tags");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /audio-tags/:id : get the "id" audioTag.
     *
     * @param id the id of the audioTagDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the audioTagDTO, or with status 404 (Not Found)
     */
    @GetMapping("/audio-tags/{id}")
    @Timed
    public ResponseEntity<AudioTagDTO> getAudioTag(@PathVariable Long id) {
        log.debug("REST request to get AudioTag : {}", id);
        AudioTagDTO audioTagDTO = audioTagService.findOne(id);
        return Optional.ofNullable(audioTagDTO)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /audio-tags/:id : delete the "id" audioTag.
     *
     * @param id the id of the audioTagDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/audio-tags/{id}")
    @Timed
    public ResponseEntity<Void> deleteAudioTag(@PathVariable Long id) {
        log.debug("REST request to delete AudioTag : {}", id);
        audioTagService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("audioTag", id.toString())).build();
    }

    /**
     * SEARCH  /_search/audio-tags?query=:query : search for the audioTag corresponding
     * to the query.
     *
     * @param query the query of the audioTag search 
     * @param pageable the pagination information
     * @return the result of the search
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @GetMapping("/_search/audio-tags")
    @Timed
    public ResponseEntity<List<AudioTagDTO>> searchAudioTags(@RequestParam String query, Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to search for a page of AudioTags for query {}", query);
        Page<AudioTagDTO> page = audioTagService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/audio-tags");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
