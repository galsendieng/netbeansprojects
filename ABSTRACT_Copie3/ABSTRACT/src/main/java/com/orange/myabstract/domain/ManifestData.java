package com.orange.myabstract.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A ManifestData.
 */
@Entity
@Table(name = "manifest_data")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "manifestdata")
public class ManifestData implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Column(name = "attribut", nullable = false)
    private String attribut;

    @NotNull
    @Column(name = "valeur_attendue", nullable = false)
    private String valeurAttendue;

    @NotNull
    @Column(name = "message_erreur", nullable = false)
    private String messageErreur;

    @Column(name = "details")
    private String details;

    @ManyToOne
    private Importance nivImportance;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAttribut() {
        return attribut;
    }

    public ManifestData attribut(String attribut) {
        this.attribut = attribut;
        return this;
    }

    public void setAttribut(String attribut) {
        this.attribut = attribut;
    }

    public String getValeurAttendue() {
        return valeurAttendue;
    }

    public ManifestData valeurAttendue(String valeurAttendue) {
        this.valeurAttendue = valeurAttendue;
        return this;
    }

    public void setValeurAttendue(String valeurAttendue) {
        this.valeurAttendue = valeurAttendue;
    }

    public String getMessageErreur() {
        return messageErreur;
    }

    public ManifestData messageErreur(String messageErreur) {
        this.messageErreur = messageErreur;
        return this;
    }

    public void setMessageErreur(String messageErreur) {
        this.messageErreur = messageErreur;
    }

    public String getDetails() {
        return details;
    }

    public ManifestData details(String details) {
        this.details = details;
        return this;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public Importance getNivImportance() {
        return nivImportance;
    }

    public ManifestData nivImportance(Importance importance) {
        this.nivImportance = importance;
        return this;
    }

    public void setNivImportance(Importance importance) {
        this.nivImportance = importance;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ManifestData manifestData = (ManifestData) o;
        if(manifestData.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, manifestData.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "ManifestData{" +
            "id=" + id +
            ", attribut='" + attribut + "'" +
            ", valeurAttendue='" + valeurAttendue + "'" +
            ", messageErreur='" + messageErreur + "'" +
            ", details='" + details + "'" +
            '}';
    }
}
