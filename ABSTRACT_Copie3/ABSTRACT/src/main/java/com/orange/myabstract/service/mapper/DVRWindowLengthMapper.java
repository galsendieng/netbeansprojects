package com.orange.myabstract.service.mapper;

import com.orange.myabstract.domain.*;
import com.orange.myabstract.service.dto.DVRWindowLengthDTO;

import org.mapstruct.*;
import java.util.List;

/**
 * Mapper for the entity DVRWindowLength and its DTO DVRWindowLengthDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface DVRWindowLengthMapper {

    DVRWindowLengthDTO dVRWindowLengthToDVRWindowLengthDTO(DVRWindowLength dVRWindowLength);

    List<DVRWindowLengthDTO> dVRWindowLengthsToDVRWindowLengthDTOs(List<DVRWindowLength> dVRWindowLengths);

    DVRWindowLength dVRWindowLengthDTOToDVRWindowLength(DVRWindowLengthDTO dVRWindowLengthDTO);

    List<DVRWindowLength> dVRWindowLengthDTOsToDVRWindowLengths(List<DVRWindowLengthDTO> dVRWindowLengthDTOs);
}
