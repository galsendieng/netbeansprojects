package com.orange.myabstract.service.impl;

import com.orange.myabstract.service.MinorVersionService;
import com.orange.myabstract.domain.MinorVersion;
import com.orange.myabstract.repository.MinorVersionRepository;
import com.orange.myabstract.repository.search.MinorVersionSearchRepository;
import com.orange.myabstract.service.dto.MinorVersionDTO;
import com.orange.myabstract.service.mapper.MinorVersionMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing MinorVersion.
 */
@Service
@Transactional
public class MinorVersionServiceImpl implements MinorVersionService{

    private final Logger log = LoggerFactory.getLogger(MinorVersionServiceImpl.class);
    
    @Inject
    private MinorVersionRepository minorVersionRepository;

    @Inject
    private MinorVersionMapper minorVersionMapper;

    @Inject
    private MinorVersionSearchRepository minorVersionSearchRepository;

    /**
     * Save a minorVersion.
     *
     * @param minorVersionDTO the entity to save
     * @return the persisted entity
     */
    public MinorVersionDTO save(MinorVersionDTO minorVersionDTO) {
        log.debug("Request to save MinorVersion : {}", minorVersionDTO);
        MinorVersion minorVersion = minorVersionMapper.minorVersionDTOToMinorVersion(minorVersionDTO);
        minorVersion = minorVersionRepository.save(minorVersion);
        MinorVersionDTO result = minorVersionMapper.minorVersionToMinorVersionDTO(minorVersion);
        minorVersionSearchRepository.save(minorVersion);
        return result;
    }

    /**
     *  Get all the minorVersions.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true) 
    public Page<MinorVersionDTO> findAll(Pageable pageable) {
        log.debug("Request to get all MinorVersions");
        Page<MinorVersion> result = minorVersionRepository.findAll(pageable);
        return result.map(minorVersion -> minorVersionMapper.minorVersionToMinorVersionDTO(minorVersion));
    }

    /**
     *  Get one minorVersion by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true) 
    public MinorVersionDTO findOne(Long id) {
        log.debug("Request to get MinorVersion : {}", id);
        MinorVersion minorVersion = minorVersionRepository.findOne(id);
        MinorVersionDTO minorVersionDTO = minorVersionMapper.minorVersionToMinorVersionDTO(minorVersion);
        return minorVersionDTO;
    }

    /**
     *  Delete the  minorVersion by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete MinorVersion : {}", id);
        minorVersionRepository.delete(id);
        minorVersionSearchRepository.delete(id);
    }

    /**
     * Search for the minorVersion corresponding to the query.
     *
     *  @param query the query of the search
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<MinorVersionDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of MinorVersions for query {}", query);
        Page<MinorVersion> result = minorVersionSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(minorVersion -> minorVersionMapper.minorVersionToMinorVersionDTO(minorVersion));
    }
}
