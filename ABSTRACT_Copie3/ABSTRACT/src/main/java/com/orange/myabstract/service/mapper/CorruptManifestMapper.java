package com.orange.myabstract.service.mapper;

import com.orange.myabstract.domain.*;
import com.orange.myabstract.service.dto.CorruptManifestDTO;

import org.mapstruct.*;
import java.util.List;

/**
 * Mapper for the entity CorruptManifest and its DTO CorruptManifestDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface CorruptManifestMapper {

    CorruptManifestDTO corruptManifestToCorruptManifestDTO(CorruptManifest corruptManifest);

    List<CorruptManifestDTO> corruptManifestsToCorruptManifestDTOs(List<CorruptManifest> corruptManifests);

    CorruptManifest corruptManifestDTOToCorruptManifest(CorruptManifestDTO corruptManifestDTO);

    List<CorruptManifest> corruptManifestDTOsToCorruptManifests(List<CorruptManifestDTO> corruptManifestDTOs);
}
