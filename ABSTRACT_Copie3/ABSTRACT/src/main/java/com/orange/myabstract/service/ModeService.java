package com.orange.myabstract.service;

import com.orange.myabstract.service.dto.ModeDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.LinkedList;
import java.util.List;

/**
 * Service Interface for managing Mode.
 */
public interface ModeService {

    /**
     * Save a mode.
     *
     * @param modeDTO the entity to save
     * @return the persisted entity
     */
    ModeDTO save(ModeDTO modeDTO);

    /**
     *  Get all the modes.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<ModeDTO> findAll(Pageable pageable);

    /**
     *  Get the "id" mode.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    ModeDTO findOne(Long id);

    /**
     *  Delete the "id" mode.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Search for the mode corresponding to the query.
     *
     *  @param query the query of the search
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<ModeDTO> search(String query, Pageable pageable);
}
