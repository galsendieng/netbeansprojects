package com.orange.myabstract.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

/**
 * A DisplayHeight.
 */
@Entity
@Table(name = "display_height")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "displayheight")
public class DisplayHeight implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "type_stream")
    private String typeStream;

    @Column(name = "streamer")
    private String streamer;

    @Column(name = "valeur")
    private String valeur;

    @Column(name = "date_sup")
    private ZonedDateTime dateSup;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTypeStream() {
        return typeStream;
    }

    public DisplayHeight typeStream(String typeStream) {
        this.typeStream = typeStream;
        return this;
    }

    public void setTypeStream(String typeStream) {
        this.typeStream = typeStream;
    }

    public String getStreamer() {
        return streamer;
    }

    public DisplayHeight streamer(String streamer) {
        this.streamer = streamer;
        return this;
    }

    public void setStreamer(String streamer) {
        this.streamer = streamer;
    }

    public String getValeur() {
        return valeur;
    }

    public DisplayHeight valeur(String valeur) {
        this.valeur = valeur;
        return this;
    }

    public void setValeur(String valeur) {
        this.valeur = valeur;
    }

    public ZonedDateTime getDateSup() {
        return dateSup;
    }

    public DisplayHeight dateSup(ZonedDateTime dateSup) {
        this.dateSup = dateSup;
        return this;
    }

    public void setDateSup(ZonedDateTime dateSup) {
        this.dateSup = dateSup;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        DisplayHeight displayHeight = (DisplayHeight) o;
        if(displayHeight.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, displayHeight.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "DisplayHeight{" +
            "id=" + id +
            ", typeStream='" + typeStream + "'" +
            ", streamer='" + streamer + "'" +
            ", valeur='" + valeur + "'" +
            ", dateSup='" + dateSup + "'" +
            '}';
    }
}
