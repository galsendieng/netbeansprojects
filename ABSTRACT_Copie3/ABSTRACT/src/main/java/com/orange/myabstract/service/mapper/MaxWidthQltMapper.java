package com.orange.myabstract.service.mapper;

import com.orange.myabstract.domain.*;
import com.orange.myabstract.service.dto.MaxWidthQltDTO;

import org.mapstruct.*;
import java.util.List;

/**
 * Mapper for the entity MaxWidthQlt and its DTO MaxWidthQltDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface MaxWidthQltMapper {

    MaxWidthQltDTO maxWidthQltToMaxWidthQltDTO(MaxWidthQlt maxWidthQlt);

    List<MaxWidthQltDTO> maxWidthQltsToMaxWidthQltDTOs(List<MaxWidthQlt> maxWidthQlts);

    MaxWidthQlt maxWidthQltDTOToMaxWidthQlt(MaxWidthQltDTO maxWidthQltDTO);

    List<MaxWidthQlt> maxWidthQltDTOsToMaxWidthQlts(List<MaxWidthQltDTO> maxWidthQltDTOs);
}
