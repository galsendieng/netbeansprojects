package com.orange.myabstract.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.orange.myabstract.service.ChunksService;
import com.orange.myabstract.web.rest.util.HeaderUtil;
import com.orange.myabstract.web.rest.util.PaginationUtil;
import com.orange.myabstract.service.dto.ChunksDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Chunks.
 */
@RestController
@RequestMapping("/api")
public class ChunksResource {

    private final Logger log = LoggerFactory.getLogger(ChunksResource.class);
        
    @Inject
    private ChunksService chunksService;

    /**
     * POST  /chunks : Create a new chunks.
     *
     * @param chunksDTO the chunksDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new chunksDTO, or with status 400 (Bad Request) if the chunks has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/chunks")
    @Timed
    public ResponseEntity<ChunksDTO> createChunks(@RequestBody ChunksDTO chunksDTO) throws URISyntaxException {
        log.debug("REST request to save Chunks : {}", chunksDTO);
        if (chunksDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("chunks", "idexists", "A new chunks cannot already have an ID")).body(null);
        }
        ChunksDTO result = chunksService.save(chunksDTO);
        return ResponseEntity.created(new URI("/api/chunks/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("chunks", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /chunks : Updates an existing chunks.
     *
     * @param chunksDTO the chunksDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated chunksDTO,
     * or with status 400 (Bad Request) if the chunksDTO is not valid,
     * or with status 500 (Internal Server Error) if the chunksDTO couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/chunks")
    @Timed
    public ResponseEntity<ChunksDTO> updateChunks(@RequestBody ChunksDTO chunksDTO) throws URISyntaxException {
        log.debug("REST request to update Chunks : {}", chunksDTO);
        if (chunksDTO.getId() == null) {
            return createChunks(chunksDTO);
        }
        ChunksDTO result = chunksService.save(chunksDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("chunks", chunksDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /chunks : get all the chunks.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of chunks in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @GetMapping("/chunks")
    @Timed
    public ResponseEntity<List<ChunksDTO>> getAllChunks(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of Chunks");
        Page<ChunksDTO> page = chunksService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/chunks");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /chunks/:id : get the "id" chunks.
     *
     * @param id the id of the chunksDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the chunksDTO, or with status 404 (Not Found)
     */
    @GetMapping("/chunks/{id}")
    @Timed
    public ResponseEntity<ChunksDTO> getChunks(@PathVariable Long id) {
        log.debug("REST request to get Chunks : {}", id);
        ChunksDTO chunksDTO = chunksService.findOne(id);
        return Optional.ofNullable(chunksDTO)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /chunks/:id : delete the "id" chunks.
     *
     * @param id the id of the chunksDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/chunks/{id}")
    @Timed
    public ResponseEntity<Void> deleteChunks(@PathVariable Long id) {
        log.debug("REST request to delete Chunks : {}", id);
        chunksService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("chunks", id.toString())).build();
    }

    /**
     * SEARCH  /_search/chunks?query=:query : search for the chunks corresponding
     * to the query.
     *
     * @param query the query of the chunks search 
     * @param pageable the pagination information
     * @return the result of the search
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @GetMapping("/_search/chunks")
    @Timed
    public ResponseEntity<List<ChunksDTO>> searchChunks(@RequestParam String query, Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to search for a page of Chunks for query {}", query);
        Page<ChunksDTO> page = chunksService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/chunks");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
