package com.orange.myabstract.service.impl;

import com.orange.myabstract.service.MaxHeightService;
import com.orange.myabstract.domain.MaxHeight;
import com.orange.myabstract.repository.MaxHeightRepository;
import com.orange.myabstract.repository.search.MaxHeightSearchRepository;
import com.orange.myabstract.service.dto.MaxHeightDTO;
import com.orange.myabstract.service.mapper.MaxHeightMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing MaxHeight.
 */
@Service
@Transactional
public class MaxHeightServiceImpl implements MaxHeightService{

    private final Logger log = LoggerFactory.getLogger(MaxHeightServiceImpl.class);
    
    @Inject
    private MaxHeightRepository maxHeightRepository;

    @Inject
    private MaxHeightMapper maxHeightMapper;

    @Inject
    private MaxHeightSearchRepository maxHeightSearchRepository;

    /**
     * Save a maxHeight.
     *
     * @param maxHeightDTO the entity to save
     * @return the persisted entity
     */
    public MaxHeightDTO save(MaxHeightDTO maxHeightDTO) {
        log.debug("Request to save MaxHeight : {}", maxHeightDTO);
        MaxHeight maxHeight = maxHeightMapper.maxHeightDTOToMaxHeight(maxHeightDTO);
        maxHeight = maxHeightRepository.save(maxHeight);
        MaxHeightDTO result = maxHeightMapper.maxHeightToMaxHeightDTO(maxHeight);
        maxHeightSearchRepository.save(maxHeight);
        return result;
    }

    /**
     *  Get all the maxHeights.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true) 
    public Page<MaxHeightDTO> findAll(Pageable pageable) {
        log.debug("Request to get all MaxHeights");
        Page<MaxHeight> result = maxHeightRepository.findAll(pageable);
        return result.map(maxHeight -> maxHeightMapper.maxHeightToMaxHeightDTO(maxHeight));
    }

    /**
     *  Get one maxHeight by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true) 
    public MaxHeightDTO findOne(Long id) {
        log.debug("Request to get MaxHeight : {}", id);
        MaxHeight maxHeight = maxHeightRepository.findOne(id);
        MaxHeightDTO maxHeightDTO = maxHeightMapper.maxHeightToMaxHeightDTO(maxHeight);
        return maxHeightDTO;
    }

    /**
     *  Delete the  maxHeight by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete MaxHeight : {}", id);
        maxHeightRepository.delete(id);
        maxHeightSearchRepository.delete(id);
    }

    /**
     * Search for the maxHeight corresponding to the query.
     *
     *  @param query the query of the search
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<MaxHeightDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of MaxHeights for query {}", query);
        Page<MaxHeight> result = maxHeightSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(maxHeight -> maxHeightMapper.maxHeightToMaxHeightDTO(maxHeight));
    }
}
