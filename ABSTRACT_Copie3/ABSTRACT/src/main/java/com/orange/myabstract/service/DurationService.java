package com.orange.myabstract.service;

import com.orange.myabstract.service.dto.DurationDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.LinkedList;
import java.util.List;

/**
 * Service Interface for managing Duration.
 */
public interface DurationService {

    /**
     * Save a duration.
     *
     * @param durationDTO the entity to save
     * @return the persisted entity
     */
    DurationDTO save(DurationDTO durationDTO);

    /**
     *  Get all the durations.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<DurationDTO> findAll(Pageable pageable);

    /**
     *  Get the "id" duration.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    DurationDTO findOne(Long id);

    /**
     *  Delete the "id" duration.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Search for the duration corresponding to the query.
     *
     *  @param query the query of the search
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<DurationDTO> search(String query, Pageable pageable);
}
