package com.orange.myabstract.service;

import com.orange.myabstract.service.dto.PacketSizeDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.LinkedList;
import java.util.List;

/**
 * Service Interface for managing PacketSize.
 */
public interface PacketSizeService {

    /**
     * Save a packetSize.
     *
     * @param packetSizeDTO the entity to save
     * @return the persisted entity
     */
    PacketSizeDTO save(PacketSizeDTO packetSizeDTO);

    /**
     *  Get all the packetSizes.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<PacketSizeDTO> findAll(Pageable pageable);

    /**
     *  Get the "id" packetSize.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    PacketSizeDTO findOne(Long id);

    /**
     *  Delete the "id" packetSize.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Search for the packetSize corresponding to the query.
     *
     *  @param query the query of the search
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<PacketSizeDTO> search(String query, Pageable pageable);
}
