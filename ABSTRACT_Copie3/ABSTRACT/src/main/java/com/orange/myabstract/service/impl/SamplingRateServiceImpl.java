package com.orange.myabstract.service.impl;

import com.orange.myabstract.service.SamplingRateService;
import com.orange.myabstract.domain.SamplingRate;
import com.orange.myabstract.repository.SamplingRateRepository;
import com.orange.myabstract.repository.search.SamplingRateSearchRepository;
import com.orange.myabstract.service.dto.SamplingRateDTO;
import com.orange.myabstract.service.mapper.SamplingRateMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing SamplingRate.
 */
@Service
@Transactional
public class SamplingRateServiceImpl implements SamplingRateService{

    private final Logger log = LoggerFactory.getLogger(SamplingRateServiceImpl.class);
    
    @Inject
    private SamplingRateRepository samplingRateRepository;

    @Inject
    private SamplingRateMapper samplingRateMapper;

    @Inject
    private SamplingRateSearchRepository samplingRateSearchRepository;

    /**
     * Save a samplingRate.
     *
     * @param samplingRateDTO the entity to save
     * @return the persisted entity
     */
    public SamplingRateDTO save(SamplingRateDTO samplingRateDTO) {
        log.debug("Request to save SamplingRate : {}", samplingRateDTO);
        SamplingRate samplingRate = samplingRateMapper.samplingRateDTOToSamplingRate(samplingRateDTO);
        samplingRate = samplingRateRepository.save(samplingRate);
        SamplingRateDTO result = samplingRateMapper.samplingRateToSamplingRateDTO(samplingRate);
        samplingRateSearchRepository.save(samplingRate);
        return result;
    }

    /**
     *  Get all the samplingRates.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true) 
    public Page<SamplingRateDTO> findAll(Pageable pageable) {
        log.debug("Request to get all SamplingRates");
        Page<SamplingRate> result = samplingRateRepository.findAll(pageable);
        return result.map(samplingRate -> samplingRateMapper.samplingRateToSamplingRateDTO(samplingRate));
    }

    /**
     *  Get one samplingRate by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true) 
    public SamplingRateDTO findOne(Long id) {
        log.debug("Request to get SamplingRate : {}", id);
        SamplingRate samplingRate = samplingRateRepository.findOne(id);
        SamplingRateDTO samplingRateDTO = samplingRateMapper.samplingRateToSamplingRateDTO(samplingRate);
        return samplingRateDTO;
    }

    /**
     *  Delete the  samplingRate by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete SamplingRate : {}", id);
        samplingRateRepository.delete(id);
        samplingRateSearchRepository.delete(id);
    }

    /**
     * Search for the samplingRate corresponding to the query.
     *
     *  @param query the query of the search
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<SamplingRateDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of SamplingRates for query {}", query);
        Page<SamplingRate> result = samplingRateSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(samplingRate -> samplingRateMapper.samplingRateToSamplingRateDTO(samplingRate));
    }
}
