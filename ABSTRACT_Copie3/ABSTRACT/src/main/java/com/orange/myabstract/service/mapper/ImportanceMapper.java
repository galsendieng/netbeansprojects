package com.orange.myabstract.service.mapper;

import com.orange.myabstract.domain.*;
import com.orange.myabstract.service.dto.ImportanceDTO;

import org.mapstruct.*;
import java.util.List;

/**
 * Mapper for the entity Importance and its DTO ImportanceDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface ImportanceMapper {

    ImportanceDTO importanceToImportanceDTO(Importance importance);

    List<ImportanceDTO> importancesToImportanceDTOs(List<Importance> importances);

    @Mapping(target = "manifests", ignore = true)
    Importance importanceDTOToImportance(ImportanceDTO importanceDTO);

    List<Importance> importanceDTOsToImportances(List<ImportanceDTO> importanceDTOs);
}
