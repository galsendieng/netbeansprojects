package com.orange.myabstract.service.mapper;

import com.orange.myabstract.domain.*;
import com.orange.myabstract.service.dto.FourCCDTO;

import org.mapstruct.*;
import java.util.List;

/**
 * Mapper for the entity FourCC and its DTO FourCCDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface FourCCMapper {

    FourCCDTO fourCCToFourCCDTO(FourCC fourCC);

    List<FourCCDTO> fourCCSToFourCCDTOs(List<FourCC> fourCCS);

    FourCC fourCCDTOToFourCC(FourCCDTO fourCCDTO);

    List<FourCC> fourCCDTOsToFourCCS(List<FourCCDTO> fourCCDTOs);
}
