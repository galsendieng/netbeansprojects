package com.orange.myabstract.service;

import com.orange.myabstract.service.dto.BitsPerSampleDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.LinkedList;
import java.util.List;

/**
 * Service Interface for managing BitsPerSample.
 */
public interface BitsPerSampleService {

    /**
     * Save a bitsPerSample.
     *
     * @param bitsPerSampleDTO the entity to save
     * @return the persisted entity
     */
    BitsPerSampleDTO save(BitsPerSampleDTO bitsPerSampleDTO);

    /**
     *  Get all the bitsPerSamples.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<BitsPerSampleDTO> findAll(Pageable pageable);

    /**
     *  Get the "id" bitsPerSample.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    BitsPerSampleDTO findOne(Long id);

    /**
     *  Delete the "id" bitsPerSample.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Search for the bitsPerSample corresponding to the query.
     *
     *  @param query the query of the search
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<BitsPerSampleDTO> search(String query, Pageable pageable);
}
