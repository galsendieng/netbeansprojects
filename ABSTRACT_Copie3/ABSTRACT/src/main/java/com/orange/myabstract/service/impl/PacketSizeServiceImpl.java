package com.orange.myabstract.service.impl;

import com.orange.myabstract.service.PacketSizeService;
import com.orange.myabstract.domain.PacketSize;
import com.orange.myabstract.repository.PacketSizeRepository;
import com.orange.myabstract.repository.search.PacketSizeSearchRepository;
import com.orange.myabstract.service.dto.PacketSizeDTO;
import com.orange.myabstract.service.mapper.PacketSizeMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing PacketSize.
 */
@Service
@Transactional
public class PacketSizeServiceImpl implements PacketSizeService{

    private final Logger log = LoggerFactory.getLogger(PacketSizeServiceImpl.class);
    
    @Inject
    private PacketSizeRepository packetSizeRepository;

    @Inject
    private PacketSizeMapper packetSizeMapper;

    @Inject
    private PacketSizeSearchRepository packetSizeSearchRepository;

    /**
     * Save a packetSize.
     *
     * @param packetSizeDTO the entity to save
     * @return the persisted entity
     */
    public PacketSizeDTO save(PacketSizeDTO packetSizeDTO) {
        log.debug("Request to save PacketSize : {}", packetSizeDTO);
        PacketSize packetSize = packetSizeMapper.packetSizeDTOToPacketSize(packetSizeDTO);
        packetSize = packetSizeRepository.save(packetSize);
        PacketSizeDTO result = packetSizeMapper.packetSizeToPacketSizeDTO(packetSize);
        packetSizeSearchRepository.save(packetSize);
        return result;
    }

    /**
     *  Get all the packetSizes.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true) 
    public Page<PacketSizeDTO> findAll(Pageable pageable) {
        log.debug("Request to get all PacketSizes");
        Page<PacketSize> result = packetSizeRepository.findAll(pageable);
        return result.map(packetSize -> packetSizeMapper.packetSizeToPacketSizeDTO(packetSize));
    }

    /**
     *  Get one packetSize by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true) 
    public PacketSizeDTO findOne(Long id) {
        log.debug("Request to get PacketSize : {}", id);
        PacketSize packetSize = packetSizeRepository.findOne(id);
        PacketSizeDTO packetSizeDTO = packetSizeMapper.packetSizeToPacketSizeDTO(packetSize);
        return packetSizeDTO;
    }

    /**
     *  Delete the  packetSize by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete PacketSize : {}", id);
        packetSizeRepository.delete(id);
        packetSizeSearchRepository.delete(id);
    }

    /**
     * Search for the packetSize corresponding to the query.
     *
     *  @param query the query of the search
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<PacketSizeDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of PacketSizes for query {}", query);
        Page<PacketSize> result = packetSizeSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(packetSize -> packetSizeMapper.packetSizeToPacketSizeDTO(packetSize));
    }
}
