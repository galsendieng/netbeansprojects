package com.orange.myabstract.service;

import com.orange.myabstract.service.dto.ImportanceDTO;

import java.util.LinkedList;
import java.util.List;

/**
 * Service Interface for managing Importance.
 */
public interface ImportanceService {

    /**
     * Save a importance.
     *
     * @param importanceDTO the entity to save
     * @return the persisted entity
     */
    ImportanceDTO save(ImportanceDTO importanceDTO);

    /**
     *  Get all the importances.
     *  
     *  @return the list of entities
     */
    List<ImportanceDTO> findAll();

    /**
     *  Get the "id" importance.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    ImportanceDTO findOne(Long id);

    /**
     *  Delete the "id" importance.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Search for the importance corresponding to the query.
     *
     *  @param query the query of the search
     *  
     *  @return the list of entities
     */
    List<ImportanceDTO> search(String query);
}
