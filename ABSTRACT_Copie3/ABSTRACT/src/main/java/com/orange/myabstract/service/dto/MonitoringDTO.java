package com.orange.myabstract.service.dto;

import java.time.ZonedDateTime;
import java.io.Serializable;
import java.util.Objects;


/**
 * A DTO for the Monitoring entity.
 */
public class MonitoringDTO implements Serializable {

    private Long id;

    private String attribut;

    private String severite;

    private String importance;

    private String valeurAttendue;

    private String valeurObtenue;

    private ZonedDateTime dateSup;

    private String message;

    private String infos;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public String getAttribut() {
        return attribut;
    }

    public void setAttribut(String attribut) {
        this.attribut = attribut;
    }
    public String getSeverite() {
        return severite;
    }

    public void setSeverite(String severite) {
        this.severite = severite;
    }
    public String getImportance() {
        return importance;
    }

    public void setImportance(String importance) {
        this.importance = importance;
    }
    public String getValeurAttendue() {
        return valeurAttendue;
    }

    public void setValeurAttendue(String valeurAttendue) {
        this.valeurAttendue = valeurAttendue;
    }
    public String getValeurObtenue() {
        return valeurObtenue;
    }

    public void setValeurObtenue(String valeurObtenue) {
        this.valeurObtenue = valeurObtenue;
    }
    public ZonedDateTime getDateSup() {
        return dateSup;
    }

    public void setDateSup(ZonedDateTime dateSup) {
        this.dateSup = dateSup;
    }
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
    public String getInfos() {
        return infos;
    }

    public void setInfos(String infos) {
        this.infos = infos;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        MonitoringDTO monitoringDTO = (MonitoringDTO) o;

        if ( ! Objects.equals(id, monitoringDTO.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "MonitoringDTO{" +
            "id=" + id +
            ", attribut='" + attribut + "'" +
            ", severite='" + severite + "'" +
            ", importance='" + importance + "'" +
            ", valeurAttendue='" + valeurAttendue + "'" +
            ", valeurObtenue='" + valeurObtenue + "'" +
            ", dateSup='" + dateSup + "'" +
            ", message='" + message + "'" +
            ", infos='" + infos + "'" +
            '}';
    }
}
