package com.orange.myabstract.repository.search;

import com.orange.myabstract.domain.Chunks;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the Chunks entity.
 */
public interface ChunksSearchRepository extends ElasticsearchRepository<Chunks, Long> {
}
