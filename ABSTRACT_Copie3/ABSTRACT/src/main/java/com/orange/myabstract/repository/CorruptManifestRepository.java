package com.orange.myabstract.repository;

import com.orange.myabstract.domain.CorruptManifest;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the CorruptManifest entity.
 */
@SuppressWarnings("unused")
public interface CorruptManifestRepository extends JpaRepository<CorruptManifest,Long> {

}
