package com.orange.myabstract.service;

import com.orange.myabstract.service.dto.MaxHeightQltDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.LinkedList;
import java.util.List;

/**
 * Service Interface for managing MaxHeightQlt.
 */
public interface MaxHeightQltService {

    /**
     * Save a maxHeightQlt.
     *
     * @param maxHeightQltDTO the entity to save
     * @return the persisted entity
     */
    MaxHeightQltDTO save(MaxHeightQltDTO maxHeightQltDTO);

    /**
     *  Get all the maxHeightQlts.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<MaxHeightQltDTO> findAll(Pageable pageable);

    /**
     *  Get the "id" maxHeightQlt.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    MaxHeightQltDTO findOne(Long id);

    /**
     *  Delete the "id" maxHeightQlt.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Search for the maxHeightQlt corresponding to the query.
     *
     *  @param query the query of the search
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<MaxHeightQltDTO> search(String query, Pageable pageable);
}
