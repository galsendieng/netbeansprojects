package com.orange.myabstract.service.dto;

import java.time.ZonedDateTime;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Lob;


/**
 * A DTO for the CorruptManifest entity.
 */
public class CorruptManifestDTO implements Serializable {

    private Long id;

    private ZonedDateTime dateManifest;

    private String command;

    private String profil;

    @Lob
    private byte[] file;

    private String fileContentType;
    private String errors;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public ZonedDateTime getDateManifest() {
        return dateManifest;
    }

    public void setDateManifest(ZonedDateTime dateManifest) {
        this.dateManifest = dateManifest;
    }
    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }
    public String getProfil() {
        return profil;
    }

    public void setProfil(String profil) {
        this.profil = profil;
    }
    public byte[] getFile() {
        return file;
    }

    public void setFile(byte[] file) {
        this.file = file;
    }

    public String getFileContentType() {
        return fileContentType;
    }

    public void setFileContentType(String fileContentType) {
        this.fileContentType = fileContentType;
    }
    public String getErrors() {
        return errors;
    }

    public void setErrors(String errors) {
        this.errors = errors;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        CorruptManifestDTO corruptManifestDTO = (CorruptManifestDTO) o;

        if ( ! Objects.equals(id, corruptManifestDTO.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "CorruptManifestDTO{" +
            "id=" + id +
            ", dateManifest='" + dateManifest + "'" +
            ", command='" + command + "'" +
            ", profil='" + profil + "'" +
            ", file='" + file + "'" +
            ", errors='" + errors + "'" +
            '}';
    }
}
