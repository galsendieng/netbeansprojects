package com.orange.myabstract.service.impl;

import com.orange.myabstract.service.ImportanceService;
import com.orange.myabstract.domain.Importance;
import com.orange.myabstract.repository.ImportanceRepository;
import com.orange.myabstract.repository.search.ImportanceSearchRepository;
import com.orange.myabstract.service.dto.ImportanceDTO;
import com.orange.myabstract.service.mapper.ImportanceMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing Importance.
 */
@Service
@Transactional
public class ImportanceServiceImpl implements ImportanceService{

    private final Logger log = LoggerFactory.getLogger(ImportanceServiceImpl.class);
    
    @Inject
    private ImportanceRepository importanceRepository;

    @Inject
    private ImportanceMapper importanceMapper;

    @Inject
    private ImportanceSearchRepository importanceSearchRepository;

    /**
     * Save a importance.
     *
     * @param importanceDTO the entity to save
     * @return the persisted entity
     */
    public ImportanceDTO save(ImportanceDTO importanceDTO) {
        log.debug("Request to save Importance : {}", importanceDTO);
        Importance importance = importanceMapper.importanceDTOToImportance(importanceDTO);
        importance = importanceRepository.save(importance);
        ImportanceDTO result = importanceMapper.importanceToImportanceDTO(importance);
        importanceSearchRepository.save(importance);
        return result;
    }

    /**
     *  Get all the importances.
     *  
     *  @return the list of entities
     */
    @Transactional(readOnly = true) 
    public List<ImportanceDTO> findAll() {
        log.debug("Request to get all Importances");
        List<ImportanceDTO> result = importanceRepository.findAll().stream()
            .map(importanceMapper::importanceToImportanceDTO)
            .collect(Collectors.toCollection(LinkedList::new));

        return result;
    }

    /**
     *  Get one importance by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true) 
    public ImportanceDTO findOne(Long id) {
        log.debug("Request to get Importance : {}", id);
        Importance importance = importanceRepository.findOne(id);
        ImportanceDTO importanceDTO = importanceMapper.importanceToImportanceDTO(importance);
        return importanceDTO;
    }

    /**
     *  Delete the  importance by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Importance : {}", id);
        importanceRepository.delete(id);
        importanceSearchRepository.delete(id);
    }

    /**
     * Search for the importance corresponding to the query.
     *
     *  @param query the query of the search
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<ImportanceDTO> search(String query) {
        log.debug("Request to search Importances for query {}", query);
        return StreamSupport
            .stream(importanceSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .map(importanceMapper::importanceToImportanceDTO)
            .collect(Collectors.toList());
    }
}
