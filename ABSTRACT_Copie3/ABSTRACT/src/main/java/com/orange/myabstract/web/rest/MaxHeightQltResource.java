package com.orange.myabstract.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.orange.myabstract.service.MaxHeightQltService;
import com.orange.myabstract.web.rest.util.HeaderUtil;
import com.orange.myabstract.web.rest.util.PaginationUtil;
import com.orange.myabstract.service.dto.MaxHeightQltDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing MaxHeightQlt.
 */
@RestController
@RequestMapping("/api")
public class MaxHeightQltResource {

    private final Logger log = LoggerFactory.getLogger(MaxHeightQltResource.class);
        
    @Inject
    private MaxHeightQltService maxHeightQltService;

    /**
     * POST  /max-height-qlts : Create a new maxHeightQlt.
     *
     * @param maxHeightQltDTO the maxHeightQltDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new maxHeightQltDTO, or with status 400 (Bad Request) if the maxHeightQlt has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/max-height-qlts")
    @Timed
    public ResponseEntity<MaxHeightQltDTO> createMaxHeightQlt(@RequestBody MaxHeightQltDTO maxHeightQltDTO) throws URISyntaxException {
        log.debug("REST request to save MaxHeightQlt : {}", maxHeightQltDTO);
        if (maxHeightQltDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("maxHeightQlt", "idexists", "A new maxHeightQlt cannot already have an ID")).body(null);
        }
        MaxHeightQltDTO result = maxHeightQltService.save(maxHeightQltDTO);
        return ResponseEntity.created(new URI("/api/max-height-qlts/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("maxHeightQlt", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /max-height-qlts : Updates an existing maxHeightQlt.
     *
     * @param maxHeightQltDTO the maxHeightQltDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated maxHeightQltDTO,
     * or with status 400 (Bad Request) if the maxHeightQltDTO is not valid,
     * or with status 500 (Internal Server Error) if the maxHeightQltDTO couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/max-height-qlts")
    @Timed
    public ResponseEntity<MaxHeightQltDTO> updateMaxHeightQlt(@RequestBody MaxHeightQltDTO maxHeightQltDTO) throws URISyntaxException {
        log.debug("REST request to update MaxHeightQlt : {}", maxHeightQltDTO);
        if (maxHeightQltDTO.getId() == null) {
            return createMaxHeightQlt(maxHeightQltDTO);
        }
        MaxHeightQltDTO result = maxHeightQltService.save(maxHeightQltDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("maxHeightQlt", maxHeightQltDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /max-height-qlts : get all the maxHeightQlts.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of maxHeightQlts in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @GetMapping("/max-height-qlts")
    @Timed
    public ResponseEntity<List<MaxHeightQltDTO>> getAllMaxHeightQlts(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of MaxHeightQlts");
        Page<MaxHeightQltDTO> page = maxHeightQltService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/max-height-qlts");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /max-height-qlts/:id : get the "id" maxHeightQlt.
     *
     * @param id the id of the maxHeightQltDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the maxHeightQltDTO, or with status 404 (Not Found)
     */
    @GetMapping("/max-height-qlts/{id}")
    @Timed
    public ResponseEntity<MaxHeightQltDTO> getMaxHeightQlt(@PathVariable Long id) {
        log.debug("REST request to get MaxHeightQlt : {}", id);
        MaxHeightQltDTO maxHeightQltDTO = maxHeightQltService.findOne(id);
        return Optional.ofNullable(maxHeightQltDTO)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /max-height-qlts/:id : delete the "id" maxHeightQlt.
     *
     * @param id the id of the maxHeightQltDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/max-height-qlts/{id}")
    @Timed
    public ResponseEntity<Void> deleteMaxHeightQlt(@PathVariable Long id) {
        log.debug("REST request to delete MaxHeightQlt : {}", id);
        maxHeightQltService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("maxHeightQlt", id.toString())).build();
    }

    /**
     * SEARCH  /_search/max-height-qlts?query=:query : search for the maxHeightQlt corresponding
     * to the query.
     *
     * @param query the query of the maxHeightQlt search 
     * @param pageable the pagination information
     * @return the result of the search
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @GetMapping("/_search/max-height-qlts")
    @Timed
    public ResponseEntity<List<MaxHeightQltDTO>> searchMaxHeightQlts(@RequestParam String query, Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to search for a page of MaxHeightQlts for query {}", query);
        Page<MaxHeightQltDTO> page = maxHeightQltService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/max-height-qlts");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
