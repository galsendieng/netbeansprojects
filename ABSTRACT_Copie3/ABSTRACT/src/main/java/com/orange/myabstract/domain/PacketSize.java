package com.orange.myabstract.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

/**
 * A PacketSize.
 */
@Entity
@Table(name = "packet_size")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "packetsize")
public class PacketSize implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "type_stream")
    private String typeStream;

    @Column(name = "valeur")
    private String valeur;

    @Column(name = "streamer")
    private String streamer;

    @Column(name = "date_sup")
    private ZonedDateTime dateSup;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTypeStream() {
        return typeStream;
    }

    public PacketSize typeStream(String typeStream) {
        this.typeStream = typeStream;
        return this;
    }

    public void setTypeStream(String typeStream) {
        this.typeStream = typeStream;
    }

    public String getValeur() {
        return valeur;
    }

    public PacketSize valeur(String valeur) {
        this.valeur = valeur;
        return this;
    }

    public void setValeur(String valeur) {
        this.valeur = valeur;
    }

    public String getStreamer() {
        return streamer;
    }

    public PacketSize streamer(String streamer) {
        this.streamer = streamer;
        return this;
    }

    public void setStreamer(String streamer) {
        this.streamer = streamer;
    }

    public ZonedDateTime getDateSup() {
        return dateSup;
    }

    public PacketSize dateSup(ZonedDateTime dateSup) {
        this.dateSup = dateSup;
        return this;
    }

    public void setDateSup(ZonedDateTime dateSup) {
        this.dateSup = dateSup;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        PacketSize packetSize = (PacketSize) o;
        if(packetSize.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, packetSize.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "PacketSize{" +
            "id=" + id +
            ", typeStream='" + typeStream + "'" +
            ", valeur='" + valeur + "'" +
            ", streamer='" + streamer + "'" +
            ", dateSup='" + dateSup + "'" +
            '}';
    }
}
