package com.orange.myabstract.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.orange.myabstract.service.BitrateService;
import com.orange.myabstract.web.rest.util.HeaderUtil;
import com.orange.myabstract.web.rest.util.PaginationUtil;
import com.orange.myabstract.service.dto.BitrateDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Bitrate.
 */
@RestController
@RequestMapping("/api")
public class BitrateResource {

    private final Logger log = LoggerFactory.getLogger(BitrateResource.class);

    @Inject
    private BitrateService bitrateService;

    /**
     * POST  /bitrates : Create a new bitrate.
     *
     * @param bitrateDTO the bitrateDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new bitrateDTO, or with status 400 (Bad Request) if the bitrate has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/bitrates")
    @Timed
    public ResponseEntity<BitrateDTO> createBitrate(@RequestBody BitrateDTO bitrateDTO) throws URISyntaxException {
        log.debug("REST request to save Bitrate : {}", bitrateDTO);
        if (bitrateDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("bitrate", "idexists", "A new bitrate cannot already have an ID")).body(null);
        }
        BitrateDTO result = bitrateService.save(bitrateDTO);
        return ResponseEntity.created(new URI("/api/bitrates/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("bitrate", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /bitrates : Updates an existing bitrate.
     *
     * @param bitrateDTO the bitrateDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated bitrateDTO,
     * or with status 400 (Bad Request) if the bitrateDTO is not valid,
     * or with status 500 (Internal Server Error) if the bitrateDTO couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/bitrates")
    @Timed
    public ResponseEntity<BitrateDTO> updateBitrate(@RequestBody BitrateDTO bitrateDTO) throws URISyntaxException {
        log.debug("REST request to update Bitrate : {}", bitrateDTO);
        if (bitrateDTO.getId() == null) {
            return createBitrate(bitrateDTO);
        }
        BitrateDTO result = bitrateService.save(bitrateDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("bitrate", bitrateDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /bitrates : get all the bitrates.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of bitrates in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @GetMapping("/bitrates")
    @Timed
    public ResponseEntity<List<BitrateDTO>> getAllBitrates(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of Bitrates");
        Page<BitrateDTO> page = bitrateService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/bitrates");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /bitrates/:id : get the "id" bitrate.
     *
     * @param id the id of the bitrateDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the bitrateDTO, or with status 404 (Not Found)
     */
    @GetMapping("/bitrates/{id}")
    @Timed
    public ResponseEntity<BitrateDTO> getBitrate(@PathVariable Long id) {
        log.debug("REST request to get Bitrate : {}", id);
        BitrateDTO bitrateDTO = bitrateService.findOne(id);
        return Optional.ofNullable(bitrateDTO)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /bitrates/:id : delete the "id" bitrate.
     *
     * @param id the id of the bitrateDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/bitrates/{id}")
    @Timed
    public ResponseEntity<Void> deleteBitrate(@PathVariable Long id) {
        log.debug("REST request to delete Bitrate : {}", id);
        bitrateService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("bitrate", id.toString())).build();
    }

    /**
     * SEARCH  /_search/bitrates?query=:query : search for the bitrate corresponding
     * to the query.
     *
     * @param query the query of the bitrate search
     * @param pageable the pagination information
     * @return the result of the search
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @GetMapping("/_search/bitrates")
    @Timed
    public ResponseEntity<List<BitrateDTO>> searchBitrates(@RequestParam String query, Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to search for a page of Bitrates for query {}", query);
        Page<BitrateDTO> page = bitrateService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/bitrates");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
