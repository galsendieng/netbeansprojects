package com.orange.myabstract.repository.search;

import com.orange.myabstract.domain.Duration;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the Duration entity.
 */
public interface DurationSearchRepository extends ElasticsearchRepository<Duration, Long> {
}
