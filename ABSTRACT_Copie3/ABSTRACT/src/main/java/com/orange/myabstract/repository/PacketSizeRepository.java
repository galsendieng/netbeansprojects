package com.orange.myabstract.repository;

import com.orange.myabstract.domain.PacketSize;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the PacketSize entity.
 */
@SuppressWarnings("unused")
public interface PacketSizeRepository extends JpaRepository<PacketSize,Long> {

}
