package com.orange.myabstract.service;

import com.orange.myabstract.service.dto.DVRWindowLengthDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.LinkedList;
import java.util.List;

/**
 * Service Interface for managing DVRWindowLength.
 */
public interface DVRWindowLengthService {

    /**
     * Save a dVRWindowLength.
     *
     * @param dVRWindowLengthDTO the entity to save
     * @return the persisted entity
     */
    DVRWindowLengthDTO save(DVRWindowLengthDTO dVRWindowLengthDTO);

    /**
     *  Get all the dVRWindowLengths.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<DVRWindowLengthDTO> findAll(Pageable pageable);

    /**
     *  Get the "id" dVRWindowLength.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    DVRWindowLengthDTO findOne(Long id);

    /**
     *  Delete the "id" dVRWindowLength.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Search for the dVRWindowLength corresponding to the query.
     *
     *  @param query the query of the search
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<DVRWindowLengthDTO> search(String query, Pageable pageable);
}
