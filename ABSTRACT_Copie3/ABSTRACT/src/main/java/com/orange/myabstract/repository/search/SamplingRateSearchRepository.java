package com.orange.myabstract.repository.search;

import com.orange.myabstract.domain.SamplingRate;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the SamplingRate entity.
 */
public interface SamplingRateSearchRepository extends ElasticsearchRepository<SamplingRate, Long> {
}
