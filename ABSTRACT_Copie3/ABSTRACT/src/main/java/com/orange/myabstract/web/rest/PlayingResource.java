package com.orange.myabstract.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.orange.myabstract.service.PlayingService;
import com.orange.myabstract.web.rest.util.HeaderUtil;
import com.orange.myabstract.service.dto.PlayingDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Playing.
 */
@RestController
@RequestMapping("/api")
public class PlayingResource {

    private final Logger log = LoggerFactory.getLogger(PlayingResource.class);
        
    @Inject
    private PlayingService playingService;

    /**
     * POST  /playings : Create a new playing.
     *
     * @param playingDTO the playingDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new playingDTO, or with status 400 (Bad Request) if the playing has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/playings")
    @Timed
    public ResponseEntity<PlayingDTO> createPlaying(@Valid @RequestBody PlayingDTO playingDTO) throws URISyntaxException {
        log.debug("REST request to save Playing : {}", playingDTO);
        if (playingDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("playing", "idexists", "A new playing cannot already have an ID")).body(null);
        }
        PlayingDTO result = playingService.save(playingDTO);
        return ResponseEntity.created(new URI("/api/playings/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("playing", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /playings : Updates an existing playing.
     *
     * @param playingDTO the playingDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated playingDTO,
     * or with status 400 (Bad Request) if the playingDTO is not valid,
     * or with status 500 (Internal Server Error) if the playingDTO couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/playings")
    @Timed
    public ResponseEntity<PlayingDTO> updatePlaying(@Valid @RequestBody PlayingDTO playingDTO) throws URISyntaxException {
        log.debug("REST request to update Playing : {}", playingDTO);
        if (playingDTO.getId() == null) {
            return createPlaying(playingDTO);
        }
        PlayingDTO result = playingService.save(playingDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("playing", playingDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /playings : get all the playings.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of playings in body
     */
    @GetMapping("/playings")
    @Timed
    public List<PlayingDTO> getAllPlayings() {
        log.debug("REST request to get all Playings");
        return playingService.findAll();
    }

    /**
     * GET  /playings/:id : get the "id" playing.
     *
     * @param id the id of the playingDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the playingDTO, or with status 404 (Not Found)
     */
    @GetMapping("/playings/{id}")
    @Timed
    public ResponseEntity<PlayingDTO> getPlaying(@PathVariable Long id) {
        log.debug("REST request to get Playing : {}", id);
        PlayingDTO playingDTO = playingService.findOne(id);
        return Optional.ofNullable(playingDTO)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /playings/:id : delete the "id" playing.
     *
     * @param id the id of the playingDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/playings/{id}")
    @Timed
    public ResponseEntity<Void> deletePlaying(@PathVariable Long id) {
        log.debug("REST request to delete Playing : {}", id);
        playingService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("playing", id.toString())).build();
    }

    /**
     * SEARCH  /_search/playings?query=:query : search for the playing corresponding
     * to the query.
     *
     * @param query the query of the playing search 
     * @return the result of the search
     */
    @GetMapping("/_search/playings")
    @Timed
    public List<PlayingDTO> searchPlayings(@RequestParam String query) {
        log.debug("REST request to search Playings for query {}", query);
        return playingService.search(query);
    }


}
