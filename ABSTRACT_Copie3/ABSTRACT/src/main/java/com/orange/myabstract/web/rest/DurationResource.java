package com.orange.myabstract.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.orange.myabstract.service.DurationService;
import com.orange.myabstract.web.rest.util.HeaderUtil;
import com.orange.myabstract.web.rest.util.PaginationUtil;
import com.orange.myabstract.service.dto.DurationDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Duration.
 */
@RestController
@RequestMapping("/api")
public class DurationResource {

    private final Logger log = LoggerFactory.getLogger(DurationResource.class);
        
    @Inject
    private DurationService durationService;

    /**
     * POST  /durations : Create a new duration.
     *
     * @param durationDTO the durationDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new durationDTO, or with status 400 (Bad Request) if the duration has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/durations")
    @Timed
    public ResponseEntity<DurationDTO> createDuration(@RequestBody DurationDTO durationDTO) throws URISyntaxException {
        log.debug("REST request to save Duration : {}", durationDTO);
        if (durationDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("duration", "idexists", "A new duration cannot already have an ID")).body(null);
        }
        DurationDTO result = durationService.save(durationDTO);
        return ResponseEntity.created(new URI("/api/durations/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("duration", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /durations : Updates an existing duration.
     *
     * @param durationDTO the durationDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated durationDTO,
     * or with status 400 (Bad Request) if the durationDTO is not valid,
     * or with status 500 (Internal Server Error) if the durationDTO couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/durations")
    @Timed
    public ResponseEntity<DurationDTO> updateDuration(@RequestBody DurationDTO durationDTO) throws URISyntaxException {
        log.debug("REST request to update Duration : {}", durationDTO);
        if (durationDTO.getId() == null) {
            return createDuration(durationDTO);
        }
        DurationDTO result = durationService.save(durationDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("duration", durationDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /durations : get all the durations.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of durations in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @GetMapping("/durations")
    @Timed
    public ResponseEntity<List<DurationDTO>> getAllDurations(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of Durations");
        Page<DurationDTO> page = durationService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/durations");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /durations/:id : get the "id" duration.
     *
     * @param id the id of the durationDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the durationDTO, or with status 404 (Not Found)
     */
    @GetMapping("/durations/{id}")
    @Timed
    public ResponseEntity<DurationDTO> getDuration(@PathVariable Long id) {
        log.debug("REST request to get Duration : {}", id);
        DurationDTO durationDTO = durationService.findOne(id);
        return Optional.ofNullable(durationDTO)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /durations/:id : delete the "id" duration.
     *
     * @param id the id of the durationDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/durations/{id}")
    @Timed
    public ResponseEntity<Void> deleteDuration(@PathVariable Long id) {
        log.debug("REST request to delete Duration : {}", id);
        durationService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("duration", id.toString())).build();
    }

    /**
     * SEARCH  /_search/durations?query=:query : search for the duration corresponding
     * to the query.
     *
     * @param query the query of the duration search 
     * @param pageable the pagination information
     * @return the result of the search
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @GetMapping("/_search/durations")
    @Timed
    public ResponseEntity<List<DurationDTO>> searchDurations(@RequestParam String query, Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to search for a page of Durations for query {}", query);
        Page<DurationDTO> page = durationService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/durations");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
