package com.orange.myabstract.repository;

import com.orange.myabstract.domain.Importance;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the Importance entity.
 */
@SuppressWarnings("unused")
public interface ImportanceRepository extends JpaRepository<Importance,Long> {

}
