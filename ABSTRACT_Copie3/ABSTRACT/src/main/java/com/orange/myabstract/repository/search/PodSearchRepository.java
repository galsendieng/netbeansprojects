package com.orange.myabstract.repository.search;

import com.orange.myabstract.domain.Pod;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the Pod entity.
 */
public interface PodSearchRepository extends ElasticsearchRepository<Pod, Long> {
}
