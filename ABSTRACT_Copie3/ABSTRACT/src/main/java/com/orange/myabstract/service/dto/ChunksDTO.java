package com.orange.myabstract.service.dto;

import java.time.ZonedDateTime;
import java.io.Serializable;
import java.util.Objects;


/**
 * A DTO for the Chunks entity.
 */
public class ChunksDTO implements Serializable {

    private Long id;

    private String typeStream;

    private String streamer;

    private String valeur;

    private ZonedDateTime dateSup;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public String getTypeStream() {
        return typeStream;
    }

    public void setTypeStream(String typeStream) {
        this.typeStream = typeStream;
    }
    public String getStreamer() {
        return streamer;
    }

    public void setStreamer(String streamer) {
        this.streamer = streamer;
    }
    public String getValeur() {
        return valeur;
    }

    public void setValeur(String valeur) {
        this.valeur = valeur;
    }
    public ZonedDateTime getDateSup() {
        return dateSup;
    }

    public void setDateSup(ZonedDateTime dateSup) {
        this.dateSup = dateSup;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ChunksDTO chunksDTO = (ChunksDTO) o;

        if ( ! Objects.equals(id, chunksDTO.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "ChunksDTO{" +
            "id=" + id +
            ", typeStream='" + typeStream + "'" +
            ", streamer='" + streamer + "'" +
            ", valeur='" + valeur + "'" +
            ", dateSup='" + dateSup + "'" +
            '}';
    }
}
