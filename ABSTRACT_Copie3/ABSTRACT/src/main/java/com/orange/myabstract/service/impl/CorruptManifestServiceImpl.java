package com.orange.myabstract.service.impl;

import com.orange.myabstract.service.CorruptManifestService;
import com.orange.myabstract.domain.CorruptManifest;
import com.orange.myabstract.repository.CorruptManifestRepository;
import com.orange.myabstract.repository.search.CorruptManifestSearchRepository;
import com.orange.myabstract.service.dto.CorruptManifestDTO;
import com.orange.myabstract.service.mapper.CorruptManifestMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing CorruptManifest.
 */
@Service
@Transactional
public class CorruptManifestServiceImpl implements CorruptManifestService{

    private final Logger log = LoggerFactory.getLogger(CorruptManifestServiceImpl.class);
    
    @Inject
    private CorruptManifestRepository corruptManifestRepository;

    @Inject
    private CorruptManifestMapper corruptManifestMapper;

    @Inject
    private CorruptManifestSearchRepository corruptManifestSearchRepository;

    /**
     * Save a corruptManifest.
     *
     * @param corruptManifestDTO the entity to save
     * @return the persisted entity
     */
    public CorruptManifestDTO save(CorruptManifestDTO corruptManifestDTO) {
        log.debug("Request to save CorruptManifest : {}", corruptManifestDTO);
        CorruptManifest corruptManifest = corruptManifestMapper.corruptManifestDTOToCorruptManifest(corruptManifestDTO);
        corruptManifest = corruptManifestRepository.save(corruptManifest);
        CorruptManifestDTO result = corruptManifestMapper.corruptManifestToCorruptManifestDTO(corruptManifest);
        corruptManifestSearchRepository.save(corruptManifest);
        return result;
    }

    /**
     *  Get all the corruptManifests.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true) 
    public Page<CorruptManifestDTO> findAll(Pageable pageable) {
        log.debug("Request to get all CorruptManifests");
        Page<CorruptManifest> result = corruptManifestRepository.findAll(pageable);
        return result.map(corruptManifest -> corruptManifestMapper.corruptManifestToCorruptManifestDTO(corruptManifest));
    }

    /**
     *  Get one corruptManifest by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true) 
    public CorruptManifestDTO findOne(Long id) {
        log.debug("Request to get CorruptManifest : {}", id);
        CorruptManifest corruptManifest = corruptManifestRepository.findOne(id);
        CorruptManifestDTO corruptManifestDTO = corruptManifestMapper.corruptManifestToCorruptManifestDTO(corruptManifest);
        return corruptManifestDTO;
    }

    /**
     *  Delete the  corruptManifest by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete CorruptManifest : {}", id);
        corruptManifestRepository.delete(id);
        corruptManifestSearchRepository.delete(id);
    }

    /**
     * Search for the corruptManifest corresponding to the query.
     *
     *  @param query the query of the search
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<CorruptManifestDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of CorruptManifests for query {}", query);
        Page<CorruptManifest> result = corruptManifestSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(corruptManifest -> corruptManifestMapper.corruptManifestToCorruptManifestDTO(corruptManifest));
    }
}
