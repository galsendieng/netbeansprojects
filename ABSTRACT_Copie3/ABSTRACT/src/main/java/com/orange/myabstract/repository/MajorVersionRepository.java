package com.orange.myabstract.repository;

import com.orange.myabstract.domain.MajorVersion;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the MajorVersion entity.
 */
@SuppressWarnings("unused")
public interface MajorVersionRepository extends JpaRepository<MajorVersion,Long> {

}
