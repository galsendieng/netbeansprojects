package com.orange.myabstract.service.mapper;

import com.orange.myabstract.domain.*;
import com.orange.myabstract.service.dto.BitrateDTO;

import org.mapstruct.*;
import java.util.List;

/**
 * Mapper for the entity Bitrate and its DTO BitrateDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface BitrateMapper {

    BitrateDTO bitrateToBitrateDTO(Bitrate bitrate);

    List<BitrateDTO> bitratesToBitrateDTOs(List<Bitrate> bitrates);

    Bitrate bitrateDTOToBitrate(BitrateDTO bitrateDTO);

    List<Bitrate> bitrateDTOsToBitrates(List<BitrateDTO> bitrateDTOs);
}
