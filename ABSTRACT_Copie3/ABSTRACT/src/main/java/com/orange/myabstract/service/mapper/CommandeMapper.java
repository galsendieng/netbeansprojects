package com.orange.myabstract.service.mapper;

import com.orange.myabstract.domain.*;
import com.orange.myabstract.service.dto.CommandeDTO;

import org.mapstruct.*;
import java.util.List;

/**
 * Mapper for the entity Commande and its DTO CommandeDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface CommandeMapper {

    CommandeDTO commandeToCommandeDTO(Commande commande);

    List<CommandeDTO> commandesToCommandeDTOs(List<Commande> commandes);

    @Mapping(target = "cmdPlays", ignore = true)
    Commande commandeDTOToCommande(CommandeDTO commandeDTO);

    List<Commande> commandeDTOsToCommandes(List<CommandeDTO> commandeDTOs);
}
