package com.orange.myabstract.service;

import com.orange.myabstract.service.dto.CommandeDTO;

import java.util.LinkedList;
import java.util.List;

/**
 * Service Interface for managing Commande.
 */
public interface CommandeService {

    /**
     * Save a commande.
     *
     * @param commandeDTO the entity to save
     * @return the persisted entity
     */
    CommandeDTO save(CommandeDTO commandeDTO);

    /**
     *  Get all the commandes.
     *  
     *  @return the list of entities
     */
    List<CommandeDTO> findAll();

    /**
     *  Get the "id" commande.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    CommandeDTO findOne(Long id);

    /**
     *  Delete the "id" commande.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Search for the commande corresponding to the query.
     *
     *  @param query the query of the search
     *  
     *  @return the list of entities
     */
    List<CommandeDTO> search(String query);
}
