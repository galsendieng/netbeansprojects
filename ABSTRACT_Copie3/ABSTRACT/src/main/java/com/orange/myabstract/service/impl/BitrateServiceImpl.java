package com.orange.myabstract.service.impl;

import com.orange.myabstract.service.BitrateService;
import com.orange.myabstract.domain.Bitrate;
import com.orange.myabstract.repository.BitrateRepository;
import com.orange.myabstract.repository.search.BitrateSearchRepository;
import com.orange.myabstract.service.dto.BitrateDTO;
import com.orange.myabstract.service.mapper.BitrateMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing Bitrate.
 */
@Service
@Transactional
public class BitrateServiceImpl implements BitrateService{

    private final Logger log = LoggerFactory.getLogger(BitrateServiceImpl.class);

    @Inject
    private BitrateRepository bitrateRepository;

    @Inject
    private BitrateMapper bitrateMapper;

    @Inject
    private BitrateSearchRepository bitrateSearchRepository;

    /**
     * Save a bitrate.
     *
     * @param bitrateDTO the entity to save
     * @return the persisted entity
     */
    public BitrateDTO save(BitrateDTO bitrateDTO) {
        log.debug("Request to save Bitrate : {}", bitrateDTO);
        Bitrate bitrate = bitrateMapper.bitrateDTOToBitrate(bitrateDTO);
        bitrate = bitrateRepository.save(bitrate);
        BitrateDTO result = bitrateMapper.bitrateToBitrateDTO(bitrate);
        bitrateSearchRepository.save(bitrate);
        return result;
    }

    /**
     *  Get all the bitrates.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<BitrateDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Bitrates");
        Page<Bitrate> result = bitrateRepository.findAll(pageable);
        return result.map(bitrate -> bitrateMapper.bitrateToBitrateDTO(bitrate));
    }

    /**
     *  Get one bitrate by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public BitrateDTO findOne(Long id) {
        log.debug("Request to get Bitrate : {}", id);
        Bitrate bitrate = bitrateRepository.findOne(id);
        BitrateDTO bitrateDTO = bitrateMapper.bitrateToBitrateDTO(bitrate);
        return bitrateDTO;
    }
    /**
     *  Delete the  bitrate by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Bitrate : {}", id);
        bitrateRepository.delete(id);
        bitrateSearchRepository.delete(id);
    }

    /**
     * Search for the bitrate corresponding to the query.
     *
     *  @param query the query of the search
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<BitrateDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Bitrates for query {}", query);
        Page<Bitrate> result = bitrateSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(bitrate -> bitrateMapper.bitrateToBitrateDTO(bitrate));
    }
}
