package com.orange.myabstract.repository.search;

import com.orange.myabstract.domain.MajorVersion;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the MajorVersion entity.
 */
public interface MajorVersionSearchRepository extends ElasticsearchRepository<MajorVersion, Long> {
}
