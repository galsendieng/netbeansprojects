package com.orange.myabstract.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * For playing                                                                 
 * 
 */
@ApiModel(description = "For playing")
@Entity
@Table(name = "mode")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "mode")
public class Mode implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Column(name = "name", nullable = false)
    private String name;

    @NotNull
    @Column(name = "abr_static", nullable = false)
    private String abrStatic;

    @NotNull
    @Column(name = "fragment", nullable = false)
    private Integer fragment;

    @NotNull
    @Column(name = "suffix_manifest", nullable = false)
    private String suffixManifest;

    @NotNull
    @Column(name = "device_profil", nullable = false)
    private String deviceProfil;

    @Column(name = "client_version")
    private String clientVersion;

    @Column(name = "type_manifest")
    private String typeManifest;

    @Column(name = "description")
    private String description;

    @OneToMany(mappedBy = "theMode")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Playing> playings = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Mode name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAbrStatic() {
        return abrStatic;
    }

    public Mode abrStatic(String abrStatic) {
        this.abrStatic = abrStatic;
        return this;
    }

    public void setAbrStatic(String abrStatic) {
        this.abrStatic = abrStatic;
    }

    public Integer getFragment() {
        return fragment;
    }

    public Mode fragment(Integer fragment) {
        this.fragment = fragment;
        return this;
    }

    public void setFragment(Integer fragment) {
        this.fragment = fragment;
    }

    public String getSuffixManifest() {
        return suffixManifest;
    }

    public Mode suffixManifest(String suffixManifest) {
        this.suffixManifest = suffixManifest;
        return this;
    }

    public void setSuffixManifest(String suffixManifest) {
        this.suffixManifest = suffixManifest;
    }

    public String getDeviceProfil() {
        return deviceProfil;
    }

    public Mode deviceProfil(String deviceProfil) {
        this.deviceProfil = deviceProfil;
        return this;
    }

    public void setDeviceProfil(String deviceProfil) {
        this.deviceProfil = deviceProfil;
    }

    public String getClientVersion() {
        return clientVersion;
    }

    public Mode clientVersion(String clientVersion) {
        this.clientVersion = clientVersion;
        return this;
    }

    public void setClientVersion(String clientVersion) {
        this.clientVersion = clientVersion;
    }

    public String getTypeManifest() {
        return typeManifest;
    }

    public Mode typeManifest(String typeManifest) {
        this.typeManifest = typeManifest;
        return this;
    }

    public void setTypeManifest(String typeManifest) {
        this.typeManifest = typeManifest;
    }

    public String getDescription() {
        return description;
    }

    public Mode description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<Playing> getPlayings() {
        return playings;
    }

    public Mode playings(Set<Playing> playings) {
        this.playings = playings;
        return this;
    }

    public Mode addPlaying(Playing playing) {
        playings.add(playing);
        playing.setTheMode(this);
        return this;
    }

    public Mode removePlaying(Playing playing) {
        playings.remove(playing);
        playing.setTheMode(null);
        return this;
    }

    public void setPlayings(Set<Playing> playings) {
        this.playings = playings;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Mode mode = (Mode) o;
        if(mode.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, mode.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Mode{" +
            "id=" + id +
            ", name='" + name + "'" +
            ", abrStatic='" + abrStatic + "'" +
            ", fragment='" + fragment + "'" +
            ", suffixManifest='" + suffixManifest + "'" +
            ", deviceProfil='" + deviceProfil + "'" +
            ", clientVersion='" + clientVersion + "'" +
            ", typeManifest='" + typeManifest + "'" +
            ", description='" + description + "'" +
            '}';
    }
}
