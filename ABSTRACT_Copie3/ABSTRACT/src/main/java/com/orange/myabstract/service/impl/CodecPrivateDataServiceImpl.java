package com.orange.myabstract.service.impl;

import com.orange.myabstract.service.CodecPrivateDataService;
import com.orange.myabstract.domain.CodecPrivateData;
import com.orange.myabstract.repository.CodecPrivateDataRepository;
import com.orange.myabstract.repository.search.CodecPrivateDataSearchRepository;
import com.orange.myabstract.service.dto.CodecPrivateDataDTO;
import com.orange.myabstract.service.mapper.CodecPrivateDataMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing CodecPrivateData.
 */
@Service
@Transactional
public class CodecPrivateDataServiceImpl implements CodecPrivateDataService{

    private final Logger log = LoggerFactory.getLogger(CodecPrivateDataServiceImpl.class);
    
    @Inject
    private CodecPrivateDataRepository codecPrivateDataRepository;

    @Inject
    private CodecPrivateDataMapper codecPrivateDataMapper;

    @Inject
    private CodecPrivateDataSearchRepository codecPrivateDataSearchRepository;

    /**
     * Save a codecPrivateData.
     *
     * @param codecPrivateDataDTO the entity to save
     * @return the persisted entity
     */
    public CodecPrivateDataDTO save(CodecPrivateDataDTO codecPrivateDataDTO) {
        log.debug("Request to save CodecPrivateData : {}", codecPrivateDataDTO);
        CodecPrivateData codecPrivateData = codecPrivateDataMapper.codecPrivateDataDTOToCodecPrivateData(codecPrivateDataDTO);
        codecPrivateData = codecPrivateDataRepository.save(codecPrivateData);
        CodecPrivateDataDTO result = codecPrivateDataMapper.codecPrivateDataToCodecPrivateDataDTO(codecPrivateData);
        codecPrivateDataSearchRepository.save(codecPrivateData);
        return result;
    }

    /**
     *  Get all the codecPrivateData.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true) 
    public Page<CodecPrivateDataDTO> findAll(Pageable pageable) {
        log.debug("Request to get all CodecPrivateData");
        Page<CodecPrivateData> result = codecPrivateDataRepository.findAll(pageable);
        return result.map(codecPrivateData -> codecPrivateDataMapper.codecPrivateDataToCodecPrivateDataDTO(codecPrivateData));
    }

    /**
     *  Get one codecPrivateData by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true) 
    public CodecPrivateDataDTO findOne(Long id) {
        log.debug("Request to get CodecPrivateData : {}", id);
        CodecPrivateData codecPrivateData = codecPrivateDataRepository.findOne(id);
        CodecPrivateDataDTO codecPrivateDataDTO = codecPrivateDataMapper.codecPrivateDataToCodecPrivateDataDTO(codecPrivateData);
        return codecPrivateDataDTO;
    }

    /**
     *  Delete the  codecPrivateData by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete CodecPrivateData : {}", id);
        codecPrivateDataRepository.delete(id);
        codecPrivateDataSearchRepository.delete(id);
    }

    /**
     * Search for the codecPrivateData corresponding to the query.
     *
     *  @param query the query of the search
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<CodecPrivateDataDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of CodecPrivateData for query {}", query);
        Page<CodecPrivateData> result = codecPrivateDataSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(codecPrivateData -> codecPrivateDataMapper.codecPrivateDataToCodecPrivateDataDTO(codecPrivateData));
    }
}
