package com.orange.myabstract.service.mapper;

import com.orange.myabstract.domain.*;
import com.orange.myabstract.service.dto.QualityLevelsDTO;

import org.mapstruct.*;
import java.util.List;

/**
 * Mapper for the entity QualityLevels and its DTO QualityLevelsDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface QualityLevelsMapper {

    QualityLevelsDTO qualityLevelsToQualityLevelsDTO(QualityLevels qualityLevels);

    List<QualityLevelsDTO> qualityLevelsToQualityLevelsDTOs(List<QualityLevels> qualityLevels);

    QualityLevels qualityLevelsDTOToQualityLevels(QualityLevelsDTO qualityLevelsDTO);

    List<QualityLevels> qualityLevelsDTOsToQualityLevels(List<QualityLevelsDTO> qualityLevelsDTOs);
}
