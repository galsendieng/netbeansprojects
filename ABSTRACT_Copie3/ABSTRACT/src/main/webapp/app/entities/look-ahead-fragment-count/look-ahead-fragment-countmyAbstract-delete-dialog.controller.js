(function() {
    'use strict';

    angular
        .module('abstractApp')
        .controller('LookAheadFragmentCountMyAbstractDeleteController',LookAheadFragmentCountMyAbstractDeleteController);

    LookAheadFragmentCountMyAbstractDeleteController.$inject = ['$uibModalInstance', 'entity', 'LookAheadFragmentCount'];

    function LookAheadFragmentCountMyAbstractDeleteController($uibModalInstance, entity, LookAheadFragmentCount) {
        var vm = this;

        vm.lookAheadFragmentCount = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            LookAheadFragmentCount.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
