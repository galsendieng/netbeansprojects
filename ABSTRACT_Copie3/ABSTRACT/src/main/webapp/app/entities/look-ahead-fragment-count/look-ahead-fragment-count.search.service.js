(function() {
    'use strict';

    angular
        .module('abstractApp')
        .factory('LookAheadFragmentCountSearch', LookAheadFragmentCountSearch);

    LookAheadFragmentCountSearch.$inject = ['$resource'];

    function LookAheadFragmentCountSearch($resource) {
        var resourceUrl =  'api/_search/look-ahead-fragment-counts/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
