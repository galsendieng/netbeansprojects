(function() {
    'use strict';

    angular
        .module('abstractApp')
        .controller('SamplingRateMyAbstractDeleteController',SamplingRateMyAbstractDeleteController);

    SamplingRateMyAbstractDeleteController.$inject = ['$uibModalInstance', 'entity', 'SamplingRate'];

    function SamplingRateMyAbstractDeleteController($uibModalInstance, entity, SamplingRate) {
        var vm = this;

        vm.samplingRate = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            SamplingRate.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
