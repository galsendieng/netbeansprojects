(function() {
    'use strict';

    angular
        .module('abstractApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('sampling-ratemyAbstract', {
            parent: 'entity',
            url: '/sampling-ratemyAbstract?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'abstractApp.samplingRate.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/sampling-rate/sampling-ratesmyAbstract.html',
                    controller: 'SamplingRateMyAbstractController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('samplingRate');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('sampling-ratemyAbstract-detail', {
            parent: 'entity',
            url: '/sampling-ratemyAbstract/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'abstractApp.samplingRate.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/sampling-rate/sampling-ratemyAbstract-detail.html',
                    controller: 'SamplingRateMyAbstractDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('samplingRate');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'SamplingRate', function($stateParams, SamplingRate) {
                    return SamplingRate.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'sampling-ratemyAbstract',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('sampling-ratemyAbstract-detail.edit', {
            parent: 'sampling-ratemyAbstract-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/sampling-rate/sampling-ratemyAbstract-dialog.html',
                    controller: 'SamplingRateMyAbstractDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['SamplingRate', function(SamplingRate) {
                            return SamplingRate.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('sampling-ratemyAbstract.new', {
            parent: 'sampling-ratemyAbstract',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/sampling-rate/sampling-ratemyAbstract-dialog.html',
                    controller: 'SamplingRateMyAbstractDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                typeStream: null,
                                valeur: null,
                                streamer: null,
                                dateSup: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('sampling-ratemyAbstract', null, { reload: 'sampling-ratemyAbstract' });
                }, function() {
                    $state.go('sampling-ratemyAbstract');
                });
            }]
        })
        .state('sampling-ratemyAbstract.edit', {
            parent: 'sampling-ratemyAbstract',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/sampling-rate/sampling-ratemyAbstract-dialog.html',
                    controller: 'SamplingRateMyAbstractDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['SamplingRate', function(SamplingRate) {
                            return SamplingRate.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('sampling-ratemyAbstract', null, { reload: 'sampling-ratemyAbstract' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('sampling-ratemyAbstract.delete', {
            parent: 'sampling-ratemyAbstract',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/sampling-rate/sampling-ratemyAbstract-delete-dialog.html',
                    controller: 'SamplingRateMyAbstractDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['SamplingRate', function(SamplingRate) {
                            return SamplingRate.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('sampling-ratemyAbstract', null, { reload: 'sampling-ratemyAbstract' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
