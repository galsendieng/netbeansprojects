(function() {
    'use strict';

    angular
        .module('abstractApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('major-versionmyAbstract', {
            parent: 'entity',
            url: '/major-versionmyAbstract?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'abstractApp.majorVersion.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/major-version/major-versionsmyAbstract.html',
                    controller: 'MajorVersionMyAbstractController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('majorVersion');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('major-versionmyAbstract-detail', {
            parent: 'entity',
            url: '/major-versionmyAbstract/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'abstractApp.majorVersion.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/major-version/major-versionmyAbstract-detail.html',
                    controller: 'MajorVersionMyAbstractDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('majorVersion');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'MajorVersion', function($stateParams, MajorVersion) {
                    return MajorVersion.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'major-versionmyAbstract',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('major-versionmyAbstract-detail.edit', {
            parent: 'major-versionmyAbstract-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/major-version/major-versionmyAbstract-dialog.html',
                    controller: 'MajorVersionMyAbstractDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['MajorVersion', function(MajorVersion) {
                            return MajorVersion.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('major-versionmyAbstract.new', {
            parent: 'major-versionmyAbstract',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/major-version/major-versionmyAbstract-dialog.html',
                    controller: 'MajorVersionMyAbstractDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                valeur: null,
                                streamer: null,
                                dateSup: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('major-versionmyAbstract', null, { reload: 'major-versionmyAbstract' });
                }, function() {
                    $state.go('major-versionmyAbstract');
                });
            }]
        })
        .state('major-versionmyAbstract.edit', {
            parent: 'major-versionmyAbstract',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/major-version/major-versionmyAbstract-dialog.html',
                    controller: 'MajorVersionMyAbstractDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['MajorVersion', function(MajorVersion) {
                            return MajorVersion.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('major-versionmyAbstract', null, { reload: 'major-versionmyAbstract' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('major-versionmyAbstract.delete', {
            parent: 'major-versionmyAbstract',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/major-version/major-versionmyAbstract-delete-dialog.html',
                    controller: 'MajorVersionMyAbstractDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['MajorVersion', function(MajorVersion) {
                            return MajorVersion.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('major-versionmyAbstract', null, { reload: 'major-versionmyAbstract' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
