(function() {
    'use strict';

    angular
        .module('abstractApp')
        .controller('MajorVersionMyAbstractDeleteController',MajorVersionMyAbstractDeleteController);

    MajorVersionMyAbstractDeleteController.$inject = ['$uibModalInstance', 'entity', 'MajorVersion'];

    function MajorVersionMyAbstractDeleteController($uibModalInstance, entity, MajorVersion) {
        var vm = this;

        vm.majorVersion = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            MajorVersion.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
