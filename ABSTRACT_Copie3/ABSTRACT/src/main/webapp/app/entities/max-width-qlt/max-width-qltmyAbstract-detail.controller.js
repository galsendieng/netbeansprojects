(function() {
    'use strict';

    angular
        .module('abstractApp')
        .controller('MaxWidthQltMyAbstractDetailController', MaxWidthQltMyAbstractDetailController);

    MaxWidthQltMyAbstractDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'MaxWidthQlt'];

    function MaxWidthQltMyAbstractDetailController($scope, $rootScope, $stateParams, previousState, entity, MaxWidthQlt) {
        var vm = this;

        vm.maxWidthQlt = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('abstractApp:maxWidthQltUpdate', function(event, result) {
            vm.maxWidthQlt = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
