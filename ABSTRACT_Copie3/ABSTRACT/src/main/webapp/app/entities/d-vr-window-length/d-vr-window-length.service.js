(function() {
    'use strict';
    angular
        .module('abstractApp')
        .factory('DVRWindowLength', DVRWindowLength);

    DVRWindowLength.$inject = ['$resource', 'DateUtils'];

    function DVRWindowLength ($resource, DateUtils) {
        var resourceUrl =  'api/d-vr-window-lengths/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.dateSup = DateUtils.convertDateTimeFromServer(data.dateSup);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
