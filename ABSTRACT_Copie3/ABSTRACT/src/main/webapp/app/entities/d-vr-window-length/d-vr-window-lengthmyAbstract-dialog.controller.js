(function() {
    'use strict';

    angular
        .module('abstractApp')
        .controller('DVRWindowLengthMyAbstractDialogController', DVRWindowLengthMyAbstractDialogController);

    DVRWindowLengthMyAbstractDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'DVRWindowLength'];

    function DVRWindowLengthMyAbstractDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, DVRWindowLength) {
        var vm = this;

        vm.dVRWindowLength = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.dVRWindowLength.id !== null) {
                DVRWindowLength.update(vm.dVRWindowLength, onSaveSuccess, onSaveError);
            } else {
                DVRWindowLength.save(vm.dVRWindowLength, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('abstractApp:dVRWindowLengthUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.dateSup = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
