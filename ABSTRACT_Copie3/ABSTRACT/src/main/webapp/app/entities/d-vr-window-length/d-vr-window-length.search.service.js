(function() {
    'use strict';

    angular
        .module('abstractApp')
        .factory('DVRWindowLengthSearch', DVRWindowLengthSearch);

    DVRWindowLengthSearch.$inject = ['$resource'];

    function DVRWindowLengthSearch($resource) {
        var resourceUrl =  'api/_search/d-vr-window-lengths/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
