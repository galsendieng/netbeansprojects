(function() {
    'use strict';

    angular
        .module('abstractApp')
        .controller('DVRWindowLengthMyAbstractDeleteController',DVRWindowLengthMyAbstractDeleteController);

    DVRWindowLengthMyAbstractDeleteController.$inject = ['$uibModalInstance', 'entity', 'DVRWindowLength'];

    function DVRWindowLengthMyAbstractDeleteController($uibModalInstance, entity, DVRWindowLength) {
        var vm = this;

        vm.dVRWindowLength = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            DVRWindowLength.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
