(function() {
    'use strict';

    angular
        .module('abstractApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('quality-levelsmyAbstract', {
            parent: 'entity',
            url: '/quality-levelsmyAbstract?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'abstractApp.qualityLevels.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/quality-levels/quality-levelsmyAbstract.html',
                    controller: 'QualityLevelsMyAbstractController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('qualityLevels');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('quality-levelsmyAbstract-detail', {
            parent: 'entity',
            url: '/quality-levelsmyAbstract/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'abstractApp.qualityLevels.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/quality-levels/quality-levelsmyAbstract-detail.html',
                    controller: 'QualityLevelsMyAbstractDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('qualityLevels');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'QualityLevels', function($stateParams, QualityLevels) {
                    return QualityLevels.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'quality-levelsmyAbstract',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('quality-levelsmyAbstract-detail.edit', {
            parent: 'quality-levelsmyAbstract-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/quality-levels/quality-levelsmyAbstract-dialog.html',
                    controller: 'QualityLevelsMyAbstractDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['QualityLevels', function(QualityLevels) {
                            return QualityLevels.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('quality-levelsmyAbstract.new', {
            parent: 'quality-levelsmyAbstract',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/quality-levels/quality-levelsmyAbstract-dialog.html',
                    controller: 'QualityLevelsMyAbstractDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                typeStream: null,
                                streamer: null,
                                valeur: null,
                                dateSup: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('quality-levelsmyAbstract', null, { reload: 'quality-levelsmyAbstract' });
                }, function() {
                    $state.go('quality-levelsmyAbstract');
                });
            }]
        })
        .state('quality-levelsmyAbstract.edit', {
            parent: 'quality-levelsmyAbstract',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/quality-levels/quality-levelsmyAbstract-dialog.html',
                    controller: 'QualityLevelsMyAbstractDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['QualityLevels', function(QualityLevels) {
                            return QualityLevels.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('quality-levelsmyAbstract', null, { reload: 'quality-levelsmyAbstract' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('quality-levelsmyAbstract.delete', {
            parent: 'quality-levelsmyAbstract',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/quality-levels/quality-levelsmyAbstract-delete-dialog.html',
                    controller: 'QualityLevelsMyAbstractDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['QualityLevels', function(QualityLevels) {
                            return QualityLevels.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('quality-levelsmyAbstract', null, { reload: 'quality-levelsmyAbstract' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
