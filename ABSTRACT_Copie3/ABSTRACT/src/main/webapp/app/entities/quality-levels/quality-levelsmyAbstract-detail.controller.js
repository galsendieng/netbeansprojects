(function() {
    'use strict';

    angular
        .module('abstractApp')
        .controller('QualityLevelsMyAbstractDetailController', QualityLevelsMyAbstractDetailController);

    QualityLevelsMyAbstractDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'QualityLevels'];

    function QualityLevelsMyAbstractDetailController($scope, $rootScope, $stateParams, previousState, entity, QualityLevels) {
        var vm = this;

        vm.qualityLevels = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('abstractApp:qualityLevelsUpdate', function(event, result) {
            vm.qualityLevels = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
