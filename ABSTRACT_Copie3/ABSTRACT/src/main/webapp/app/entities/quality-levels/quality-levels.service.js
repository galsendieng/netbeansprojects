(function() {
    'use strict';
    angular
        .module('abstractApp')
        .factory('QualityLevels', QualityLevels);

    QualityLevels.$inject = ['$resource', 'DateUtils'];

    function QualityLevels ($resource, DateUtils) {
        var resourceUrl =  'api/quality-levels/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.dateSup = DateUtils.convertDateTimeFromServer(data.dateSup);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
