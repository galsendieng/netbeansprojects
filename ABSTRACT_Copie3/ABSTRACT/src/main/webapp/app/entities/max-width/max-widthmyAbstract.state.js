(function() {
    'use strict';

    angular
        .module('abstractApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('max-widthmyAbstract', {
            parent: 'entity',
            url: '/max-widthmyAbstract?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'abstractApp.maxWidth.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/max-width/max-widthsmyAbstract.html',
                    controller: 'MaxWidthMyAbstractController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('maxWidth');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('max-widthmyAbstract-detail', {
            parent: 'entity',
            url: '/max-widthmyAbstract/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'abstractApp.maxWidth.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/max-width/max-widthmyAbstract-detail.html',
                    controller: 'MaxWidthMyAbstractDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('maxWidth');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'MaxWidth', function($stateParams, MaxWidth) {
                    return MaxWidth.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'max-widthmyAbstract',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('max-widthmyAbstract-detail.edit', {
            parent: 'max-widthmyAbstract-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/max-width/max-widthmyAbstract-dialog.html',
                    controller: 'MaxWidthMyAbstractDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['MaxWidth', function(MaxWidth) {
                            return MaxWidth.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('max-widthmyAbstract.new', {
            parent: 'max-widthmyAbstract',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/max-width/max-widthmyAbstract-dialog.html',
                    controller: 'MaxWidthMyAbstractDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                typeStream: null,
                                streamer: null,
                                valeur: null,
                                dateSup: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('max-widthmyAbstract', null, { reload: 'max-widthmyAbstract' });
                }, function() {
                    $state.go('max-widthmyAbstract');
                });
            }]
        })
        .state('max-widthmyAbstract.edit', {
            parent: 'max-widthmyAbstract',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/max-width/max-widthmyAbstract-dialog.html',
                    controller: 'MaxWidthMyAbstractDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['MaxWidth', function(MaxWidth) {
                            return MaxWidth.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('max-widthmyAbstract', null, { reload: 'max-widthmyAbstract' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('max-widthmyAbstract.delete', {
            parent: 'max-widthmyAbstract',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/max-width/max-widthmyAbstract-delete-dialog.html',
                    controller: 'MaxWidthMyAbstractDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['MaxWidth', function(MaxWidth) {
                            return MaxWidth.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('max-widthmyAbstract', null, { reload: 'max-widthmyAbstract' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
