(function() {
    'use strict';

    angular
        .module('abstractApp')
        .controller('MaxWidthMyAbstractDeleteController',MaxWidthMyAbstractDeleteController);

    MaxWidthMyAbstractDeleteController.$inject = ['$uibModalInstance', 'entity', 'MaxWidth'];

    function MaxWidthMyAbstractDeleteController($uibModalInstance, entity, MaxWidth) {
        var vm = this;

        vm.maxWidth = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            MaxWidth.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
