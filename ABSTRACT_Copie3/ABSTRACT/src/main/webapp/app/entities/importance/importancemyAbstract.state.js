(function() {
    'use strict';

    angular
        .module('abstractApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('importancemyAbstract', {
            parent: 'entity',
            url: '/importancemyAbstract',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'abstractApp.importance.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/importance/importancesmyAbstract.html',
                    controller: 'ImportanceMyAbstractController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('importance');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('importancemyAbstract-detail', {
            parent: 'entity',
            url: '/importancemyAbstract/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'abstractApp.importance.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/importance/importancemyAbstract-detail.html',
                    controller: 'ImportanceMyAbstractDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('importance');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'Importance', function($stateParams, Importance) {
                    return Importance.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'importancemyAbstract',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('importancemyAbstract-detail.edit', {
            parent: 'importancemyAbstract-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/importance/importancemyAbstract-dialog.html',
                    controller: 'ImportanceMyAbstractDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Importance', function(Importance) {
                            return Importance.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('importancemyAbstract.new', {
            parent: 'importancemyAbstract',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/importance/importancemyAbstract-dialog.html',
                    controller: 'ImportanceMyAbstractDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                niveau: null,
                                description: null,
                                icone: null,
                                iconeContentType: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('importancemyAbstract', null, { reload: 'importancemyAbstract' });
                }, function() {
                    $state.go('importancemyAbstract');
                });
            }]
        })
        .state('importancemyAbstract.edit', {
            parent: 'importancemyAbstract',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/importance/importancemyAbstract-dialog.html',
                    controller: 'ImportanceMyAbstractDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Importance', function(Importance) {
                            return Importance.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('importancemyAbstract', null, { reload: 'importancemyAbstract' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('importancemyAbstract.delete', {
            parent: 'importancemyAbstract',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/importance/importancemyAbstract-delete-dialog.html',
                    controller: 'ImportanceMyAbstractDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['Importance', function(Importance) {
                            return Importance.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('importancemyAbstract', null, { reload: 'importancemyAbstract' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
