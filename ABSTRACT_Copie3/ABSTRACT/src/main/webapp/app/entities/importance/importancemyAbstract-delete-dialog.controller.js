(function() {
    'use strict';

    angular
        .module('abstractApp')
        .controller('ImportanceMyAbstractDeleteController',ImportanceMyAbstractDeleteController);

    ImportanceMyAbstractDeleteController.$inject = ['$uibModalInstance', 'entity', 'Importance'];

    function ImportanceMyAbstractDeleteController($uibModalInstance, entity, Importance) {
        var vm = this;

        vm.importance = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            Importance.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
