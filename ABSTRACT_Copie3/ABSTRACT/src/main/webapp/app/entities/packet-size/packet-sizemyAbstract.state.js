(function() {
    'use strict';

    angular
        .module('abstractApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('packet-sizemyAbstract', {
            parent: 'entity',
            url: '/packet-sizemyAbstract?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'abstractApp.packetSize.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/packet-size/packet-sizesmyAbstract.html',
                    controller: 'PacketSizeMyAbstractController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('packetSize');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('packet-sizemyAbstract-detail', {
            parent: 'entity',
            url: '/packet-sizemyAbstract/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'abstractApp.packetSize.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/packet-size/packet-sizemyAbstract-detail.html',
                    controller: 'PacketSizeMyAbstractDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('packetSize');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'PacketSize', function($stateParams, PacketSize) {
                    return PacketSize.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'packet-sizemyAbstract',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('packet-sizemyAbstract-detail.edit', {
            parent: 'packet-sizemyAbstract-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/packet-size/packet-sizemyAbstract-dialog.html',
                    controller: 'PacketSizeMyAbstractDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['PacketSize', function(PacketSize) {
                            return PacketSize.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('packet-sizemyAbstract.new', {
            parent: 'packet-sizemyAbstract',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/packet-size/packet-sizemyAbstract-dialog.html',
                    controller: 'PacketSizeMyAbstractDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                typeStream: null,
                                valeur: null,
                                streamer: null,
                                dateSup: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('packet-sizemyAbstract', null, { reload: 'packet-sizemyAbstract' });
                }, function() {
                    $state.go('packet-sizemyAbstract');
                });
            }]
        })
        .state('packet-sizemyAbstract.edit', {
            parent: 'packet-sizemyAbstract',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/packet-size/packet-sizemyAbstract-dialog.html',
                    controller: 'PacketSizeMyAbstractDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['PacketSize', function(PacketSize) {
                            return PacketSize.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('packet-sizemyAbstract', null, { reload: 'packet-sizemyAbstract' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('packet-sizemyAbstract.delete', {
            parent: 'packet-sizemyAbstract',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/packet-size/packet-sizemyAbstract-delete-dialog.html',
                    controller: 'PacketSizeMyAbstractDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['PacketSize', function(PacketSize) {
                            return PacketSize.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('packet-sizemyAbstract', null, { reload: 'packet-sizemyAbstract' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
