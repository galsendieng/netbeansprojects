(function() {
    'use strict';

    angular
        .module('abstractApp')
        .controller('MaxHeightQltMyAbstractDeleteController',MaxHeightQltMyAbstractDeleteController);

    MaxHeightQltMyAbstractDeleteController.$inject = ['$uibModalInstance', 'entity', 'MaxHeightQlt'];

    function MaxHeightQltMyAbstractDeleteController($uibModalInstance, entity, MaxHeightQlt) {
        var vm = this;

        vm.maxHeightQlt = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            MaxHeightQlt.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
