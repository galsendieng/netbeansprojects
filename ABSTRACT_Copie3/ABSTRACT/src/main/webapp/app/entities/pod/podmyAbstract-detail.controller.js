(function() {
    'use strict';

    angular
        .module('abstractApp')
        .controller('PodMyAbstractDetailController', PodMyAbstractDetailController);

    PodMyAbstractDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Pod', 'Streamer'];

    function PodMyAbstractDetailController($scope, $rootScope, $stateParams, previousState, entity, Pod, Streamer) {
        var vm = this;

        vm.pod = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('abstractApp:podUpdate', function(event, result) {
            vm.pod = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
