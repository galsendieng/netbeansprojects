(function() {
    'use strict';

    angular
        .module('abstractApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('corrupt-manifestmyAbstract', {
            parent: 'entity',
            url: '/corrupt-manifestmyAbstract?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'abstractApp.corruptManifest.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/corrupt-manifest/corrupt-manifestsmyAbstract.html',
                    controller: 'CorruptManifestMyAbstractController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('corruptManifest');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('corrupt-manifestmyAbstract-detail', {
            parent: 'entity',
            url: '/corrupt-manifestmyAbstract/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'abstractApp.corruptManifest.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/corrupt-manifest/corrupt-manifestmyAbstract-detail.html',
                    controller: 'CorruptManifestMyAbstractDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('corruptManifest');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'CorruptManifest', function($stateParams, CorruptManifest) {
                    return CorruptManifest.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'corrupt-manifestmyAbstract',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('corrupt-manifestmyAbstract-detail.edit', {
            parent: 'corrupt-manifestmyAbstract-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/corrupt-manifest/corrupt-manifestmyAbstract-dialog.html',
                    controller: 'CorruptManifestMyAbstractDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['CorruptManifest', function(CorruptManifest) {
                            return CorruptManifest.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('corrupt-manifestmyAbstract.new', {
            parent: 'corrupt-manifestmyAbstract',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/corrupt-manifest/corrupt-manifestmyAbstract-dialog.html',
                    controller: 'CorruptManifestMyAbstractDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                dateManifest: null,
                                command: null,
                                profil: null,
                                file: null,
                                fileContentType: null,
                                errors: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('corrupt-manifestmyAbstract', null, { reload: 'corrupt-manifestmyAbstract' });
                }, function() {
                    $state.go('corrupt-manifestmyAbstract');
                });
            }]
        })
        .state('corrupt-manifestmyAbstract.edit', {
            parent: 'corrupt-manifestmyAbstract',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/corrupt-manifest/corrupt-manifestmyAbstract-dialog.html',
                    controller: 'CorruptManifestMyAbstractDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['CorruptManifest', function(CorruptManifest) {
                            return CorruptManifest.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('corrupt-manifestmyAbstract', null, { reload: 'corrupt-manifestmyAbstract' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('corrupt-manifestmyAbstract.delete', {
            parent: 'corrupt-manifestmyAbstract',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/corrupt-manifest/corrupt-manifestmyAbstract-delete-dialog.html',
                    controller: 'CorruptManifestMyAbstractDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['CorruptManifest', function(CorruptManifest) {
                            return CorruptManifest.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('corrupt-manifestmyAbstract', null, { reload: 'corrupt-manifestmyAbstract' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
