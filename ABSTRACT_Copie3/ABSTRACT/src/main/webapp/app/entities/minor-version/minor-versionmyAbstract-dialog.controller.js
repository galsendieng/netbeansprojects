(function() {
    'use strict';

    angular
        .module('abstractApp')
        .controller('MinorVersionMyAbstractDialogController', MinorVersionMyAbstractDialogController);

    MinorVersionMyAbstractDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'MinorVersion'];

    function MinorVersionMyAbstractDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, MinorVersion) {
        var vm = this;

        vm.minorVersion = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.minorVersion.id !== null) {
                MinorVersion.update(vm.minorVersion, onSaveSuccess, onSaveError);
            } else {
                MinorVersion.save(vm.minorVersion, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('abstractApp:minorVersionUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.dateSup = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
