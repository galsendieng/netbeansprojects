(function() {
    'use strict';

    angular
        .module('abstractApp')
        .controller('CodecPrivateDataMyAbstractDeleteController',CodecPrivateDataMyAbstractDeleteController);

    CodecPrivateDataMyAbstractDeleteController.$inject = ['$uibModalInstance', 'entity', 'CodecPrivateData'];

    function CodecPrivateDataMyAbstractDeleteController($uibModalInstance, entity, CodecPrivateData) {
        var vm = this;

        vm.codecPrivateData = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            CodecPrivateData.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
