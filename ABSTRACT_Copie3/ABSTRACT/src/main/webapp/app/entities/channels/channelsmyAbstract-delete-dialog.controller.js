(function() {
    'use strict';

    angular
        .module('abstractApp')
        .controller('ChannelsMyAbstractDeleteController',ChannelsMyAbstractDeleteController);

    ChannelsMyAbstractDeleteController.$inject = ['$uibModalInstance', 'entity', 'Channels'];

    function ChannelsMyAbstractDeleteController($uibModalInstance, entity, Channels) {
        var vm = this;

        vm.channels = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            Channels.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
