(function() {
    'use strict';
    angular
        .module('abstractApp')
        .factory('Bitrate', Bitrate);

    Bitrate.$inject = ['$resource', 'DateUtils'];

    function Bitrate ($resource, DateUtils) {
        var resourceUrl =  'api/bitrates/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.dateSup = DateUtils.convertDateTimeFromServer(data.dateSup);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
