(function() {
    'use strict';

    angular
        .module('abstractApp')
        .factory('BitrateSearch', BitrateSearch);

    BitrateSearch.$inject = ['$resource'];

    function BitrateSearch($resource) {
        var resourceUrl =  'api/_search/bitrates/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
