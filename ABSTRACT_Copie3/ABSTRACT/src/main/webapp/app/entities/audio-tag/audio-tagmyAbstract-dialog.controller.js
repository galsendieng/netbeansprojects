(function() {
    'use strict';

    angular
        .module('abstractApp')
        .controller('AudioTagMyAbstractDialogController', AudioTagMyAbstractDialogController);

    AudioTagMyAbstractDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'AudioTag'];

    function AudioTagMyAbstractDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, AudioTag) {
        var vm = this;

        vm.audioTag = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.audioTag.id !== null) {
                AudioTag.update(vm.audioTag, onSaveSuccess, onSaveError);
            } else {
                AudioTag.save(vm.audioTag, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('abstractApp:audioTagUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.dateSup = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
