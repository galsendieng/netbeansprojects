(function() {
    'use strict';

    angular
        .module('abstractApp')
        .controller('ModeMyAbstractDetailController', ModeMyAbstractDetailController);

    ModeMyAbstractDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Mode', 'Playing'];

    function ModeMyAbstractDetailController($scope, $rootScope, $stateParams, previousState, entity, Mode, Playing) {
        var vm = this;

        vm.mode = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('abstractApp:modeUpdate', function(event, result) {
            vm.mode = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
