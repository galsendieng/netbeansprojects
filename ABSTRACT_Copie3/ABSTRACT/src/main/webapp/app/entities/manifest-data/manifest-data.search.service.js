(function() {
    'use strict';

    angular
        .module('abstractApp')
        .factory('ManifestDataSearch', ManifestDataSearch);

    ManifestDataSearch.$inject = ['$resource'];

    function ManifestDataSearch($resource) {
        var resourceUrl =  'api/_search/manifest-data/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
