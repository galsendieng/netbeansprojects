(function() {
    'use strict';

    angular
        .module('abstractApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('manifest-datamyAbstract', {
            parent: 'entity',
            url: '/manifest-datamyAbstract?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'abstractApp.manifestData.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/manifest-data/manifest-datamyAbstract.html',
                    controller: 'ManifestDataMyAbstractController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('manifestData');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('manifest-datamyAbstract-detail', {
            parent: 'entity',
            url: '/manifest-datamyAbstract/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'abstractApp.manifestData.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/manifest-data/manifest-datamyAbstract-detail.html',
                    controller: 'ManifestDataMyAbstractDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('manifestData');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'ManifestData', function($stateParams, ManifestData) {
                    return ManifestData.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'manifest-datamyAbstract',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('manifest-datamyAbstract-detail.edit', {
            parent: 'manifest-datamyAbstract-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/manifest-data/manifest-datamyAbstract-dialog.html',
                    controller: 'ManifestDataMyAbstractDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['ManifestData', function(ManifestData) {
                            return ManifestData.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('manifest-datamyAbstract.new', {
            parent: 'manifest-datamyAbstract',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/manifest-data/manifest-datamyAbstract-dialog.html',
                    controller: 'ManifestDataMyAbstractDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                attribut: null,
                                valeurAttendue: null,
                                messageErreur: null,
                                details: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('manifest-datamyAbstract', null, { reload: 'manifest-datamyAbstract' });
                }, function() {
                    $state.go('manifest-datamyAbstract');
                });
            }]
        })
        .state('manifest-datamyAbstract.edit', {
            parent: 'manifest-datamyAbstract',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/manifest-data/manifest-datamyAbstract-dialog.html',
                    controller: 'ManifestDataMyAbstractDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['ManifestData', function(ManifestData) {
                            return ManifestData.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('manifest-datamyAbstract', null, { reload: 'manifest-datamyAbstract' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('manifest-datamyAbstract.delete', {
            parent: 'manifest-datamyAbstract',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/manifest-data/manifest-datamyAbstract-delete-dialog.html',
                    controller: 'ManifestDataMyAbstractDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['ManifestData', function(ManifestData) {
                            return ManifestData.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('manifest-datamyAbstract', null, { reload: 'manifest-datamyAbstract' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
