(function() {
    'use strict';

    angular
        .module('abstractApp')
        .controller('ManifestDataMyAbstractDeleteController',ManifestDataMyAbstractDeleteController);

    ManifestDataMyAbstractDeleteController.$inject = ['$uibModalInstance', 'entity', 'ManifestData'];

    function ManifestDataMyAbstractDeleteController($uibModalInstance, entity, ManifestData) {
        var vm = this;

        vm.manifestData = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            ManifestData.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
