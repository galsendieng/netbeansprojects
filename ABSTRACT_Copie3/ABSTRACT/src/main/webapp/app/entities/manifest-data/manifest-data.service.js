(function() {
    'use strict';
    angular
        .module('abstractApp')
        .factory('ManifestData', ManifestData);

    ManifestData.$inject = ['$resource'];

    function ManifestData ($resource) {
        var resourceUrl =  'api/manifest-data/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
