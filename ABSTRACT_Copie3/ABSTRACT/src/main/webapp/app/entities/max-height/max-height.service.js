(function() {
    'use strict';
    angular
        .module('abstractApp')
        .factory('MaxHeight', MaxHeight);

    MaxHeight.$inject = ['$resource', 'DateUtils'];

    function MaxHeight ($resource, DateUtils) {
        var resourceUrl =  'api/max-heights/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.dateSup = DateUtils.convertDateTimeFromServer(data.dateSup);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
