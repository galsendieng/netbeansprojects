(function() {
    'use strict';

    angular
        .module('abstractApp')
        .controller('MaxHeightMyAbstractDetailController', MaxHeightMyAbstractDetailController);

    MaxHeightMyAbstractDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'MaxHeight'];

    function MaxHeightMyAbstractDetailController($scope, $rootScope, $stateParams, previousState, entity, MaxHeight) {
        var vm = this;

        vm.maxHeight = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('abstractApp:maxHeightUpdate', function(event, result) {
            vm.maxHeight = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
