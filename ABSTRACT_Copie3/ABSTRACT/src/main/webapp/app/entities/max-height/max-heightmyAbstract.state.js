(function() {
    'use strict';

    angular
        .module('abstractApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('max-heightmyAbstract', {
            parent: 'entity',
            url: '/max-heightmyAbstract?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'abstractApp.maxHeight.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/max-height/max-heightsmyAbstract.html',
                    controller: 'MaxHeightMyAbstractController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('maxHeight');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('max-heightmyAbstract-detail', {
            parent: 'entity',
            url: '/max-heightmyAbstract/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'abstractApp.maxHeight.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/max-height/max-heightmyAbstract-detail.html',
                    controller: 'MaxHeightMyAbstractDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('maxHeight');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'MaxHeight', function($stateParams, MaxHeight) {
                    return MaxHeight.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'max-heightmyAbstract',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('max-heightmyAbstract-detail.edit', {
            parent: 'max-heightmyAbstract-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/max-height/max-heightmyAbstract-dialog.html',
                    controller: 'MaxHeightMyAbstractDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['MaxHeight', function(MaxHeight) {
                            return MaxHeight.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('max-heightmyAbstract.new', {
            parent: 'max-heightmyAbstract',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/max-height/max-heightmyAbstract-dialog.html',
                    controller: 'MaxHeightMyAbstractDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                typeStream: null,
                                streamer: null,
                                valeur: null,
                                dateSup: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('max-heightmyAbstract', null, { reload: 'max-heightmyAbstract' });
                }, function() {
                    $state.go('max-heightmyAbstract');
                });
            }]
        })
        .state('max-heightmyAbstract.edit', {
            parent: 'max-heightmyAbstract',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/max-height/max-heightmyAbstract-dialog.html',
                    controller: 'MaxHeightMyAbstractDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['MaxHeight', function(MaxHeight) {
                            return MaxHeight.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('max-heightmyAbstract', null, { reload: 'max-heightmyAbstract' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('max-heightmyAbstract.delete', {
            parent: 'max-heightmyAbstract',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/max-height/max-heightmyAbstract-delete-dialog.html',
                    controller: 'MaxHeightMyAbstractDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['MaxHeight', function(MaxHeight) {
                            return MaxHeight.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('max-heightmyAbstract', null, { reload: 'max-heightmyAbstract' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
