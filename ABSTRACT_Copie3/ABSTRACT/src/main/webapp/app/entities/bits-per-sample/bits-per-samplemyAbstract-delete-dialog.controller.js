(function() {
    'use strict';

    angular
        .module('abstractApp')
        .controller('BitsPerSampleMyAbstractDeleteController',BitsPerSampleMyAbstractDeleteController);

    BitsPerSampleMyAbstractDeleteController.$inject = ['$uibModalInstance', 'entity', 'BitsPerSample'];

    function BitsPerSampleMyAbstractDeleteController($uibModalInstance, entity, BitsPerSample) {
        var vm = this;

        vm.bitsPerSample = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            BitsPerSample.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
