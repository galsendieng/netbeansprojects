(function() {
    'use strict';

    angular
        .module('abstractApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('bits-per-samplemyAbstract', {
            parent: 'entity',
            url: '/bits-per-samplemyAbstract?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'abstractApp.bitsPerSample.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/bits-per-sample/bits-per-samplesmyAbstract.html',
                    controller: 'BitsPerSampleMyAbstractController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('bitsPerSample');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('bits-per-samplemyAbstract-detail', {
            parent: 'entity',
            url: '/bits-per-samplemyAbstract/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'abstractApp.bitsPerSample.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/bits-per-sample/bits-per-samplemyAbstract-detail.html',
                    controller: 'BitsPerSampleMyAbstractDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('bitsPerSample');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'BitsPerSample', function($stateParams, BitsPerSample) {
                    return BitsPerSample.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'bits-per-samplemyAbstract',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('bits-per-samplemyAbstract-detail.edit', {
            parent: 'bits-per-samplemyAbstract-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/bits-per-sample/bits-per-samplemyAbstract-dialog.html',
                    controller: 'BitsPerSampleMyAbstractDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['BitsPerSample', function(BitsPerSample) {
                            return BitsPerSample.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('bits-per-samplemyAbstract.new', {
            parent: 'bits-per-samplemyAbstract',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/bits-per-sample/bits-per-samplemyAbstract-dialog.html',
                    controller: 'BitsPerSampleMyAbstractDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                typeStream: null,
                                valeur: null,
                                streamer: null,
                                dateSup: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('bits-per-samplemyAbstract', null, { reload: 'bits-per-samplemyAbstract' });
                }, function() {
                    $state.go('bits-per-samplemyAbstract');
                });
            }]
        })
        .state('bits-per-samplemyAbstract.edit', {
            parent: 'bits-per-samplemyAbstract',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/bits-per-sample/bits-per-samplemyAbstract-dialog.html',
                    controller: 'BitsPerSampleMyAbstractDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['BitsPerSample', function(BitsPerSample) {
                            return BitsPerSample.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('bits-per-samplemyAbstract', null, { reload: 'bits-per-samplemyAbstract' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('bits-per-samplemyAbstract.delete', {
            parent: 'bits-per-samplemyAbstract',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/bits-per-sample/bits-per-samplemyAbstract-delete-dialog.html',
                    controller: 'BitsPerSampleMyAbstractDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['BitsPerSample', function(BitsPerSample) {
                            return BitsPerSample.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('bits-per-samplemyAbstract', null, { reload: 'bits-per-samplemyAbstract' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
