(function() {
    'use strict';

    angular
        .module('abstractApp')
        .controller('BitsPerSampleMyAbstractDialogController', BitsPerSampleMyAbstractDialogController);

    BitsPerSampleMyAbstractDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'BitsPerSample'];

    function BitsPerSampleMyAbstractDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, BitsPerSample) {
        var vm = this;

        vm.bitsPerSample = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.bitsPerSample.id !== null) {
                BitsPerSample.update(vm.bitsPerSample, onSaveSuccess, onSaveError);
            } else {
                BitsPerSample.save(vm.bitsPerSample, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('abstractApp:bitsPerSampleUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.dateSup = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
