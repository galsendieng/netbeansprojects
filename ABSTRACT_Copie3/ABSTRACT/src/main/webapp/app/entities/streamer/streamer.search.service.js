(function() {
    'use strict';

    angular
        .module('abstractApp')
        .factory('StreamerSearch', StreamerSearch);

    StreamerSearch.$inject = ['$resource'];

    function StreamerSearch($resource) {
        var resourceUrl =  'api/_search/streamers/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
