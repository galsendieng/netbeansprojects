(function() {
    'use strict';
    angular
        .module('abstractApp')
        .factory('Streamer', Streamer);

    Streamer.$inject = ['$resource'];

    function Streamer ($resource) {
        var resourceUrl =  'api/streamers/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
