(function() {
    'use strict';

    angular
        .module('abstractApp')
        .controller('StreamerMyAbstractDetailController', StreamerMyAbstractDetailController);

    StreamerMyAbstractDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Streamer', 'Pod', 'Channels'];

    function StreamerMyAbstractDetailController($scope, $rootScope, $stateParams, previousState, entity, Streamer, Pod, Channels) {
        var vm = this;

        vm.streamer = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('abstractApp:streamerUpdate', function(event, result) {
            vm.streamer = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
