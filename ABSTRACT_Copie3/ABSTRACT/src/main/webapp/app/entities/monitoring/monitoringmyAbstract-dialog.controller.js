(function() {
    'use strict';

    angular
        .module('abstractApp')
        .controller('MonitoringMyAbstractDialogController', MonitoringMyAbstractDialogController);

    MonitoringMyAbstractDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'Monitoring'];

    function MonitoringMyAbstractDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, Monitoring) {
        var vm = this;

        vm.monitoring = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.monitoring.id !== null) {
                Monitoring.update(vm.monitoring, onSaveSuccess, onSaveError);
            } else {
                Monitoring.save(vm.monitoring, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('abstractApp:monitoringUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.dateSup = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
