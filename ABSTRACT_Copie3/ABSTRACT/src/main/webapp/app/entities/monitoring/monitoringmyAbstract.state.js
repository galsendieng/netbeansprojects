(function() {
    'use strict';

    angular
        .module('abstractApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('monitoringmyAbstract', {
            parent: 'entity',
            url: '/monitoringmyAbstract?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'abstractApp.monitoring.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/monitoring/monitoringsmyAbstract.html',
                    controller: 'MonitoringMyAbstractController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('monitoring');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('monitoringmyAbstract-detail', {
            parent: 'entity',
            url: '/monitoringmyAbstract/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'abstractApp.monitoring.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/monitoring/monitoringmyAbstract-detail.html',
                    controller: 'MonitoringMyAbstractDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('monitoring');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'Monitoring', function($stateParams, Monitoring) {
                    return Monitoring.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'monitoringmyAbstract',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('monitoringmyAbstract-detail.edit', {
            parent: 'monitoringmyAbstract-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/monitoring/monitoringmyAbstract-dialog.html',
                    controller: 'MonitoringMyAbstractDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Monitoring', function(Monitoring) {
                            return Monitoring.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('monitoringmyAbstract.new', {
            parent: 'monitoringmyAbstract',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/monitoring/monitoringmyAbstract-dialog.html',
                    controller: 'MonitoringMyAbstractDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                attribut: null,
                                severite: null,
                                importance: null,
                                valeurAttendue: null,
                                valeurObtenue: null,
                                dateSup: null,
                                message: null,
                                infos: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('monitoringmyAbstract', null, { reload: 'monitoringmyAbstract' });
                }, function() {
                    $state.go('monitoringmyAbstract');
                });
            }]
        })
        .state('monitoringmyAbstract.edit', {
            parent: 'monitoringmyAbstract',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/monitoring/monitoringmyAbstract-dialog.html',
                    controller: 'MonitoringMyAbstractDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Monitoring', function(Monitoring) {
                            return Monitoring.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('monitoringmyAbstract', null, { reload: 'monitoringmyAbstract' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('monitoringmyAbstract.delete', {
            parent: 'monitoringmyAbstract',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/monitoring/monitoringmyAbstract-delete-dialog.html',
                    controller: 'MonitoringMyAbstractDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['Monitoring', function(Monitoring) {
                            return Monitoring.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('monitoringmyAbstract', null, { reload: 'monitoringmyAbstract' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
