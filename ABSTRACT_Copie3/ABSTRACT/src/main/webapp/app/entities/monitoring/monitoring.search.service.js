(function() {
    'use strict';

    angular
        .module('abstractApp')
        .factory('MonitoringSearch', MonitoringSearch);

    MonitoringSearch.$inject = ['$resource'];

    function MonitoringSearch($resource) {
        var resourceUrl =  'api/_search/monitorings/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
