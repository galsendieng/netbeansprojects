(function() {
    'use strict';

    angular
        .module('abstractApp')
        .controller('ChunksMyAbstractDeleteController',ChunksMyAbstractDeleteController);

    ChunksMyAbstractDeleteController.$inject = ['$uibModalInstance', 'entity', 'Chunks'];

    function ChunksMyAbstractDeleteController($uibModalInstance, entity, Chunks) {
        var vm = this;

        vm.chunks = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            Chunks.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
