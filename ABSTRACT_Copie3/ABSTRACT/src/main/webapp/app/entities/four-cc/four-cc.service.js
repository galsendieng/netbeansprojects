(function() {
    'use strict';
    angular
        .module('abstractApp')
        .factory('FourCC', FourCC);

    FourCC.$inject = ['$resource', 'DateUtils'];

    function FourCC ($resource, DateUtils) {
        var resourceUrl =  'api/four-ccs/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.dateSup = DateUtils.convertDateTimeFromServer(data.dateSup);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
