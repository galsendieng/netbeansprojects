(function() {
    'use strict';

    angular
        .module('abstractApp')
        .factory('FourCCSearch', FourCCSearch);

    FourCCSearch.$inject = ['$resource'];

    function FourCCSearch($resource) {
        var resourceUrl =  'api/_search/four-ccs/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
