(function() {
    'use strict';

    angular
        .module('abstractApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('four-ccmyAbstract', {
            parent: 'entity',
            url: '/four-ccmyAbstract?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'abstractApp.fourCC.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/four-cc/four-ccsmyAbstract.html',
                    controller: 'FourCCMyAbstractController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('fourCC');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('four-ccmyAbstract-detail', {
            parent: 'entity',
            url: '/four-ccmyAbstract/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'abstractApp.fourCC.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/four-cc/four-ccmyAbstract-detail.html',
                    controller: 'FourCCMyAbstractDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('fourCC');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'FourCC', function($stateParams, FourCC) {
                    return FourCC.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'four-ccmyAbstract',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('four-ccmyAbstract-detail.edit', {
            parent: 'four-ccmyAbstract-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/four-cc/four-ccmyAbstract-dialog.html',
                    controller: 'FourCCMyAbstractDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['FourCC', function(FourCC) {
                            return FourCC.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('four-ccmyAbstract.new', {
            parent: 'four-ccmyAbstract',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/four-cc/four-ccmyAbstract-dialog.html',
                    controller: 'FourCCMyAbstractDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                typeStream: null,
                                valeur: null,
                                streamer: null,
                                dateSup: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('four-ccmyAbstract', null, { reload: 'four-ccmyAbstract' });
                }, function() {
                    $state.go('four-ccmyAbstract');
                });
            }]
        })
        .state('four-ccmyAbstract.edit', {
            parent: 'four-ccmyAbstract',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/four-cc/four-ccmyAbstract-dialog.html',
                    controller: 'FourCCMyAbstractDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['FourCC', function(FourCC) {
                            return FourCC.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('four-ccmyAbstract', null, { reload: 'four-ccmyAbstract' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('four-ccmyAbstract.delete', {
            parent: 'four-ccmyAbstract',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/four-cc/four-ccmyAbstract-delete-dialog.html',
                    controller: 'FourCCMyAbstractDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['FourCC', function(FourCC) {
                            return FourCC.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('four-ccmyAbstract', null, { reload: 'four-ccmyAbstract' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
