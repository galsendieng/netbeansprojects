(function() {
    'use strict';

    angular
        .module('abstractApp')
        .controller('BitrateMyAbstractDialogController', BitrateMyAbstractDialogController);

    BitrateMyAbstractDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'Bitrate'];

    function BitrateMyAbstractDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, Bitrate) {
        var vm = this;

        vm.bitrate = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.bitrate.id !== null) {
                Bitrate.update(vm.bitrate, onSaveSuccess, onSaveError);
            } else {
                Bitrate.save(vm.bitrate, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('abstractApp:bitrateUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.dateSup = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
