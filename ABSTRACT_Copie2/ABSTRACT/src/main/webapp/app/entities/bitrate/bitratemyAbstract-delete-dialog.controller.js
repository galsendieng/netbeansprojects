(function() {
    'use strict';

    angular
        .module('abstractApp')
        .controller('BitrateMyAbstractDeleteController',BitrateMyAbstractDeleteController);

    BitrateMyAbstractDeleteController.$inject = ['$uibModalInstance', 'entity', 'Bitrate'];

    function BitrateMyAbstractDeleteController($uibModalInstance, entity, Bitrate) {
        var vm = this;

        vm.bitrate = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            Bitrate.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
