(function() {
    'use strict';

    angular
        .module('abstractApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('bitratemyAbstract', {
            parent: 'entity',
            url: '/bitratemyAbstract?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'abstractApp.bitrate.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/bitrate/bitratesmyAbstract.html',
                    controller: 'BitrateMyAbstractController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('bitrate');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('bitratemyAbstract-detail', {
            parent: 'entity',
            url: '/bitratemyAbstract/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'abstractApp.bitrate.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/bitrate/bitratemyAbstract-detail.html',
                    controller: 'BitrateMyAbstractDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('bitrate');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'Bitrate', function($stateParams, Bitrate) {
                    return Bitrate.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'bitratemyAbstract',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('bitratemyAbstract-detail.edit', {
            parent: 'bitratemyAbstract-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/bitrate/bitratemyAbstract-dialog.html',
                    controller: 'BitrateMyAbstractDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Bitrate', function(Bitrate) {
                            return Bitrate.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('bitratemyAbstract.new', {
            parent: 'bitratemyAbstract',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/bitrate/bitratemyAbstract-dialog.html',
                    controller: 'BitrateMyAbstractDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                typeStream: null,
                                streamer: null,
                                valeur: null,
                                dateSup: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('bitratemyAbstract', null, { reload: 'bitratemyAbstract' });
                }, function() {
                    $state.go('bitratemyAbstract');
                });
            }]
        })
        .state('bitratemyAbstract.edit', {
            parent: 'bitratemyAbstract',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/bitrate/bitratemyAbstract-dialog.html',
                    controller: 'BitrateMyAbstractDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Bitrate', function(Bitrate) {
                            return Bitrate.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('bitratemyAbstract', null, { reload: 'bitratemyAbstract' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('bitratemyAbstract.delete', {
            parent: 'bitratemyAbstract',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/bitrate/bitratemyAbstract-delete-dialog.html',
                    controller: 'BitrateMyAbstractDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['Bitrate', function(Bitrate) {
                            return Bitrate.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('bitratemyAbstract', null, { reload: 'bitratemyAbstract' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
