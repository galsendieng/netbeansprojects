(function() {
    'use strict';

    angular
        .module('abstractApp')
        .controller('BitrateMyAbstractDetailController', BitrateMyAbstractDetailController);

    BitrateMyAbstractDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Bitrate'];

    function BitrateMyAbstractDetailController($scope, $rootScope, $stateParams, previousState, entity, Bitrate) {
        var vm = this;

        vm.bitrate = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('abstractApp:bitrateUpdate', function(event, result) {
            vm.bitrate = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
