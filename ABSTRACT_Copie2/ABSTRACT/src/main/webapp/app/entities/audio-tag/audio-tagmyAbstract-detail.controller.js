(function() {
    'use strict';

    angular
        .module('abstractApp')
        .controller('AudioTagMyAbstractDetailController', AudioTagMyAbstractDetailController);

    AudioTagMyAbstractDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'AudioTag'];

    function AudioTagMyAbstractDetailController($scope, $rootScope, $stateParams, previousState, entity, AudioTag) {
        var vm = this;

        vm.audioTag = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('abstractApp:audioTagUpdate', function(event, result) {
            vm.audioTag = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
