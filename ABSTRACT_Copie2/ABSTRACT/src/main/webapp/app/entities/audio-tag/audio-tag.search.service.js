(function() {
    'use strict';

    angular
        .module('abstractApp')
        .factory('AudioTagSearch', AudioTagSearch);

    AudioTagSearch.$inject = ['$resource'];

    function AudioTagSearch($resource) {
        var resourceUrl =  'api/_search/audio-tags/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
