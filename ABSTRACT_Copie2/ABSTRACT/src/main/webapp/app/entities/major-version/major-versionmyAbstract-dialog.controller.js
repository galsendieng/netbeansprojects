(function() {
    'use strict';

    angular
        .module('abstractApp')
        .controller('MajorVersionMyAbstractDialogController', MajorVersionMyAbstractDialogController);

    MajorVersionMyAbstractDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'MajorVersion'];

    function MajorVersionMyAbstractDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, MajorVersion) {
        var vm = this;

        vm.majorVersion = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.majorVersion.id !== null) {
                MajorVersion.update(vm.majorVersion, onSaveSuccess, onSaveError);
            } else {
                MajorVersion.save(vm.majorVersion, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('abstractApp:majorVersionUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.dateSup = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
