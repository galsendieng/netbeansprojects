(function() {
    'use strict';

    angular
        .module('abstractApp')
        .factory('MajorVersionSearch', MajorVersionSearch);

    MajorVersionSearch.$inject = ['$resource'];

    function MajorVersionSearch($resource) {
        var resourceUrl =  'api/_search/major-versions/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
