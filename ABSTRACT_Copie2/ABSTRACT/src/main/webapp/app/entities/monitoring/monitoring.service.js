(function() {
    'use strict';
    angular
        .module('abstractApp')
        .factory('Monitoring', Monitoring);

    Monitoring.$inject = ['$resource', 'DateUtils'];

    function Monitoring ($resource, DateUtils) {
        var resourceUrl =  'api/monitorings/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.dateSup = DateUtils.convertDateTimeFromServer(data.dateSup);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
