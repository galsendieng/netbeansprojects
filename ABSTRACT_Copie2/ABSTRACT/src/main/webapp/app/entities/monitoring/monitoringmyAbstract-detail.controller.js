(function() {
    'use strict';

    angular
        .module('abstractApp')
        .controller('MonitoringMyAbstractDetailController', MonitoringMyAbstractDetailController);

    MonitoringMyAbstractDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Monitoring'];

    function MonitoringMyAbstractDetailController($scope, $rootScope, $stateParams, previousState, entity, Monitoring) {
        var vm = this;

        vm.monitoring = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('abstractApp:monitoringUpdate', function(event, result) {
            vm.monitoring = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
