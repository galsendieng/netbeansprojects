(function() {
    'use strict';

    angular
        .module('abstractApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('minor-versionmyAbstract', {
            parent: 'entity',
            url: '/minor-versionmyAbstract?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'abstractApp.minorVersion.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/minor-version/minor-versionsmyAbstract.html',
                    controller: 'MinorVersionMyAbstractController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('minorVersion');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('minor-versionmyAbstract-detail', {
            parent: 'entity',
            url: '/minor-versionmyAbstract/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'abstractApp.minorVersion.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/minor-version/minor-versionmyAbstract-detail.html',
                    controller: 'MinorVersionMyAbstractDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('minorVersion');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'MinorVersion', function($stateParams, MinorVersion) {
                    return MinorVersion.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'minor-versionmyAbstract',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('minor-versionmyAbstract-detail.edit', {
            parent: 'minor-versionmyAbstract-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/minor-version/minor-versionmyAbstract-dialog.html',
                    controller: 'MinorVersionMyAbstractDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['MinorVersion', function(MinorVersion) {
                            return MinorVersion.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('minor-versionmyAbstract.new', {
            parent: 'minor-versionmyAbstract',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/minor-version/minor-versionmyAbstract-dialog.html',
                    controller: 'MinorVersionMyAbstractDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                valeur: null,
                                streamer: null,
                                dateSup: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('minor-versionmyAbstract', null, { reload: 'minor-versionmyAbstract' });
                }, function() {
                    $state.go('minor-versionmyAbstract');
                });
            }]
        })
        .state('minor-versionmyAbstract.edit', {
            parent: 'minor-versionmyAbstract',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/minor-version/minor-versionmyAbstract-dialog.html',
                    controller: 'MinorVersionMyAbstractDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['MinorVersion', function(MinorVersion) {
                            return MinorVersion.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('minor-versionmyAbstract', null, { reload: 'minor-versionmyAbstract' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('minor-versionmyAbstract.delete', {
            parent: 'minor-versionmyAbstract',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/minor-version/minor-versionmyAbstract-delete-dialog.html',
                    controller: 'MinorVersionMyAbstractDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['MinorVersion', function(MinorVersion) {
                            return MinorVersion.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('minor-versionmyAbstract', null, { reload: 'minor-versionmyAbstract' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
