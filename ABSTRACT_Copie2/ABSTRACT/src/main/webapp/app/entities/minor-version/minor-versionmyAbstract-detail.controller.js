(function() {
    'use strict';

    angular
        .module('abstractApp')
        .controller('MinorVersionMyAbstractDetailController', MinorVersionMyAbstractDetailController);

    MinorVersionMyAbstractDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'MinorVersion'];

    function MinorVersionMyAbstractDetailController($scope, $rootScope, $stateParams, previousState, entity, MinorVersion) {
        var vm = this;

        vm.minorVersion = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('abstractApp:minorVersionUpdate', function(event, result) {
            vm.minorVersion = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
