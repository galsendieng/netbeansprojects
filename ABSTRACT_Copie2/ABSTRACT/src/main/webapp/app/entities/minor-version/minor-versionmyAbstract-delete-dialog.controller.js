(function() {
    'use strict';

    angular
        .module('abstractApp')
        .controller('MinorVersionMyAbstractDeleteController',MinorVersionMyAbstractDeleteController);

    MinorVersionMyAbstractDeleteController.$inject = ['$uibModalInstance', 'entity', 'MinorVersion'];

    function MinorVersionMyAbstractDeleteController($uibModalInstance, entity, MinorVersion) {
        var vm = this;

        vm.minorVersion = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            MinorVersion.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
