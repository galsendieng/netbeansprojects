(function() {
    'use strict';

    angular
        .module('abstractApp')
        .factory('MinorVersionSearch', MinorVersionSearch);

    MinorVersionSearch.$inject = ['$resource'];

    function MinorVersionSearch($resource) {
        var resourceUrl =  'api/_search/minor-versions/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
