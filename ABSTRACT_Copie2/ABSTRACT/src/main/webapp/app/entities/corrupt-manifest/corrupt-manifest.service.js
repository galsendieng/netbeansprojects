(function() {
    'use strict';
    angular
        .module('abstractApp')
        .factory('CorruptManifest', CorruptManifest);

    CorruptManifest.$inject = ['$resource', 'DateUtils'];

    function CorruptManifest ($resource, DateUtils) {
        var resourceUrl =  'api/corrupt-manifests/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.dateManifest = DateUtils.convertDateTimeFromServer(data.dateManifest);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
