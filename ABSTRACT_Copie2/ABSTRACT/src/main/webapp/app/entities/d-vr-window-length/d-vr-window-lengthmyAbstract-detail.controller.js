(function() {
    'use strict';

    angular
        .module('abstractApp')
        .controller('DVRWindowLengthMyAbstractDetailController', DVRWindowLengthMyAbstractDetailController);

    DVRWindowLengthMyAbstractDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'DVRWindowLength'];

    function DVRWindowLengthMyAbstractDetailController($scope, $rootScope, $stateParams, previousState, entity, DVRWindowLength) {
        var vm = this;

        vm.dVRWindowLength = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('abstractApp:dVRWindowLengthUpdate', function(event, result) {
            vm.dVRWindowLength = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
