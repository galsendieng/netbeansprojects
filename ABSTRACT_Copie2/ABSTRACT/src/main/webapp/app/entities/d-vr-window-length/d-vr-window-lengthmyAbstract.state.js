(function() {
    'use strict';

    angular
        .module('abstractApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('d-vr-window-lengthmyAbstract', {
            parent: 'entity',
            url: '/d-vr-window-lengthmyAbstract?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'abstractApp.dVRWindowLength.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/d-vr-window-length/d-vr-window-lengthsmyAbstract.html',
                    controller: 'DVRWindowLengthMyAbstractController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('dVRWindowLength');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('d-vr-window-lengthmyAbstract-detail', {
            parent: 'entity',
            url: '/d-vr-window-lengthmyAbstract/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'abstractApp.dVRWindowLength.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/d-vr-window-length/d-vr-window-lengthmyAbstract-detail.html',
                    controller: 'DVRWindowLengthMyAbstractDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('dVRWindowLength');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'DVRWindowLength', function($stateParams, DVRWindowLength) {
                    return DVRWindowLength.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'd-vr-window-lengthmyAbstract',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('d-vr-window-lengthmyAbstract-detail.edit', {
            parent: 'd-vr-window-lengthmyAbstract-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/d-vr-window-length/d-vr-window-lengthmyAbstract-dialog.html',
                    controller: 'DVRWindowLengthMyAbstractDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['DVRWindowLength', function(DVRWindowLength) {
                            return DVRWindowLength.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('d-vr-window-lengthmyAbstract.new', {
            parent: 'd-vr-window-lengthmyAbstract',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/d-vr-window-length/d-vr-window-lengthmyAbstract-dialog.html',
                    controller: 'DVRWindowLengthMyAbstractDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                valeur: null,
                                streamer: null,
                                dateSup: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('d-vr-window-lengthmyAbstract', null, { reload: 'd-vr-window-lengthmyAbstract' });
                }, function() {
                    $state.go('d-vr-window-lengthmyAbstract');
                });
            }]
        })
        .state('d-vr-window-lengthmyAbstract.edit', {
            parent: 'd-vr-window-lengthmyAbstract',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/d-vr-window-length/d-vr-window-lengthmyAbstract-dialog.html',
                    controller: 'DVRWindowLengthMyAbstractDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['DVRWindowLength', function(DVRWindowLength) {
                            return DVRWindowLength.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('d-vr-window-lengthmyAbstract', null, { reload: 'd-vr-window-lengthmyAbstract' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('d-vr-window-lengthmyAbstract.delete', {
            parent: 'd-vr-window-lengthmyAbstract',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/d-vr-window-length/d-vr-window-lengthmyAbstract-delete-dialog.html',
                    controller: 'DVRWindowLengthMyAbstractDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['DVRWindowLength', function(DVRWindowLength) {
                            return DVRWindowLength.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('d-vr-window-lengthmyAbstract', null, { reload: 'd-vr-window-lengthmyAbstract' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
