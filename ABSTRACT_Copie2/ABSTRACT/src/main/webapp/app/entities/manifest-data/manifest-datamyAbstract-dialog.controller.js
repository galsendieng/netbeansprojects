(function() {
    'use strict';

    angular
        .module('abstractApp')
        .controller('ManifestDataMyAbstractDialogController', ManifestDataMyAbstractDialogController);

    ManifestDataMyAbstractDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'ManifestData', 'Importance'];

    function ManifestDataMyAbstractDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, ManifestData, Importance) {
        var vm = this;

        vm.manifestData = entity;
        vm.clear = clear;
        vm.save = save;
        vm.importances = Importance.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.manifestData.id !== null) {
                ManifestData.update(vm.manifestData, onSaveSuccess, onSaveError);
            } else {
                ManifestData.save(vm.manifestData, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('abstractApp:manifestDataUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
