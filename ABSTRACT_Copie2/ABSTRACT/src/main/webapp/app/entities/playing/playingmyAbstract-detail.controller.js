(function() {
    'use strict';

    angular
        .module('abstractApp')
        .controller('PlayingMyAbstractDetailController', PlayingMyAbstractDetailController);

    PlayingMyAbstractDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'DataUtils', 'entity', 'Playing', 'Channels', 'Commande', 'Mode'];

    function PlayingMyAbstractDetailController($scope, $rootScope, $stateParams, previousState, DataUtils, entity, Playing, Channels, Commande, Mode) {
        var vm = this;

        vm.playing = entity;
        vm.previousState = previousState.name;
        vm.byteSize = DataUtils.byteSize;
        vm.openFile = DataUtils.openFile;

        var unsubscribe = $rootScope.$on('abstractApp:playingUpdate', function(event, result) {
            vm.playing = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
