(function() {
    'use strict';

    angular
        .module('abstractApp')
        .factory('PlayingSearch', PlayingSearch);

    PlayingSearch.$inject = ['$resource'];

    function PlayingSearch($resource) {
        var resourceUrl =  'api/_search/playings/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
