(function() {
    'use strict';
    angular
        .module('abstractApp')
        .factory('CodecPrivateData', CodecPrivateData);

    CodecPrivateData.$inject = ['$resource', 'DateUtils'];

    function CodecPrivateData ($resource, DateUtils) {
        var resourceUrl =  'api/codec-private-data/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.dateSup = DateUtils.convertDateTimeFromServer(data.dateSup);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
