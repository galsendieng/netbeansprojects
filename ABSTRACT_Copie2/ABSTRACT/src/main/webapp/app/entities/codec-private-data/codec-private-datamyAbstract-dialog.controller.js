(function() {
    'use strict';

    angular
        .module('abstractApp')
        .controller('CodecPrivateDataMyAbstractDialogController', CodecPrivateDataMyAbstractDialogController);

    CodecPrivateDataMyAbstractDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'CodecPrivateData'];

    function CodecPrivateDataMyAbstractDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, CodecPrivateData) {
        var vm = this;

        vm.codecPrivateData = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.codecPrivateData.id !== null) {
                CodecPrivateData.update(vm.codecPrivateData, onSaveSuccess, onSaveError);
            } else {
                CodecPrivateData.save(vm.codecPrivateData, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('abstractApp:codecPrivateDataUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.dateSup = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
