(function() {
    'use strict';

    angular
        .module('abstractApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('codec-private-datamyAbstract', {
            parent: 'entity',
            url: '/codec-private-datamyAbstract?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'abstractApp.codecPrivateData.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/codec-private-data/codec-private-datamyAbstract.html',
                    controller: 'CodecPrivateDataMyAbstractController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('codecPrivateData');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('codec-private-datamyAbstract-detail', {
            parent: 'entity',
            url: '/codec-private-datamyAbstract/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'abstractApp.codecPrivateData.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/codec-private-data/codec-private-datamyAbstract-detail.html',
                    controller: 'CodecPrivateDataMyAbstractDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('codecPrivateData');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'CodecPrivateData', function($stateParams, CodecPrivateData) {
                    return CodecPrivateData.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'codec-private-datamyAbstract',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('codec-private-datamyAbstract-detail.edit', {
            parent: 'codec-private-datamyAbstract-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/codec-private-data/codec-private-datamyAbstract-dialog.html',
                    controller: 'CodecPrivateDataMyAbstractDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['CodecPrivateData', function(CodecPrivateData) {
                            return CodecPrivateData.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('codec-private-datamyAbstract.new', {
            parent: 'codec-private-datamyAbstract',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/codec-private-data/codec-private-datamyAbstract-dialog.html',
                    controller: 'CodecPrivateDataMyAbstractDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                typeStream: null,
                                valeur: null,
                                streamer: null,
                                dateSup: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('codec-private-datamyAbstract', null, { reload: 'codec-private-datamyAbstract' });
                }, function() {
                    $state.go('codec-private-datamyAbstract');
                });
            }]
        })
        .state('codec-private-datamyAbstract.edit', {
            parent: 'codec-private-datamyAbstract',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/codec-private-data/codec-private-datamyAbstract-dialog.html',
                    controller: 'CodecPrivateDataMyAbstractDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['CodecPrivateData', function(CodecPrivateData) {
                            return CodecPrivateData.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('codec-private-datamyAbstract', null, { reload: 'codec-private-datamyAbstract' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('codec-private-datamyAbstract.delete', {
            parent: 'codec-private-datamyAbstract',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/codec-private-data/codec-private-datamyAbstract-delete-dialog.html',
                    controller: 'CodecPrivateDataMyAbstractDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['CodecPrivateData', function(CodecPrivateData) {
                            return CodecPrivateData.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('codec-private-datamyAbstract', null, { reload: 'codec-private-datamyAbstract' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
