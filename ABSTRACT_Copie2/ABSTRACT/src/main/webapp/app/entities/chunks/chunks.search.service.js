(function() {
    'use strict';

    angular
        .module('abstractApp')
        .factory('ChunksSearch', ChunksSearch);

    ChunksSearch.$inject = ['$resource'];

    function ChunksSearch($resource) {
        var resourceUrl =  'api/_search/chunks/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
