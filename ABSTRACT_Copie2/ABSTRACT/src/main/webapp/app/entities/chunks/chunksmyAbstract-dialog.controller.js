(function() {
    'use strict';

    angular
        .module('abstractApp')
        .controller('ChunksMyAbstractDialogController', ChunksMyAbstractDialogController);

    ChunksMyAbstractDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'Chunks'];

    function ChunksMyAbstractDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, Chunks) {
        var vm = this;

        vm.chunks = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.chunks.id !== null) {
                Chunks.update(vm.chunks, onSaveSuccess, onSaveError);
            } else {
                Chunks.save(vm.chunks, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('abstractApp:chunksUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.dateSup = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
