(function() {
    'use strict';
    angular
        .module('abstractApp')
        .factory('Chunks', Chunks);

    Chunks.$inject = ['$resource', 'DateUtils'];

    function Chunks ($resource, DateUtils) {
        var resourceUrl =  'api/chunks/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.dateSup = DateUtils.convertDateTimeFromServer(data.dateSup);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
