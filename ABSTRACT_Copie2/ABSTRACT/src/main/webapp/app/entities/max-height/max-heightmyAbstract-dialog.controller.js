(function() {
    'use strict';

    angular
        .module('abstractApp')
        .controller('MaxHeightMyAbstractDialogController', MaxHeightMyAbstractDialogController);

    MaxHeightMyAbstractDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'MaxHeight'];

    function MaxHeightMyAbstractDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, MaxHeight) {
        var vm = this;

        vm.maxHeight = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.maxHeight.id !== null) {
                MaxHeight.update(vm.maxHeight, onSaveSuccess, onSaveError);
            } else {
                MaxHeight.save(vm.maxHeight, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('abstractApp:maxHeightUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.dateSup = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
