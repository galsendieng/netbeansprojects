(function() {
    'use strict';

    angular
        .module('abstractApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('max-height-qltmyAbstract', {
            parent: 'entity',
            url: '/max-height-qltmyAbstract?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'abstractApp.maxHeightQlt.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/max-height-qlt/max-height-qltsmyAbstract.html',
                    controller: 'MaxHeightQltMyAbstractController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('maxHeightQlt');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('max-height-qltmyAbstract-detail', {
            parent: 'entity',
            url: '/max-height-qltmyAbstract/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'abstractApp.maxHeightQlt.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/max-height-qlt/max-height-qltmyAbstract-detail.html',
                    controller: 'MaxHeightQltMyAbstractDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('maxHeightQlt');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'MaxHeightQlt', function($stateParams, MaxHeightQlt) {
                    return MaxHeightQlt.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'max-height-qltmyAbstract',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('max-height-qltmyAbstract-detail.edit', {
            parent: 'max-height-qltmyAbstract-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/max-height-qlt/max-height-qltmyAbstract-dialog.html',
                    controller: 'MaxHeightQltMyAbstractDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['MaxHeightQlt', function(MaxHeightQlt) {
                            return MaxHeightQlt.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('max-height-qltmyAbstract.new', {
            parent: 'max-height-qltmyAbstract',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/max-height-qlt/max-height-qltmyAbstract-dialog.html',
                    controller: 'MaxHeightQltMyAbstractDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                typeStream: null,
                                valeur: null,
                                streamer: null,
                                dateSup: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('max-height-qltmyAbstract', null, { reload: 'max-height-qltmyAbstract' });
                }, function() {
                    $state.go('max-height-qltmyAbstract');
                });
            }]
        })
        .state('max-height-qltmyAbstract.edit', {
            parent: 'max-height-qltmyAbstract',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/max-height-qlt/max-height-qltmyAbstract-dialog.html',
                    controller: 'MaxHeightQltMyAbstractDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['MaxHeightQlt', function(MaxHeightQlt) {
                            return MaxHeightQlt.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('max-height-qltmyAbstract', null, { reload: 'max-height-qltmyAbstract' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('max-height-qltmyAbstract.delete', {
            parent: 'max-height-qltmyAbstract',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/max-height-qlt/max-height-qltmyAbstract-delete-dialog.html',
                    controller: 'MaxHeightQltMyAbstractDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['MaxHeightQlt', function(MaxHeightQlt) {
                            return MaxHeightQlt.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('max-height-qltmyAbstract', null, { reload: 'max-height-qltmyAbstract' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
