(function() {
    'use strict';
    angular
        .module('abstractApp')
        .factory('MaxHeightQlt', MaxHeightQlt);

    MaxHeightQlt.$inject = ['$resource', 'DateUtils'];

    function MaxHeightQlt ($resource, DateUtils) {
        var resourceUrl =  'api/max-height-qlts/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.dateSup = DateUtils.convertDateTimeFromServer(data.dateSup);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
