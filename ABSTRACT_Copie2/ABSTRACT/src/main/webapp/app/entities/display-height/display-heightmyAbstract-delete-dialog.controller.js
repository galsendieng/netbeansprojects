(function() {
    'use strict';

    angular
        .module('abstractApp')
        .controller('DisplayHeightMyAbstractDeleteController',DisplayHeightMyAbstractDeleteController);

    DisplayHeightMyAbstractDeleteController.$inject = ['$uibModalInstance', 'entity', 'DisplayHeight'];

    function DisplayHeightMyAbstractDeleteController($uibModalInstance, entity, DisplayHeight) {
        var vm = this;

        vm.displayHeight = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            DisplayHeight.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
