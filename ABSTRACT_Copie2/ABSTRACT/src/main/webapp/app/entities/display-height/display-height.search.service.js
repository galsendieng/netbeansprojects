(function() {
    'use strict';

    angular
        .module('abstractApp')
        .factory('DisplayHeightSearch', DisplayHeightSearch);

    DisplayHeightSearch.$inject = ['$resource'];

    function DisplayHeightSearch($resource) {
        var resourceUrl =  'api/_search/display-heights/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
