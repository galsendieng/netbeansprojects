(function() {
    'use strict';

    angular
        .module('abstractApp')
        .controller('ImportanceMyAbstractDialogController', ImportanceMyAbstractDialogController);

    ImportanceMyAbstractDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'Importance', 'ManifestData'];

    function ImportanceMyAbstractDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, Importance, ManifestData) {
        var vm = this;

        vm.importance = entity;
        vm.clear = clear;
        vm.save = save;
        vm.manifestdata = ManifestData.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.importance.id !== null) {
                Importance.update(vm.importance, onSaveSuccess, onSaveError);
            } else {
                Importance.save(vm.importance, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('abstractApp:importanceUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
