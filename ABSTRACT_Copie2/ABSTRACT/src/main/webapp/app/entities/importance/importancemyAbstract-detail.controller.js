(function() {
    'use strict';

    angular
        .module('abstractApp')
        .controller('ImportanceMyAbstractDetailController', ImportanceMyAbstractDetailController);

    ImportanceMyAbstractDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Importance', 'ManifestData'];

    function ImportanceMyAbstractDetailController($scope, $rootScope, $stateParams, previousState, entity, Importance, ManifestData) {
        var vm = this;

        vm.importance = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('abstractApp:importanceUpdate', function(event, result) {
            vm.importance = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
