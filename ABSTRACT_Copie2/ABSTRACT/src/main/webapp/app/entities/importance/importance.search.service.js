(function() {
    'use strict';

    angular
        .module('abstractApp')
        .factory('ImportanceSearch', ImportanceSearch);

    ImportanceSearch.$inject = ['$resource'];

    function ImportanceSearch($resource) {
        var resourceUrl =  'api/_search/importances/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
