(function() {
    'use strict';

    angular
        .module('abstractApp')
        .factory('MaxWidthQltSearch', MaxWidthQltSearch);

    MaxWidthQltSearch.$inject = ['$resource'];

    function MaxWidthQltSearch($resource) {
        var resourceUrl =  'api/_search/max-width-qlts/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
