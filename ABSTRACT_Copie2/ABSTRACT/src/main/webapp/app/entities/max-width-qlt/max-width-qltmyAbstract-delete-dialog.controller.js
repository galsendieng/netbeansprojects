(function() {
    'use strict';

    angular
        .module('abstractApp')
        .controller('MaxWidthQltMyAbstractDeleteController',MaxWidthQltMyAbstractDeleteController);

    MaxWidthQltMyAbstractDeleteController.$inject = ['$uibModalInstance', 'entity', 'MaxWidthQlt'];

    function MaxWidthQltMyAbstractDeleteController($uibModalInstance, entity, MaxWidthQlt) {
        var vm = this;

        vm.maxWidthQlt = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            MaxWidthQlt.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
