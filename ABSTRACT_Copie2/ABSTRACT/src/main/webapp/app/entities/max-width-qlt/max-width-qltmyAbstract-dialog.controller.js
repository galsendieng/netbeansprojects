(function() {
    'use strict';

    angular
        .module('abstractApp')
        .controller('MaxWidthQltMyAbstractDialogController', MaxWidthQltMyAbstractDialogController);

    MaxWidthQltMyAbstractDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'MaxWidthQlt'];

    function MaxWidthQltMyAbstractDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, MaxWidthQlt) {
        var vm = this;

        vm.maxWidthQlt = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.maxWidthQlt.id !== null) {
                MaxWidthQlt.update(vm.maxWidthQlt, onSaveSuccess, onSaveError);
            } else {
                MaxWidthQlt.save(vm.maxWidthQlt, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('abstractApp:maxWidthQltUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.dateSup = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
