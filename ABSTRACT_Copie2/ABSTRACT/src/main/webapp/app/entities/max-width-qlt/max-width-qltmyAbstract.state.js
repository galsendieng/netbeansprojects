(function() {
    'use strict';

    angular
        .module('abstractApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('max-width-qltmyAbstract', {
            parent: 'entity',
            url: '/max-width-qltmyAbstract?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'abstractApp.maxWidthQlt.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/max-width-qlt/max-width-qltsmyAbstract.html',
                    controller: 'MaxWidthQltMyAbstractController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('maxWidthQlt');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('max-width-qltmyAbstract-detail', {
            parent: 'entity',
            url: '/max-width-qltmyAbstract/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'abstractApp.maxWidthQlt.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/max-width-qlt/max-width-qltmyAbstract-detail.html',
                    controller: 'MaxWidthQltMyAbstractDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('maxWidthQlt');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'MaxWidthQlt', function($stateParams, MaxWidthQlt) {
                    return MaxWidthQlt.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'max-width-qltmyAbstract',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('max-width-qltmyAbstract-detail.edit', {
            parent: 'max-width-qltmyAbstract-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/max-width-qlt/max-width-qltmyAbstract-dialog.html',
                    controller: 'MaxWidthQltMyAbstractDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['MaxWidthQlt', function(MaxWidthQlt) {
                            return MaxWidthQlt.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('max-width-qltmyAbstract.new', {
            parent: 'max-width-qltmyAbstract',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/max-width-qlt/max-width-qltmyAbstract-dialog.html',
                    controller: 'MaxWidthQltMyAbstractDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                typeStream: null,
                                valeur: null,
                                streamer: null,
                                dateSup: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('max-width-qltmyAbstract', null, { reload: 'max-width-qltmyAbstract' });
                }, function() {
                    $state.go('max-width-qltmyAbstract');
                });
            }]
        })
        .state('max-width-qltmyAbstract.edit', {
            parent: 'max-width-qltmyAbstract',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/max-width-qlt/max-width-qltmyAbstract-dialog.html',
                    controller: 'MaxWidthQltMyAbstractDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['MaxWidthQlt', function(MaxWidthQlt) {
                            return MaxWidthQlt.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('max-width-qltmyAbstract', null, { reload: 'max-width-qltmyAbstract' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('max-width-qltmyAbstract.delete', {
            parent: 'max-width-qltmyAbstract',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/max-width-qlt/max-width-qltmyAbstract-delete-dialog.html',
                    controller: 'MaxWidthQltMyAbstractDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['MaxWidthQlt', function(MaxWidthQlt) {
                            return MaxWidthQlt.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('max-width-qltmyAbstract', null, { reload: 'max-width-qltmyAbstract' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
