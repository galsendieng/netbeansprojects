(function() {
    'use strict';

    angular
        .module('abstractApp')
        .controller('SamplingRateMyAbstractDialogController', SamplingRateMyAbstractDialogController);

    SamplingRateMyAbstractDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'SamplingRate'];

    function SamplingRateMyAbstractDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, SamplingRate) {
        var vm = this;

        vm.samplingRate = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.samplingRate.id !== null) {
                SamplingRate.update(vm.samplingRate, onSaveSuccess, onSaveError);
            } else {
                SamplingRate.save(vm.samplingRate, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('abstractApp:samplingRateUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.dateSup = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
