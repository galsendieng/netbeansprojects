(function() {
    'use strict';

    angular
        .module('abstractApp')
        .factory('SamplingRateSearch', SamplingRateSearch);

    SamplingRateSearch.$inject = ['$resource'];

    function SamplingRateSearch($resource) {
        var resourceUrl =  'api/_search/sampling-rates/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
