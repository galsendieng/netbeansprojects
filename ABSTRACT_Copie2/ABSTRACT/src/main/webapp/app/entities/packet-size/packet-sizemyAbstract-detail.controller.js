(function() {
    'use strict';

    angular
        .module('abstractApp')
        .controller('PacketSizeMyAbstractDetailController', PacketSizeMyAbstractDetailController);

    PacketSizeMyAbstractDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'PacketSize'];

    function PacketSizeMyAbstractDetailController($scope, $rootScope, $stateParams, previousState, entity, PacketSize) {
        var vm = this;

        vm.packetSize = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('abstractApp:packetSizeUpdate', function(event, result) {
            vm.packetSize = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
