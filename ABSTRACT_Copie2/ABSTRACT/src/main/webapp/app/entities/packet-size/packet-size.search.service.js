(function() {
    'use strict';

    angular
        .module('abstractApp')
        .factory('PacketSizeSearch', PacketSizeSearch);

    PacketSizeSearch.$inject = ['$resource'];

    function PacketSizeSearch($resource) {
        var resourceUrl =  'api/_search/packet-sizes/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
