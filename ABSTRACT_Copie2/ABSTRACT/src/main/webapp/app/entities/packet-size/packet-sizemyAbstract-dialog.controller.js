(function() {
    'use strict';

    angular
        .module('abstractApp')
        .controller('PacketSizeMyAbstractDialogController', PacketSizeMyAbstractDialogController);

    PacketSizeMyAbstractDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'PacketSize'];

    function PacketSizeMyAbstractDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, PacketSize) {
        var vm = this;

        vm.packetSize = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.packetSize.id !== null) {
                PacketSize.update(vm.packetSize, onSaveSuccess, onSaveError);
            } else {
                PacketSize.save(vm.packetSize, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('abstractApp:packetSizeUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.dateSup = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
