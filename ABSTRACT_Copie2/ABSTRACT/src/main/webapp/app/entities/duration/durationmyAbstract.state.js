(function() {
    'use strict';

    angular
        .module('abstractApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('durationmyAbstract', {
            parent: 'entity',
            url: '/durationmyAbstract?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'abstractApp.duration.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/duration/durationsmyAbstract.html',
                    controller: 'DurationMyAbstractController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('duration');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('durationmyAbstract-detail', {
            parent: 'entity',
            url: '/durationmyAbstract/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'abstractApp.duration.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/duration/durationmyAbstract-detail.html',
                    controller: 'DurationMyAbstractDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('duration');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'Duration', function($stateParams, Duration) {
                    return Duration.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'durationmyAbstract',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('durationmyAbstract-detail.edit', {
            parent: 'durationmyAbstract-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/duration/durationmyAbstract-dialog.html',
                    controller: 'DurationMyAbstractDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Duration', function(Duration) {
                            return Duration.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('durationmyAbstract.new', {
            parent: 'durationmyAbstract',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/duration/durationmyAbstract-dialog.html',
                    controller: 'DurationMyAbstractDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                valeur: null,
                                streamer: null,
                                dateSup: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('durationmyAbstract', null, { reload: 'durationmyAbstract' });
                }, function() {
                    $state.go('durationmyAbstract');
                });
            }]
        })
        .state('durationmyAbstract.edit', {
            parent: 'durationmyAbstract',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/duration/durationmyAbstract-dialog.html',
                    controller: 'DurationMyAbstractDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Duration', function(Duration) {
                            return Duration.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('durationmyAbstract', null, { reload: 'durationmyAbstract' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('durationmyAbstract.delete', {
            parent: 'durationmyAbstract',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/duration/durationmyAbstract-delete-dialog.html',
                    controller: 'DurationMyAbstractDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['Duration', function(Duration) {
                            return Duration.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('durationmyAbstract', null, { reload: 'durationmyAbstract' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
