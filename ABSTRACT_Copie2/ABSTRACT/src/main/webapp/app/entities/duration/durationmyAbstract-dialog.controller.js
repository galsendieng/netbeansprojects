(function() {
    'use strict';

    angular
        .module('abstractApp')
        .controller('DurationMyAbstractDialogController', DurationMyAbstractDialogController);

    DurationMyAbstractDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'Duration'];

    function DurationMyAbstractDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, Duration) {
        var vm = this;

        vm.duration = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.duration.id !== null) {
                Duration.update(vm.duration, onSaveSuccess, onSaveError);
            } else {
                Duration.save(vm.duration, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('abstractApp:durationUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.dateSup = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
