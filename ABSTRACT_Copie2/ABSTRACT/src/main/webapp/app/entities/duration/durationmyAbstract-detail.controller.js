(function() {
    'use strict';

    angular
        .module('abstractApp')
        .controller('DurationMyAbstractDetailController', DurationMyAbstractDetailController);

    DurationMyAbstractDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Duration'];

    function DurationMyAbstractDetailController($scope, $rootScope, $stateParams, previousState, entity, Duration) {
        var vm = this;

        vm.duration = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('abstractApp:durationUpdate', function(event, result) {
            vm.duration = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
