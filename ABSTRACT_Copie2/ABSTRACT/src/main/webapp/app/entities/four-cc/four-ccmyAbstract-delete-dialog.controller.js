(function() {
    'use strict';

    angular
        .module('abstractApp')
        .controller('FourCCMyAbstractDeleteController',FourCCMyAbstractDeleteController);

    FourCCMyAbstractDeleteController.$inject = ['$uibModalInstance', 'entity', 'FourCC'];

    function FourCCMyAbstractDeleteController($uibModalInstance, entity, FourCC) {
        var vm = this;

        vm.fourCC = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            FourCC.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
