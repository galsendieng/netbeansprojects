(function() {
    'use strict';

    angular
        .module('abstractApp')
        .controller('FourCCMyAbstractDetailController', FourCCMyAbstractDetailController);

    FourCCMyAbstractDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'FourCC'];

    function FourCCMyAbstractDetailController($scope, $rootScope, $stateParams, previousState, entity, FourCC) {
        var vm = this;

        vm.fourCC = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('abstractApp:fourCCUpdate', function(event, result) {
            vm.fourCC = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
