(function() {
    'use strict';

    angular
        .module('abstractApp')
        .controller('DisplayWidthMyAbstractDeleteController',DisplayWidthMyAbstractDeleteController);

    DisplayWidthMyAbstractDeleteController.$inject = ['$uibModalInstance', 'entity', 'DisplayWidth'];

    function DisplayWidthMyAbstractDeleteController($uibModalInstance, entity, DisplayWidth) {
        var vm = this;

        vm.displayWidth = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            DisplayWidth.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
