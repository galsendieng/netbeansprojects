(function() {
    'use strict';
    angular
        .module('abstractApp')
        .factory('DisplayWidth', DisplayWidth);

    DisplayWidth.$inject = ['$resource', 'DateUtils'];

    function DisplayWidth ($resource, DateUtils) {
        var resourceUrl =  'api/display-widths/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.dateSup = DateUtils.convertDateTimeFromServer(data.dateSup);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
