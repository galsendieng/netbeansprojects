(function() {
    'use strict';

    angular
        .module('abstractApp')
        .controller('CommandeMyAbstractDetailController', CommandeMyAbstractDetailController);

    CommandeMyAbstractDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Commande', 'Playing'];

    function CommandeMyAbstractDetailController($scope, $rootScope, $stateParams, previousState, entity, Commande, Playing) {
        var vm = this;

        vm.commande = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('abstractApp:commandeUpdate', function(event, result) {
            vm.commande = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
