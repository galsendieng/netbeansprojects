(function() {
    'use strict';

    angular
        .module('abstractApp')
        .controller('CommandeMyAbstractDialogController', CommandeMyAbstractDialogController);

    CommandeMyAbstractDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'Commande', 'Playing'];

    function CommandeMyAbstractDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, Commande, Playing) {
        var vm = this;

        vm.commande = entity;
        vm.clear = clear;
        vm.save = save;
        vm.playings = Playing.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.commande.id !== null) {
                Commande.update(vm.commande, onSaveSuccess, onSaveError);
            } else {
                Commande.save(vm.commande, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('abstractApp:commandeUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
