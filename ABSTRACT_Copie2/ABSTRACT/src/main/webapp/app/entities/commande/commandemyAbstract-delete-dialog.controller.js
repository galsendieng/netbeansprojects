(function() {
    'use strict';

    angular
        .module('abstractApp')
        .controller('CommandeMyAbstractDeleteController',CommandeMyAbstractDeleteController);

    CommandeMyAbstractDeleteController.$inject = ['$uibModalInstance', 'entity', 'Commande'];

    function CommandeMyAbstractDeleteController($uibModalInstance, entity, Commande) {
        var vm = this;

        vm.commande = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            Commande.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
