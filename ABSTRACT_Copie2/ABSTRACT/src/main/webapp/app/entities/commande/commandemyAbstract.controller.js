(function() {
    'use strict';

    angular
        .module('abstractApp')
        .controller('CommandeMyAbstractController', CommandeMyAbstractController);

    CommandeMyAbstractController.$inject = ['$scope', '$state', 'Commande', 'CommandeSearch'];

    function CommandeMyAbstractController ($scope, $state, Commande, CommandeSearch) {
        var vm = this;

        vm.commandes = [];
        vm.search = search;
        vm.loadAll = loadAll;

        loadAll();

        function loadAll() {
            Commande.query(function(result) {
                vm.commandes = result;
            });
        }

        function search () {
            if (!vm.searchQuery) {
                return vm.loadAll();
            }
            CommandeSearch.query({query: vm.searchQuery}, function(result) {
                vm.commandes = result;
            });
        }    }
})();
