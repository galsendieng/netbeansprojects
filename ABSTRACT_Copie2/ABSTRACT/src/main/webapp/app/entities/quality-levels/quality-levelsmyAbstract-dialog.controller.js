(function() {
    'use strict';

    angular
        .module('abstractApp')
        .controller('QualityLevelsMyAbstractDialogController', QualityLevelsMyAbstractDialogController);

    QualityLevelsMyAbstractDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'QualityLevels'];

    function QualityLevelsMyAbstractDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, QualityLevels) {
        var vm = this;

        vm.qualityLevels = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.qualityLevels.id !== null) {
                QualityLevels.update(vm.qualityLevels, onSaveSuccess, onSaveError);
            } else {
                QualityLevels.save(vm.qualityLevels, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('abstractApp:qualityLevelsUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.dateSup = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
