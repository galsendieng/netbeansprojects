(function() {
    'use strict';

    angular
        .module('abstractApp')
        .controller('QualityLevelsMyAbstractDeleteController',QualityLevelsMyAbstractDeleteController);

    QualityLevelsMyAbstractDeleteController.$inject = ['$uibModalInstance', 'entity', 'QualityLevels'];

    function QualityLevelsMyAbstractDeleteController($uibModalInstance, entity, QualityLevels) {
        var vm = this;

        vm.qualityLevels = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            QualityLevels.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
