(function() {
    'use strict';

    angular
        .module('abstractApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('look-ahead-fragment-countmyAbstract', {
            parent: 'entity',
            url: '/look-ahead-fragment-countmyAbstract?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'abstractApp.lookAheadFragmentCount.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/look-ahead-fragment-count/look-ahead-fragment-countsmyAbstract.html',
                    controller: 'LookAheadFragmentCountMyAbstractController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('lookAheadFragmentCount');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('look-ahead-fragment-countmyAbstract-detail', {
            parent: 'entity',
            url: '/look-ahead-fragment-countmyAbstract/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'abstractApp.lookAheadFragmentCount.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/look-ahead-fragment-count/look-ahead-fragment-countmyAbstract-detail.html',
                    controller: 'LookAheadFragmentCountMyAbstractDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('lookAheadFragmentCount');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'LookAheadFragmentCount', function($stateParams, LookAheadFragmentCount) {
                    return LookAheadFragmentCount.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'look-ahead-fragment-countmyAbstract',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('look-ahead-fragment-countmyAbstract-detail.edit', {
            parent: 'look-ahead-fragment-countmyAbstract-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/look-ahead-fragment-count/look-ahead-fragment-countmyAbstract-dialog.html',
                    controller: 'LookAheadFragmentCountMyAbstractDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['LookAheadFragmentCount', function(LookAheadFragmentCount) {
                            return LookAheadFragmentCount.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('look-ahead-fragment-countmyAbstract.new', {
            parent: 'look-ahead-fragment-countmyAbstract',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/look-ahead-fragment-count/look-ahead-fragment-countmyAbstract-dialog.html',
                    controller: 'LookAheadFragmentCountMyAbstractDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                valeur: null,
                                streamer: null,
                                dateSup: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('look-ahead-fragment-countmyAbstract', null, { reload: 'look-ahead-fragment-countmyAbstract' });
                }, function() {
                    $state.go('look-ahead-fragment-countmyAbstract');
                });
            }]
        })
        .state('look-ahead-fragment-countmyAbstract.edit', {
            parent: 'look-ahead-fragment-countmyAbstract',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/look-ahead-fragment-count/look-ahead-fragment-countmyAbstract-dialog.html',
                    controller: 'LookAheadFragmentCountMyAbstractDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['LookAheadFragmentCount', function(LookAheadFragmentCount) {
                            return LookAheadFragmentCount.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('look-ahead-fragment-countmyAbstract', null, { reload: 'look-ahead-fragment-countmyAbstract' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('look-ahead-fragment-countmyAbstract.delete', {
            parent: 'look-ahead-fragment-countmyAbstract',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/look-ahead-fragment-count/look-ahead-fragment-countmyAbstract-delete-dialog.html',
                    controller: 'LookAheadFragmentCountMyAbstractDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['LookAheadFragmentCount', function(LookAheadFragmentCount) {
                            return LookAheadFragmentCount.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('look-ahead-fragment-countmyAbstract', null, { reload: 'look-ahead-fragment-countmyAbstract' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
