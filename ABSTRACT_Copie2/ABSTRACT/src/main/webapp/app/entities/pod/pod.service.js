(function() {
    'use strict';
    angular
        .module('abstractApp')
        .factory('Pod', Pod);

    Pod.$inject = ['$resource'];

    function Pod ($resource) {
        var resourceUrl =  'api/pods/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
