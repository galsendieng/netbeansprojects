(function() {
    'use strict';
    angular
        .module('abstractApp')
        .factory('BitsPerSample', BitsPerSample);

    BitsPerSample.$inject = ['$resource', 'DateUtils'];

    function BitsPerSample ($resource, DateUtils) {
        var resourceUrl =  'api/bits-per-samples/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.dateSup = DateUtils.convertDateTimeFromServer(data.dateSup);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
