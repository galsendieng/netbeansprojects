(function() {
    'use strict';

    angular
        .module('abstractApp')
        .controller('StreamerMyAbstractDeleteController',StreamerMyAbstractDeleteController);

    StreamerMyAbstractDeleteController.$inject = ['$uibModalInstance', 'entity', 'Streamer'];

    function StreamerMyAbstractDeleteController($uibModalInstance, entity, Streamer) {
        var vm = this;

        vm.streamer = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            Streamer.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
