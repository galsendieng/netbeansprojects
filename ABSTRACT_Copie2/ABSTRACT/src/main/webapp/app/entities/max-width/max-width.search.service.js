(function() {
    'use strict';

    angular
        .module('abstractApp')
        .factory('MaxWidthSearch', MaxWidthSearch);

    MaxWidthSearch.$inject = ['$resource'];

    function MaxWidthSearch($resource) {
        var resourceUrl =  'api/_search/max-widths/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
