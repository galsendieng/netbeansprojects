(function() {
    'use strict';
    angular
        .module('abstractApp')
        .factory('MaxWidth', MaxWidth);

    MaxWidth.$inject = ['$resource', 'DateUtils'];

    function MaxWidth ($resource, DateUtils) {
        var resourceUrl =  'api/max-widths/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.dateSup = DateUtils.convertDateTimeFromServer(data.dateSup);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
