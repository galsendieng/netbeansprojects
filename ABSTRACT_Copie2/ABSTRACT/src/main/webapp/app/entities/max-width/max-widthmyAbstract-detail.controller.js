(function() {
    'use strict';

    angular
        .module('abstractApp')
        .controller('MaxWidthMyAbstractDetailController', MaxWidthMyAbstractDetailController);

    MaxWidthMyAbstractDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'MaxWidth'];

    function MaxWidthMyAbstractDetailController($scope, $rootScope, $stateParams, previousState, entity, MaxWidth) {
        var vm = this;

        vm.maxWidth = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('abstractApp:maxWidthUpdate', function(event, result) {
            vm.maxWidth = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
