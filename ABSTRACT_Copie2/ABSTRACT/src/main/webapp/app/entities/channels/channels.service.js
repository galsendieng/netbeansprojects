(function() {
    'use strict';
    angular
        .module('abstractApp')
        .factory('Channels', Channels);

    Channels.$inject = ['$resource'];

    function Channels ($resource) {
        var resourceUrl =  'api/channels/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
