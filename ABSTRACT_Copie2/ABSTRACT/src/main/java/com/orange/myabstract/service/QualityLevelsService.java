package com.orange.myabstract.service;

import com.orange.myabstract.service.dto.QualityLevelsDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.LinkedList;
import java.util.List;

/**
 * Service Interface for managing QualityLevels.
 */
public interface QualityLevelsService {

    /**
     * Save a qualityLevels.
     *
     * @param qualityLevelsDTO the entity to save
     * @return the persisted entity
     */
    QualityLevelsDTO save(QualityLevelsDTO qualityLevelsDTO);

    /**
     *  Get all the qualityLevels.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<QualityLevelsDTO> findAll(Pageable pageable);

    /**
     *  Get the "id" qualityLevels.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    QualityLevelsDTO findOne(Long id);

    /**
     *  Delete the "id" qualityLevels.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Search for the qualityLevels corresponding to the query.
     *
     *  @param query the query of the search
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<QualityLevelsDTO> search(String query, Pageable pageable);
}
