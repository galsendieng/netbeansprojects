package com.orange.myabstract.service;

import com.orange.myabstract.service.dto.CorruptManifestDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.LinkedList;
import java.util.List;

/**
 * Service Interface for managing CorruptManifest.
 */
public interface CorruptManifestService {

    /**
     * Save a corruptManifest.
     *
     * @param corruptManifestDTO the entity to save
     * @return the persisted entity
     */
    CorruptManifestDTO save(CorruptManifestDTO corruptManifestDTO);

    /**
     *  Get all the corruptManifests.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<CorruptManifestDTO> findAll(Pageable pageable);

    /**
     *  Get the "id" corruptManifest.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    CorruptManifestDTO findOne(Long id);

    /**
     *  Delete the "id" corruptManifest.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Search for the corruptManifest corresponding to the query.
     *
     *  @param query the query of the search
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<CorruptManifestDTO> search(String query, Pageable pageable);
}
