package com.orange.myabstract.repository.search;

import com.orange.myabstract.domain.Bitrate;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the Bitrate entity.
 */
public interface BitrateSearchRepository extends ElasticsearchRepository<Bitrate, Long> {
}
