package com.orange.myabstract.service;

import com.orange.myabstract.service.dto.StreamerDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.LinkedList;
import java.util.List;

/**
 * Service Interface for managing Streamer.
 */
public interface StreamerService {

    /**
     * Save a streamer.
     *
     * @param streamerDTO the entity to save
     * @return the persisted entity
     */
    StreamerDTO save(StreamerDTO streamerDTO);

    /**
     *  Get all the streamers.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<StreamerDTO> findAll(Pageable pageable);

    /**
     *  Get the "id" streamer.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    StreamerDTO findOne(Long id);

    /**
     *  Delete the "id" streamer.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Search for the streamer corresponding to the query.
     *
     *  @param query the query of the search
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<StreamerDTO> search(String query, Pageable pageable);
}
