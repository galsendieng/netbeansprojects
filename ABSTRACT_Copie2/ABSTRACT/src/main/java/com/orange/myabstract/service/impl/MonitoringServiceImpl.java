package com.orange.myabstract.service.impl;

import com.orange.myabstract.service.MonitoringService;
import com.orange.myabstract.domain.Monitoring;
import com.orange.myabstract.repository.MonitoringRepository;
import com.orange.myabstract.repository.search.MonitoringSearchRepository;
import com.orange.myabstract.service.dto.MonitoringDTO;
import com.orange.myabstract.service.mapper.MonitoringMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing Monitoring.
 */
@Service
@Transactional
public class MonitoringServiceImpl implements MonitoringService{

    private final Logger log = LoggerFactory.getLogger(MonitoringServiceImpl.class);
    
    @Inject
    private MonitoringRepository monitoringRepository;

    @Inject
    private MonitoringMapper monitoringMapper;

    @Inject
    private MonitoringSearchRepository monitoringSearchRepository;

    /**
     * Save a monitoring.
     *
     * @param monitoringDTO the entity to save
     * @return the persisted entity
     */
    public MonitoringDTO save(MonitoringDTO monitoringDTO) {
        log.debug("Request to save Monitoring : {}", monitoringDTO);
        Monitoring monitoring = monitoringMapper.monitoringDTOToMonitoring(monitoringDTO);
        monitoring = monitoringRepository.save(monitoring);
        MonitoringDTO result = monitoringMapper.monitoringToMonitoringDTO(monitoring);
        monitoringSearchRepository.save(monitoring);
        return result;
    }

    /**
     *  Get all the monitorings.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true) 
    public Page<MonitoringDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Monitorings");
        Page<Monitoring> result = monitoringRepository.findAll(pageable);
        return result.map(monitoring -> monitoringMapper.monitoringToMonitoringDTO(monitoring));
    }

    /**
     *  Get one monitoring by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true) 
    public MonitoringDTO findOne(Long id) {
        log.debug("Request to get Monitoring : {}", id);
        Monitoring monitoring = monitoringRepository.findOne(id);
        MonitoringDTO monitoringDTO = monitoringMapper.monitoringToMonitoringDTO(monitoring);
        return monitoringDTO;
    }

    /**
     *  Delete the  monitoring by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Monitoring : {}", id);
        monitoringRepository.delete(id);
        monitoringSearchRepository.delete(id);
    }

    /**
     * Search for the monitoring corresponding to the query.
     *
     *  @param query the query of the search
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<MonitoringDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Monitorings for query {}", query);
        Page<Monitoring> result = monitoringSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(monitoring -> monitoringMapper.monitoringToMonitoringDTO(monitoring));
    }
}
