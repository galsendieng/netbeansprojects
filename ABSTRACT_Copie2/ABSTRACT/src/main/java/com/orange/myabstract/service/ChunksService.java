package com.orange.myabstract.service;

import com.orange.myabstract.service.dto.ChunksDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.LinkedList;
import java.util.List;

/**
 * Service Interface for managing Chunks.
 */
public interface ChunksService {

    /**
     * Save a chunks.
     *
     * @param chunksDTO the entity to save
     * @return the persisted entity
     */
    ChunksDTO save(ChunksDTO chunksDTO);

    /**
     *  Get all the chunks.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<ChunksDTO> findAll(Pageable pageable);

    /**
     *  Get the "id" chunks.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    ChunksDTO findOne(Long id);

    /**
     *  Delete the "id" chunks.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Search for the chunks corresponding to the query.
     *
     *  @param query the query of the search
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<ChunksDTO> search(String query, Pageable pageable);
}
