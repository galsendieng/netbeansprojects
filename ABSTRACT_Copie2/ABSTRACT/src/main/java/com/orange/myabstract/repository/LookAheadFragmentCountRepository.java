package com.orange.myabstract.repository;

import com.orange.myabstract.domain.LookAheadFragmentCount;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the LookAheadFragmentCount entity.
 */
@SuppressWarnings("unused")
public interface LookAheadFragmentCountRepository extends JpaRepository<LookAheadFragmentCount,Long> {

}
