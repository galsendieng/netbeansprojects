package com.orange.myabstract.repository.search;

import com.orange.myabstract.domain.LookAheadFragmentCount;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the LookAheadFragmentCount entity.
 */
public interface LookAheadFragmentCountSearchRepository extends ElasticsearchRepository<LookAheadFragmentCount, Long> {
}
