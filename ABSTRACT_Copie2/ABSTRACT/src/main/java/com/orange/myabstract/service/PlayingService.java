package com.orange.myabstract.service;

import com.orange.myabstract.service.dto.PlayingDTO;

import java.util.LinkedList;
import java.util.List;

/**
 * Service Interface for managing Playing.
 */
public interface PlayingService {

    /**
     * Save a playing.
     *
     * @param playingDTO the entity to save
     * @return the persisted entity
     */
    PlayingDTO save(PlayingDTO playingDTO);

    /**
     *  Get all the playings.
     *  
     *  @return the list of entities
     */
    List<PlayingDTO> findAll();

    /**
     *  Get the "id" playing.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    PlayingDTO findOne(Long id);

    /**
     *  Delete the "id" playing.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Search for the playing corresponding to the query.
     *
     *  @param query the query of the search
     *  
     *  @return the list of entities
     */
    List<PlayingDTO> search(String query);
}
