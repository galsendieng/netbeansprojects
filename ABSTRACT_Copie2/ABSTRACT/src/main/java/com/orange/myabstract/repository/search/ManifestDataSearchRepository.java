package com.orange.myabstract.repository.search;

import com.orange.myabstract.domain.ManifestData;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the ManifestData entity.
 */
public interface ManifestDataSearchRepository extends ElasticsearchRepository<ManifestData, Long> {
}
