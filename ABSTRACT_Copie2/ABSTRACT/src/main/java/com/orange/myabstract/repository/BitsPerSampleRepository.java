package com.orange.myabstract.repository;

import com.orange.myabstract.domain.BitsPerSample;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the BitsPerSample entity.
 */
@SuppressWarnings("unused")
public interface BitsPerSampleRepository extends JpaRepository<BitsPerSample,Long> {

}
