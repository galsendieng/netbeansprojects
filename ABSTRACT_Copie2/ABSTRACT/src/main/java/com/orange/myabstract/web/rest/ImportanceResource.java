package com.orange.myabstract.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.orange.myabstract.service.ImportanceService;
import com.orange.myabstract.web.rest.util.HeaderUtil;
import com.orange.myabstract.service.dto.ImportanceDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Importance.
 */
@RestController
@RequestMapping("/api")
public class ImportanceResource {

    private final Logger log = LoggerFactory.getLogger(ImportanceResource.class);
        
    @Inject
    private ImportanceService importanceService;

    /**
     * POST  /importances : Create a new importance.
     *
     * @param importanceDTO the importanceDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new importanceDTO, or with status 400 (Bad Request) if the importance has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/importances")
    @Timed
    public ResponseEntity<ImportanceDTO> createImportance(@Valid @RequestBody ImportanceDTO importanceDTO) throws URISyntaxException {
        log.debug("REST request to save Importance : {}", importanceDTO);
        if (importanceDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("importance", "idexists", "A new importance cannot already have an ID")).body(null);
        }
        ImportanceDTO result = importanceService.save(importanceDTO);
        return ResponseEntity.created(new URI("/api/importances/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("importance", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /importances : Updates an existing importance.
     *
     * @param importanceDTO the importanceDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated importanceDTO,
     * or with status 400 (Bad Request) if the importanceDTO is not valid,
     * or with status 500 (Internal Server Error) if the importanceDTO couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/importances")
    @Timed
    public ResponseEntity<ImportanceDTO> updateImportance(@Valid @RequestBody ImportanceDTO importanceDTO) throws URISyntaxException {
        log.debug("REST request to update Importance : {}", importanceDTO);
        if (importanceDTO.getId() == null) {
            return createImportance(importanceDTO);
        }
        ImportanceDTO result = importanceService.save(importanceDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("importance", importanceDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /importances : get all the importances.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of importances in body
     */
    @GetMapping("/importances")
    @Timed
    public List<ImportanceDTO> getAllImportances() {
        log.debug("REST request to get all Importances");
        return importanceService.findAll();
    }

    /**
     * GET  /importances/:id : get the "id" importance.
     *
     * @param id the id of the importanceDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the importanceDTO, or with status 404 (Not Found)
     */
    @GetMapping("/importances/{id}")
    @Timed
    public ResponseEntity<ImportanceDTO> getImportance(@PathVariable Long id) {
        log.debug("REST request to get Importance : {}", id);
        ImportanceDTO importanceDTO = importanceService.findOne(id);
        return Optional.ofNullable(importanceDTO)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /importances/:id : delete the "id" importance.
     *
     * @param id the id of the importanceDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/importances/{id}")
    @Timed
    public ResponseEntity<Void> deleteImportance(@PathVariable Long id) {
        log.debug("REST request to delete Importance : {}", id);
        importanceService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("importance", id.toString())).build();
    }

    /**
     * SEARCH  /_search/importances?query=:query : search for the importance corresponding
     * to the query.
     *
     * @param query the query of the importance search 
     * @return the result of the search
     */
    @GetMapping("/_search/importances")
    @Timed
    public List<ImportanceDTO> searchImportances(@RequestParam String query) {
        log.debug("REST request to search Importances for query {}", query);
        return importanceService.search(query);
    }


}
