package com.orange.myabstract.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.orange.myabstract.service.FourCCService;
import com.orange.myabstract.web.rest.util.HeaderUtil;
import com.orange.myabstract.web.rest.util.PaginationUtil;
import com.orange.myabstract.service.dto.FourCCDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing FourCC.
 */
@RestController
@RequestMapping("/api")
public class FourCCResource {

    private final Logger log = LoggerFactory.getLogger(FourCCResource.class);
        
    @Inject
    private FourCCService fourCCService;

    /**
     * POST  /four-ccs : Create a new fourCC.
     *
     * @param fourCCDTO the fourCCDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new fourCCDTO, or with status 400 (Bad Request) if the fourCC has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/four-ccs")
    @Timed
    public ResponseEntity<FourCCDTO> createFourCC(@RequestBody FourCCDTO fourCCDTO) throws URISyntaxException {
        log.debug("REST request to save FourCC : {}", fourCCDTO);
        if (fourCCDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("fourCC", "idexists", "A new fourCC cannot already have an ID")).body(null);
        }
        FourCCDTO result = fourCCService.save(fourCCDTO);
        return ResponseEntity.created(new URI("/api/four-ccs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("fourCC", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /four-ccs : Updates an existing fourCC.
     *
     * @param fourCCDTO the fourCCDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated fourCCDTO,
     * or with status 400 (Bad Request) if the fourCCDTO is not valid,
     * or with status 500 (Internal Server Error) if the fourCCDTO couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/four-ccs")
    @Timed
    public ResponseEntity<FourCCDTO> updateFourCC(@RequestBody FourCCDTO fourCCDTO) throws URISyntaxException {
        log.debug("REST request to update FourCC : {}", fourCCDTO);
        if (fourCCDTO.getId() == null) {
            return createFourCC(fourCCDTO);
        }
        FourCCDTO result = fourCCService.save(fourCCDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("fourCC", fourCCDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /four-ccs : get all the fourCCS.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of fourCCS in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @GetMapping("/four-ccs")
    @Timed
    public ResponseEntity<List<FourCCDTO>> getAllFourCCS(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of FourCCS");
        Page<FourCCDTO> page = fourCCService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/four-ccs");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /four-ccs/:id : get the "id" fourCC.
     *
     * @param id the id of the fourCCDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the fourCCDTO, or with status 404 (Not Found)
     */
    @GetMapping("/four-ccs/{id}")
    @Timed
    public ResponseEntity<FourCCDTO> getFourCC(@PathVariable Long id) {
        log.debug("REST request to get FourCC : {}", id);
        FourCCDTO fourCCDTO = fourCCService.findOne(id);
        return Optional.ofNullable(fourCCDTO)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /four-ccs/:id : delete the "id" fourCC.
     *
     * @param id the id of the fourCCDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/four-ccs/{id}")
    @Timed
    public ResponseEntity<Void> deleteFourCC(@PathVariable Long id) {
        log.debug("REST request to delete FourCC : {}", id);
        fourCCService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("fourCC", id.toString())).build();
    }

    /**
     * SEARCH  /_search/four-ccs?query=:query : search for the fourCC corresponding
     * to the query.
     *
     * @param query the query of the fourCC search 
     * @param pageable the pagination information
     * @return the result of the search
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @GetMapping("/_search/four-ccs")
    @Timed
    public ResponseEntity<List<FourCCDTO>> searchFourCCS(@RequestParam String query, Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to search for a page of FourCCS for query {}", query);
        Page<FourCCDTO> page = fourCCService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/four-ccs");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
