package com.orange.myabstract.repository;

import com.orange.myabstract.domain.DVRWindowLength;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the DVRWindowLength entity.
 */
@SuppressWarnings("unused")
public interface DVRWindowLengthRepository extends JpaRepository<DVRWindowLength,Long> {

}
