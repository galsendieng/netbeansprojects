package com.orange.myabstract.service;

import com.orange.myabstract.service.dto.MaxHeightDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.LinkedList;
import java.util.List;

/**
 * Service Interface for managing MaxHeight.
 */
public interface MaxHeightService {

    /**
     * Save a maxHeight.
     *
     * @param maxHeightDTO the entity to save
     * @return the persisted entity
     */
    MaxHeightDTO save(MaxHeightDTO maxHeightDTO);

    /**
     *  Get all the maxHeights.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<MaxHeightDTO> findAll(Pageable pageable);

    /**
     *  Get the "id" maxHeight.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    MaxHeightDTO findOne(Long id);

    /**
     *  Delete the "id" maxHeight.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Search for the maxHeight corresponding to the query.
     *
     *  @param query the query of the search
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<MaxHeightDTO> search(String query, Pageable pageable);
}
