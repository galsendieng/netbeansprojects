package com.orange.myabstract.service;

import com.orange.myabstract.service.dto.MaxWidthQltDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.LinkedList;
import java.util.List;

/**
 * Service Interface for managing MaxWidthQlt.
 */
public interface MaxWidthQltService {

    /**
     * Save a maxWidthQlt.
     *
     * @param maxWidthQltDTO the entity to save
     * @return the persisted entity
     */
    MaxWidthQltDTO save(MaxWidthQltDTO maxWidthQltDTO);

    /**
     *  Get all the maxWidthQlts.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<MaxWidthQltDTO> findAll(Pageable pageable);

    /**
     *  Get the "id" maxWidthQlt.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    MaxWidthQltDTO findOne(Long id);

    /**
     *  Delete the "id" maxWidthQlt.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Search for the maxWidthQlt corresponding to the query.
     *
     *  @param query the query of the search
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<MaxWidthQltDTO> search(String query, Pageable pageable);
}
