package com.orange.myabstract.service.impl;

import com.orange.myabstract.service.DisplayWidthService;
import com.orange.myabstract.domain.DisplayWidth;
import com.orange.myabstract.repository.DisplayWidthRepository;
import com.orange.myabstract.repository.search.DisplayWidthSearchRepository;
import com.orange.myabstract.service.dto.DisplayWidthDTO;
import com.orange.myabstract.service.mapper.DisplayWidthMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing DisplayWidth.
 */
@Service
@Transactional
public class DisplayWidthServiceImpl implements DisplayWidthService{

    private final Logger log = LoggerFactory.getLogger(DisplayWidthServiceImpl.class);
    
    @Inject
    private DisplayWidthRepository displayWidthRepository;

    @Inject
    private DisplayWidthMapper displayWidthMapper;

    @Inject
    private DisplayWidthSearchRepository displayWidthSearchRepository;

    /**
     * Save a displayWidth.
     *
     * @param displayWidthDTO the entity to save
     * @return the persisted entity
     */
    public DisplayWidthDTO save(DisplayWidthDTO displayWidthDTO) {
        log.debug("Request to save DisplayWidth : {}", displayWidthDTO);
        DisplayWidth displayWidth = displayWidthMapper.displayWidthDTOToDisplayWidth(displayWidthDTO);
        displayWidth = displayWidthRepository.save(displayWidth);
        DisplayWidthDTO result = displayWidthMapper.displayWidthToDisplayWidthDTO(displayWidth);
        displayWidthSearchRepository.save(displayWidth);
        return result;
    }

    /**
     *  Get all the displayWidths.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true) 
    public Page<DisplayWidthDTO> findAll(Pageable pageable) {
        log.debug("Request to get all DisplayWidths");
        Page<DisplayWidth> result = displayWidthRepository.findAll(pageable);
        return result.map(displayWidth -> displayWidthMapper.displayWidthToDisplayWidthDTO(displayWidth));
    }

    /**
     *  Get one displayWidth by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true) 
    public DisplayWidthDTO findOne(Long id) {
        log.debug("Request to get DisplayWidth : {}", id);
        DisplayWidth displayWidth = displayWidthRepository.findOne(id);
        DisplayWidthDTO displayWidthDTO = displayWidthMapper.displayWidthToDisplayWidthDTO(displayWidth);
        return displayWidthDTO;
    }

    /**
     *  Delete the  displayWidth by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete DisplayWidth : {}", id);
        displayWidthRepository.delete(id);
        displayWidthSearchRepository.delete(id);
    }

    /**
     * Search for the displayWidth corresponding to the query.
     *
     *  @param query the query of the search
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<DisplayWidthDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of DisplayWidths for query {}", query);
        Page<DisplayWidth> result = displayWidthSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(displayWidth -> displayWidthMapper.displayWidthToDisplayWidthDTO(displayWidth));
    }
}
