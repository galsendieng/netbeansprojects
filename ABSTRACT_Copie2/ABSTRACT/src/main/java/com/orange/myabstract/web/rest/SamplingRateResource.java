package com.orange.myabstract.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.orange.myabstract.service.SamplingRateService;
import com.orange.myabstract.web.rest.util.HeaderUtil;
import com.orange.myabstract.web.rest.util.PaginationUtil;
import com.orange.myabstract.service.dto.SamplingRateDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing SamplingRate.
 */
@RestController
@RequestMapping("/api")
public class SamplingRateResource {

    private final Logger log = LoggerFactory.getLogger(SamplingRateResource.class);
        
    @Inject
    private SamplingRateService samplingRateService;

    /**
     * POST  /sampling-rates : Create a new samplingRate.
     *
     * @param samplingRateDTO the samplingRateDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new samplingRateDTO, or with status 400 (Bad Request) if the samplingRate has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/sampling-rates")
    @Timed
    public ResponseEntity<SamplingRateDTO> createSamplingRate(@RequestBody SamplingRateDTO samplingRateDTO) throws URISyntaxException {
        log.debug("REST request to save SamplingRate : {}", samplingRateDTO);
        if (samplingRateDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("samplingRate", "idexists", "A new samplingRate cannot already have an ID")).body(null);
        }
        SamplingRateDTO result = samplingRateService.save(samplingRateDTO);
        return ResponseEntity.created(new URI("/api/sampling-rates/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("samplingRate", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /sampling-rates : Updates an existing samplingRate.
     *
     * @param samplingRateDTO the samplingRateDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated samplingRateDTO,
     * or with status 400 (Bad Request) if the samplingRateDTO is not valid,
     * or with status 500 (Internal Server Error) if the samplingRateDTO couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/sampling-rates")
    @Timed
    public ResponseEntity<SamplingRateDTO> updateSamplingRate(@RequestBody SamplingRateDTO samplingRateDTO) throws URISyntaxException {
        log.debug("REST request to update SamplingRate : {}", samplingRateDTO);
        if (samplingRateDTO.getId() == null) {
            return createSamplingRate(samplingRateDTO);
        }
        SamplingRateDTO result = samplingRateService.save(samplingRateDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("samplingRate", samplingRateDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /sampling-rates : get all the samplingRates.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of samplingRates in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @GetMapping("/sampling-rates")
    @Timed
    public ResponseEntity<List<SamplingRateDTO>> getAllSamplingRates(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of SamplingRates");
        Page<SamplingRateDTO> page = samplingRateService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/sampling-rates");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /sampling-rates/:id : get the "id" samplingRate.
     *
     * @param id the id of the samplingRateDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the samplingRateDTO, or with status 404 (Not Found)
     */
    @GetMapping("/sampling-rates/{id}")
    @Timed
    public ResponseEntity<SamplingRateDTO> getSamplingRate(@PathVariable Long id) {
        log.debug("REST request to get SamplingRate : {}", id);
        SamplingRateDTO samplingRateDTO = samplingRateService.findOne(id);
        return Optional.ofNullable(samplingRateDTO)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /sampling-rates/:id : delete the "id" samplingRate.
     *
     * @param id the id of the samplingRateDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/sampling-rates/{id}")
    @Timed
    public ResponseEntity<Void> deleteSamplingRate(@PathVariable Long id) {
        log.debug("REST request to delete SamplingRate : {}", id);
        samplingRateService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("samplingRate", id.toString())).build();
    }

    /**
     * SEARCH  /_search/sampling-rates?query=:query : search for the samplingRate corresponding
     * to the query.
     *
     * @param query the query of the samplingRate search 
     * @param pageable the pagination information
     * @return the result of the search
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @GetMapping("/_search/sampling-rates")
    @Timed
    public ResponseEntity<List<SamplingRateDTO>> searchSamplingRates(@RequestParam String query, Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to search for a page of SamplingRates for query {}", query);
        Page<SamplingRateDTO> page = samplingRateService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/sampling-rates");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
