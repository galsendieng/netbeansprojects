package com.orange.myabstract.service;

import com.orange.myabstract.service.dto.AudioTagDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.LinkedList;
import java.util.List;

/**
 * Service Interface for managing AudioTag.
 */
public interface AudioTagService {

    /**
     * Save a audioTag.
     *
     * @param audioTagDTO the entity to save
     * @return the persisted entity
     */
    AudioTagDTO save(AudioTagDTO audioTagDTO);

    /**
     *  Get all the audioTags.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<AudioTagDTO> findAll(Pageable pageable);

    /**
     *  Get the "id" audioTag.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    AudioTagDTO findOne(Long id);

    /**
     *  Delete the "id" audioTag.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Search for the audioTag corresponding to the query.
     *
     *  @param query the query of the search
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<AudioTagDTO> search(String query, Pageable pageable);
}
