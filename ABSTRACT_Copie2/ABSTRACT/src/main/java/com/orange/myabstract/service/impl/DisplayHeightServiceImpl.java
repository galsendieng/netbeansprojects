package com.orange.myabstract.service.impl;

import com.orange.myabstract.service.DisplayHeightService;
import com.orange.myabstract.domain.DisplayHeight;
import com.orange.myabstract.repository.DisplayHeightRepository;
import com.orange.myabstract.repository.search.DisplayHeightSearchRepository;
import com.orange.myabstract.service.dto.DisplayHeightDTO;
import com.orange.myabstract.service.mapper.DisplayHeightMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing DisplayHeight.
 */
@Service
@Transactional
public class DisplayHeightServiceImpl implements DisplayHeightService{

    private final Logger log = LoggerFactory.getLogger(DisplayHeightServiceImpl.class);
    
    @Inject
    private DisplayHeightRepository displayHeightRepository;

    @Inject
    private DisplayHeightMapper displayHeightMapper;

    @Inject
    private DisplayHeightSearchRepository displayHeightSearchRepository;

    /**
     * Save a displayHeight.
     *
     * @param displayHeightDTO the entity to save
     * @return the persisted entity
     */
    public DisplayHeightDTO save(DisplayHeightDTO displayHeightDTO) {
        log.debug("Request to save DisplayHeight : {}", displayHeightDTO);
        DisplayHeight displayHeight = displayHeightMapper.displayHeightDTOToDisplayHeight(displayHeightDTO);
        displayHeight = displayHeightRepository.save(displayHeight);
        DisplayHeightDTO result = displayHeightMapper.displayHeightToDisplayHeightDTO(displayHeight);
        displayHeightSearchRepository.save(displayHeight);
        return result;
    }

    /**
     *  Get all the displayHeights.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true) 
    public Page<DisplayHeightDTO> findAll(Pageable pageable) {
        log.debug("Request to get all DisplayHeights");
        Page<DisplayHeight> result = displayHeightRepository.findAll(pageable);
        return result.map(displayHeight -> displayHeightMapper.displayHeightToDisplayHeightDTO(displayHeight));
    }

    /**
     *  Get one displayHeight by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true) 
    public DisplayHeightDTO findOne(Long id) {
        log.debug("Request to get DisplayHeight : {}", id);
        DisplayHeight displayHeight = displayHeightRepository.findOne(id);
        DisplayHeightDTO displayHeightDTO = displayHeightMapper.displayHeightToDisplayHeightDTO(displayHeight);
        return displayHeightDTO;
    }

    /**
     *  Delete the  displayHeight by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete DisplayHeight : {}", id);
        displayHeightRepository.delete(id);
        displayHeightSearchRepository.delete(id);
    }

    /**
     * Search for the displayHeight corresponding to the query.
     *
     *  @param query the query of the search
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<DisplayHeightDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of DisplayHeights for query {}", query);
        Page<DisplayHeight> result = displayHeightSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(displayHeight -> displayHeightMapper.displayHeightToDisplayHeightDTO(displayHeight));
    }
}
