package com.orange.myabstract.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.orange.myabstract.service.MonitoringService;
import com.orange.myabstract.web.rest.util.HeaderUtil;
import com.orange.myabstract.web.rest.util.PaginationUtil;
import com.orange.myabstract.service.dto.MonitoringDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Monitoring.
 */
@RestController
@RequestMapping("/api")
public class MonitoringResource {

    private final Logger log = LoggerFactory.getLogger(MonitoringResource.class);
        
    @Inject
    private MonitoringService monitoringService;

    /**
     * POST  /monitorings : Create a new monitoring.
     *
     * @param monitoringDTO the monitoringDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new monitoringDTO, or with status 400 (Bad Request) if the monitoring has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/monitorings")
    @Timed
    public ResponseEntity<MonitoringDTO> createMonitoring(@RequestBody MonitoringDTO monitoringDTO) throws URISyntaxException {
        log.debug("REST request to save Monitoring : {}", monitoringDTO);
        if (monitoringDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("monitoring", "idexists", "A new monitoring cannot already have an ID")).body(null);
        }
        MonitoringDTO result = monitoringService.save(monitoringDTO);
        return ResponseEntity.created(new URI("/api/monitorings/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("monitoring", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /monitorings : Updates an existing monitoring.
     *
     * @param monitoringDTO the monitoringDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated monitoringDTO,
     * or with status 400 (Bad Request) if the monitoringDTO is not valid,
     * or with status 500 (Internal Server Error) if the monitoringDTO couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/monitorings")
    @Timed
    public ResponseEntity<MonitoringDTO> updateMonitoring(@RequestBody MonitoringDTO monitoringDTO) throws URISyntaxException {
        log.debug("REST request to update Monitoring : {}", monitoringDTO);
        if (monitoringDTO.getId() == null) {
            return createMonitoring(monitoringDTO);
        }
        MonitoringDTO result = monitoringService.save(monitoringDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("monitoring", monitoringDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /monitorings : get all the monitorings.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of monitorings in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @GetMapping("/monitorings")
    @Timed
    public ResponseEntity<List<MonitoringDTO>> getAllMonitorings(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of Monitorings");
        Page<MonitoringDTO> page = monitoringService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/monitorings");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /monitorings/:id : get the "id" monitoring.
     *
     * @param id the id of the monitoringDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the monitoringDTO, or with status 404 (Not Found)
     */
    @GetMapping("/monitorings/{id}")
    @Timed
    public ResponseEntity<MonitoringDTO> getMonitoring(@PathVariable Long id) {
        log.debug("REST request to get Monitoring : {}", id);
        MonitoringDTO monitoringDTO = monitoringService.findOne(id);
        return Optional.ofNullable(monitoringDTO)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /monitorings/:id : delete the "id" monitoring.
     *
     * @param id the id of the monitoringDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/monitorings/{id}")
    @Timed
    public ResponseEntity<Void> deleteMonitoring(@PathVariable Long id) {
        log.debug("REST request to delete Monitoring : {}", id);
        monitoringService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("monitoring", id.toString())).build();
    }

    /**
     * SEARCH  /_search/monitorings?query=:query : search for the monitoring corresponding
     * to the query.
     *
     * @param query the query of the monitoring search 
     * @param pageable the pagination information
     * @return the result of the search
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @GetMapping("/_search/monitorings")
    @Timed
    public ResponseEntity<List<MonitoringDTO>> searchMonitorings(@RequestParam String query, Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to search for a page of Monitorings for query {}", query);
        Page<MonitoringDTO> page = monitoringService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/monitorings");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
