package com.orange.myabstract.repository.search;

import com.orange.myabstract.domain.Importance;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the Importance entity.
 */
public interface ImportanceSearchRepository extends ElasticsearchRepository<Importance, Long> {
}
