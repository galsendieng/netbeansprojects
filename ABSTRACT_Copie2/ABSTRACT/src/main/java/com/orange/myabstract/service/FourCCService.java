package com.orange.myabstract.service;

import com.orange.myabstract.service.dto.FourCCDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.LinkedList;
import java.util.List;

/**
 * Service Interface for managing FourCC.
 */
public interface FourCCService {

    /**
     * Save a fourCC.
     *
     * @param fourCCDTO the entity to save
     * @return the persisted entity
     */
    FourCCDTO save(FourCCDTO fourCCDTO);

    /**
     *  Get all the fourCCS.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<FourCCDTO> findAll(Pageable pageable);

    /**
     *  Get the "id" fourCC.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    FourCCDTO findOne(Long id);

    /**
     *  Delete the "id" fourCC.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Search for the fourCC corresponding to the query.
     *
     *  @param query the query of the search
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<FourCCDTO> search(String query, Pageable pageable);
}
