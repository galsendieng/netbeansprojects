package com.orange.myabstract.service.impl;

import com.orange.myabstract.service.MaxWidthService;
import com.orange.myabstract.domain.MaxWidth;
import com.orange.myabstract.repository.MaxWidthRepository;
import com.orange.myabstract.repository.search.MaxWidthSearchRepository;
import com.orange.myabstract.service.dto.MaxWidthDTO;
import com.orange.myabstract.service.mapper.MaxWidthMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing MaxWidth.
 */
@Service
@Transactional
public class MaxWidthServiceImpl implements MaxWidthService{

    private final Logger log = LoggerFactory.getLogger(MaxWidthServiceImpl.class);
    
    @Inject
    private MaxWidthRepository maxWidthRepository;

    @Inject
    private MaxWidthMapper maxWidthMapper;

    @Inject
    private MaxWidthSearchRepository maxWidthSearchRepository;

    /**
     * Save a maxWidth.
     *
     * @param maxWidthDTO the entity to save
     * @return the persisted entity
     */
    public MaxWidthDTO save(MaxWidthDTO maxWidthDTO) {
        log.debug("Request to save MaxWidth : {}", maxWidthDTO);
        MaxWidth maxWidth = maxWidthMapper.maxWidthDTOToMaxWidth(maxWidthDTO);
        maxWidth = maxWidthRepository.save(maxWidth);
        MaxWidthDTO result = maxWidthMapper.maxWidthToMaxWidthDTO(maxWidth);
        maxWidthSearchRepository.save(maxWidth);
        return result;
    }

    /**
     *  Get all the maxWidths.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true) 
    public Page<MaxWidthDTO> findAll(Pageable pageable) {
        log.debug("Request to get all MaxWidths");
        Page<MaxWidth> result = maxWidthRepository.findAll(pageable);
        return result.map(maxWidth -> maxWidthMapper.maxWidthToMaxWidthDTO(maxWidth));
    }

    /**
     *  Get one maxWidth by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true) 
    public MaxWidthDTO findOne(Long id) {
        log.debug("Request to get MaxWidth : {}", id);
        MaxWidth maxWidth = maxWidthRepository.findOne(id);
        MaxWidthDTO maxWidthDTO = maxWidthMapper.maxWidthToMaxWidthDTO(maxWidth);
        return maxWidthDTO;
    }

    /**
     *  Delete the  maxWidth by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete MaxWidth : {}", id);
        maxWidthRepository.delete(id);
        maxWidthSearchRepository.delete(id);
    }

    /**
     * Search for the maxWidth corresponding to the query.
     *
     *  @param query the query of the search
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<MaxWidthDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of MaxWidths for query {}", query);
        Page<MaxWidth> result = maxWidthSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(maxWidth -> maxWidthMapper.maxWidthToMaxWidthDTO(maxWidth));
    }
}
