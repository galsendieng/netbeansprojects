package com.orange.myabstract.service.mapper;

import com.orange.myabstract.domain.*;
import com.orange.myabstract.service.dto.MaxHeightQltDTO;

import org.mapstruct.*;
import java.util.List;

/**
 * Mapper for the entity MaxHeightQlt and its DTO MaxHeightQltDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface MaxHeightQltMapper {

    MaxHeightQltDTO maxHeightQltToMaxHeightQltDTO(MaxHeightQlt maxHeightQlt);

    List<MaxHeightQltDTO> maxHeightQltsToMaxHeightQltDTOs(List<MaxHeightQlt> maxHeightQlts);

    MaxHeightQlt maxHeightQltDTOToMaxHeightQlt(MaxHeightQltDTO maxHeightQltDTO);

    List<MaxHeightQlt> maxHeightQltDTOsToMaxHeightQlts(List<MaxHeightQltDTO> maxHeightQltDTOs);
}
