package com.orange.myabstract.service.mapper;

import com.orange.myabstract.domain.*;
import com.orange.myabstract.service.dto.MinorVersionDTO;

import org.mapstruct.*;
import java.util.List;

/**
 * Mapper for the entity MinorVersion and its DTO MinorVersionDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface MinorVersionMapper {

    MinorVersionDTO minorVersionToMinorVersionDTO(MinorVersion minorVersion);

    List<MinorVersionDTO> minorVersionsToMinorVersionDTOs(List<MinorVersion> minorVersions);

    MinorVersion minorVersionDTOToMinorVersion(MinorVersionDTO minorVersionDTO);

    List<MinorVersion> minorVersionDTOsToMinorVersions(List<MinorVersionDTO> minorVersionDTOs);
}
