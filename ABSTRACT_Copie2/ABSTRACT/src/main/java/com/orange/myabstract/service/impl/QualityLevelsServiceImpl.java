package com.orange.myabstract.service.impl;

import com.orange.myabstract.service.QualityLevelsService;
import com.orange.myabstract.domain.QualityLevels;
import com.orange.myabstract.repository.QualityLevelsRepository;
import com.orange.myabstract.repository.search.QualityLevelsSearchRepository;
import com.orange.myabstract.service.dto.QualityLevelsDTO;
import com.orange.myabstract.service.mapper.QualityLevelsMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing QualityLevels.
 */
@Service
@Transactional
public class QualityLevelsServiceImpl implements QualityLevelsService{

    private final Logger log = LoggerFactory.getLogger(QualityLevelsServiceImpl.class);
    
    @Inject
    private QualityLevelsRepository qualityLevelsRepository;

    @Inject
    private QualityLevelsMapper qualityLevelsMapper;

    @Inject
    private QualityLevelsSearchRepository qualityLevelsSearchRepository;

    /**
     * Save a qualityLevels.
     *
     * @param qualityLevelsDTO the entity to save
     * @return the persisted entity
     */
    public QualityLevelsDTO save(QualityLevelsDTO qualityLevelsDTO) {
        log.debug("Request to save QualityLevels : {}", qualityLevelsDTO);
        QualityLevels qualityLevels = qualityLevelsMapper.qualityLevelsDTOToQualityLevels(qualityLevelsDTO);
        qualityLevels = qualityLevelsRepository.save(qualityLevels);
        QualityLevelsDTO result = qualityLevelsMapper.qualityLevelsToQualityLevelsDTO(qualityLevels);
        qualityLevelsSearchRepository.save(qualityLevels);
        return result;
    }

    /**
     *  Get all the qualityLevels.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true) 
    public Page<QualityLevelsDTO> findAll(Pageable pageable) {
        log.debug("Request to get all QualityLevels");
        Page<QualityLevels> result = qualityLevelsRepository.findAll(pageable);
        return result.map(qualityLevels -> qualityLevelsMapper.qualityLevelsToQualityLevelsDTO(qualityLevels));
    }

    /**
     *  Get one qualityLevels by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true) 
    public QualityLevelsDTO findOne(Long id) {
        log.debug("Request to get QualityLevels : {}", id);
        QualityLevels qualityLevels = qualityLevelsRepository.findOne(id);
        QualityLevelsDTO qualityLevelsDTO = qualityLevelsMapper.qualityLevelsToQualityLevelsDTO(qualityLevels);
        return qualityLevelsDTO;
    }

    /**
     *  Delete the  qualityLevels by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete QualityLevels : {}", id);
        qualityLevelsRepository.delete(id);
        qualityLevelsSearchRepository.delete(id);
    }

    /**
     * Search for the qualityLevels corresponding to the query.
     *
     *  @param query the query of the search
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<QualityLevelsDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of QualityLevels for query {}", query);
        Page<QualityLevels> result = qualityLevelsSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(qualityLevels -> qualityLevelsMapper.qualityLevelsToQualityLevelsDTO(qualityLevels));
    }
}
