package com.orange.myabstract.repository;

import com.orange.myabstract.domain.ManifestData;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the ManifestData entity.
 */
@SuppressWarnings("unused")
public interface ManifestDataRepository extends JpaRepository<ManifestData,Long> {

}
