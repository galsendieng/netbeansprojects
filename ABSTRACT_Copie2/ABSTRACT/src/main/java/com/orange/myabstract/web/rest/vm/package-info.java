/**
 * View Models used by Spring MVC REST controllers.
 */
package com.orange.myabstract.web.rest.vm;
