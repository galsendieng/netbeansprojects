package com.orange.myabstract.service.mapper;

import com.orange.myabstract.domain.*;
import com.orange.myabstract.service.dto.LookAheadFragmentCountDTO;

import org.mapstruct.*;
import java.util.List;

/**
 * Mapper for the entity LookAheadFragmentCount and its DTO LookAheadFragmentCountDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface LookAheadFragmentCountMapper {

    LookAheadFragmentCountDTO lookAheadFragmentCountToLookAheadFragmentCountDTO(LookAheadFragmentCount lookAheadFragmentCount);

    List<LookAheadFragmentCountDTO> lookAheadFragmentCountsToLookAheadFragmentCountDTOs(List<LookAheadFragmentCount> lookAheadFragmentCounts);

    LookAheadFragmentCount lookAheadFragmentCountDTOToLookAheadFragmentCount(LookAheadFragmentCountDTO lookAheadFragmentCountDTO);

    List<LookAheadFragmentCount> lookAheadFragmentCountDTOsToLookAheadFragmentCounts(List<LookAheadFragmentCountDTO> lookAheadFragmentCountDTOs);
}
