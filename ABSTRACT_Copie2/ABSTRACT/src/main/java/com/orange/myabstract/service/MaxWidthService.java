package com.orange.myabstract.service;

import com.orange.myabstract.service.dto.MaxWidthDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.LinkedList;
import java.util.List;

/**
 * Service Interface for managing MaxWidth.
 */
public interface MaxWidthService {

    /**
     * Save a maxWidth.
     *
     * @param maxWidthDTO the entity to save
     * @return the persisted entity
     */
    MaxWidthDTO save(MaxWidthDTO maxWidthDTO);

    /**
     *  Get all the maxWidths.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<MaxWidthDTO> findAll(Pageable pageable);

    /**
     *  Get the "id" maxWidth.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    MaxWidthDTO findOne(Long id);

    /**
     *  Delete the "id" maxWidth.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Search for the maxWidth corresponding to the query.
     *
     *  @param query the query of the search
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<MaxWidthDTO> search(String query, Pageable pageable);
}
