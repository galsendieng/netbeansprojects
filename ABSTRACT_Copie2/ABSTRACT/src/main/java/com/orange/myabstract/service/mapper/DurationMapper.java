package com.orange.myabstract.service.mapper;

import com.orange.myabstract.domain.*;
import com.orange.myabstract.service.dto.DurationDTO;

import org.mapstruct.*;
import java.util.List;

/**
 * Mapper for the entity Duration and its DTO DurationDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface DurationMapper {

    DurationDTO durationToDurationDTO(Duration duration);

    List<DurationDTO> durationsToDurationDTOs(List<Duration> durations);

    Duration durationDTOToDuration(DurationDTO durationDTO);

    List<Duration> durationDTOsToDurations(List<DurationDTO> durationDTOs);
}
