package com.orange.myabstract.repository;

import com.orange.myabstract.domain.Pod;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the Pod entity.
 */
@SuppressWarnings("unused")
public interface PodRepository extends JpaRepository<Pod,Long> {

}
