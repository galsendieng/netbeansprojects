package com.orange.myabstract.repository.search;

import com.orange.myabstract.domain.CorruptManifest;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the CorruptManifest entity.
 */
public interface CorruptManifestSearchRepository extends ElasticsearchRepository<CorruptManifest, Long> {
}
