package com.orange.myabstract.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Playing.
 */
@Entity
@Table(name = "playing")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "playing")
public class Playing implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Column(name = "link", nullable = false)
    private String link;

    @Lob
    @Column(name = "file_test")
    private byte[] fileTest;

    @Column(name = "file_test_content_type")
    private String fileTestContentType;

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "playing_listchannels",
               joinColumns = @JoinColumn(name="playings_id", referencedColumnName="ID"),
               inverseJoinColumns = @JoinColumn(name="listchannels_id", referencedColumnName="ID"))
    private Set<Channels> listchannels = new HashSet<>();

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "playing_listcommands",
               joinColumns = @JoinColumn(name="playings_id", referencedColumnName="ID"),
               inverseJoinColumns = @JoinColumn(name="listcommands_id", referencedColumnName="ID"))
    private Set<Commande> listcommands = new HashSet<>();

    @ManyToOne
    private Mode theMode;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLink() {
        return link;
    }

    public Playing link(String link) {
        this.link = link;
        return this;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public byte[] getFileTest() {
        return fileTest;
    }

    public Playing fileTest(byte[] fileTest) {
        this.fileTest = fileTest;
        return this;
    }

    public void setFileTest(byte[] fileTest) {
        this.fileTest = fileTest;
    }

    public String getFileTestContentType() {
        return fileTestContentType;
    }

    public Playing fileTestContentType(String fileTestContentType) {
        this.fileTestContentType = fileTestContentType;
        return this;
    }

    public void setFileTestContentType(String fileTestContentType) {
        this.fileTestContentType = fileTestContentType;
    }

    public Set<Channels> getListchannels() {
        return listchannels;
    }

    public Playing listchannels(Set<Channels> channels) {
        this.listchannels = channels;
        return this;
    }

    public Playing addListchannels(Channels channels) {
        listchannels.add(channels);
        channels.getPlays().add(this);
        return this;
    }

    public Playing removeListchannels(Channels channels) {
        listchannels.remove(channels);
        channels.getPlays().remove(this);
        return this;
    }

    public void setListchannels(Set<Channels> channels) {
        this.listchannels = channels;
    }

    public Set<Commande> getListcommands() {
        return listcommands;
    }

    public Playing listcommands(Set<Commande> commandes) {
        this.listcommands = commandes;
        return this;
    }

    public Playing addListcommands(Commande commande) {
        listcommands.add(commande);
        commande.getCmdPlays().add(this);
        return this;
    }

    public Playing removeListcommands(Commande commande) {
        listcommands.remove(commande);
        commande.getCmdPlays().remove(this);
        return this;
    }

    public void setListcommands(Set<Commande> commandes) {
        this.listcommands = commandes;
    }

    public Mode getTheMode() {
        return theMode;
    }

    public Playing theMode(Mode mode) {
        this.theMode = mode;
        return this;
    }

    public void setTheMode(Mode mode) {
        this.theMode = mode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Playing playing = (Playing) o;
        if(playing.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, playing.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Playing{" +
            "id=" + id +
            ", link='" + link + "'" +
            ", fileTest='" + fileTest + "'" +
            ", fileTestContentType='" + fileTestContentType + "'" +
            '}';
    }
}
