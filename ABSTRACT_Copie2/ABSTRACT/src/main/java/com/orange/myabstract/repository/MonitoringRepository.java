package com.orange.myabstract.repository;

import com.orange.myabstract.domain.Monitoring;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the Monitoring entity.
 */
@SuppressWarnings("unused")
public interface MonitoringRepository extends JpaRepository<Monitoring,Long> {

}
