package com.orange.myabstract.repository;

import com.orange.myabstract.domain.QualityLevels;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the QualityLevels entity.
 */
@SuppressWarnings("unused")
public interface QualityLevelsRepository extends JpaRepository<QualityLevels,Long> {

}
