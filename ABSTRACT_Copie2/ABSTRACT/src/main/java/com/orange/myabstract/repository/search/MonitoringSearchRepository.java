package com.orange.myabstract.repository.search;

import com.orange.myabstract.domain.Monitoring;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the Monitoring entity.
 */
public interface MonitoringSearchRepository extends ElasticsearchRepository<Monitoring, Long> {
}
