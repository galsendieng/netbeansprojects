package com.orange.myabstract.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

/**
 * A Monitoring.
 */
@Entity
@Table(name = "monitoring")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "monitoring")
public class Monitoring implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "attribut")
    private String attribut;

    @Column(name = "severite")
    private String severite;

    @Column(name = "importance")
    private String importance;

    @Column(name = "valeur_attendue")
    private String valeurAttendue;

    @Column(name = "valeur_obtenue")
    private String valeurObtenue;

    @Column(name = "date_sup")
    private ZonedDateTime dateSup;

    @Column(name = "message")
    private String message;

    @Column(name = "infos")
    private String infos;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAttribut() {
        return attribut;
    }

    public Monitoring attribut(String attribut) {
        this.attribut = attribut;
        return this;
    }

    public void setAttribut(String attribut) {
        this.attribut = attribut;
    }

    public String getSeverite() {
        return severite;
    }

    public Monitoring severite(String severite) {
        this.severite = severite;
        return this;
    }

    public void setSeverite(String severite) {
        this.severite = severite;
    }

    public String getImportance() {
        return importance;
    }

    public Monitoring importance(String importance) {
        this.importance = importance;
        return this;
    }

    public void setImportance(String importance) {
        this.importance = importance;
    }

    public String getValeurAttendue() {
        return valeurAttendue;
    }

    public Monitoring valeurAttendue(String valeurAttendue) {
        this.valeurAttendue = valeurAttendue;
        return this;
    }

    public void setValeurAttendue(String valeurAttendue) {
        this.valeurAttendue = valeurAttendue;
    }

    public String getValeurObtenue() {
        return valeurObtenue;
    }

    public Monitoring valeurObtenue(String valeurObtenue) {
        this.valeurObtenue = valeurObtenue;
        return this;
    }

    public void setValeurObtenue(String valeurObtenue) {
        this.valeurObtenue = valeurObtenue;
    }

    public ZonedDateTime getDateSup() {
        return dateSup;
    }

    public Monitoring dateSup(ZonedDateTime dateSup) {
        this.dateSup = dateSup;
        return this;
    }

    public void setDateSup(ZonedDateTime dateSup) {
        this.dateSup = dateSup;
    }

    public String getMessage() {
        return message;
    }

    public Monitoring message(String message) {
        this.message = message;
        return this;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getInfos() {
        return infos;
    }

    public Monitoring infos(String infos) {
        this.infos = infos;
        return this;
    }

    public void setInfos(String infos) {
        this.infos = infos;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Monitoring monitoring = (Monitoring) o;
        if(monitoring.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, monitoring.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Monitoring{" +
            "id=" + id +
            ", attribut='" + attribut + "'" +
            ", severite='" + severite + "'" +
            ", importance='" + importance + "'" +
            ", valeurAttendue='" + valeurAttendue + "'" +
            ", valeurObtenue='" + valeurObtenue + "'" +
            ", dateSup='" + dateSup + "'" +
            ", message='" + message + "'" +
            ", infos='" + infos + "'" +
            '}';
    }
}
