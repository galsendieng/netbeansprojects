package com.orange.myabstract.repository;

import com.orange.myabstract.domain.Chunks;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the Chunks entity.
 */
@SuppressWarnings("unused")
public interface ChunksRepository extends JpaRepository<Chunks,Long> {

}
