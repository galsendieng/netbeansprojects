package com.orange.myabstract.service.dto;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;


/**
 * A DTO for the Channels entity.
 */
public class ChannelsDTO implements Serializable {

    private Long id;

    @NotNull
    private String channel;

    private String description;


    private Set<StreamerDTO> streamers = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<StreamerDTO> getStreamers() {
        return streamers;
    }

    public void setStreamers(Set<StreamerDTO> streamers) {
        this.streamers = streamers;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ChannelsDTO channelsDTO = (ChannelsDTO) o;

        if ( ! Objects.equals(id, channelsDTO.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "ChannelsDTO{" +
            "id=" + id +
            ", channel='" + channel + "'" +
            ", description='" + description + "'" +
            '}';
    }
}
