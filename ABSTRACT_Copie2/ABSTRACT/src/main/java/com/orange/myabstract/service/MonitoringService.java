package com.orange.myabstract.service;

import com.orange.myabstract.service.dto.MonitoringDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.LinkedList;
import java.util.List;

/**
 * Service Interface for managing Monitoring.
 */
public interface MonitoringService {

    /**
     * Save a monitoring.
     *
     * @param monitoringDTO the entity to save
     * @return the persisted entity
     */
    MonitoringDTO save(MonitoringDTO monitoringDTO);

    /**
     *  Get all the monitorings.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<MonitoringDTO> findAll(Pageable pageable);

    /**
     *  Get the "id" monitoring.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    MonitoringDTO findOne(Long id);

    /**
     *  Delete the "id" monitoring.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Search for the monitoring corresponding to the query.
     *
     *  @param query the query of the search
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<MonitoringDTO> search(String query, Pageable pageable);
}
