package com.orange.myabstract.service.impl;

import com.orange.myabstract.service.FourCCService;
import com.orange.myabstract.domain.FourCC;
import com.orange.myabstract.repository.FourCCRepository;
import com.orange.myabstract.repository.search.FourCCSearchRepository;
import com.orange.myabstract.service.dto.FourCCDTO;
import com.orange.myabstract.service.mapper.FourCCMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing FourCC.
 */
@Service
@Transactional
public class FourCCServiceImpl implements FourCCService{

    private final Logger log = LoggerFactory.getLogger(FourCCServiceImpl.class);
    
    @Inject
    private FourCCRepository fourCCRepository;

    @Inject
    private FourCCMapper fourCCMapper;

    @Inject
    private FourCCSearchRepository fourCCSearchRepository;

    /**
     * Save a fourCC.
     *
     * @param fourCCDTO the entity to save
     * @return the persisted entity
     */
    public FourCCDTO save(FourCCDTO fourCCDTO) {
        log.debug("Request to save FourCC : {}", fourCCDTO);
        FourCC fourCC = fourCCMapper.fourCCDTOToFourCC(fourCCDTO);
        fourCC = fourCCRepository.save(fourCC);
        FourCCDTO result = fourCCMapper.fourCCToFourCCDTO(fourCC);
        fourCCSearchRepository.save(fourCC);
        return result;
    }

    /**
     *  Get all the fourCCS.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true) 
    public Page<FourCCDTO> findAll(Pageable pageable) {
        log.debug("Request to get all FourCCS");
        Page<FourCC> result = fourCCRepository.findAll(pageable);
        return result.map(fourCC -> fourCCMapper.fourCCToFourCCDTO(fourCC));
    }

    /**
     *  Get one fourCC by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true) 
    public FourCCDTO findOne(Long id) {
        log.debug("Request to get FourCC : {}", id);
        FourCC fourCC = fourCCRepository.findOne(id);
        FourCCDTO fourCCDTO = fourCCMapper.fourCCToFourCCDTO(fourCC);
        return fourCCDTO;
    }

    /**
     *  Delete the  fourCC by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete FourCC : {}", id);
        fourCCRepository.delete(id);
        fourCCSearchRepository.delete(id);
    }

    /**
     * Search for the fourCC corresponding to the query.
     *
     *  @param query the query of the search
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<FourCCDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of FourCCS for query {}", query);
        Page<FourCC> result = fourCCSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(fourCC -> fourCCMapper.fourCCToFourCCDTO(fourCC));
    }
}
