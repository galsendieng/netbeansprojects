package com.orange.myabstract.repository;

import com.orange.myabstract.domain.MaxWidth;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the MaxWidth entity.
 */
@SuppressWarnings("unused")
public interface MaxWidthRepository extends JpaRepository<MaxWidth,Long> {

}
