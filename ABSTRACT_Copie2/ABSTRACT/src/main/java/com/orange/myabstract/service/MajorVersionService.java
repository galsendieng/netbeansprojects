package com.orange.myabstract.service;

import com.orange.myabstract.service.dto.MajorVersionDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.LinkedList;
import java.util.List;

/**
 * Service Interface for managing MajorVersion.
 */
public interface MajorVersionService {

    /**
     * Save a majorVersion.
     *
     * @param majorVersionDTO the entity to save
     * @return the persisted entity
     */
    MajorVersionDTO save(MajorVersionDTO majorVersionDTO);

    /**
     *  Get all the majorVersions.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<MajorVersionDTO> findAll(Pageable pageable);

    /**
     *  Get the "id" majorVersion.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    MajorVersionDTO findOne(Long id);

    /**
     *  Delete the "id" majorVersion.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Search for the majorVersion corresponding to the query.
     *
     *  @param query the query of the search
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<MajorVersionDTO> search(String query, Pageable pageable);
}
