package com.orange.myabstract.service;

import com.orange.myabstract.service.dto.MinorVersionDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.LinkedList;
import java.util.List;

/**
 * Service Interface for managing MinorVersion.
 */
public interface MinorVersionService {

    /**
     * Save a minorVersion.
     *
     * @param minorVersionDTO the entity to save
     * @return the persisted entity
     */
    MinorVersionDTO save(MinorVersionDTO minorVersionDTO);

    /**
     *  Get all the minorVersions.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<MinorVersionDTO> findAll(Pageable pageable);

    /**
     *  Get the "id" minorVersion.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    MinorVersionDTO findOne(Long id);

    /**
     *  Delete the "id" minorVersion.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Search for the minorVersion corresponding to the query.
     *
     *  @param query the query of the search
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<MinorVersionDTO> search(String query, Pageable pageable);
}
