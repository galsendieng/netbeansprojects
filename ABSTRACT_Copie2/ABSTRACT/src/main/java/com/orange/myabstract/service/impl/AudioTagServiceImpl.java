package com.orange.myabstract.service.impl;

import com.orange.myabstract.service.AudioTagService;
import com.orange.myabstract.domain.AudioTag;
import com.orange.myabstract.repository.AudioTagRepository;
import com.orange.myabstract.repository.search.AudioTagSearchRepository;
import com.orange.myabstract.service.dto.AudioTagDTO;
import com.orange.myabstract.service.mapper.AudioTagMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing AudioTag.
 */
@Service
@Transactional
public class AudioTagServiceImpl implements AudioTagService{

    private final Logger log = LoggerFactory.getLogger(AudioTagServiceImpl.class);
    
    @Inject
    private AudioTagRepository audioTagRepository;

    @Inject
    private AudioTagMapper audioTagMapper;

    @Inject
    private AudioTagSearchRepository audioTagSearchRepository;

    /**
     * Save a audioTag.
     *
     * @param audioTagDTO the entity to save
     * @return the persisted entity
     */
    public AudioTagDTO save(AudioTagDTO audioTagDTO) {
        log.debug("Request to save AudioTag : {}", audioTagDTO);
        AudioTag audioTag = audioTagMapper.audioTagDTOToAudioTag(audioTagDTO);
        audioTag = audioTagRepository.save(audioTag);
        AudioTagDTO result = audioTagMapper.audioTagToAudioTagDTO(audioTag);
        audioTagSearchRepository.save(audioTag);
        return result;
    }

    /**
     *  Get all the audioTags.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true) 
    public Page<AudioTagDTO> findAll(Pageable pageable) {
        log.debug("Request to get all AudioTags");
        Page<AudioTag> result = audioTagRepository.findAll(pageable);
        return result.map(audioTag -> audioTagMapper.audioTagToAudioTagDTO(audioTag));
    }

    /**
     *  Get one audioTag by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true) 
    public AudioTagDTO findOne(Long id) {
        log.debug("Request to get AudioTag : {}", id);
        AudioTag audioTag = audioTagRepository.findOne(id);
        AudioTagDTO audioTagDTO = audioTagMapper.audioTagToAudioTagDTO(audioTag);
        return audioTagDTO;
    }

    /**
     *  Delete the  audioTag by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete AudioTag : {}", id);
        audioTagRepository.delete(id);
        audioTagSearchRepository.delete(id);
    }

    /**
     * Search for the audioTag corresponding to the query.
     *
     *  @param query the query of the search
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<AudioTagDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of AudioTags for query {}", query);
        Page<AudioTag> result = audioTagSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(audioTag -> audioTagMapper.audioTagToAudioTagDTO(audioTag));
    }
}
