package com.orange.myabstract.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Importance.
 */
@Entity
@Table(name = "importance")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "importance")
public class Importance implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Column(name = "niveau", nullable = false)
    private String niveau;

    @NotNull
    @Column(name = "description", nullable = false)
    private String description;

    @OneToMany(mappedBy = "nivImportance")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<ManifestData> manifests = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNiveau() {
        return niveau;
    }

    public Importance niveau(String niveau) {
        this.niveau = niveau;
        return this;
    }

    public void setNiveau(String niveau) {
        this.niveau = niveau;
    }

    public String getDescription() {
        return description;
    }

    public Importance description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<ManifestData> getManifests() {
        return manifests;
    }

    public Importance manifests(Set<ManifestData> manifestData) {
        this.manifests = manifestData;
        return this;
    }

    public Importance addManifest(ManifestData manifestData) {
        manifests.add(manifestData);
        manifestData.setNivImportance(this);
        return this;
    }

    public Importance removeManifest(ManifestData manifestData) {
        manifests.remove(manifestData);
        manifestData.setNivImportance(null);
        return this;
    }

    public void setManifests(Set<ManifestData> manifestData) {
        this.manifests = manifestData;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Importance importance = (Importance) o;
        if(importance.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, importance.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Importance{" +
            "id=" + id +
            ", niveau='" + niveau + "'" +
            ", description='" + description + "'" +
            '}';
    }
}
