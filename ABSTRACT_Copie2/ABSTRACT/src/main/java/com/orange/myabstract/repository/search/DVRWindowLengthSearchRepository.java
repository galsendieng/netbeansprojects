package com.orange.myabstract.repository.search;

import com.orange.myabstract.domain.DVRWindowLength;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the DVRWindowLength entity.
 */
public interface DVRWindowLengthSearchRepository extends ElasticsearchRepository<DVRWindowLength, Long> {
}
