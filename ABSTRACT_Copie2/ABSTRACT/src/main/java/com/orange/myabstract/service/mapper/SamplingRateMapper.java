package com.orange.myabstract.service.mapper;

import com.orange.myabstract.domain.*;
import com.orange.myabstract.service.dto.SamplingRateDTO;

import org.mapstruct.*;
import java.util.List;

/**
 * Mapper for the entity SamplingRate and its DTO SamplingRateDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface SamplingRateMapper {

    SamplingRateDTO samplingRateToSamplingRateDTO(SamplingRate samplingRate);

    List<SamplingRateDTO> samplingRatesToSamplingRateDTOs(List<SamplingRate> samplingRates);

    SamplingRate samplingRateDTOToSamplingRate(SamplingRateDTO samplingRateDTO);

    List<SamplingRate> samplingRateDTOsToSamplingRates(List<SamplingRateDTO> samplingRateDTOs);
}
