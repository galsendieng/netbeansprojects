package com.orange.myabstract.repository.search;

import com.orange.myabstract.domain.AudioTag;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the AudioTag entity.
 */
public interface AudioTagSearchRepository extends ElasticsearchRepository<AudioTag, Long> {
}
