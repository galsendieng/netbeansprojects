package com.orange.myabstract.service.mapper;

import com.orange.myabstract.domain.*;
import com.orange.myabstract.service.dto.MaxHeightDTO;

import org.mapstruct.*;
import java.util.List;

/**
 * Mapper for the entity MaxHeight and its DTO MaxHeightDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface MaxHeightMapper {

    MaxHeightDTO maxHeightToMaxHeightDTO(MaxHeight maxHeight);

    List<MaxHeightDTO> maxHeightsToMaxHeightDTOs(List<MaxHeight> maxHeights);

    MaxHeight maxHeightDTOToMaxHeight(MaxHeightDTO maxHeightDTO);

    List<MaxHeight> maxHeightDTOsToMaxHeights(List<MaxHeightDTO> maxHeightDTOs);
}
