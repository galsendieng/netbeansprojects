package com.orange.myabstract.service.dto;

import java.time.ZonedDateTime;
import java.io.Serializable;
import java.util.Objects;


/**
 * A DTO for the DVRWindowLength entity.
 */
public class DVRWindowLengthDTO implements Serializable {

    private Long id;

    private String valeur;

    private String streamer;

    private ZonedDateTime dateSup;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public String getValeur() {
        return valeur;
    }

    public void setValeur(String valeur) {
        this.valeur = valeur;
    }
    public String getStreamer() {
        return streamer;
    }

    public void setStreamer(String streamer) {
        this.streamer = streamer;
    }
    public ZonedDateTime getDateSup() {
        return dateSup;
    }

    public void setDateSup(ZonedDateTime dateSup) {
        this.dateSup = dateSup;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        DVRWindowLengthDTO dVRWindowLengthDTO = (DVRWindowLengthDTO) o;

        if ( ! Objects.equals(id, dVRWindowLengthDTO.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "DVRWindowLengthDTO{" +
            "id=" + id +
            ", valeur='" + valeur + "'" +
            ", streamer='" + streamer + "'" +
            ", dateSup='" + dateSup + "'" +
            '}';
    }
}
