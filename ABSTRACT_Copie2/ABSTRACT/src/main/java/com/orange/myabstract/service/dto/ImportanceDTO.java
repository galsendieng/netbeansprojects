package com.orange.myabstract.service.dto;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;


/**
 * A DTO for the Importance entity.
 */
public class ImportanceDTO implements Serializable {

    private Long id;

    @NotNull
    private String niveau;

    @NotNull
    private String description;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public String getNiveau() {
        return niveau;
    }

    public void setNiveau(String niveau) {
        this.niveau = niveau;
    }
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ImportanceDTO importanceDTO = (ImportanceDTO) o;

        if ( ! Objects.equals(id, importanceDTO.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "ImportanceDTO{" +
            "id=" + id +
            ", niveau='" + niveau + "'" +
            ", description='" + description + "'" +
            '}';
    }
}
