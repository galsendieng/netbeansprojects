package com.orange.myabstract.service.mapper;

import com.orange.myabstract.domain.*;
import com.orange.myabstract.service.dto.MajorVersionDTO;

import org.mapstruct.*;
import java.util.List;

/**
 * Mapper for the entity MajorVersion and its DTO MajorVersionDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface MajorVersionMapper {

    MajorVersionDTO majorVersionToMajorVersionDTO(MajorVersion majorVersion);

    List<MajorVersionDTO> majorVersionsToMajorVersionDTOs(List<MajorVersion> majorVersions);

    MajorVersion majorVersionDTOToMajorVersion(MajorVersionDTO majorVersionDTO);

    List<MajorVersion> majorVersionDTOsToMajorVersions(List<MajorVersionDTO> majorVersionDTOs);
}
