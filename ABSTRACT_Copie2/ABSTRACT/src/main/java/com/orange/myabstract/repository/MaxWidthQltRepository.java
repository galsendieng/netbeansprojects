package com.orange.myabstract.repository;

import com.orange.myabstract.domain.MaxWidthQlt;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the MaxWidthQlt entity.
 */
@SuppressWarnings("unused")
public interface MaxWidthQltRepository extends JpaRepository<MaxWidthQlt,Long> {

}
