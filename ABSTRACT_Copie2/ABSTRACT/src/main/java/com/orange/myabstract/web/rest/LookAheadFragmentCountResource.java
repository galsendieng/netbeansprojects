package com.orange.myabstract.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.orange.myabstract.service.LookAheadFragmentCountService;
import com.orange.myabstract.web.rest.util.HeaderUtil;
import com.orange.myabstract.web.rest.util.PaginationUtil;
import com.orange.myabstract.service.dto.LookAheadFragmentCountDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing LookAheadFragmentCount.
 */
@RestController
@RequestMapping("/api")
public class LookAheadFragmentCountResource {

    private final Logger log = LoggerFactory.getLogger(LookAheadFragmentCountResource.class);
        
    @Inject
    private LookAheadFragmentCountService lookAheadFragmentCountService;

    /**
     * POST  /look-ahead-fragment-counts : Create a new lookAheadFragmentCount.
     *
     * @param lookAheadFragmentCountDTO the lookAheadFragmentCountDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new lookAheadFragmentCountDTO, or with status 400 (Bad Request) if the lookAheadFragmentCount has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/look-ahead-fragment-counts")
    @Timed
    public ResponseEntity<LookAheadFragmentCountDTO> createLookAheadFragmentCount(@RequestBody LookAheadFragmentCountDTO lookAheadFragmentCountDTO) throws URISyntaxException {
        log.debug("REST request to save LookAheadFragmentCount : {}", lookAheadFragmentCountDTO);
        if (lookAheadFragmentCountDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("lookAheadFragmentCount", "idexists", "A new lookAheadFragmentCount cannot already have an ID")).body(null);
        }
        LookAheadFragmentCountDTO result = lookAheadFragmentCountService.save(lookAheadFragmentCountDTO);
        return ResponseEntity.created(new URI("/api/look-ahead-fragment-counts/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("lookAheadFragmentCount", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /look-ahead-fragment-counts : Updates an existing lookAheadFragmentCount.
     *
     * @param lookAheadFragmentCountDTO the lookAheadFragmentCountDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated lookAheadFragmentCountDTO,
     * or with status 400 (Bad Request) if the lookAheadFragmentCountDTO is not valid,
     * or with status 500 (Internal Server Error) if the lookAheadFragmentCountDTO couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/look-ahead-fragment-counts")
    @Timed
    public ResponseEntity<LookAheadFragmentCountDTO> updateLookAheadFragmentCount(@RequestBody LookAheadFragmentCountDTO lookAheadFragmentCountDTO) throws URISyntaxException {
        log.debug("REST request to update LookAheadFragmentCount : {}", lookAheadFragmentCountDTO);
        if (lookAheadFragmentCountDTO.getId() == null) {
            return createLookAheadFragmentCount(lookAheadFragmentCountDTO);
        }
        LookAheadFragmentCountDTO result = lookAheadFragmentCountService.save(lookAheadFragmentCountDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("lookAheadFragmentCount", lookAheadFragmentCountDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /look-ahead-fragment-counts : get all the lookAheadFragmentCounts.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of lookAheadFragmentCounts in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @GetMapping("/look-ahead-fragment-counts")
    @Timed
    public ResponseEntity<List<LookAheadFragmentCountDTO>> getAllLookAheadFragmentCounts(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of LookAheadFragmentCounts");
        Page<LookAheadFragmentCountDTO> page = lookAheadFragmentCountService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/look-ahead-fragment-counts");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /look-ahead-fragment-counts/:id : get the "id" lookAheadFragmentCount.
     *
     * @param id the id of the lookAheadFragmentCountDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the lookAheadFragmentCountDTO, or with status 404 (Not Found)
     */
    @GetMapping("/look-ahead-fragment-counts/{id}")
    @Timed
    public ResponseEntity<LookAheadFragmentCountDTO> getLookAheadFragmentCount(@PathVariable Long id) {
        log.debug("REST request to get LookAheadFragmentCount : {}", id);
        LookAheadFragmentCountDTO lookAheadFragmentCountDTO = lookAheadFragmentCountService.findOne(id);
        return Optional.ofNullable(lookAheadFragmentCountDTO)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /look-ahead-fragment-counts/:id : delete the "id" lookAheadFragmentCount.
     *
     * @param id the id of the lookAheadFragmentCountDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/look-ahead-fragment-counts/{id}")
    @Timed
    public ResponseEntity<Void> deleteLookAheadFragmentCount(@PathVariable Long id) {
        log.debug("REST request to delete LookAheadFragmentCount : {}", id);
        lookAheadFragmentCountService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("lookAheadFragmentCount", id.toString())).build();
    }

    /**
     * SEARCH  /_search/look-ahead-fragment-counts?query=:query : search for the lookAheadFragmentCount corresponding
     * to the query.
     *
     * @param query the query of the lookAheadFragmentCount search 
     * @param pageable the pagination information
     * @return the result of the search
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @GetMapping("/_search/look-ahead-fragment-counts")
    @Timed
    public ResponseEntity<List<LookAheadFragmentCountDTO>> searchLookAheadFragmentCounts(@RequestParam String query, Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to search for a page of LookAheadFragmentCounts for query {}", query);
        Page<LookAheadFragmentCountDTO> page = lookAheadFragmentCountService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/look-ahead-fragment-counts");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
