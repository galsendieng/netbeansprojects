package com.orange.myabstract.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.orange.myabstract.service.CorruptManifestService;
import com.orange.myabstract.web.rest.util.HeaderUtil;
import com.orange.myabstract.web.rest.util.PaginationUtil;
import com.orange.myabstract.service.dto.CorruptManifestDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing CorruptManifest.
 */
@RestController
@RequestMapping("/api")
public class CorruptManifestResource {

    private final Logger log = LoggerFactory.getLogger(CorruptManifestResource.class);
        
    @Inject
    private CorruptManifestService corruptManifestService;

    /**
     * POST  /corrupt-manifests : Create a new corruptManifest.
     *
     * @param corruptManifestDTO the corruptManifestDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new corruptManifestDTO, or with status 400 (Bad Request) if the corruptManifest has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/corrupt-manifests")
    @Timed
    public ResponseEntity<CorruptManifestDTO> createCorruptManifest(@RequestBody CorruptManifestDTO corruptManifestDTO) throws URISyntaxException {
        log.debug("REST request to save CorruptManifest : {}", corruptManifestDTO);
        if (corruptManifestDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("corruptManifest", "idexists", "A new corruptManifest cannot already have an ID")).body(null);
        }
        CorruptManifestDTO result = corruptManifestService.save(corruptManifestDTO);
        return ResponseEntity.created(new URI("/api/corrupt-manifests/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("corruptManifest", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /corrupt-manifests : Updates an existing corruptManifest.
     *
     * @param corruptManifestDTO the corruptManifestDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated corruptManifestDTO,
     * or with status 400 (Bad Request) if the corruptManifestDTO is not valid,
     * or with status 500 (Internal Server Error) if the corruptManifestDTO couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/corrupt-manifests")
    @Timed
    public ResponseEntity<CorruptManifestDTO> updateCorruptManifest(@RequestBody CorruptManifestDTO corruptManifestDTO) throws URISyntaxException {
        log.debug("REST request to update CorruptManifest : {}", corruptManifestDTO);
        if (corruptManifestDTO.getId() == null) {
            return createCorruptManifest(corruptManifestDTO);
        }
        CorruptManifestDTO result = corruptManifestService.save(corruptManifestDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("corruptManifest", corruptManifestDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /corrupt-manifests : get all the corruptManifests.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of corruptManifests in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @GetMapping("/corrupt-manifests")
    @Timed
    public ResponseEntity<List<CorruptManifestDTO>> getAllCorruptManifests(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of CorruptManifests");
        Page<CorruptManifestDTO> page = corruptManifestService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/corrupt-manifests");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /corrupt-manifests/:id : get the "id" corruptManifest.
     *
     * @param id the id of the corruptManifestDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the corruptManifestDTO, or with status 404 (Not Found)
     */
    @GetMapping("/corrupt-manifests/{id}")
    @Timed
    public ResponseEntity<CorruptManifestDTO> getCorruptManifest(@PathVariable Long id) {
        log.debug("REST request to get CorruptManifest : {}", id);
        CorruptManifestDTO corruptManifestDTO = corruptManifestService.findOne(id);
        return Optional.ofNullable(corruptManifestDTO)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /corrupt-manifests/:id : delete the "id" corruptManifest.
     *
     * @param id the id of the corruptManifestDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/corrupt-manifests/{id}")
    @Timed
    public ResponseEntity<Void> deleteCorruptManifest(@PathVariable Long id) {
        log.debug("REST request to delete CorruptManifest : {}", id);
        corruptManifestService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("corruptManifest", id.toString())).build();
    }

    /**
     * SEARCH  /_search/corrupt-manifests?query=:query : search for the corruptManifest corresponding
     * to the query.
     *
     * @param query the query of the corruptManifest search 
     * @param pageable the pagination information
     * @return the result of the search
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @GetMapping("/_search/corrupt-manifests")
    @Timed
    public ResponseEntity<List<CorruptManifestDTO>> searchCorruptManifests(@RequestParam String query, Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to search for a page of CorruptManifests for query {}", query);
        Page<CorruptManifestDTO> page = corruptManifestService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/corrupt-manifests");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
