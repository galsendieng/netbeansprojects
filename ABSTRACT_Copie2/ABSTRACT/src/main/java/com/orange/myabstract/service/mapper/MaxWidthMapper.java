package com.orange.myabstract.service.mapper;

import com.orange.myabstract.domain.*;
import com.orange.myabstract.service.dto.MaxWidthDTO;

import org.mapstruct.*;
import java.util.List;

/**
 * Mapper for the entity MaxWidth and its DTO MaxWidthDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface MaxWidthMapper {

    MaxWidthDTO maxWidthToMaxWidthDTO(MaxWidth maxWidth);

    List<MaxWidthDTO> maxWidthsToMaxWidthDTOs(List<MaxWidth> maxWidths);

    MaxWidth maxWidthDTOToMaxWidth(MaxWidthDTO maxWidthDTO);

    List<MaxWidth> maxWidthDTOsToMaxWidths(List<MaxWidthDTO> maxWidthDTOs);
}
