package com.orange.myabstract.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.orange.myabstract.service.DVRWindowLengthService;
import com.orange.myabstract.web.rest.util.HeaderUtil;
import com.orange.myabstract.web.rest.util.PaginationUtil;
import com.orange.myabstract.service.dto.DVRWindowLengthDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing DVRWindowLength.
 */
@RestController
@RequestMapping("/api")
public class DVRWindowLengthResource {

    private final Logger log = LoggerFactory.getLogger(DVRWindowLengthResource.class);
        
    @Inject
    private DVRWindowLengthService dVRWindowLengthService;

    /**
     * POST  /d-vr-window-lengths : Create a new dVRWindowLength.
     *
     * @param dVRWindowLengthDTO the dVRWindowLengthDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new dVRWindowLengthDTO, or with status 400 (Bad Request) if the dVRWindowLength has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/d-vr-window-lengths")
    @Timed
    public ResponseEntity<DVRWindowLengthDTO> createDVRWindowLength(@RequestBody DVRWindowLengthDTO dVRWindowLengthDTO) throws URISyntaxException {
        log.debug("REST request to save DVRWindowLength : {}", dVRWindowLengthDTO);
        if (dVRWindowLengthDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("dVRWindowLength", "idexists", "A new dVRWindowLength cannot already have an ID")).body(null);
        }
        DVRWindowLengthDTO result = dVRWindowLengthService.save(dVRWindowLengthDTO);
        return ResponseEntity.created(new URI("/api/d-vr-window-lengths/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("dVRWindowLength", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /d-vr-window-lengths : Updates an existing dVRWindowLength.
     *
     * @param dVRWindowLengthDTO the dVRWindowLengthDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated dVRWindowLengthDTO,
     * or with status 400 (Bad Request) if the dVRWindowLengthDTO is not valid,
     * or with status 500 (Internal Server Error) if the dVRWindowLengthDTO couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/d-vr-window-lengths")
    @Timed
    public ResponseEntity<DVRWindowLengthDTO> updateDVRWindowLength(@RequestBody DVRWindowLengthDTO dVRWindowLengthDTO) throws URISyntaxException {
        log.debug("REST request to update DVRWindowLength : {}", dVRWindowLengthDTO);
        if (dVRWindowLengthDTO.getId() == null) {
            return createDVRWindowLength(dVRWindowLengthDTO);
        }
        DVRWindowLengthDTO result = dVRWindowLengthService.save(dVRWindowLengthDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("dVRWindowLength", dVRWindowLengthDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /d-vr-window-lengths : get all the dVRWindowLengths.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of dVRWindowLengths in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @GetMapping("/d-vr-window-lengths")
    @Timed
    public ResponseEntity<List<DVRWindowLengthDTO>> getAllDVRWindowLengths(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of DVRWindowLengths");
        Page<DVRWindowLengthDTO> page = dVRWindowLengthService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/d-vr-window-lengths");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /d-vr-window-lengths/:id : get the "id" dVRWindowLength.
     *
     * @param id the id of the dVRWindowLengthDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the dVRWindowLengthDTO, or with status 404 (Not Found)
     */
    @GetMapping("/d-vr-window-lengths/{id}")
    @Timed
    public ResponseEntity<DVRWindowLengthDTO> getDVRWindowLength(@PathVariable Long id) {
        log.debug("REST request to get DVRWindowLength : {}", id);
        DVRWindowLengthDTO dVRWindowLengthDTO = dVRWindowLengthService.findOne(id);
        return Optional.ofNullable(dVRWindowLengthDTO)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /d-vr-window-lengths/:id : delete the "id" dVRWindowLength.
     *
     * @param id the id of the dVRWindowLengthDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/d-vr-window-lengths/{id}")
    @Timed
    public ResponseEntity<Void> deleteDVRWindowLength(@PathVariable Long id) {
        log.debug("REST request to delete DVRWindowLength : {}", id);
        dVRWindowLengthService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("dVRWindowLength", id.toString())).build();
    }

    /**
     * SEARCH  /_search/d-vr-window-lengths?query=:query : search for the dVRWindowLength corresponding
     * to the query.
     *
     * @param query the query of the dVRWindowLength search 
     * @param pageable the pagination information
     * @return the result of the search
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @GetMapping("/_search/d-vr-window-lengths")
    @Timed
    public ResponseEntity<List<DVRWindowLengthDTO>> searchDVRWindowLengths(@RequestParam String query, Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to search for a page of DVRWindowLengths for query {}", query);
        Page<DVRWindowLengthDTO> page = dVRWindowLengthService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/d-vr-window-lengths");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
