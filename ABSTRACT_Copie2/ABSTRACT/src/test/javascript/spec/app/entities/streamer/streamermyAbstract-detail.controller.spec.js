'use strict';

describe('Controller Tests', function() {

    describe('Streamer Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockPreviousState, MockStreamer, MockPod, MockChannels;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockPreviousState = jasmine.createSpy('MockPreviousState');
            MockStreamer = jasmine.createSpy('MockStreamer');
            MockPod = jasmine.createSpy('MockPod');
            MockChannels = jasmine.createSpy('MockChannels');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity,
                'previousState': MockPreviousState,
                'Streamer': MockStreamer,
                'Pod': MockPod,
                'Channels': MockChannels
            };
            createController = function() {
                $injector.get('$controller')("StreamerMyAbstractDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'abstractApp:streamerUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
