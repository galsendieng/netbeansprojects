package com.orange.myabstract.web.rest;

import com.orange.myabstract.AbstractApp;

import com.orange.myabstract.domain.Channels;
import com.orange.myabstract.repository.ChannelsRepository;
import com.orange.myabstract.service.ChannelsService;
import com.orange.myabstract.repository.search.ChannelsSearchRepository;
import com.orange.myabstract.service.dto.ChannelsDTO;
import com.orange.myabstract.service.mapper.ChannelsMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ChannelsResource REST controller.
 *
 * @see ChannelsResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = AbstractApp.class)
public class ChannelsResourceIntTest {

    private static final String DEFAULT_CHANNEL = "AAAAAAAAAA";
    private static final String UPDATED_CHANNEL = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    @Inject
    private ChannelsRepository channelsRepository;

    @Inject
    private ChannelsMapper channelsMapper;

    @Inject
    private ChannelsService channelsService;

    @Inject
    private ChannelsSearchRepository channelsSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Inject
    private EntityManager em;

    private MockMvc restChannelsMockMvc;

    private Channels channels;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        ChannelsResource channelsResource = new ChannelsResource();
        ReflectionTestUtils.setField(channelsResource, "channelsService", channelsService);
        this.restChannelsMockMvc = MockMvcBuilders.standaloneSetup(channelsResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Channels createEntity(EntityManager em) {
        Channels channels = new Channels()
                .channel(DEFAULT_CHANNEL)
                .description(DEFAULT_DESCRIPTION);
        return channels;
    }

    @Before
    public void initTest() {
        channelsSearchRepository.deleteAll();
        channels = createEntity(em);
    }

    @Test
    @Transactional
    public void createChannels() throws Exception {
        int databaseSizeBeforeCreate = channelsRepository.findAll().size();

        // Create the Channels
        ChannelsDTO channelsDTO = channelsMapper.channelsToChannelsDTO(channels);

        restChannelsMockMvc.perform(post("/api/channels")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(channelsDTO)))
                .andExpect(status().isCreated());

        // Validate the Channels in the database
        List<Channels> channels = channelsRepository.findAll();
        assertThat(channels).hasSize(databaseSizeBeforeCreate + 1);
        Channels testChannels = channels.get(channels.size() - 1);
        assertThat(testChannels.getChannel()).isEqualTo(DEFAULT_CHANNEL);
        assertThat(testChannels.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);

        // Validate the Channels in ElasticSearch
        Channels channelsEs = channelsSearchRepository.findOne(testChannels.getId());
        assertThat(channelsEs).isEqualToComparingFieldByField(testChannels);
    }

    @Test
    @Transactional
    public void checkChannelIsRequired() throws Exception {
        int databaseSizeBeforeTest = channelsRepository.findAll().size();
        // set the field null
        channels.setChannel(null);

        // Create the Channels, which fails.
        ChannelsDTO channelsDTO = channelsMapper.channelsToChannelsDTO(channels);

        restChannelsMockMvc.perform(post("/api/channels")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(channelsDTO)))
                .andExpect(status().isBadRequest());

        List<Channels> channels = channelsRepository.findAll();
        assertThat(channels).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllChannels() throws Exception {
        // Initialize the database
        channelsRepository.saveAndFlush(channels);

        // Get all the channels
        restChannelsMockMvc.perform(get("/api/channels?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.[*].id").value(hasItem(channels.getId().intValue())))
                .andExpect(jsonPath("$.[*].channel").value(hasItem(DEFAULT_CHANNEL.toString())))
                .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())));
    }

    @Test
    @Transactional
    public void getChannels() throws Exception {
        // Initialize the database
        channelsRepository.saveAndFlush(channels);

        // Get the channels
        restChannelsMockMvc.perform(get("/api/channels/{id}", channels.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(channels.getId().intValue()))
            .andExpect(jsonPath("$.channel").value(DEFAULT_CHANNEL.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingChannels() throws Exception {
        // Get the channels
        restChannelsMockMvc.perform(get("/api/channels/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateChannels() throws Exception {
        // Initialize the database
        channelsRepository.saveAndFlush(channels);
        channelsSearchRepository.save(channels);
        int databaseSizeBeforeUpdate = channelsRepository.findAll().size();

        // Update the channels
        Channels updatedChannels = channelsRepository.findOne(channels.getId());
        updatedChannels
                .channel(UPDATED_CHANNEL)
                .description(UPDATED_DESCRIPTION);
        ChannelsDTO channelsDTO = channelsMapper.channelsToChannelsDTO(updatedChannels);

        restChannelsMockMvc.perform(put("/api/channels")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(channelsDTO)))
                .andExpect(status().isOk());

        // Validate the Channels in the database
        List<Channels> channels = channelsRepository.findAll();
        assertThat(channels).hasSize(databaseSizeBeforeUpdate);
        Channels testChannels = channels.get(channels.size() - 1);
        assertThat(testChannels.getChannel()).isEqualTo(UPDATED_CHANNEL);
        assertThat(testChannels.getDescription()).isEqualTo(UPDATED_DESCRIPTION);

        // Validate the Channels in ElasticSearch
        Channels channelsEs = channelsSearchRepository.findOne(testChannels.getId());
        assertThat(channelsEs).isEqualToComparingFieldByField(testChannels);
    }

    @Test
    @Transactional
    public void deleteChannels() throws Exception {
        // Initialize the database
        channelsRepository.saveAndFlush(channels);
        channelsSearchRepository.save(channels);
        int databaseSizeBeforeDelete = channelsRepository.findAll().size();

        // Get the channels
        restChannelsMockMvc.perform(delete("/api/channels/{id}", channels.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate ElasticSearch is empty
        boolean channelsExistsInEs = channelsSearchRepository.exists(channels.getId());
        assertThat(channelsExistsInEs).isFalse();

        // Validate the database is empty
        List<Channels> channels = channelsRepository.findAll();
        assertThat(channels).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchChannels() throws Exception {
        // Initialize the database
        channelsRepository.saveAndFlush(channels);
        channelsSearchRepository.save(channels);

        // Search the channels
        restChannelsMockMvc.perform(get("/api/_search/channels?query=id:" + channels.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(channels.getId().intValue())))
            .andExpect(jsonPath("$.[*].channel").value(hasItem(DEFAULT_CHANNEL.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())));
    }
}
