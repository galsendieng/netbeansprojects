package com.orange.myabstract.web.rest;

import com.orange.myabstract.AbstractApp;

import com.orange.myabstract.domain.Bitrate;
import com.orange.myabstract.repository.BitrateRepository;
import com.orange.myabstract.service.BitrateService;
import com.orange.myabstract.repository.search.BitrateSearchRepository;
import com.orange.myabstract.service.dto.BitrateDTO;
import com.orange.myabstract.service.mapper.BitrateMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the BitrateResource REST controller.
 *
 * @see BitrateResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = AbstractApp.class)
public class BitrateResourceIntTest {

    private static final String DEFAULT_TYPE_STREAM = "AAAAAAAAAA";
    private static final String UPDATED_TYPE_STREAM = "BBBBBBBBBB";

    private static final String DEFAULT_STREAMER = "AAAAAAAAAA";
    private static final String UPDATED_STREAMER = "BBBBBBBBBB";

    private static final String DEFAULT_VALEUR = "AAAAAAAAAA";
    private static final String UPDATED_VALEUR = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_DATE_SUP = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_DATE_SUP = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final String DEFAULT_DATE_SUP_STR = DateTimeFormatter.ISO_INSTANT.format(DEFAULT_DATE_SUP);

    @Inject
    private BitrateRepository bitrateRepository;

    @Inject
    private BitrateMapper bitrateMapper;

    @Inject
    private BitrateService bitrateService;

    @Inject
    private BitrateSearchRepository bitrateSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Inject
    private EntityManager em;

    private MockMvc restBitrateMockMvc;

    private Bitrate bitrate;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        BitrateResource bitrateResource = new BitrateResource();
        ReflectionTestUtils.setField(bitrateResource, "bitrateService", bitrateService);
        this.restBitrateMockMvc = MockMvcBuilders.standaloneSetup(bitrateResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Bitrate createEntity(EntityManager em) {
        Bitrate bitrate = new Bitrate()
                .typeStream(DEFAULT_TYPE_STREAM)
                .streamer(DEFAULT_STREAMER)
                .valeur(DEFAULT_VALEUR)
                .dateSup(DEFAULT_DATE_SUP);
        return bitrate;
    }

    @Before
    public void initTest() {
        bitrateSearchRepository.deleteAll();
        bitrate = createEntity(em);
    }

    @Test
    @Transactional
    public void createBitrate() throws Exception {
        int databaseSizeBeforeCreate = bitrateRepository.findAll().size();

        // Create the Bitrate
        BitrateDTO bitrateDTO = bitrateMapper.bitrateToBitrateDTO(bitrate);

        restBitrateMockMvc.perform(post("/api/bitrates")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(bitrateDTO)))
                .andExpect(status().isCreated());

        // Validate the Bitrate in the database
        List<Bitrate> bitrates = bitrateRepository.findAll();
        assertThat(bitrates).hasSize(databaseSizeBeforeCreate + 1);
        Bitrate testBitrate = bitrates.get(bitrates.size() - 1);
        assertThat(testBitrate.getTypeStream()).isEqualTo(DEFAULT_TYPE_STREAM);
        assertThat(testBitrate.getStreamer()).isEqualTo(DEFAULT_STREAMER);
        assertThat(testBitrate.getValeur()).isEqualTo(DEFAULT_VALEUR);
        assertThat(testBitrate.getDateSup()).isEqualTo(DEFAULT_DATE_SUP);

        // Validate the Bitrate in ElasticSearch
        Bitrate bitrateEs = bitrateSearchRepository.findOne(testBitrate.getId());
        assertThat(bitrateEs).isEqualToComparingFieldByField(testBitrate);
    }

    @Test
    @Transactional
    public void getAllBitrates() throws Exception {
        // Initialize the database
        bitrateRepository.saveAndFlush(bitrate);

        // Get all the bitrates
        restBitrateMockMvc.perform(get("/api/bitrates?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.[*].id").value(hasItem(bitrate.getId().intValue())))
                .andExpect(jsonPath("$.[*].typeStream").value(hasItem(DEFAULT_TYPE_STREAM.toString())))
                .andExpect(jsonPath("$.[*].streamer").value(hasItem(DEFAULT_STREAMER.toString())))
                .andExpect(jsonPath("$.[*].valeur").value(hasItem(DEFAULT_VALEUR.toString())))
                .andExpect(jsonPath("$.[*].dateSup").value(hasItem(DEFAULT_DATE_SUP_STR)));
    }

    @Test
    @Transactional
    public void getBitrate() throws Exception {
        // Initialize the database
        bitrateRepository.saveAndFlush(bitrate);

        // Get the bitrate
        restBitrateMockMvc.perform(get("/api/bitrates/{id}", bitrate.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(bitrate.getId().intValue()))
            .andExpect(jsonPath("$.typeStream").value(DEFAULT_TYPE_STREAM.toString()))
            .andExpect(jsonPath("$.streamer").value(DEFAULT_STREAMER.toString()))
            .andExpect(jsonPath("$.valeur").value(DEFAULT_VALEUR.toString()))
            .andExpect(jsonPath("$.dateSup").value(DEFAULT_DATE_SUP_STR));
    }

    @Test
    @Transactional
    public void getNonExistingBitrate() throws Exception {
        // Get the bitrate
        restBitrateMockMvc.perform(get("/api/bitrates/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateBitrate() throws Exception {
        // Initialize the database
        bitrateRepository.saveAndFlush(bitrate);
        bitrateSearchRepository.save(bitrate);
        int databaseSizeBeforeUpdate = bitrateRepository.findAll().size();

        // Update the bitrate
        Bitrate updatedBitrate = bitrateRepository.findOne(bitrate.getId());
        updatedBitrate
                .typeStream(UPDATED_TYPE_STREAM)
                .streamer(UPDATED_STREAMER)
                .valeur(UPDATED_VALEUR)
                .dateSup(UPDATED_DATE_SUP);
        BitrateDTO bitrateDTO = bitrateMapper.bitrateToBitrateDTO(updatedBitrate);

        restBitrateMockMvc.perform(put("/api/bitrates")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(bitrateDTO)))
                .andExpect(status().isOk());

        // Validate the Bitrate in the database
        List<Bitrate> bitrates = bitrateRepository.findAll();
        assertThat(bitrates).hasSize(databaseSizeBeforeUpdate);
        Bitrate testBitrate = bitrates.get(bitrates.size() - 1);
        assertThat(testBitrate.getTypeStream()).isEqualTo(UPDATED_TYPE_STREAM);
        assertThat(testBitrate.getStreamer()).isEqualTo(UPDATED_STREAMER);
        assertThat(testBitrate.getValeur()).isEqualTo(UPDATED_VALEUR);
        assertThat(testBitrate.getDateSup()).isEqualTo(UPDATED_DATE_SUP);

        // Validate the Bitrate in ElasticSearch
        Bitrate bitrateEs = bitrateSearchRepository.findOne(testBitrate.getId());
        assertThat(bitrateEs).isEqualToComparingFieldByField(testBitrate);
    }

    @Test
    @Transactional
    public void deleteBitrate() throws Exception {
        // Initialize the database
        bitrateRepository.saveAndFlush(bitrate);
        bitrateSearchRepository.save(bitrate);
        int databaseSizeBeforeDelete = bitrateRepository.findAll().size();

        // Get the bitrate
        restBitrateMockMvc.perform(delete("/api/bitrates/{id}", bitrate.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate ElasticSearch is empty
        boolean bitrateExistsInEs = bitrateSearchRepository.exists(bitrate.getId());
        assertThat(bitrateExistsInEs).isFalse();

        // Validate the database is empty
        List<Bitrate> bitrates = bitrateRepository.findAll();
        assertThat(bitrates).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchBitrate() throws Exception {
        // Initialize the database
        bitrateRepository.saveAndFlush(bitrate);
        bitrateSearchRepository.save(bitrate);

        // Search the bitrate
        restBitrateMockMvc.perform(get("/api/_search/bitrates?query=id:" + bitrate.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(bitrate.getId().intValue())))
            .andExpect(jsonPath("$.[*].typeStream").value(hasItem(DEFAULT_TYPE_STREAM.toString())))
            .andExpect(jsonPath("$.[*].streamer").value(hasItem(DEFAULT_STREAMER.toString())))
            .andExpect(jsonPath("$.[*].valeur").value(hasItem(DEFAULT_VALEUR.toString())))
            .andExpect(jsonPath("$.[*].dateSup").value(hasItem(DEFAULT_DATE_SUP_STR)));
    }
}
