package com.orange.myabstract.web.rest;

import com.orange.myabstract.AbstractApp;

import com.orange.myabstract.domain.BitsPerSample;
import com.orange.myabstract.repository.BitsPerSampleRepository;
import com.orange.myabstract.service.BitsPerSampleService;
import com.orange.myabstract.repository.search.BitsPerSampleSearchRepository;
import com.orange.myabstract.service.dto.BitsPerSampleDTO;
import com.orange.myabstract.service.mapper.BitsPerSampleMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the BitsPerSampleResource REST controller.
 *
 * @see BitsPerSampleResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = AbstractApp.class)
public class BitsPerSampleResourceIntTest {

    private static final String DEFAULT_TYPE_STREAM = "AAAAAAAAAA";
    private static final String UPDATED_TYPE_STREAM = "BBBBBBBBBB";

    private static final String DEFAULT_VALEUR = "AAAAAAAAAA";
    private static final String UPDATED_VALEUR = "BBBBBBBBBB";

    private static final String DEFAULT_STREAMER = "AAAAAAAAAA";
    private static final String UPDATED_STREAMER = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_DATE_SUP = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_DATE_SUP = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final String DEFAULT_DATE_SUP_STR = DateTimeFormatter.ISO_INSTANT.format(DEFAULT_DATE_SUP);

    @Inject
    private BitsPerSampleRepository bitsPerSampleRepository;

    @Inject
    private BitsPerSampleMapper bitsPerSampleMapper;

    @Inject
    private BitsPerSampleService bitsPerSampleService;

    @Inject
    private BitsPerSampleSearchRepository bitsPerSampleSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Inject
    private EntityManager em;

    private MockMvc restBitsPerSampleMockMvc;

    private BitsPerSample bitsPerSample;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        BitsPerSampleResource bitsPerSampleResource = new BitsPerSampleResource();
        ReflectionTestUtils.setField(bitsPerSampleResource, "bitsPerSampleService", bitsPerSampleService);
        this.restBitsPerSampleMockMvc = MockMvcBuilders.standaloneSetup(bitsPerSampleResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static BitsPerSample createEntity(EntityManager em) {
        BitsPerSample bitsPerSample = new BitsPerSample()
                .typeStream(DEFAULT_TYPE_STREAM)
                .valeur(DEFAULT_VALEUR)
                .streamer(DEFAULT_STREAMER)
                .dateSup(DEFAULT_DATE_SUP);
        return bitsPerSample;
    }

    @Before
    public void initTest() {
        bitsPerSampleSearchRepository.deleteAll();
        bitsPerSample = createEntity(em);
    }

    @Test
    @Transactional
    public void createBitsPerSample() throws Exception {
        int databaseSizeBeforeCreate = bitsPerSampleRepository.findAll().size();

        // Create the BitsPerSample
        BitsPerSampleDTO bitsPerSampleDTO = bitsPerSampleMapper.bitsPerSampleToBitsPerSampleDTO(bitsPerSample);

        restBitsPerSampleMockMvc.perform(post("/api/bits-per-samples")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(bitsPerSampleDTO)))
                .andExpect(status().isCreated());

        // Validate the BitsPerSample in the database
        List<BitsPerSample> bitsPerSamples = bitsPerSampleRepository.findAll();
        assertThat(bitsPerSamples).hasSize(databaseSizeBeforeCreate + 1);
        BitsPerSample testBitsPerSample = bitsPerSamples.get(bitsPerSamples.size() - 1);
        assertThat(testBitsPerSample.getTypeStream()).isEqualTo(DEFAULT_TYPE_STREAM);
        assertThat(testBitsPerSample.getValeur()).isEqualTo(DEFAULT_VALEUR);
        assertThat(testBitsPerSample.getStreamer()).isEqualTo(DEFAULT_STREAMER);
        assertThat(testBitsPerSample.getDateSup()).isEqualTo(DEFAULT_DATE_SUP);

        // Validate the BitsPerSample in ElasticSearch
        BitsPerSample bitsPerSampleEs = bitsPerSampleSearchRepository.findOne(testBitsPerSample.getId());
        assertThat(bitsPerSampleEs).isEqualToComparingFieldByField(testBitsPerSample);
    }

    @Test
    @Transactional
    public void getAllBitsPerSamples() throws Exception {
        // Initialize the database
        bitsPerSampleRepository.saveAndFlush(bitsPerSample);

        // Get all the bitsPerSamples
        restBitsPerSampleMockMvc.perform(get("/api/bits-per-samples?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.[*].id").value(hasItem(bitsPerSample.getId().intValue())))
                .andExpect(jsonPath("$.[*].typeStream").value(hasItem(DEFAULT_TYPE_STREAM.toString())))
                .andExpect(jsonPath("$.[*].valeur").value(hasItem(DEFAULT_VALEUR.toString())))
                .andExpect(jsonPath("$.[*].streamer").value(hasItem(DEFAULT_STREAMER.toString())))
                .andExpect(jsonPath("$.[*].dateSup").value(hasItem(DEFAULT_DATE_SUP_STR)));
    }

    @Test
    @Transactional
    public void getBitsPerSample() throws Exception {
        // Initialize the database
        bitsPerSampleRepository.saveAndFlush(bitsPerSample);

        // Get the bitsPerSample
        restBitsPerSampleMockMvc.perform(get("/api/bits-per-samples/{id}", bitsPerSample.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(bitsPerSample.getId().intValue()))
            .andExpect(jsonPath("$.typeStream").value(DEFAULT_TYPE_STREAM.toString()))
            .andExpect(jsonPath("$.valeur").value(DEFAULT_VALEUR.toString()))
            .andExpect(jsonPath("$.streamer").value(DEFAULT_STREAMER.toString()))
            .andExpect(jsonPath("$.dateSup").value(DEFAULT_DATE_SUP_STR));
    }

    @Test
    @Transactional
    public void getNonExistingBitsPerSample() throws Exception {
        // Get the bitsPerSample
        restBitsPerSampleMockMvc.perform(get("/api/bits-per-samples/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateBitsPerSample() throws Exception {
        // Initialize the database
        bitsPerSampleRepository.saveAndFlush(bitsPerSample);
        bitsPerSampleSearchRepository.save(bitsPerSample);
        int databaseSizeBeforeUpdate = bitsPerSampleRepository.findAll().size();

        // Update the bitsPerSample
        BitsPerSample updatedBitsPerSample = bitsPerSampleRepository.findOne(bitsPerSample.getId());
        updatedBitsPerSample
                .typeStream(UPDATED_TYPE_STREAM)
                .valeur(UPDATED_VALEUR)
                .streamer(UPDATED_STREAMER)
                .dateSup(UPDATED_DATE_SUP);
        BitsPerSampleDTO bitsPerSampleDTO = bitsPerSampleMapper.bitsPerSampleToBitsPerSampleDTO(updatedBitsPerSample);

        restBitsPerSampleMockMvc.perform(put("/api/bits-per-samples")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(bitsPerSampleDTO)))
                .andExpect(status().isOk());

        // Validate the BitsPerSample in the database
        List<BitsPerSample> bitsPerSamples = bitsPerSampleRepository.findAll();
        assertThat(bitsPerSamples).hasSize(databaseSizeBeforeUpdate);
        BitsPerSample testBitsPerSample = bitsPerSamples.get(bitsPerSamples.size() - 1);
        assertThat(testBitsPerSample.getTypeStream()).isEqualTo(UPDATED_TYPE_STREAM);
        assertThat(testBitsPerSample.getValeur()).isEqualTo(UPDATED_VALEUR);
        assertThat(testBitsPerSample.getStreamer()).isEqualTo(UPDATED_STREAMER);
        assertThat(testBitsPerSample.getDateSup()).isEqualTo(UPDATED_DATE_SUP);

        // Validate the BitsPerSample in ElasticSearch
        BitsPerSample bitsPerSampleEs = bitsPerSampleSearchRepository.findOne(testBitsPerSample.getId());
        assertThat(bitsPerSampleEs).isEqualToComparingFieldByField(testBitsPerSample);
    }

    @Test
    @Transactional
    public void deleteBitsPerSample() throws Exception {
        // Initialize the database
        bitsPerSampleRepository.saveAndFlush(bitsPerSample);
        bitsPerSampleSearchRepository.save(bitsPerSample);
        int databaseSizeBeforeDelete = bitsPerSampleRepository.findAll().size();

        // Get the bitsPerSample
        restBitsPerSampleMockMvc.perform(delete("/api/bits-per-samples/{id}", bitsPerSample.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate ElasticSearch is empty
        boolean bitsPerSampleExistsInEs = bitsPerSampleSearchRepository.exists(bitsPerSample.getId());
        assertThat(bitsPerSampleExistsInEs).isFalse();

        // Validate the database is empty
        List<BitsPerSample> bitsPerSamples = bitsPerSampleRepository.findAll();
        assertThat(bitsPerSamples).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchBitsPerSample() throws Exception {
        // Initialize the database
        bitsPerSampleRepository.saveAndFlush(bitsPerSample);
        bitsPerSampleSearchRepository.save(bitsPerSample);

        // Search the bitsPerSample
        restBitsPerSampleMockMvc.perform(get("/api/_search/bits-per-samples?query=id:" + bitsPerSample.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(bitsPerSample.getId().intValue())))
            .andExpect(jsonPath("$.[*].typeStream").value(hasItem(DEFAULT_TYPE_STREAM.toString())))
            .andExpect(jsonPath("$.[*].valeur").value(hasItem(DEFAULT_VALEUR.toString())))
            .andExpect(jsonPath("$.[*].streamer").value(hasItem(DEFAULT_STREAMER.toString())))
            .andExpect(jsonPath("$.[*].dateSup").value(hasItem(DEFAULT_DATE_SUP_STR)));
    }
}
