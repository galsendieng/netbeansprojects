package com.orange.myabstract.web.rest;

import com.orange.myabstract.AbstractApp;

import com.orange.myabstract.domain.DisplayHeight;
import com.orange.myabstract.repository.DisplayHeightRepository;
import com.orange.myabstract.service.DisplayHeightService;
import com.orange.myabstract.repository.search.DisplayHeightSearchRepository;
import com.orange.myabstract.service.dto.DisplayHeightDTO;
import com.orange.myabstract.service.mapper.DisplayHeightMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the DisplayHeightResource REST controller.
 *
 * @see DisplayHeightResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = AbstractApp.class)
public class DisplayHeightResourceIntTest {

    private static final String DEFAULT_TYPE_STREAM = "AAAAAAAAAA";
    private static final String UPDATED_TYPE_STREAM = "BBBBBBBBBB";

    private static final String DEFAULT_STREAMER = "AAAAAAAAAA";
    private static final String UPDATED_STREAMER = "BBBBBBBBBB";

    private static final String DEFAULT_VALEUR = "AAAAAAAAAA";
    private static final String UPDATED_VALEUR = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_DATE_SUP = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_DATE_SUP = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final String DEFAULT_DATE_SUP_STR = DateTimeFormatter.ISO_INSTANT.format(DEFAULT_DATE_SUP);

    @Inject
    private DisplayHeightRepository displayHeightRepository;

    @Inject
    private DisplayHeightMapper displayHeightMapper;

    @Inject
    private DisplayHeightService displayHeightService;

    @Inject
    private DisplayHeightSearchRepository displayHeightSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Inject
    private EntityManager em;

    private MockMvc restDisplayHeightMockMvc;

    private DisplayHeight displayHeight;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        DisplayHeightResource displayHeightResource = new DisplayHeightResource();
        ReflectionTestUtils.setField(displayHeightResource, "displayHeightService", displayHeightService);
        this.restDisplayHeightMockMvc = MockMvcBuilders.standaloneSetup(displayHeightResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DisplayHeight createEntity(EntityManager em) {
        DisplayHeight displayHeight = new DisplayHeight()
                .typeStream(DEFAULT_TYPE_STREAM)
                .streamer(DEFAULT_STREAMER)
                .valeur(DEFAULT_VALEUR)
                .dateSup(DEFAULT_DATE_SUP);
        return displayHeight;
    }

    @Before
    public void initTest() {
        displayHeightSearchRepository.deleteAll();
        displayHeight = createEntity(em);
    }

    @Test
    @Transactional
    public void createDisplayHeight() throws Exception {
        int databaseSizeBeforeCreate = displayHeightRepository.findAll().size();

        // Create the DisplayHeight
        DisplayHeightDTO displayHeightDTO = displayHeightMapper.displayHeightToDisplayHeightDTO(displayHeight);

        restDisplayHeightMockMvc.perform(post("/api/display-heights")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(displayHeightDTO)))
                .andExpect(status().isCreated());

        // Validate the DisplayHeight in the database
        List<DisplayHeight> displayHeights = displayHeightRepository.findAll();
        assertThat(displayHeights).hasSize(databaseSizeBeforeCreate + 1);
        DisplayHeight testDisplayHeight = displayHeights.get(displayHeights.size() - 1);
        assertThat(testDisplayHeight.getTypeStream()).isEqualTo(DEFAULT_TYPE_STREAM);
        assertThat(testDisplayHeight.getStreamer()).isEqualTo(DEFAULT_STREAMER);
        assertThat(testDisplayHeight.getValeur()).isEqualTo(DEFAULT_VALEUR);
        assertThat(testDisplayHeight.getDateSup()).isEqualTo(DEFAULT_DATE_SUP);

        // Validate the DisplayHeight in ElasticSearch
        DisplayHeight displayHeightEs = displayHeightSearchRepository.findOne(testDisplayHeight.getId());
        assertThat(displayHeightEs).isEqualToComparingFieldByField(testDisplayHeight);
    }

    @Test
    @Transactional
    public void getAllDisplayHeights() throws Exception {
        // Initialize the database
        displayHeightRepository.saveAndFlush(displayHeight);

        // Get all the displayHeights
        restDisplayHeightMockMvc.perform(get("/api/display-heights?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.[*].id").value(hasItem(displayHeight.getId().intValue())))
                .andExpect(jsonPath("$.[*].typeStream").value(hasItem(DEFAULT_TYPE_STREAM.toString())))
                .andExpect(jsonPath("$.[*].streamer").value(hasItem(DEFAULT_STREAMER.toString())))
                .andExpect(jsonPath("$.[*].valeur").value(hasItem(DEFAULT_VALEUR.toString())))
                .andExpect(jsonPath("$.[*].dateSup").value(hasItem(DEFAULT_DATE_SUP_STR)));
    }

    @Test
    @Transactional
    public void getDisplayHeight() throws Exception {
        // Initialize the database
        displayHeightRepository.saveAndFlush(displayHeight);

        // Get the displayHeight
        restDisplayHeightMockMvc.perform(get("/api/display-heights/{id}", displayHeight.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(displayHeight.getId().intValue()))
            .andExpect(jsonPath("$.typeStream").value(DEFAULT_TYPE_STREAM.toString()))
            .andExpect(jsonPath("$.streamer").value(DEFAULT_STREAMER.toString()))
            .andExpect(jsonPath("$.valeur").value(DEFAULT_VALEUR.toString()))
            .andExpect(jsonPath("$.dateSup").value(DEFAULT_DATE_SUP_STR));
    }

    @Test
    @Transactional
    public void getNonExistingDisplayHeight() throws Exception {
        // Get the displayHeight
        restDisplayHeightMockMvc.perform(get("/api/display-heights/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateDisplayHeight() throws Exception {
        // Initialize the database
        displayHeightRepository.saveAndFlush(displayHeight);
        displayHeightSearchRepository.save(displayHeight);
        int databaseSizeBeforeUpdate = displayHeightRepository.findAll().size();

        // Update the displayHeight
        DisplayHeight updatedDisplayHeight = displayHeightRepository.findOne(displayHeight.getId());
        updatedDisplayHeight
                .typeStream(UPDATED_TYPE_STREAM)
                .streamer(UPDATED_STREAMER)
                .valeur(UPDATED_VALEUR)
                .dateSup(UPDATED_DATE_SUP);
        DisplayHeightDTO displayHeightDTO = displayHeightMapper.displayHeightToDisplayHeightDTO(updatedDisplayHeight);

        restDisplayHeightMockMvc.perform(put("/api/display-heights")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(displayHeightDTO)))
                .andExpect(status().isOk());

        // Validate the DisplayHeight in the database
        List<DisplayHeight> displayHeights = displayHeightRepository.findAll();
        assertThat(displayHeights).hasSize(databaseSizeBeforeUpdate);
        DisplayHeight testDisplayHeight = displayHeights.get(displayHeights.size() - 1);
        assertThat(testDisplayHeight.getTypeStream()).isEqualTo(UPDATED_TYPE_STREAM);
        assertThat(testDisplayHeight.getStreamer()).isEqualTo(UPDATED_STREAMER);
        assertThat(testDisplayHeight.getValeur()).isEqualTo(UPDATED_VALEUR);
        assertThat(testDisplayHeight.getDateSup()).isEqualTo(UPDATED_DATE_SUP);

        // Validate the DisplayHeight in ElasticSearch
        DisplayHeight displayHeightEs = displayHeightSearchRepository.findOne(testDisplayHeight.getId());
        assertThat(displayHeightEs).isEqualToComparingFieldByField(testDisplayHeight);
    }

    @Test
    @Transactional
    public void deleteDisplayHeight() throws Exception {
        // Initialize the database
        displayHeightRepository.saveAndFlush(displayHeight);
        displayHeightSearchRepository.save(displayHeight);
        int databaseSizeBeforeDelete = displayHeightRepository.findAll().size();

        // Get the displayHeight
        restDisplayHeightMockMvc.perform(delete("/api/display-heights/{id}", displayHeight.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate ElasticSearch is empty
        boolean displayHeightExistsInEs = displayHeightSearchRepository.exists(displayHeight.getId());
        assertThat(displayHeightExistsInEs).isFalse();

        // Validate the database is empty
        List<DisplayHeight> displayHeights = displayHeightRepository.findAll();
        assertThat(displayHeights).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchDisplayHeight() throws Exception {
        // Initialize the database
        displayHeightRepository.saveAndFlush(displayHeight);
        displayHeightSearchRepository.save(displayHeight);

        // Search the displayHeight
        restDisplayHeightMockMvc.perform(get("/api/_search/display-heights?query=id:" + displayHeight.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(displayHeight.getId().intValue())))
            .andExpect(jsonPath("$.[*].typeStream").value(hasItem(DEFAULT_TYPE_STREAM.toString())))
            .andExpect(jsonPath("$.[*].streamer").value(hasItem(DEFAULT_STREAMER.toString())))
            .andExpect(jsonPath("$.[*].valeur").value(hasItem(DEFAULT_VALEUR.toString())))
            .andExpect(jsonPath("$.[*].dateSup").value(hasItem(DEFAULT_DATE_SUP_STR)));
    }
}
