package com.orange.myabstract.web.rest;

import com.orange.myabstract.AbstractApp;

import com.orange.myabstract.domain.CodecPrivateData;
import com.orange.myabstract.repository.CodecPrivateDataRepository;
import com.orange.myabstract.service.CodecPrivateDataService;
import com.orange.myabstract.repository.search.CodecPrivateDataSearchRepository;
import com.orange.myabstract.service.dto.CodecPrivateDataDTO;
import com.orange.myabstract.service.mapper.CodecPrivateDataMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the CodecPrivateDataResource REST controller.
 *
 * @see CodecPrivateDataResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = AbstractApp.class)
public class CodecPrivateDataResourceIntTest {

    private static final String DEFAULT_TYPE_STREAM = "AAAAAAAAAA";
    private static final String UPDATED_TYPE_STREAM = "BBBBBBBBBB";

    private static final String DEFAULT_VALEUR = "AAAAAAAAAA";
    private static final String UPDATED_VALEUR = "BBBBBBBBBB";

    private static final String DEFAULT_STREAMER = "AAAAAAAAAA";
    private static final String UPDATED_STREAMER = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_DATE_SUP = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_DATE_SUP = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final String DEFAULT_DATE_SUP_STR = DateTimeFormatter.ISO_INSTANT.format(DEFAULT_DATE_SUP);

    @Inject
    private CodecPrivateDataRepository codecPrivateDataRepository;

    @Inject
    private CodecPrivateDataMapper codecPrivateDataMapper;

    @Inject
    private CodecPrivateDataService codecPrivateDataService;

    @Inject
    private CodecPrivateDataSearchRepository codecPrivateDataSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Inject
    private EntityManager em;

    private MockMvc restCodecPrivateDataMockMvc;

    private CodecPrivateData codecPrivateData;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        CodecPrivateDataResource codecPrivateDataResource = new CodecPrivateDataResource();
        ReflectionTestUtils.setField(codecPrivateDataResource, "codecPrivateDataService", codecPrivateDataService);
        this.restCodecPrivateDataMockMvc = MockMvcBuilders.standaloneSetup(codecPrivateDataResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CodecPrivateData createEntity(EntityManager em) {
        CodecPrivateData codecPrivateData = new CodecPrivateData()
                .typeStream(DEFAULT_TYPE_STREAM)
                .valeur(DEFAULT_VALEUR)
                .streamer(DEFAULT_STREAMER)
                .dateSup(DEFAULT_DATE_SUP);
        return codecPrivateData;
    }

    @Before
    public void initTest() {
        codecPrivateDataSearchRepository.deleteAll();
        codecPrivateData = createEntity(em);
    }

    @Test
    @Transactional
    public void createCodecPrivateData() throws Exception {
        int databaseSizeBeforeCreate = codecPrivateDataRepository.findAll().size();

        // Create the CodecPrivateData
        CodecPrivateDataDTO codecPrivateDataDTO = codecPrivateDataMapper.codecPrivateDataToCodecPrivateDataDTO(codecPrivateData);

        restCodecPrivateDataMockMvc.perform(post("/api/codec-private-data")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(codecPrivateDataDTO)))
                .andExpect(status().isCreated());

        // Validate the CodecPrivateData in the database
        List<CodecPrivateData> codecPrivateData = codecPrivateDataRepository.findAll();
        assertThat(codecPrivateData).hasSize(databaseSizeBeforeCreate + 1);
        CodecPrivateData testCodecPrivateData = codecPrivateData.get(codecPrivateData.size() - 1);
        assertThat(testCodecPrivateData.getTypeStream()).isEqualTo(DEFAULT_TYPE_STREAM);
        assertThat(testCodecPrivateData.getValeur()).isEqualTo(DEFAULT_VALEUR);
        assertThat(testCodecPrivateData.getStreamer()).isEqualTo(DEFAULT_STREAMER);
        assertThat(testCodecPrivateData.getDateSup()).isEqualTo(DEFAULT_DATE_SUP);

        // Validate the CodecPrivateData in ElasticSearch
        CodecPrivateData codecPrivateDataEs = codecPrivateDataSearchRepository.findOne(testCodecPrivateData.getId());
        assertThat(codecPrivateDataEs).isEqualToComparingFieldByField(testCodecPrivateData);
    }

    @Test
    @Transactional
    public void getAllCodecPrivateData() throws Exception {
        // Initialize the database
        codecPrivateDataRepository.saveAndFlush(codecPrivateData);

        // Get all the codecPrivateData
        restCodecPrivateDataMockMvc.perform(get("/api/codec-private-data?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.[*].id").value(hasItem(codecPrivateData.getId().intValue())))
                .andExpect(jsonPath("$.[*].typeStream").value(hasItem(DEFAULT_TYPE_STREAM.toString())))
                .andExpect(jsonPath("$.[*].valeur").value(hasItem(DEFAULT_VALEUR.toString())))
                .andExpect(jsonPath("$.[*].streamer").value(hasItem(DEFAULT_STREAMER.toString())))
                .andExpect(jsonPath("$.[*].dateSup").value(hasItem(DEFAULT_DATE_SUP_STR)));
    }

    @Test
    @Transactional
    public void getCodecPrivateData() throws Exception {
        // Initialize the database
        codecPrivateDataRepository.saveAndFlush(codecPrivateData);

        // Get the codecPrivateData
        restCodecPrivateDataMockMvc.perform(get("/api/codec-private-data/{id}", codecPrivateData.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(codecPrivateData.getId().intValue()))
            .andExpect(jsonPath("$.typeStream").value(DEFAULT_TYPE_STREAM.toString()))
            .andExpect(jsonPath("$.valeur").value(DEFAULT_VALEUR.toString()))
            .andExpect(jsonPath("$.streamer").value(DEFAULT_STREAMER.toString()))
            .andExpect(jsonPath("$.dateSup").value(DEFAULT_DATE_SUP_STR));
    }

    @Test
    @Transactional
    public void getNonExistingCodecPrivateData() throws Exception {
        // Get the codecPrivateData
        restCodecPrivateDataMockMvc.perform(get("/api/codec-private-data/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCodecPrivateData() throws Exception {
        // Initialize the database
        codecPrivateDataRepository.saveAndFlush(codecPrivateData);
        codecPrivateDataSearchRepository.save(codecPrivateData);
        int databaseSizeBeforeUpdate = codecPrivateDataRepository.findAll().size();

        // Update the codecPrivateData
        CodecPrivateData updatedCodecPrivateData = codecPrivateDataRepository.findOne(codecPrivateData.getId());
        updatedCodecPrivateData
                .typeStream(UPDATED_TYPE_STREAM)
                .valeur(UPDATED_VALEUR)
                .streamer(UPDATED_STREAMER)
                .dateSup(UPDATED_DATE_SUP);
        CodecPrivateDataDTO codecPrivateDataDTO = codecPrivateDataMapper.codecPrivateDataToCodecPrivateDataDTO(updatedCodecPrivateData);

        restCodecPrivateDataMockMvc.perform(put("/api/codec-private-data")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(codecPrivateDataDTO)))
                .andExpect(status().isOk());

        // Validate the CodecPrivateData in the database
        List<CodecPrivateData> codecPrivateData = codecPrivateDataRepository.findAll();
        assertThat(codecPrivateData).hasSize(databaseSizeBeforeUpdate);
        CodecPrivateData testCodecPrivateData = codecPrivateData.get(codecPrivateData.size() - 1);
        assertThat(testCodecPrivateData.getTypeStream()).isEqualTo(UPDATED_TYPE_STREAM);
        assertThat(testCodecPrivateData.getValeur()).isEqualTo(UPDATED_VALEUR);
        assertThat(testCodecPrivateData.getStreamer()).isEqualTo(UPDATED_STREAMER);
        assertThat(testCodecPrivateData.getDateSup()).isEqualTo(UPDATED_DATE_SUP);

        // Validate the CodecPrivateData in ElasticSearch
        CodecPrivateData codecPrivateDataEs = codecPrivateDataSearchRepository.findOne(testCodecPrivateData.getId());
        assertThat(codecPrivateDataEs).isEqualToComparingFieldByField(testCodecPrivateData);
    }

    @Test
    @Transactional
    public void deleteCodecPrivateData() throws Exception {
        // Initialize the database
        codecPrivateDataRepository.saveAndFlush(codecPrivateData);
        codecPrivateDataSearchRepository.save(codecPrivateData);
        int databaseSizeBeforeDelete = codecPrivateDataRepository.findAll().size();

        // Get the codecPrivateData
        restCodecPrivateDataMockMvc.perform(delete("/api/codec-private-data/{id}", codecPrivateData.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate ElasticSearch is empty
        boolean codecPrivateDataExistsInEs = codecPrivateDataSearchRepository.exists(codecPrivateData.getId());
        assertThat(codecPrivateDataExistsInEs).isFalse();

        // Validate the database is empty
        List<CodecPrivateData> codecPrivateData = codecPrivateDataRepository.findAll();
        assertThat(codecPrivateData).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchCodecPrivateData() throws Exception {
        // Initialize the database
        codecPrivateDataRepository.saveAndFlush(codecPrivateData);
        codecPrivateDataSearchRepository.save(codecPrivateData);

        // Search the codecPrivateData
        restCodecPrivateDataMockMvc.perform(get("/api/_search/codec-private-data?query=id:" + codecPrivateData.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(codecPrivateData.getId().intValue())))
            .andExpect(jsonPath("$.[*].typeStream").value(hasItem(DEFAULT_TYPE_STREAM.toString())))
            .andExpect(jsonPath("$.[*].valeur").value(hasItem(DEFAULT_VALEUR.toString())))
            .andExpect(jsonPath("$.[*].streamer").value(hasItem(DEFAULT_STREAMER.toString())))
            .andExpect(jsonPath("$.[*].dateSup").value(hasItem(DEFAULT_DATE_SUP_STR)));
    }
}
