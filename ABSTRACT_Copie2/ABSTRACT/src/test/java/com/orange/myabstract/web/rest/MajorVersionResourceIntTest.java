package com.orange.myabstract.web.rest;

import com.orange.myabstract.AbstractApp;

import com.orange.myabstract.domain.MajorVersion;
import com.orange.myabstract.repository.MajorVersionRepository;
import com.orange.myabstract.service.MajorVersionService;
import com.orange.myabstract.repository.search.MajorVersionSearchRepository;
import com.orange.myabstract.service.dto.MajorVersionDTO;
import com.orange.myabstract.service.mapper.MajorVersionMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the MajorVersionResource REST controller.
 *
 * @see MajorVersionResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = AbstractApp.class)
public class MajorVersionResourceIntTest {

    private static final String DEFAULT_VALEUR = "AAAAAAAAAA";
    private static final String UPDATED_VALEUR = "BBBBBBBBBB";

    private static final String DEFAULT_STREAMER = "AAAAAAAAAA";
    private static final String UPDATED_STREAMER = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_DATE_SUP = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_DATE_SUP = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final String DEFAULT_DATE_SUP_STR = DateTimeFormatter.ISO_INSTANT.format(DEFAULT_DATE_SUP);

    @Inject
    private MajorVersionRepository majorVersionRepository;

    @Inject
    private MajorVersionMapper majorVersionMapper;

    @Inject
    private MajorVersionService majorVersionService;

    @Inject
    private MajorVersionSearchRepository majorVersionSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Inject
    private EntityManager em;

    private MockMvc restMajorVersionMockMvc;

    private MajorVersion majorVersion;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        MajorVersionResource majorVersionResource = new MajorVersionResource();
        ReflectionTestUtils.setField(majorVersionResource, "majorVersionService", majorVersionService);
        this.restMajorVersionMockMvc = MockMvcBuilders.standaloneSetup(majorVersionResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static MajorVersion createEntity(EntityManager em) {
        MajorVersion majorVersion = new MajorVersion()
                .valeur(DEFAULT_VALEUR)
                .streamer(DEFAULT_STREAMER)
                .dateSup(DEFAULT_DATE_SUP);
        return majorVersion;
    }

    @Before
    public void initTest() {
        majorVersionSearchRepository.deleteAll();
        majorVersion = createEntity(em);
    }

    @Test
    @Transactional
    public void createMajorVersion() throws Exception {
        int databaseSizeBeforeCreate = majorVersionRepository.findAll().size();

        // Create the MajorVersion
        MajorVersionDTO majorVersionDTO = majorVersionMapper.majorVersionToMajorVersionDTO(majorVersion);

        restMajorVersionMockMvc.perform(post("/api/major-versions")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(majorVersionDTO)))
                .andExpect(status().isCreated());

        // Validate the MajorVersion in the database
        List<MajorVersion> majorVersions = majorVersionRepository.findAll();
        assertThat(majorVersions).hasSize(databaseSizeBeforeCreate + 1);
        MajorVersion testMajorVersion = majorVersions.get(majorVersions.size() - 1);
        assertThat(testMajorVersion.getValeur()).isEqualTo(DEFAULT_VALEUR);
        assertThat(testMajorVersion.getStreamer()).isEqualTo(DEFAULT_STREAMER);
        assertThat(testMajorVersion.getDateSup()).isEqualTo(DEFAULT_DATE_SUP);

        // Validate the MajorVersion in ElasticSearch
        MajorVersion majorVersionEs = majorVersionSearchRepository.findOne(testMajorVersion.getId());
        assertThat(majorVersionEs).isEqualToComparingFieldByField(testMajorVersion);
    }

    @Test
    @Transactional
    public void getAllMajorVersions() throws Exception {
        // Initialize the database
        majorVersionRepository.saveAndFlush(majorVersion);

        // Get all the majorVersions
        restMajorVersionMockMvc.perform(get("/api/major-versions?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.[*].id").value(hasItem(majorVersion.getId().intValue())))
                .andExpect(jsonPath("$.[*].valeur").value(hasItem(DEFAULT_VALEUR.toString())))
                .andExpect(jsonPath("$.[*].streamer").value(hasItem(DEFAULT_STREAMER.toString())))
                .andExpect(jsonPath("$.[*].dateSup").value(hasItem(DEFAULT_DATE_SUP_STR)));
    }

    @Test
    @Transactional
    public void getMajorVersion() throws Exception {
        // Initialize the database
        majorVersionRepository.saveAndFlush(majorVersion);

        // Get the majorVersion
        restMajorVersionMockMvc.perform(get("/api/major-versions/{id}", majorVersion.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(majorVersion.getId().intValue()))
            .andExpect(jsonPath("$.valeur").value(DEFAULT_VALEUR.toString()))
            .andExpect(jsonPath("$.streamer").value(DEFAULT_STREAMER.toString()))
            .andExpect(jsonPath("$.dateSup").value(DEFAULT_DATE_SUP_STR));
    }

    @Test
    @Transactional
    public void getNonExistingMajorVersion() throws Exception {
        // Get the majorVersion
        restMajorVersionMockMvc.perform(get("/api/major-versions/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateMajorVersion() throws Exception {
        // Initialize the database
        majorVersionRepository.saveAndFlush(majorVersion);
        majorVersionSearchRepository.save(majorVersion);
        int databaseSizeBeforeUpdate = majorVersionRepository.findAll().size();

        // Update the majorVersion
        MajorVersion updatedMajorVersion = majorVersionRepository.findOne(majorVersion.getId());
        updatedMajorVersion
                .valeur(UPDATED_VALEUR)
                .streamer(UPDATED_STREAMER)
                .dateSup(UPDATED_DATE_SUP);
        MajorVersionDTO majorVersionDTO = majorVersionMapper.majorVersionToMajorVersionDTO(updatedMajorVersion);

        restMajorVersionMockMvc.perform(put("/api/major-versions")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(majorVersionDTO)))
                .andExpect(status().isOk());

        // Validate the MajorVersion in the database
        List<MajorVersion> majorVersions = majorVersionRepository.findAll();
        assertThat(majorVersions).hasSize(databaseSizeBeforeUpdate);
        MajorVersion testMajorVersion = majorVersions.get(majorVersions.size() - 1);
        assertThat(testMajorVersion.getValeur()).isEqualTo(UPDATED_VALEUR);
        assertThat(testMajorVersion.getStreamer()).isEqualTo(UPDATED_STREAMER);
        assertThat(testMajorVersion.getDateSup()).isEqualTo(UPDATED_DATE_SUP);

        // Validate the MajorVersion in ElasticSearch
        MajorVersion majorVersionEs = majorVersionSearchRepository.findOne(testMajorVersion.getId());
        assertThat(majorVersionEs).isEqualToComparingFieldByField(testMajorVersion);
    }

    @Test
    @Transactional
    public void deleteMajorVersion() throws Exception {
        // Initialize the database
        majorVersionRepository.saveAndFlush(majorVersion);
        majorVersionSearchRepository.save(majorVersion);
        int databaseSizeBeforeDelete = majorVersionRepository.findAll().size();

        // Get the majorVersion
        restMajorVersionMockMvc.perform(delete("/api/major-versions/{id}", majorVersion.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate ElasticSearch is empty
        boolean majorVersionExistsInEs = majorVersionSearchRepository.exists(majorVersion.getId());
        assertThat(majorVersionExistsInEs).isFalse();

        // Validate the database is empty
        List<MajorVersion> majorVersions = majorVersionRepository.findAll();
        assertThat(majorVersions).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchMajorVersion() throws Exception {
        // Initialize the database
        majorVersionRepository.saveAndFlush(majorVersion);
        majorVersionSearchRepository.save(majorVersion);

        // Search the majorVersion
        restMajorVersionMockMvc.perform(get("/api/_search/major-versions?query=id:" + majorVersion.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(majorVersion.getId().intValue())))
            .andExpect(jsonPath("$.[*].valeur").value(hasItem(DEFAULT_VALEUR.toString())))
            .andExpect(jsonPath("$.[*].streamer").value(hasItem(DEFAULT_STREAMER.toString())))
            .andExpect(jsonPath("$.[*].dateSup").value(hasItem(DEFAULT_DATE_SUP_STR)));
    }
}
