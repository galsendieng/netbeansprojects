package com.orange.myabstract.web.rest;

import com.orange.myabstract.AbstractApp;

import com.orange.myabstract.domain.Duration;
import com.orange.myabstract.repository.DurationRepository;
import com.orange.myabstract.service.DurationService;
import com.orange.myabstract.repository.search.DurationSearchRepository;
import com.orange.myabstract.service.dto.DurationDTO;
import com.orange.myabstract.service.mapper.DurationMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the DurationResource REST controller.
 *
 * @see DurationResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = AbstractApp.class)
public class DurationResourceIntTest {

    private static final String DEFAULT_VALEUR = "AAAAAAAAAA";
    private static final String UPDATED_VALEUR = "BBBBBBBBBB";

    private static final String DEFAULT_STREAMER = "AAAAAAAAAA";
    private static final String UPDATED_STREAMER = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_DATE_SUP = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_DATE_SUP = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final String DEFAULT_DATE_SUP_STR = DateTimeFormatter.ISO_INSTANT.format(DEFAULT_DATE_SUP);

    @Inject
    private DurationRepository durationRepository;

    @Inject
    private DurationMapper durationMapper;

    @Inject
    private DurationService durationService;

    @Inject
    private DurationSearchRepository durationSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Inject
    private EntityManager em;

    private MockMvc restDurationMockMvc;

    private Duration duration;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        DurationResource durationResource = new DurationResource();
        ReflectionTestUtils.setField(durationResource, "durationService", durationService);
        this.restDurationMockMvc = MockMvcBuilders.standaloneSetup(durationResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Duration createEntity(EntityManager em) {
        Duration duration = new Duration()
                .valeur(DEFAULT_VALEUR)
                .streamer(DEFAULT_STREAMER)
                .dateSup(DEFAULT_DATE_SUP);
        return duration;
    }

    @Before
    public void initTest() {
        durationSearchRepository.deleteAll();
        duration = createEntity(em);
    }

    @Test
    @Transactional
    public void createDuration() throws Exception {
        int databaseSizeBeforeCreate = durationRepository.findAll().size();

        // Create the Duration
        DurationDTO durationDTO = durationMapper.durationToDurationDTO(duration);

        restDurationMockMvc.perform(post("/api/durations")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(durationDTO)))
                .andExpect(status().isCreated());

        // Validate the Duration in the database
        List<Duration> durations = durationRepository.findAll();
        assertThat(durations).hasSize(databaseSizeBeforeCreate + 1);
        Duration testDuration = durations.get(durations.size() - 1);
        assertThat(testDuration.getValeur()).isEqualTo(DEFAULT_VALEUR);
        assertThat(testDuration.getStreamer()).isEqualTo(DEFAULT_STREAMER);
        assertThat(testDuration.getDateSup()).isEqualTo(DEFAULT_DATE_SUP);

        // Validate the Duration in ElasticSearch
        Duration durationEs = durationSearchRepository.findOne(testDuration.getId());
        assertThat(durationEs).isEqualToComparingFieldByField(testDuration);
    }

    @Test
    @Transactional
    public void getAllDurations() throws Exception {
        // Initialize the database
        durationRepository.saveAndFlush(duration);

        // Get all the durations
        restDurationMockMvc.perform(get("/api/durations?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.[*].id").value(hasItem(duration.getId().intValue())))
                .andExpect(jsonPath("$.[*].valeur").value(hasItem(DEFAULT_VALEUR.toString())))
                .andExpect(jsonPath("$.[*].streamer").value(hasItem(DEFAULT_STREAMER.toString())))
                .andExpect(jsonPath("$.[*].dateSup").value(hasItem(DEFAULT_DATE_SUP_STR)));
    }

    @Test
    @Transactional
    public void getDuration() throws Exception {
        // Initialize the database
        durationRepository.saveAndFlush(duration);

        // Get the duration
        restDurationMockMvc.perform(get("/api/durations/{id}", duration.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(duration.getId().intValue()))
            .andExpect(jsonPath("$.valeur").value(DEFAULT_VALEUR.toString()))
            .andExpect(jsonPath("$.streamer").value(DEFAULT_STREAMER.toString()))
            .andExpect(jsonPath("$.dateSup").value(DEFAULT_DATE_SUP_STR));
    }

    @Test
    @Transactional
    public void getNonExistingDuration() throws Exception {
        // Get the duration
        restDurationMockMvc.perform(get("/api/durations/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateDuration() throws Exception {
        // Initialize the database
        durationRepository.saveAndFlush(duration);
        durationSearchRepository.save(duration);
        int databaseSizeBeforeUpdate = durationRepository.findAll().size();

        // Update the duration
        Duration updatedDuration = durationRepository.findOne(duration.getId());
        updatedDuration
                .valeur(UPDATED_VALEUR)
                .streamer(UPDATED_STREAMER)
                .dateSup(UPDATED_DATE_SUP);
        DurationDTO durationDTO = durationMapper.durationToDurationDTO(updatedDuration);

        restDurationMockMvc.perform(put("/api/durations")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(durationDTO)))
                .andExpect(status().isOk());

        // Validate the Duration in the database
        List<Duration> durations = durationRepository.findAll();
        assertThat(durations).hasSize(databaseSizeBeforeUpdate);
        Duration testDuration = durations.get(durations.size() - 1);
        assertThat(testDuration.getValeur()).isEqualTo(UPDATED_VALEUR);
        assertThat(testDuration.getStreamer()).isEqualTo(UPDATED_STREAMER);
        assertThat(testDuration.getDateSup()).isEqualTo(UPDATED_DATE_SUP);

        // Validate the Duration in ElasticSearch
        Duration durationEs = durationSearchRepository.findOne(testDuration.getId());
        assertThat(durationEs).isEqualToComparingFieldByField(testDuration);
    }

    @Test
    @Transactional
    public void deleteDuration() throws Exception {
        // Initialize the database
        durationRepository.saveAndFlush(duration);
        durationSearchRepository.save(duration);
        int databaseSizeBeforeDelete = durationRepository.findAll().size();

        // Get the duration
        restDurationMockMvc.perform(delete("/api/durations/{id}", duration.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate ElasticSearch is empty
        boolean durationExistsInEs = durationSearchRepository.exists(duration.getId());
        assertThat(durationExistsInEs).isFalse();

        // Validate the database is empty
        List<Duration> durations = durationRepository.findAll();
        assertThat(durations).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchDuration() throws Exception {
        // Initialize the database
        durationRepository.saveAndFlush(duration);
        durationSearchRepository.save(duration);

        // Search the duration
        restDurationMockMvc.perform(get("/api/_search/durations?query=id:" + duration.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(duration.getId().intValue())))
            .andExpect(jsonPath("$.[*].valeur").value(hasItem(DEFAULT_VALEUR.toString())))
            .andExpect(jsonPath("$.[*].streamer").value(hasItem(DEFAULT_STREAMER.toString())))
            .andExpect(jsonPath("$.[*].dateSup").value(hasItem(DEFAULT_DATE_SUP_STR)));
    }
}
