package com.orange.myabstract.web.rest;

import com.orange.myabstract.AbstractApp;

import com.orange.myabstract.domain.Importance;
import com.orange.myabstract.repository.ImportanceRepository;
import com.orange.myabstract.service.ImportanceService;
import com.orange.myabstract.repository.search.ImportanceSearchRepository;
import com.orange.myabstract.service.dto.ImportanceDTO;
import com.orange.myabstract.service.mapper.ImportanceMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ImportanceResource REST controller.
 *
 * @see ImportanceResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = AbstractApp.class)
public class ImportanceResourceIntTest {

    private static final String DEFAULT_NIVEAU = "AAAAAAAAAA";
    private static final String UPDATED_NIVEAU = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    @Inject
    private ImportanceRepository importanceRepository;

    @Inject
    private ImportanceMapper importanceMapper;

    @Inject
    private ImportanceService importanceService;

    @Inject
    private ImportanceSearchRepository importanceSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Inject
    private EntityManager em;

    private MockMvc restImportanceMockMvc;

    private Importance importance;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        ImportanceResource importanceResource = new ImportanceResource();
        ReflectionTestUtils.setField(importanceResource, "importanceService", importanceService);
        this.restImportanceMockMvc = MockMvcBuilders.standaloneSetup(importanceResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Importance createEntity(EntityManager em) {
        Importance importance = new Importance()
                .niveau(DEFAULT_NIVEAU)
                .description(DEFAULT_DESCRIPTION);
        return importance;
    }

    @Before
    public void initTest() {
        importanceSearchRepository.deleteAll();
        importance = createEntity(em);
    }

    @Test
    @Transactional
    public void createImportance() throws Exception {
        int databaseSizeBeforeCreate = importanceRepository.findAll().size();

        // Create the Importance
        ImportanceDTO importanceDTO = importanceMapper.importanceToImportanceDTO(importance);

        restImportanceMockMvc.perform(post("/api/importances")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(importanceDTO)))
                .andExpect(status().isCreated());

        // Validate the Importance in the database
        List<Importance> importances = importanceRepository.findAll();
        assertThat(importances).hasSize(databaseSizeBeforeCreate + 1);
        Importance testImportance = importances.get(importances.size() - 1);
        assertThat(testImportance.getNiveau()).isEqualTo(DEFAULT_NIVEAU);
        assertThat(testImportance.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);

        // Validate the Importance in ElasticSearch
        Importance importanceEs = importanceSearchRepository.findOne(testImportance.getId());
        assertThat(importanceEs).isEqualToComparingFieldByField(testImportance);
    }

    @Test
    @Transactional
    public void checkNiveauIsRequired() throws Exception {
        int databaseSizeBeforeTest = importanceRepository.findAll().size();
        // set the field null
        importance.setNiveau(null);

        // Create the Importance, which fails.
        ImportanceDTO importanceDTO = importanceMapper.importanceToImportanceDTO(importance);

        restImportanceMockMvc.perform(post("/api/importances")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(importanceDTO)))
                .andExpect(status().isBadRequest());

        List<Importance> importances = importanceRepository.findAll();
        assertThat(importances).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDescriptionIsRequired() throws Exception {
        int databaseSizeBeforeTest = importanceRepository.findAll().size();
        // set the field null
        importance.setDescription(null);

        // Create the Importance, which fails.
        ImportanceDTO importanceDTO = importanceMapper.importanceToImportanceDTO(importance);

        restImportanceMockMvc.perform(post("/api/importances")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(importanceDTO)))
                .andExpect(status().isBadRequest());

        List<Importance> importances = importanceRepository.findAll();
        assertThat(importances).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllImportances() throws Exception {
        // Initialize the database
        importanceRepository.saveAndFlush(importance);

        // Get all the importances
        restImportanceMockMvc.perform(get("/api/importances?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.[*].id").value(hasItem(importance.getId().intValue())))
                .andExpect(jsonPath("$.[*].niveau").value(hasItem(DEFAULT_NIVEAU.toString())))
                .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())));
    }

    @Test
    @Transactional
    public void getImportance() throws Exception {
        // Initialize the database
        importanceRepository.saveAndFlush(importance);

        // Get the importance
        restImportanceMockMvc.perform(get("/api/importances/{id}", importance.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(importance.getId().intValue()))
            .andExpect(jsonPath("$.niveau").value(DEFAULT_NIVEAU.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingImportance() throws Exception {
        // Get the importance
        restImportanceMockMvc.perform(get("/api/importances/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateImportance() throws Exception {
        // Initialize the database
        importanceRepository.saveAndFlush(importance);
        importanceSearchRepository.save(importance);
        int databaseSizeBeforeUpdate = importanceRepository.findAll().size();

        // Update the importance
        Importance updatedImportance = importanceRepository.findOne(importance.getId());
        updatedImportance
                .niveau(UPDATED_NIVEAU)
                .description(UPDATED_DESCRIPTION);
        ImportanceDTO importanceDTO = importanceMapper.importanceToImportanceDTO(updatedImportance);

        restImportanceMockMvc.perform(put("/api/importances")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(importanceDTO)))
                .andExpect(status().isOk());

        // Validate the Importance in the database
        List<Importance> importances = importanceRepository.findAll();
        assertThat(importances).hasSize(databaseSizeBeforeUpdate);
        Importance testImportance = importances.get(importances.size() - 1);
        assertThat(testImportance.getNiveau()).isEqualTo(UPDATED_NIVEAU);
        assertThat(testImportance.getDescription()).isEqualTo(UPDATED_DESCRIPTION);

        // Validate the Importance in ElasticSearch
        Importance importanceEs = importanceSearchRepository.findOne(testImportance.getId());
        assertThat(importanceEs).isEqualToComparingFieldByField(testImportance);
    }

    @Test
    @Transactional
    public void deleteImportance() throws Exception {
        // Initialize the database
        importanceRepository.saveAndFlush(importance);
        importanceSearchRepository.save(importance);
        int databaseSizeBeforeDelete = importanceRepository.findAll().size();

        // Get the importance
        restImportanceMockMvc.perform(delete("/api/importances/{id}", importance.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate ElasticSearch is empty
        boolean importanceExistsInEs = importanceSearchRepository.exists(importance.getId());
        assertThat(importanceExistsInEs).isFalse();

        // Validate the database is empty
        List<Importance> importances = importanceRepository.findAll();
        assertThat(importances).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchImportance() throws Exception {
        // Initialize the database
        importanceRepository.saveAndFlush(importance);
        importanceSearchRepository.save(importance);

        // Search the importance
        restImportanceMockMvc.perform(get("/api/_search/importances?query=id:" + importance.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(importance.getId().intValue())))
            .andExpect(jsonPath("$.[*].niveau").value(hasItem(DEFAULT_NIVEAU.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())));
    }
}
