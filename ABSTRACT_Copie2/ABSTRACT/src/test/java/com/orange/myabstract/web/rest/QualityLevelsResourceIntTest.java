package com.orange.myabstract.web.rest;

import com.orange.myabstract.AbstractApp;

import com.orange.myabstract.domain.QualityLevels;
import com.orange.myabstract.repository.QualityLevelsRepository;
import com.orange.myabstract.service.QualityLevelsService;
import com.orange.myabstract.repository.search.QualityLevelsSearchRepository;
import com.orange.myabstract.service.dto.QualityLevelsDTO;
import com.orange.myabstract.service.mapper.QualityLevelsMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the QualityLevelsResource REST controller.
 *
 * @see QualityLevelsResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = AbstractApp.class)
public class QualityLevelsResourceIntTest {

    private static final String DEFAULT_TYPE_STREAM = "AAAAAAAAAA";
    private static final String UPDATED_TYPE_STREAM = "BBBBBBBBBB";

    private static final String DEFAULT_STREAMER = "AAAAAAAAAA";
    private static final String UPDATED_STREAMER = "BBBBBBBBBB";

    private static final String DEFAULT_VALEUR = "AAAAAAAAAA";
    private static final String UPDATED_VALEUR = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_DATE_SUP = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_DATE_SUP = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final String DEFAULT_DATE_SUP_STR = DateTimeFormatter.ISO_INSTANT.format(DEFAULT_DATE_SUP);

    @Inject
    private QualityLevelsRepository qualityLevelsRepository;

    @Inject
    private QualityLevelsMapper qualityLevelsMapper;

    @Inject
    private QualityLevelsService qualityLevelsService;

    @Inject
    private QualityLevelsSearchRepository qualityLevelsSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Inject
    private EntityManager em;

    private MockMvc restQualityLevelsMockMvc;

    private QualityLevels qualityLevels;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        QualityLevelsResource qualityLevelsResource = new QualityLevelsResource();
        ReflectionTestUtils.setField(qualityLevelsResource, "qualityLevelsService", qualityLevelsService);
        this.restQualityLevelsMockMvc = MockMvcBuilders.standaloneSetup(qualityLevelsResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static QualityLevels createEntity(EntityManager em) {
        QualityLevels qualityLevels = new QualityLevels()
                .typeStream(DEFAULT_TYPE_STREAM)
                .streamer(DEFAULT_STREAMER)
                .valeur(DEFAULT_VALEUR)
                .dateSup(DEFAULT_DATE_SUP);
        return qualityLevels;
    }

    @Before
    public void initTest() {
        qualityLevelsSearchRepository.deleteAll();
        qualityLevels = createEntity(em);
    }

    @Test
    @Transactional
    public void createQualityLevels() throws Exception {
        int databaseSizeBeforeCreate = qualityLevelsRepository.findAll().size();

        // Create the QualityLevels
        QualityLevelsDTO qualityLevelsDTO = qualityLevelsMapper.qualityLevelsToQualityLevelsDTO(qualityLevels);

        restQualityLevelsMockMvc.perform(post("/api/quality-levels")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(qualityLevelsDTO)))
                .andExpect(status().isCreated());

        // Validate the QualityLevels in the database
        List<QualityLevels> qualityLevels = qualityLevelsRepository.findAll();
        assertThat(qualityLevels).hasSize(databaseSizeBeforeCreate + 1);
        QualityLevels testQualityLevels = qualityLevels.get(qualityLevels.size() - 1);
        assertThat(testQualityLevels.getTypeStream()).isEqualTo(DEFAULT_TYPE_STREAM);
        assertThat(testQualityLevels.getStreamer()).isEqualTo(DEFAULT_STREAMER);
        assertThat(testQualityLevels.getValeur()).isEqualTo(DEFAULT_VALEUR);
        assertThat(testQualityLevels.getDateSup()).isEqualTo(DEFAULT_DATE_SUP);

        // Validate the QualityLevels in ElasticSearch
        QualityLevels qualityLevelsEs = qualityLevelsSearchRepository.findOne(testQualityLevels.getId());
        assertThat(qualityLevelsEs).isEqualToComparingFieldByField(testQualityLevels);
    }

    @Test
    @Transactional
    public void getAllQualityLevels() throws Exception {
        // Initialize the database
        qualityLevelsRepository.saveAndFlush(qualityLevels);

        // Get all the qualityLevels
        restQualityLevelsMockMvc.perform(get("/api/quality-levels?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.[*].id").value(hasItem(qualityLevels.getId().intValue())))
                .andExpect(jsonPath("$.[*].typeStream").value(hasItem(DEFAULT_TYPE_STREAM.toString())))
                .andExpect(jsonPath("$.[*].streamer").value(hasItem(DEFAULT_STREAMER.toString())))
                .andExpect(jsonPath("$.[*].valeur").value(hasItem(DEFAULT_VALEUR.toString())))
                .andExpect(jsonPath("$.[*].dateSup").value(hasItem(DEFAULT_DATE_SUP_STR)));
    }

    @Test
    @Transactional
    public void getQualityLevels() throws Exception {
        // Initialize the database
        qualityLevelsRepository.saveAndFlush(qualityLevels);

        // Get the qualityLevels
        restQualityLevelsMockMvc.perform(get("/api/quality-levels/{id}", qualityLevels.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(qualityLevels.getId().intValue()))
            .andExpect(jsonPath("$.typeStream").value(DEFAULT_TYPE_STREAM.toString()))
            .andExpect(jsonPath("$.streamer").value(DEFAULT_STREAMER.toString()))
            .andExpect(jsonPath("$.valeur").value(DEFAULT_VALEUR.toString()))
            .andExpect(jsonPath("$.dateSup").value(DEFAULT_DATE_SUP_STR));
    }

    @Test
    @Transactional
    public void getNonExistingQualityLevels() throws Exception {
        // Get the qualityLevels
        restQualityLevelsMockMvc.perform(get("/api/quality-levels/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateQualityLevels() throws Exception {
        // Initialize the database
        qualityLevelsRepository.saveAndFlush(qualityLevels);
        qualityLevelsSearchRepository.save(qualityLevels);
        int databaseSizeBeforeUpdate = qualityLevelsRepository.findAll().size();

        // Update the qualityLevels
        QualityLevels updatedQualityLevels = qualityLevelsRepository.findOne(qualityLevels.getId());
        updatedQualityLevels
                .typeStream(UPDATED_TYPE_STREAM)
                .streamer(UPDATED_STREAMER)
                .valeur(UPDATED_VALEUR)
                .dateSup(UPDATED_DATE_SUP);
        QualityLevelsDTO qualityLevelsDTO = qualityLevelsMapper.qualityLevelsToQualityLevelsDTO(updatedQualityLevels);

        restQualityLevelsMockMvc.perform(put("/api/quality-levels")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(qualityLevelsDTO)))
                .andExpect(status().isOk());

        // Validate the QualityLevels in the database
        List<QualityLevels> qualityLevels = qualityLevelsRepository.findAll();
        assertThat(qualityLevels).hasSize(databaseSizeBeforeUpdate);
        QualityLevels testQualityLevels = qualityLevels.get(qualityLevels.size() - 1);
        assertThat(testQualityLevels.getTypeStream()).isEqualTo(UPDATED_TYPE_STREAM);
        assertThat(testQualityLevels.getStreamer()).isEqualTo(UPDATED_STREAMER);
        assertThat(testQualityLevels.getValeur()).isEqualTo(UPDATED_VALEUR);
        assertThat(testQualityLevels.getDateSup()).isEqualTo(UPDATED_DATE_SUP);

        // Validate the QualityLevels in ElasticSearch
        QualityLevels qualityLevelsEs = qualityLevelsSearchRepository.findOne(testQualityLevels.getId());
        assertThat(qualityLevelsEs).isEqualToComparingFieldByField(testQualityLevels);
    }

    @Test
    @Transactional
    public void deleteQualityLevels() throws Exception {
        // Initialize the database
        qualityLevelsRepository.saveAndFlush(qualityLevels);
        qualityLevelsSearchRepository.save(qualityLevels);
        int databaseSizeBeforeDelete = qualityLevelsRepository.findAll().size();

        // Get the qualityLevels
        restQualityLevelsMockMvc.perform(delete("/api/quality-levels/{id}", qualityLevels.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate ElasticSearch is empty
        boolean qualityLevelsExistsInEs = qualityLevelsSearchRepository.exists(qualityLevels.getId());
        assertThat(qualityLevelsExistsInEs).isFalse();

        // Validate the database is empty
        List<QualityLevels> qualityLevels = qualityLevelsRepository.findAll();
        assertThat(qualityLevels).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchQualityLevels() throws Exception {
        // Initialize the database
        qualityLevelsRepository.saveAndFlush(qualityLevels);
        qualityLevelsSearchRepository.save(qualityLevels);

        // Search the qualityLevels
        restQualityLevelsMockMvc.perform(get("/api/_search/quality-levels?query=id:" + qualityLevels.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(qualityLevels.getId().intValue())))
            .andExpect(jsonPath("$.[*].typeStream").value(hasItem(DEFAULT_TYPE_STREAM.toString())))
            .andExpect(jsonPath("$.[*].streamer").value(hasItem(DEFAULT_STREAMER.toString())))
            .andExpect(jsonPath("$.[*].valeur").value(hasItem(DEFAULT_VALEUR.toString())))
            .andExpect(jsonPath("$.[*].dateSup").value(hasItem(DEFAULT_DATE_SUP_STR)));
    }
}
