import _root_.io.gatling.core.scenario.Simulation
import ch.qos.logback.classic.{Level, LoggerContext}
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import org.slf4j.LoggerFactory

import scala.concurrent.duration._

/**
 * Performance test for the SmoothStreamingMedia entity.
 */
class SmoothStreamingMediaGatlingTest extends Simulation {

    val context: LoggerContext = LoggerFactory.getILoggerFactory.asInstanceOf[LoggerContext]
    // Log all HTTP requests
    //context.getLogger("io.gatling.http").setLevel(Level.valueOf("TRACE"))
    // Log failed HTTP requests
    //context.getLogger("io.gatling.http").setLevel(Level.valueOf("DEBUG"))

    val baseURL = Option(System.getProperty("baseURL")) getOrElse """http://127.0.0.1:8080"""

    val httpConf = http
        .baseURL(baseURL)
        .inferHtmlResources()
        .acceptHeader("*/*")
        .acceptEncodingHeader("gzip, deflate")
        .acceptLanguageHeader("fr,fr-fr;q=0.8,en-us;q=0.5,en;q=0.3")
        .connectionHeader("keep-alive")
        .userAgentHeader("Mozilla/5.0 (Macintosh; Intel Mac OS X 10.10; rv:33.0) Gecko/20100101 Firefox/33.0")

    val headers_http = Map(
        "Accept" -> """application/json"""
    )

    val headers_http_authenticated = Map(
        "Accept" -> """application/json""",
        "X-XSRF-TOKEN" -> "${xsrf_token}"
    )

    val scn = scenario("Test the SmoothStreamingMedia entity")
        .exec(http("First unauthenticated request")
        .get("/api/account")
        .headers(headers_http)
        .check(status.is(401))
        .check(headerRegex("Set-Cookie", "XSRF-TOKEN=(.*);[\\s]").saveAs("xsrf_token"))).exitHereIfFailed
        .pause(10)
        .exec(http("Authentication")
        .post("/api/authentication")
        .headers(headers_http_authenticated)
        .formParam("j_username", "admin")
        .formParam("j_password", "admin")
        .formParam("remember-me", "true")
        .formParam("submit", "Login")
        .check(headerRegex("Set-Cookie", "XSRF-TOKEN=(.*);[\\s]").saveAs("xsrf_token"))).exitHereIfFailed
        .pause(1)
        .exec(http("Authenticated request")
        .get("/api/account")
        .headers(headers_http_authenticated)
        .check(status.is(200)))
        .pause(10)
        .repeat(2) {
            exec(http("Get all smoothStreamingMedias")
            .get("/api/smooth-streaming-medias")
            .headers(headers_http_authenticated)
            .check(status.is(200)))
            .pause(10 seconds, 20 seconds)
            .exec(http("Create new smoothStreamingMedia")
            .post("/api/smooth-streaming-medias")
            .headers(headers_http_authenticated)
            .body(StringBody("""{"id":null, "nameSmooth":"SAMPLE_TEXT", "majorVersion":"SAMPLE_TEXT", "minorVersion":"SAMPLE_TEXT", "duration":"SAMPLE_TEXT", "dvrWindowLength":"SAMPLE_TEXT", "lookAheadFragmentCount":"SAMPLE_TEXT", "isLive":null, "canSeek":null, "canPause":null, "dateManifest":"2020-01-01T00:00:00.000Z", "smoothOk":null, "manifestFile":null, "typeStreamIndex":"SAMPLE_TEXT", "nameVideo":"SAMPLE_TEXT", "nameAudio":"SAMPLE_TEXT", "nbrChunksVideo":"0", "nbrChunksAudio":"0", "chunksVideo":"SAMPLE_TEXT", "chunksAudio":"SAMPLE_TEXT", "qualityLevelsVideo":"SAMPLE_TEXT", "qualityLevelsAudio":"SAMPLE_TEXT", "maxWidthVideo":"SAMPLE_TEXT", "maxHeightVideo":"SAMPLE_TEXT", "maxWidthAudio":"SAMPLE_TEXT", "maxHeightAudio":"SAMPLE_TEXT", "displayWidthVideo":"SAMPLE_TEXT", "displayHeightVideo":"SAMPLE_TEXT", "urlVideo":"SAMPLE_TEXT", "urlAudio":"SAMPLE_TEXT", "indexStrmAudio":"SAMPLE_TEXT", "language":"SAMPLE_TEXT", "indexQlityVideo":"SAMPLE_TEXT", "indexQlityAudio":"SAMPLE_TEXT", "bitrateVideo":"SAMPLE_TEXT", "bitrateAudio":"SAMPLE_TEXT", "fourCcVideo":"SAMPLE_TEXT", "fourCcAudio":"SAMPLE_TEXT", "codecPrivateDataVideo":"SAMPLE_TEXT", "codecPrivateDataAudio":"SAMPLE_TEXT", "samplingRate":"SAMPLE_TEXT", "channels":"SAMPLE_TEXT", "bitsPerSample":"SAMPLE_TEXT", "packetSize":"SAMPLE_TEXT", "audioTag":"SAMPLE_TEXT"}""")).asJSON
            .check(status.is(201))
            .check(headerRegex("Location", "(.*)").saveAs("new_smoothStreamingMedia_url"))).exitHereIfFailed
            .pause(10)
            .repeat(5) {
                exec(http("Get created smoothStreamingMedia")
                .get("${new_smoothStreamingMedia_url}")
                .headers(headers_http_authenticated))
                .pause(10)
            }
            .exec(http("Delete created smoothStreamingMedia")
            .delete("${new_smoothStreamingMedia_url}")
            .headers(headers_http_authenticated))
            .pause(10)
        }

    val users = scenario("Users").exec(scn)

    setUp(
        users.inject(rampUsers(100) over (1 minutes))
    ).protocols(httpConf)
}
