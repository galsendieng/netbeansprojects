'use strict';

describe('Controller Tests', function() {

    describe('Channels Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockPreviousState, MockChannels, MockStreamer, MockPlaying;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockPreviousState = jasmine.createSpy('MockPreviousState');
            MockChannels = jasmine.createSpy('MockChannels');
            MockStreamer = jasmine.createSpy('MockStreamer');
            MockPlaying = jasmine.createSpy('MockPlaying');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity,
                'previousState': MockPreviousState,
                'Channels': MockChannels,
                'Streamer': MockStreamer,
                'Playing': MockPlaying
            };
            createController = function() {
                $injector.get('$controller')("ChannelsMyAbstractDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'abstractApp:channelsUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
