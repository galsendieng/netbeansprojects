package com.orange.myabstract.web.rest;

import com.orange.myabstract.AbstractApp;

import com.orange.myabstract.domain.Streamer;
import com.orange.myabstract.repository.StreamerRepository;
import com.orange.myabstract.service.StreamerService;
import com.orange.myabstract.repository.search.StreamerSearchRepository;
import com.orange.myabstract.service.dto.StreamerDTO;
import com.orange.myabstract.service.mapper.StreamerMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the StreamerResource REST controller.
 *
 * @see StreamerResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = AbstractApp.class)
public class StreamerResourceIntTest {

    private static final String DEFAULT_NAME_STREAMER = "AAAAAAAAAA";
    private static final String UPDATED_NAME_STREAMER = "BBBBBBBBBB";

    private static final String DEFAULT_ADDRESS = "AAAAAAAAAA";
    private static final String UPDATED_ADDRESS = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    @Inject
    private StreamerRepository streamerRepository;

    @Inject
    private StreamerMapper streamerMapper;

    @Inject
    private StreamerService streamerService;

    @Inject
    private StreamerSearchRepository streamerSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Inject
    private EntityManager em;

    private MockMvc restStreamerMockMvc;

    private Streamer streamer;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        StreamerResource streamerResource = new StreamerResource();
        ReflectionTestUtils.setField(streamerResource, "streamerService", streamerService);
        this.restStreamerMockMvc = MockMvcBuilders.standaloneSetup(streamerResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Streamer createEntity(EntityManager em) {
        Streamer streamer = new Streamer()
                .nameStreamer(DEFAULT_NAME_STREAMER)
                .address(DEFAULT_ADDRESS)
                .description(DEFAULT_DESCRIPTION);
        return streamer;
    }

    @Before
    public void initTest() {
        streamerSearchRepository.deleteAll();
        streamer = createEntity(em);
    }

    @Test
    @Transactional
    public void createStreamer() throws Exception {
        int databaseSizeBeforeCreate = streamerRepository.findAll().size();

        // Create the Streamer
        StreamerDTO streamerDTO = streamerMapper.streamerToStreamerDTO(streamer);

        restStreamerMockMvc.perform(post("/api/streamers")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(streamerDTO)))
                .andExpect(status().isCreated());

        // Validate the Streamer in the database
        List<Streamer> streamers = streamerRepository.findAll();
        assertThat(streamers).hasSize(databaseSizeBeforeCreate + 1);
        Streamer testStreamer = streamers.get(streamers.size() - 1);
        assertThat(testStreamer.getNameStreamer()).isEqualTo(DEFAULT_NAME_STREAMER);
        assertThat(testStreamer.getAddress()).isEqualTo(DEFAULT_ADDRESS);
        assertThat(testStreamer.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);

        // Validate the Streamer in ElasticSearch
        Streamer streamerEs = streamerSearchRepository.findOne(testStreamer.getId());
        assertThat(streamerEs).isEqualToComparingFieldByField(testStreamer);
    }

    @Test
    @Transactional
    public void checkNameStreamerIsRequired() throws Exception {
        int databaseSizeBeforeTest = streamerRepository.findAll().size();
        // set the field null
        streamer.setNameStreamer(null);

        // Create the Streamer, which fails.
        StreamerDTO streamerDTO = streamerMapper.streamerToStreamerDTO(streamer);

        restStreamerMockMvc.perform(post("/api/streamers")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(streamerDTO)))
                .andExpect(status().isBadRequest());

        List<Streamer> streamers = streamerRepository.findAll();
        assertThat(streamers).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkAddressIsRequired() throws Exception {
        int databaseSizeBeforeTest = streamerRepository.findAll().size();
        // set the field null
        streamer.setAddress(null);

        // Create the Streamer, which fails.
        StreamerDTO streamerDTO = streamerMapper.streamerToStreamerDTO(streamer);

        restStreamerMockMvc.perform(post("/api/streamers")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(streamerDTO)))
                .andExpect(status().isBadRequest());

        List<Streamer> streamers = streamerRepository.findAll();
        assertThat(streamers).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllStreamers() throws Exception {
        // Initialize the database
        streamerRepository.saveAndFlush(streamer);

        // Get all the streamers
        restStreamerMockMvc.perform(get("/api/streamers?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.[*].id").value(hasItem(streamer.getId().intValue())))
                .andExpect(jsonPath("$.[*].nameStreamer").value(hasItem(DEFAULT_NAME_STREAMER.toString())))
                .andExpect(jsonPath("$.[*].address").value(hasItem(DEFAULT_ADDRESS.toString())))
                .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())));
    }

    @Test
    @Transactional
    public void getStreamer() throws Exception {
        // Initialize the database
        streamerRepository.saveAndFlush(streamer);

        // Get the streamer
        restStreamerMockMvc.perform(get("/api/streamers/{id}", streamer.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(streamer.getId().intValue()))
            .andExpect(jsonPath("$.nameStreamer").value(DEFAULT_NAME_STREAMER.toString()))
            .andExpect(jsonPath("$.address").value(DEFAULT_ADDRESS.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingStreamer() throws Exception {
        // Get the streamer
        restStreamerMockMvc.perform(get("/api/streamers/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateStreamer() throws Exception {
        // Initialize the database
        streamerRepository.saveAndFlush(streamer);
        streamerSearchRepository.save(streamer);
        int databaseSizeBeforeUpdate = streamerRepository.findAll().size();

        // Update the streamer
        Streamer updatedStreamer = streamerRepository.findOne(streamer.getId());
        updatedStreamer
                .nameStreamer(UPDATED_NAME_STREAMER)
                .address(UPDATED_ADDRESS)
                .description(UPDATED_DESCRIPTION);
        StreamerDTO streamerDTO = streamerMapper.streamerToStreamerDTO(updatedStreamer);

        restStreamerMockMvc.perform(put("/api/streamers")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(streamerDTO)))
                .andExpect(status().isOk());

        // Validate the Streamer in the database
        List<Streamer> streamers = streamerRepository.findAll();
        assertThat(streamers).hasSize(databaseSizeBeforeUpdate);
        Streamer testStreamer = streamers.get(streamers.size() - 1);
        assertThat(testStreamer.getNameStreamer()).isEqualTo(UPDATED_NAME_STREAMER);
        assertThat(testStreamer.getAddress()).isEqualTo(UPDATED_ADDRESS);
        assertThat(testStreamer.getDescription()).isEqualTo(UPDATED_DESCRIPTION);

        // Validate the Streamer in ElasticSearch
        Streamer streamerEs = streamerSearchRepository.findOne(testStreamer.getId());
        assertThat(streamerEs).isEqualToComparingFieldByField(testStreamer);
    }

    @Test
    @Transactional
    public void deleteStreamer() throws Exception {
        // Initialize the database
        streamerRepository.saveAndFlush(streamer);
        streamerSearchRepository.save(streamer);
        int databaseSizeBeforeDelete = streamerRepository.findAll().size();

        // Get the streamer
        restStreamerMockMvc.perform(delete("/api/streamers/{id}", streamer.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate ElasticSearch is empty
        boolean streamerExistsInEs = streamerSearchRepository.exists(streamer.getId());
        assertThat(streamerExistsInEs).isFalse();

        // Validate the database is empty
        List<Streamer> streamers = streamerRepository.findAll();
        assertThat(streamers).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchStreamer() throws Exception {
        // Initialize the database
        streamerRepository.saveAndFlush(streamer);
        streamerSearchRepository.save(streamer);

        // Search the streamer
        restStreamerMockMvc.perform(get("/api/_search/streamers?query=id:" + streamer.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(streamer.getId().intValue())))
            .andExpect(jsonPath("$.[*].nameStreamer").value(hasItem(DEFAULT_NAME_STREAMER.toString())))
            .andExpect(jsonPath("$.[*].address").value(hasItem(DEFAULT_ADDRESS.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())));
    }
}
