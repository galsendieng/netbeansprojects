package com.orange.myabstract.web.rest;

import com.orange.myabstract.AbstractApp;

import com.orange.myabstract.domain.SmoothStreamingMedia;
import com.orange.myabstract.repository.SmoothStreamingMediaRepository;
import com.orange.myabstract.service.SmoothStreamingMediaService;
import com.orange.myabstract.repository.search.SmoothStreamingMediaSearchRepository;
import com.orange.myabstract.service.dto.SmoothStreamingMediaDTO;
import com.orange.myabstract.service.mapper.SmoothStreamingMediaMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the SmoothStreamingMediaResource REST controller.
 *
 * @see SmoothStreamingMediaResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = AbstractApp.class)
public class SmoothStreamingMediaResourceIntTest {

    private static final String DEFAULT_NAME_SMOOTH = "AAAAAAAAAA";
    private static final String UPDATED_NAME_SMOOTH = "BBBBBBBBBB";

    private static final String DEFAULT_MAJOR_VERSION = "AAAAAAAAAA";
    private static final String UPDATED_MAJOR_VERSION = "BBBBBBBBBB";

    private static final String DEFAULT_MINOR_VERSION = "AAAAAAAAAA";
    private static final String UPDATED_MINOR_VERSION = "BBBBBBBBBB";

    private static final String DEFAULT_DURATION = "AAAAAAAAAA";
    private static final String UPDATED_DURATION = "BBBBBBBBBB";

    private static final String DEFAULT_DVR_WINDOW_LENGTH = "AAAAAAAAAA";
    private static final String UPDATED_DVR_WINDOW_LENGTH = "BBBBBBBBBB";

    private static final String DEFAULT_LOOK_AHEAD_FRAGMENT_COUNT = "AAAAAAAAAA";
    private static final String UPDATED_LOOK_AHEAD_FRAGMENT_COUNT = "BBBBBBBBBB";

    private static final Boolean DEFAULT_IS_LIVE = false;
    private static final Boolean UPDATED_IS_LIVE = true;

    private static final Boolean DEFAULT_CAN_SEEK = false;
    private static final Boolean UPDATED_CAN_SEEK = true;

    private static final Boolean DEFAULT_CAN_PAUSE = false;
    private static final Boolean UPDATED_CAN_PAUSE = true;

    private static final ZonedDateTime DEFAULT_DATE_MANIFEST = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_DATE_MANIFEST = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final String DEFAULT_DATE_MANIFEST_STR = DateTimeFormatter.ISO_INSTANT.format(DEFAULT_DATE_MANIFEST);

    private static final Boolean DEFAULT_SMOOTH_OK = false;
    private static final Boolean UPDATED_SMOOTH_OK = true;

    private static final byte[] DEFAULT_MANIFEST_FILE = TestUtil.createByteArray(1, "0");
    private static final byte[] UPDATED_MANIFEST_FILE = TestUtil.createByteArray(2, "1");
    private static final String DEFAULT_MANIFEST_FILE_CONTENT_TYPE = "image/jpg";
    private static final String UPDATED_MANIFEST_FILE_CONTENT_TYPE = "image/png";

    private static final String DEFAULT_TYPE_STREAM_INDEX = "AAAAAAAAAA";
    private static final String UPDATED_TYPE_STREAM_INDEX = "BBBBBBBBBB";

    private static final String DEFAULT_NAME_VIDEO = "AAAAAAAAAA";
    private static final String UPDATED_NAME_VIDEO = "BBBBBBBBBB";

    private static final String DEFAULT_NAME_AUDIO = "AAAAAAAAAA";
    private static final String UPDATED_NAME_AUDIO = "BBBBBBBBBB";

    private static final Integer DEFAULT_NBR_CHUNKS_VIDEO = 1;
    private static final Integer UPDATED_NBR_CHUNKS_VIDEO = 2;

    private static final Integer DEFAULT_NBR_CHUNKS_AUDIO = 1;
    private static final Integer UPDATED_NBR_CHUNKS_AUDIO = 2;

    private static final String DEFAULT_CHUNKS_VIDEO = "AAAAAAAAAA";
    private static final String UPDATED_CHUNKS_VIDEO = "BBBBBBBBBB";

    private static final String DEFAULT_CHUNKS_AUDIO = "AAAAAAAAAA";
    private static final String UPDATED_CHUNKS_AUDIO = "BBBBBBBBBB";

    private static final String DEFAULT_QUALITY_LEVELS_VIDEO = "AAAAAAAAAA";
    private static final String UPDATED_QUALITY_LEVELS_VIDEO = "BBBBBBBBBB";

    private static final String DEFAULT_QUALITY_LEVELS_AUDIO = "AAAAAAAAAA";
    private static final String UPDATED_QUALITY_LEVELS_AUDIO = "BBBBBBBBBB";

    private static final String DEFAULT_MAX_WIDTH_VIDEO = "AAAAAAAAAA";
    private static final String UPDATED_MAX_WIDTH_VIDEO = "BBBBBBBBBB";

    private static final String DEFAULT_MAX_HEIGHT_VIDEO = "AAAAAAAAAA";
    private static final String UPDATED_MAX_HEIGHT_VIDEO = "BBBBBBBBBB";

    private static final String DEFAULT_MAX_WIDTH_AUDIO = "AAAAAAAAAA";
    private static final String UPDATED_MAX_WIDTH_AUDIO = "BBBBBBBBBB";

    private static final String DEFAULT_MAX_HEIGHT_AUDIO = "AAAAAAAAAA";
    private static final String UPDATED_MAX_HEIGHT_AUDIO = "BBBBBBBBBB";

    private static final String DEFAULT_DISPLAY_WIDTH_VIDEO = "AAAAAAAAAA";
    private static final String UPDATED_DISPLAY_WIDTH_VIDEO = "BBBBBBBBBB";

    private static final String DEFAULT_DISPLAY_HEIGHT_VIDEO = "AAAAAAAAAA";
    private static final String UPDATED_DISPLAY_HEIGHT_VIDEO = "BBBBBBBBBB";

    private static final String DEFAULT_URL_VIDEO = "AAAAAAAAAA";
    private static final String UPDATED_URL_VIDEO = "BBBBBBBBBB";

    private static final String DEFAULT_URL_AUDIO = "AAAAAAAAAA";
    private static final String UPDATED_URL_AUDIO = "BBBBBBBBBB";

    private static final String DEFAULT_INDEX_STRM_AUDIO = "AAAAAAAAAA";
    private static final String UPDATED_INDEX_STRM_AUDIO = "BBBBBBBBBB";

    private static final String DEFAULT_LANGUAGE = "AAAAAAAAAA";
    private static final String UPDATED_LANGUAGE = "BBBBBBBBBB";

    private static final String DEFAULT_INDEX_QLITY_VIDEO = "AAAAAAAAAA";
    private static final String UPDATED_INDEX_QLITY_VIDEO = "BBBBBBBBBB";

    private static final String DEFAULT_INDEX_QLITY_AUDIO = "AAAAAAAAAA";
    private static final String UPDATED_INDEX_QLITY_AUDIO = "BBBBBBBBBB";

    private static final String DEFAULT_BITRATE_VIDEO = "AAAAAAAAAA";
    private static final String UPDATED_BITRATE_VIDEO = "BBBBBBBBBB";

    private static final String DEFAULT_BITRATE_AUDIO = "AAAAAAAAAA";
    private static final String UPDATED_BITRATE_AUDIO = "BBBBBBBBBB";

    private static final String DEFAULT_FOUR_CC_VIDEO = "AAAAAAAAAA";
    private static final String UPDATED_FOUR_CC_VIDEO = "BBBBBBBBBB";

    private static final String DEFAULT_FOUR_CC_AUDIO = "AAAAAAAAAA";
    private static final String UPDATED_FOUR_CC_AUDIO = "BBBBBBBBBB";

    private static final String DEFAULT_CODEC_PRIVATE_DATA_VIDEO = "AAAAAAAAAA";
    private static final String UPDATED_CODEC_PRIVATE_DATA_VIDEO = "BBBBBBBBBB";

    private static final String DEFAULT_CODEC_PRIVATE_DATA_AUDIO = "AAAAAAAAAA";
    private static final String UPDATED_CODEC_PRIVATE_DATA_AUDIO = "BBBBBBBBBB";

    private static final String DEFAULT_SAMPLING_RATE = "AAAAAAAAAA";
    private static final String UPDATED_SAMPLING_RATE = "BBBBBBBBBB";

    private static final String DEFAULT_CHANNELS = "AAAAAAAAAA";
    private static final String UPDATED_CHANNELS = "BBBBBBBBBB";

    private static final String DEFAULT_BITS_PER_SAMPLE = "AAAAAAAAAA";
    private static final String UPDATED_BITS_PER_SAMPLE = "BBBBBBBBBB";

    private static final String DEFAULT_PACKET_SIZE = "AAAAAAAAAA";
    private static final String UPDATED_PACKET_SIZE = "BBBBBBBBBB";

    private static final String DEFAULT_AUDIO_TAG = "AAAAAAAAAA";
    private static final String UPDATED_AUDIO_TAG = "BBBBBBBBBB";

    @Inject
    private SmoothStreamingMediaRepository smoothStreamingMediaRepository;

    @Inject
    private SmoothStreamingMediaMapper smoothStreamingMediaMapper;

    @Inject
    private SmoothStreamingMediaService smoothStreamingMediaService;

    @Inject
    private SmoothStreamingMediaSearchRepository smoothStreamingMediaSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Inject
    private EntityManager em;

    private MockMvc restSmoothStreamingMediaMockMvc;

    private SmoothStreamingMedia smoothStreamingMedia;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        SmoothStreamingMediaResource smoothStreamingMediaResource = new SmoothStreamingMediaResource();
        ReflectionTestUtils.setField(smoothStreamingMediaResource, "smoothStreamingMediaService", smoothStreamingMediaService);
        this.restSmoothStreamingMediaMockMvc = MockMvcBuilders.standaloneSetup(smoothStreamingMediaResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static SmoothStreamingMedia createEntity(EntityManager em) {
        SmoothStreamingMedia smoothStreamingMedia = new SmoothStreamingMedia()
                .nameSmooth(DEFAULT_NAME_SMOOTH)
                .majorVersion(DEFAULT_MAJOR_VERSION)
                .minorVersion(DEFAULT_MINOR_VERSION)
                .duration(DEFAULT_DURATION)
                .dvrWindowLength(DEFAULT_DVR_WINDOW_LENGTH)
                .lookAheadFragmentCount(DEFAULT_LOOK_AHEAD_FRAGMENT_COUNT)
                .isLive(DEFAULT_IS_LIVE)
                .canSeek(DEFAULT_CAN_SEEK)
                .canPause(DEFAULT_CAN_PAUSE)
                .dateManifest(DEFAULT_DATE_MANIFEST)
                .smoothOk(DEFAULT_SMOOTH_OK)
                .manifestFile(DEFAULT_MANIFEST_FILE)
                .manifestFileContentType(DEFAULT_MANIFEST_FILE_CONTENT_TYPE)
                .typeStreamIndex(DEFAULT_TYPE_STREAM_INDEX)
                .nameVideo(DEFAULT_NAME_VIDEO)
                .nameAudio(DEFAULT_NAME_AUDIO)
                .nbrChunksVideo(DEFAULT_NBR_CHUNKS_VIDEO)
                .nbrChunksAudio(DEFAULT_NBR_CHUNKS_AUDIO)
                .chunksVideo(DEFAULT_CHUNKS_VIDEO)
                .chunksAudio(DEFAULT_CHUNKS_AUDIO)
                .qualityLevelsVideo(DEFAULT_QUALITY_LEVELS_VIDEO)
                .qualityLevelsAudio(DEFAULT_QUALITY_LEVELS_AUDIO)
                .maxWidthVideo(DEFAULT_MAX_WIDTH_VIDEO)
                .maxHeightVideo(DEFAULT_MAX_HEIGHT_VIDEO)
                .maxWidthAudio(DEFAULT_MAX_WIDTH_AUDIO)
                .maxHeightAudio(DEFAULT_MAX_HEIGHT_AUDIO)
                .displayWidthVideo(DEFAULT_DISPLAY_WIDTH_VIDEO)
                .displayHeightVideo(DEFAULT_DISPLAY_HEIGHT_VIDEO)
                .urlVideo(DEFAULT_URL_VIDEO)
                .urlAudio(DEFAULT_URL_AUDIO)
                .indexStrmAudio(DEFAULT_INDEX_STRM_AUDIO)
                .language(DEFAULT_LANGUAGE)
                .indexQlityVideo(DEFAULT_INDEX_QLITY_VIDEO)
                .indexQlityAudio(DEFAULT_INDEX_QLITY_AUDIO)
                .bitrateVideo(DEFAULT_BITRATE_VIDEO)
                .bitrateAudio(DEFAULT_BITRATE_AUDIO)
                .fourCcVideo(DEFAULT_FOUR_CC_VIDEO)
                .fourCcAudio(DEFAULT_FOUR_CC_AUDIO)
                .codecPrivateDataVideo(DEFAULT_CODEC_PRIVATE_DATA_VIDEO)
                .codecPrivateDataAudio(DEFAULT_CODEC_PRIVATE_DATA_AUDIO)
                .samplingRate(DEFAULT_SAMPLING_RATE)
                .channels(DEFAULT_CHANNELS)
                .bitsPerSample(DEFAULT_BITS_PER_SAMPLE)
                .packetSize(DEFAULT_PACKET_SIZE)
                .audioTag(DEFAULT_AUDIO_TAG);
        return smoothStreamingMedia;
    }

    @Before
    public void initTest() {
        smoothStreamingMediaSearchRepository.deleteAll();
        smoothStreamingMedia = createEntity(em);
    }

    @Test
    @Transactional
    public void createSmoothStreamingMedia() throws Exception {
        int databaseSizeBeforeCreate = smoothStreamingMediaRepository.findAll().size();

        // Create the SmoothStreamingMedia
        SmoothStreamingMediaDTO smoothStreamingMediaDTO = smoothStreamingMediaMapper.smoothStreamingMediaToSmoothStreamingMediaDTO(smoothStreamingMedia);

        restSmoothStreamingMediaMockMvc.perform(post("/api/smooth-streaming-medias")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(smoothStreamingMediaDTO)))
                .andExpect(status().isCreated());

        // Validate the SmoothStreamingMedia in the database
        List<SmoothStreamingMedia> smoothStreamingMedias = smoothStreamingMediaRepository.findAll();
        assertThat(smoothStreamingMedias).hasSize(databaseSizeBeforeCreate + 1);
        SmoothStreamingMedia testSmoothStreamingMedia = smoothStreamingMedias.get(smoothStreamingMedias.size() - 1);
        assertThat(testSmoothStreamingMedia.getNameSmooth()).isEqualTo(DEFAULT_NAME_SMOOTH);
        assertThat(testSmoothStreamingMedia.getMajorVersion()).isEqualTo(DEFAULT_MAJOR_VERSION);
        assertThat(testSmoothStreamingMedia.getMinorVersion()).isEqualTo(DEFAULT_MINOR_VERSION);
        assertThat(testSmoothStreamingMedia.getDuration()).isEqualTo(DEFAULT_DURATION);
        assertThat(testSmoothStreamingMedia.getDvrWindowLength()).isEqualTo(DEFAULT_DVR_WINDOW_LENGTH);
        assertThat(testSmoothStreamingMedia.getLookAheadFragmentCount()).isEqualTo(DEFAULT_LOOK_AHEAD_FRAGMENT_COUNT);
        assertThat(testSmoothStreamingMedia.isIsLive()).isEqualTo(DEFAULT_IS_LIVE);
        assertThat(testSmoothStreamingMedia.isCanSeek()).isEqualTo(DEFAULT_CAN_SEEK);
        assertThat(testSmoothStreamingMedia.isCanPause()).isEqualTo(DEFAULT_CAN_PAUSE);
        assertThat(testSmoothStreamingMedia.getDateManifest()).isEqualTo(DEFAULT_DATE_MANIFEST);
        assertThat(testSmoothStreamingMedia.isSmoothOk()).isEqualTo(DEFAULT_SMOOTH_OK);
        assertThat(testSmoothStreamingMedia.getManifestFile()).isEqualTo(DEFAULT_MANIFEST_FILE);
        assertThat(testSmoothStreamingMedia.getManifestFileContentType()).isEqualTo(DEFAULT_MANIFEST_FILE_CONTENT_TYPE);
        assertThat(testSmoothStreamingMedia.getTypeStreamIndex()).isEqualTo(DEFAULT_TYPE_STREAM_INDEX);
        assertThat(testSmoothStreamingMedia.getNameVideo()).isEqualTo(DEFAULT_NAME_VIDEO);
        assertThat(testSmoothStreamingMedia.getNameAudio()).isEqualTo(DEFAULT_NAME_AUDIO);
        assertThat(testSmoothStreamingMedia.getNbrChunksVideo()).isEqualTo(DEFAULT_NBR_CHUNKS_VIDEO);
        assertThat(testSmoothStreamingMedia.getNbrChunksAudio()).isEqualTo(DEFAULT_NBR_CHUNKS_AUDIO);
        assertThat(testSmoothStreamingMedia.getChunksVideo()).isEqualTo(DEFAULT_CHUNKS_VIDEO);
        assertThat(testSmoothStreamingMedia.getChunksAudio()).isEqualTo(DEFAULT_CHUNKS_AUDIO);
        assertThat(testSmoothStreamingMedia.getQualityLevelsVideo()).isEqualTo(DEFAULT_QUALITY_LEVELS_VIDEO);
        assertThat(testSmoothStreamingMedia.getQualityLevelsAudio()).isEqualTo(DEFAULT_QUALITY_LEVELS_AUDIO);
        assertThat(testSmoothStreamingMedia.getMaxWidthVideo()).isEqualTo(DEFAULT_MAX_WIDTH_VIDEO);
        assertThat(testSmoothStreamingMedia.getMaxHeightVideo()).isEqualTo(DEFAULT_MAX_HEIGHT_VIDEO);
        assertThat(testSmoothStreamingMedia.getMaxWidthAudio()).isEqualTo(DEFAULT_MAX_WIDTH_AUDIO);
        assertThat(testSmoothStreamingMedia.getMaxHeightAudio()).isEqualTo(DEFAULT_MAX_HEIGHT_AUDIO);
        assertThat(testSmoothStreamingMedia.getDisplayWidthVideo()).isEqualTo(DEFAULT_DISPLAY_WIDTH_VIDEO);
        assertThat(testSmoothStreamingMedia.getDisplayHeightVideo()).isEqualTo(DEFAULT_DISPLAY_HEIGHT_VIDEO);
        assertThat(testSmoothStreamingMedia.getUrlVideo()).isEqualTo(DEFAULT_URL_VIDEO);
        assertThat(testSmoothStreamingMedia.getUrlAudio()).isEqualTo(DEFAULT_URL_AUDIO);
        assertThat(testSmoothStreamingMedia.getIndexStrmAudio()).isEqualTo(DEFAULT_INDEX_STRM_AUDIO);
        assertThat(testSmoothStreamingMedia.getLanguage()).isEqualTo(DEFAULT_LANGUAGE);
        assertThat(testSmoothStreamingMedia.getIndexQlityVideo()).isEqualTo(DEFAULT_INDEX_QLITY_VIDEO);
        assertThat(testSmoothStreamingMedia.getIndexQlityAudio()).isEqualTo(DEFAULT_INDEX_QLITY_AUDIO);
        assertThat(testSmoothStreamingMedia.getBitrateVideo()).isEqualTo(DEFAULT_BITRATE_VIDEO);
        assertThat(testSmoothStreamingMedia.getBitrateAudio()).isEqualTo(DEFAULT_BITRATE_AUDIO);
        assertThat(testSmoothStreamingMedia.getFourCcVideo()).isEqualTo(DEFAULT_FOUR_CC_VIDEO);
        assertThat(testSmoothStreamingMedia.getFourCcAudio()).isEqualTo(DEFAULT_FOUR_CC_AUDIO);
        assertThat(testSmoothStreamingMedia.getCodecPrivateDataVideo()).isEqualTo(DEFAULT_CODEC_PRIVATE_DATA_VIDEO);
        assertThat(testSmoothStreamingMedia.getCodecPrivateDataAudio()).isEqualTo(DEFAULT_CODEC_PRIVATE_DATA_AUDIO);
        assertThat(testSmoothStreamingMedia.getSamplingRate()).isEqualTo(DEFAULT_SAMPLING_RATE);
        assertThat(testSmoothStreamingMedia.getChannels()).isEqualTo(DEFAULT_CHANNELS);
        assertThat(testSmoothStreamingMedia.getBitsPerSample()).isEqualTo(DEFAULT_BITS_PER_SAMPLE);
        assertThat(testSmoothStreamingMedia.getPacketSize()).isEqualTo(DEFAULT_PACKET_SIZE);
        assertThat(testSmoothStreamingMedia.getAudioTag()).isEqualTo(DEFAULT_AUDIO_TAG);

        // Validate the SmoothStreamingMedia in ElasticSearch
        SmoothStreamingMedia smoothStreamingMediaEs = smoothStreamingMediaSearchRepository.findOne(testSmoothStreamingMedia.getId());
        assertThat(smoothStreamingMediaEs).isEqualToComparingFieldByField(testSmoothStreamingMedia);
    }

    @Test
    @Transactional
    public void checkNameSmoothIsRequired() throws Exception {
        int databaseSizeBeforeTest = smoothStreamingMediaRepository.findAll().size();
        // set the field null
        smoothStreamingMedia.setNameSmooth(null);

        // Create the SmoothStreamingMedia, which fails.
        SmoothStreamingMediaDTO smoothStreamingMediaDTO = smoothStreamingMediaMapper.smoothStreamingMediaToSmoothStreamingMediaDTO(smoothStreamingMedia);

        restSmoothStreamingMediaMockMvc.perform(post("/api/smooth-streaming-medias")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(smoothStreamingMediaDTO)))
                .andExpect(status().isBadRequest());

        List<SmoothStreamingMedia> smoothStreamingMedias = smoothStreamingMediaRepository.findAll();
        assertThat(smoothStreamingMedias).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllSmoothStreamingMedias() throws Exception {
        // Initialize the database
        smoothStreamingMediaRepository.saveAndFlush(smoothStreamingMedia);

        // Get all the smoothStreamingMedias
        restSmoothStreamingMediaMockMvc.perform(get("/api/smooth-streaming-medias?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.[*].id").value(hasItem(smoothStreamingMedia.getId().intValue())))
                .andExpect(jsonPath("$.[*].nameSmooth").value(hasItem(DEFAULT_NAME_SMOOTH.toString())))
                .andExpect(jsonPath("$.[*].majorVersion").value(hasItem(DEFAULT_MAJOR_VERSION.toString())))
                .andExpect(jsonPath("$.[*].minorVersion").value(hasItem(DEFAULT_MINOR_VERSION.toString())))
                .andExpect(jsonPath("$.[*].duration").value(hasItem(DEFAULT_DURATION.toString())))
                .andExpect(jsonPath("$.[*].dvrWindowLength").value(hasItem(DEFAULT_DVR_WINDOW_LENGTH.toString())))
                .andExpect(jsonPath("$.[*].lookAheadFragmentCount").value(hasItem(DEFAULT_LOOK_AHEAD_FRAGMENT_COUNT.toString())))
                .andExpect(jsonPath("$.[*].isLive").value(hasItem(DEFAULT_IS_LIVE.booleanValue())))
                .andExpect(jsonPath("$.[*].canSeek").value(hasItem(DEFAULT_CAN_SEEK.booleanValue())))
                .andExpect(jsonPath("$.[*].canPause").value(hasItem(DEFAULT_CAN_PAUSE.booleanValue())))
                .andExpect(jsonPath("$.[*].dateManifest").value(hasItem(DEFAULT_DATE_MANIFEST_STR)))
                .andExpect(jsonPath("$.[*].smoothOk").value(hasItem(DEFAULT_SMOOTH_OK.booleanValue())))
                .andExpect(jsonPath("$.[*].manifestFileContentType").value(hasItem(DEFAULT_MANIFEST_FILE_CONTENT_TYPE)))
                .andExpect(jsonPath("$.[*].manifestFile").value(hasItem(Base64Utils.encodeToString(DEFAULT_MANIFEST_FILE))))
                .andExpect(jsonPath("$.[*].typeStreamIndex").value(hasItem(DEFAULT_TYPE_STREAM_INDEX.toString())))
                .andExpect(jsonPath("$.[*].nameVideo").value(hasItem(DEFAULT_NAME_VIDEO.toString())))
                .andExpect(jsonPath("$.[*].nameAudio").value(hasItem(DEFAULT_NAME_AUDIO.toString())))
                .andExpect(jsonPath("$.[*].nbrChunksVideo").value(hasItem(DEFAULT_NBR_CHUNKS_VIDEO)))
                .andExpect(jsonPath("$.[*].nbrChunksAudio").value(hasItem(DEFAULT_NBR_CHUNKS_AUDIO)))
                .andExpect(jsonPath("$.[*].chunksVideo").value(hasItem(DEFAULT_CHUNKS_VIDEO.toString())))
                .andExpect(jsonPath("$.[*].chunksAudio").value(hasItem(DEFAULT_CHUNKS_AUDIO.toString())))
                .andExpect(jsonPath("$.[*].qualityLevelsVideo").value(hasItem(DEFAULT_QUALITY_LEVELS_VIDEO.toString())))
                .andExpect(jsonPath("$.[*].qualityLevelsAudio").value(hasItem(DEFAULT_QUALITY_LEVELS_AUDIO.toString())))
                .andExpect(jsonPath("$.[*].maxWidthVideo").value(hasItem(DEFAULT_MAX_WIDTH_VIDEO.toString())))
                .andExpect(jsonPath("$.[*].maxHeightVideo").value(hasItem(DEFAULT_MAX_HEIGHT_VIDEO.toString())))
                .andExpect(jsonPath("$.[*].maxWidthAudio").value(hasItem(DEFAULT_MAX_WIDTH_AUDIO.toString())))
                .andExpect(jsonPath("$.[*].maxHeightAudio").value(hasItem(DEFAULT_MAX_HEIGHT_AUDIO.toString())))
                .andExpect(jsonPath("$.[*].displayWidthVideo").value(hasItem(DEFAULT_DISPLAY_WIDTH_VIDEO.toString())))
                .andExpect(jsonPath("$.[*].displayHeightVideo").value(hasItem(DEFAULT_DISPLAY_HEIGHT_VIDEO.toString())))
                .andExpect(jsonPath("$.[*].urlVideo").value(hasItem(DEFAULT_URL_VIDEO.toString())))
                .andExpect(jsonPath("$.[*].urlAudio").value(hasItem(DEFAULT_URL_AUDIO.toString())))
                .andExpect(jsonPath("$.[*].indexStrmAudio").value(hasItem(DEFAULT_INDEX_STRM_AUDIO.toString())))
                .andExpect(jsonPath("$.[*].language").value(hasItem(DEFAULT_LANGUAGE.toString())))
                .andExpect(jsonPath("$.[*].indexQlityVideo").value(hasItem(DEFAULT_INDEX_QLITY_VIDEO.toString())))
                .andExpect(jsonPath("$.[*].indexQlityAudio").value(hasItem(DEFAULT_INDEX_QLITY_AUDIO.toString())))
                .andExpect(jsonPath("$.[*].bitrateVideo").value(hasItem(DEFAULT_BITRATE_VIDEO.toString())))
                .andExpect(jsonPath("$.[*].bitrateAudio").value(hasItem(DEFAULT_BITRATE_AUDIO.toString())))
                .andExpect(jsonPath("$.[*].fourCcVideo").value(hasItem(DEFAULT_FOUR_CC_VIDEO.toString())))
                .andExpect(jsonPath("$.[*].fourCcAudio").value(hasItem(DEFAULT_FOUR_CC_AUDIO.toString())))
                .andExpect(jsonPath("$.[*].codecPrivateDataVideo").value(hasItem(DEFAULT_CODEC_PRIVATE_DATA_VIDEO.toString())))
                .andExpect(jsonPath("$.[*].codecPrivateDataAudio").value(hasItem(DEFAULT_CODEC_PRIVATE_DATA_AUDIO.toString())))
                .andExpect(jsonPath("$.[*].samplingRate").value(hasItem(DEFAULT_SAMPLING_RATE.toString())))
                .andExpect(jsonPath("$.[*].channels").value(hasItem(DEFAULT_CHANNELS.toString())))
                .andExpect(jsonPath("$.[*].bitsPerSample").value(hasItem(DEFAULT_BITS_PER_SAMPLE.toString())))
                .andExpect(jsonPath("$.[*].packetSize").value(hasItem(DEFAULT_PACKET_SIZE.toString())))
                .andExpect(jsonPath("$.[*].audioTag").value(hasItem(DEFAULT_AUDIO_TAG.toString())));
    }

    @Test
    @Transactional
    public void getSmoothStreamingMedia() throws Exception {
        // Initialize the database
        smoothStreamingMediaRepository.saveAndFlush(smoothStreamingMedia);

        // Get the smoothStreamingMedia
        restSmoothStreamingMediaMockMvc.perform(get("/api/smooth-streaming-medias/{id}", smoothStreamingMedia.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(smoothStreamingMedia.getId().intValue()))
            .andExpect(jsonPath("$.nameSmooth").value(DEFAULT_NAME_SMOOTH.toString()))
            .andExpect(jsonPath("$.majorVersion").value(DEFAULT_MAJOR_VERSION.toString()))
            .andExpect(jsonPath("$.minorVersion").value(DEFAULT_MINOR_VERSION.toString()))
            .andExpect(jsonPath("$.duration").value(DEFAULT_DURATION.toString()))
            .andExpect(jsonPath("$.dvrWindowLength").value(DEFAULT_DVR_WINDOW_LENGTH.toString()))
            .andExpect(jsonPath("$.lookAheadFragmentCount").value(DEFAULT_LOOK_AHEAD_FRAGMENT_COUNT.toString()))
            .andExpect(jsonPath("$.isLive").value(DEFAULT_IS_LIVE.booleanValue()))
            .andExpect(jsonPath("$.canSeek").value(DEFAULT_CAN_SEEK.booleanValue()))
            .andExpect(jsonPath("$.canPause").value(DEFAULT_CAN_PAUSE.booleanValue()))
            .andExpect(jsonPath("$.dateManifest").value(DEFAULT_DATE_MANIFEST_STR))
            .andExpect(jsonPath("$.smoothOk").value(DEFAULT_SMOOTH_OK.booleanValue()))
            .andExpect(jsonPath("$.manifestFileContentType").value(DEFAULT_MANIFEST_FILE_CONTENT_TYPE))
            .andExpect(jsonPath("$.manifestFile").value(Base64Utils.encodeToString(DEFAULT_MANIFEST_FILE)))
            .andExpect(jsonPath("$.typeStreamIndex").value(DEFAULT_TYPE_STREAM_INDEX.toString()))
            .andExpect(jsonPath("$.nameVideo").value(DEFAULT_NAME_VIDEO.toString()))
            .andExpect(jsonPath("$.nameAudio").value(DEFAULT_NAME_AUDIO.toString()))
            .andExpect(jsonPath("$.nbrChunksVideo").value(DEFAULT_NBR_CHUNKS_VIDEO))
            .andExpect(jsonPath("$.nbrChunksAudio").value(DEFAULT_NBR_CHUNKS_AUDIO))
            .andExpect(jsonPath("$.chunksVideo").value(DEFAULT_CHUNKS_VIDEO.toString()))
            .andExpect(jsonPath("$.chunksAudio").value(DEFAULT_CHUNKS_AUDIO.toString()))
            .andExpect(jsonPath("$.qualityLevelsVideo").value(DEFAULT_QUALITY_LEVELS_VIDEO.toString()))
            .andExpect(jsonPath("$.qualityLevelsAudio").value(DEFAULT_QUALITY_LEVELS_AUDIO.toString()))
            .andExpect(jsonPath("$.maxWidthVideo").value(DEFAULT_MAX_WIDTH_VIDEO.toString()))
            .andExpect(jsonPath("$.maxHeightVideo").value(DEFAULT_MAX_HEIGHT_VIDEO.toString()))
            .andExpect(jsonPath("$.maxWidthAudio").value(DEFAULT_MAX_WIDTH_AUDIO.toString()))
            .andExpect(jsonPath("$.maxHeightAudio").value(DEFAULT_MAX_HEIGHT_AUDIO.toString()))
            .andExpect(jsonPath("$.displayWidthVideo").value(DEFAULT_DISPLAY_WIDTH_VIDEO.toString()))
            .andExpect(jsonPath("$.displayHeightVideo").value(DEFAULT_DISPLAY_HEIGHT_VIDEO.toString()))
            .andExpect(jsonPath("$.urlVideo").value(DEFAULT_URL_VIDEO.toString()))
            .andExpect(jsonPath("$.urlAudio").value(DEFAULT_URL_AUDIO.toString()))
            .andExpect(jsonPath("$.indexStrmAudio").value(DEFAULT_INDEX_STRM_AUDIO.toString()))
            .andExpect(jsonPath("$.language").value(DEFAULT_LANGUAGE.toString()))
            .andExpect(jsonPath("$.indexQlityVideo").value(DEFAULT_INDEX_QLITY_VIDEO.toString()))
            .andExpect(jsonPath("$.indexQlityAudio").value(DEFAULT_INDEX_QLITY_AUDIO.toString()))
            .andExpect(jsonPath("$.bitrateVideo").value(DEFAULT_BITRATE_VIDEO.toString()))
            .andExpect(jsonPath("$.bitrateAudio").value(DEFAULT_BITRATE_AUDIO.toString()))
            .andExpect(jsonPath("$.fourCcVideo").value(DEFAULT_FOUR_CC_VIDEO.toString()))
            .andExpect(jsonPath("$.fourCcAudio").value(DEFAULT_FOUR_CC_AUDIO.toString()))
            .andExpect(jsonPath("$.codecPrivateDataVideo").value(DEFAULT_CODEC_PRIVATE_DATA_VIDEO.toString()))
            .andExpect(jsonPath("$.codecPrivateDataAudio").value(DEFAULT_CODEC_PRIVATE_DATA_AUDIO.toString()))
            .andExpect(jsonPath("$.samplingRate").value(DEFAULT_SAMPLING_RATE.toString()))
            .andExpect(jsonPath("$.channels").value(DEFAULT_CHANNELS.toString()))
            .andExpect(jsonPath("$.bitsPerSample").value(DEFAULT_BITS_PER_SAMPLE.toString()))
            .andExpect(jsonPath("$.packetSize").value(DEFAULT_PACKET_SIZE.toString()))
            .andExpect(jsonPath("$.audioTag").value(DEFAULT_AUDIO_TAG.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingSmoothStreamingMedia() throws Exception {
        // Get the smoothStreamingMedia
        restSmoothStreamingMediaMockMvc.perform(get("/api/smooth-streaming-medias/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSmoothStreamingMedia() throws Exception {
        // Initialize the database
        smoothStreamingMediaRepository.saveAndFlush(smoothStreamingMedia);
        smoothStreamingMediaSearchRepository.save(smoothStreamingMedia);
        int databaseSizeBeforeUpdate = smoothStreamingMediaRepository.findAll().size();

        // Update the smoothStreamingMedia
        SmoothStreamingMedia updatedSmoothStreamingMedia = smoothStreamingMediaRepository.findOne(smoothStreamingMedia.getId());
        updatedSmoothStreamingMedia
                .nameSmooth(UPDATED_NAME_SMOOTH)
                .majorVersion(UPDATED_MAJOR_VERSION)
                .minorVersion(UPDATED_MINOR_VERSION)
                .duration(UPDATED_DURATION)
                .dvrWindowLength(UPDATED_DVR_WINDOW_LENGTH)
                .lookAheadFragmentCount(UPDATED_LOOK_AHEAD_FRAGMENT_COUNT)
                .isLive(UPDATED_IS_LIVE)
                .canSeek(UPDATED_CAN_SEEK)
                .canPause(UPDATED_CAN_PAUSE)
                .dateManifest(UPDATED_DATE_MANIFEST)
                .smoothOk(UPDATED_SMOOTH_OK)
                .manifestFile(UPDATED_MANIFEST_FILE)
                .manifestFileContentType(UPDATED_MANIFEST_FILE_CONTENT_TYPE)
                .typeStreamIndex(UPDATED_TYPE_STREAM_INDEX)
                .nameVideo(UPDATED_NAME_VIDEO)
                .nameAudio(UPDATED_NAME_AUDIO)
                .nbrChunksVideo(UPDATED_NBR_CHUNKS_VIDEO)
                .nbrChunksAudio(UPDATED_NBR_CHUNKS_AUDIO)
                .chunksVideo(UPDATED_CHUNKS_VIDEO)
                .chunksAudio(UPDATED_CHUNKS_AUDIO)
                .qualityLevelsVideo(UPDATED_QUALITY_LEVELS_VIDEO)
                .qualityLevelsAudio(UPDATED_QUALITY_LEVELS_AUDIO)
                .maxWidthVideo(UPDATED_MAX_WIDTH_VIDEO)
                .maxHeightVideo(UPDATED_MAX_HEIGHT_VIDEO)
                .maxWidthAudio(UPDATED_MAX_WIDTH_AUDIO)
                .maxHeightAudio(UPDATED_MAX_HEIGHT_AUDIO)
                .displayWidthVideo(UPDATED_DISPLAY_WIDTH_VIDEO)
                .displayHeightVideo(UPDATED_DISPLAY_HEIGHT_VIDEO)
                .urlVideo(UPDATED_URL_VIDEO)
                .urlAudio(UPDATED_URL_AUDIO)
                .indexStrmAudio(UPDATED_INDEX_STRM_AUDIO)
                .language(UPDATED_LANGUAGE)
                .indexQlityVideo(UPDATED_INDEX_QLITY_VIDEO)
                .indexQlityAudio(UPDATED_INDEX_QLITY_AUDIO)
                .bitrateVideo(UPDATED_BITRATE_VIDEO)
                .bitrateAudio(UPDATED_BITRATE_AUDIO)
                .fourCcVideo(UPDATED_FOUR_CC_VIDEO)
                .fourCcAudio(UPDATED_FOUR_CC_AUDIO)
                .codecPrivateDataVideo(UPDATED_CODEC_PRIVATE_DATA_VIDEO)
                .codecPrivateDataAudio(UPDATED_CODEC_PRIVATE_DATA_AUDIO)
                .samplingRate(UPDATED_SAMPLING_RATE)
                .channels(UPDATED_CHANNELS)
                .bitsPerSample(UPDATED_BITS_PER_SAMPLE)
                .packetSize(UPDATED_PACKET_SIZE)
                .audioTag(UPDATED_AUDIO_TAG);
        SmoothStreamingMediaDTO smoothStreamingMediaDTO = smoothStreamingMediaMapper.smoothStreamingMediaToSmoothStreamingMediaDTO(updatedSmoothStreamingMedia);

        restSmoothStreamingMediaMockMvc.perform(put("/api/smooth-streaming-medias")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(smoothStreamingMediaDTO)))
                .andExpect(status().isOk());

        // Validate the SmoothStreamingMedia in the database
        List<SmoothStreamingMedia> smoothStreamingMedias = smoothStreamingMediaRepository.findAll();
        assertThat(smoothStreamingMedias).hasSize(databaseSizeBeforeUpdate);
        SmoothStreamingMedia testSmoothStreamingMedia = smoothStreamingMedias.get(smoothStreamingMedias.size() - 1);
        assertThat(testSmoothStreamingMedia.getNameSmooth()).isEqualTo(UPDATED_NAME_SMOOTH);
        assertThat(testSmoothStreamingMedia.getMajorVersion()).isEqualTo(UPDATED_MAJOR_VERSION);
        assertThat(testSmoothStreamingMedia.getMinorVersion()).isEqualTo(UPDATED_MINOR_VERSION);
        assertThat(testSmoothStreamingMedia.getDuration()).isEqualTo(UPDATED_DURATION);
        assertThat(testSmoothStreamingMedia.getDvrWindowLength()).isEqualTo(UPDATED_DVR_WINDOW_LENGTH);
        assertThat(testSmoothStreamingMedia.getLookAheadFragmentCount()).isEqualTo(UPDATED_LOOK_AHEAD_FRAGMENT_COUNT);
        assertThat(testSmoothStreamingMedia.isIsLive()).isEqualTo(UPDATED_IS_LIVE);
        assertThat(testSmoothStreamingMedia.isCanSeek()).isEqualTo(UPDATED_CAN_SEEK);
        assertThat(testSmoothStreamingMedia.isCanPause()).isEqualTo(UPDATED_CAN_PAUSE);
        assertThat(testSmoothStreamingMedia.getDateManifest()).isEqualTo(UPDATED_DATE_MANIFEST);
        assertThat(testSmoothStreamingMedia.isSmoothOk()).isEqualTo(UPDATED_SMOOTH_OK);
        assertThat(testSmoothStreamingMedia.getManifestFile()).isEqualTo(UPDATED_MANIFEST_FILE);
        assertThat(testSmoothStreamingMedia.getManifestFileContentType()).isEqualTo(UPDATED_MANIFEST_FILE_CONTENT_TYPE);
        assertThat(testSmoothStreamingMedia.getTypeStreamIndex()).isEqualTo(UPDATED_TYPE_STREAM_INDEX);
        assertThat(testSmoothStreamingMedia.getNameVideo()).isEqualTo(UPDATED_NAME_VIDEO);
        assertThat(testSmoothStreamingMedia.getNameAudio()).isEqualTo(UPDATED_NAME_AUDIO);
        assertThat(testSmoothStreamingMedia.getNbrChunksVideo()).isEqualTo(UPDATED_NBR_CHUNKS_VIDEO);
        assertThat(testSmoothStreamingMedia.getNbrChunksAudio()).isEqualTo(UPDATED_NBR_CHUNKS_AUDIO);
        assertThat(testSmoothStreamingMedia.getChunksVideo()).isEqualTo(UPDATED_CHUNKS_VIDEO);
        assertThat(testSmoothStreamingMedia.getChunksAudio()).isEqualTo(UPDATED_CHUNKS_AUDIO);
        assertThat(testSmoothStreamingMedia.getQualityLevelsVideo()).isEqualTo(UPDATED_QUALITY_LEVELS_VIDEO);
        assertThat(testSmoothStreamingMedia.getQualityLevelsAudio()).isEqualTo(UPDATED_QUALITY_LEVELS_AUDIO);
        assertThat(testSmoothStreamingMedia.getMaxWidthVideo()).isEqualTo(UPDATED_MAX_WIDTH_VIDEO);
        assertThat(testSmoothStreamingMedia.getMaxHeightVideo()).isEqualTo(UPDATED_MAX_HEIGHT_VIDEO);
        assertThat(testSmoothStreamingMedia.getMaxWidthAudio()).isEqualTo(UPDATED_MAX_WIDTH_AUDIO);
        assertThat(testSmoothStreamingMedia.getMaxHeightAudio()).isEqualTo(UPDATED_MAX_HEIGHT_AUDIO);
        assertThat(testSmoothStreamingMedia.getDisplayWidthVideo()).isEqualTo(UPDATED_DISPLAY_WIDTH_VIDEO);
        assertThat(testSmoothStreamingMedia.getDisplayHeightVideo()).isEqualTo(UPDATED_DISPLAY_HEIGHT_VIDEO);
        assertThat(testSmoothStreamingMedia.getUrlVideo()).isEqualTo(UPDATED_URL_VIDEO);
        assertThat(testSmoothStreamingMedia.getUrlAudio()).isEqualTo(UPDATED_URL_AUDIO);
        assertThat(testSmoothStreamingMedia.getIndexStrmAudio()).isEqualTo(UPDATED_INDEX_STRM_AUDIO);
        assertThat(testSmoothStreamingMedia.getLanguage()).isEqualTo(UPDATED_LANGUAGE);
        assertThat(testSmoothStreamingMedia.getIndexQlityVideo()).isEqualTo(UPDATED_INDEX_QLITY_VIDEO);
        assertThat(testSmoothStreamingMedia.getIndexQlityAudio()).isEqualTo(UPDATED_INDEX_QLITY_AUDIO);
        assertThat(testSmoothStreamingMedia.getBitrateVideo()).isEqualTo(UPDATED_BITRATE_VIDEO);
        assertThat(testSmoothStreamingMedia.getBitrateAudio()).isEqualTo(UPDATED_BITRATE_AUDIO);
        assertThat(testSmoothStreamingMedia.getFourCcVideo()).isEqualTo(UPDATED_FOUR_CC_VIDEO);
        assertThat(testSmoothStreamingMedia.getFourCcAudio()).isEqualTo(UPDATED_FOUR_CC_AUDIO);
        assertThat(testSmoothStreamingMedia.getCodecPrivateDataVideo()).isEqualTo(UPDATED_CODEC_PRIVATE_DATA_VIDEO);
        assertThat(testSmoothStreamingMedia.getCodecPrivateDataAudio()).isEqualTo(UPDATED_CODEC_PRIVATE_DATA_AUDIO);
        assertThat(testSmoothStreamingMedia.getSamplingRate()).isEqualTo(UPDATED_SAMPLING_RATE);
        assertThat(testSmoothStreamingMedia.getChannels()).isEqualTo(UPDATED_CHANNELS);
        assertThat(testSmoothStreamingMedia.getBitsPerSample()).isEqualTo(UPDATED_BITS_PER_SAMPLE);
        assertThat(testSmoothStreamingMedia.getPacketSize()).isEqualTo(UPDATED_PACKET_SIZE);
        assertThat(testSmoothStreamingMedia.getAudioTag()).isEqualTo(UPDATED_AUDIO_TAG);

        // Validate the SmoothStreamingMedia in ElasticSearch
        SmoothStreamingMedia smoothStreamingMediaEs = smoothStreamingMediaSearchRepository.findOne(testSmoothStreamingMedia.getId());
        assertThat(smoothStreamingMediaEs).isEqualToComparingFieldByField(testSmoothStreamingMedia);
    }

    @Test
    @Transactional
    public void deleteSmoothStreamingMedia() throws Exception {
        // Initialize the database
        smoothStreamingMediaRepository.saveAndFlush(smoothStreamingMedia);
        smoothStreamingMediaSearchRepository.save(smoothStreamingMedia);
        int databaseSizeBeforeDelete = smoothStreamingMediaRepository.findAll().size();

        // Get the smoothStreamingMedia
        restSmoothStreamingMediaMockMvc.perform(delete("/api/smooth-streaming-medias/{id}", smoothStreamingMedia.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate ElasticSearch is empty
        boolean smoothStreamingMediaExistsInEs = smoothStreamingMediaSearchRepository.exists(smoothStreamingMedia.getId());
        assertThat(smoothStreamingMediaExistsInEs).isFalse();

        // Validate the database is empty
        List<SmoothStreamingMedia> smoothStreamingMedias = smoothStreamingMediaRepository.findAll();
        assertThat(smoothStreamingMedias).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchSmoothStreamingMedia() throws Exception {
        // Initialize the database
        smoothStreamingMediaRepository.saveAndFlush(smoothStreamingMedia);
        smoothStreamingMediaSearchRepository.save(smoothStreamingMedia);

        // Search the smoothStreamingMedia
        restSmoothStreamingMediaMockMvc.perform(get("/api/_search/smooth-streaming-medias?query=id:" + smoothStreamingMedia.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(smoothStreamingMedia.getId().intValue())))
            .andExpect(jsonPath("$.[*].nameSmooth").value(hasItem(DEFAULT_NAME_SMOOTH.toString())))
            .andExpect(jsonPath("$.[*].majorVersion").value(hasItem(DEFAULT_MAJOR_VERSION.toString())))
            .andExpect(jsonPath("$.[*].minorVersion").value(hasItem(DEFAULT_MINOR_VERSION.toString())))
            .andExpect(jsonPath("$.[*].duration").value(hasItem(DEFAULT_DURATION.toString())))
            .andExpect(jsonPath("$.[*].dvrWindowLength").value(hasItem(DEFAULT_DVR_WINDOW_LENGTH.toString())))
            .andExpect(jsonPath("$.[*].lookAheadFragmentCount").value(hasItem(DEFAULT_LOOK_AHEAD_FRAGMENT_COUNT.toString())))
            .andExpect(jsonPath("$.[*].isLive").value(hasItem(DEFAULT_IS_LIVE.booleanValue())))
            .andExpect(jsonPath("$.[*].canSeek").value(hasItem(DEFAULT_CAN_SEEK.booleanValue())))
            .andExpect(jsonPath("$.[*].canPause").value(hasItem(DEFAULT_CAN_PAUSE.booleanValue())))
            .andExpect(jsonPath("$.[*].dateManifest").value(hasItem(DEFAULT_DATE_MANIFEST_STR)))
            .andExpect(jsonPath("$.[*].smoothOk").value(hasItem(DEFAULT_SMOOTH_OK.booleanValue())))
            .andExpect(jsonPath("$.[*].manifestFileContentType").value(hasItem(DEFAULT_MANIFEST_FILE_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].manifestFile").value(hasItem(Base64Utils.encodeToString(DEFAULT_MANIFEST_FILE))))
            .andExpect(jsonPath("$.[*].typeStreamIndex").value(hasItem(DEFAULT_TYPE_STREAM_INDEX.toString())))
            .andExpect(jsonPath("$.[*].nameVideo").value(hasItem(DEFAULT_NAME_VIDEO.toString())))
            .andExpect(jsonPath("$.[*].nameAudio").value(hasItem(DEFAULT_NAME_AUDIO.toString())))
            .andExpect(jsonPath("$.[*].nbrChunksVideo").value(hasItem(DEFAULT_NBR_CHUNKS_VIDEO)))
            .andExpect(jsonPath("$.[*].nbrChunksAudio").value(hasItem(DEFAULT_NBR_CHUNKS_AUDIO)))
            .andExpect(jsonPath("$.[*].chunksVideo").value(hasItem(DEFAULT_CHUNKS_VIDEO.toString())))
            .andExpect(jsonPath("$.[*].chunksAudio").value(hasItem(DEFAULT_CHUNKS_AUDIO.toString())))
            .andExpect(jsonPath("$.[*].qualityLevelsVideo").value(hasItem(DEFAULT_QUALITY_LEVELS_VIDEO.toString())))
            .andExpect(jsonPath("$.[*].qualityLevelsAudio").value(hasItem(DEFAULT_QUALITY_LEVELS_AUDIO.toString())))
            .andExpect(jsonPath("$.[*].maxWidthVideo").value(hasItem(DEFAULT_MAX_WIDTH_VIDEO.toString())))
            .andExpect(jsonPath("$.[*].maxHeightVideo").value(hasItem(DEFAULT_MAX_HEIGHT_VIDEO.toString())))
            .andExpect(jsonPath("$.[*].maxWidthAudio").value(hasItem(DEFAULT_MAX_WIDTH_AUDIO.toString())))
            .andExpect(jsonPath("$.[*].maxHeightAudio").value(hasItem(DEFAULT_MAX_HEIGHT_AUDIO.toString())))
            .andExpect(jsonPath("$.[*].displayWidthVideo").value(hasItem(DEFAULT_DISPLAY_WIDTH_VIDEO.toString())))
            .andExpect(jsonPath("$.[*].displayHeightVideo").value(hasItem(DEFAULT_DISPLAY_HEIGHT_VIDEO.toString())))
            .andExpect(jsonPath("$.[*].urlVideo").value(hasItem(DEFAULT_URL_VIDEO.toString())))
            .andExpect(jsonPath("$.[*].urlAudio").value(hasItem(DEFAULT_URL_AUDIO.toString())))
            .andExpect(jsonPath("$.[*].indexStrmAudio").value(hasItem(DEFAULT_INDEX_STRM_AUDIO.toString())))
            .andExpect(jsonPath("$.[*].language").value(hasItem(DEFAULT_LANGUAGE.toString())))
            .andExpect(jsonPath("$.[*].indexQlityVideo").value(hasItem(DEFAULT_INDEX_QLITY_VIDEO.toString())))
            .andExpect(jsonPath("$.[*].indexQlityAudio").value(hasItem(DEFAULT_INDEX_QLITY_AUDIO.toString())))
            .andExpect(jsonPath("$.[*].bitrateVideo").value(hasItem(DEFAULT_BITRATE_VIDEO.toString())))
            .andExpect(jsonPath("$.[*].bitrateAudio").value(hasItem(DEFAULT_BITRATE_AUDIO.toString())))
            .andExpect(jsonPath("$.[*].fourCcVideo").value(hasItem(DEFAULT_FOUR_CC_VIDEO.toString())))
            .andExpect(jsonPath("$.[*].fourCcAudio").value(hasItem(DEFAULT_FOUR_CC_AUDIO.toString())))
            .andExpect(jsonPath("$.[*].codecPrivateDataVideo").value(hasItem(DEFAULT_CODEC_PRIVATE_DATA_VIDEO.toString())))
            .andExpect(jsonPath("$.[*].codecPrivateDataAudio").value(hasItem(DEFAULT_CODEC_PRIVATE_DATA_AUDIO.toString())))
            .andExpect(jsonPath("$.[*].samplingRate").value(hasItem(DEFAULT_SAMPLING_RATE.toString())))
            .andExpect(jsonPath("$.[*].channels").value(hasItem(DEFAULT_CHANNELS.toString())))
            .andExpect(jsonPath("$.[*].bitsPerSample").value(hasItem(DEFAULT_BITS_PER_SAMPLE.toString())))
            .andExpect(jsonPath("$.[*].packetSize").value(hasItem(DEFAULT_PACKET_SIZE.toString())))
            .andExpect(jsonPath("$.[*].audioTag").value(hasItem(DEFAULT_AUDIO_TAG.toString())));
    }
}
