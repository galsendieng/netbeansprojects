(function() {
    'use strict';

    angular
        .module('abstractApp')
        .controller('ModeMyAbstractDialogController', ModeMyAbstractDialogController);

    ModeMyAbstractDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'Mode', 'Playing'];

    function ModeMyAbstractDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, Mode, Playing) {
        var vm = this;

        vm.mode = entity;
        vm.clear = clear;
        vm.save = save;
        vm.playings = Playing.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.mode.id !== null) {
                Mode.update(vm.mode, onSaveSuccess, onSaveError);
            } else {
                Mode.save(vm.mode, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('abstractApp:modeUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
