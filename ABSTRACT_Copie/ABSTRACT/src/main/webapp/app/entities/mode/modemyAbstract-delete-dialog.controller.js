(function() {
    'use strict';

    angular
        .module('abstractApp')
        .controller('ModeMyAbstractDeleteController',ModeMyAbstractDeleteController);

    ModeMyAbstractDeleteController.$inject = ['$uibModalInstance', 'entity', 'Mode'];

    function ModeMyAbstractDeleteController($uibModalInstance, entity, Mode) {
        var vm = this;

        vm.mode = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            Mode.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
