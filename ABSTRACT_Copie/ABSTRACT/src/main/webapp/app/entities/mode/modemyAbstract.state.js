(function() {
    'use strict';

    angular
        .module('abstractApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('modemyAbstract', {
            parent: 'entity',
            url: '/modemyAbstract',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'abstractApp.mode.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/mode/modesmyAbstract.html',
                    controller: 'ModeMyAbstractController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('mode');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('modemyAbstract-detail', {
            parent: 'entity',
            url: '/modemyAbstract/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'abstractApp.mode.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/mode/modemyAbstract-detail.html',
                    controller: 'ModeMyAbstractDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('mode');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'Mode', function($stateParams, Mode) {
                    return Mode.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'modemyAbstract',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('modemyAbstract-detail.edit', {
            parent: 'modemyAbstract-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/mode/modemyAbstract-dialog.html',
                    controller: 'ModeMyAbstractDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Mode', function(Mode) {
                            return Mode.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('modemyAbstract.new', {
            parent: 'modemyAbstract',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/mode/modemyAbstract-dialog.html',
                    controller: 'ModeMyAbstractDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                name: null,
                                abrStatic: null,
                                fragment: null,
                                suffixManifest: null,
                                deviceProfil: null,
                                clientVersion: null,
                                typeManifest: null,
                                description: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('modemyAbstract', null, { reload: 'modemyAbstract' });
                }, function() {
                    $state.go('modemyAbstract');
                });
            }]
        })
        .state('modemyAbstract.edit', {
            parent: 'modemyAbstract',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/mode/modemyAbstract-dialog.html',
                    controller: 'ModeMyAbstractDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Mode', function(Mode) {
                            return Mode.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('modemyAbstract', null, { reload: 'modemyAbstract' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('modemyAbstract.delete', {
            parent: 'modemyAbstract',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/mode/modemyAbstract-delete-dialog.html',
                    controller: 'ModeMyAbstractDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['Mode', function(Mode) {
                            return Mode.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('modemyAbstract', null, { reload: 'modemyAbstract' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
