(function() {
    'use strict';

    angular
        .module('abstractApp')
        .controller('PodMyAbstractDialogController', PodMyAbstractDialogController);

    PodMyAbstractDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'Pod', 'Streamer'];

    function PodMyAbstractDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, Pod, Streamer) {
        var vm = this;

        vm.pod = entity;
        vm.clear = clear;
        vm.save = save;
        vm.streamers = Streamer.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.pod.id !== null) {
                Pod.update(vm.pod, onSaveSuccess, onSaveError);
            } else {
                Pod.save(vm.pod, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('abstractApp:podUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
