(function() {
    'use strict';
    angular
        .module('abstractApp')
        .factory('Playing', Playing);

    Playing.$inject = ['$resource'];

    function Playing ($resource) {
        var resourceUrl =  'api/playings/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
