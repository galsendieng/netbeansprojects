(function() {
    'use strict';

    angular
        .module('abstractApp')
        .controller('PlayingMyAbstractDialogController', PlayingMyAbstractDialogController);

    PlayingMyAbstractDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'DataUtils', 'entity', 'Playing', 'Channels', 'Mode'];

    function PlayingMyAbstractDialogController ($timeout, $scope, $stateParams, $uibModalInstance, DataUtils, entity, Playing, Channels, Mode) {
        var vm = this;

        vm.playing = entity;
        vm.clear = clear;
        vm.byteSize = DataUtils.byteSize;
        vm.openFile = DataUtils.openFile;
        vm.save = save;
        vm.channels = Channels.query();
        vm.modes = Mode.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.playing.id !== null) {
                Playing.update(vm.playing, onSaveSuccess, onSaveError);
            } else {
                Playing.save(vm.playing, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('abstractApp:playingUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


        vm.setFileTest = function ($file, playing) {
            if ($file) {
                DataUtils.toBase64($file, function(base64Data) {
                    $scope.$apply(function() {
                        playing.fileTest = base64Data;
                        playing.fileTestContentType = $file.type;
                    });
                });
            }
        };

    }
})();
