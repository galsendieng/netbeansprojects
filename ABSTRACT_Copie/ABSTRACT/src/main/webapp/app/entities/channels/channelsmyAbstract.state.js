(function() {
    'use strict';

    angular
        .module('abstractApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('channelsmyAbstract', {
            parent: 'entity',
            url: '/channelsmyAbstract?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'abstractApp.channels.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/channels/channelsmyAbstract.html',
                    controller: 'ChannelsMyAbstractController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('channels');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('channelsmyAbstract-detail', {
            parent: 'entity',
            url: '/channelsmyAbstract/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'abstractApp.channels.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/channels/channelsmyAbstract-detail.html',
                    controller: 'ChannelsMyAbstractDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('channels');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'Channels', function($stateParams, Channels) {
                    return Channels.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'channelsmyAbstract',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('channelsmyAbstract-detail.edit', {
            parent: 'channelsmyAbstract-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/channels/channelsmyAbstract-dialog.html',
                    controller: 'ChannelsMyAbstractDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Channels', function(Channels) {
                            return Channels.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('channelsmyAbstract.new', {
            parent: 'channelsmyAbstract',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/channels/channelsmyAbstract-dialog.html',
                    controller: 'ChannelsMyAbstractDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                channel: null,
                                description: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('channelsmyAbstract', null, { reload: 'channelsmyAbstract' });
                }, function() {
                    $state.go('channelsmyAbstract');
                });
            }]
        })
        .state('channelsmyAbstract.edit', {
            parent: 'channelsmyAbstract',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/channels/channelsmyAbstract-dialog.html',
                    controller: 'ChannelsMyAbstractDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Channels', function(Channels) {
                            return Channels.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('channelsmyAbstract', null, { reload: 'channelsmyAbstract' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('channelsmyAbstract.delete', {
            parent: 'channelsmyAbstract',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/channels/channelsmyAbstract-delete-dialog.html',
                    controller: 'ChannelsMyAbstractDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['Channels', function(Channels) {
                            return Channels.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('channelsmyAbstract', null, { reload: 'channelsmyAbstract' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
