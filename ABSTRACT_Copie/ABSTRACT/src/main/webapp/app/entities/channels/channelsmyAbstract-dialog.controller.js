(function() {
    'use strict';

    angular
        .module('abstractApp')
        .controller('ChannelsMyAbstractDialogController', ChannelsMyAbstractDialogController);

    ChannelsMyAbstractDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'Channels', 'Streamer', 'Playing'];

    function ChannelsMyAbstractDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, Channels, Streamer, Playing) {
        var vm = this;

        vm.channels = entity;
        vm.clear = clear;
        vm.save = save;
        vm.streamers = Streamer.query();
        vm.playings = Playing.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.channels.id !== null) {
                Channels.update(vm.channels, onSaveSuccess, onSaveError);
            } else {
                Channels.save(vm.channels, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('abstractApp:channelsUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
