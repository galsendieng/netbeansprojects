(function() {
    'use strict';

    angular
        .module('abstractApp')
        .controller('ChannelsMyAbstractDetailController', ChannelsMyAbstractDetailController);

    ChannelsMyAbstractDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Channels', 'Streamer', 'Playing'];

    function ChannelsMyAbstractDetailController($scope, $rootScope, $stateParams, previousState, entity, Channels, Streamer, Playing) {
        var vm = this;

        vm.channels = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('abstractApp:channelsUpdate', function(event, result) {
            vm.channels = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
