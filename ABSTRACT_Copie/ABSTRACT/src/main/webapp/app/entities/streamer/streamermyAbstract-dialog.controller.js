(function() {
    'use strict';

    angular
        .module('abstractApp')
        .controller('StreamerMyAbstractDialogController', StreamerMyAbstractDialogController);

    StreamerMyAbstractDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'Streamer', 'Pod', 'Channels'];

    function StreamerMyAbstractDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, Streamer, Pod, Channels) {
        var vm = this;

        vm.streamer = entity;
        vm.clear = clear;
        vm.save = save;
        vm.pods = Pod.query();
        vm.channels = Channels.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.streamer.id !== null) {
                Streamer.update(vm.streamer, onSaveSuccess, onSaveError);
            } else {
                Streamer.save(vm.streamer, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('abstractApp:streamerUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
