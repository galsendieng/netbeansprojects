(function() {
    'use strict';

    angular
        .module('abstractApp')
        .controller('SmoothStreamingMediaMyAbstractDetailController', SmoothStreamingMediaMyAbstractDetailController);

    SmoothStreamingMediaMyAbstractDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'DataUtils', 'entity', 'SmoothStreamingMedia'];

    function SmoothStreamingMediaMyAbstractDetailController($scope, $rootScope, $stateParams, previousState, DataUtils, entity, SmoothStreamingMedia) {
        var vm = this;

        vm.smoothStreamingMedia = entity;
        vm.previousState = previousState.name;
        vm.byteSize = DataUtils.byteSize;
        vm.openFile = DataUtils.openFile;

        var unsubscribe = $rootScope.$on('abstractApp:smoothStreamingMediaUpdate', function(event, result) {
            vm.smoothStreamingMedia = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
