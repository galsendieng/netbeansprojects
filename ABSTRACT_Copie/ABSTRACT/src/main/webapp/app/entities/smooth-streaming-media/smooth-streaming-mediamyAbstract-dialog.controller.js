(function() {
    'use strict';

    angular
        .module('abstractApp')
        .controller('SmoothStreamingMediaMyAbstractDialogController', SmoothStreamingMediaMyAbstractDialogController);

    SmoothStreamingMediaMyAbstractDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'DataUtils', 'entity', 'SmoothStreamingMedia'];

    function SmoothStreamingMediaMyAbstractDialogController ($timeout, $scope, $stateParams, $uibModalInstance, DataUtils, entity, SmoothStreamingMedia) {
        var vm = this;

        vm.smoothStreamingMedia = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.byteSize = DataUtils.byteSize;
        vm.openFile = DataUtils.openFile;
        vm.save = save;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.smoothStreamingMedia.id !== null) {
                SmoothStreamingMedia.update(vm.smoothStreamingMedia, onSaveSuccess, onSaveError);
            } else {
                SmoothStreamingMedia.save(vm.smoothStreamingMedia, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('abstractApp:smoothStreamingMediaUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.dateManifest = false;

        vm.setManifestFile = function ($file, smoothStreamingMedia) {
            if ($file) {
                DataUtils.toBase64($file, function(base64Data) {
                    $scope.$apply(function() {
                        smoothStreamingMedia.manifestFile = base64Data;
                        smoothStreamingMedia.manifestFileContentType = $file.type;
                    });
                });
            }
        };

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
