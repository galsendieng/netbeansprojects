package com.orange.myabstract.service.impl;

import com.orange.myabstract.service.SmoothStreamingMediaService;
import com.orange.myabstract.domain.SmoothStreamingMedia;
import com.orange.myabstract.repository.SmoothStreamingMediaRepository;
import com.orange.myabstract.repository.search.SmoothStreamingMediaSearchRepository;
import com.orange.myabstract.service.dto.SmoothStreamingMediaDTO;
import com.orange.myabstract.service.mapper.SmoothStreamingMediaMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing SmoothStreamingMedia.
 */
@Service
@Transactional
public class SmoothStreamingMediaServiceImpl implements SmoothStreamingMediaService{

    private final Logger log = LoggerFactory.getLogger(SmoothStreamingMediaServiceImpl.class);
    
    @Inject
    private SmoothStreamingMediaRepository smoothStreamingMediaRepository;

    @Inject
    private SmoothStreamingMediaMapper smoothStreamingMediaMapper;

    @Inject
    private SmoothStreamingMediaSearchRepository smoothStreamingMediaSearchRepository;

    /**
     * Save a smoothStreamingMedia.
     *
     * @param smoothStreamingMediaDTO the entity to save
     * @return the persisted entity
     */
    public SmoothStreamingMediaDTO save(SmoothStreamingMediaDTO smoothStreamingMediaDTO) {
        log.debug("Request to save SmoothStreamingMedia : {}", smoothStreamingMediaDTO);
        SmoothStreamingMedia smoothStreamingMedia = smoothStreamingMediaMapper.smoothStreamingMediaDTOToSmoothStreamingMedia(smoothStreamingMediaDTO);
        smoothStreamingMedia = smoothStreamingMediaRepository.save(smoothStreamingMedia);
        SmoothStreamingMediaDTO result = smoothStreamingMediaMapper.smoothStreamingMediaToSmoothStreamingMediaDTO(smoothStreamingMedia);
        smoothStreamingMediaSearchRepository.save(smoothStreamingMedia);
        return result;
    }

    /**
     *  Get all the smoothStreamingMedias.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true) 
    public Page<SmoothStreamingMediaDTO> findAll(Pageable pageable) {
        log.debug("Request to get all SmoothStreamingMedias");
        Page<SmoothStreamingMedia> result = smoothStreamingMediaRepository.findAll(pageable);
        return result.map(smoothStreamingMedia -> smoothStreamingMediaMapper.smoothStreamingMediaToSmoothStreamingMediaDTO(smoothStreamingMedia));
    }

    /**
     *  Get one smoothStreamingMedia by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true) 
    public SmoothStreamingMediaDTO findOne(Long id) {
        log.debug("Request to get SmoothStreamingMedia : {}", id);
        SmoothStreamingMedia smoothStreamingMedia = smoothStreamingMediaRepository.findOne(id);
        SmoothStreamingMediaDTO smoothStreamingMediaDTO = smoothStreamingMediaMapper.smoothStreamingMediaToSmoothStreamingMediaDTO(smoothStreamingMedia);
        return smoothStreamingMediaDTO;
    }

    /**
     *  Delete the  smoothStreamingMedia by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete SmoothStreamingMedia : {}", id);
        smoothStreamingMediaRepository.delete(id);
        smoothStreamingMediaSearchRepository.delete(id);
    }

    /**
     * Search for the smoothStreamingMedia corresponding to the query.
     *
     *  @param query the query of the search
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<SmoothStreamingMediaDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of SmoothStreamingMedias for query {}", query);
        Page<SmoothStreamingMedia> result = smoothStreamingMediaSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(smoothStreamingMedia -> smoothStreamingMediaMapper.smoothStreamingMediaToSmoothStreamingMediaDTO(smoothStreamingMedia));
    }
}
