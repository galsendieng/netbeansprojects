package com.orange.myabstract.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.orange.myabstract.service.ModeService;
import com.orange.myabstract.web.rest.util.HeaderUtil;
import com.orange.myabstract.service.dto.ModeDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Mode.
 */
@RestController
@RequestMapping("/api")
public class ModeResource {

    private final Logger log = LoggerFactory.getLogger(ModeResource.class);
        
    @Inject
    private ModeService modeService;

    /**
     * POST  /modes : Create a new mode.
     *
     * @param modeDTO the modeDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new modeDTO, or with status 400 (Bad Request) if the mode has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/modes")
    @Timed
    public ResponseEntity<ModeDTO> createMode(@Valid @RequestBody ModeDTO modeDTO) throws URISyntaxException {
        log.debug("REST request to save Mode : {}", modeDTO);
        if (modeDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("mode", "idexists", "A new mode cannot already have an ID")).body(null);
        }
        ModeDTO result = modeService.save(modeDTO);
        return ResponseEntity.created(new URI("/api/modes/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("mode", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /modes : Updates an existing mode.
     *
     * @param modeDTO the modeDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated modeDTO,
     * or with status 400 (Bad Request) if the modeDTO is not valid,
     * or with status 500 (Internal Server Error) if the modeDTO couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/modes")
    @Timed
    public ResponseEntity<ModeDTO> updateMode(@Valid @RequestBody ModeDTO modeDTO) throws URISyntaxException {
        log.debug("REST request to update Mode : {}", modeDTO);
        if (modeDTO.getId() == null) {
            return createMode(modeDTO);
        }
        ModeDTO result = modeService.save(modeDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("mode", modeDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /modes : get all the modes.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of modes in body
     */
    @GetMapping("/modes")
    @Timed
    public List<ModeDTO> getAllModes() {
        log.debug("REST request to get all Modes");
        return modeService.findAll();
    }

    /**
     * GET  /modes/:id : get the "id" mode.
     *
     * @param id the id of the modeDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the modeDTO, or with status 404 (Not Found)
     */
    @GetMapping("/modes/{id}")
    @Timed
    public ResponseEntity<ModeDTO> getMode(@PathVariable Long id) {
        log.debug("REST request to get Mode : {}", id);
        ModeDTO modeDTO = modeService.findOne(id);
        return Optional.ofNullable(modeDTO)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /modes/:id : delete the "id" mode.
     *
     * @param id the id of the modeDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/modes/{id}")
    @Timed
    public ResponseEntity<Void> deleteMode(@PathVariable Long id) {
        log.debug("REST request to delete Mode : {}", id);
        modeService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("mode", id.toString())).build();
    }

    /**
     * SEARCH  /_search/modes?query=:query : search for the mode corresponding
     * to the query.
     *
     * @param query the query of the mode search 
     * @return the result of the search
     */
    @GetMapping("/_search/modes")
    @Timed
    public List<ModeDTO> searchModes(@RequestParam String query) {
        log.debug("REST request to search Modes for query {}", query);
        return modeService.search(query);
    }


}
