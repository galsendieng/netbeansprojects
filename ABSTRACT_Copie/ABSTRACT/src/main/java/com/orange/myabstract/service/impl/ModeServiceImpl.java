package com.orange.myabstract.service.impl;

import com.orange.myabstract.service.ModeService;
import com.orange.myabstract.domain.Mode;
import com.orange.myabstract.repository.ModeRepository;
import com.orange.myabstract.repository.search.ModeSearchRepository;
import com.orange.myabstract.service.dto.ModeDTO;
import com.orange.myabstract.service.mapper.ModeMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing Mode.
 */
@Service
@Transactional
public class ModeServiceImpl implements ModeService{

    private final Logger log = LoggerFactory.getLogger(ModeServiceImpl.class);
    
    @Inject
    private ModeRepository modeRepository;

    @Inject
    private ModeMapper modeMapper;

    @Inject
    private ModeSearchRepository modeSearchRepository;

    /**
     * Save a mode.
     *
     * @param modeDTO the entity to save
     * @return the persisted entity
     */
    public ModeDTO save(ModeDTO modeDTO) {
        log.debug("Request to save Mode : {}", modeDTO);
        Mode mode = modeMapper.modeDTOToMode(modeDTO);
        mode = modeRepository.save(mode);
        ModeDTO result = modeMapper.modeToModeDTO(mode);
        modeSearchRepository.save(mode);
        return result;
    }

    /**
     *  Get all the modes.
     *  
     *  @return the list of entities
     */
    @Transactional(readOnly = true) 
    public List<ModeDTO> findAll() {
        log.debug("Request to get all Modes");
        List<ModeDTO> result = modeRepository.findAll().stream()
            .map(modeMapper::modeToModeDTO)
            .collect(Collectors.toCollection(LinkedList::new));

        return result;
    }

    /**
     *  Get one mode by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true) 
    public ModeDTO findOne(Long id) {
        log.debug("Request to get Mode : {}", id);
        Mode mode = modeRepository.findOne(id);
        ModeDTO modeDTO = modeMapper.modeToModeDTO(mode);
        return modeDTO;
    }

    /**
     *  Delete the  mode by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Mode : {}", id);
        modeRepository.delete(id);
        modeSearchRepository.delete(id);
    }

    /**
     * Search for the mode corresponding to the query.
     *
     *  @param query the query of the search
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<ModeDTO> search(String query) {
        log.debug("Request to search Modes for query {}", query);
        return StreamSupport
            .stream(modeSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .map(modeMapper::modeToModeDTO)
            .collect(Collectors.toList());
    }
}
