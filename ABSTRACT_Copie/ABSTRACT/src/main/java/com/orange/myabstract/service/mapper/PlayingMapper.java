package com.orange.myabstract.service.mapper;

import com.orange.myabstract.domain.*;
import com.orange.myabstract.service.dto.PlayingDTO;

import org.mapstruct.*;
import java.util.List;

/**
 * Mapper for the entity Playing and its DTO PlayingDTO.
 */
@Mapper(componentModel = "spring", uses = {ChannelsMapper.class, })
public interface PlayingMapper {

    @Mapping(source = "theMode.id", target = "theModeId")
    @Mapping(source = "theMode.name", target = "theModeName")
    PlayingDTO playingToPlayingDTO(Playing playing);

    List<PlayingDTO> playingsToPlayingDTOs(List<Playing> playings);

    @Mapping(source = "theModeId", target = "theMode")
    Playing playingDTOToPlaying(PlayingDTO playingDTO);

    List<Playing> playingDTOsToPlayings(List<PlayingDTO> playingDTOs);

    default Channels channelsFromId(Long id) {
        if (id == null) {
            return null;
        }
        Channels channels = new Channels();
        channels.setId(id);
        return channels;
    }

    default Mode modeFromId(Long id) {
        if (id == null) {
            return null;
        }
        Mode mode = new Mode();
        mode.setId(id);
        return mode;
    }
}
