package com.orange.myabstract.repository;

import com.orange.myabstract.domain.SmoothStreamingMedia;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the SmoothStreamingMedia entity.
 */
@SuppressWarnings("unused")
public interface SmoothStreamingMediaRepository extends JpaRepository<SmoothStreamingMedia,Long> {

}
