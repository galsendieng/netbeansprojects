package com.orange.myabstract.service.dto;

import java.time.ZonedDateTime;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Lob;


/**
 * A DTO for the SmoothStreamingMedia entity.
 */
public class SmoothStreamingMediaDTO implements Serializable {

    private Long id;

    @NotNull
    private String nameSmooth;

    private String majorVersion;

    private String minorVersion;

    private String duration;

    private String dvrWindowLength;

    private String lookAheadFragmentCount;

    private Boolean isLive;

    private Boolean canSeek;

    private Boolean canPause;

    private ZonedDateTime dateManifest;

    private Boolean smoothOk;

    @Lob
    private byte[] manifestFile;

    private String manifestFileContentType;
    private String typeStreamIndex;

    private String nameVideo;

    private String nameAudio;

    private Integer nbrChunksVideo;

    private Integer nbrChunksAudio;

    private String chunksVideo;

    private String chunksAudio;

    private String qualityLevelsVideo;

    private String qualityLevelsAudio;

    private String maxWidthVideo;

    private String maxHeightVideo;

    private String maxWidthAudio;

    private String maxHeightAudio;

    private String displayWidthVideo;

    private String displayHeightVideo;

    private String urlVideo;

    private String urlAudio;

    private String indexStrmAudio;

    private String language;

    private String indexQlityVideo;

    private String indexQlityAudio;

    private String bitrateVideo;

    private String bitrateAudio;

    private String fourCcVideo;

    private String fourCcAudio;

    private String codecPrivateDataVideo;

    private String codecPrivateDataAudio;

    private String samplingRate;

    private String channels;

    private String bitsPerSample;

    private String packetSize;

    private String audioTag;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public String getNameSmooth() {
        return nameSmooth;
    }

    public void setNameSmooth(String nameSmooth) {
        this.nameSmooth = nameSmooth;
    }
    public String getMajorVersion() {
        return majorVersion;
    }

    public void setMajorVersion(String majorVersion) {
        this.majorVersion = majorVersion;
    }
    public String getMinorVersion() {
        return minorVersion;
    }

    public void setMinorVersion(String minorVersion) {
        this.minorVersion = minorVersion;
    }
    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }
    public String getDvrWindowLength() {
        return dvrWindowLength;
    }

    public void setDvrWindowLength(String dvrWindowLength) {
        this.dvrWindowLength = dvrWindowLength;
    }
    public String getLookAheadFragmentCount() {
        return lookAheadFragmentCount;
    }

    public void setLookAheadFragmentCount(String lookAheadFragmentCount) {
        this.lookAheadFragmentCount = lookAheadFragmentCount;
    }
    public Boolean getIsLive() {
        return isLive;
    }

    public void setIsLive(Boolean isLive) {
        this.isLive = isLive;
    }
    public Boolean getCanSeek() {
        return canSeek;
    }

    public void setCanSeek(Boolean canSeek) {
        this.canSeek = canSeek;
    }
    public Boolean getCanPause() {
        return canPause;
    }

    public void setCanPause(Boolean canPause) {
        this.canPause = canPause;
    }
    public ZonedDateTime getDateManifest() {
        return dateManifest;
    }

    public void setDateManifest(ZonedDateTime dateManifest) {
        this.dateManifest = dateManifest;
    }
    public Boolean getSmoothOk() {
        return smoothOk;
    }

    public void setSmoothOk(Boolean smoothOk) {
        this.smoothOk = smoothOk;
    }
    public byte[] getManifestFile() {
        return manifestFile;
    }

    public void setManifestFile(byte[] manifestFile) {
        this.manifestFile = manifestFile;
    }

    public String getManifestFileContentType() {
        return manifestFileContentType;
    }

    public void setManifestFileContentType(String manifestFileContentType) {
        this.manifestFileContentType = manifestFileContentType;
    }
    public String getTypeStreamIndex() {
        return typeStreamIndex;
    }

    public void setTypeStreamIndex(String typeStreamIndex) {
        this.typeStreamIndex = typeStreamIndex;
    }
    public String getNameVideo() {
        return nameVideo;
    }

    public void setNameVideo(String nameVideo) {
        this.nameVideo = nameVideo;
    }
    public String getNameAudio() {
        return nameAudio;
    }

    public void setNameAudio(String nameAudio) {
        this.nameAudio = nameAudio;
    }
    public Integer getNbrChunksVideo() {
        return nbrChunksVideo;
    }

    public void setNbrChunksVideo(Integer nbrChunksVideo) {
        this.nbrChunksVideo = nbrChunksVideo;
    }
    public Integer getNbrChunksAudio() {
        return nbrChunksAudio;
    }

    public void setNbrChunksAudio(Integer nbrChunksAudio) {
        this.nbrChunksAudio = nbrChunksAudio;
    }
    public String getChunksVideo() {
        return chunksVideo;
    }

    public void setChunksVideo(String chunksVideo) {
        this.chunksVideo = chunksVideo;
    }
    public String getChunksAudio() {
        return chunksAudio;
    }

    public void setChunksAudio(String chunksAudio) {
        this.chunksAudio = chunksAudio;
    }
    public String getQualityLevelsVideo() {
        return qualityLevelsVideo;
    }

    public void setQualityLevelsVideo(String qualityLevelsVideo) {
        this.qualityLevelsVideo = qualityLevelsVideo;
    }
    public String getQualityLevelsAudio() {
        return qualityLevelsAudio;
    }

    public void setQualityLevelsAudio(String qualityLevelsAudio) {
        this.qualityLevelsAudio = qualityLevelsAudio;
    }
    public String getMaxWidthVideo() {
        return maxWidthVideo;
    }

    public void setMaxWidthVideo(String maxWidthVideo) {
        this.maxWidthVideo = maxWidthVideo;
    }
    public String getMaxHeightVideo() {
        return maxHeightVideo;
    }

    public void setMaxHeightVideo(String maxHeightVideo) {
        this.maxHeightVideo = maxHeightVideo;
    }
    public String getMaxWidthAudio() {
        return maxWidthAudio;
    }

    public void setMaxWidthAudio(String maxWidthAudio) {
        this.maxWidthAudio = maxWidthAudio;
    }
    public String getMaxHeightAudio() {
        return maxHeightAudio;
    }

    public void setMaxHeightAudio(String maxHeightAudio) {
        this.maxHeightAudio = maxHeightAudio;
    }
    public String getDisplayWidthVideo() {
        return displayWidthVideo;
    }

    public void setDisplayWidthVideo(String displayWidthVideo) {
        this.displayWidthVideo = displayWidthVideo;
    }
    public String getDisplayHeightVideo() {
        return displayHeightVideo;
    }

    public void setDisplayHeightVideo(String displayHeightVideo) {
        this.displayHeightVideo = displayHeightVideo;
    }
    public String getUrlVideo() {
        return urlVideo;
    }

    public void setUrlVideo(String urlVideo) {
        this.urlVideo = urlVideo;
    }
    public String getUrlAudio() {
        return urlAudio;
    }

    public void setUrlAudio(String urlAudio) {
        this.urlAudio = urlAudio;
    }
    public String getIndexStrmAudio() {
        return indexStrmAudio;
    }

    public void setIndexStrmAudio(String indexStrmAudio) {
        this.indexStrmAudio = indexStrmAudio;
    }
    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }
    public String getIndexQlityVideo() {
        return indexQlityVideo;
    }

    public void setIndexQlityVideo(String indexQlityVideo) {
        this.indexQlityVideo = indexQlityVideo;
    }
    public String getIndexQlityAudio() {
        return indexQlityAudio;
    }

    public void setIndexQlityAudio(String indexQlityAudio) {
        this.indexQlityAudio = indexQlityAudio;
    }
    public String getBitrateVideo() {
        return bitrateVideo;
    }

    public void setBitrateVideo(String bitrateVideo) {
        this.bitrateVideo = bitrateVideo;
    }
    public String getBitrateAudio() {
        return bitrateAudio;
    }

    public void setBitrateAudio(String bitrateAudio) {
        this.bitrateAudio = bitrateAudio;
    }
    public String getFourCcVideo() {
        return fourCcVideo;
    }

    public void setFourCcVideo(String fourCcVideo) {
        this.fourCcVideo = fourCcVideo;
    }
    public String getFourCcAudio() {
        return fourCcAudio;
    }

    public void setFourCcAudio(String fourCcAudio) {
        this.fourCcAudio = fourCcAudio;
    }
    public String getCodecPrivateDataVideo() {
        return codecPrivateDataVideo;
    }

    public void setCodecPrivateDataVideo(String codecPrivateDataVideo) {
        this.codecPrivateDataVideo = codecPrivateDataVideo;
    }
    public String getCodecPrivateDataAudio() {
        return codecPrivateDataAudio;
    }

    public void setCodecPrivateDataAudio(String codecPrivateDataAudio) {
        this.codecPrivateDataAudio = codecPrivateDataAudio;
    }
    public String getSamplingRate() {
        return samplingRate;
    }

    public void setSamplingRate(String samplingRate) {
        this.samplingRate = samplingRate;
    }
    public String getChannels() {
        return channels;
    }

    public void setChannels(String channels) {
        this.channels = channels;
    }
    public String getBitsPerSample() {
        return bitsPerSample;
    }

    public void setBitsPerSample(String bitsPerSample) {
        this.bitsPerSample = bitsPerSample;
    }
    public String getPacketSize() {
        return packetSize;
    }

    public void setPacketSize(String packetSize) {
        this.packetSize = packetSize;
    }
    public String getAudioTag() {
        return audioTag;
    }

    public void setAudioTag(String audioTag) {
        this.audioTag = audioTag;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        SmoothStreamingMediaDTO smoothStreamingMediaDTO = (SmoothStreamingMediaDTO) o;

        if ( ! Objects.equals(id, smoothStreamingMediaDTO.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "SmoothStreamingMediaDTO{" +
            "id=" + id +
            ", nameSmooth='" + nameSmooth + "'" +
            ", majorVersion='" + majorVersion + "'" +
            ", minorVersion='" + minorVersion + "'" +
            ", duration='" + duration + "'" +
            ", dvrWindowLength='" + dvrWindowLength + "'" +
            ", lookAheadFragmentCount='" + lookAheadFragmentCount + "'" +
            ", isLive='" + isLive + "'" +
            ", canSeek='" + canSeek + "'" +
            ", canPause='" + canPause + "'" +
            ", dateManifest='" + dateManifest + "'" +
            ", smoothOk='" + smoothOk + "'" +
            ", manifestFile='" + manifestFile + "'" +
            ", typeStreamIndex='" + typeStreamIndex + "'" +
            ", nameVideo='" + nameVideo + "'" +
            ", nameAudio='" + nameAudio + "'" +
            ", nbrChunksVideo='" + nbrChunksVideo + "'" +
            ", nbrChunksAudio='" + nbrChunksAudio + "'" +
            ", chunksVideo='" + chunksVideo + "'" +
            ", chunksAudio='" + chunksAudio + "'" +
            ", qualityLevelsVideo='" + qualityLevelsVideo + "'" +
            ", qualityLevelsAudio='" + qualityLevelsAudio + "'" +
            ", maxWidthVideo='" + maxWidthVideo + "'" +
            ", maxHeightVideo='" + maxHeightVideo + "'" +
            ", maxWidthAudio='" + maxWidthAudio + "'" +
            ", maxHeightAudio='" + maxHeightAudio + "'" +
            ", displayWidthVideo='" + displayWidthVideo + "'" +
            ", displayHeightVideo='" + displayHeightVideo + "'" +
            ", urlVideo='" + urlVideo + "'" +
            ", urlAudio='" + urlAudio + "'" +
            ", indexStrmAudio='" + indexStrmAudio + "'" +
            ", language='" + language + "'" +
            ", indexQlityVideo='" + indexQlityVideo + "'" +
            ", indexQlityAudio='" + indexQlityAudio + "'" +
            ", bitrateVideo='" + bitrateVideo + "'" +
            ", bitrateAudio='" + bitrateAudio + "'" +
            ", fourCcVideo='" + fourCcVideo + "'" +
            ", fourCcAudio='" + fourCcAudio + "'" +
            ", codecPrivateDataVideo='" + codecPrivateDataVideo + "'" +
            ", codecPrivateDataAudio='" + codecPrivateDataAudio + "'" +
            ", samplingRate='" + samplingRate + "'" +
            ", channels='" + channels + "'" +
            ", bitsPerSample='" + bitsPerSample + "'" +
            ", packetSize='" + packetSize + "'" +
            ", audioTag='" + audioTag + "'" +
            '}';
    }
}
