package com.orange.myabstract.service.dto;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;


/**
 * A DTO for the Streamer entity.
 */
public class StreamerDTO implements Serializable {

    private Long id;

    @NotNull
    private String nameStreamer;

    @NotNull
    private String address;

    @Size(max = 200)
    private String description;


    private Long lePodId;
    

    private String lePodName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public String getNameStreamer() {
        return nameStreamer;
    }

    public void setNameStreamer(String nameStreamer) {
        this.nameStreamer = nameStreamer;
    }
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getLePodId() {
        return lePodId;
    }

    public void setLePodId(Long podId) {
        this.lePodId = podId;
    }


    public String getLePodName() {
        return lePodName;
    }

    public void setLePodName(String podName) {
        this.lePodName = podName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        StreamerDTO streamerDTO = (StreamerDTO) o;

        if ( ! Objects.equals(id, streamerDTO.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "StreamerDTO{" +
            "id=" + id +
            ", nameStreamer='" + nameStreamer + "'" +
            ", address='" + address + "'" +
            ", description='" + description + "'" +
            '}';
    }
}
