package com.orange.myabstract.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Channels.
 */
@Entity
@Table(name = "channels")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "channels")
public class Channels implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Column(name = "channel", nullable = false)
    private String channel;

    @Size(max = 200)
    @Column(name = "description", length = 200)
    private String description;

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "channels_streamer",
               joinColumns = @JoinColumn(name="channels_id", referencedColumnName="ID"),
               inverseJoinColumns = @JoinColumn(name="streamers_id", referencedColumnName="ID"))
    private Set<Streamer> streamers = new HashSet<>();

    @ManyToMany(mappedBy = "listchannels")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Playing> plays = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getChannel() {
        return channel;
    }

    public Channels channel(String channel) {
        this.channel = channel;
        return this;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getDescription() {
        return description;
    }

    public Channels description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<Streamer> getStreamers() {
        return streamers;
    }

    public Channels streamers(Set<Streamer> streamers) {
        this.streamers = streamers;
        return this;
    }

    public Channels addStreamer(Streamer streamer) {
        streamers.add(streamer);
        streamer.getTheChannels().add(this);
        return this;
    }

    public Channels removeStreamer(Streamer streamer) {
        streamers.remove(streamer);
        streamer.getTheChannels().remove(this);
        return this;
    }

    public void setStreamers(Set<Streamer> streamers) {
        this.streamers = streamers;
    }

    public Set<Playing> getPlays() {
        return plays;
    }

    public Channels plays(Set<Playing> playings) {
        this.plays = playings;
        return this;
    }

    public Channels addPlay(Playing playing) {
        plays.add(playing);
        playing.getListchannels().add(this);
        return this;
    }

    public Channels removePlay(Playing playing) {
        plays.remove(playing);
        playing.getListchannels().remove(this);
        return this;
    }

    public void setPlays(Set<Playing> playings) {
        this.plays = playings;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Channels channels = (Channels) o;
        if(channels.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, channels.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Channels{" +
            "id=" + id +
            ", channel='" + channel + "'" +
            ", description='" + description + "'" +
            '}';
    }
}
