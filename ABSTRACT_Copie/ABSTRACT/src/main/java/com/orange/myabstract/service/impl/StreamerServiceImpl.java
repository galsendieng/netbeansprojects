package com.orange.myabstract.service.impl;

import com.orange.myabstract.service.StreamerService;
import com.orange.myabstract.domain.Streamer;
import com.orange.myabstract.repository.StreamerRepository;
import com.orange.myabstract.repository.search.StreamerSearchRepository;
import com.orange.myabstract.service.dto.StreamerDTO;
import com.orange.myabstract.service.mapper.StreamerMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing Streamer.
 */
@Service
@Transactional
public class StreamerServiceImpl implements StreamerService{

    private final Logger log = LoggerFactory.getLogger(StreamerServiceImpl.class);
    
    @Inject
    private StreamerRepository streamerRepository;

    @Inject
    private StreamerMapper streamerMapper;

    @Inject
    private StreamerSearchRepository streamerSearchRepository;

    /**
     * Save a streamer.
     *
     * @param streamerDTO the entity to save
     * @return the persisted entity
     */
    public StreamerDTO save(StreamerDTO streamerDTO) {
        log.debug("Request to save Streamer : {}", streamerDTO);
        Streamer streamer = streamerMapper.streamerDTOToStreamer(streamerDTO);
        streamer = streamerRepository.save(streamer);
        StreamerDTO result = streamerMapper.streamerToStreamerDTO(streamer);
        streamerSearchRepository.save(streamer);
        return result;
    }

    /**
     *  Get all the streamers.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true) 
    public Page<StreamerDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Streamers");
        Page<Streamer> result = streamerRepository.findAll(pageable);
        return result.map(streamer -> streamerMapper.streamerToStreamerDTO(streamer));
    }

    /**
     *  Get one streamer by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true) 
    public StreamerDTO findOne(Long id) {
        log.debug("Request to get Streamer : {}", id);
        Streamer streamer = streamerRepository.findOne(id);
        StreamerDTO streamerDTO = streamerMapper.streamerToStreamerDTO(streamer);
        return streamerDTO;
    }

    /**
     *  Delete the  streamer by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Streamer : {}", id);
        streamerRepository.delete(id);
        streamerSearchRepository.delete(id);
    }

    /**
     * Search for the streamer corresponding to the query.
     *
     *  @param query the query of the search
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<StreamerDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Streamers for query {}", query);
        Page<Streamer> result = streamerSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(streamer -> streamerMapper.streamerToStreamerDTO(streamer));
    }
}
