package com.orange.myabstract.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.orange.myabstract.service.SmoothStreamingMediaService;
import com.orange.myabstract.web.rest.util.HeaderUtil;
import com.orange.myabstract.web.rest.util.PaginationUtil;
import com.orange.myabstract.service.dto.SmoothStreamingMediaDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing SmoothStreamingMedia.
 */
@RestController
@RequestMapping("/api")
public class SmoothStreamingMediaResource {

    private final Logger log = LoggerFactory.getLogger(SmoothStreamingMediaResource.class);
        
    @Inject
    private SmoothStreamingMediaService smoothStreamingMediaService;

    /**
     * POST  /smooth-streaming-medias : Create a new smoothStreamingMedia.
     *
     * @param smoothStreamingMediaDTO the smoothStreamingMediaDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new smoothStreamingMediaDTO, or with status 400 (Bad Request) if the smoothStreamingMedia has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/smooth-streaming-medias")
    @Timed
    public ResponseEntity<SmoothStreamingMediaDTO> createSmoothStreamingMedia(@Valid @RequestBody SmoothStreamingMediaDTO smoothStreamingMediaDTO) throws URISyntaxException {
        log.debug("REST request to save SmoothStreamingMedia : {}", smoothStreamingMediaDTO);
        if (smoothStreamingMediaDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("smoothStreamingMedia", "idexists", "A new smoothStreamingMedia cannot already have an ID")).body(null);
        }
        SmoothStreamingMediaDTO result = smoothStreamingMediaService.save(smoothStreamingMediaDTO);
        return ResponseEntity.created(new URI("/api/smooth-streaming-medias/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("smoothStreamingMedia", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /smooth-streaming-medias : Updates an existing smoothStreamingMedia.
     *
     * @param smoothStreamingMediaDTO the smoothStreamingMediaDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated smoothStreamingMediaDTO,
     * or with status 400 (Bad Request) if the smoothStreamingMediaDTO is not valid,
     * or with status 500 (Internal Server Error) if the smoothStreamingMediaDTO couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/smooth-streaming-medias")
    @Timed
    public ResponseEntity<SmoothStreamingMediaDTO> updateSmoothStreamingMedia(@Valid @RequestBody SmoothStreamingMediaDTO smoothStreamingMediaDTO) throws URISyntaxException {
        log.debug("REST request to update SmoothStreamingMedia : {}", smoothStreamingMediaDTO);
        if (smoothStreamingMediaDTO.getId() == null) {
            return createSmoothStreamingMedia(smoothStreamingMediaDTO);
        }
        SmoothStreamingMediaDTO result = smoothStreamingMediaService.save(smoothStreamingMediaDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("smoothStreamingMedia", smoothStreamingMediaDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /smooth-streaming-medias : get all the smoothStreamingMedias.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of smoothStreamingMedias in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @GetMapping("/smooth-streaming-medias")
    @Timed
    public ResponseEntity<List<SmoothStreamingMediaDTO>> getAllSmoothStreamingMedias(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of SmoothStreamingMedias");
        Page<SmoothStreamingMediaDTO> page = smoothStreamingMediaService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/smooth-streaming-medias");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /smooth-streaming-medias/:id : get the "id" smoothStreamingMedia.
     *
     * @param id the id of the smoothStreamingMediaDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the smoothStreamingMediaDTO, or with status 404 (Not Found)
     */
    @GetMapping("/smooth-streaming-medias/{id}")
    @Timed
    public ResponseEntity<SmoothStreamingMediaDTO> getSmoothStreamingMedia(@PathVariable Long id) {
        log.debug("REST request to get SmoothStreamingMedia : {}", id);
        SmoothStreamingMediaDTO smoothStreamingMediaDTO = smoothStreamingMediaService.findOne(id);
        return Optional.ofNullable(smoothStreamingMediaDTO)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /smooth-streaming-medias/:id : delete the "id" smoothStreamingMedia.
     *
     * @param id the id of the smoothStreamingMediaDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/smooth-streaming-medias/{id}")
    @Timed
    public ResponseEntity<Void> deleteSmoothStreamingMedia(@PathVariable Long id) {
        log.debug("REST request to delete SmoothStreamingMedia : {}", id);
        smoothStreamingMediaService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("smoothStreamingMedia", id.toString())).build();
    }

    /**
     * SEARCH  /_search/smooth-streaming-medias?query=:query : search for the smoothStreamingMedia corresponding
     * to the query.
     *
     * @param query the query of the smoothStreamingMedia search 
     * @param pageable the pagination information
     * @return the result of the search
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @GetMapping("/_search/smooth-streaming-medias")
    @Timed
    public ResponseEntity<List<SmoothStreamingMediaDTO>> searchSmoothStreamingMedias(@RequestParam String query, Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to search for a page of SmoothStreamingMedias for query {}", query);
        Page<SmoothStreamingMediaDTO> page = smoothStreamingMediaService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/smooth-streaming-medias");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
