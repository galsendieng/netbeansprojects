package com.orange.myabstract.service.impl;

import com.orange.myabstract.service.ChannelsService;
import com.orange.myabstract.domain.Channels;
import com.orange.myabstract.repository.ChannelsRepository;
import com.orange.myabstract.repository.search.ChannelsSearchRepository;
import com.orange.myabstract.service.dto.ChannelsDTO;
import com.orange.myabstract.service.mapper.ChannelsMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing Channels.
 */
@Service
@Transactional
public class ChannelsServiceImpl implements ChannelsService{

    private final Logger log = LoggerFactory.getLogger(ChannelsServiceImpl.class);
    
    @Inject
    private ChannelsRepository channelsRepository;

    @Inject
    private ChannelsMapper channelsMapper;

    @Inject
    private ChannelsSearchRepository channelsSearchRepository;

    /**
     * Save a channels.
     *
     * @param channelsDTO the entity to save
     * @return the persisted entity
     */
    public ChannelsDTO save(ChannelsDTO channelsDTO) {
        log.debug("Request to save Channels : {}", channelsDTO);
        Channels channels = channelsMapper.channelsDTOToChannels(channelsDTO);
        channels = channelsRepository.save(channels);
        ChannelsDTO result = channelsMapper.channelsToChannelsDTO(channels);
        channelsSearchRepository.save(channels);
        return result;
    }

    /**
     *  Get all the channels.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true) 
    public Page<ChannelsDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Channels");
        Page<Channels> result = channelsRepository.findAll(pageable);
        return result.map(channels -> channelsMapper.channelsToChannelsDTO(channels));
    }

    /**
     *  Get one channels by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true) 
    public ChannelsDTO findOne(Long id) {
        log.debug("Request to get Channels : {}", id);
        Channels channels = channelsRepository.findOneWithEagerRelationships(id);
        ChannelsDTO channelsDTO = channelsMapper.channelsToChannelsDTO(channels);
        return channelsDTO;
    }

    /**
     *  Delete the  channels by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Channels : {}", id);
        channelsRepository.delete(id);
        channelsSearchRepository.delete(id);
    }

    /**
     * Search for the channels corresponding to the query.
     *
     *  @param query the query of the search
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ChannelsDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Channels for query {}", query);
        Page<Channels> result = channelsSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(channels -> channelsMapper.channelsToChannelsDTO(channels));
    }
}
