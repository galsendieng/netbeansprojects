package com.orange.myabstract.service;

import com.orange.myabstract.service.dto.PodDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.LinkedList;
import java.util.List;

/**
 * Service Interface for managing Pod.
 */
public interface PodService {

    /**
     * Save a pod.
     *
     * @param podDTO the entity to save
     * @return the persisted entity
     */
    PodDTO save(PodDTO podDTO);

    /**
     *  Get all the pods.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<PodDTO> findAll(Pageable pageable);

    /**
     *  Get the "id" pod.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    PodDTO findOne(Long id);

    /**
     *  Delete the "id" pod.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Search for the pod corresponding to the query.
     *
     *  @param query the query of the search
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<PodDTO> search(String query, Pageable pageable);
}
