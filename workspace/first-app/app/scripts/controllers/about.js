'use strict';

/**
 * @ngdoc function
 * @name firstAppApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the firstAppApp
 */
angular.module('firstAppApp')
  .controller('AboutCtrl', function ($scope) {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
    //
     $scope.helloWorld = "Hello, World!"
  });
