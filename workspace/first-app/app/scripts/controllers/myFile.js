var app = angular.module("firstAppApp", []);

app.controller("headerCtrl", function($scope){
    //...
});

app.controller("footerCtrl", function($scope){
    //...
});

app.controller("menuCtrl", function($scope){
    //...
});

app.controller("contentCtrl", function($scope){
    //...
});
