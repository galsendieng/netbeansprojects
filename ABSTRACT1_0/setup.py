from setuptools import setup, find_packages

setup (
       name='ABSTRACT1_0',
       version='0.1',
       packages=find_packages(),

       # Declare your packages' dependencies here, for eg:
       install_requires=['foo>=3'],

       # Fill in these to make your Egg ready for upload to
       # PyPI
       author='Adama DIENG',
       author_email='adama.dieng@orange.com',

       #summary = 'Just another Python package for the cheese shop',
       url='http://localhost:8085',
       license='orange',
       long_description='Script Python ABSTRACT',
       executables = [Executable("abstract.py")]
       # could also include long_description, download_url, classifiers, etc.

  
       )