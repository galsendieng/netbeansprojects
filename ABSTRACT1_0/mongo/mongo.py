#!/usr/bin/env python
# -*- coding:utf-8 -*-
import pymongo
from pymongo import MongoClient
import os
import datetime
import json
import xmltodict
import dicttoxml
from xml.dom.minidom import parseString

class MyMongoPy:
    
    def __init__(self, host = "localhost", client="root", password="passer", port = 27017):
        #MongoDB starting : pid=4067 port=27017 dbpath=/data/db/ 64-bit host=pc-79
        self.host = host
        self.client = client
        self.password = password
        self.port = port
        self.db = ""
    
    def stopMongo(self):
        try:
            os.system('/etc/init.d/mongodb stop')
        except Exception as x:
            print(x)
        
    
    def startMongo(self):
        try:
            os.system('/etc/init.d/mongodb start')
        except Exception as x:
            print(x)
    
    def statusMongo(self):
        try:
            os.system('sudo /etc/init.d/mongodb status')
        except Exception as x:
            print(x)
            
    def connexion(self):
        try:
            print("Connexion MongoDB of " + self.host + "...")
            self.client = MongoClient(str(self.host), int(self.port))
            self.db = self.client.DIENGSALA
            print (self.db)
        except Exception as x:
            print (x)
            
    
    def insertion(self):
        try:
            print("Insertion in MongoDB of " + self.host + "...")
            self.db.test.insert({"nom":"jay gould", "prenom":"stephen", "dated":datetime.datetime.utcnow()})
            
        except Exception as x:
            print (x)
            
     
    def convert(self,xmlfile,destfile, xml_attribs=True):
        with open(str(xmlfile), "rb") as f:    # notice the "rb" mode
            d = xmltodict.parse(f, xml_attribs=xml_attribs)
            #f= json.dumps(d, indent=4)
            fh= open('/home/adama/manifests/'+str(destfile), 'w')
            fh.writelines(json.dumps(d, indent=4)) 
            fh.close() 
                
    def converttoxml(self,file):
        print("-------JSON t XML ---------")
        obj = json.loads(file)
        print(obj)
        xml = dicttoxml.dicttoxml(obj)
        dom = parseString(xml)
        print(dom.toprettyxml())
        #print (xml)

    def update(self):
        try:
            print("Updating in MongoDB of " + self.host + "...")
            
        except Exception as x:
            print (x)
    
    
    def delete(self):
        try:
            print("Deleting in MongoDB of " + self.host + "...")
            
        except Exception as x:
            print (x)
    

mongo = MyMongoPy()
#mongo.statusMongo()
mongo.convert("/home/adama/NetBeansProjects/ABSTRACT1_0/abstract1_0/configs/dash.xml", "dash.json")
mongo.convert("/home/adama/NetBeansProjects/ABSTRACT1_0/abstract1_0/configs/configurations.xml", "config.json")
mongo.convert("/home/adama/NetBeansProjects/ABSTRACT1_0/abstract1_0/configs/mss.xml", "mss.json")

#mongo.converttoxml(file)
#mongo.connexion()