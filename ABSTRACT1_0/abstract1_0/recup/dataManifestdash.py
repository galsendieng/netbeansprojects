#!/usr/bin/python
#-*- coding: utf-8 -*-

from lxml import etree                                                                                
import lxml             
import sys, os  
import xml.etree.ElementTree as ET

class DataManifestDASH:

    def __init__(self, manifest="../profils/static/DASH/ManifestDASH.xml", schema = "../profils/static/DASH/MPD.xsd"):
        self.file = manifest
        self.tree = ET.parse('ManifestDASH.xml')
        self.schema = schema
        self.tree = etree.parse(manifest)
        self.typeMedia = ""
        self.StreamIndex = {}
        self.QualityLevel = {}
    
    def validSchema(self):                                                        
        with open(self.schema) as f:
            valid = False                                             
            self.tree = etree.parse(f)                                    
        try:                                                                        
            self.schema = etree.XMLSchema(self.tree)
            valid = True  
            print ("Schema is valid")                                          
        except lxml.etree.XMLSchemaParseError as e:
            valid = False
            print ("Schema incorrect!")   
            print (e)                           
        return valid
    
    def validManifest(self):
        if self.validSchema():
            with open(self.file) as f:
                valid = False                                                
                self.tree = etree.parse(f)                                                    
            try:                                                                        
                self.schema.assertValid(self.tree)
                valid = True
                print ("Manifest is valid")
                #return self.file
                                                             
            except lxml.etree.DocumentInvalid as e:    
                valid = False  
                print ("Manifest incorrect!")
                print (e)                                                                   
            return valid
        else:
            return False
    
    def getMPDAttributes(self):
        print ("Arributs MPD...")
        self.root = self.tree.getroot()
        print (self.root.attrib)
        return self.root.attrib
    
    def getAdaptationSetAttributes(self):
        print ("Arributs AdaptationSet...")
        self.tree = etree.parse("ManifestDASH.xml")
        for dash in self.tree.xpath("/MPD/Period/AdaptationSet"):
            print dash.attrib
        
        
testdash = DataManifestDASH("ManifestDASH.xml")
print(testdash)
#dash.validSchema()
#dash.validManifest()
#dic = dash.getAttribMPD() 
testdash.getMPDAttributes()
testdash.getAdaptationSetAttributes()
