--- abstract.py	(original)
+++ abstract.py	(refactored)
@@ -75,22 +75,22 @@
                     Type = manifest.getStreamIndexAttribute("video","Type")
                     print (Type)
                     Name = manifest.getStreamIndexAttribute("video","Name")
-                    print Name
+                    print(Name)
                     NbrChunks = manifest.getStreamIndexAttribute("video","Chunks")
                     #print Chunks
                     QualityLevels = manifest.getStreamIndexAttribute("video","QualityLevels")
-                    print QualityLevels
+                    print(QualityLevels)
                     MaxWidth = manifest.getStreamIndexAttribute("video","MaxWidth")
-                    print MaxWidth
+                    print(MaxWidth)
                     MaxHeight = manifest.getStreamIndexAttribute("video","MaxHeight")
-                    print MaxHeight
+                    print(MaxHeight)
                     DisplayWidth = manifest.getStreamIndexAttribute("video","DisplayWidth")
-                    print DisplayWidth
+                    print(DisplayWidth)
                     DisplayHeight = manifest.getStreamIndexAttribute("video","DisplayHeight")
-                    print DisplayHeight
+                    print(DisplayHeight)
                     Url = manifest.getStreamIndexAttribute("video","Url")
-                    print Url
-                    print "-----QualityLevel Video-----"
+                    print(Url)
+                    print("-----QualityLevel Video-----")
                     
                     IndexVideo= manifest.getQualityLevelAttribute("video","Index")
                     BitrateVideo= manifest.getQualityLevelAttribute("video","Bitrate")
@@ -103,25 +103,25 @@
                     #manifest.viewList(liste)
                     #print ":".join(liste)
                     
-                    print "----StreamIndex Audio---"
+                    print("----StreamIndex Audio---")
                     TypeAudio = manifest.getStreamIndexAttribute("audio","Type")
-                    print TypeAudio
+                    print(TypeAudio)
                     IndexAudio = manifest.getStreamIndexAttribute("audio","Index")
-                    print IndexAudio
+                    print(IndexAudio)
                     LanguageAudio = manifest.getStreamIndexAttribute("audio","Language")
-                    print LanguageAudio
+                    print(LanguageAudio)
                     NameAudio = manifest.getStreamIndexAttribute("audio","Name")
-                    print "Name Audio "+ NameAudio
+                    print("Name Audio "+ NameAudio)
                     NbrChunksAudio = manifest.getStreamIndexAttribute("audio","Chunks")
