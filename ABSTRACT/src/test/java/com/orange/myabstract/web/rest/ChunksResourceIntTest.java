package com.orange.myabstract.web.rest;

import com.orange.myabstract.AbstractApp;

import com.orange.myabstract.domain.Chunks;
import com.orange.myabstract.repository.ChunksRepository;
import com.orange.myabstract.service.ChunksService;
import com.orange.myabstract.repository.search.ChunksSearchRepository;
import com.orange.myabstract.service.dto.ChunksDTO;
import com.orange.myabstract.service.mapper.ChunksMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ChunksResource REST controller.
 *
 * @see ChunksResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = AbstractApp.class)
public class ChunksResourceIntTest {

    private static final String DEFAULT_TYPE_STREAM = "AAAAAAAAAA";
    private static final String UPDATED_TYPE_STREAM = "BBBBBBBBBB";

    private static final String DEFAULT_STREAMER = "AAAAAAAAAA";
    private static final String UPDATED_STREAMER = "BBBBBBBBBB";

    private static final String DEFAULT_VALEUR = "AAAAAAAAAA";
    private static final String UPDATED_VALEUR = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_DATE_SUP = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_DATE_SUP = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final String DEFAULT_DATE_SUP_STR = DateTimeFormatter.ISO_INSTANT.format(DEFAULT_DATE_SUP);

    @Inject
    private ChunksRepository chunksRepository;

    @Inject
    private ChunksMapper chunksMapper;

    @Inject
    private ChunksService chunksService;

    @Inject
    private ChunksSearchRepository chunksSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Inject
    private EntityManager em;

    private MockMvc restChunksMockMvc;

    private Chunks chunks;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        ChunksResource chunksResource = new ChunksResource();
        ReflectionTestUtils.setField(chunksResource, "chunksService", chunksService);
        this.restChunksMockMvc = MockMvcBuilders.standaloneSetup(chunksResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Chunks createEntity(EntityManager em) {
        Chunks chunks = new Chunks()
                .typeStream(DEFAULT_TYPE_STREAM)
                .streamer(DEFAULT_STREAMER)
                .valeur(DEFAULT_VALEUR)
                .dateSup(DEFAULT_DATE_SUP);
        return chunks;
    }

    @Before
    public void initTest() {
        chunksSearchRepository.deleteAll();
        chunks = createEntity(em);
    }

    @Test
    @Transactional
    public void createChunks() throws Exception {
        int databaseSizeBeforeCreate = chunksRepository.findAll().size();

        // Create the Chunks
        ChunksDTO chunksDTO = chunksMapper.chunksToChunksDTO(chunks);

        restChunksMockMvc.perform(post("/api/chunks")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(chunksDTO)))
                .andExpect(status().isCreated());

        // Validate the Chunks in the database
        List<Chunks> chunks = chunksRepository.findAll();
        assertThat(chunks).hasSize(databaseSizeBeforeCreate + 1);
        Chunks testChunks = chunks.get(chunks.size() - 1);
        assertThat(testChunks.getTypeStream()).isEqualTo(DEFAULT_TYPE_STREAM);
        assertThat(testChunks.getStreamer()).isEqualTo(DEFAULT_STREAMER);
        assertThat(testChunks.getValeur()).isEqualTo(DEFAULT_VALEUR);
        assertThat(testChunks.getDateSup()).isEqualTo(DEFAULT_DATE_SUP);

        // Validate the Chunks in ElasticSearch
        Chunks chunksEs = chunksSearchRepository.findOne(testChunks.getId());
        assertThat(chunksEs).isEqualToComparingFieldByField(testChunks);
    }

    @Test
    @Transactional
    public void getAllChunks() throws Exception {
        // Initialize the database
        chunksRepository.saveAndFlush(chunks);

        // Get all the chunks
        restChunksMockMvc.perform(get("/api/chunks?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.[*].id").value(hasItem(chunks.getId().intValue())))
                .andExpect(jsonPath("$.[*].typeStream").value(hasItem(DEFAULT_TYPE_STREAM.toString())))
                .andExpect(jsonPath("$.[*].streamer").value(hasItem(DEFAULT_STREAMER.toString())))
                .andExpect(jsonPath("$.[*].valeur").value(hasItem(DEFAULT_VALEUR.toString())))
                .andExpect(jsonPath("$.[*].dateSup").value(hasItem(DEFAULT_DATE_SUP_STR)));
    }

    @Test
    @Transactional
    public void getChunks() throws Exception {
        // Initialize the database
        chunksRepository.saveAndFlush(chunks);

        // Get the chunks
        restChunksMockMvc.perform(get("/api/chunks/{id}", chunks.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(chunks.getId().intValue()))
            .andExpect(jsonPath("$.typeStream").value(DEFAULT_TYPE_STREAM.toString()))
            .andExpect(jsonPath("$.streamer").value(DEFAULT_STREAMER.toString()))
            .andExpect(jsonPath("$.valeur").value(DEFAULT_VALEUR.toString()))
            .andExpect(jsonPath("$.dateSup").value(DEFAULT_DATE_SUP_STR));
    }

    @Test
    @Transactional
    public void getNonExistingChunks() throws Exception {
        // Get the chunks
        restChunksMockMvc.perform(get("/api/chunks/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateChunks() throws Exception {
        // Initialize the database
        chunksRepository.saveAndFlush(chunks);
        chunksSearchRepository.save(chunks);
        int databaseSizeBeforeUpdate = chunksRepository.findAll().size();

        // Update the chunks
        Chunks updatedChunks = chunksRepository.findOne(chunks.getId());
        updatedChunks
                .typeStream(UPDATED_TYPE_STREAM)
                .streamer(UPDATED_STREAMER)
                .valeur(UPDATED_VALEUR)
                .dateSup(UPDATED_DATE_SUP);
        ChunksDTO chunksDTO = chunksMapper.chunksToChunksDTO(updatedChunks);

        restChunksMockMvc.perform(put("/api/chunks")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(chunksDTO)))
                .andExpect(status().isOk());

        // Validate the Chunks in the database
        List<Chunks> chunks = chunksRepository.findAll();
        assertThat(chunks).hasSize(databaseSizeBeforeUpdate);
        Chunks testChunks = chunks.get(chunks.size() - 1);
        assertThat(testChunks.getTypeStream()).isEqualTo(UPDATED_TYPE_STREAM);
        assertThat(testChunks.getStreamer()).isEqualTo(UPDATED_STREAMER);
        assertThat(testChunks.getValeur()).isEqualTo(UPDATED_VALEUR);
        assertThat(testChunks.getDateSup()).isEqualTo(UPDATED_DATE_SUP);

        // Validate the Chunks in ElasticSearch
        Chunks chunksEs = chunksSearchRepository.findOne(testChunks.getId());
        assertThat(chunksEs).isEqualToComparingFieldByField(testChunks);
    }

    @Test
    @Transactional
    public void deleteChunks() throws Exception {
        // Initialize the database
        chunksRepository.saveAndFlush(chunks);
        chunksSearchRepository.save(chunks);
        int databaseSizeBeforeDelete = chunksRepository.findAll().size();

        // Get the chunks
        restChunksMockMvc.perform(delete("/api/chunks/{id}", chunks.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate ElasticSearch is empty
        boolean chunksExistsInEs = chunksSearchRepository.exists(chunks.getId());
        assertThat(chunksExistsInEs).isFalse();

        // Validate the database is empty
        List<Chunks> chunks = chunksRepository.findAll();
        assertThat(chunks).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchChunks() throws Exception {
        // Initialize the database
        chunksRepository.saveAndFlush(chunks);
        chunksSearchRepository.save(chunks);

        // Search the chunks
        restChunksMockMvc.perform(get("/api/_search/chunks?query=id:" + chunks.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(chunks.getId().intValue())))
            .andExpect(jsonPath("$.[*].typeStream").value(hasItem(DEFAULT_TYPE_STREAM.toString())))
            .andExpect(jsonPath("$.[*].streamer").value(hasItem(DEFAULT_STREAMER.toString())))
            .andExpect(jsonPath("$.[*].valeur").value(hasItem(DEFAULT_VALEUR.toString())))
            .andExpect(jsonPath("$.[*].dateSup").value(hasItem(DEFAULT_DATE_SUP_STR)));
    }
}
