package com.orange.myabstract.web.rest;

import com.orange.myabstract.AbstractApp;

import com.orange.myabstract.domain.SamplingRate;
import com.orange.myabstract.repository.SamplingRateRepository;
import com.orange.myabstract.service.SamplingRateService;
import com.orange.myabstract.repository.search.SamplingRateSearchRepository;
import com.orange.myabstract.service.dto.SamplingRateDTO;
import com.orange.myabstract.service.mapper.SamplingRateMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the SamplingRateResource REST controller.
 *
 * @see SamplingRateResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = AbstractApp.class)
public class SamplingRateResourceIntTest {

    private static final String DEFAULT_TYPE_STREAM = "AAAAAAAAAA";
    private static final String UPDATED_TYPE_STREAM = "BBBBBBBBBB";

    private static final String DEFAULT_VALEUR = "AAAAAAAAAA";
    private static final String UPDATED_VALEUR = "BBBBBBBBBB";

    private static final String DEFAULT_STREAMER = "AAAAAAAAAA";
    private static final String UPDATED_STREAMER = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_DATE_SUP = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_DATE_SUP = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final String DEFAULT_DATE_SUP_STR = DateTimeFormatter.ISO_INSTANT.format(DEFAULT_DATE_SUP);

    @Inject
    private SamplingRateRepository samplingRateRepository;

    @Inject
    private SamplingRateMapper samplingRateMapper;

    @Inject
    private SamplingRateService samplingRateService;

    @Inject
    private SamplingRateSearchRepository samplingRateSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Inject
    private EntityManager em;

    private MockMvc restSamplingRateMockMvc;

    private SamplingRate samplingRate;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        SamplingRateResource samplingRateResource = new SamplingRateResource();
        ReflectionTestUtils.setField(samplingRateResource, "samplingRateService", samplingRateService);
        this.restSamplingRateMockMvc = MockMvcBuilders.standaloneSetup(samplingRateResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static SamplingRate createEntity(EntityManager em) {
        SamplingRate samplingRate = new SamplingRate()
                .typeStream(DEFAULT_TYPE_STREAM)
                .valeur(DEFAULT_VALEUR)
                .streamer(DEFAULT_STREAMER)
                .dateSup(DEFAULT_DATE_SUP);
        return samplingRate;
    }

    @Before
    public void initTest() {
        samplingRateSearchRepository.deleteAll();
        samplingRate = createEntity(em);
    }

    @Test
    @Transactional
    public void createSamplingRate() throws Exception {
        int databaseSizeBeforeCreate = samplingRateRepository.findAll().size();

        // Create the SamplingRate
        SamplingRateDTO samplingRateDTO = samplingRateMapper.samplingRateToSamplingRateDTO(samplingRate);

        restSamplingRateMockMvc.perform(post("/api/sampling-rates")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(samplingRateDTO)))
                .andExpect(status().isCreated());

        // Validate the SamplingRate in the database
        List<SamplingRate> samplingRates = samplingRateRepository.findAll();
        assertThat(samplingRates).hasSize(databaseSizeBeforeCreate + 1);
        SamplingRate testSamplingRate = samplingRates.get(samplingRates.size() - 1);
        assertThat(testSamplingRate.getTypeStream()).isEqualTo(DEFAULT_TYPE_STREAM);
        assertThat(testSamplingRate.getValeur()).isEqualTo(DEFAULT_VALEUR);
        assertThat(testSamplingRate.getStreamer()).isEqualTo(DEFAULT_STREAMER);
        assertThat(testSamplingRate.getDateSup()).isEqualTo(DEFAULT_DATE_SUP);

        // Validate the SamplingRate in ElasticSearch
        SamplingRate samplingRateEs = samplingRateSearchRepository.findOne(testSamplingRate.getId());
        assertThat(samplingRateEs).isEqualToComparingFieldByField(testSamplingRate);
    }

    @Test
    @Transactional
    public void getAllSamplingRates() throws Exception {
        // Initialize the database
        samplingRateRepository.saveAndFlush(samplingRate);

        // Get all the samplingRates
        restSamplingRateMockMvc.perform(get("/api/sampling-rates?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.[*].id").value(hasItem(samplingRate.getId().intValue())))
                .andExpect(jsonPath("$.[*].typeStream").value(hasItem(DEFAULT_TYPE_STREAM.toString())))
                .andExpect(jsonPath("$.[*].valeur").value(hasItem(DEFAULT_VALEUR.toString())))
                .andExpect(jsonPath("$.[*].streamer").value(hasItem(DEFAULT_STREAMER.toString())))
                .andExpect(jsonPath("$.[*].dateSup").value(hasItem(DEFAULT_DATE_SUP_STR)));
    }

    @Test
    @Transactional
    public void getSamplingRate() throws Exception {
        // Initialize the database
        samplingRateRepository.saveAndFlush(samplingRate);

        // Get the samplingRate
        restSamplingRateMockMvc.perform(get("/api/sampling-rates/{id}", samplingRate.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(samplingRate.getId().intValue()))
            .andExpect(jsonPath("$.typeStream").value(DEFAULT_TYPE_STREAM.toString()))
            .andExpect(jsonPath("$.valeur").value(DEFAULT_VALEUR.toString()))
            .andExpect(jsonPath("$.streamer").value(DEFAULT_STREAMER.toString()))
            .andExpect(jsonPath("$.dateSup").value(DEFAULT_DATE_SUP_STR));
    }

    @Test
    @Transactional
    public void getNonExistingSamplingRate() throws Exception {
        // Get the samplingRate
        restSamplingRateMockMvc.perform(get("/api/sampling-rates/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSamplingRate() throws Exception {
        // Initialize the database
        samplingRateRepository.saveAndFlush(samplingRate);
        samplingRateSearchRepository.save(samplingRate);
        int databaseSizeBeforeUpdate = samplingRateRepository.findAll().size();

        // Update the samplingRate
        SamplingRate updatedSamplingRate = samplingRateRepository.findOne(samplingRate.getId());
        updatedSamplingRate
                .typeStream(UPDATED_TYPE_STREAM)
                .valeur(UPDATED_VALEUR)
                .streamer(UPDATED_STREAMER)
                .dateSup(UPDATED_DATE_SUP);
        SamplingRateDTO samplingRateDTO = samplingRateMapper.samplingRateToSamplingRateDTO(updatedSamplingRate);

        restSamplingRateMockMvc.perform(put("/api/sampling-rates")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(samplingRateDTO)))
                .andExpect(status().isOk());

        // Validate the SamplingRate in the database
        List<SamplingRate> samplingRates = samplingRateRepository.findAll();
        assertThat(samplingRates).hasSize(databaseSizeBeforeUpdate);
        SamplingRate testSamplingRate = samplingRates.get(samplingRates.size() - 1);
        assertThat(testSamplingRate.getTypeStream()).isEqualTo(UPDATED_TYPE_STREAM);
        assertThat(testSamplingRate.getValeur()).isEqualTo(UPDATED_VALEUR);
        assertThat(testSamplingRate.getStreamer()).isEqualTo(UPDATED_STREAMER);
        assertThat(testSamplingRate.getDateSup()).isEqualTo(UPDATED_DATE_SUP);

        // Validate the SamplingRate in ElasticSearch
        SamplingRate samplingRateEs = samplingRateSearchRepository.findOne(testSamplingRate.getId());
        assertThat(samplingRateEs).isEqualToComparingFieldByField(testSamplingRate);
    }

    @Test
    @Transactional
    public void deleteSamplingRate() throws Exception {
        // Initialize the database
        samplingRateRepository.saveAndFlush(samplingRate);
        samplingRateSearchRepository.save(samplingRate);
        int databaseSizeBeforeDelete = samplingRateRepository.findAll().size();

        // Get the samplingRate
        restSamplingRateMockMvc.perform(delete("/api/sampling-rates/{id}", samplingRate.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate ElasticSearch is empty
        boolean samplingRateExistsInEs = samplingRateSearchRepository.exists(samplingRate.getId());
        assertThat(samplingRateExistsInEs).isFalse();

        // Validate the database is empty
        List<SamplingRate> samplingRates = samplingRateRepository.findAll();
        assertThat(samplingRates).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchSamplingRate() throws Exception {
        // Initialize the database
        samplingRateRepository.saveAndFlush(samplingRate);
        samplingRateSearchRepository.save(samplingRate);

        // Search the samplingRate
        restSamplingRateMockMvc.perform(get("/api/_search/sampling-rates?query=id:" + samplingRate.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(samplingRate.getId().intValue())))
            .andExpect(jsonPath("$.[*].typeStream").value(hasItem(DEFAULT_TYPE_STREAM.toString())))
            .andExpect(jsonPath("$.[*].valeur").value(hasItem(DEFAULT_VALEUR.toString())))
            .andExpect(jsonPath("$.[*].streamer").value(hasItem(DEFAULT_STREAMER.toString())))
            .andExpect(jsonPath("$.[*].dateSup").value(hasItem(DEFAULT_DATE_SUP_STR)));
    }
}
