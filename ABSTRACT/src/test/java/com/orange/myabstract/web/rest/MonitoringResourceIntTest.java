package com.orange.myabstract.web.rest;

import com.orange.myabstract.AbstractApp;

import com.orange.myabstract.domain.Monitoring;
import com.orange.myabstract.repository.MonitoringRepository;
import com.orange.myabstract.service.MonitoringService;
import com.orange.myabstract.repository.search.MonitoringSearchRepository;
import com.orange.myabstract.service.dto.MonitoringDTO;
import com.orange.myabstract.service.mapper.MonitoringMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the MonitoringResource REST controller.
 *
 * @see MonitoringResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = AbstractApp.class)
public class MonitoringResourceIntTest {

    private static final String DEFAULT_ATTRIBUT = "AAAAAAAAAA";
    private static final String UPDATED_ATTRIBUT = "BBBBBBBBBB";

    private static final String DEFAULT_SEVERITE = "AAAAAAAAAA";
    private static final String UPDATED_SEVERITE = "BBBBBBBBBB";

    private static final String DEFAULT_IMPORTANCE = "AAAAAAAAAA";
    private static final String UPDATED_IMPORTANCE = "BBBBBBBBBB";

    private static final String DEFAULT_VALEUR_ATTENDUE = "AAAAAAAAAA";
    private static final String UPDATED_VALEUR_ATTENDUE = "BBBBBBBBBB";

    private static final String DEFAULT_VALEUR_OBTENUE = "AAAAAAAAAA";
    private static final String UPDATED_VALEUR_OBTENUE = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_DATE_SUP = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_DATE_SUP = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final String DEFAULT_DATE_SUP_STR = DateTimeFormatter.ISO_INSTANT.format(DEFAULT_DATE_SUP);

    private static final String DEFAULT_MESSAGE = "AAAAAAAAAA";
    private static final String UPDATED_MESSAGE = "BBBBBBBBBB";

    private static final String DEFAULT_INFOS = "AAAAAAAAAA";
    private static final String UPDATED_INFOS = "BBBBBBBBBB";

    @Inject
    private MonitoringRepository monitoringRepository;

    @Inject
    private MonitoringMapper monitoringMapper;

    @Inject
    private MonitoringService monitoringService;

    @Inject
    private MonitoringSearchRepository monitoringSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Inject
    private EntityManager em;

    private MockMvc restMonitoringMockMvc;

    private Monitoring monitoring;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        MonitoringResource monitoringResource = new MonitoringResource();
        ReflectionTestUtils.setField(monitoringResource, "monitoringService", monitoringService);
        this.restMonitoringMockMvc = MockMvcBuilders.standaloneSetup(monitoringResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Monitoring createEntity(EntityManager em) {
        Monitoring monitoring = new Monitoring()
                .attribut(DEFAULT_ATTRIBUT)
                .severite(DEFAULT_SEVERITE)
                .importance(DEFAULT_IMPORTANCE)
                .valeurAttendue(DEFAULT_VALEUR_ATTENDUE)
                .valeurObtenue(DEFAULT_VALEUR_OBTENUE)
                .dateSup(DEFAULT_DATE_SUP)
                .message(DEFAULT_MESSAGE)
                .infos(DEFAULT_INFOS);
        return monitoring;
    }

    @Before
    public void initTest() {
        monitoringSearchRepository.deleteAll();
        monitoring = createEntity(em);
    }

    @Test
    @Transactional
    public void createMonitoring() throws Exception {
        int databaseSizeBeforeCreate = monitoringRepository.findAll().size();

        // Create the Monitoring
        MonitoringDTO monitoringDTO = monitoringMapper.monitoringToMonitoringDTO(monitoring);

        restMonitoringMockMvc.perform(post("/api/monitorings")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(monitoringDTO)))
                .andExpect(status().isCreated());

        // Validate the Monitoring in the database
        List<Monitoring> monitorings = monitoringRepository.findAll();
        assertThat(monitorings).hasSize(databaseSizeBeforeCreate + 1);
        Monitoring testMonitoring = monitorings.get(monitorings.size() - 1);
        assertThat(testMonitoring.getAttribut()).isEqualTo(DEFAULT_ATTRIBUT);
        assertThat(testMonitoring.getSeverite()).isEqualTo(DEFAULT_SEVERITE);
        assertThat(testMonitoring.getImportance()).isEqualTo(DEFAULT_IMPORTANCE);
        assertThat(testMonitoring.getValeurAttendue()).isEqualTo(DEFAULT_VALEUR_ATTENDUE);
        assertThat(testMonitoring.getValeurObtenue()).isEqualTo(DEFAULT_VALEUR_OBTENUE);
        assertThat(testMonitoring.getDateSup()).isEqualTo(DEFAULT_DATE_SUP);
        assertThat(testMonitoring.getMessage()).isEqualTo(DEFAULT_MESSAGE);
        assertThat(testMonitoring.getInfos()).isEqualTo(DEFAULT_INFOS);

        // Validate the Monitoring in ElasticSearch
        Monitoring monitoringEs = monitoringSearchRepository.findOne(testMonitoring.getId());
        assertThat(monitoringEs).isEqualToComparingFieldByField(testMonitoring);
    }

    @Test
    @Transactional
    public void getAllMonitorings() throws Exception {
        // Initialize the database
        monitoringRepository.saveAndFlush(monitoring);

        // Get all the monitorings
        restMonitoringMockMvc.perform(get("/api/monitorings?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.[*].id").value(hasItem(monitoring.getId().intValue())))
                .andExpect(jsonPath("$.[*].attribut").value(hasItem(DEFAULT_ATTRIBUT.toString())))
                .andExpect(jsonPath("$.[*].severite").value(hasItem(DEFAULT_SEVERITE.toString())))
                .andExpect(jsonPath("$.[*].importance").value(hasItem(DEFAULT_IMPORTANCE.toString())))
                .andExpect(jsonPath("$.[*].valeurAttendue").value(hasItem(DEFAULT_VALEUR_ATTENDUE.toString())))
                .andExpect(jsonPath("$.[*].valeurObtenue").value(hasItem(DEFAULT_VALEUR_OBTENUE.toString())))
                .andExpect(jsonPath("$.[*].dateSup").value(hasItem(DEFAULT_DATE_SUP_STR)))
                .andExpect(jsonPath("$.[*].message").value(hasItem(DEFAULT_MESSAGE.toString())))
                .andExpect(jsonPath("$.[*].infos").value(hasItem(DEFAULT_INFOS.toString())));
    }

    @Test
    @Transactional
    public void getMonitoring() throws Exception {
        // Initialize the database
        monitoringRepository.saveAndFlush(monitoring);

        // Get the monitoring
        restMonitoringMockMvc.perform(get("/api/monitorings/{id}", monitoring.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(monitoring.getId().intValue()))
            .andExpect(jsonPath("$.attribut").value(DEFAULT_ATTRIBUT.toString()))
            .andExpect(jsonPath("$.severite").value(DEFAULT_SEVERITE.toString()))
            .andExpect(jsonPath("$.importance").value(DEFAULT_IMPORTANCE.toString()))
            .andExpect(jsonPath("$.valeurAttendue").value(DEFAULT_VALEUR_ATTENDUE.toString()))
            .andExpect(jsonPath("$.valeurObtenue").value(DEFAULT_VALEUR_OBTENUE.toString()))
            .andExpect(jsonPath("$.dateSup").value(DEFAULT_DATE_SUP_STR))
            .andExpect(jsonPath("$.message").value(DEFAULT_MESSAGE.toString()))
            .andExpect(jsonPath("$.infos").value(DEFAULT_INFOS.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingMonitoring() throws Exception {
        // Get the monitoring
        restMonitoringMockMvc.perform(get("/api/monitorings/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateMonitoring() throws Exception {
        // Initialize the database
        monitoringRepository.saveAndFlush(monitoring);
        monitoringSearchRepository.save(monitoring);
        int databaseSizeBeforeUpdate = monitoringRepository.findAll().size();

        // Update the monitoring
        Monitoring updatedMonitoring = monitoringRepository.findOne(monitoring.getId());
        updatedMonitoring
                .attribut(UPDATED_ATTRIBUT)
                .severite(UPDATED_SEVERITE)
                .importance(UPDATED_IMPORTANCE)
                .valeurAttendue(UPDATED_VALEUR_ATTENDUE)
                .valeurObtenue(UPDATED_VALEUR_OBTENUE)
                .dateSup(UPDATED_DATE_SUP)
                .message(UPDATED_MESSAGE)
                .infos(UPDATED_INFOS);
        MonitoringDTO monitoringDTO = monitoringMapper.monitoringToMonitoringDTO(updatedMonitoring);

        restMonitoringMockMvc.perform(put("/api/monitorings")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(monitoringDTO)))
                .andExpect(status().isOk());

        // Validate the Monitoring in the database
        List<Monitoring> monitorings = monitoringRepository.findAll();
        assertThat(monitorings).hasSize(databaseSizeBeforeUpdate);
        Monitoring testMonitoring = monitorings.get(monitorings.size() - 1);
        assertThat(testMonitoring.getAttribut()).isEqualTo(UPDATED_ATTRIBUT);
        assertThat(testMonitoring.getSeverite()).isEqualTo(UPDATED_SEVERITE);
        assertThat(testMonitoring.getImportance()).isEqualTo(UPDATED_IMPORTANCE);
        assertThat(testMonitoring.getValeurAttendue()).isEqualTo(UPDATED_VALEUR_ATTENDUE);
        assertThat(testMonitoring.getValeurObtenue()).isEqualTo(UPDATED_VALEUR_OBTENUE);
        assertThat(testMonitoring.getDateSup()).isEqualTo(UPDATED_DATE_SUP);
        assertThat(testMonitoring.getMessage()).isEqualTo(UPDATED_MESSAGE);
        assertThat(testMonitoring.getInfos()).isEqualTo(UPDATED_INFOS);

        // Validate the Monitoring in ElasticSearch
        Monitoring monitoringEs = monitoringSearchRepository.findOne(testMonitoring.getId());
        assertThat(monitoringEs).isEqualToComparingFieldByField(testMonitoring);
    }

    @Test
    @Transactional
    public void deleteMonitoring() throws Exception {
        // Initialize the database
        monitoringRepository.saveAndFlush(monitoring);
        monitoringSearchRepository.save(monitoring);
        int databaseSizeBeforeDelete = monitoringRepository.findAll().size();

        // Get the monitoring
        restMonitoringMockMvc.perform(delete("/api/monitorings/{id}", monitoring.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate ElasticSearch is empty
        boolean monitoringExistsInEs = monitoringSearchRepository.exists(monitoring.getId());
        assertThat(monitoringExistsInEs).isFalse();

        // Validate the database is empty
        List<Monitoring> monitorings = monitoringRepository.findAll();
        assertThat(monitorings).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchMonitoring() throws Exception {
        // Initialize the database
        monitoringRepository.saveAndFlush(monitoring);
        monitoringSearchRepository.save(monitoring);

        // Search the monitoring
        restMonitoringMockMvc.perform(get("/api/_search/monitorings?query=id:" + monitoring.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(monitoring.getId().intValue())))
            .andExpect(jsonPath("$.[*].attribut").value(hasItem(DEFAULT_ATTRIBUT.toString())))
            .andExpect(jsonPath("$.[*].severite").value(hasItem(DEFAULT_SEVERITE.toString())))
            .andExpect(jsonPath("$.[*].importance").value(hasItem(DEFAULT_IMPORTANCE.toString())))
            .andExpect(jsonPath("$.[*].valeurAttendue").value(hasItem(DEFAULT_VALEUR_ATTENDUE.toString())))
            .andExpect(jsonPath("$.[*].valeurObtenue").value(hasItem(DEFAULT_VALEUR_OBTENUE.toString())))
            .andExpect(jsonPath("$.[*].dateSup").value(hasItem(DEFAULT_DATE_SUP_STR)))
            .andExpect(jsonPath("$.[*].message").value(hasItem(DEFAULT_MESSAGE.toString())))
            .andExpect(jsonPath("$.[*].infos").value(hasItem(DEFAULT_INFOS.toString())));
    }
}
