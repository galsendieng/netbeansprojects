package com.orange.myabstract.web.rest;

import com.orange.myabstract.AbstractApp;

import com.orange.myabstract.domain.MaxWidthQlt;
import com.orange.myabstract.repository.MaxWidthQltRepository;
import com.orange.myabstract.service.MaxWidthQltService;
import com.orange.myabstract.repository.search.MaxWidthQltSearchRepository;
import com.orange.myabstract.service.dto.MaxWidthQltDTO;
import com.orange.myabstract.service.mapper.MaxWidthQltMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the MaxWidthQltResource REST controller.
 *
 * @see MaxWidthQltResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = AbstractApp.class)
public class MaxWidthQltResourceIntTest {

    private static final String DEFAULT_TYPE_STREAM = "AAAAAAAAAA";
    private static final String UPDATED_TYPE_STREAM = "BBBBBBBBBB";

    private static final String DEFAULT_VALEUR = "AAAAAAAAAA";
    private static final String UPDATED_VALEUR = "BBBBBBBBBB";

    private static final String DEFAULT_STREAMER = "AAAAAAAAAA";
    private static final String UPDATED_STREAMER = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_DATE_SUP = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_DATE_SUP = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final String DEFAULT_DATE_SUP_STR = DateTimeFormatter.ISO_INSTANT.format(DEFAULT_DATE_SUP);

    @Inject
    private MaxWidthQltRepository maxWidthQltRepository;

    @Inject
    private MaxWidthQltMapper maxWidthQltMapper;

    @Inject
    private MaxWidthQltService maxWidthQltService;

    @Inject
    private MaxWidthQltSearchRepository maxWidthQltSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Inject
    private EntityManager em;

    private MockMvc restMaxWidthQltMockMvc;

    private MaxWidthQlt maxWidthQlt;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        MaxWidthQltResource maxWidthQltResource = new MaxWidthQltResource();
        ReflectionTestUtils.setField(maxWidthQltResource, "maxWidthQltService", maxWidthQltService);
        this.restMaxWidthQltMockMvc = MockMvcBuilders.standaloneSetup(maxWidthQltResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static MaxWidthQlt createEntity(EntityManager em) {
        MaxWidthQlt maxWidthQlt = new MaxWidthQlt()
                .typeStream(DEFAULT_TYPE_STREAM)
                .valeur(DEFAULT_VALEUR)
                .streamer(DEFAULT_STREAMER)
                .dateSup(DEFAULT_DATE_SUP);
        return maxWidthQlt;
    }

    @Before
    public void initTest() {
        maxWidthQltSearchRepository.deleteAll();
        maxWidthQlt = createEntity(em);
    }

    @Test
    @Transactional
    public void createMaxWidthQlt() throws Exception {
        int databaseSizeBeforeCreate = maxWidthQltRepository.findAll().size();

        // Create the MaxWidthQlt
        MaxWidthQltDTO maxWidthQltDTO = maxWidthQltMapper.maxWidthQltToMaxWidthQltDTO(maxWidthQlt);

        restMaxWidthQltMockMvc.perform(post("/api/max-width-qlts")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(maxWidthQltDTO)))
                .andExpect(status().isCreated());

        // Validate the MaxWidthQlt in the database
        List<MaxWidthQlt> maxWidthQlts = maxWidthQltRepository.findAll();
        assertThat(maxWidthQlts).hasSize(databaseSizeBeforeCreate + 1);
        MaxWidthQlt testMaxWidthQlt = maxWidthQlts.get(maxWidthQlts.size() - 1);
        assertThat(testMaxWidthQlt.getTypeStream()).isEqualTo(DEFAULT_TYPE_STREAM);
        assertThat(testMaxWidthQlt.getValeur()).isEqualTo(DEFAULT_VALEUR);
        assertThat(testMaxWidthQlt.getStreamer()).isEqualTo(DEFAULT_STREAMER);
        assertThat(testMaxWidthQlt.getDateSup()).isEqualTo(DEFAULT_DATE_SUP);

        // Validate the MaxWidthQlt in ElasticSearch
        MaxWidthQlt maxWidthQltEs = maxWidthQltSearchRepository.findOne(testMaxWidthQlt.getId());
        assertThat(maxWidthQltEs).isEqualToComparingFieldByField(testMaxWidthQlt);
    }

    @Test
    @Transactional
    public void getAllMaxWidthQlts() throws Exception {
        // Initialize the database
        maxWidthQltRepository.saveAndFlush(maxWidthQlt);

        // Get all the maxWidthQlts
        restMaxWidthQltMockMvc.perform(get("/api/max-width-qlts?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.[*].id").value(hasItem(maxWidthQlt.getId().intValue())))
                .andExpect(jsonPath("$.[*].typeStream").value(hasItem(DEFAULT_TYPE_STREAM.toString())))
                .andExpect(jsonPath("$.[*].valeur").value(hasItem(DEFAULT_VALEUR.toString())))
                .andExpect(jsonPath("$.[*].streamer").value(hasItem(DEFAULT_STREAMER.toString())))
                .andExpect(jsonPath("$.[*].dateSup").value(hasItem(DEFAULT_DATE_SUP_STR)));
    }

    @Test
    @Transactional
    public void getMaxWidthQlt() throws Exception {
        // Initialize the database
        maxWidthQltRepository.saveAndFlush(maxWidthQlt);

        // Get the maxWidthQlt
        restMaxWidthQltMockMvc.perform(get("/api/max-width-qlts/{id}", maxWidthQlt.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(maxWidthQlt.getId().intValue()))
            .andExpect(jsonPath("$.typeStream").value(DEFAULT_TYPE_STREAM.toString()))
            .andExpect(jsonPath("$.valeur").value(DEFAULT_VALEUR.toString()))
            .andExpect(jsonPath("$.streamer").value(DEFAULT_STREAMER.toString()))
            .andExpect(jsonPath("$.dateSup").value(DEFAULT_DATE_SUP_STR));
    }

    @Test
    @Transactional
    public void getNonExistingMaxWidthQlt() throws Exception {
        // Get the maxWidthQlt
        restMaxWidthQltMockMvc.perform(get("/api/max-width-qlts/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateMaxWidthQlt() throws Exception {
        // Initialize the database
        maxWidthQltRepository.saveAndFlush(maxWidthQlt);
        maxWidthQltSearchRepository.save(maxWidthQlt);
        int databaseSizeBeforeUpdate = maxWidthQltRepository.findAll().size();

        // Update the maxWidthQlt
        MaxWidthQlt updatedMaxWidthQlt = maxWidthQltRepository.findOne(maxWidthQlt.getId());
        updatedMaxWidthQlt
                .typeStream(UPDATED_TYPE_STREAM)
                .valeur(UPDATED_VALEUR)
                .streamer(UPDATED_STREAMER)
                .dateSup(UPDATED_DATE_SUP);
        MaxWidthQltDTO maxWidthQltDTO = maxWidthQltMapper.maxWidthQltToMaxWidthQltDTO(updatedMaxWidthQlt);

        restMaxWidthQltMockMvc.perform(put("/api/max-width-qlts")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(maxWidthQltDTO)))
                .andExpect(status().isOk());

        // Validate the MaxWidthQlt in the database
        List<MaxWidthQlt> maxWidthQlts = maxWidthQltRepository.findAll();
        assertThat(maxWidthQlts).hasSize(databaseSizeBeforeUpdate);
        MaxWidthQlt testMaxWidthQlt = maxWidthQlts.get(maxWidthQlts.size() - 1);
        assertThat(testMaxWidthQlt.getTypeStream()).isEqualTo(UPDATED_TYPE_STREAM);
        assertThat(testMaxWidthQlt.getValeur()).isEqualTo(UPDATED_VALEUR);
        assertThat(testMaxWidthQlt.getStreamer()).isEqualTo(UPDATED_STREAMER);
        assertThat(testMaxWidthQlt.getDateSup()).isEqualTo(UPDATED_DATE_SUP);

        // Validate the MaxWidthQlt in ElasticSearch
        MaxWidthQlt maxWidthQltEs = maxWidthQltSearchRepository.findOne(testMaxWidthQlt.getId());
        assertThat(maxWidthQltEs).isEqualToComparingFieldByField(testMaxWidthQlt);
    }

    @Test
    @Transactional
    public void deleteMaxWidthQlt() throws Exception {
        // Initialize the database
        maxWidthQltRepository.saveAndFlush(maxWidthQlt);
        maxWidthQltSearchRepository.save(maxWidthQlt);
        int databaseSizeBeforeDelete = maxWidthQltRepository.findAll().size();

        // Get the maxWidthQlt
        restMaxWidthQltMockMvc.perform(delete("/api/max-width-qlts/{id}", maxWidthQlt.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate ElasticSearch is empty
        boolean maxWidthQltExistsInEs = maxWidthQltSearchRepository.exists(maxWidthQlt.getId());
        assertThat(maxWidthQltExistsInEs).isFalse();

        // Validate the database is empty
        List<MaxWidthQlt> maxWidthQlts = maxWidthQltRepository.findAll();
        assertThat(maxWidthQlts).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchMaxWidthQlt() throws Exception {
        // Initialize the database
        maxWidthQltRepository.saveAndFlush(maxWidthQlt);
        maxWidthQltSearchRepository.save(maxWidthQlt);

        // Search the maxWidthQlt
        restMaxWidthQltMockMvc.perform(get("/api/_search/max-width-qlts?query=id:" + maxWidthQlt.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(maxWidthQlt.getId().intValue())))
            .andExpect(jsonPath("$.[*].typeStream").value(hasItem(DEFAULT_TYPE_STREAM.toString())))
            .andExpect(jsonPath("$.[*].valeur").value(hasItem(DEFAULT_VALEUR.toString())))
            .andExpect(jsonPath("$.[*].streamer").value(hasItem(DEFAULT_STREAMER.toString())))
            .andExpect(jsonPath("$.[*].dateSup").value(hasItem(DEFAULT_DATE_SUP_STR)));
    }
}
