package com.orange.myabstract.web.rest;

import com.orange.myabstract.AbstractApp;

import com.orange.myabstract.domain.FourCC;
import com.orange.myabstract.repository.FourCCRepository;
import com.orange.myabstract.service.FourCCService;
import com.orange.myabstract.repository.search.FourCCSearchRepository;
import com.orange.myabstract.service.dto.FourCCDTO;
import com.orange.myabstract.service.mapper.FourCCMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the FourCCResource REST controller.
 *
 * @see FourCCResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = AbstractApp.class)
public class FourCCResourceIntTest {

    private static final String DEFAULT_TYPE_STREAM = "AAAAAAAAAA";
    private static final String UPDATED_TYPE_STREAM = "BBBBBBBBBB";

    private static final String DEFAULT_VALEUR = "AAAAAAAAAA";
    private static final String UPDATED_VALEUR = "BBBBBBBBBB";

    private static final String DEFAULT_STREAMER = "AAAAAAAAAA";
    private static final String UPDATED_STREAMER = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_DATE_SUP = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_DATE_SUP = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final String DEFAULT_DATE_SUP_STR = DateTimeFormatter.ISO_INSTANT.format(DEFAULT_DATE_SUP);

    @Inject
    private FourCCRepository fourCCRepository;

    @Inject
    private FourCCMapper fourCCMapper;

    @Inject
    private FourCCService fourCCService;

    @Inject
    private FourCCSearchRepository fourCCSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Inject
    private EntityManager em;

    private MockMvc restFourCCMockMvc;

    private FourCC fourCC;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        FourCCResource fourCCResource = new FourCCResource();
        ReflectionTestUtils.setField(fourCCResource, "fourCCService", fourCCService);
        this.restFourCCMockMvc = MockMvcBuilders.standaloneSetup(fourCCResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static FourCC createEntity(EntityManager em) {
        FourCC fourCC = new FourCC()
                .typeStream(DEFAULT_TYPE_STREAM)
                .valeur(DEFAULT_VALEUR)
                .streamer(DEFAULT_STREAMER)
                .dateSup(DEFAULT_DATE_SUP);
        return fourCC;
    }

    @Before
    public void initTest() {
        fourCCSearchRepository.deleteAll();
        fourCC = createEntity(em);
    }

    @Test
    @Transactional
    public void createFourCC() throws Exception {
        int databaseSizeBeforeCreate = fourCCRepository.findAll().size();

        // Create the FourCC
        FourCCDTO fourCCDTO = fourCCMapper.fourCCToFourCCDTO(fourCC);

        restFourCCMockMvc.perform(post("/api/four-ccs")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(fourCCDTO)))
                .andExpect(status().isCreated());

        // Validate the FourCC in the database
        List<FourCC> fourCCS = fourCCRepository.findAll();
        assertThat(fourCCS).hasSize(databaseSizeBeforeCreate + 1);
        FourCC testFourCC = fourCCS.get(fourCCS.size() - 1);
        assertThat(testFourCC.getTypeStream()).isEqualTo(DEFAULT_TYPE_STREAM);
        assertThat(testFourCC.getValeur()).isEqualTo(DEFAULT_VALEUR);
        assertThat(testFourCC.getStreamer()).isEqualTo(DEFAULT_STREAMER);
        assertThat(testFourCC.getDateSup()).isEqualTo(DEFAULT_DATE_SUP);

        // Validate the FourCC in ElasticSearch
        FourCC fourCCEs = fourCCSearchRepository.findOne(testFourCC.getId());
        assertThat(fourCCEs).isEqualToComparingFieldByField(testFourCC);
    }

    @Test
    @Transactional
    public void getAllFourCCS() throws Exception {
        // Initialize the database
        fourCCRepository.saveAndFlush(fourCC);

        // Get all the fourCCS
        restFourCCMockMvc.perform(get("/api/four-ccs?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.[*].id").value(hasItem(fourCC.getId().intValue())))
                .andExpect(jsonPath("$.[*].typeStream").value(hasItem(DEFAULT_TYPE_STREAM.toString())))
                .andExpect(jsonPath("$.[*].valeur").value(hasItem(DEFAULT_VALEUR.toString())))
                .andExpect(jsonPath("$.[*].streamer").value(hasItem(DEFAULT_STREAMER.toString())))
                .andExpect(jsonPath("$.[*].dateSup").value(hasItem(DEFAULT_DATE_SUP_STR)));
    }

    @Test
    @Transactional
    public void getFourCC() throws Exception {
        // Initialize the database
        fourCCRepository.saveAndFlush(fourCC);

        // Get the fourCC
        restFourCCMockMvc.perform(get("/api/four-ccs/{id}", fourCC.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(fourCC.getId().intValue()))
            .andExpect(jsonPath("$.typeStream").value(DEFAULT_TYPE_STREAM.toString()))
            .andExpect(jsonPath("$.valeur").value(DEFAULT_VALEUR.toString()))
            .andExpect(jsonPath("$.streamer").value(DEFAULT_STREAMER.toString()))
            .andExpect(jsonPath("$.dateSup").value(DEFAULT_DATE_SUP_STR));
    }

    @Test
    @Transactional
    public void getNonExistingFourCC() throws Exception {
        // Get the fourCC
        restFourCCMockMvc.perform(get("/api/four-ccs/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateFourCC() throws Exception {
        // Initialize the database
        fourCCRepository.saveAndFlush(fourCC);
        fourCCSearchRepository.save(fourCC);
        int databaseSizeBeforeUpdate = fourCCRepository.findAll().size();

        // Update the fourCC
        FourCC updatedFourCC = fourCCRepository.findOne(fourCC.getId());
        updatedFourCC
                .typeStream(UPDATED_TYPE_STREAM)
                .valeur(UPDATED_VALEUR)
                .streamer(UPDATED_STREAMER)
                .dateSup(UPDATED_DATE_SUP);
        FourCCDTO fourCCDTO = fourCCMapper.fourCCToFourCCDTO(updatedFourCC);

        restFourCCMockMvc.perform(put("/api/four-ccs")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(fourCCDTO)))
                .andExpect(status().isOk());

        // Validate the FourCC in the database
        List<FourCC> fourCCS = fourCCRepository.findAll();
        assertThat(fourCCS).hasSize(databaseSizeBeforeUpdate);
        FourCC testFourCC = fourCCS.get(fourCCS.size() - 1);
        assertThat(testFourCC.getTypeStream()).isEqualTo(UPDATED_TYPE_STREAM);
        assertThat(testFourCC.getValeur()).isEqualTo(UPDATED_VALEUR);
        assertThat(testFourCC.getStreamer()).isEqualTo(UPDATED_STREAMER);
        assertThat(testFourCC.getDateSup()).isEqualTo(UPDATED_DATE_SUP);

        // Validate the FourCC in ElasticSearch
        FourCC fourCCEs = fourCCSearchRepository.findOne(testFourCC.getId());
        assertThat(fourCCEs).isEqualToComparingFieldByField(testFourCC);
    }

    @Test
    @Transactional
    public void deleteFourCC() throws Exception {
        // Initialize the database
        fourCCRepository.saveAndFlush(fourCC);
        fourCCSearchRepository.save(fourCC);
        int databaseSizeBeforeDelete = fourCCRepository.findAll().size();

        // Get the fourCC
        restFourCCMockMvc.perform(delete("/api/four-ccs/{id}", fourCC.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate ElasticSearch is empty
        boolean fourCCExistsInEs = fourCCSearchRepository.exists(fourCC.getId());
        assertThat(fourCCExistsInEs).isFalse();

        // Validate the database is empty
        List<FourCC> fourCCS = fourCCRepository.findAll();
        assertThat(fourCCS).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchFourCC() throws Exception {
        // Initialize the database
        fourCCRepository.saveAndFlush(fourCC);
        fourCCSearchRepository.save(fourCC);

        // Search the fourCC
        restFourCCMockMvc.perform(get("/api/_search/four-ccs?query=id:" + fourCC.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(fourCC.getId().intValue())))
            .andExpect(jsonPath("$.[*].typeStream").value(hasItem(DEFAULT_TYPE_STREAM.toString())))
            .andExpect(jsonPath("$.[*].valeur").value(hasItem(DEFAULT_VALEUR.toString())))
            .andExpect(jsonPath("$.[*].streamer").value(hasItem(DEFAULT_STREAMER.toString())))
            .andExpect(jsonPath("$.[*].dateSup").value(hasItem(DEFAULT_DATE_SUP_STR)));
    }
}
