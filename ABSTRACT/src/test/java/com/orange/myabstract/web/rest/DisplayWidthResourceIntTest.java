package com.orange.myabstract.web.rest;

import com.orange.myabstract.AbstractApp;

import com.orange.myabstract.domain.DisplayWidth;
import com.orange.myabstract.repository.DisplayWidthRepository;
import com.orange.myabstract.service.DisplayWidthService;
import com.orange.myabstract.repository.search.DisplayWidthSearchRepository;
import com.orange.myabstract.service.dto.DisplayWidthDTO;
import com.orange.myabstract.service.mapper.DisplayWidthMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the DisplayWidthResource REST controller.
 *
 * @see DisplayWidthResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = AbstractApp.class)
public class DisplayWidthResourceIntTest {

    private static final String DEFAULT_TYPE_STREAM = "AAAAAAAAAA";
    private static final String UPDATED_TYPE_STREAM = "BBBBBBBBBB";

    private static final String DEFAULT_STREAMER = "AAAAAAAAAA";
    private static final String UPDATED_STREAMER = "BBBBBBBBBB";

    private static final String DEFAULT_VALEUR = "AAAAAAAAAA";
    private static final String UPDATED_VALEUR = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_DATE_SUP = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_DATE_SUP = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final String DEFAULT_DATE_SUP_STR = DateTimeFormatter.ISO_INSTANT.format(DEFAULT_DATE_SUP);

    @Inject
    private DisplayWidthRepository displayWidthRepository;

    @Inject
    private DisplayWidthMapper displayWidthMapper;

    @Inject
    private DisplayWidthService displayWidthService;

    @Inject
    private DisplayWidthSearchRepository displayWidthSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Inject
    private EntityManager em;

    private MockMvc restDisplayWidthMockMvc;

    private DisplayWidth displayWidth;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        DisplayWidthResource displayWidthResource = new DisplayWidthResource();
        ReflectionTestUtils.setField(displayWidthResource, "displayWidthService", displayWidthService);
        this.restDisplayWidthMockMvc = MockMvcBuilders.standaloneSetup(displayWidthResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DisplayWidth createEntity(EntityManager em) {
        DisplayWidth displayWidth = new DisplayWidth()
                .typeStream(DEFAULT_TYPE_STREAM)
                .streamer(DEFAULT_STREAMER)
                .valeur(DEFAULT_VALEUR)
                .dateSup(DEFAULT_DATE_SUP);
        return displayWidth;
    }

    @Before
    public void initTest() {
        displayWidthSearchRepository.deleteAll();
        displayWidth = createEntity(em);
    }

    @Test
    @Transactional
    public void createDisplayWidth() throws Exception {
        int databaseSizeBeforeCreate = displayWidthRepository.findAll().size();

        // Create the DisplayWidth
        DisplayWidthDTO displayWidthDTO = displayWidthMapper.displayWidthToDisplayWidthDTO(displayWidth);

        restDisplayWidthMockMvc.perform(post("/api/display-widths")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(displayWidthDTO)))
                .andExpect(status().isCreated());

        // Validate the DisplayWidth in the database
        List<DisplayWidth> displayWidths = displayWidthRepository.findAll();
        assertThat(displayWidths).hasSize(databaseSizeBeforeCreate + 1);
        DisplayWidth testDisplayWidth = displayWidths.get(displayWidths.size() - 1);
        assertThat(testDisplayWidth.getTypeStream()).isEqualTo(DEFAULT_TYPE_STREAM);
        assertThat(testDisplayWidth.getStreamer()).isEqualTo(DEFAULT_STREAMER);
        assertThat(testDisplayWidth.getValeur()).isEqualTo(DEFAULT_VALEUR);
        assertThat(testDisplayWidth.getDateSup()).isEqualTo(DEFAULT_DATE_SUP);

        // Validate the DisplayWidth in ElasticSearch
        DisplayWidth displayWidthEs = displayWidthSearchRepository.findOne(testDisplayWidth.getId());
        assertThat(displayWidthEs).isEqualToComparingFieldByField(testDisplayWidth);
    }

    @Test
    @Transactional
    public void getAllDisplayWidths() throws Exception {
        // Initialize the database
        displayWidthRepository.saveAndFlush(displayWidth);

        // Get all the displayWidths
        restDisplayWidthMockMvc.perform(get("/api/display-widths?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.[*].id").value(hasItem(displayWidth.getId().intValue())))
                .andExpect(jsonPath("$.[*].typeStream").value(hasItem(DEFAULT_TYPE_STREAM.toString())))
                .andExpect(jsonPath("$.[*].streamer").value(hasItem(DEFAULT_STREAMER.toString())))
                .andExpect(jsonPath("$.[*].valeur").value(hasItem(DEFAULT_VALEUR.toString())))
                .andExpect(jsonPath("$.[*].dateSup").value(hasItem(DEFAULT_DATE_SUP_STR)));
    }

    @Test
    @Transactional
    public void getDisplayWidth() throws Exception {
        // Initialize the database
        displayWidthRepository.saveAndFlush(displayWidth);

        // Get the displayWidth
        restDisplayWidthMockMvc.perform(get("/api/display-widths/{id}", displayWidth.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(displayWidth.getId().intValue()))
            .andExpect(jsonPath("$.typeStream").value(DEFAULT_TYPE_STREAM.toString()))
            .andExpect(jsonPath("$.streamer").value(DEFAULT_STREAMER.toString()))
            .andExpect(jsonPath("$.valeur").value(DEFAULT_VALEUR.toString()))
            .andExpect(jsonPath("$.dateSup").value(DEFAULT_DATE_SUP_STR));
    }

    @Test
    @Transactional
    public void getNonExistingDisplayWidth() throws Exception {
        // Get the displayWidth
        restDisplayWidthMockMvc.perform(get("/api/display-widths/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateDisplayWidth() throws Exception {
        // Initialize the database
        displayWidthRepository.saveAndFlush(displayWidth);
        displayWidthSearchRepository.save(displayWidth);
        int databaseSizeBeforeUpdate = displayWidthRepository.findAll().size();

        // Update the displayWidth
        DisplayWidth updatedDisplayWidth = displayWidthRepository.findOne(displayWidth.getId());
        updatedDisplayWidth
                .typeStream(UPDATED_TYPE_STREAM)
                .streamer(UPDATED_STREAMER)
                .valeur(UPDATED_VALEUR)
                .dateSup(UPDATED_DATE_SUP);
        DisplayWidthDTO displayWidthDTO = displayWidthMapper.displayWidthToDisplayWidthDTO(updatedDisplayWidth);

        restDisplayWidthMockMvc.perform(put("/api/display-widths")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(displayWidthDTO)))
                .andExpect(status().isOk());

        // Validate the DisplayWidth in the database
        List<DisplayWidth> displayWidths = displayWidthRepository.findAll();
        assertThat(displayWidths).hasSize(databaseSizeBeforeUpdate);
        DisplayWidth testDisplayWidth = displayWidths.get(displayWidths.size() - 1);
        assertThat(testDisplayWidth.getTypeStream()).isEqualTo(UPDATED_TYPE_STREAM);
        assertThat(testDisplayWidth.getStreamer()).isEqualTo(UPDATED_STREAMER);
        assertThat(testDisplayWidth.getValeur()).isEqualTo(UPDATED_VALEUR);
        assertThat(testDisplayWidth.getDateSup()).isEqualTo(UPDATED_DATE_SUP);

        // Validate the DisplayWidth in ElasticSearch
        DisplayWidth displayWidthEs = displayWidthSearchRepository.findOne(testDisplayWidth.getId());
        assertThat(displayWidthEs).isEqualToComparingFieldByField(testDisplayWidth);
    }

    @Test
    @Transactional
    public void deleteDisplayWidth() throws Exception {
        // Initialize the database
        displayWidthRepository.saveAndFlush(displayWidth);
        displayWidthSearchRepository.save(displayWidth);
        int databaseSizeBeforeDelete = displayWidthRepository.findAll().size();

        // Get the displayWidth
        restDisplayWidthMockMvc.perform(delete("/api/display-widths/{id}", displayWidth.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate ElasticSearch is empty
        boolean displayWidthExistsInEs = displayWidthSearchRepository.exists(displayWidth.getId());
        assertThat(displayWidthExistsInEs).isFalse();

        // Validate the database is empty
        List<DisplayWidth> displayWidths = displayWidthRepository.findAll();
        assertThat(displayWidths).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchDisplayWidth() throws Exception {
        // Initialize the database
        displayWidthRepository.saveAndFlush(displayWidth);
        displayWidthSearchRepository.save(displayWidth);

        // Search the displayWidth
        restDisplayWidthMockMvc.perform(get("/api/_search/display-widths?query=id:" + displayWidth.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(displayWidth.getId().intValue())))
            .andExpect(jsonPath("$.[*].typeStream").value(hasItem(DEFAULT_TYPE_STREAM.toString())))
            .andExpect(jsonPath("$.[*].streamer").value(hasItem(DEFAULT_STREAMER.toString())))
            .andExpect(jsonPath("$.[*].valeur").value(hasItem(DEFAULT_VALEUR.toString())))
            .andExpect(jsonPath("$.[*].dateSup").value(hasItem(DEFAULT_DATE_SUP_STR)));
    }
}
