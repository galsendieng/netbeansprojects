package com.orange.myabstract.web.rest;

import com.orange.myabstract.AbstractApp;

import com.orange.myabstract.domain.MaxHeight;
import com.orange.myabstract.repository.MaxHeightRepository;
import com.orange.myabstract.service.MaxHeightService;
import com.orange.myabstract.repository.search.MaxHeightSearchRepository;
import com.orange.myabstract.service.dto.MaxHeightDTO;
import com.orange.myabstract.service.mapper.MaxHeightMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the MaxHeightResource REST controller.
 *
 * @see MaxHeightResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = AbstractApp.class)
public class MaxHeightResourceIntTest {

    private static final String DEFAULT_TYPE_STREAM = "AAAAAAAAAA";
    private static final String UPDATED_TYPE_STREAM = "BBBBBBBBBB";

    private static final String DEFAULT_STREAMER = "AAAAAAAAAA";
    private static final String UPDATED_STREAMER = "BBBBBBBBBB";

    private static final String DEFAULT_VALEUR = "AAAAAAAAAA";
    private static final String UPDATED_VALEUR = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_DATE_SUP = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_DATE_SUP = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final String DEFAULT_DATE_SUP_STR = DateTimeFormatter.ISO_INSTANT.format(DEFAULT_DATE_SUP);

    @Inject
    private MaxHeightRepository maxHeightRepository;

    @Inject
    private MaxHeightMapper maxHeightMapper;

    @Inject
    private MaxHeightService maxHeightService;

    @Inject
    private MaxHeightSearchRepository maxHeightSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Inject
    private EntityManager em;

    private MockMvc restMaxHeightMockMvc;

    private MaxHeight maxHeight;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        MaxHeightResource maxHeightResource = new MaxHeightResource();
        ReflectionTestUtils.setField(maxHeightResource, "maxHeightService", maxHeightService);
        this.restMaxHeightMockMvc = MockMvcBuilders.standaloneSetup(maxHeightResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static MaxHeight createEntity(EntityManager em) {
        MaxHeight maxHeight = new MaxHeight()
                .typeStream(DEFAULT_TYPE_STREAM)
                .streamer(DEFAULT_STREAMER)
                .valeur(DEFAULT_VALEUR)
                .dateSup(DEFAULT_DATE_SUP);
        return maxHeight;
    }

    @Before
    public void initTest() {
        maxHeightSearchRepository.deleteAll();
        maxHeight = createEntity(em);
    }

    @Test
    @Transactional
    public void createMaxHeight() throws Exception {
        int databaseSizeBeforeCreate = maxHeightRepository.findAll().size();

        // Create the MaxHeight
        MaxHeightDTO maxHeightDTO = maxHeightMapper.maxHeightToMaxHeightDTO(maxHeight);

        restMaxHeightMockMvc.perform(post("/api/max-heights")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(maxHeightDTO)))
                .andExpect(status().isCreated());

        // Validate the MaxHeight in the database
        List<MaxHeight> maxHeights = maxHeightRepository.findAll();
        assertThat(maxHeights).hasSize(databaseSizeBeforeCreate + 1);
        MaxHeight testMaxHeight = maxHeights.get(maxHeights.size() - 1);
        assertThat(testMaxHeight.getTypeStream()).isEqualTo(DEFAULT_TYPE_STREAM);
        assertThat(testMaxHeight.getStreamer()).isEqualTo(DEFAULT_STREAMER);
        assertThat(testMaxHeight.getValeur()).isEqualTo(DEFAULT_VALEUR);
        assertThat(testMaxHeight.getDateSup()).isEqualTo(DEFAULT_DATE_SUP);

        // Validate the MaxHeight in ElasticSearch
        MaxHeight maxHeightEs = maxHeightSearchRepository.findOne(testMaxHeight.getId());
        assertThat(maxHeightEs).isEqualToComparingFieldByField(testMaxHeight);
    }

    @Test
    @Transactional
    public void getAllMaxHeights() throws Exception {
        // Initialize the database
        maxHeightRepository.saveAndFlush(maxHeight);

        // Get all the maxHeights
        restMaxHeightMockMvc.perform(get("/api/max-heights?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.[*].id").value(hasItem(maxHeight.getId().intValue())))
                .andExpect(jsonPath("$.[*].typeStream").value(hasItem(DEFAULT_TYPE_STREAM.toString())))
                .andExpect(jsonPath("$.[*].streamer").value(hasItem(DEFAULT_STREAMER.toString())))
                .andExpect(jsonPath("$.[*].valeur").value(hasItem(DEFAULT_VALEUR.toString())))
                .andExpect(jsonPath("$.[*].dateSup").value(hasItem(DEFAULT_DATE_SUP_STR)));
    }

    @Test
    @Transactional
    public void getMaxHeight() throws Exception {
        // Initialize the database
        maxHeightRepository.saveAndFlush(maxHeight);

        // Get the maxHeight
        restMaxHeightMockMvc.perform(get("/api/max-heights/{id}", maxHeight.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(maxHeight.getId().intValue()))
            .andExpect(jsonPath("$.typeStream").value(DEFAULT_TYPE_STREAM.toString()))
            .andExpect(jsonPath("$.streamer").value(DEFAULT_STREAMER.toString()))
            .andExpect(jsonPath("$.valeur").value(DEFAULT_VALEUR.toString()))
            .andExpect(jsonPath("$.dateSup").value(DEFAULT_DATE_SUP_STR));
    }

    @Test
    @Transactional
    public void getNonExistingMaxHeight() throws Exception {
        // Get the maxHeight
        restMaxHeightMockMvc.perform(get("/api/max-heights/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateMaxHeight() throws Exception {
        // Initialize the database
        maxHeightRepository.saveAndFlush(maxHeight);
        maxHeightSearchRepository.save(maxHeight);
        int databaseSizeBeforeUpdate = maxHeightRepository.findAll().size();

        // Update the maxHeight
        MaxHeight updatedMaxHeight = maxHeightRepository.findOne(maxHeight.getId());
        updatedMaxHeight
                .typeStream(UPDATED_TYPE_STREAM)
                .streamer(UPDATED_STREAMER)
                .valeur(UPDATED_VALEUR)
                .dateSup(UPDATED_DATE_SUP);
        MaxHeightDTO maxHeightDTO = maxHeightMapper.maxHeightToMaxHeightDTO(updatedMaxHeight);

        restMaxHeightMockMvc.perform(put("/api/max-heights")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(maxHeightDTO)))
                .andExpect(status().isOk());

        // Validate the MaxHeight in the database
        List<MaxHeight> maxHeights = maxHeightRepository.findAll();
        assertThat(maxHeights).hasSize(databaseSizeBeforeUpdate);
        MaxHeight testMaxHeight = maxHeights.get(maxHeights.size() - 1);
        assertThat(testMaxHeight.getTypeStream()).isEqualTo(UPDATED_TYPE_STREAM);
        assertThat(testMaxHeight.getStreamer()).isEqualTo(UPDATED_STREAMER);
        assertThat(testMaxHeight.getValeur()).isEqualTo(UPDATED_VALEUR);
        assertThat(testMaxHeight.getDateSup()).isEqualTo(UPDATED_DATE_SUP);

        // Validate the MaxHeight in ElasticSearch
        MaxHeight maxHeightEs = maxHeightSearchRepository.findOne(testMaxHeight.getId());
        assertThat(maxHeightEs).isEqualToComparingFieldByField(testMaxHeight);
    }

    @Test
    @Transactional
    public void deleteMaxHeight() throws Exception {
        // Initialize the database
        maxHeightRepository.saveAndFlush(maxHeight);
        maxHeightSearchRepository.save(maxHeight);
        int databaseSizeBeforeDelete = maxHeightRepository.findAll().size();

        // Get the maxHeight
        restMaxHeightMockMvc.perform(delete("/api/max-heights/{id}", maxHeight.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate ElasticSearch is empty
        boolean maxHeightExistsInEs = maxHeightSearchRepository.exists(maxHeight.getId());
        assertThat(maxHeightExistsInEs).isFalse();

        // Validate the database is empty
        List<MaxHeight> maxHeights = maxHeightRepository.findAll();
        assertThat(maxHeights).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchMaxHeight() throws Exception {
        // Initialize the database
        maxHeightRepository.saveAndFlush(maxHeight);
        maxHeightSearchRepository.save(maxHeight);

        // Search the maxHeight
        restMaxHeightMockMvc.perform(get("/api/_search/max-heights?query=id:" + maxHeight.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(maxHeight.getId().intValue())))
            .andExpect(jsonPath("$.[*].typeStream").value(hasItem(DEFAULT_TYPE_STREAM.toString())))
            .andExpect(jsonPath("$.[*].streamer").value(hasItem(DEFAULT_STREAMER.toString())))
            .andExpect(jsonPath("$.[*].valeur").value(hasItem(DEFAULT_VALEUR.toString())))
            .andExpect(jsonPath("$.[*].dateSup").value(hasItem(DEFAULT_DATE_SUP_STR)));
    }
}
