package com.orange.myabstract.web.rest;

import com.orange.myabstract.AbstractApp;

import com.orange.myabstract.domain.PacketSize;
import com.orange.myabstract.repository.PacketSizeRepository;
import com.orange.myabstract.service.PacketSizeService;
import com.orange.myabstract.repository.search.PacketSizeSearchRepository;
import com.orange.myabstract.service.dto.PacketSizeDTO;
import com.orange.myabstract.service.mapper.PacketSizeMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the PacketSizeResource REST controller.
 *
 * @see PacketSizeResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = AbstractApp.class)
public class PacketSizeResourceIntTest {

    private static final String DEFAULT_TYPE_STREAM = "AAAAAAAAAA";
    private static final String UPDATED_TYPE_STREAM = "BBBBBBBBBB";

    private static final String DEFAULT_VALEUR = "AAAAAAAAAA";
    private static final String UPDATED_VALEUR = "BBBBBBBBBB";

    private static final String DEFAULT_STREAMER = "AAAAAAAAAA";
    private static final String UPDATED_STREAMER = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_DATE_SUP = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_DATE_SUP = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final String DEFAULT_DATE_SUP_STR = DateTimeFormatter.ISO_INSTANT.format(DEFAULT_DATE_SUP);

    @Inject
    private PacketSizeRepository packetSizeRepository;

    @Inject
    private PacketSizeMapper packetSizeMapper;

    @Inject
    private PacketSizeService packetSizeService;

    @Inject
    private PacketSizeSearchRepository packetSizeSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Inject
    private EntityManager em;

    private MockMvc restPacketSizeMockMvc;

    private PacketSize packetSize;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        PacketSizeResource packetSizeResource = new PacketSizeResource();
        ReflectionTestUtils.setField(packetSizeResource, "packetSizeService", packetSizeService);
        this.restPacketSizeMockMvc = MockMvcBuilders.standaloneSetup(packetSizeResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PacketSize createEntity(EntityManager em) {
        PacketSize packetSize = new PacketSize()
                .typeStream(DEFAULT_TYPE_STREAM)
                .valeur(DEFAULT_VALEUR)
                .streamer(DEFAULT_STREAMER)
                .dateSup(DEFAULT_DATE_SUP);
        return packetSize;
    }

    @Before
    public void initTest() {
        packetSizeSearchRepository.deleteAll();
        packetSize = createEntity(em);
    }

    @Test
    @Transactional
    public void createPacketSize() throws Exception {
        int databaseSizeBeforeCreate = packetSizeRepository.findAll().size();

        // Create the PacketSize
        PacketSizeDTO packetSizeDTO = packetSizeMapper.packetSizeToPacketSizeDTO(packetSize);

        restPacketSizeMockMvc.perform(post("/api/packet-sizes")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(packetSizeDTO)))
                .andExpect(status().isCreated());

        // Validate the PacketSize in the database
        List<PacketSize> packetSizes = packetSizeRepository.findAll();
        assertThat(packetSizes).hasSize(databaseSizeBeforeCreate + 1);
        PacketSize testPacketSize = packetSizes.get(packetSizes.size() - 1);
        assertThat(testPacketSize.getTypeStream()).isEqualTo(DEFAULT_TYPE_STREAM);
        assertThat(testPacketSize.getValeur()).isEqualTo(DEFAULT_VALEUR);
        assertThat(testPacketSize.getStreamer()).isEqualTo(DEFAULT_STREAMER);
        assertThat(testPacketSize.getDateSup()).isEqualTo(DEFAULT_DATE_SUP);

        // Validate the PacketSize in ElasticSearch
        PacketSize packetSizeEs = packetSizeSearchRepository.findOne(testPacketSize.getId());
        assertThat(packetSizeEs).isEqualToComparingFieldByField(testPacketSize);
    }

    @Test
    @Transactional
    public void getAllPacketSizes() throws Exception {
        // Initialize the database
        packetSizeRepository.saveAndFlush(packetSize);

        // Get all the packetSizes
        restPacketSizeMockMvc.perform(get("/api/packet-sizes?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.[*].id").value(hasItem(packetSize.getId().intValue())))
                .andExpect(jsonPath("$.[*].typeStream").value(hasItem(DEFAULT_TYPE_STREAM.toString())))
                .andExpect(jsonPath("$.[*].valeur").value(hasItem(DEFAULT_VALEUR.toString())))
                .andExpect(jsonPath("$.[*].streamer").value(hasItem(DEFAULT_STREAMER.toString())))
                .andExpect(jsonPath("$.[*].dateSup").value(hasItem(DEFAULT_DATE_SUP_STR)));
    }

    @Test
    @Transactional
    public void getPacketSize() throws Exception {
        // Initialize the database
        packetSizeRepository.saveAndFlush(packetSize);

        // Get the packetSize
        restPacketSizeMockMvc.perform(get("/api/packet-sizes/{id}", packetSize.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(packetSize.getId().intValue()))
            .andExpect(jsonPath("$.typeStream").value(DEFAULT_TYPE_STREAM.toString()))
            .andExpect(jsonPath("$.valeur").value(DEFAULT_VALEUR.toString()))
            .andExpect(jsonPath("$.streamer").value(DEFAULT_STREAMER.toString()))
            .andExpect(jsonPath("$.dateSup").value(DEFAULT_DATE_SUP_STR));
    }

    @Test
    @Transactional
    public void getNonExistingPacketSize() throws Exception {
        // Get the packetSize
        restPacketSizeMockMvc.perform(get("/api/packet-sizes/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePacketSize() throws Exception {
        // Initialize the database
        packetSizeRepository.saveAndFlush(packetSize);
        packetSizeSearchRepository.save(packetSize);
        int databaseSizeBeforeUpdate = packetSizeRepository.findAll().size();

        // Update the packetSize
        PacketSize updatedPacketSize = packetSizeRepository.findOne(packetSize.getId());
        updatedPacketSize
                .typeStream(UPDATED_TYPE_STREAM)
                .valeur(UPDATED_VALEUR)
                .streamer(UPDATED_STREAMER)
                .dateSup(UPDATED_DATE_SUP);
        PacketSizeDTO packetSizeDTO = packetSizeMapper.packetSizeToPacketSizeDTO(updatedPacketSize);

        restPacketSizeMockMvc.perform(put("/api/packet-sizes")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(packetSizeDTO)))
                .andExpect(status().isOk());

        // Validate the PacketSize in the database
        List<PacketSize> packetSizes = packetSizeRepository.findAll();
        assertThat(packetSizes).hasSize(databaseSizeBeforeUpdate);
        PacketSize testPacketSize = packetSizes.get(packetSizes.size() - 1);
        assertThat(testPacketSize.getTypeStream()).isEqualTo(UPDATED_TYPE_STREAM);
        assertThat(testPacketSize.getValeur()).isEqualTo(UPDATED_VALEUR);
        assertThat(testPacketSize.getStreamer()).isEqualTo(UPDATED_STREAMER);
        assertThat(testPacketSize.getDateSup()).isEqualTo(UPDATED_DATE_SUP);

        // Validate the PacketSize in ElasticSearch
        PacketSize packetSizeEs = packetSizeSearchRepository.findOne(testPacketSize.getId());
        assertThat(packetSizeEs).isEqualToComparingFieldByField(testPacketSize);
    }

    @Test
    @Transactional
    public void deletePacketSize() throws Exception {
        // Initialize the database
        packetSizeRepository.saveAndFlush(packetSize);
        packetSizeSearchRepository.save(packetSize);
        int databaseSizeBeforeDelete = packetSizeRepository.findAll().size();

        // Get the packetSize
        restPacketSizeMockMvc.perform(delete("/api/packet-sizes/{id}", packetSize.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate ElasticSearch is empty
        boolean packetSizeExistsInEs = packetSizeSearchRepository.exists(packetSize.getId());
        assertThat(packetSizeExistsInEs).isFalse();

        // Validate the database is empty
        List<PacketSize> packetSizes = packetSizeRepository.findAll();
        assertThat(packetSizes).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchPacketSize() throws Exception {
        // Initialize the database
        packetSizeRepository.saveAndFlush(packetSize);
        packetSizeSearchRepository.save(packetSize);

        // Search the packetSize
        restPacketSizeMockMvc.perform(get("/api/_search/packet-sizes?query=id:" + packetSize.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(packetSize.getId().intValue())))
            .andExpect(jsonPath("$.[*].typeStream").value(hasItem(DEFAULT_TYPE_STREAM.toString())))
            .andExpect(jsonPath("$.[*].valeur").value(hasItem(DEFAULT_VALEUR.toString())))
            .andExpect(jsonPath("$.[*].streamer").value(hasItem(DEFAULT_STREAMER.toString())))
            .andExpect(jsonPath("$.[*].dateSup").value(hasItem(DEFAULT_DATE_SUP_STR)));
    }
}
