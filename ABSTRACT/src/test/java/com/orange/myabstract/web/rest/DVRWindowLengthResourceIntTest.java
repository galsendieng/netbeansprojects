package com.orange.myabstract.web.rest;

import com.orange.myabstract.AbstractApp;

import com.orange.myabstract.domain.DVRWindowLength;
import com.orange.myabstract.repository.DVRWindowLengthRepository;
import com.orange.myabstract.service.DVRWindowLengthService;
import com.orange.myabstract.repository.search.DVRWindowLengthSearchRepository;
import com.orange.myabstract.service.dto.DVRWindowLengthDTO;
import com.orange.myabstract.service.mapper.DVRWindowLengthMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the DVRWindowLengthResource REST controller.
 *
 * @see DVRWindowLengthResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = AbstractApp.class)
public class DVRWindowLengthResourceIntTest {

    private static final String DEFAULT_VALEUR = "AAAAAAAAAA";
    private static final String UPDATED_VALEUR = "BBBBBBBBBB";

    private static final String DEFAULT_STREAMER = "AAAAAAAAAA";
    private static final String UPDATED_STREAMER = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_DATE_SUP = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_DATE_SUP = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final String DEFAULT_DATE_SUP_STR = DateTimeFormatter.ISO_INSTANT.format(DEFAULT_DATE_SUP);

    @Inject
    private DVRWindowLengthRepository dVRWindowLengthRepository;

    @Inject
    private DVRWindowLengthMapper dVRWindowLengthMapper;

    @Inject
    private DVRWindowLengthService dVRWindowLengthService;

    @Inject
    private DVRWindowLengthSearchRepository dVRWindowLengthSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Inject
    private EntityManager em;

    private MockMvc restDVRWindowLengthMockMvc;

    private DVRWindowLength dVRWindowLength;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        DVRWindowLengthResource dVRWindowLengthResource = new DVRWindowLengthResource();
        ReflectionTestUtils.setField(dVRWindowLengthResource, "dVRWindowLengthService", dVRWindowLengthService);
        this.restDVRWindowLengthMockMvc = MockMvcBuilders.standaloneSetup(dVRWindowLengthResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DVRWindowLength createEntity(EntityManager em) {
        DVRWindowLength dVRWindowLength = new DVRWindowLength()
                .valeur(DEFAULT_VALEUR)
                .streamer(DEFAULT_STREAMER)
                .dateSup(DEFAULT_DATE_SUP);
        return dVRWindowLength;
    }

    @Before
    public void initTest() {
        dVRWindowLengthSearchRepository.deleteAll();
        dVRWindowLength = createEntity(em);
    }

    @Test
    @Transactional
    public void createDVRWindowLength() throws Exception {
        int databaseSizeBeforeCreate = dVRWindowLengthRepository.findAll().size();

        // Create the DVRWindowLength
        DVRWindowLengthDTO dVRWindowLengthDTO = dVRWindowLengthMapper.dVRWindowLengthToDVRWindowLengthDTO(dVRWindowLength);

        restDVRWindowLengthMockMvc.perform(post("/api/d-vr-window-lengths")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(dVRWindowLengthDTO)))
                .andExpect(status().isCreated());

        // Validate the DVRWindowLength in the database
        List<DVRWindowLength> dVRWindowLengths = dVRWindowLengthRepository.findAll();
        assertThat(dVRWindowLengths).hasSize(databaseSizeBeforeCreate + 1);
        DVRWindowLength testDVRWindowLength = dVRWindowLengths.get(dVRWindowLengths.size() - 1);
        assertThat(testDVRWindowLength.getValeur()).isEqualTo(DEFAULT_VALEUR);
        assertThat(testDVRWindowLength.getStreamer()).isEqualTo(DEFAULT_STREAMER);
        assertThat(testDVRWindowLength.getDateSup()).isEqualTo(DEFAULT_DATE_SUP);

        // Validate the DVRWindowLength in ElasticSearch
        DVRWindowLength dVRWindowLengthEs = dVRWindowLengthSearchRepository.findOne(testDVRWindowLength.getId());
        assertThat(dVRWindowLengthEs).isEqualToComparingFieldByField(testDVRWindowLength);
    }

    @Test
    @Transactional
    public void getAllDVRWindowLengths() throws Exception {
        // Initialize the database
        dVRWindowLengthRepository.saveAndFlush(dVRWindowLength);

        // Get all the dVRWindowLengths
        restDVRWindowLengthMockMvc.perform(get("/api/d-vr-window-lengths?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.[*].id").value(hasItem(dVRWindowLength.getId().intValue())))
                .andExpect(jsonPath("$.[*].valeur").value(hasItem(DEFAULT_VALEUR.toString())))
                .andExpect(jsonPath("$.[*].streamer").value(hasItem(DEFAULT_STREAMER.toString())))
                .andExpect(jsonPath("$.[*].dateSup").value(hasItem(DEFAULT_DATE_SUP_STR)));
    }

    @Test
    @Transactional
    public void getDVRWindowLength() throws Exception {
        // Initialize the database
        dVRWindowLengthRepository.saveAndFlush(dVRWindowLength);

        // Get the dVRWindowLength
        restDVRWindowLengthMockMvc.perform(get("/api/d-vr-window-lengths/{id}", dVRWindowLength.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(dVRWindowLength.getId().intValue()))
            .andExpect(jsonPath("$.valeur").value(DEFAULT_VALEUR.toString()))
            .andExpect(jsonPath("$.streamer").value(DEFAULT_STREAMER.toString()))
            .andExpect(jsonPath("$.dateSup").value(DEFAULT_DATE_SUP_STR));
    }

    @Test
    @Transactional
    public void getNonExistingDVRWindowLength() throws Exception {
        // Get the dVRWindowLength
        restDVRWindowLengthMockMvc.perform(get("/api/d-vr-window-lengths/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateDVRWindowLength() throws Exception {
        // Initialize the database
        dVRWindowLengthRepository.saveAndFlush(dVRWindowLength);
        dVRWindowLengthSearchRepository.save(dVRWindowLength);
        int databaseSizeBeforeUpdate = dVRWindowLengthRepository.findAll().size();

        // Update the dVRWindowLength
        DVRWindowLength updatedDVRWindowLength = dVRWindowLengthRepository.findOne(dVRWindowLength.getId());
        updatedDVRWindowLength
                .valeur(UPDATED_VALEUR)
                .streamer(UPDATED_STREAMER)
                .dateSup(UPDATED_DATE_SUP);
        DVRWindowLengthDTO dVRWindowLengthDTO = dVRWindowLengthMapper.dVRWindowLengthToDVRWindowLengthDTO(updatedDVRWindowLength);

        restDVRWindowLengthMockMvc.perform(put("/api/d-vr-window-lengths")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(dVRWindowLengthDTO)))
                .andExpect(status().isOk());

        // Validate the DVRWindowLength in the database
        List<DVRWindowLength> dVRWindowLengths = dVRWindowLengthRepository.findAll();
        assertThat(dVRWindowLengths).hasSize(databaseSizeBeforeUpdate);
        DVRWindowLength testDVRWindowLength = dVRWindowLengths.get(dVRWindowLengths.size() - 1);
        assertThat(testDVRWindowLength.getValeur()).isEqualTo(UPDATED_VALEUR);
        assertThat(testDVRWindowLength.getStreamer()).isEqualTo(UPDATED_STREAMER);
        assertThat(testDVRWindowLength.getDateSup()).isEqualTo(UPDATED_DATE_SUP);

        // Validate the DVRWindowLength in ElasticSearch
        DVRWindowLength dVRWindowLengthEs = dVRWindowLengthSearchRepository.findOne(testDVRWindowLength.getId());
        assertThat(dVRWindowLengthEs).isEqualToComparingFieldByField(testDVRWindowLength);
    }

    @Test
    @Transactional
    public void deleteDVRWindowLength() throws Exception {
        // Initialize the database
        dVRWindowLengthRepository.saveAndFlush(dVRWindowLength);
        dVRWindowLengthSearchRepository.save(dVRWindowLength);
        int databaseSizeBeforeDelete = dVRWindowLengthRepository.findAll().size();

        // Get the dVRWindowLength
        restDVRWindowLengthMockMvc.perform(delete("/api/d-vr-window-lengths/{id}", dVRWindowLength.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate ElasticSearch is empty
        boolean dVRWindowLengthExistsInEs = dVRWindowLengthSearchRepository.exists(dVRWindowLength.getId());
        assertThat(dVRWindowLengthExistsInEs).isFalse();

        // Validate the database is empty
        List<DVRWindowLength> dVRWindowLengths = dVRWindowLengthRepository.findAll();
        assertThat(dVRWindowLengths).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchDVRWindowLength() throws Exception {
        // Initialize the database
        dVRWindowLengthRepository.saveAndFlush(dVRWindowLength);
        dVRWindowLengthSearchRepository.save(dVRWindowLength);

        // Search the dVRWindowLength
        restDVRWindowLengthMockMvc.perform(get("/api/_search/d-vr-window-lengths?query=id:" + dVRWindowLength.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(dVRWindowLength.getId().intValue())))
            .andExpect(jsonPath("$.[*].valeur").value(hasItem(DEFAULT_VALEUR.toString())))
            .andExpect(jsonPath("$.[*].streamer").value(hasItem(DEFAULT_STREAMER.toString())))
            .andExpect(jsonPath("$.[*].dateSup").value(hasItem(DEFAULT_DATE_SUP_STR)));
    }
}
