package com.orange.myabstract.web.rest;

import com.orange.myabstract.AbstractApp;

import com.orange.myabstract.domain.Mode;
import com.orange.myabstract.repository.ModeRepository;
import com.orange.myabstract.service.ModeService;
import com.orange.myabstract.repository.search.ModeSearchRepository;
import com.orange.myabstract.service.dto.ModeDTO;
import com.orange.myabstract.service.mapper.ModeMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ModeResource REST controller.
 *
 * @see ModeResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = AbstractApp.class)
public class ModeResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_ABR_STATIC = "AAAAAAAAAA";
    private static final String UPDATED_ABR_STATIC = "BBBBBBBBBB";

    private static final Integer DEFAULT_FRAGMENT = 1;
    private static final Integer UPDATED_FRAGMENT = 2;

    private static final String DEFAULT_SUFFIX_MANIFEST = "AAAAAAAAAA";
    private static final String UPDATED_SUFFIX_MANIFEST = "BBBBBBBBBB";

    private static final String DEFAULT_DEVICE_PROFIL = "AAAAAAAAAA";
    private static final String UPDATED_DEVICE_PROFIL = "BBBBBBBBBB";

    private static final String DEFAULT_CLIENT_VERSION = "AAAAAAAAAA";
    private static final String UPDATED_CLIENT_VERSION = "BBBBBBBBBB";

    private static final String DEFAULT_TYPE_MANIFEST = "AAAAAAAAAA";
    private static final String UPDATED_TYPE_MANIFEST = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    @Inject
    private ModeRepository modeRepository;

    @Inject
    private ModeMapper modeMapper;

    @Inject
    private ModeService modeService;

    @Inject
    private ModeSearchRepository modeSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Inject
    private EntityManager em;

    private MockMvc restModeMockMvc;

    private Mode mode;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        ModeResource modeResource = new ModeResource();
        ReflectionTestUtils.setField(modeResource, "modeService", modeService);
        this.restModeMockMvc = MockMvcBuilders.standaloneSetup(modeResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Mode createEntity(EntityManager em) {
        Mode mode = new Mode()
                .name(DEFAULT_NAME)
                .abrStatic(DEFAULT_ABR_STATIC)
                .fragment(DEFAULT_FRAGMENT)
                .suffixManifest(DEFAULT_SUFFIX_MANIFEST)
                .deviceProfil(DEFAULT_DEVICE_PROFIL)
                .clientVersion(DEFAULT_CLIENT_VERSION)
                .typeManifest(DEFAULT_TYPE_MANIFEST)
                .description(DEFAULT_DESCRIPTION);
        return mode;
    }

    @Before
    public void initTest() {
        modeSearchRepository.deleteAll();
        mode = createEntity(em);
    }

    @Test
    @Transactional
    public void createMode() throws Exception {
        int databaseSizeBeforeCreate = modeRepository.findAll().size();

        // Create the Mode
        ModeDTO modeDTO = modeMapper.modeToModeDTO(mode);

        restModeMockMvc.perform(post("/api/modes")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(modeDTO)))
                .andExpect(status().isCreated());

        // Validate the Mode in the database
        List<Mode> modes = modeRepository.findAll();
        assertThat(modes).hasSize(databaseSizeBeforeCreate + 1);
        Mode testMode = modes.get(modes.size() - 1);
        assertThat(testMode.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testMode.getAbrStatic()).isEqualTo(DEFAULT_ABR_STATIC);
        assertThat(testMode.getFragment()).isEqualTo(DEFAULT_FRAGMENT);
        assertThat(testMode.getSuffixManifest()).isEqualTo(DEFAULT_SUFFIX_MANIFEST);
        assertThat(testMode.getDeviceProfil()).isEqualTo(DEFAULT_DEVICE_PROFIL);
        assertThat(testMode.getClientVersion()).isEqualTo(DEFAULT_CLIENT_VERSION);
        assertThat(testMode.getTypeManifest()).isEqualTo(DEFAULT_TYPE_MANIFEST);
        assertThat(testMode.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);

        // Validate the Mode in ElasticSearch
        Mode modeEs = modeSearchRepository.findOne(testMode.getId());
        assertThat(modeEs).isEqualToComparingFieldByField(testMode);
    }

    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = modeRepository.findAll().size();
        // set the field null
        mode.setName(null);

        // Create the Mode, which fails.
        ModeDTO modeDTO = modeMapper.modeToModeDTO(mode);

        restModeMockMvc.perform(post("/api/modes")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(modeDTO)))
                .andExpect(status().isBadRequest());

        List<Mode> modes = modeRepository.findAll();
        assertThat(modes).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkAbrStaticIsRequired() throws Exception {
        int databaseSizeBeforeTest = modeRepository.findAll().size();
        // set the field null
        mode.setAbrStatic(null);

        // Create the Mode, which fails.
        ModeDTO modeDTO = modeMapper.modeToModeDTO(mode);

        restModeMockMvc.perform(post("/api/modes")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(modeDTO)))
                .andExpect(status().isBadRequest());

        List<Mode> modes = modeRepository.findAll();
        assertThat(modes).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkFragmentIsRequired() throws Exception {
        int databaseSizeBeforeTest = modeRepository.findAll().size();
        // set the field null
        mode.setFragment(null);

        // Create the Mode, which fails.
        ModeDTO modeDTO = modeMapper.modeToModeDTO(mode);

        restModeMockMvc.perform(post("/api/modes")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(modeDTO)))
                .andExpect(status().isBadRequest());

        List<Mode> modes = modeRepository.findAll();
        assertThat(modes).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkSuffixManifestIsRequired() throws Exception {
        int databaseSizeBeforeTest = modeRepository.findAll().size();
        // set the field null
        mode.setSuffixManifest(null);

        // Create the Mode, which fails.
        ModeDTO modeDTO = modeMapper.modeToModeDTO(mode);

        restModeMockMvc.perform(post("/api/modes")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(modeDTO)))
                .andExpect(status().isBadRequest());

        List<Mode> modes = modeRepository.findAll();
        assertThat(modes).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDeviceProfilIsRequired() throws Exception {
        int databaseSizeBeforeTest = modeRepository.findAll().size();
        // set the field null
        mode.setDeviceProfil(null);

        // Create the Mode, which fails.
        ModeDTO modeDTO = modeMapper.modeToModeDTO(mode);

        restModeMockMvc.perform(post("/api/modes")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(modeDTO)))
                .andExpect(status().isBadRequest());

        List<Mode> modes = modeRepository.findAll();
        assertThat(modes).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllModes() throws Exception {
        // Initialize the database
        modeRepository.saveAndFlush(mode);

        // Get all the modes
        restModeMockMvc.perform(get("/api/modes?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.[*].id").value(hasItem(mode.getId().intValue())))
                .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
                .andExpect(jsonPath("$.[*].abrStatic").value(hasItem(DEFAULT_ABR_STATIC.toString())))
                .andExpect(jsonPath("$.[*].fragment").value(hasItem(DEFAULT_FRAGMENT)))
                .andExpect(jsonPath("$.[*].suffixManifest").value(hasItem(DEFAULT_SUFFIX_MANIFEST.toString())))
                .andExpect(jsonPath("$.[*].deviceProfil").value(hasItem(DEFAULT_DEVICE_PROFIL.toString())))
                .andExpect(jsonPath("$.[*].clientVersion").value(hasItem(DEFAULT_CLIENT_VERSION.toString())))
                .andExpect(jsonPath("$.[*].typeManifest").value(hasItem(DEFAULT_TYPE_MANIFEST.toString())))
                .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())));
    }

    @Test
    @Transactional
    public void getMode() throws Exception {
        // Initialize the database
        modeRepository.saveAndFlush(mode);

        // Get the mode
        restModeMockMvc.perform(get("/api/modes/{id}", mode.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(mode.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.abrStatic").value(DEFAULT_ABR_STATIC.toString()))
            .andExpect(jsonPath("$.fragment").value(DEFAULT_FRAGMENT))
            .andExpect(jsonPath("$.suffixManifest").value(DEFAULT_SUFFIX_MANIFEST.toString()))
            .andExpect(jsonPath("$.deviceProfil").value(DEFAULT_DEVICE_PROFIL.toString()))
            .andExpect(jsonPath("$.clientVersion").value(DEFAULT_CLIENT_VERSION.toString()))
            .andExpect(jsonPath("$.typeManifest").value(DEFAULT_TYPE_MANIFEST.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingMode() throws Exception {
        // Get the mode
        restModeMockMvc.perform(get("/api/modes/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateMode() throws Exception {
        // Initialize the database
        modeRepository.saveAndFlush(mode);
        modeSearchRepository.save(mode);
        int databaseSizeBeforeUpdate = modeRepository.findAll().size();

        // Update the mode
        Mode updatedMode = modeRepository.findOne(mode.getId());
        updatedMode
                .name(UPDATED_NAME)
                .abrStatic(UPDATED_ABR_STATIC)
                .fragment(UPDATED_FRAGMENT)
                .suffixManifest(UPDATED_SUFFIX_MANIFEST)
                .deviceProfil(UPDATED_DEVICE_PROFIL)
                .clientVersion(UPDATED_CLIENT_VERSION)
                .typeManifest(UPDATED_TYPE_MANIFEST)
                .description(UPDATED_DESCRIPTION);
        ModeDTO modeDTO = modeMapper.modeToModeDTO(updatedMode);

        restModeMockMvc.perform(put("/api/modes")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(modeDTO)))
                .andExpect(status().isOk());

        // Validate the Mode in the database
        List<Mode> modes = modeRepository.findAll();
        assertThat(modes).hasSize(databaseSizeBeforeUpdate);
        Mode testMode = modes.get(modes.size() - 1);
        assertThat(testMode.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testMode.getAbrStatic()).isEqualTo(UPDATED_ABR_STATIC);
        assertThat(testMode.getFragment()).isEqualTo(UPDATED_FRAGMENT);
        assertThat(testMode.getSuffixManifest()).isEqualTo(UPDATED_SUFFIX_MANIFEST);
        assertThat(testMode.getDeviceProfil()).isEqualTo(UPDATED_DEVICE_PROFIL);
        assertThat(testMode.getClientVersion()).isEqualTo(UPDATED_CLIENT_VERSION);
        assertThat(testMode.getTypeManifest()).isEqualTo(UPDATED_TYPE_MANIFEST);
        assertThat(testMode.getDescription()).isEqualTo(UPDATED_DESCRIPTION);

        // Validate the Mode in ElasticSearch
        Mode modeEs = modeSearchRepository.findOne(testMode.getId());
        assertThat(modeEs).isEqualToComparingFieldByField(testMode);
    }

    @Test
    @Transactional
    public void deleteMode() throws Exception {
        // Initialize the database
        modeRepository.saveAndFlush(mode);
        modeSearchRepository.save(mode);
        int databaseSizeBeforeDelete = modeRepository.findAll().size();

        // Get the mode
        restModeMockMvc.perform(delete("/api/modes/{id}", mode.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate ElasticSearch is empty
        boolean modeExistsInEs = modeSearchRepository.exists(mode.getId());
        assertThat(modeExistsInEs).isFalse();

        // Validate the database is empty
        List<Mode> modes = modeRepository.findAll();
        assertThat(modes).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchMode() throws Exception {
        // Initialize the database
        modeRepository.saveAndFlush(mode);
        modeSearchRepository.save(mode);

        // Search the mode
        restModeMockMvc.perform(get("/api/_search/modes?query=id:" + mode.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(mode.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].abrStatic").value(hasItem(DEFAULT_ABR_STATIC.toString())))
            .andExpect(jsonPath("$.[*].fragment").value(hasItem(DEFAULT_FRAGMENT)))
            .andExpect(jsonPath("$.[*].suffixManifest").value(hasItem(DEFAULT_SUFFIX_MANIFEST.toString())))
            .andExpect(jsonPath("$.[*].deviceProfil").value(hasItem(DEFAULT_DEVICE_PROFIL.toString())))
            .andExpect(jsonPath("$.[*].clientVersion").value(hasItem(DEFAULT_CLIENT_VERSION.toString())))
            .andExpect(jsonPath("$.[*].typeManifest").value(hasItem(DEFAULT_TYPE_MANIFEST.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())));
    }
}
