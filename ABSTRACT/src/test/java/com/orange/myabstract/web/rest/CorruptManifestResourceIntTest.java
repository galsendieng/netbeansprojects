package com.orange.myabstract.web.rest;

import com.orange.myabstract.AbstractApp;

import com.orange.myabstract.domain.CorruptManifest;
import com.orange.myabstract.repository.CorruptManifestRepository;
import com.orange.myabstract.service.CorruptManifestService;
import com.orange.myabstract.repository.search.CorruptManifestSearchRepository;
import com.orange.myabstract.service.dto.CorruptManifestDTO;
import com.orange.myabstract.service.mapper.CorruptManifestMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the CorruptManifestResource REST controller.
 *
 * @see CorruptManifestResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = AbstractApp.class)
public class CorruptManifestResourceIntTest {

    private static final ZonedDateTime DEFAULT_DATE_MANIFEST = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_DATE_MANIFEST = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final String DEFAULT_DATE_MANIFEST_STR = DateTimeFormatter.ISO_INSTANT.format(DEFAULT_DATE_MANIFEST);

    private static final String DEFAULT_COMMAND = "AAAAAAAAAA";
    private static final String UPDATED_COMMAND = "BBBBBBBBBB";

    private static final String DEFAULT_PROFIL = "AAAAAAAAAA";
    private static final String UPDATED_PROFIL = "BBBBBBBBBB";

    private static final byte[] DEFAULT_FILE = TestUtil.createByteArray(1, "0");
    private static final byte[] UPDATED_FILE = TestUtil.createByteArray(2, "1");
    private static final String DEFAULT_FILE_CONTENT_TYPE = "image/jpg";
    private static final String UPDATED_FILE_CONTENT_TYPE = "image/png";

    private static final String DEFAULT_ERRORS = "AAAAAAAAAA";
    private static final String UPDATED_ERRORS = "BBBBBBBBBB";

    @Inject
    private CorruptManifestRepository corruptManifestRepository;

    @Inject
    private CorruptManifestMapper corruptManifestMapper;

    @Inject
    private CorruptManifestService corruptManifestService;

    @Inject
    private CorruptManifestSearchRepository corruptManifestSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Inject
    private EntityManager em;

    private MockMvc restCorruptManifestMockMvc;

    private CorruptManifest corruptManifest;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        CorruptManifestResource corruptManifestResource = new CorruptManifestResource();
        ReflectionTestUtils.setField(corruptManifestResource, "corruptManifestService", corruptManifestService);
        this.restCorruptManifestMockMvc = MockMvcBuilders.standaloneSetup(corruptManifestResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CorruptManifest createEntity(EntityManager em) {
        CorruptManifest corruptManifest = new CorruptManifest()
                .dateManifest(DEFAULT_DATE_MANIFEST)
                .command(DEFAULT_COMMAND)
                .profil(DEFAULT_PROFIL)
                .file(DEFAULT_FILE)
                .fileContentType(DEFAULT_FILE_CONTENT_TYPE)
                .errors(DEFAULT_ERRORS);
        return corruptManifest;
    }

    @Before
    public void initTest() {
        corruptManifestSearchRepository.deleteAll();
        corruptManifest = createEntity(em);
    }

    @Test
    @Transactional
    public void createCorruptManifest() throws Exception {
        int databaseSizeBeforeCreate = corruptManifestRepository.findAll().size();

        // Create the CorruptManifest
        CorruptManifestDTO corruptManifestDTO = corruptManifestMapper.corruptManifestToCorruptManifestDTO(corruptManifest);

        restCorruptManifestMockMvc.perform(post("/api/corrupt-manifests")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(corruptManifestDTO)))
                .andExpect(status().isCreated());

        // Validate the CorruptManifest in the database
        List<CorruptManifest> corruptManifests = corruptManifestRepository.findAll();
        assertThat(corruptManifests).hasSize(databaseSizeBeforeCreate + 1);
        CorruptManifest testCorruptManifest = corruptManifests.get(corruptManifests.size() - 1);
        assertThat(testCorruptManifest.getDateManifest()).isEqualTo(DEFAULT_DATE_MANIFEST);
        assertThat(testCorruptManifest.getCommand()).isEqualTo(DEFAULT_COMMAND);
        assertThat(testCorruptManifest.getProfil()).isEqualTo(DEFAULT_PROFIL);
        assertThat(testCorruptManifest.getFile()).isEqualTo(DEFAULT_FILE);
        assertThat(testCorruptManifest.getFileContentType()).isEqualTo(DEFAULT_FILE_CONTENT_TYPE);
        assertThat(testCorruptManifest.getErrors()).isEqualTo(DEFAULT_ERRORS);

        // Validate the CorruptManifest in ElasticSearch
        CorruptManifest corruptManifestEs = corruptManifestSearchRepository.findOne(testCorruptManifest.getId());
        assertThat(corruptManifestEs).isEqualToComparingFieldByField(testCorruptManifest);
    }

    @Test
    @Transactional
    public void getAllCorruptManifests() throws Exception {
        // Initialize the database
        corruptManifestRepository.saveAndFlush(corruptManifest);

        // Get all the corruptManifests
        restCorruptManifestMockMvc.perform(get("/api/corrupt-manifests?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.[*].id").value(hasItem(corruptManifest.getId().intValue())))
                .andExpect(jsonPath("$.[*].dateManifest").value(hasItem(DEFAULT_DATE_MANIFEST_STR)))
                .andExpect(jsonPath("$.[*].command").value(hasItem(DEFAULT_COMMAND.toString())))
                .andExpect(jsonPath("$.[*].profil").value(hasItem(DEFAULT_PROFIL.toString())))
                .andExpect(jsonPath("$.[*].fileContentType").value(hasItem(DEFAULT_FILE_CONTENT_TYPE)))
                .andExpect(jsonPath("$.[*].file").value(hasItem(Base64Utils.encodeToString(DEFAULT_FILE))))
                .andExpect(jsonPath("$.[*].errors").value(hasItem(DEFAULT_ERRORS.toString())));
    }

    @Test
    @Transactional
    public void getCorruptManifest() throws Exception {
        // Initialize the database
        corruptManifestRepository.saveAndFlush(corruptManifest);

        // Get the corruptManifest
        restCorruptManifestMockMvc.perform(get("/api/corrupt-manifests/{id}", corruptManifest.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(corruptManifest.getId().intValue()))
            .andExpect(jsonPath("$.dateManifest").value(DEFAULT_DATE_MANIFEST_STR))
            .andExpect(jsonPath("$.command").value(DEFAULT_COMMAND.toString()))
            .andExpect(jsonPath("$.profil").value(DEFAULT_PROFIL.toString()))
            .andExpect(jsonPath("$.fileContentType").value(DEFAULT_FILE_CONTENT_TYPE))
            .andExpect(jsonPath("$.file").value(Base64Utils.encodeToString(DEFAULT_FILE)))
            .andExpect(jsonPath("$.errors").value(DEFAULT_ERRORS.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingCorruptManifest() throws Exception {
        // Get the corruptManifest
        restCorruptManifestMockMvc.perform(get("/api/corrupt-manifests/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCorruptManifest() throws Exception {
        // Initialize the database
        corruptManifestRepository.saveAndFlush(corruptManifest);
        corruptManifestSearchRepository.save(corruptManifest);
        int databaseSizeBeforeUpdate = corruptManifestRepository.findAll().size();

        // Update the corruptManifest
        CorruptManifest updatedCorruptManifest = corruptManifestRepository.findOne(corruptManifest.getId());
        updatedCorruptManifest
                .dateManifest(UPDATED_DATE_MANIFEST)
                .command(UPDATED_COMMAND)
                .profil(UPDATED_PROFIL)
                .file(UPDATED_FILE)
                .fileContentType(UPDATED_FILE_CONTENT_TYPE)
                .errors(UPDATED_ERRORS);
        CorruptManifestDTO corruptManifestDTO = corruptManifestMapper.corruptManifestToCorruptManifestDTO(updatedCorruptManifest);

        restCorruptManifestMockMvc.perform(put("/api/corrupt-manifests")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(corruptManifestDTO)))
                .andExpect(status().isOk());

        // Validate the CorruptManifest in the database
        List<CorruptManifest> corruptManifests = corruptManifestRepository.findAll();
        assertThat(corruptManifests).hasSize(databaseSizeBeforeUpdate);
        CorruptManifest testCorruptManifest = corruptManifests.get(corruptManifests.size() - 1);
        assertThat(testCorruptManifest.getDateManifest()).isEqualTo(UPDATED_DATE_MANIFEST);
        assertThat(testCorruptManifest.getCommand()).isEqualTo(UPDATED_COMMAND);
        assertThat(testCorruptManifest.getProfil()).isEqualTo(UPDATED_PROFIL);
        assertThat(testCorruptManifest.getFile()).isEqualTo(UPDATED_FILE);
        assertThat(testCorruptManifest.getFileContentType()).isEqualTo(UPDATED_FILE_CONTENT_TYPE);
        assertThat(testCorruptManifest.getErrors()).isEqualTo(UPDATED_ERRORS);

        // Validate the CorruptManifest in ElasticSearch
        CorruptManifest corruptManifestEs = corruptManifestSearchRepository.findOne(testCorruptManifest.getId());
        assertThat(corruptManifestEs).isEqualToComparingFieldByField(testCorruptManifest);
    }

    @Test
    @Transactional
    public void deleteCorruptManifest() throws Exception {
        // Initialize the database
        corruptManifestRepository.saveAndFlush(corruptManifest);
        corruptManifestSearchRepository.save(corruptManifest);
        int databaseSizeBeforeDelete = corruptManifestRepository.findAll().size();

        // Get the corruptManifest
        restCorruptManifestMockMvc.perform(delete("/api/corrupt-manifests/{id}", corruptManifest.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate ElasticSearch is empty
        boolean corruptManifestExistsInEs = corruptManifestSearchRepository.exists(corruptManifest.getId());
        assertThat(corruptManifestExistsInEs).isFalse();

        // Validate the database is empty
        List<CorruptManifest> corruptManifests = corruptManifestRepository.findAll();
        assertThat(corruptManifests).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchCorruptManifest() throws Exception {
        // Initialize the database
        corruptManifestRepository.saveAndFlush(corruptManifest);
        corruptManifestSearchRepository.save(corruptManifest);

        // Search the corruptManifest
        restCorruptManifestMockMvc.perform(get("/api/_search/corrupt-manifests?query=id:" + corruptManifest.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(corruptManifest.getId().intValue())))
            .andExpect(jsonPath("$.[*].dateManifest").value(hasItem(DEFAULT_DATE_MANIFEST_STR)))
            .andExpect(jsonPath("$.[*].command").value(hasItem(DEFAULT_COMMAND.toString())))
            .andExpect(jsonPath("$.[*].profil").value(hasItem(DEFAULT_PROFIL.toString())))
            .andExpect(jsonPath("$.[*].fileContentType").value(hasItem(DEFAULT_FILE_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].file").value(hasItem(Base64Utils.encodeToString(DEFAULT_FILE))))
            .andExpect(jsonPath("$.[*].errors").value(hasItem(DEFAULT_ERRORS.toString())));
    }
}
