(function() {
    'use strict';

    angular
        .module('abstractApp')
        .controller('HomeController', HomeController);

    HomeController.$inject = ['$scope', 'Principal', 'LoginService', '$state','Bitrate', 'BitrateSearch','$http'];

    function HomeController ($scope, Principal, LoginService, $state,Bitrate, BitrateSearch,$http) {
        var vm = this;

        vm.account = null;
        vm.isAuthenticated = null;
        vm.login = LoginService.open;
        vm.register = register;
        
        showNLastBitrate();
        $scope.$on('authenticationSuccess', function() {
            getAccount();
        });

        getAccount();

        function getAccount() {
            Principal.identity().then(function(account) {
                vm.account = account;
                vm.isAuthenticated = Principal.isAuthenticated;
            });
        }
        function register () {
            $state.go('register');
        }
        
        function showNLastBitrate(){
            $http.get("http://localhost:8080/api/bitrates")
                 .then(function(response) {
             $scope.myBitrate = response.data;

            var indexedStreamers = [];

            $scope.bitratesToFilter = function() {
                indexedStreamers = [];
                return $scope.myBitrate;
            }

            $scope.filterStreamers = function(bitrate) {
                var streamerIsNew = indexedStreamers.indexOf(bitrate.streamer) == -1;
                if (streamerIsNew) {
                    indexedStreamers.push(bitrate.streamer);
                }
                //alert(indexedStreamers);
                return streamerIsNew;
            }
            // alert(Ok);
             return response.data;
            });
            
            //Highcharts
            
            
    Highcharts.chart('mongraphe', {
        data: {
            table: 'datatable'
        },
        chart: {
            type: 'column'
        },
        title: {
            text: 'Data extracted from a HTML table in the page'
        },
        yAxis: {
            allowDecimals: false,
            title: {
                text: 'Units'
            }
        },
        tooltip: {
            formatter: function () {
                return '<b>' + this.series.name + '</b><br/>' +
                    this.point.y + ' ' + this.point.name.toLowerCase();
            }
        }
    });
            //End Highcharts
            
            //
            
    
            //
        }
    }
})();

