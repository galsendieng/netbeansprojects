(function() {
    'use strict';

    angular
        .module('abstractApp')
        .controller('ChunksMyAbstractDetailController', ChunksMyAbstractDetailController);

    ChunksMyAbstractDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Chunks'];

    function ChunksMyAbstractDetailController($scope, $rootScope, $stateParams, previousState, entity, Chunks) {
        var vm = this;

        vm.chunks = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('abstractApp:chunksUpdate', function(event, result) {
            vm.chunks = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
