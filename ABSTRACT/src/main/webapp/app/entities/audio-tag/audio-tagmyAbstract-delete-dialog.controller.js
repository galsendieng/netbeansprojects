(function() {
    'use strict';

    angular
        .module('abstractApp')
        .controller('AudioTagMyAbstractDeleteController',AudioTagMyAbstractDeleteController);

    AudioTagMyAbstractDeleteController.$inject = ['$uibModalInstance', 'entity', 'AudioTag'];

    function AudioTagMyAbstractDeleteController($uibModalInstance, entity, AudioTag) {
        var vm = this;

        vm.audioTag = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            AudioTag.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
