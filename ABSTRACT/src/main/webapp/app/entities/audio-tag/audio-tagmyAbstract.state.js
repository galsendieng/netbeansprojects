(function() {
    'use strict';

    angular
        .module('abstractApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('audio-tagmyAbstract', {
            parent: 'entity',
            url: '/audio-tagmyAbstract?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'abstractApp.audioTag.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/audio-tag/audio-tagsmyAbstract.html',
                    controller: 'AudioTagMyAbstractController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('audioTag');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('audio-tagmyAbstract-detail', {
            parent: 'entity',
            url: '/audio-tagmyAbstract/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'abstractApp.audioTag.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/audio-tag/audio-tagmyAbstract-detail.html',
                    controller: 'AudioTagMyAbstractDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('audioTag');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'AudioTag', function($stateParams, AudioTag) {
                    return AudioTag.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'audio-tagmyAbstract',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('audio-tagmyAbstract-detail.edit', {
            parent: 'audio-tagmyAbstract-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/audio-tag/audio-tagmyAbstract-dialog.html',
                    controller: 'AudioTagMyAbstractDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['AudioTag', function(AudioTag) {
                            return AudioTag.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('audio-tagmyAbstract.new', {
            parent: 'audio-tagmyAbstract',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/audio-tag/audio-tagmyAbstract-dialog.html',
                    controller: 'AudioTagMyAbstractDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                typeStream: null,
                                valeur: null,
                                streamer: null,
                                dateSup: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('audio-tagmyAbstract', null, { reload: 'audio-tagmyAbstract' });
                }, function() {
                    $state.go('audio-tagmyAbstract');
                });
            }]
        })
        .state('audio-tagmyAbstract.edit', {
            parent: 'audio-tagmyAbstract',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/audio-tag/audio-tagmyAbstract-dialog.html',
                    controller: 'AudioTagMyAbstractDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['AudioTag', function(AudioTag) {
                            return AudioTag.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('audio-tagmyAbstract', null, { reload: 'audio-tagmyAbstract' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('audio-tagmyAbstract.delete', {
            parent: 'audio-tagmyAbstract',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/audio-tag/audio-tagmyAbstract-delete-dialog.html',
                    controller: 'AudioTagMyAbstractDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['AudioTag', function(AudioTag) {
                            return AudioTag.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('audio-tagmyAbstract', null, { reload: 'audio-tagmyAbstract' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
