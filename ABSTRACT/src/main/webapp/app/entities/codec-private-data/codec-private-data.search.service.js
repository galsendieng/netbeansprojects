(function() {
    'use strict';

    angular
        .module('abstractApp')
        .factory('CodecPrivateDataSearch', CodecPrivateDataSearch);

    CodecPrivateDataSearch.$inject = ['$resource'];

    function CodecPrivateDataSearch($resource) {
        var resourceUrl =  'api/_search/codec-private-data/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
