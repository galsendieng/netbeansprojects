(function() {
    'use strict';

    angular
        .module('abstractApp')
        .controller('CodecPrivateDataMyAbstractDetailController', CodecPrivateDataMyAbstractDetailController);

    CodecPrivateDataMyAbstractDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'CodecPrivateData'];

    function CodecPrivateDataMyAbstractDetailController($scope, $rootScope, $stateParams, previousState, entity, CodecPrivateData) {
        var vm = this;

        vm.codecPrivateData = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('abstractApp:codecPrivateDataUpdate', function(event, result) {
            vm.codecPrivateData = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
