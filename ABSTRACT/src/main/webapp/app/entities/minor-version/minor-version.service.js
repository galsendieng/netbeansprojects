(function() {
    'use strict';
    angular
        .module('abstractApp')
        .factory('MinorVersion', MinorVersion);

    MinorVersion.$inject = ['$resource', 'DateUtils'];

    function MinorVersion ($resource, DateUtils) {
        var resourceUrl =  'api/minor-versions/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.dateSup = DateUtils.convertDateTimeFromServer(data.dateSup);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
