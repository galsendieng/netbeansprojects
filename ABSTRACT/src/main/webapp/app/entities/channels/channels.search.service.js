(function() {
    'use strict';

    angular
        .module('abstractApp')
        .factory('ChannelsSearch', ChannelsSearch);

    ChannelsSearch.$inject = ['$resource'];

    function ChannelsSearch($resource) {
        var resourceUrl =  'api/_search/channels/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
