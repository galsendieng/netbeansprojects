(function() {
    'use strict';

    angular
        .module('abstractApp')
        .factory('BitsPerSampleSearch', BitsPerSampleSearch);

    BitsPerSampleSearch.$inject = ['$resource'];

    function BitsPerSampleSearch($resource) {
        var resourceUrl =  'api/_search/bits-per-samples/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
