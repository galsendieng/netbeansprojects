(function() {
    'use strict';

    angular
        .module('abstractApp')
        .controller('BitsPerSampleMyAbstractDetailController', BitsPerSampleMyAbstractDetailController);

    BitsPerSampleMyAbstractDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'BitsPerSample'];

    function BitsPerSampleMyAbstractDetailController($scope, $rootScope, $stateParams, previousState, entity, BitsPerSample) {
        var vm = this;

        vm.bitsPerSample = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('abstractApp:bitsPerSampleUpdate', function(event, result) {
            vm.bitsPerSample = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
