(function() {
    'use strict';

    angular
        .module('abstractApp')
        .controller('LookAheadFragmentCountMyAbstractDialogController', LookAheadFragmentCountMyAbstractDialogController);

    LookAheadFragmentCountMyAbstractDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'LookAheadFragmentCount'];

    function LookAheadFragmentCountMyAbstractDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, LookAheadFragmentCount) {
        var vm = this;

        vm.lookAheadFragmentCount = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.lookAheadFragmentCount.id !== null) {
                LookAheadFragmentCount.update(vm.lookAheadFragmentCount, onSaveSuccess, onSaveError);
            } else {
                LookAheadFragmentCount.save(vm.lookAheadFragmentCount, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('abstractApp:lookAheadFragmentCountUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.dateSup = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
