(function() {
    'use strict';

    angular
        .module('abstractApp')
        .controller('LookAheadFragmentCountMyAbstractDetailController', LookAheadFragmentCountMyAbstractDetailController);

    LookAheadFragmentCountMyAbstractDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'LookAheadFragmentCount'];

    function LookAheadFragmentCountMyAbstractDetailController($scope, $rootScope, $stateParams, previousState, entity, LookAheadFragmentCount) {
        var vm = this;

        vm.lookAheadFragmentCount = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('abstractApp:lookAheadFragmentCountUpdate', function(event, result) {
            vm.lookAheadFragmentCount = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
