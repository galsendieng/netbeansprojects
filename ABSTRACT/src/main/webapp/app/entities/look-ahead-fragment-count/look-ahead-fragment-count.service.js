(function() {
    'use strict';
    angular
        .module('abstractApp')
        .factory('LookAheadFragmentCount', LookAheadFragmentCount);

    LookAheadFragmentCount.$inject = ['$resource', 'DateUtils'];

    function LookAheadFragmentCount ($resource, DateUtils) {
        var resourceUrl =  'api/look-ahead-fragment-counts/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.dateSup = DateUtils.convertDateTimeFromServer(data.dateSup);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
