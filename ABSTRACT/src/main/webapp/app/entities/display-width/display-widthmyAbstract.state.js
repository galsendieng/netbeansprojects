(function() {
    'use strict';

    angular
        .module('abstractApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('display-widthmyAbstract', {
            parent: 'entity',
            url: '/display-widthmyAbstract?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'abstractApp.displayWidth.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/display-width/display-widthsmyAbstract.html',
                    controller: 'DisplayWidthMyAbstractController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('displayWidth');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('display-widthmyAbstract-detail', {
            parent: 'entity',
            url: '/display-widthmyAbstract/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'abstractApp.displayWidth.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/display-width/display-widthmyAbstract-detail.html',
                    controller: 'DisplayWidthMyAbstractDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('displayWidth');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'DisplayWidth', function($stateParams, DisplayWidth) {
                    return DisplayWidth.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'display-widthmyAbstract',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('display-widthmyAbstract-detail.edit', {
            parent: 'display-widthmyAbstract-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/display-width/display-widthmyAbstract-dialog.html',
                    controller: 'DisplayWidthMyAbstractDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['DisplayWidth', function(DisplayWidth) {
                            return DisplayWidth.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('display-widthmyAbstract.new', {
            parent: 'display-widthmyAbstract',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/display-width/display-widthmyAbstract-dialog.html',
                    controller: 'DisplayWidthMyAbstractDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                typeStream: null,
                                streamer: null,
                                valeur: null,
                                dateSup: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('display-widthmyAbstract', null, { reload: 'display-widthmyAbstract' });
                }, function() {
                    $state.go('display-widthmyAbstract');
                });
            }]
        })
        .state('display-widthmyAbstract.edit', {
            parent: 'display-widthmyAbstract',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/display-width/display-widthmyAbstract-dialog.html',
                    controller: 'DisplayWidthMyAbstractDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['DisplayWidth', function(DisplayWidth) {
                            return DisplayWidth.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('display-widthmyAbstract', null, { reload: 'display-widthmyAbstract' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('display-widthmyAbstract.delete', {
            parent: 'display-widthmyAbstract',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/display-width/display-widthmyAbstract-delete-dialog.html',
                    controller: 'DisplayWidthMyAbstractDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['DisplayWidth', function(DisplayWidth) {
                            return DisplayWidth.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('display-widthmyAbstract', null, { reload: 'display-widthmyAbstract' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
