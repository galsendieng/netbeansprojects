(function() {
    'use strict';

    angular
        .module('abstractApp')
        .controller('DisplayWidthMyAbstractDetailController', DisplayWidthMyAbstractDetailController);

    DisplayWidthMyAbstractDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'DisplayWidth'];

    function DisplayWidthMyAbstractDetailController($scope, $rootScope, $stateParams, previousState, entity, DisplayWidth) {
        var vm = this;

        vm.displayWidth = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('abstractApp:displayWidthUpdate', function(event, result) {
            vm.displayWidth = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
