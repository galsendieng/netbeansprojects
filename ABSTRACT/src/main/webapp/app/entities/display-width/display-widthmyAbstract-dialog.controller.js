(function() {
    'use strict';

    angular
        .module('abstractApp')
        .controller('DisplayWidthMyAbstractDialogController', DisplayWidthMyAbstractDialogController);

    DisplayWidthMyAbstractDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'DisplayWidth'];

    function DisplayWidthMyAbstractDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, DisplayWidth) {
        var vm = this;

        vm.displayWidth = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.displayWidth.id !== null) {
                DisplayWidth.update(vm.displayWidth, onSaveSuccess, onSaveError);
            } else {
                DisplayWidth.save(vm.displayWidth, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('abstractApp:displayWidthUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.dateSup = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
