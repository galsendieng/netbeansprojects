(function() {
    'use strict';

    angular
        .module('abstractApp')
        .factory('DisplayWidthSearch', DisplayWidthSearch);

    DisplayWidthSearch.$inject = ['$resource'];

    function DisplayWidthSearch($resource) {
        var resourceUrl =  'api/_search/display-widths/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
