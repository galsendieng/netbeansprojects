(function() {
    'use strict';

    angular
        .module('abstractApp')
        .controller('MaxHeightMyAbstractDeleteController',MaxHeightMyAbstractDeleteController);

    MaxHeightMyAbstractDeleteController.$inject = ['$uibModalInstance', 'entity', 'MaxHeight'];

    function MaxHeightMyAbstractDeleteController($uibModalInstance, entity, MaxHeight) {
        var vm = this;

        vm.maxHeight = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            MaxHeight.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
