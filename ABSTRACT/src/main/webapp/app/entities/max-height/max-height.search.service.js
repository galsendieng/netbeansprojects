(function() {
    'use strict';

    angular
        .module('abstractApp')
        .factory('MaxHeightSearch', MaxHeightSearch);

    MaxHeightSearch.$inject = ['$resource'];

    function MaxHeightSearch($resource) {
        var resourceUrl =  'api/_search/max-heights/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
