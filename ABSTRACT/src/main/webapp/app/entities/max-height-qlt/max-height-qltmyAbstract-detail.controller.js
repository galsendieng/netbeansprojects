(function() {
    'use strict';

    angular
        .module('abstractApp')
        .controller('MaxHeightQltMyAbstractDetailController', MaxHeightQltMyAbstractDetailController);

    MaxHeightQltMyAbstractDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'MaxHeightQlt'];

    function MaxHeightQltMyAbstractDetailController($scope, $rootScope, $stateParams, previousState, entity, MaxHeightQlt) {
        var vm = this;

        vm.maxHeightQlt = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('abstractApp:maxHeightQltUpdate', function(event, result) {
            vm.maxHeightQlt = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
