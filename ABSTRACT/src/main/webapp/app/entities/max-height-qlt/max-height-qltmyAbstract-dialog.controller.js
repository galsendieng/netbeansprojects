(function() {
    'use strict';

    angular
        .module('abstractApp')
        .controller('MaxHeightQltMyAbstractDialogController', MaxHeightQltMyAbstractDialogController);

    MaxHeightQltMyAbstractDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'MaxHeightQlt'];

    function MaxHeightQltMyAbstractDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, MaxHeightQlt) {
        var vm = this;

        vm.maxHeightQlt = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.maxHeightQlt.id !== null) {
                MaxHeightQlt.update(vm.maxHeightQlt, onSaveSuccess, onSaveError);
            } else {
                MaxHeightQlt.save(vm.maxHeightQlt, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('abstractApp:maxHeightQltUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.dateSup = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
