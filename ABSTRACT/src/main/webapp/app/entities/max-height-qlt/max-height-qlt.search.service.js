(function() {
    'use strict';

    angular
        .module('abstractApp')
        .factory('MaxHeightQltSearch', MaxHeightQltSearch);

    MaxHeightQltSearch.$inject = ['$resource'];

    function MaxHeightQltSearch($resource) {
        var resourceUrl =  'api/_search/max-height-qlts/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
