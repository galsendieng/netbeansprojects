(function() {
    'use strict';

    angular
        .module('abstractApp')
        .controller('MonitoringMyAbstractDeleteController',MonitoringMyAbstractDeleteController);

    MonitoringMyAbstractDeleteController.$inject = ['$uibModalInstance', 'entity', 'Monitoring'];

    function MonitoringMyAbstractDeleteController($uibModalInstance, entity, Monitoring) {
        var vm = this;

        vm.monitoring = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            Monitoring.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
