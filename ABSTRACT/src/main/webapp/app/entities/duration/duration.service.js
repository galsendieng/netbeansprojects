(function() {
    'use strict';
    angular
        .module('abstractApp')
        .factory('Duration', Duration);

    Duration.$inject = ['$resource', 'DateUtils'];

    function Duration ($resource, DateUtils) {
        var resourceUrl =  'api/durations/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.dateSup = DateUtils.convertDateTimeFromServer(data.dateSup);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
