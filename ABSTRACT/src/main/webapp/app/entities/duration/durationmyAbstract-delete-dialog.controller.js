(function() {
    'use strict';

    angular
        .module('abstractApp')
        .controller('DurationMyAbstractDeleteController',DurationMyAbstractDeleteController);

    DurationMyAbstractDeleteController.$inject = ['$uibModalInstance', 'entity', 'Duration'];

    function DurationMyAbstractDeleteController($uibModalInstance, entity, Duration) {
        var vm = this;

        vm.duration = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            Duration.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
