(function() {
    'use strict';

    angular
        .module('abstractApp')
        .controller('DisplayHeightMyAbstractDetailController', DisplayHeightMyAbstractDetailController);

    DisplayHeightMyAbstractDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'DisplayHeight'];

    function DisplayHeightMyAbstractDetailController($scope, $rootScope, $stateParams, previousState, entity, DisplayHeight) {
        var vm = this;

        vm.displayHeight = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('abstractApp:displayHeightUpdate', function(event, result) {
            vm.displayHeight = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
