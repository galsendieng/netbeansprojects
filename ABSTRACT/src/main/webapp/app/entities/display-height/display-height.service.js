(function() {
    'use strict';
    angular
        .module('abstractApp')
        .factory('DisplayHeight', DisplayHeight);

    DisplayHeight.$inject = ['$resource', 'DateUtils'];

    function DisplayHeight ($resource, DateUtils) {
        var resourceUrl =  'api/display-heights/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.dateSup = DateUtils.convertDateTimeFromServer(data.dateSup);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
