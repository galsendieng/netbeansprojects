(function() {
    'use strict';

    angular
        .module('abstractApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('display-heightmyAbstract', {
            parent: 'entity',
            url: '/display-heightmyAbstract?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'abstractApp.displayHeight.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/display-height/display-heightsmyAbstract.html',
                    controller: 'DisplayHeightMyAbstractController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('displayHeight');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('display-heightmyAbstract-detail', {
            parent: 'entity',
            url: '/display-heightmyAbstract/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'abstractApp.displayHeight.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/display-height/display-heightmyAbstract-detail.html',
                    controller: 'DisplayHeightMyAbstractDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('displayHeight');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'DisplayHeight', function($stateParams, DisplayHeight) {
                    return DisplayHeight.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'display-heightmyAbstract',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('display-heightmyAbstract-detail.edit', {
            parent: 'display-heightmyAbstract-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/display-height/display-heightmyAbstract-dialog.html',
                    controller: 'DisplayHeightMyAbstractDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['DisplayHeight', function(DisplayHeight) {
                            return DisplayHeight.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('display-heightmyAbstract.new', {
            parent: 'display-heightmyAbstract',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/display-height/display-heightmyAbstract-dialog.html',
                    controller: 'DisplayHeightMyAbstractDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                typeStream: null,
                                streamer: null,
                                valeur: null,
                                dateSup: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('display-heightmyAbstract', null, { reload: 'display-heightmyAbstract' });
                }, function() {
                    $state.go('display-heightmyAbstract');
                });
            }]
        })
        .state('display-heightmyAbstract.edit', {
            parent: 'display-heightmyAbstract',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/display-height/display-heightmyAbstract-dialog.html',
                    controller: 'DisplayHeightMyAbstractDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['DisplayHeight', function(DisplayHeight) {
                            return DisplayHeight.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('display-heightmyAbstract', null, { reload: 'display-heightmyAbstract' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('display-heightmyAbstract.delete', {
            parent: 'display-heightmyAbstract',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/display-height/display-heightmyAbstract-delete-dialog.html',
                    controller: 'DisplayHeightMyAbstractDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['DisplayHeight', function(DisplayHeight) {
                            return DisplayHeight.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('display-heightmyAbstract', null, { reload: 'display-heightmyAbstract' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
