(function() {
    'use strict';
    angular
        .module('abstractApp')
        .factory('Commande', Commande);

    Commande.$inject = ['$resource'];

    function Commande ($resource) {
        var resourceUrl =  'api/commandes/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
