(function() {
    'use strict';

    angular
        .module('abstractApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('commandemyAbstract', {
            parent: 'entity',
            url: '/commandemyAbstract',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'abstractApp.commande.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/commande/commandesmyAbstract.html',
                    controller: 'CommandeMyAbstractController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('commande');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('commandemyAbstract-detail', {
            parent: 'entity',
            url: '/commandemyAbstract/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'abstractApp.commande.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/commande/commandemyAbstract-detail.html',
                    controller: 'CommandeMyAbstractDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('commande');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'Commande', function($stateParams, Commande) {
                    return Commande.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'commandemyAbstract',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('commandemyAbstract-detail.edit', {
            parent: 'commandemyAbstract-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/commande/commandemyAbstract-dialog.html',
                    controller: 'CommandeMyAbstractDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Commande', function(Commande) {
                            return Commande.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('commandemyAbstract.new', {
            parent: 'commandemyAbstract',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/commande/commandemyAbstract-dialog.html',
                    controller: 'CommandeMyAbstractDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                command: null,
                                description: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('commandemyAbstract', null, { reload: 'commandemyAbstract' });
                }, function() {
                    $state.go('commandemyAbstract');
                });
            }]
        })
        .state('commandemyAbstract.edit', {
            parent: 'commandemyAbstract',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/commande/commandemyAbstract-dialog.html',
                    controller: 'CommandeMyAbstractDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Commande', function(Commande) {
                            return Commande.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('commandemyAbstract', null, { reload: 'commandemyAbstract' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('commandemyAbstract.delete', {
            parent: 'commandemyAbstract',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/commande/commandemyAbstract-delete-dialog.html',
                    controller: 'CommandeMyAbstractDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['Commande', function(Commande) {
                            return Commande.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('commandemyAbstract', null, { reload: 'commandemyAbstract' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
