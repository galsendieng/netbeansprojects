(function() {
    'use strict';

    angular
        .module('abstractApp')
        .controller('ManifestDataMyAbstractDetailController', ManifestDataMyAbstractDetailController);

    ManifestDataMyAbstractDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'ManifestData', 'Importance'];

    function ManifestDataMyAbstractDetailController($scope, $rootScope, $stateParams, previousState, entity, ManifestData, Importance) {
        var vm = this;

        vm.manifestData = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('abstractApp:manifestDataUpdate', function(event, result) {
            vm.manifestData = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
