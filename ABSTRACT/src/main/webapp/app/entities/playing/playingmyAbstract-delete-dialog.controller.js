(function() {
    'use strict';

    angular
        .module('abstractApp')
        .controller('PlayingMyAbstractDeleteController',PlayingMyAbstractDeleteController);

    PlayingMyAbstractDeleteController.$inject = ['$uibModalInstance', 'entity', 'Playing'];

    function PlayingMyAbstractDeleteController($uibModalInstance, entity, Playing) {
        var vm = this;

        vm.playing = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            Playing.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
