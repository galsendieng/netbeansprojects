(function() {
    'use strict';

    angular
        .module('abstractApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('playingmyAbstract', {
            parent: 'entity',
            url: '/playingmyAbstract',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'abstractApp.playing.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/playing/playingsmyAbstract.html',
                    controller: 'PlayingMyAbstractController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('playing');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('playingmyAbstract-detail', {
            parent: 'entity',
            url: '/playingmyAbstract/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'abstractApp.playing.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/playing/playingmyAbstract-detail.html',
                    controller: 'PlayingMyAbstractDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('playing');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'Playing', function($stateParams, Playing) {
                    return Playing.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'playingmyAbstract',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('playingmyAbstract-detail.edit', {
            parent: 'playingmyAbstract-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/playing/playingmyAbstract-dialog.html',
                    controller: 'PlayingMyAbstractDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Playing', function(Playing) {
                            return Playing.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('playingmyAbstract.new', {
            parent: 'playingmyAbstract',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/playing/playingmyAbstract-dialog.html',
                    controller: 'PlayingMyAbstractDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                link: null,
                                fileTest: null,
                                fileTestContentType: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('playingmyAbstract', null, { reload: 'playingmyAbstract' });
                }, function() {
                    $state.go('playingmyAbstract');
                });
            }]
        })
        .state('playingmyAbstract.edit', {
            parent: 'playingmyAbstract',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/playing/playingmyAbstract-dialog.html',
                    controller: 'PlayingMyAbstractDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Playing', function(Playing) {
                            return Playing.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('playingmyAbstract', null, { reload: 'playingmyAbstract' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('playingmyAbstract.delete', {
            parent: 'playingmyAbstract',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/playing/playingmyAbstract-delete-dialog.html',
                    controller: 'PlayingMyAbstractDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['Playing', function(Playing) {
                            return Playing.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('playingmyAbstract', null, { reload: 'playingmyAbstract' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
