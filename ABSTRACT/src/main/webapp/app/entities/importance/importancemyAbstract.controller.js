(function() {
    'use strict';

    angular
        .module('abstractApp')
        .controller('ImportanceMyAbstractController', ImportanceMyAbstractController);

    ImportanceMyAbstractController.$inject = ['$scope', '$state', 'DataUtils', 'Importance', 'ImportanceSearch'];

    function ImportanceMyAbstractController ($scope, $state, DataUtils, Importance, ImportanceSearch) {
        var vm = this;

        vm.importances = [];
        vm.openFile = DataUtils.openFile;
        vm.byteSize = DataUtils.byteSize;
        vm.search = search;
        vm.loadAll = loadAll;

        loadAll();

        function loadAll() {
            Importance.query(function(result) {
                vm.importances = result;
            });
        }

        function search () {
            if (!vm.searchQuery) {
                return vm.loadAll();
            }
            ImportanceSearch.query({query: vm.searchQuery}, function(result) {
                vm.importances = result;
            });
        }    }
})();
