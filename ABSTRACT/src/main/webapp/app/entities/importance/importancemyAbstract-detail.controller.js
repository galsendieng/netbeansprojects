(function() {
    'use strict';

    angular
        .module('abstractApp')
        .controller('ImportanceMyAbstractDetailController', ImportanceMyAbstractDetailController);

    ImportanceMyAbstractDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'DataUtils', 'entity', 'Importance', 'ManifestData'];

    function ImportanceMyAbstractDetailController($scope, $rootScope, $stateParams, previousState, DataUtils, entity, Importance, ManifestData) {
        var vm = this;

        vm.importance = entity;
        vm.previousState = previousState.name;
        vm.byteSize = DataUtils.byteSize;
        vm.openFile = DataUtils.openFile;

        var unsubscribe = $rootScope.$on('abstractApp:importanceUpdate', function(event, result) {
            vm.importance = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
