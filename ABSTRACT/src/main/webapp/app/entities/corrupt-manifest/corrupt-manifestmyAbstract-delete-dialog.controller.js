(function() {
    'use strict';

    angular
        .module('abstractApp')
        .controller('CorruptManifestMyAbstractDeleteController',CorruptManifestMyAbstractDeleteController);

    CorruptManifestMyAbstractDeleteController.$inject = ['$uibModalInstance', 'entity', 'CorruptManifest'];

    function CorruptManifestMyAbstractDeleteController($uibModalInstance, entity, CorruptManifest) {
        var vm = this;

        vm.corruptManifest = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            CorruptManifest.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
