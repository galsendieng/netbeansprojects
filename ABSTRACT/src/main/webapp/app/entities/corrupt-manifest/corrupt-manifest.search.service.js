(function() {
    'use strict';

    angular
        .module('abstractApp')
        .factory('CorruptManifestSearch', CorruptManifestSearch);

    CorruptManifestSearch.$inject = ['$resource'];

    function CorruptManifestSearch($resource) {
        var resourceUrl =  'api/_search/corrupt-manifests/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
