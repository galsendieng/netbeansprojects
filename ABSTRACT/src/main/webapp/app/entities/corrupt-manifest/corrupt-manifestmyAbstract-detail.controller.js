(function() {
    'use strict';

    angular
        .module('abstractApp')
        .controller('CorruptManifestMyAbstractDetailController', CorruptManifestMyAbstractDetailController);

    CorruptManifestMyAbstractDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'DataUtils', 'entity', 'CorruptManifest'];

    function CorruptManifestMyAbstractDetailController($scope, $rootScope, $stateParams, previousState, DataUtils, entity, CorruptManifest) {
        var vm = this;

        vm.corruptManifest = entity;
        vm.previousState = previousState.name;
        vm.byteSize = DataUtils.byteSize;
        vm.openFile = DataUtils.openFile;

        var unsubscribe = $rootScope.$on('abstractApp:corruptManifestUpdate', function(event, result) {
            vm.corruptManifest = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
