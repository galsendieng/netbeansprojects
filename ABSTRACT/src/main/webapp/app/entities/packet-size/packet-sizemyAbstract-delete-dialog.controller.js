(function() {
    'use strict';

    angular
        .module('abstractApp')
        .controller('PacketSizeMyAbstractDeleteController',PacketSizeMyAbstractDeleteController);

    PacketSizeMyAbstractDeleteController.$inject = ['$uibModalInstance', 'entity', 'PacketSize'];

    function PacketSizeMyAbstractDeleteController($uibModalInstance, entity, PacketSize) {
        var vm = this;

        vm.packetSize = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            PacketSize.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
