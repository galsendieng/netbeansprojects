(function() {
    'use strict';

    angular
        .module('abstractApp')
        .controller('MaxWidthMyAbstractDialogController', MaxWidthMyAbstractDialogController);

    MaxWidthMyAbstractDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'MaxWidth'];

    function MaxWidthMyAbstractDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, MaxWidth) {
        var vm = this;

        vm.maxWidth = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.maxWidth.id !== null) {
                MaxWidth.update(vm.maxWidth, onSaveSuccess, onSaveError);
            } else {
                MaxWidth.save(vm.maxWidth, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('abstractApp:maxWidthUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.dateSup = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
