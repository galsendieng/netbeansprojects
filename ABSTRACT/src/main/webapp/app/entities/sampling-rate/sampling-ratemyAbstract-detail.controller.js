(function() {
    'use strict';

    angular
        .module('abstractApp')
        .controller('SamplingRateMyAbstractDetailController', SamplingRateMyAbstractDetailController);

    SamplingRateMyAbstractDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'SamplingRate'];

    function SamplingRateMyAbstractDetailController($scope, $rootScope, $stateParams, previousState, entity, SamplingRate) {
        var vm = this;

        vm.samplingRate = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('abstractApp:samplingRateUpdate', function(event, result) {
            vm.samplingRate = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
