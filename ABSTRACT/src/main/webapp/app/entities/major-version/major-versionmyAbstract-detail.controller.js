(function() {
    'use strict';

    angular
        .module('abstractApp')
        .controller('MajorVersionMyAbstractDetailController', MajorVersionMyAbstractDetailController);

    MajorVersionMyAbstractDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'MajorVersion'];

    function MajorVersionMyAbstractDetailController($scope, $rootScope, $stateParams, previousState, entity, MajorVersion) {
        var vm = this;

        vm.majorVersion = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('abstractApp:majorVersionUpdate', function(event, result) {
            vm.majorVersion = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
