(function() {
    'use strict';
    angular
        .module('abstractApp')
        .factory('MajorVersion', MajorVersion);

    MajorVersion.$inject = ['$resource', 'DateUtils'];

    function MajorVersion ($resource, DateUtils) {
        var resourceUrl =  'api/major-versions/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.dateSup = DateUtils.convertDateTimeFromServer(data.dateSup);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
