(function() {
    'use strict';

    angular
        .module('abstractApp')
        .factory('QualityLevelsSearch', QualityLevelsSearch);

    QualityLevelsSearch.$inject = ['$resource'];

    function QualityLevelsSearch($resource) {
        var resourceUrl =  'api/_search/quality-levels/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
