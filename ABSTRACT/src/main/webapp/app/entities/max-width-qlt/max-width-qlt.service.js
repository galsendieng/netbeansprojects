(function() {
    'use strict';
    angular
        .module('abstractApp')
        .factory('MaxWidthQlt', MaxWidthQlt);

    MaxWidthQlt.$inject = ['$resource', 'DateUtils'];

    function MaxWidthQlt ($resource, DateUtils) {
        var resourceUrl =  'api/max-width-qlts/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.dateSup = DateUtils.convertDateTimeFromServer(data.dateSup);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
