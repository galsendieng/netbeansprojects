(function() {
    'use strict';

    angular
        .module('abstractApp')
        .controller('FourCCMyAbstractDialogController', FourCCMyAbstractDialogController);

    FourCCMyAbstractDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'FourCC'];

    function FourCCMyAbstractDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, FourCC) {
        var vm = this;

        vm.fourCC = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.fourCC.id !== null) {
                FourCC.update(vm.fourCC, onSaveSuccess, onSaveError);
            } else {
                FourCC.save(vm.fourCC, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('abstractApp:fourCCUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.dateSup = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
