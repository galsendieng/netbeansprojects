(function () {
    'use strict';

    angular
        .module('abstractApp')

        /*
         Languages codes are ISO_639-1 codes, see http://en.wikipedia.org/wiki/List_of_ISO_639-1_codes
         They are written in English to avoid character encoding issues (not a perfect solution)
         */
        .constant('LANGUAGES', [
            'en',
            'ca',
            'zh-cn',
            'zh-tw',
            'fr',
            'gl',
            'hu',
            'it',
            'pl',
            'pt-br',
            'pt-pt',
            'ro',
            'sk',
            'es',
            'tr'
            // jhipster-needle-i18n-language-constant - JHipster will add/remove languages in this array
        ]
    );
})();
