package com.orange.myabstract.service;

import com.orange.myabstract.service.dto.ManifestDataDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.LinkedList;
import java.util.List;

/**
 * Service Interface for managing ManifestData.
 */
public interface ManifestDataService {

    /**
     * Save a manifestData.
     *
     * @param manifestDataDTO the entity to save
     * @return the persisted entity
     */
    ManifestDataDTO save(ManifestDataDTO manifestDataDTO);

    /**
     *  Get all the manifestData.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<ManifestDataDTO> findAll(Pageable pageable);

    /**
     *  Get the "id" manifestData.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    ManifestDataDTO findOne(Long id);

    /**
     *  Delete the "id" manifestData.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Search for the manifestData corresponding to the query.
     *
     *  @param query the query of the search
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<ManifestDataDTO> search(String query, Pageable pageable);
}
