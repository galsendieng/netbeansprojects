package com.orange.myabstract.service.mapper;

import com.orange.myabstract.domain.*;
import com.orange.myabstract.service.dto.MonitoringDTO;

import org.mapstruct.*;
import java.util.List;

/**
 * Mapper for the entity Monitoring and its DTO MonitoringDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface MonitoringMapper {

    MonitoringDTO monitoringToMonitoringDTO(Monitoring monitoring);

    List<MonitoringDTO> monitoringsToMonitoringDTOs(List<Monitoring> monitorings);

    Monitoring monitoringDTOToMonitoring(MonitoringDTO monitoringDTO);

    List<Monitoring> monitoringDTOsToMonitorings(List<MonitoringDTO> monitoringDTOs);
}
