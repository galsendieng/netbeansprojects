package com.orange.myabstract.service.impl;

import com.orange.myabstract.service.MaxWidthQltService;
import com.orange.myabstract.domain.MaxWidthQlt;
import com.orange.myabstract.repository.MaxWidthQltRepository;
import com.orange.myabstract.repository.search.MaxWidthQltSearchRepository;
import com.orange.myabstract.service.dto.MaxWidthQltDTO;
import com.orange.myabstract.service.mapper.MaxWidthQltMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing MaxWidthQlt.
 */
@Service
@Transactional
public class MaxWidthQltServiceImpl implements MaxWidthQltService{

    private final Logger log = LoggerFactory.getLogger(MaxWidthQltServiceImpl.class);
    
    @Inject
    private MaxWidthQltRepository maxWidthQltRepository;

    @Inject
    private MaxWidthQltMapper maxWidthQltMapper;

    @Inject
    private MaxWidthQltSearchRepository maxWidthQltSearchRepository;

    /**
     * Save a maxWidthQlt.
     *
     * @param maxWidthQltDTO the entity to save
     * @return the persisted entity
     */
    public MaxWidthQltDTO save(MaxWidthQltDTO maxWidthQltDTO) {
        log.debug("Request to save MaxWidthQlt : {}", maxWidthQltDTO);
        MaxWidthQlt maxWidthQlt = maxWidthQltMapper.maxWidthQltDTOToMaxWidthQlt(maxWidthQltDTO);
        maxWidthQlt = maxWidthQltRepository.save(maxWidthQlt);
        MaxWidthQltDTO result = maxWidthQltMapper.maxWidthQltToMaxWidthQltDTO(maxWidthQlt);
        maxWidthQltSearchRepository.save(maxWidthQlt);
        return result;
    }

    /**
     *  Get all the maxWidthQlts.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true) 
    public Page<MaxWidthQltDTO> findAll(Pageable pageable) {
        log.debug("Request to get all MaxWidthQlts");
        Page<MaxWidthQlt> result = maxWidthQltRepository.findAll(pageable);
        return result.map(maxWidthQlt -> maxWidthQltMapper.maxWidthQltToMaxWidthQltDTO(maxWidthQlt));
    }

    /**
     *  Get one maxWidthQlt by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true) 
    public MaxWidthQltDTO findOne(Long id) {
        log.debug("Request to get MaxWidthQlt : {}", id);
        MaxWidthQlt maxWidthQlt = maxWidthQltRepository.findOne(id);
        MaxWidthQltDTO maxWidthQltDTO = maxWidthQltMapper.maxWidthQltToMaxWidthQltDTO(maxWidthQlt);
        return maxWidthQltDTO;
    }

    /**
     *  Delete the  maxWidthQlt by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete MaxWidthQlt : {}", id);
        maxWidthQltRepository.delete(id);
        maxWidthQltSearchRepository.delete(id);
    }

    /**
     * Search for the maxWidthQlt corresponding to the query.
     *
     *  @param query the query of the search
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<MaxWidthQltDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of MaxWidthQlts for query {}", query);
        Page<MaxWidthQlt> result = maxWidthQltSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(maxWidthQlt -> maxWidthQltMapper.maxWidthQltToMaxWidthQltDTO(maxWidthQlt));
    }
}
