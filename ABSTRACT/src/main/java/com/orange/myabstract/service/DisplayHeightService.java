package com.orange.myabstract.service;

import com.orange.myabstract.service.dto.DisplayHeightDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.LinkedList;
import java.util.List;

/**
 * Service Interface for managing DisplayHeight.
 */
public interface DisplayHeightService {

    /**
     * Save a displayHeight.
     *
     * @param displayHeightDTO the entity to save
     * @return the persisted entity
     */
    DisplayHeightDTO save(DisplayHeightDTO displayHeightDTO);

    /**
     *  Get all the displayHeights.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<DisplayHeightDTO> findAll(Pageable pageable);

    /**
     *  Get the "id" displayHeight.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    DisplayHeightDTO findOne(Long id);

    /**
     *  Delete the "id" displayHeight.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Search for the displayHeight corresponding to the query.
     *
     *  @param query the query of the search
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<DisplayHeightDTO> search(String query, Pageable pageable);
}
