package com.orange.myabstract.service.mapper;

import com.orange.myabstract.domain.*;
import com.orange.myabstract.service.dto.ChunksDTO;

import org.mapstruct.*;
import java.util.List;

/**
 * Mapper for the entity Chunks and its DTO ChunksDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface ChunksMapper {

    ChunksDTO chunksToChunksDTO(Chunks chunks);

    List<ChunksDTO> chunksToChunksDTOs(List<Chunks> chunks);

    Chunks chunksDTOToChunks(ChunksDTO chunksDTO);

    List<Chunks> chunksDTOsToChunks(List<ChunksDTO> chunksDTOs);
}
