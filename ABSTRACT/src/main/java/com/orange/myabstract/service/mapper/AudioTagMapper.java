package com.orange.myabstract.service.mapper;

import com.orange.myabstract.domain.*;
import com.orange.myabstract.service.dto.AudioTagDTO;

import org.mapstruct.*;
import java.util.List;

/**
 * Mapper for the entity AudioTag and its DTO AudioTagDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface AudioTagMapper {

    AudioTagDTO audioTagToAudioTagDTO(AudioTag audioTag);

    List<AudioTagDTO> audioTagsToAudioTagDTOs(List<AudioTag> audioTags);

    AudioTag audioTagDTOToAudioTag(AudioTagDTO audioTagDTO);

    List<AudioTag> audioTagDTOsToAudioTags(List<AudioTagDTO> audioTagDTOs);
}
