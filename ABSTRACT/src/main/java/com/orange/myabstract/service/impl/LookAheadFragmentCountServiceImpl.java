package com.orange.myabstract.service.impl;

import com.orange.myabstract.service.LookAheadFragmentCountService;
import com.orange.myabstract.domain.LookAheadFragmentCount;
import com.orange.myabstract.repository.LookAheadFragmentCountRepository;
import com.orange.myabstract.repository.search.LookAheadFragmentCountSearchRepository;
import com.orange.myabstract.service.dto.LookAheadFragmentCountDTO;
import com.orange.myabstract.service.mapper.LookAheadFragmentCountMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing LookAheadFragmentCount.
 */
@Service
@Transactional
public class LookAheadFragmentCountServiceImpl implements LookAheadFragmentCountService{

    private final Logger log = LoggerFactory.getLogger(LookAheadFragmentCountServiceImpl.class);
    
    @Inject
    private LookAheadFragmentCountRepository lookAheadFragmentCountRepository;

    @Inject
    private LookAheadFragmentCountMapper lookAheadFragmentCountMapper;

    @Inject
    private LookAheadFragmentCountSearchRepository lookAheadFragmentCountSearchRepository;

    /**
     * Save a lookAheadFragmentCount.
     *
     * @param lookAheadFragmentCountDTO the entity to save
     * @return the persisted entity
     */
    public LookAheadFragmentCountDTO save(LookAheadFragmentCountDTO lookAheadFragmentCountDTO) {
        log.debug("Request to save LookAheadFragmentCount : {}", lookAheadFragmentCountDTO);
        LookAheadFragmentCount lookAheadFragmentCount = lookAheadFragmentCountMapper.lookAheadFragmentCountDTOToLookAheadFragmentCount(lookAheadFragmentCountDTO);
        lookAheadFragmentCount = lookAheadFragmentCountRepository.save(lookAheadFragmentCount);
        LookAheadFragmentCountDTO result = lookAheadFragmentCountMapper.lookAheadFragmentCountToLookAheadFragmentCountDTO(lookAheadFragmentCount);
        lookAheadFragmentCountSearchRepository.save(lookAheadFragmentCount);
        return result;
    }

    /**
     *  Get all the lookAheadFragmentCounts.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true) 
    public Page<LookAheadFragmentCountDTO> findAll(Pageable pageable) {
        log.debug("Request to get all LookAheadFragmentCounts");
        Page<LookAheadFragmentCount> result = lookAheadFragmentCountRepository.findAll(pageable);
        return result.map(lookAheadFragmentCount -> lookAheadFragmentCountMapper.lookAheadFragmentCountToLookAheadFragmentCountDTO(lookAheadFragmentCount));
    }

    /**
     *  Get one lookAheadFragmentCount by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true) 
    public LookAheadFragmentCountDTO findOne(Long id) {
        log.debug("Request to get LookAheadFragmentCount : {}", id);
        LookAheadFragmentCount lookAheadFragmentCount = lookAheadFragmentCountRepository.findOne(id);
        LookAheadFragmentCountDTO lookAheadFragmentCountDTO = lookAheadFragmentCountMapper.lookAheadFragmentCountToLookAheadFragmentCountDTO(lookAheadFragmentCount);
        return lookAheadFragmentCountDTO;
    }

    /**
     *  Delete the  lookAheadFragmentCount by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete LookAheadFragmentCount : {}", id);
        lookAheadFragmentCountRepository.delete(id);
        lookAheadFragmentCountSearchRepository.delete(id);
    }

    /**
     * Search for the lookAheadFragmentCount corresponding to the query.
     *
     *  @param query the query of the search
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<LookAheadFragmentCountDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of LookAheadFragmentCounts for query {}", query);
        Page<LookAheadFragmentCount> result = lookAheadFragmentCountSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(lookAheadFragmentCount -> lookAheadFragmentCountMapper.lookAheadFragmentCountToLookAheadFragmentCountDTO(lookAheadFragmentCount));
    }
}
