package com.orange.myabstract.service.mapper;

import com.orange.myabstract.domain.*;
import com.orange.myabstract.service.dto.PacketSizeDTO;

import org.mapstruct.*;
import java.util.List;

/**
 * Mapper for the entity PacketSize and its DTO PacketSizeDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface PacketSizeMapper {

    PacketSizeDTO packetSizeToPacketSizeDTO(PacketSize packetSize);

    List<PacketSizeDTO> packetSizesToPacketSizeDTOs(List<PacketSize> packetSizes);

    PacketSize packetSizeDTOToPacketSize(PacketSizeDTO packetSizeDTO);

    List<PacketSize> packetSizeDTOsToPacketSizes(List<PacketSizeDTO> packetSizeDTOs);
}
