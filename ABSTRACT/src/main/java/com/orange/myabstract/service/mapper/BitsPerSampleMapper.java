package com.orange.myabstract.service.mapper;

import com.orange.myabstract.domain.*;
import com.orange.myabstract.service.dto.BitsPerSampleDTO;

import org.mapstruct.*;
import java.util.List;

/**
 * Mapper for the entity BitsPerSample and its DTO BitsPerSampleDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface BitsPerSampleMapper {

    BitsPerSampleDTO bitsPerSampleToBitsPerSampleDTO(BitsPerSample bitsPerSample);

    List<BitsPerSampleDTO> bitsPerSamplesToBitsPerSampleDTOs(List<BitsPerSample> bitsPerSamples);

    BitsPerSample bitsPerSampleDTOToBitsPerSample(BitsPerSampleDTO bitsPerSampleDTO);

    List<BitsPerSample> bitsPerSampleDTOsToBitsPerSamples(List<BitsPerSampleDTO> bitsPerSampleDTOs);
}
