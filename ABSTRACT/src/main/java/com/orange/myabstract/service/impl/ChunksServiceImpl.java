package com.orange.myabstract.service.impl;

import com.orange.myabstract.service.ChunksService;
import com.orange.myabstract.domain.Chunks;
import com.orange.myabstract.repository.ChunksRepository;
import com.orange.myabstract.repository.search.ChunksSearchRepository;
import com.orange.myabstract.service.dto.ChunksDTO;
import com.orange.myabstract.service.mapper.ChunksMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing Chunks.
 */
@Service
@Transactional
public class ChunksServiceImpl implements ChunksService{

    private final Logger log = LoggerFactory.getLogger(ChunksServiceImpl.class);
    
    @Inject
    private ChunksRepository chunksRepository;

    @Inject
    private ChunksMapper chunksMapper;

    @Inject
    private ChunksSearchRepository chunksSearchRepository;

    /**
     * Save a chunks.
     *
     * @param chunksDTO the entity to save
     * @return the persisted entity
     */
    public ChunksDTO save(ChunksDTO chunksDTO) {
        log.debug("Request to save Chunks : {}", chunksDTO);
        Chunks chunks = chunksMapper.chunksDTOToChunks(chunksDTO);
        chunks = chunksRepository.save(chunks);
        ChunksDTO result = chunksMapper.chunksToChunksDTO(chunks);
        chunksSearchRepository.save(chunks);
        return result;
    }

    /**
     *  Get all the chunks.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true) 
    public Page<ChunksDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Chunks");
        Page<Chunks> result = chunksRepository.findAll(pageable);
        return result.map(chunks -> chunksMapper.chunksToChunksDTO(chunks));
    }

    /**
     *  Get one chunks by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true) 
    public ChunksDTO findOne(Long id) {
        log.debug("Request to get Chunks : {}", id);
        Chunks chunks = chunksRepository.findOne(id);
        ChunksDTO chunksDTO = chunksMapper.chunksToChunksDTO(chunks);
        return chunksDTO;
    }

    /**
     *  Delete the  chunks by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Chunks : {}", id);
        chunksRepository.delete(id);
        chunksSearchRepository.delete(id);
    }

    /**
     * Search for the chunks corresponding to the query.
     *
     *  @param query the query of the search
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ChunksDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Chunks for query {}", query);
        Page<Chunks> result = chunksSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(chunks -> chunksMapper.chunksToChunksDTO(chunks));
    }
}
