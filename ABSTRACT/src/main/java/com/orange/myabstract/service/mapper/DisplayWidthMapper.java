package com.orange.myabstract.service.mapper;

import com.orange.myabstract.domain.*;
import com.orange.myabstract.service.dto.DisplayWidthDTO;

import org.mapstruct.*;
import java.util.List;

/**
 * Mapper for the entity DisplayWidth and its DTO DisplayWidthDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface DisplayWidthMapper {

    DisplayWidthDTO displayWidthToDisplayWidthDTO(DisplayWidth displayWidth);

    List<DisplayWidthDTO> displayWidthsToDisplayWidthDTOs(List<DisplayWidth> displayWidths);

    DisplayWidth displayWidthDTOToDisplayWidth(DisplayWidthDTO displayWidthDTO);

    List<DisplayWidth> displayWidthDTOsToDisplayWidths(List<DisplayWidthDTO> displayWidthDTOs);
}
