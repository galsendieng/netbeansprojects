package com.orange.myabstract.service.impl;

import com.orange.myabstract.service.BitsPerSampleService;
import com.orange.myabstract.domain.BitsPerSample;
import com.orange.myabstract.repository.BitsPerSampleRepository;
import com.orange.myabstract.repository.search.BitsPerSampleSearchRepository;
import com.orange.myabstract.service.dto.BitsPerSampleDTO;
import com.orange.myabstract.service.mapper.BitsPerSampleMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing BitsPerSample.
 */
@Service
@Transactional
public class BitsPerSampleServiceImpl implements BitsPerSampleService{

    private final Logger log = LoggerFactory.getLogger(BitsPerSampleServiceImpl.class);
    
    @Inject
    private BitsPerSampleRepository bitsPerSampleRepository;

    @Inject
    private BitsPerSampleMapper bitsPerSampleMapper;

    @Inject
    private BitsPerSampleSearchRepository bitsPerSampleSearchRepository;

    /**
     * Save a bitsPerSample.
     *
     * @param bitsPerSampleDTO the entity to save
     * @return the persisted entity
     */
    public BitsPerSampleDTO save(BitsPerSampleDTO bitsPerSampleDTO) {
        log.debug("Request to save BitsPerSample : {}", bitsPerSampleDTO);
        BitsPerSample bitsPerSample = bitsPerSampleMapper.bitsPerSampleDTOToBitsPerSample(bitsPerSampleDTO);
        bitsPerSample = bitsPerSampleRepository.save(bitsPerSample);
        BitsPerSampleDTO result = bitsPerSampleMapper.bitsPerSampleToBitsPerSampleDTO(bitsPerSample);
        bitsPerSampleSearchRepository.save(bitsPerSample);
        return result;
    }

    /**
     *  Get all the bitsPerSamples.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true) 
    public Page<BitsPerSampleDTO> findAll(Pageable pageable) {
        log.debug("Request to get all BitsPerSamples");
        Page<BitsPerSample> result = bitsPerSampleRepository.findAll(pageable);
        return result.map(bitsPerSample -> bitsPerSampleMapper.bitsPerSampleToBitsPerSampleDTO(bitsPerSample));
    }

    /**
     *  Get one bitsPerSample by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true) 
    public BitsPerSampleDTO findOne(Long id) {
        log.debug("Request to get BitsPerSample : {}", id);
        BitsPerSample bitsPerSample = bitsPerSampleRepository.findOne(id);
        BitsPerSampleDTO bitsPerSampleDTO = bitsPerSampleMapper.bitsPerSampleToBitsPerSampleDTO(bitsPerSample);
        return bitsPerSampleDTO;
    }

    /**
     *  Delete the  bitsPerSample by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete BitsPerSample : {}", id);
        bitsPerSampleRepository.delete(id);
        bitsPerSampleSearchRepository.delete(id);
    }

    /**
     * Search for the bitsPerSample corresponding to the query.
     *
     *  @param query the query of the search
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<BitsPerSampleDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of BitsPerSamples for query {}", query);
        Page<BitsPerSample> result = bitsPerSampleSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(bitsPerSample -> bitsPerSampleMapper.bitsPerSampleToBitsPerSampleDTO(bitsPerSample));
    }
}
