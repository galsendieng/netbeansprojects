package com.orange.myabstract.service.mapper;

import com.orange.myabstract.domain.*;
import com.orange.myabstract.service.dto.StreamerDTO;

import org.mapstruct.*;
import java.util.List;

/**
 * Mapper for the entity Streamer and its DTO StreamerDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface StreamerMapper {

    @Mapping(source = "lePod.id", target = "lePodId")
    @Mapping(source = "lePod.name", target = "lePodName")
    StreamerDTO streamerToStreamerDTO(Streamer streamer);

    List<StreamerDTO> streamersToStreamerDTOs(List<Streamer> streamers);

    @Mapping(source = "lePodId", target = "lePod")
    @Mapping(target = "theChannels", ignore = true)
    Streamer streamerDTOToStreamer(StreamerDTO streamerDTO);

    List<Streamer> streamerDTOsToStreamers(List<StreamerDTO> streamerDTOs);

    default Pod podFromId(Long id) {
        if (id == null) {
            return null;
        }
        Pod pod = new Pod();
        pod.setId(id);
        return pod;
    }
}
