package com.orange.myabstract.service.mapper;

import com.orange.myabstract.domain.*;
import com.orange.myabstract.service.dto.ManifestDataDTO;

import org.mapstruct.*;
import java.util.List;

/**
 * Mapper for the entity ManifestData and its DTO ManifestDataDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface ManifestDataMapper {

    @Mapping(source = "nivImportance.id", target = "nivImportanceId")
    @Mapping(source = "nivImportance.niveau", target = "nivImportanceNiveau")
    ManifestDataDTO manifestDataToManifestDataDTO(ManifestData manifestData);

    List<ManifestDataDTO> manifestDataToManifestDataDTOs(List<ManifestData> manifestData);

    @Mapping(source = "nivImportanceId", target = "nivImportance")
    ManifestData manifestDataDTOToManifestData(ManifestDataDTO manifestDataDTO);

    List<ManifestData> manifestDataDTOsToManifestData(List<ManifestDataDTO> manifestDataDTOs);

    default Importance importanceFromId(Long id) {
        if (id == null) {
            return null;
        }
        Importance importance = new Importance();
        importance.setId(id);
        return importance;
    }
}
