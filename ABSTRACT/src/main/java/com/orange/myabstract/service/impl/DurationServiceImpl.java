package com.orange.myabstract.service.impl;

import com.orange.myabstract.service.DurationService;
import com.orange.myabstract.domain.Duration;
import com.orange.myabstract.repository.DurationRepository;
import com.orange.myabstract.repository.search.DurationSearchRepository;
import com.orange.myabstract.service.dto.DurationDTO;
import com.orange.myabstract.service.mapper.DurationMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing Duration.
 */
@Service
@Transactional
public class DurationServiceImpl implements DurationService{

    private final Logger log = LoggerFactory.getLogger(DurationServiceImpl.class);
    
    @Inject
    private DurationRepository durationRepository;

    @Inject
    private DurationMapper durationMapper;

    @Inject
    private DurationSearchRepository durationSearchRepository;

    /**
     * Save a duration.
     *
     * @param durationDTO the entity to save
     * @return the persisted entity
     */
    public DurationDTO save(DurationDTO durationDTO) {
        log.debug("Request to save Duration : {}", durationDTO);
        Duration duration = durationMapper.durationDTOToDuration(durationDTO);
        duration = durationRepository.save(duration);
        DurationDTO result = durationMapper.durationToDurationDTO(duration);
        durationSearchRepository.save(duration);
        return result;
    }

    /**
     *  Get all the durations.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true) 
    public Page<DurationDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Durations");
        Page<Duration> result = durationRepository.findAll(pageable);
        return result.map(duration -> durationMapper.durationToDurationDTO(duration));
    }

    /**
     *  Get one duration by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true) 
    public DurationDTO findOne(Long id) {
        log.debug("Request to get Duration : {}", id);
        Duration duration = durationRepository.findOne(id);
        DurationDTO durationDTO = durationMapper.durationToDurationDTO(duration);
        return durationDTO;
    }

    /**
     *  Delete the  duration by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Duration : {}", id);
        durationRepository.delete(id);
        durationSearchRepository.delete(id);
    }

    /**
     * Search for the duration corresponding to the query.
     *
     *  @param query the query of the search
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<DurationDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Durations for query {}", query);
        Page<Duration> result = durationSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(duration -> durationMapper.durationToDurationDTO(duration));
    }
}
