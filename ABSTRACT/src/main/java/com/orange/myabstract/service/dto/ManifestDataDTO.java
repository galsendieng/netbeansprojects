package com.orange.myabstract.service.dto;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;


/**
 * A DTO for the ManifestData entity.
 */
public class ManifestDataDTO implements Serializable {

    private Long id;

    @NotNull
    private String attribut;

    @NotNull
    private String valeurAttendue;

    @NotNull
    private String messageErreur;

    private String details;


    private Long nivImportanceId;
    

    private String nivImportanceNiveau;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public String getAttribut() {
        return attribut;
    }

    public void setAttribut(String attribut) {
        this.attribut = attribut;
    }
    public String getValeurAttendue() {
        return valeurAttendue;
    }

    public void setValeurAttendue(String valeurAttendue) {
        this.valeurAttendue = valeurAttendue;
    }
    public String getMessageErreur() {
        return messageErreur;
    }

    public void setMessageErreur(String messageErreur) {
        this.messageErreur = messageErreur;
    }
    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public Long getNivImportanceId() {
        return nivImportanceId;
    }

    public void setNivImportanceId(Long importanceId) {
        this.nivImportanceId = importanceId;
    }


    public String getNivImportanceNiveau() {
        return nivImportanceNiveau;
    }

    public void setNivImportanceNiveau(String importanceNiveau) {
        this.nivImportanceNiveau = importanceNiveau;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ManifestDataDTO manifestDataDTO = (ManifestDataDTO) o;

        if ( ! Objects.equals(id, manifestDataDTO.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "ManifestDataDTO{" +
            "id=" + id +
            ", attribut='" + attribut + "'" +
            ", valeurAttendue='" + valeurAttendue + "'" +
            ", messageErreur='" + messageErreur + "'" +
            ", details='" + details + "'" +
            '}';
    }
}
