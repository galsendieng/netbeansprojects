package com.orange.myabstract.service.impl;

import com.orange.myabstract.service.MaxHeightQltService;
import com.orange.myabstract.domain.MaxHeightQlt;
import com.orange.myabstract.repository.MaxHeightQltRepository;
import com.orange.myabstract.repository.search.MaxHeightQltSearchRepository;
import com.orange.myabstract.service.dto.MaxHeightQltDTO;
import com.orange.myabstract.service.mapper.MaxHeightQltMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing MaxHeightQlt.
 */
@Service
@Transactional
public class MaxHeightQltServiceImpl implements MaxHeightQltService{

    private final Logger log = LoggerFactory.getLogger(MaxHeightQltServiceImpl.class);
    
    @Inject
    private MaxHeightQltRepository maxHeightQltRepository;

    @Inject
    private MaxHeightQltMapper maxHeightQltMapper;

    @Inject
    private MaxHeightQltSearchRepository maxHeightQltSearchRepository;

    /**
     * Save a maxHeightQlt.
     *
     * @param maxHeightQltDTO the entity to save
     * @return the persisted entity
     */
    public MaxHeightQltDTO save(MaxHeightQltDTO maxHeightQltDTO) {
        log.debug("Request to save MaxHeightQlt : {}", maxHeightQltDTO);
        MaxHeightQlt maxHeightQlt = maxHeightQltMapper.maxHeightQltDTOToMaxHeightQlt(maxHeightQltDTO);
        maxHeightQlt = maxHeightQltRepository.save(maxHeightQlt);
        MaxHeightQltDTO result = maxHeightQltMapper.maxHeightQltToMaxHeightQltDTO(maxHeightQlt);
        maxHeightQltSearchRepository.save(maxHeightQlt);
        return result;
    }

    /**
     *  Get all the maxHeightQlts.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true) 
    public Page<MaxHeightQltDTO> findAll(Pageable pageable) {
        log.debug("Request to get all MaxHeightQlts");
        Page<MaxHeightQlt> result = maxHeightQltRepository.findAll(pageable);
        return result.map(maxHeightQlt -> maxHeightQltMapper.maxHeightQltToMaxHeightQltDTO(maxHeightQlt));
    }

    /**
     *  Get one maxHeightQlt by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true) 
    public MaxHeightQltDTO findOne(Long id) {
        log.debug("Request to get MaxHeightQlt : {}", id);
        MaxHeightQlt maxHeightQlt = maxHeightQltRepository.findOne(id);
        MaxHeightQltDTO maxHeightQltDTO = maxHeightQltMapper.maxHeightQltToMaxHeightQltDTO(maxHeightQlt);
        return maxHeightQltDTO;
    }

    /**
     *  Delete the  maxHeightQlt by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete MaxHeightQlt : {}", id);
        maxHeightQltRepository.delete(id);
        maxHeightQltSearchRepository.delete(id);
    }

    /**
     * Search for the maxHeightQlt corresponding to the query.
     *
     *  @param query the query of the search
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<MaxHeightQltDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of MaxHeightQlts for query {}", query);
        Page<MaxHeightQlt> result = maxHeightQltSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(maxHeightQlt -> maxHeightQltMapper.maxHeightQltToMaxHeightQltDTO(maxHeightQlt));
    }
}
