package com.orange.myabstract.service.impl;

import com.orange.myabstract.service.PlayingService;
import com.orange.myabstract.domain.Playing;
import com.orange.myabstract.repository.PlayingRepository;
import com.orange.myabstract.repository.search.PlayingSearchRepository;
import com.orange.myabstract.service.dto.PlayingDTO;
import com.orange.myabstract.service.mapper.PlayingMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing Playing.
 */
@Service
@Transactional
public class PlayingServiceImpl implements PlayingService{

    private final Logger log = LoggerFactory.getLogger(PlayingServiceImpl.class);
    
    @Inject
    private PlayingRepository playingRepository;

    @Inject
    private PlayingMapper playingMapper;

    @Inject
    private PlayingSearchRepository playingSearchRepository;

    /**
     * Save a playing.
     *
     * @param playingDTO the entity to save
     * @return the persisted entity
     */
    public PlayingDTO save(PlayingDTO playingDTO) {
        log.debug("Request to save Playing : {}", playingDTO);
        Playing playing = playingMapper.playingDTOToPlaying(playingDTO);
        playing = playingRepository.save(playing);
        PlayingDTO result = playingMapper.playingToPlayingDTO(playing);
        playingSearchRepository.save(playing);
        return result;
    }

    /**
     *  Get all the playings.
     *  
     *  @return the list of entities
     */
    @Transactional(readOnly = true) 
    public List<PlayingDTO> findAll() {
        log.debug("Request to get all Playings");
        List<PlayingDTO> result = playingRepository.findAllWithEagerRelationships().stream()
            .map(playingMapper::playingToPlayingDTO)
            .collect(Collectors.toCollection(LinkedList::new));

        return result;
    }

    /**
     *  Get one playing by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true) 
    public PlayingDTO findOne(Long id) {
        log.debug("Request to get Playing : {}", id);
        Playing playing = playingRepository.findOneWithEagerRelationships(id);
        PlayingDTO playingDTO = playingMapper.playingToPlayingDTO(playing);
        return playingDTO;
    }

    /**
     *  Delete the  playing by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Playing : {}", id);
        playingRepository.delete(id);
        playingSearchRepository.delete(id);
    }

    /**
     * Search for the playing corresponding to the query.
     *
     *  @param query the query of the search
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<PlayingDTO> search(String query) {
        log.debug("Request to search Playings for query {}", query);
        return StreamSupport
            .stream(playingSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .map(playingMapper::playingToPlayingDTO)
            .collect(Collectors.toList());
    }
}
