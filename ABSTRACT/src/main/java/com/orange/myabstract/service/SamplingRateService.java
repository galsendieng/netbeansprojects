package com.orange.myabstract.service;

import com.orange.myabstract.service.dto.SamplingRateDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.LinkedList;
import java.util.List;

/**
 * Service Interface for managing SamplingRate.
 */
public interface SamplingRateService {

    /**
     * Save a samplingRate.
     *
     * @param samplingRateDTO the entity to save
     * @return the persisted entity
     */
    SamplingRateDTO save(SamplingRateDTO samplingRateDTO);

    /**
     *  Get all the samplingRates.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<SamplingRateDTO> findAll(Pageable pageable);

    /**
     *  Get the "id" samplingRate.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    SamplingRateDTO findOne(Long id);

    /**
     *  Delete the "id" samplingRate.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Search for the samplingRate corresponding to the query.
     *
     *  @param query the query of the search
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<SamplingRateDTO> search(String query, Pageable pageable);
}
