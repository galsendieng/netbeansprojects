package com.orange.myabstract.service.impl;

import com.orange.myabstract.service.CommandeService;
import com.orange.myabstract.domain.Commande;
import com.orange.myabstract.repository.CommandeRepository;
import com.orange.myabstract.repository.search.CommandeSearchRepository;
import com.orange.myabstract.service.dto.CommandeDTO;
import com.orange.myabstract.service.mapper.CommandeMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing Commande.
 */
@Service
@Transactional
public class CommandeServiceImpl implements CommandeService{

    private final Logger log = LoggerFactory.getLogger(CommandeServiceImpl.class);
    
    @Inject
    private CommandeRepository commandeRepository;

    @Inject
    private CommandeMapper commandeMapper;

    @Inject
    private CommandeSearchRepository commandeSearchRepository;

    /**
     * Save a commande.
     *
     * @param commandeDTO the entity to save
     * @return the persisted entity
     */
    public CommandeDTO save(CommandeDTO commandeDTO) {
        log.debug("Request to save Commande : {}", commandeDTO);
        Commande commande = commandeMapper.commandeDTOToCommande(commandeDTO);
        commande = commandeRepository.save(commande);
        CommandeDTO result = commandeMapper.commandeToCommandeDTO(commande);
        commandeSearchRepository.save(commande);
        return result;
    }

    /**
     *  Get all the commandes.
     *  
     *  @return the list of entities
     */
    @Transactional(readOnly = true) 
    public List<CommandeDTO> findAll() {
        log.debug("Request to get all Commandes");
        List<CommandeDTO> result = commandeRepository.findAll().stream()
            .map(commandeMapper::commandeToCommandeDTO)
            .collect(Collectors.toCollection(LinkedList::new));

        return result;
    }

    /**
     *  Get one commande by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true) 
    public CommandeDTO findOne(Long id) {
        log.debug("Request to get Commande : {}", id);
        Commande commande = commandeRepository.findOne(id);
        CommandeDTO commandeDTO = commandeMapper.commandeToCommandeDTO(commande);
        return commandeDTO;
    }

    /**
     *  Delete the  commande by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Commande : {}", id);
        commandeRepository.delete(id);
        commandeSearchRepository.delete(id);
    }

    /**
     * Search for the commande corresponding to the query.
     *
     *  @param query the query of the search
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<CommandeDTO> search(String query) {
        log.debug("Request to search Commandes for query {}", query);
        return StreamSupport
            .stream(commandeSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .map(commandeMapper::commandeToCommandeDTO)
            .collect(Collectors.toList());
    }
}
