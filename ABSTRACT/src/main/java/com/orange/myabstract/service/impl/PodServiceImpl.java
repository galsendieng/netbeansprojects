package com.orange.myabstract.service.impl;

import com.orange.myabstract.service.PodService;
import com.orange.myabstract.domain.Pod;
import com.orange.myabstract.repository.PodRepository;
import com.orange.myabstract.repository.search.PodSearchRepository;
import com.orange.myabstract.service.dto.PodDTO;
import com.orange.myabstract.service.mapper.PodMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing Pod.
 */
@Service
@Transactional
public class PodServiceImpl implements PodService{

    private final Logger log = LoggerFactory.getLogger(PodServiceImpl.class);
    
    @Inject
    private PodRepository podRepository;

    @Inject
    private PodMapper podMapper;

    @Inject
    private PodSearchRepository podSearchRepository;

    /**
     * Save a pod.
     *
     * @param podDTO the entity to save
     * @return the persisted entity
     */
    public PodDTO save(PodDTO podDTO) {
        log.debug("Request to save Pod : {}", podDTO);
        Pod pod = podMapper.podDTOToPod(podDTO);
        pod = podRepository.save(pod);
        PodDTO result = podMapper.podToPodDTO(pod);
        podSearchRepository.save(pod);
        return result;
    }

    /**
     *  Get all the pods.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true) 
    public Page<PodDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Pods");
        Page<Pod> result = podRepository.findAll(pageable);
        return result.map(pod -> podMapper.podToPodDTO(pod));
    }

    /**
     *  Get one pod by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true) 
    public PodDTO findOne(Long id) {
        log.debug("Request to get Pod : {}", id);
        Pod pod = podRepository.findOne(id);
        PodDTO podDTO = podMapper.podToPodDTO(pod);
        return podDTO;
    }

    /**
     *  Delete the  pod by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Pod : {}", id);
        podRepository.delete(id);
        podSearchRepository.delete(id);
    }

    /**
     * Search for the pod corresponding to the query.
     *
     *  @param query the query of the search
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<PodDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Pods for query {}", query);
        Page<Pod> result = podSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(pod -> podMapper.podToPodDTO(pod));
    }
}
