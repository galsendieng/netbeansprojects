package com.orange.myabstract.service.mapper;

import com.orange.myabstract.domain.*;
import com.orange.myabstract.service.dto.CodecPrivateDataDTO;

import org.mapstruct.*;
import java.util.List;

/**
 * Mapper for the entity CodecPrivateData and its DTO CodecPrivateDataDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface CodecPrivateDataMapper {

    CodecPrivateDataDTO codecPrivateDataToCodecPrivateDataDTO(CodecPrivateData codecPrivateData);

    List<CodecPrivateDataDTO> codecPrivateDataToCodecPrivateDataDTOs(List<CodecPrivateData> codecPrivateData);

    CodecPrivateData codecPrivateDataDTOToCodecPrivateData(CodecPrivateDataDTO codecPrivateDataDTO);

    List<CodecPrivateData> codecPrivateDataDTOsToCodecPrivateData(List<CodecPrivateDataDTO> codecPrivateDataDTOs);
}
