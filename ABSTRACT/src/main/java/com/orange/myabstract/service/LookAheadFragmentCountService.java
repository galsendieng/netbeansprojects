package com.orange.myabstract.service;

import com.orange.myabstract.service.dto.LookAheadFragmentCountDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.LinkedList;
import java.util.List;

/**
 * Service Interface for managing LookAheadFragmentCount.
 */
public interface LookAheadFragmentCountService {

    /**
     * Save a lookAheadFragmentCount.
     *
     * @param lookAheadFragmentCountDTO the entity to save
     * @return the persisted entity
     */
    LookAheadFragmentCountDTO save(LookAheadFragmentCountDTO lookAheadFragmentCountDTO);

    /**
     *  Get all the lookAheadFragmentCounts.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<LookAheadFragmentCountDTO> findAll(Pageable pageable);

    /**
     *  Get the "id" lookAheadFragmentCount.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    LookAheadFragmentCountDTO findOne(Long id);

    /**
     *  Delete the "id" lookAheadFragmentCount.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Search for the lookAheadFragmentCount corresponding to the query.
     *
     *  @param query the query of the search
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<LookAheadFragmentCountDTO> search(String query, Pageable pageable);
}
