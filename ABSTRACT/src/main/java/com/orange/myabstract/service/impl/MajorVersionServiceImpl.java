package com.orange.myabstract.service.impl;

import com.orange.myabstract.service.MajorVersionService;
import com.orange.myabstract.domain.MajorVersion;
import com.orange.myabstract.repository.MajorVersionRepository;
import com.orange.myabstract.repository.search.MajorVersionSearchRepository;
import com.orange.myabstract.service.dto.MajorVersionDTO;
import com.orange.myabstract.service.mapper.MajorVersionMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing MajorVersion.
 */
@Service
@Transactional
public class MajorVersionServiceImpl implements MajorVersionService{

    private final Logger log = LoggerFactory.getLogger(MajorVersionServiceImpl.class);
    
    @Inject
    private MajorVersionRepository majorVersionRepository;

    @Inject
    private MajorVersionMapper majorVersionMapper;

    @Inject
    private MajorVersionSearchRepository majorVersionSearchRepository;

    /**
     * Save a majorVersion.
     *
     * @param majorVersionDTO the entity to save
     * @return the persisted entity
     */
    public MajorVersionDTO save(MajorVersionDTO majorVersionDTO) {
        log.debug("Request to save MajorVersion : {}", majorVersionDTO);
        MajorVersion majorVersion = majorVersionMapper.majorVersionDTOToMajorVersion(majorVersionDTO);
        majorVersion = majorVersionRepository.save(majorVersion);
        MajorVersionDTO result = majorVersionMapper.majorVersionToMajorVersionDTO(majorVersion);
        majorVersionSearchRepository.save(majorVersion);
        return result;
    }

    /**
     *  Get all the majorVersions.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true) 
    public Page<MajorVersionDTO> findAll(Pageable pageable) {
        log.debug("Request to get all MajorVersions");
        Page<MajorVersion> result = majorVersionRepository.findAll(pageable);
        return result.map(majorVersion -> majorVersionMapper.majorVersionToMajorVersionDTO(majorVersion));
    }

    /**
     *  Get one majorVersion by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true) 
    public MajorVersionDTO findOne(Long id) {
        log.debug("Request to get MajorVersion : {}", id);
        MajorVersion majorVersion = majorVersionRepository.findOne(id);
        MajorVersionDTO majorVersionDTO = majorVersionMapper.majorVersionToMajorVersionDTO(majorVersion);
        return majorVersionDTO;
    }

    /**
     *  Delete the  majorVersion by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete MajorVersion : {}", id);
        majorVersionRepository.delete(id);
        majorVersionSearchRepository.delete(id);
    }

    /**
     * Search for the majorVersion corresponding to the query.
     *
     *  @param query the query of the search
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<MajorVersionDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of MajorVersions for query {}", query);
        Page<MajorVersion> result = majorVersionSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(majorVersion -> majorVersionMapper.majorVersionToMajorVersionDTO(majorVersion));
    }
}
