package com.orange.myabstract.service;

import com.orange.myabstract.service.dto.DisplayWidthDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.LinkedList;
import java.util.List;

/**
 * Service Interface for managing DisplayWidth.
 */
public interface DisplayWidthService {

    /**
     * Save a displayWidth.
     *
     * @param displayWidthDTO the entity to save
     * @return the persisted entity
     */
    DisplayWidthDTO save(DisplayWidthDTO displayWidthDTO);

    /**
     *  Get all the displayWidths.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<DisplayWidthDTO> findAll(Pageable pageable);

    /**
     *  Get the "id" displayWidth.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    DisplayWidthDTO findOne(Long id);

    /**
     *  Delete the "id" displayWidth.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Search for the displayWidth corresponding to the query.
     *
     *  @param query the query of the search
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<DisplayWidthDTO> search(String query, Pageable pageable);
}
