package com.orange.myabstract.repository;

import com.orange.myabstract.domain.DisplayWidth;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the DisplayWidth entity.
 */
@SuppressWarnings("unused")
public interface DisplayWidthRepository extends JpaRepository<DisplayWidth,Long> {

}
