package com.orange.myabstract.repository.search;

import com.orange.myabstract.domain.MaxHeightQlt;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the MaxHeightQlt entity.
 */
public interface MaxHeightQltSearchRepository extends ElasticsearchRepository<MaxHeightQlt, Long> {
}
