package com.orange.myabstract.repository.search;

import com.orange.myabstract.domain.Playing;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the Playing entity.
 */
public interface PlayingSearchRepository extends ElasticsearchRepository<Playing, Long> {
}
