package com.orange.myabstract.repository;

import com.orange.myabstract.domain.SamplingRate;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the SamplingRate entity.
 */
@SuppressWarnings("unused")
public interface SamplingRateRepository extends JpaRepository<SamplingRate,Long> {

}
