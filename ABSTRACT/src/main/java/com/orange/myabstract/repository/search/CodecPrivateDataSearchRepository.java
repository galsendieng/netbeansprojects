package com.orange.myabstract.repository.search;

import com.orange.myabstract.domain.CodecPrivateData;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the CodecPrivateData entity.
 */
public interface CodecPrivateDataSearchRepository extends ElasticsearchRepository<CodecPrivateData, Long> {
}
