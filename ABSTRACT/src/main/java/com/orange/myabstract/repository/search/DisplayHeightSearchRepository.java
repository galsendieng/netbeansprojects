package com.orange.myabstract.repository.search;

import com.orange.myabstract.domain.DisplayHeight;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the DisplayHeight entity.
 */
public interface DisplayHeightSearchRepository extends ElasticsearchRepository<DisplayHeight, Long> {
}
