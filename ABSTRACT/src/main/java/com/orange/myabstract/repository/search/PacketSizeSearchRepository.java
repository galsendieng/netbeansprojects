package com.orange.myabstract.repository.search;

import com.orange.myabstract.domain.PacketSize;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the PacketSize entity.
 */
public interface PacketSizeSearchRepository extends ElasticsearchRepository<PacketSize, Long> {
}
