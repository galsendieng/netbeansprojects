package com.orange.myabstract.repository;

import com.orange.myabstract.domain.Channels;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Spring Data JPA repository for the Channels entity.
 */
@SuppressWarnings("unused")
public interface ChannelsRepository extends JpaRepository<Channels,Long> {

    @Query("select distinct channels from Channels channels left join fetch channels.streamers")
    List<Channels> findAllWithEagerRelationships();

    @Query("select channels from Channels channels left join fetch channels.streamers where channels.id =:id")
    Channels findOneWithEagerRelationships(@Param("id") Long id);

}
