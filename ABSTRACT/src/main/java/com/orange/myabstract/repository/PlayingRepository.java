package com.orange.myabstract.repository;

import com.orange.myabstract.domain.Playing;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Spring Data JPA repository for the Playing entity.
 */
@SuppressWarnings("unused")
public interface PlayingRepository extends JpaRepository<Playing,Long> {

    @Query("select distinct playing from Playing playing left join fetch playing.listchannels left join fetch playing.listcommands")
    List<Playing> findAllWithEagerRelationships();

    @Query("select playing from Playing playing left join fetch playing.listchannels left join fetch playing.listcommands where playing.id =:id")
    Playing findOneWithEagerRelationships(@Param("id") Long id);

}
