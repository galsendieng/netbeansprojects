package com.orange.myabstract.repository;

import com.orange.myabstract.domain.AudioTag;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the AudioTag entity.
 */
@SuppressWarnings("unused")
public interface AudioTagRepository extends JpaRepository<AudioTag,Long> {

}
