package com.orange.myabstract.repository.search;

import com.orange.myabstract.domain.BitsPerSample;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the BitsPerSample entity.
 */
public interface BitsPerSampleSearchRepository extends ElasticsearchRepository<BitsPerSample, Long> {
}
