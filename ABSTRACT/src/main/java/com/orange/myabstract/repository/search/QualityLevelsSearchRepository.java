package com.orange.myabstract.repository.search;

import com.orange.myabstract.domain.QualityLevels;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the QualityLevels entity.
 */
public interface QualityLevelsSearchRepository extends ElasticsearchRepository<QualityLevels, Long> {
}
