package com.orange.myabstract.repository.search;

import com.orange.myabstract.domain.FourCC;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the FourCC entity.
 */
public interface FourCCSearchRepository extends ElasticsearchRepository<FourCC, Long> {
}
