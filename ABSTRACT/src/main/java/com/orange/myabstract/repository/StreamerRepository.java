package com.orange.myabstract.repository;

import com.orange.myabstract.domain.Streamer;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the Streamer entity.
 */
@SuppressWarnings("unused")
public interface StreamerRepository extends JpaRepository<Streamer,Long> {

}
