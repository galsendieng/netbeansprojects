package com.orange.myabstract.repository;

import com.orange.myabstract.domain.MaxHeightQlt;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the MaxHeightQlt entity.
 */
@SuppressWarnings("unused")
public interface MaxHeightQltRepository extends JpaRepository<MaxHeightQlt,Long> {

}
