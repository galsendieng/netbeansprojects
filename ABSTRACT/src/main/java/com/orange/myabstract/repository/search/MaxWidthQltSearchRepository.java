package com.orange.myabstract.repository.search;

import com.orange.myabstract.domain.MaxWidthQlt;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the MaxWidthQlt entity.
 */
public interface MaxWidthQltSearchRepository extends ElasticsearchRepository<MaxWidthQlt, Long> {
}
