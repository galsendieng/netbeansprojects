package com.orange.myabstract.repository;

import com.orange.myabstract.domain.CodecPrivateData;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the CodecPrivateData entity.
 */
@SuppressWarnings("unused")
public interface CodecPrivateDataRepository extends JpaRepository<CodecPrivateData,Long> {

}
