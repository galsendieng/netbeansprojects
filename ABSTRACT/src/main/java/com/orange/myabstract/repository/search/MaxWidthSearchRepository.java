package com.orange.myabstract.repository.search;

import com.orange.myabstract.domain.MaxWidth;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the MaxWidth entity.
 */
public interface MaxWidthSearchRepository extends ElasticsearchRepository<MaxWidth, Long> {
}
