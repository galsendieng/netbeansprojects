package com.orange.myabstract.repository;

import com.orange.myabstract.domain.MaxHeight;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the MaxHeight entity.
 */
@SuppressWarnings("unused")
public interface MaxHeightRepository extends JpaRepository<MaxHeight,Long> {

}
