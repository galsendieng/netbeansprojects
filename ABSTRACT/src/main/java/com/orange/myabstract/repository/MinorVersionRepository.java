package com.orange.myabstract.repository;

import com.orange.myabstract.domain.MinorVersion;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the MinorVersion entity.
 */
@SuppressWarnings("unused")
public interface MinorVersionRepository extends JpaRepository<MinorVersion,Long> {

}
