package com.orange.myabstract.domain;

import io.swagger.annotations.ApiModel;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

/**
 * For SmoothStreamingMedia                                                    
 * 
 */
@ApiModel(description = "For SmoothStreamingMedia")
@Entity
@Table(name = "major_version")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "majorversion")
public class MajorVersion implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "valeur")
    private String valeur;

    @Column(name = "streamer")
    private String streamer;

    @Column(name = "date_sup")
    private ZonedDateTime dateSup;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getValeur() {
        return valeur;
    }

    public MajorVersion valeur(String valeur) {
        this.valeur = valeur;
        return this;
    }

    public void setValeur(String valeur) {
        this.valeur = valeur;
    }

    public String getStreamer() {
        return streamer;
    }

    public MajorVersion streamer(String streamer) {
        this.streamer = streamer;
        return this;
    }

    public void setStreamer(String streamer) {
        this.streamer = streamer;
    }

    public ZonedDateTime getDateSup() {
        return dateSup;
    }

    public MajorVersion dateSup(ZonedDateTime dateSup) {
        this.dateSup = dateSup;
        return this;
    }

    public void setDateSup(ZonedDateTime dateSup) {
        this.dateSup = dateSup;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        MajorVersion majorVersion = (MajorVersion) o;
        if(majorVersion.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, majorVersion.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "MajorVersion{" +
            "id=" + id +
            ", valeur='" + valeur + "'" +
            ", streamer='" + streamer + "'" +
            ", dateSup='" + dateSup + "'" +
            '}';
    }
}
