package com.orange.myabstract.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Pod.
 */
@Entity
@Table(name = "pod")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "pod")
public class Pod implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Column(name = "name", nullable = false)
    private String name;

    @NotNull
    @Column(name = "link", nullable = false)
    private String link;

    @Column(name = "description")
    private String description;

    @OneToMany(mappedBy = "lePod")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Streamer> streamers = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Pod name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLink() {
        return link;
    }

    public Pod link(String link) {
        this.link = link;
        return this;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getDescription() {
        return description;
    }

    public Pod description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<Streamer> getStreamers() {
        return streamers;
    }

    public Pod streamers(Set<Streamer> streamers) {
        this.streamers = streamers;
        return this;
    }

    public Pod addStreamer(Streamer streamer) {
        streamers.add(streamer);
        streamer.setLePod(this);
        return this;
    }

    public Pod removeStreamer(Streamer streamer) {
        streamers.remove(streamer);
        streamer.setLePod(null);
        return this;
    }

    public void setStreamers(Set<Streamer> streamers) {
        this.streamers = streamers;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Pod pod = (Pod) o;
        if(pod.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, pod.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Pod{" +
            "id=" + id +
            ", name='" + name + "'" +
            ", link='" + link + "'" +
            ", description='" + description + "'" +
            '}';
    }
}
