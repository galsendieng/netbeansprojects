package com.orange.myabstract.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

/**
 * A CorruptManifest.
 */
@Entity
@Table(name = "corrupt_manifest")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "corruptmanifest")
public class CorruptManifest implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "date_manifest")
    private ZonedDateTime dateManifest;

    @Column(name = "command")
    private String command;

    @Column(name = "profil")
    private String profil;

    @Lob
    @Column(name = "file")
    private byte[] file;

    @Column(name = "file_content_type")
    private String fileContentType;

    @Column(name = "errors")
    private String errors;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ZonedDateTime getDateManifest() {
        return dateManifest;
    }

    public CorruptManifest dateManifest(ZonedDateTime dateManifest) {
        this.dateManifest = dateManifest;
        return this;
    }

    public void setDateManifest(ZonedDateTime dateManifest) {
        this.dateManifest = dateManifest;
    }

    public String getCommand() {
        return command;
    }

    public CorruptManifest command(String command) {
        this.command = command;
        return this;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    public String getProfil() {
        return profil;
    }

    public CorruptManifest profil(String profil) {
        this.profil = profil;
        return this;
    }

    public void setProfil(String profil) {
        this.profil = profil;
    }

    public byte[] getFile() {
        return file;
    }

    public CorruptManifest file(byte[] file) {
        this.file = file;
        return this;
    }

    public void setFile(byte[] file) {
        this.file = file;
    }

    public String getFileContentType() {
        return fileContentType;
    }

    public CorruptManifest fileContentType(String fileContentType) {
        this.fileContentType = fileContentType;
        return this;
    }

    public void setFileContentType(String fileContentType) {
        this.fileContentType = fileContentType;
    }

    public String getErrors() {
        return errors;
    }

    public CorruptManifest errors(String errors) {
        this.errors = errors;
        return this;
    }

    public void setErrors(String errors) {
        this.errors = errors;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CorruptManifest corruptManifest = (CorruptManifest) o;
        if(corruptManifest.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, corruptManifest.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "CorruptManifest{" +
            "id=" + id +
            ", dateManifest='" + dateManifest + "'" +
            ", command='" + command + "'" +
            ", profil='" + profil + "'" +
            ", file='" + file + "'" +
            ", fileContentType='" + fileContentType + "'" +
            ", errors='" + errors + "'" +
            '}';
    }
}
