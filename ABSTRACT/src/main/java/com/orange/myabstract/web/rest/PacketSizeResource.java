package com.orange.myabstract.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.orange.myabstract.service.PacketSizeService;
import com.orange.myabstract.web.rest.util.HeaderUtil;
import com.orange.myabstract.web.rest.util.PaginationUtil;
import com.orange.myabstract.service.dto.PacketSizeDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing PacketSize.
 */
@RestController
@RequestMapping("/api")
public class PacketSizeResource {

    private final Logger log = LoggerFactory.getLogger(PacketSizeResource.class);
        
    @Inject
    private PacketSizeService packetSizeService;

    /**
     * POST  /packet-sizes : Create a new packetSize.
     *
     * @param packetSizeDTO the packetSizeDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new packetSizeDTO, or with status 400 (Bad Request) if the packetSize has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/packet-sizes")
    @Timed
    public ResponseEntity<PacketSizeDTO> createPacketSize(@RequestBody PacketSizeDTO packetSizeDTO) throws URISyntaxException {
        log.debug("REST request to save PacketSize : {}", packetSizeDTO);
        if (packetSizeDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("packetSize", "idexists", "A new packetSize cannot already have an ID")).body(null);
        }
        PacketSizeDTO result = packetSizeService.save(packetSizeDTO);
        return ResponseEntity.created(new URI("/api/packet-sizes/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("packetSize", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /packet-sizes : Updates an existing packetSize.
     *
     * @param packetSizeDTO the packetSizeDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated packetSizeDTO,
     * or with status 400 (Bad Request) if the packetSizeDTO is not valid,
     * or with status 500 (Internal Server Error) if the packetSizeDTO couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/packet-sizes")
    @Timed
    public ResponseEntity<PacketSizeDTO> updatePacketSize(@RequestBody PacketSizeDTO packetSizeDTO) throws URISyntaxException {
        log.debug("REST request to update PacketSize : {}", packetSizeDTO);
        if (packetSizeDTO.getId() == null) {
            return createPacketSize(packetSizeDTO);
        }
        PacketSizeDTO result = packetSizeService.save(packetSizeDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("packetSize", packetSizeDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /packet-sizes : get all the packetSizes.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of packetSizes in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @GetMapping("/packet-sizes")
    @Timed
    public ResponseEntity<List<PacketSizeDTO>> getAllPacketSizes(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of PacketSizes");
        Page<PacketSizeDTO> page = packetSizeService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/packet-sizes");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /packet-sizes/:id : get the "id" packetSize.
     *
     * @param id the id of the packetSizeDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the packetSizeDTO, or with status 404 (Not Found)
     */
    @GetMapping("/packet-sizes/{id}")
    @Timed
    public ResponseEntity<PacketSizeDTO> getPacketSize(@PathVariable Long id) {
        log.debug("REST request to get PacketSize : {}", id);
        PacketSizeDTO packetSizeDTO = packetSizeService.findOne(id);
        return Optional.ofNullable(packetSizeDTO)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /packet-sizes/:id : delete the "id" packetSize.
     *
     * @param id the id of the packetSizeDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/packet-sizes/{id}")
    @Timed
    public ResponseEntity<Void> deletePacketSize(@PathVariable Long id) {
        log.debug("REST request to delete PacketSize : {}", id);
        packetSizeService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("packetSize", id.toString())).build();
    }

    /**
     * SEARCH  /_search/packet-sizes?query=:query : search for the packetSize corresponding
     * to the query.
     *
     * @param query the query of the packetSize search 
     * @param pageable the pagination information
     * @return the result of the search
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @GetMapping("/_search/packet-sizes")
    @Timed
    public ResponseEntity<List<PacketSizeDTO>> searchPacketSizes(@RequestParam String query, Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to search for a page of PacketSizes for query {}", query);
        Page<PacketSizeDTO> page = packetSizeService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/packet-sizes");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
