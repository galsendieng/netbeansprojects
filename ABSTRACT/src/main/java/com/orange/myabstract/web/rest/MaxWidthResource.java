package com.orange.myabstract.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.orange.myabstract.service.MaxWidthService;
import com.orange.myabstract.web.rest.util.HeaderUtil;
import com.orange.myabstract.web.rest.util.PaginationUtil;
import com.orange.myabstract.service.dto.MaxWidthDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing MaxWidth.
 */
@RestController
@RequestMapping("/api")
public class MaxWidthResource {

    private final Logger log = LoggerFactory.getLogger(MaxWidthResource.class);
        
    @Inject
    private MaxWidthService maxWidthService;

    /**
     * POST  /max-widths : Create a new maxWidth.
     *
     * @param maxWidthDTO the maxWidthDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new maxWidthDTO, or with status 400 (Bad Request) if the maxWidth has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/max-widths")
    @Timed
    public ResponseEntity<MaxWidthDTO> createMaxWidth(@RequestBody MaxWidthDTO maxWidthDTO) throws URISyntaxException {
        log.debug("REST request to save MaxWidth : {}", maxWidthDTO);
        if (maxWidthDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("maxWidth", "idexists", "A new maxWidth cannot already have an ID")).body(null);
        }
        MaxWidthDTO result = maxWidthService.save(maxWidthDTO);
        return ResponseEntity.created(new URI("/api/max-widths/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("maxWidth", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /max-widths : Updates an existing maxWidth.
     *
     * @param maxWidthDTO the maxWidthDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated maxWidthDTO,
     * or with status 400 (Bad Request) if the maxWidthDTO is not valid,
     * or with status 500 (Internal Server Error) if the maxWidthDTO couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/max-widths")
    @Timed
    public ResponseEntity<MaxWidthDTO> updateMaxWidth(@RequestBody MaxWidthDTO maxWidthDTO) throws URISyntaxException {
        log.debug("REST request to update MaxWidth : {}", maxWidthDTO);
        if (maxWidthDTO.getId() == null) {
            return createMaxWidth(maxWidthDTO);
        }
        MaxWidthDTO result = maxWidthService.save(maxWidthDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("maxWidth", maxWidthDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /max-widths : get all the maxWidths.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of maxWidths in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @GetMapping("/max-widths")
    @Timed
    public ResponseEntity<List<MaxWidthDTO>> getAllMaxWidths(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of MaxWidths");
        Page<MaxWidthDTO> page = maxWidthService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/max-widths");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /max-widths/:id : get the "id" maxWidth.
     *
     * @param id the id of the maxWidthDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the maxWidthDTO, or with status 404 (Not Found)
     */
    @GetMapping("/max-widths/{id}")
    @Timed
    public ResponseEntity<MaxWidthDTO> getMaxWidth(@PathVariable Long id) {
        log.debug("REST request to get MaxWidth : {}", id);
        MaxWidthDTO maxWidthDTO = maxWidthService.findOne(id);
        return Optional.ofNullable(maxWidthDTO)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /max-widths/:id : delete the "id" maxWidth.
     *
     * @param id the id of the maxWidthDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/max-widths/{id}")
    @Timed
    public ResponseEntity<Void> deleteMaxWidth(@PathVariable Long id) {
        log.debug("REST request to delete MaxWidth : {}", id);
        maxWidthService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("maxWidth", id.toString())).build();
    }

    /**
     * SEARCH  /_search/max-widths?query=:query : search for the maxWidth corresponding
     * to the query.
     *
     * @param query the query of the maxWidth search 
     * @param pageable the pagination information
     * @return the result of the search
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @GetMapping("/_search/max-widths")
    @Timed
    public ResponseEntity<List<MaxWidthDTO>> searchMaxWidths(@RequestParam String query, Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to search for a page of MaxWidths for query {}", query);
        Page<MaxWidthDTO> page = maxWidthService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/max-widths");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
