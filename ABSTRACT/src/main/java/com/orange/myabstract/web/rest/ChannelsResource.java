package com.orange.myabstract.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.orange.myabstract.service.ChannelsService;
import com.orange.myabstract.web.rest.util.HeaderUtil;
import com.orange.myabstract.web.rest.util.PaginationUtil;
import com.orange.myabstract.service.dto.ChannelsDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Channels.
 */
@RestController
@RequestMapping("/api")
public class ChannelsResource {

    private final Logger log = LoggerFactory.getLogger(ChannelsResource.class);

    @Inject
    private ChannelsService channelsService;

    /**
     * POST  /channels : Create a new channels.
     *
     * @param channelsDTO the channelsDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new channelsDTO, or with status 400 (Bad Request) if the channels has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/channels")
    @Timed
    public ResponseEntity<ChannelsDTO> createChannels(@Valid @RequestBody ChannelsDTO channelsDTO) throws URISyntaxException {
        log.debug("REST request to save Channels : {}", channelsDTO);
        if (channelsDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("channels", "idexists", "A new channels cannot already have an ID")).body(null);
        }
        ChannelsDTO result = channelsService.save(channelsDTO);
        return ResponseEntity.created(new URI("/api/channels/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("channels", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /channels : Updates an existing channels.
     *
     * @param channelsDTO the channelsDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated channelsDTO,
     * or with status 400 (Bad Request) if the channelsDTO is not valid,
     * or with status 500 (Internal Server Error) if the channelsDTO couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/channels")
    @Timed
    public ResponseEntity<ChannelsDTO> updateChannels(@Valid @RequestBody ChannelsDTO channelsDTO) throws URISyntaxException {
        log.debug("REST request to update Channels : {}", channelsDTO);
        if (channelsDTO.getId() == null) {
            return createChannels(channelsDTO);
        }
        ChannelsDTO result = channelsService.save(channelsDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("channels", channelsDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /channels : get all the channels.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of channels in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @GetMapping("/channels")
    @Timed
    public ResponseEntity<List<ChannelsDTO>> getAllChannels(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of Channels");
        Page<ChannelsDTO> page = channelsService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/channels");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /channels/:id : get the "id" channels.
     *
     * @param id the id of the channelsDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the channelsDTO, or with status 404 (Not Found)
     */
    @GetMapping("/channels/{id}")
    @Timed
    public ResponseEntity<ChannelsDTO> getChannels(@PathVariable Long id) {
        log.debug("REST request to get Channels : {}", id);
        ChannelsDTO channelsDTO = channelsService.findOne(id);
        return Optional.ofNullable(channelsDTO)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /channels/:id : delete the "id" channels.
     *
     * @param id the id of the channelsDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/channels/{id}")
    @Timed
    public ResponseEntity<Void> deleteChannels(@PathVariable Long id) {
        log.debug("REST request to delete Channels : {}", id);
        channelsService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("channels", id.toString())).build();
    }

    /**
     * SEARCH  /_search/channels?query=:query : search for the channels corresponding
     * to the query.
     *
     * @param query the query of the channels search
     * @param pageable the pagination information
     * @return the result of the search
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @GetMapping("/_search/channels")
    @Timed
    public ResponseEntity<List<ChannelsDTO>> searchChannels(@RequestParam String query, Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to search for a page of Channels for query {}", query);
        Page<ChannelsDTO> page = channelsService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/channels");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
