package com.orange.myabstract.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.orange.myabstract.service.MinorVersionService;
import com.orange.myabstract.web.rest.util.HeaderUtil;
import com.orange.myabstract.web.rest.util.PaginationUtil;
import com.orange.myabstract.service.dto.MinorVersionDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing MinorVersion.
 */
@RestController
@RequestMapping("/api")
public class MinorVersionResource {

    private final Logger log = LoggerFactory.getLogger(MinorVersionResource.class);
        
    @Inject
    private MinorVersionService minorVersionService;

    /**
     * POST  /minor-versions : Create a new minorVersion.
     *
     * @param minorVersionDTO the minorVersionDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new minorVersionDTO, or with status 400 (Bad Request) if the minorVersion has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/minor-versions")
    @Timed
    public ResponseEntity<MinorVersionDTO> createMinorVersion(@RequestBody MinorVersionDTO minorVersionDTO) throws URISyntaxException {
        log.debug("REST request to save MinorVersion : {}", minorVersionDTO);
        if (minorVersionDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("minorVersion", "idexists", "A new minorVersion cannot already have an ID")).body(null);
        }
        MinorVersionDTO result = minorVersionService.save(minorVersionDTO);
        return ResponseEntity.created(new URI("/api/minor-versions/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("minorVersion", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /minor-versions : Updates an existing minorVersion.
     *
     * @param minorVersionDTO the minorVersionDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated minorVersionDTO,
     * or with status 400 (Bad Request) if the minorVersionDTO is not valid,
     * or with status 500 (Internal Server Error) if the minorVersionDTO couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/minor-versions")
    @Timed
    public ResponseEntity<MinorVersionDTO> updateMinorVersion(@RequestBody MinorVersionDTO minorVersionDTO) throws URISyntaxException {
        log.debug("REST request to update MinorVersion : {}", minorVersionDTO);
        if (minorVersionDTO.getId() == null) {
            return createMinorVersion(minorVersionDTO);
        }
        MinorVersionDTO result = minorVersionService.save(minorVersionDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("minorVersion", minorVersionDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /minor-versions : get all the minorVersions.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of minorVersions in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @GetMapping("/minor-versions")
    @Timed
    public ResponseEntity<List<MinorVersionDTO>> getAllMinorVersions(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of MinorVersions");
        Page<MinorVersionDTO> page = minorVersionService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/minor-versions");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /minor-versions/:id : get the "id" minorVersion.
     *
     * @param id the id of the minorVersionDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the minorVersionDTO, or with status 404 (Not Found)
     */
    @GetMapping("/minor-versions/{id}")
    @Timed
    public ResponseEntity<MinorVersionDTO> getMinorVersion(@PathVariable Long id) {
        log.debug("REST request to get MinorVersion : {}", id);
        MinorVersionDTO minorVersionDTO = minorVersionService.findOne(id);
        return Optional.ofNullable(minorVersionDTO)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /minor-versions/:id : delete the "id" minorVersion.
     *
     * @param id the id of the minorVersionDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/minor-versions/{id}")
    @Timed
    public ResponseEntity<Void> deleteMinorVersion(@PathVariable Long id) {
        log.debug("REST request to delete MinorVersion : {}", id);
        minorVersionService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("minorVersion", id.toString())).build();
    }

    /**
     * SEARCH  /_search/minor-versions?query=:query : search for the minorVersion corresponding
     * to the query.
     *
     * @param query the query of the minorVersion search 
     * @param pageable the pagination information
     * @return the result of the search
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @GetMapping("/_search/minor-versions")
    @Timed
    public ResponseEntity<List<MinorVersionDTO>> searchMinorVersions(@RequestParam String query, Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to search for a page of MinorVersions for query {}", query);
        Page<MinorVersionDTO> page = minorVersionService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/minor-versions");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
