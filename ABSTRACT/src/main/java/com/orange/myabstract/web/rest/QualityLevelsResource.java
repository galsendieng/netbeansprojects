package com.orange.myabstract.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.orange.myabstract.service.QualityLevelsService;
import com.orange.myabstract.web.rest.util.HeaderUtil;
import com.orange.myabstract.web.rest.util.PaginationUtil;
import com.orange.myabstract.service.dto.QualityLevelsDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing QualityLevels.
 */
@RestController
@RequestMapping("/api")
public class QualityLevelsResource {

    private final Logger log = LoggerFactory.getLogger(QualityLevelsResource.class);
        
    @Inject
    private QualityLevelsService qualityLevelsService;

    /**
     * POST  /quality-levels : Create a new qualityLevels.
     *
     * @param qualityLevelsDTO the qualityLevelsDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new qualityLevelsDTO, or with status 400 (Bad Request) if the qualityLevels has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/quality-levels")
    @Timed
    public ResponseEntity<QualityLevelsDTO> createQualityLevels(@RequestBody QualityLevelsDTO qualityLevelsDTO) throws URISyntaxException {
        log.debug("REST request to save QualityLevels : {}", qualityLevelsDTO);
        if (qualityLevelsDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("qualityLevels", "idexists", "A new qualityLevels cannot already have an ID")).body(null);
        }
        QualityLevelsDTO result = qualityLevelsService.save(qualityLevelsDTO);
        return ResponseEntity.created(new URI("/api/quality-levels/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("qualityLevels", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /quality-levels : Updates an existing qualityLevels.
     *
     * @param qualityLevelsDTO the qualityLevelsDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated qualityLevelsDTO,
     * or with status 400 (Bad Request) if the qualityLevelsDTO is not valid,
     * or with status 500 (Internal Server Error) if the qualityLevelsDTO couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/quality-levels")
    @Timed
    public ResponseEntity<QualityLevelsDTO> updateQualityLevels(@RequestBody QualityLevelsDTO qualityLevelsDTO) throws URISyntaxException {
        log.debug("REST request to update QualityLevels : {}", qualityLevelsDTO);
        if (qualityLevelsDTO.getId() == null) {
            return createQualityLevels(qualityLevelsDTO);
        }
        QualityLevelsDTO result = qualityLevelsService.save(qualityLevelsDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("qualityLevels", qualityLevelsDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /quality-levels : get all the qualityLevels.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of qualityLevels in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @GetMapping("/quality-levels")
    @Timed
    public ResponseEntity<List<QualityLevelsDTO>> getAllQualityLevels(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of QualityLevels");
        Page<QualityLevelsDTO> page = qualityLevelsService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/quality-levels");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /quality-levels/:id : get the "id" qualityLevels.
     *
     * @param id the id of the qualityLevelsDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the qualityLevelsDTO, or with status 404 (Not Found)
     */
    @GetMapping("/quality-levels/{id}")
    @Timed
    public ResponseEntity<QualityLevelsDTO> getQualityLevels(@PathVariable Long id) {
        log.debug("REST request to get QualityLevels : {}", id);
        QualityLevelsDTO qualityLevelsDTO = qualityLevelsService.findOne(id);
        return Optional.ofNullable(qualityLevelsDTO)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /quality-levels/:id : delete the "id" qualityLevels.
     *
     * @param id the id of the qualityLevelsDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/quality-levels/{id}")
    @Timed
    public ResponseEntity<Void> deleteQualityLevels(@PathVariable Long id) {
        log.debug("REST request to delete QualityLevels : {}", id);
        qualityLevelsService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("qualityLevels", id.toString())).build();
    }

    /**
     * SEARCH  /_search/quality-levels?query=:query : search for the qualityLevels corresponding
     * to the query.
     *
     * @param query the query of the qualityLevels search 
     * @param pageable the pagination information
     * @return the result of the search
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @GetMapping("/_search/quality-levels")
    @Timed
    public ResponseEntity<List<QualityLevelsDTO>> searchQualityLevels(@RequestParam String query, Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to search for a page of QualityLevels for query {}", query);
        Page<QualityLevelsDTO> page = qualityLevelsService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/quality-levels");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
