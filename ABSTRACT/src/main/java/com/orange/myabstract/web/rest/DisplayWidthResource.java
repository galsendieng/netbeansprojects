package com.orange.myabstract.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.orange.myabstract.service.DisplayWidthService;
import com.orange.myabstract.web.rest.util.HeaderUtil;
import com.orange.myabstract.web.rest.util.PaginationUtil;
import com.orange.myabstract.service.dto.DisplayWidthDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing DisplayWidth.
 */
@RestController
@RequestMapping("/api")
public class DisplayWidthResource {

    private final Logger log = LoggerFactory.getLogger(DisplayWidthResource.class);
        
    @Inject
    private DisplayWidthService displayWidthService;

    /**
     * POST  /display-widths : Create a new displayWidth.
     *
     * @param displayWidthDTO the displayWidthDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new displayWidthDTO, or with status 400 (Bad Request) if the displayWidth has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/display-widths")
    @Timed
    public ResponseEntity<DisplayWidthDTO> createDisplayWidth(@RequestBody DisplayWidthDTO displayWidthDTO) throws URISyntaxException {
        log.debug("REST request to save DisplayWidth : {}", displayWidthDTO);
        if (displayWidthDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("displayWidth", "idexists", "A new displayWidth cannot already have an ID")).body(null);
        }
        DisplayWidthDTO result = displayWidthService.save(displayWidthDTO);
        return ResponseEntity.created(new URI("/api/display-widths/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("displayWidth", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /display-widths : Updates an existing displayWidth.
     *
     * @param displayWidthDTO the displayWidthDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated displayWidthDTO,
     * or with status 400 (Bad Request) if the displayWidthDTO is not valid,
     * or with status 500 (Internal Server Error) if the displayWidthDTO couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/display-widths")
    @Timed
    public ResponseEntity<DisplayWidthDTO> updateDisplayWidth(@RequestBody DisplayWidthDTO displayWidthDTO) throws URISyntaxException {
        log.debug("REST request to update DisplayWidth : {}", displayWidthDTO);
        if (displayWidthDTO.getId() == null) {
            return createDisplayWidth(displayWidthDTO);
        }
        DisplayWidthDTO result = displayWidthService.save(displayWidthDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("displayWidth", displayWidthDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /display-widths : get all the displayWidths.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of displayWidths in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @GetMapping("/display-widths")
    @Timed
    public ResponseEntity<List<DisplayWidthDTO>> getAllDisplayWidths(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of DisplayWidths");
        Page<DisplayWidthDTO> page = displayWidthService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/display-widths");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /display-widths/:id : get the "id" displayWidth.
     *
     * @param id the id of the displayWidthDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the displayWidthDTO, or with status 404 (Not Found)
     */
    @GetMapping("/display-widths/{id}")
    @Timed
    public ResponseEntity<DisplayWidthDTO> getDisplayWidth(@PathVariable Long id) {
        log.debug("REST request to get DisplayWidth : {}", id);
        DisplayWidthDTO displayWidthDTO = displayWidthService.findOne(id);
        return Optional.ofNullable(displayWidthDTO)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /display-widths/:id : delete the "id" displayWidth.
     *
     * @param id the id of the displayWidthDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/display-widths/{id}")
    @Timed
    public ResponseEntity<Void> deleteDisplayWidth(@PathVariable Long id) {
        log.debug("REST request to delete DisplayWidth : {}", id);
        displayWidthService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("displayWidth", id.toString())).build();
    }

    /**
     * SEARCH  /_search/display-widths?query=:query : search for the displayWidth corresponding
     * to the query.
     *
     * @param query the query of the displayWidth search 
     * @param pageable the pagination information
     * @return the result of the search
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @GetMapping("/_search/display-widths")
    @Timed
    public ResponseEntity<List<DisplayWidthDTO>> searchDisplayWidths(@RequestParam String query, Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to search for a page of DisplayWidths for query {}", query);
        Page<DisplayWidthDTO> page = displayWidthService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/display-widths");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
