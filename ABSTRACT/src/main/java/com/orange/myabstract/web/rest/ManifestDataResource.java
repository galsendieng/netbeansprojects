package com.orange.myabstract.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.orange.myabstract.service.ManifestDataService;
import com.orange.myabstract.web.rest.util.HeaderUtil;
import com.orange.myabstract.web.rest.util.PaginationUtil;
import com.orange.myabstract.service.dto.ManifestDataDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing ManifestData.
 */
@RestController
@RequestMapping("/api")
public class ManifestDataResource {

    private final Logger log = LoggerFactory.getLogger(ManifestDataResource.class);
        
    @Inject
    private ManifestDataService manifestDataService;

    /**
     * POST  /manifest-data : Create a new manifestData.
     *
     * @param manifestDataDTO the manifestDataDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new manifestDataDTO, or with status 400 (Bad Request) if the manifestData has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/manifest-data")
    @Timed
    public ResponseEntity<ManifestDataDTO> createManifestData(@Valid @RequestBody ManifestDataDTO manifestDataDTO) throws URISyntaxException {
        log.debug("REST request to save ManifestData : {}", manifestDataDTO);
        if (manifestDataDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("manifestData", "idexists", "A new manifestData cannot already have an ID")).body(null);
        }
        ManifestDataDTO result = manifestDataService.save(manifestDataDTO);
        return ResponseEntity.created(new URI("/api/manifest-data/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("manifestData", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /manifest-data : Updates an existing manifestData.
     *
     * @param manifestDataDTO the manifestDataDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated manifestDataDTO,
     * or with status 400 (Bad Request) if the manifestDataDTO is not valid,
     * or with status 500 (Internal Server Error) if the manifestDataDTO couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/manifest-data")
    @Timed
    public ResponseEntity<ManifestDataDTO> updateManifestData(@Valid @RequestBody ManifestDataDTO manifestDataDTO) throws URISyntaxException {
        log.debug("REST request to update ManifestData : {}", manifestDataDTO);
        if (manifestDataDTO.getId() == null) {
            return createManifestData(manifestDataDTO);
        }
        ManifestDataDTO result = manifestDataService.save(manifestDataDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("manifestData", manifestDataDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /manifest-data : get all the manifestData.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of manifestData in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @GetMapping("/manifest-data")
    @Timed
    public ResponseEntity<List<ManifestDataDTO>> getAllManifestData(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of ManifestData");
        Page<ManifestDataDTO> page = manifestDataService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/manifest-data");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /manifest-data/:id : get the "id" manifestData.
     *
     * @param id the id of the manifestDataDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the manifestDataDTO, or with status 404 (Not Found)
     */
    @GetMapping("/manifest-data/{id}")
    @Timed
    public ResponseEntity<ManifestDataDTO> getManifestData(@PathVariable Long id) {
        log.debug("REST request to get ManifestData : {}", id);
        ManifestDataDTO manifestDataDTO = manifestDataService.findOne(id);
        return Optional.ofNullable(manifestDataDTO)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /manifest-data/:id : delete the "id" manifestData.
     *
     * @param id the id of the manifestDataDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/manifest-data/{id}")
    @Timed
    public ResponseEntity<Void> deleteManifestData(@PathVariable Long id) {
        log.debug("REST request to delete ManifestData : {}", id);
        manifestDataService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("manifestData", id.toString())).build();
    }

    /**
     * SEARCH  /_search/manifest-data?query=:query : search for the manifestData corresponding
     * to the query.
     *
     * @param query the query of the manifestData search 
     * @param pageable the pagination information
     * @return the result of the search
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @GetMapping("/_search/manifest-data")
    @Timed
    public ResponseEntity<List<ManifestDataDTO>> searchManifestData(@RequestParam String query, Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to search for a page of ManifestData for query {}", query);
        Page<ManifestDataDTO> page = manifestDataService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/manifest-data");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
