package com.orange.myabstract.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.orange.myabstract.service.MaxHeightService;
import com.orange.myabstract.web.rest.util.HeaderUtil;
import com.orange.myabstract.web.rest.util.PaginationUtil;
import com.orange.myabstract.service.dto.MaxHeightDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing MaxHeight.
 */
@RestController
@RequestMapping("/api")
public class MaxHeightResource {

    private final Logger log = LoggerFactory.getLogger(MaxHeightResource.class);
        
    @Inject
    private MaxHeightService maxHeightService;

    /**
     * POST  /max-heights : Create a new maxHeight.
     *
     * @param maxHeightDTO the maxHeightDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new maxHeightDTO, or with status 400 (Bad Request) if the maxHeight has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/max-heights")
    @Timed
    public ResponseEntity<MaxHeightDTO> createMaxHeight(@RequestBody MaxHeightDTO maxHeightDTO) throws URISyntaxException {
        log.debug("REST request to save MaxHeight : {}", maxHeightDTO);
        if (maxHeightDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("maxHeight", "idexists", "A new maxHeight cannot already have an ID")).body(null);
        }
        MaxHeightDTO result = maxHeightService.save(maxHeightDTO);
        return ResponseEntity.created(new URI("/api/max-heights/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("maxHeight", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /max-heights : Updates an existing maxHeight.
     *
     * @param maxHeightDTO the maxHeightDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated maxHeightDTO,
     * or with status 400 (Bad Request) if the maxHeightDTO is not valid,
     * or with status 500 (Internal Server Error) if the maxHeightDTO couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/max-heights")
    @Timed
    public ResponseEntity<MaxHeightDTO> updateMaxHeight(@RequestBody MaxHeightDTO maxHeightDTO) throws URISyntaxException {
        log.debug("REST request to update MaxHeight : {}", maxHeightDTO);
        if (maxHeightDTO.getId() == null) {
            return createMaxHeight(maxHeightDTO);
        }
        MaxHeightDTO result = maxHeightService.save(maxHeightDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("maxHeight", maxHeightDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /max-heights : get all the maxHeights.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of maxHeights in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @GetMapping("/max-heights")
    @Timed
    public ResponseEntity<List<MaxHeightDTO>> getAllMaxHeights(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of MaxHeights");
        Page<MaxHeightDTO> page = maxHeightService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/max-heights");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /max-heights/:id : get the "id" maxHeight.
     *
     * @param id the id of the maxHeightDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the maxHeightDTO, or with status 404 (Not Found)
     */
    @GetMapping("/max-heights/{id}")
    @Timed
    public ResponseEntity<MaxHeightDTO> getMaxHeight(@PathVariable Long id) {
        log.debug("REST request to get MaxHeight : {}", id);
        MaxHeightDTO maxHeightDTO = maxHeightService.findOne(id);
        return Optional.ofNullable(maxHeightDTO)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /max-heights/:id : delete the "id" maxHeight.
     *
     * @param id the id of the maxHeightDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/max-heights/{id}")
    @Timed
    public ResponseEntity<Void> deleteMaxHeight(@PathVariable Long id) {
        log.debug("REST request to delete MaxHeight : {}", id);
        maxHeightService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("maxHeight", id.toString())).build();
    }

    /**
     * SEARCH  /_search/max-heights?query=:query : search for the maxHeight corresponding
     * to the query.
     *
     * @param query the query of the maxHeight search 
     * @param pageable the pagination information
     * @return the result of the search
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @GetMapping("/_search/max-heights")
    @Timed
    public ResponseEntity<List<MaxHeightDTO>> searchMaxHeights(@RequestParam String query, Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to search for a page of MaxHeights for query {}", query);
        Page<MaxHeightDTO> page = maxHeightService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/max-heights");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
