package com.orange.myabstract.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.orange.myabstract.service.DisplayHeightService;
import com.orange.myabstract.web.rest.util.HeaderUtil;
import com.orange.myabstract.web.rest.util.PaginationUtil;
import com.orange.myabstract.service.dto.DisplayHeightDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing DisplayHeight.
 */
@RestController
@RequestMapping("/api")
public class DisplayHeightResource {

    private final Logger log = LoggerFactory.getLogger(DisplayHeightResource.class);
        
    @Inject
    private DisplayHeightService displayHeightService;

    /**
     * POST  /display-heights : Create a new displayHeight.
     *
     * @param displayHeightDTO the displayHeightDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new displayHeightDTO, or with status 400 (Bad Request) if the displayHeight has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/display-heights")
    @Timed
    public ResponseEntity<DisplayHeightDTO> createDisplayHeight(@RequestBody DisplayHeightDTO displayHeightDTO) throws URISyntaxException {
        log.debug("REST request to save DisplayHeight : {}", displayHeightDTO);
        if (displayHeightDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("displayHeight", "idexists", "A new displayHeight cannot already have an ID")).body(null);
        }
        DisplayHeightDTO result = displayHeightService.save(displayHeightDTO);
        return ResponseEntity.created(new URI("/api/display-heights/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("displayHeight", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /display-heights : Updates an existing displayHeight.
     *
     * @param displayHeightDTO the displayHeightDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated displayHeightDTO,
     * or with status 400 (Bad Request) if the displayHeightDTO is not valid,
     * or with status 500 (Internal Server Error) if the displayHeightDTO couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/display-heights")
    @Timed
    public ResponseEntity<DisplayHeightDTO> updateDisplayHeight(@RequestBody DisplayHeightDTO displayHeightDTO) throws URISyntaxException {
        log.debug("REST request to update DisplayHeight : {}", displayHeightDTO);
        if (displayHeightDTO.getId() == null) {
            return createDisplayHeight(displayHeightDTO);
        }
        DisplayHeightDTO result = displayHeightService.save(displayHeightDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("displayHeight", displayHeightDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /display-heights : get all the displayHeights.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of displayHeights in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @GetMapping("/display-heights")
    @Timed
    public ResponseEntity<List<DisplayHeightDTO>> getAllDisplayHeights(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of DisplayHeights");
        Page<DisplayHeightDTO> page = displayHeightService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/display-heights");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /display-heights/:id : get the "id" displayHeight.
     *
     * @param id the id of the displayHeightDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the displayHeightDTO, or with status 404 (Not Found)
     */
    @GetMapping("/display-heights/{id}")
    @Timed
    public ResponseEntity<DisplayHeightDTO> getDisplayHeight(@PathVariable Long id) {
        log.debug("REST request to get DisplayHeight : {}", id);
        DisplayHeightDTO displayHeightDTO = displayHeightService.findOne(id);
        return Optional.ofNullable(displayHeightDTO)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /display-heights/:id : delete the "id" displayHeight.
     *
     * @param id the id of the displayHeightDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/display-heights/{id}")
    @Timed
    public ResponseEntity<Void> deleteDisplayHeight(@PathVariable Long id) {
        log.debug("REST request to delete DisplayHeight : {}", id);
        displayHeightService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("displayHeight", id.toString())).build();
    }

    /**
     * SEARCH  /_search/display-heights?query=:query : search for the displayHeight corresponding
     * to the query.
     *
     * @param query the query of the displayHeight search 
     * @param pageable the pagination information
     * @return the result of the search
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @GetMapping("/_search/display-heights")
    @Timed
    public ResponseEntity<List<DisplayHeightDTO>> searchDisplayHeights(@RequestParam String query, Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to search for a page of DisplayHeights for query {}", query);
        Page<DisplayHeightDTO> page = displayHeightService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/display-heights");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
