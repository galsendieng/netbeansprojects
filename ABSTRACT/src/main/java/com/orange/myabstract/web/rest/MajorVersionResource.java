package com.orange.myabstract.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.orange.myabstract.service.MajorVersionService;
import com.orange.myabstract.web.rest.util.HeaderUtil;
import com.orange.myabstract.web.rest.util.PaginationUtil;
import com.orange.myabstract.service.dto.MajorVersionDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing MajorVersion.
 */
@RestController
@RequestMapping("/api")
public class MajorVersionResource {

    private final Logger log = LoggerFactory.getLogger(MajorVersionResource.class);
        
    @Inject
    private MajorVersionService majorVersionService;

    /**
     * POST  /major-versions : Create a new majorVersion.
     *
     * @param majorVersionDTO the majorVersionDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new majorVersionDTO, or with status 400 (Bad Request) if the majorVersion has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/major-versions")
    @Timed
    public ResponseEntity<MajorVersionDTO> createMajorVersion(@RequestBody MajorVersionDTO majorVersionDTO) throws URISyntaxException {
        log.debug("REST request to save MajorVersion : {}", majorVersionDTO);
        if (majorVersionDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("majorVersion", "idexists", "A new majorVersion cannot already have an ID")).body(null);
        }
        MajorVersionDTO result = majorVersionService.save(majorVersionDTO);
        return ResponseEntity.created(new URI("/api/major-versions/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("majorVersion", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /major-versions : Updates an existing majorVersion.
     *
     * @param majorVersionDTO the majorVersionDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated majorVersionDTO,
     * or with status 400 (Bad Request) if the majorVersionDTO is not valid,
     * or with status 500 (Internal Server Error) if the majorVersionDTO couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/major-versions")
    @Timed
    public ResponseEntity<MajorVersionDTO> updateMajorVersion(@RequestBody MajorVersionDTO majorVersionDTO) throws URISyntaxException {
        log.debug("REST request to update MajorVersion : {}", majorVersionDTO);
        if (majorVersionDTO.getId() == null) {
            return createMajorVersion(majorVersionDTO);
        }
        MajorVersionDTO result = majorVersionService.save(majorVersionDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("majorVersion", majorVersionDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /major-versions : get all the majorVersions.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of majorVersions in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @GetMapping("/major-versions")
    @Timed
    public ResponseEntity<List<MajorVersionDTO>> getAllMajorVersions(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of MajorVersions");
        Page<MajorVersionDTO> page = majorVersionService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/major-versions");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /major-versions/:id : get the "id" majorVersion.
     *
     * @param id the id of the majorVersionDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the majorVersionDTO, or with status 404 (Not Found)
     */
    @GetMapping("/major-versions/{id}")
    @Timed
    public ResponseEntity<MajorVersionDTO> getMajorVersion(@PathVariable Long id) {
        log.debug("REST request to get MajorVersion : {}", id);
        MajorVersionDTO majorVersionDTO = majorVersionService.findOne(id);
        return Optional.ofNullable(majorVersionDTO)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /major-versions/:id : delete the "id" majorVersion.
     *
     * @param id the id of the majorVersionDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/major-versions/{id}")
    @Timed
    public ResponseEntity<Void> deleteMajorVersion(@PathVariable Long id) {
        log.debug("REST request to delete MajorVersion : {}", id);
        majorVersionService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("majorVersion", id.toString())).build();
    }

    /**
     * SEARCH  /_search/major-versions?query=:query : search for the majorVersion corresponding
     * to the query.
     *
     * @param query the query of the majorVersion search 
     * @param pageable the pagination information
     * @return the result of the search
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @GetMapping("/_search/major-versions")
    @Timed
    public ResponseEntity<List<MajorVersionDTO>> searchMajorVersions(@RequestParam String query, Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to search for a page of MajorVersions for query {}", query);
        Page<MajorVersionDTO> page = majorVersionService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/major-versions");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
