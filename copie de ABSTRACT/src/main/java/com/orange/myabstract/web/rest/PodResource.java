package com.orange.myabstract.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.orange.myabstract.service.PodService;
import com.orange.myabstract.web.rest.util.HeaderUtil;
import com.orange.myabstract.web.rest.util.PaginationUtil;
import com.orange.myabstract.service.dto.PodDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Pod.
 */
@RestController
@RequestMapping("/api")
public class PodResource {

    private final Logger log = LoggerFactory.getLogger(PodResource.class);
        
    @Inject
    private PodService podService;

    /**
     * POST  /pods : Create a new pod.
     *
     * @param podDTO the podDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new podDTO, or with status 400 (Bad Request) if the pod has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/pods")
    @Timed
    public ResponseEntity<PodDTO> createPod(@Valid @RequestBody PodDTO podDTO) throws URISyntaxException {
        log.debug("REST request to save Pod : {}", podDTO);
        if (podDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("pod", "idexists", "A new pod cannot already have an ID")).body(null);
        }
        PodDTO result = podService.save(podDTO);
        return ResponseEntity.created(new URI("/api/pods/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("pod", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /pods : Updates an existing pod.
     *
     * @param podDTO the podDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated podDTO,
     * or with status 400 (Bad Request) if the podDTO is not valid,
     * or with status 500 (Internal Server Error) if the podDTO couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/pods")
    @Timed
    public ResponseEntity<PodDTO> updatePod(@Valid @RequestBody PodDTO podDTO) throws URISyntaxException {
        log.debug("REST request to update Pod : {}", podDTO);
        if (podDTO.getId() == null) {
            return createPod(podDTO);
        }
        PodDTO result = podService.save(podDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("pod", podDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /pods : get all the pods.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of pods in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @GetMapping("/pods")
    @Timed
    public ResponseEntity<List<PodDTO>> getAllPods(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of Pods");
        Page<PodDTO> page = podService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/pods");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /pods/:id : get the "id" pod.
     *
     * @param id the id of the podDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the podDTO, or with status 404 (Not Found)
     */
    @GetMapping("/pods/{id}")
    @Timed
    public ResponseEntity<PodDTO> getPod(@PathVariable Long id) {
        log.debug("REST request to get Pod : {}", id);
        PodDTO podDTO = podService.findOne(id);
        return Optional.ofNullable(podDTO)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /pods/:id : delete the "id" pod.
     *
     * @param id the id of the podDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/pods/{id}")
    @Timed
    public ResponseEntity<Void> deletePod(@PathVariable Long id) {
        log.debug("REST request to delete Pod : {}", id);
        podService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("pod", id.toString())).build();
    }

    /**
     * SEARCH  /_search/pods?query=:query : search for the pod corresponding
     * to the query.
     *
     * @param query the query of the pod search 
     * @param pageable the pagination information
     * @return the result of the search
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @GetMapping("/_search/pods")
    @Timed
    public ResponseEntity<List<PodDTO>> searchPods(@RequestParam String query, Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to search for a page of Pods for query {}", query);
        Page<PodDTO> page = podService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/pods");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
