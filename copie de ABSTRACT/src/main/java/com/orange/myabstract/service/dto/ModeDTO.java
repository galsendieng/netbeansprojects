package com.orange.myabstract.service.dto;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;


/**
 * A DTO for the Mode entity.
 */
public class ModeDTO implements Serializable {

    private Long id;

    @NotNull
    private String name;

    @NotNull
    private String abrStatic;

    @NotNull
    private Integer fragment;

    @NotNull
    private String suffixManifest;

    @NotNull
    private String deviceProfil;

    private String clientVersion;

    private String typeManifest;

    @Size(max = 200)
    private String description;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public String getAbrStatic() {
        return abrStatic;
    }

    public void setAbrStatic(String abrStatic) {
        this.abrStatic = abrStatic;
    }
    public Integer getFragment() {
        return fragment;
    }

    public void setFragment(Integer fragment) {
        this.fragment = fragment;
    }
    public String getSuffixManifest() {
        return suffixManifest;
    }

    public void setSuffixManifest(String suffixManifest) {
        this.suffixManifest = suffixManifest;
    }
    public String getDeviceProfil() {
        return deviceProfil;
    }

    public void setDeviceProfil(String deviceProfil) {
        this.deviceProfil = deviceProfil;
    }
    public String getClientVersion() {
        return clientVersion;
    }

    public void setClientVersion(String clientVersion) {
        this.clientVersion = clientVersion;
    }
    public String getTypeManifest() {
        return typeManifest;
    }

    public void setTypeManifest(String typeManifest) {
        this.typeManifest = typeManifest;
    }
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ModeDTO modeDTO = (ModeDTO) o;

        if ( ! Objects.equals(id, modeDTO.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "ModeDTO{" +
            "id=" + id +
            ", name='" + name + "'" +
            ", abrStatic='" + abrStatic + "'" +
            ", fragment='" + fragment + "'" +
            ", suffixManifest='" + suffixManifest + "'" +
            ", deviceProfil='" + deviceProfil + "'" +
            ", clientVersion='" + clientVersion + "'" +
            ", typeManifest='" + typeManifest + "'" +
            ", description='" + description + "'" +
            '}';
    }
}
