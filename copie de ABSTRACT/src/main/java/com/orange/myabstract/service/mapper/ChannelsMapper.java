package com.orange.myabstract.service.mapper;

import com.orange.myabstract.domain.*;
import com.orange.myabstract.service.dto.ChannelsDTO;

import org.mapstruct.*;
import java.util.List;

/**
 * Mapper for the entity Channels and its DTO ChannelsDTO.
 */
@Mapper(componentModel = "spring", uses = {StreamerMapper.class, })
public interface ChannelsMapper {

    ChannelsDTO channelsToChannelsDTO(Channels channels);

    List<ChannelsDTO> channelsToChannelsDTOs(List<Channels> channels);

    @Mapping(target = "plays", ignore = true)
    Channels channelsDTOToChannels(ChannelsDTO channelsDTO);

    List<Channels> channelsDTOsToChannels(List<ChannelsDTO> channelsDTOs);

    default Streamer streamerFromId(Long id) {
        if (id == null) {
            return null;
        }
        Streamer streamer = new Streamer();
        streamer.setId(id);
        return streamer;
    }
}
