package com.orange.myabstract.service.mapper;

import com.orange.myabstract.domain.*;
import com.orange.myabstract.service.dto.SmoothStreamingMediaDTO;

import org.mapstruct.*;
import java.util.List;

/**
 * Mapper for the entity SmoothStreamingMedia and its DTO SmoothStreamingMediaDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface SmoothStreamingMediaMapper {

    SmoothStreamingMediaDTO smoothStreamingMediaToSmoothStreamingMediaDTO(SmoothStreamingMedia smoothStreamingMedia);

    List<SmoothStreamingMediaDTO> smoothStreamingMediasToSmoothStreamingMediaDTOs(List<SmoothStreamingMedia> smoothStreamingMedias);

    SmoothStreamingMedia smoothStreamingMediaDTOToSmoothStreamingMedia(SmoothStreamingMediaDTO smoothStreamingMediaDTO);

    List<SmoothStreamingMedia> smoothStreamingMediaDTOsToSmoothStreamingMedias(List<SmoothStreamingMediaDTO> smoothStreamingMediaDTOs);
}
