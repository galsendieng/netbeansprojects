package com.orange.myabstract.service;

import com.orange.myabstract.service.dto.ChannelsDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.LinkedList;
import java.util.List;

/**
 * Service Interface for managing Channels.
 */
public interface ChannelsService {

    /**
     * Save a channels.
     *
     * @param channelsDTO the entity to save
     * @return the persisted entity
     */
    ChannelsDTO save(ChannelsDTO channelsDTO);

    /**
     *  Get all the channels.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<ChannelsDTO> findAll(Pageable pageable);

    /**
     *  Get the "id" channels.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    ChannelsDTO findOne(Long id);

    /**
     *  Delete the "id" channels.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Search for the channels corresponding to the query.
     *
     *  @param query the query of the search
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<ChannelsDTO> search(String query, Pageable pageable);
}
