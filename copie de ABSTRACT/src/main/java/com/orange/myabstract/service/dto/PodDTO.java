package com.orange.myabstract.service.dto;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;


/**
 * A DTO for the Pod entity.
 */
public class PodDTO implements Serializable {

    private Long id;

    @NotNull
    private String name;

    @NotNull
    private String link;

    @Size(max = 200)
    private String description;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PodDTO podDTO = (PodDTO) o;

        if ( ! Objects.equals(id, podDTO.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "PodDTO{" +
            "id=" + id +
            ", name='" + name + "'" +
            ", link='" + link + "'" +
            ", description='" + description + "'" +
            '}';
    }
}
