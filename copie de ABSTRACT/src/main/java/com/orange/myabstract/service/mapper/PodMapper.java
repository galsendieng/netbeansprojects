package com.orange.myabstract.service.mapper;

import com.orange.myabstract.domain.*;
import com.orange.myabstract.service.dto.PodDTO;

import org.mapstruct.*;
import java.util.List;

/**
 * Mapper for the entity Pod and its DTO PodDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface PodMapper {

    PodDTO podToPodDTO(Pod pod);

    List<PodDTO> podsToPodDTOs(List<Pod> pods);

    @Mapping(target = "streamers", ignore = true)
    Pod podDTOToPod(PodDTO podDTO);

    List<Pod> podDTOsToPods(List<PodDTO> podDTOs);
}
