package com.orange.myabstract.service;

import com.orange.myabstract.service.dto.SmoothStreamingMediaDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.LinkedList;
import java.util.List;

/**
 * Service Interface for managing SmoothStreamingMedia.
 */
public interface SmoothStreamingMediaService {

    /**
     * Save a smoothStreamingMedia.
     *
     * @param smoothStreamingMediaDTO the entity to save
     * @return the persisted entity
     */
    SmoothStreamingMediaDTO save(SmoothStreamingMediaDTO smoothStreamingMediaDTO);

    /**
     *  Get all the smoothStreamingMedias.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<SmoothStreamingMediaDTO> findAll(Pageable pageable);

    /**
     *  Get the "id" smoothStreamingMedia.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    SmoothStreamingMediaDTO findOne(Long id);

    /**
     *  Delete the "id" smoothStreamingMedia.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Search for the smoothStreamingMedia corresponding to the query.
     *
     *  @param query the query of the search
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<SmoothStreamingMediaDTO> search(String query, Pageable pageable);
}
