package com.orange.myabstract.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Streamer.
 */
@Entity
@Table(name = "streamer")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "streamer")
public class Streamer implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Column(name = "name_streamer", nullable = false)
    private String nameStreamer;

    @NotNull
    @Column(name = "address", nullable = false)
    private String address;

    @Size(max = 200)
    @Column(name = "description", length = 200)
    private String description;

    @ManyToOne
    private Pod lePod;

    @ManyToMany(mappedBy = "streamers")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Channels> theChannels = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNameStreamer() {
        return nameStreamer;
    }

    public Streamer nameStreamer(String nameStreamer) {
        this.nameStreamer = nameStreamer;
        return this;
    }

    public void setNameStreamer(String nameStreamer) {
        this.nameStreamer = nameStreamer;
    }

    public String getAddress() {
        return address;
    }

    public Streamer address(String address) {
        this.address = address;
        return this;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDescription() {
        return description;
    }

    public Streamer description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Pod getLePod() {
        return lePod;
    }

    public Streamer lePod(Pod pod) {
        this.lePod = pod;
        return this;
    }

    public void setLePod(Pod pod) {
        this.lePod = pod;
    }

    public Set<Channels> getTheChannels() {
        return theChannels;
    }

    public Streamer theChannels(Set<Channels> channels) {
        this.theChannels = channels;
        return this;
    }

    public Streamer addTheChannels(Channels channels) {
        theChannels.add(channels);
        channels.getStreamers().add(this);
        return this;
    }

    public Streamer removeTheChannels(Channels channels) {
        theChannels.remove(channels);
        channels.getStreamers().remove(this);
        return this;
    }

    public void setTheChannels(Set<Channels> channels) {
        this.theChannels = channels;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Streamer streamer = (Streamer) o;
        if(streamer.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, streamer.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Streamer{" +
            "id=" + id +
            ", nameStreamer='" + nameStreamer + "'" +
            ", address='" + address + "'" +
            ", description='" + description + "'" +
            '}';
    }
}
