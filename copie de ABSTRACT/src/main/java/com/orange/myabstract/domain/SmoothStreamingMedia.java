package com.orange.myabstract.domain;

import io.swagger.annotations.ApiModelProperty;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

/**
 * A SmoothStreamingMedia.
 */
@Entity
@Table(name = "smooth_streaming_media")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "smoothstreamingmedia")
public class SmoothStreamingMedia implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Column(name = "name_smooth", nullable = false)
    private String nameSmooth;

    @Column(name = "major_version")
    private String majorVersion;

    @Column(name = "minor_version")
    private String minorVersion;

    @Column(name = "duration")
    private String duration;

    @Column(name = "dvr_window_length")
    private String dvrWindowLength;

    @Column(name = "look_ahead_fragment_count")
    private String lookAheadFragmentCount;

    @Column(name = "is_live")
    private Boolean isLive;

    @Column(name = "can_seek")
    private Boolean canSeek;

    @Column(name = "can_pause")
    private Boolean canPause;

    @Column(name = "date_manifest")
    private ZonedDateTime dateManifest;

    @Column(name = "smooth_ok")
    private Boolean smoothOk;

    @Lob
    @Column(name = "manifest_file")
    private byte[] manifestFile;

    @Column(name = "manifest_file_content_type")
    private String manifestFileContentType;

    /**
     * StreamIndex                                                             
     * 
     */
    @ApiModelProperty(value = "StreamIndex")
    @Column(name = "type_stream_index")
    private String typeStreamIndex;

    @Column(name = "name_video")
    private String nameVideo;

    @Column(name = "name_audio")
    private String nameAudio;

    @Column(name = "nbr_chunks_video")
    private Integer nbrChunksVideo;

    @Column(name = "nbr_chunks_audio")
    private Integer nbrChunksAudio;

    @Column(name = "chunks_video")
    private String chunksVideo;

    @Column(name = "chunks_audio")
    private String chunksAudio;

    @Column(name = "quality_levels_video")
    private String qualityLevelsVideo;

    @Column(name = "quality_levels_audio")
    private String qualityLevelsAudio;

    @Column(name = "max_width_video")
    private String maxWidthVideo;

    @Column(name = "max_height_video")
    private String maxHeightVideo;

    /**
     * QualityLevelVideo                                                       
     * 
     */
    @ApiModelProperty(value = "QualityLevelVideo")
    @Column(name = "max_width_audio")
    private String maxWidthAudio;

    @Column(name = "max_height_audio")
    private String maxHeightAudio;

    @Column(name = "display_width_video")
    private String displayWidthVideo;

    @Column(name = "display_height_video")
    private String displayHeightVideo;

    @Column(name = "url_video")
    private String urlVideo;

    @Column(name = "url_audio")
    private String urlAudio;

    @Column(name = "index_strm_audio")
    private String indexStrmAudio;

    @Column(name = "language")
    private String language;

    /**
     * QualityLevelAudio                                                       
     * 
     */
    @ApiModelProperty(value = "QualityLevelAudio")
    @Column(name = "index_qlity_video")
    private String indexQlityVideo;

    @Column(name = "index_qlity_audio")
    private String indexQlityAudio;

    @Column(name = "bitrate_video")
    private String bitrateVideo;

    @Column(name = "bitrate_audio")
    private String bitrateAudio;

    @Column(name = "four_cc_video")
    private String fourCcVideo;

    @Column(name = "four_cc_audio")
    private String fourCcAudio;

    @Column(name = "codec_private_data_video")
    private String codecPrivateDataVideo;

    @Column(name = "codec_private_data_audio")
    private String codecPrivateDataAudio;

    @Column(name = "sampling_rate")
    private String samplingRate;

    @Column(name = "channels")
    private String channels;

    @Column(name = "bits_per_sample")
    private String bitsPerSample;

    @Column(name = "packet_size")
    private String packetSize;

    @Column(name = "audio_tag")
    private String audioTag;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNameSmooth() {
        return nameSmooth;
    }

    public SmoothStreamingMedia nameSmooth(String nameSmooth) {
        this.nameSmooth = nameSmooth;
        return this;
    }

    public void setNameSmooth(String nameSmooth) {
        this.nameSmooth = nameSmooth;
    }

    public String getMajorVersion() {
        return majorVersion;
    }

    public SmoothStreamingMedia majorVersion(String majorVersion) {
        this.majorVersion = majorVersion;
        return this;
    }

    public void setMajorVersion(String majorVersion) {
        this.majorVersion = majorVersion;
    }

    public String getMinorVersion() {
        return minorVersion;
    }

    public SmoothStreamingMedia minorVersion(String minorVersion) {
        this.minorVersion = minorVersion;
        return this;
    }

    public void setMinorVersion(String minorVersion) {
        this.minorVersion = minorVersion;
    }

    public String getDuration() {
        return duration;
    }

    public SmoothStreamingMedia duration(String duration) {
        this.duration = duration;
        return this;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getDvrWindowLength() {
        return dvrWindowLength;
    }

    public SmoothStreamingMedia dvrWindowLength(String dvrWindowLength) {
        this.dvrWindowLength = dvrWindowLength;
        return this;
    }

    public void setDvrWindowLength(String dvrWindowLength) {
        this.dvrWindowLength = dvrWindowLength;
    }

    public String getLookAheadFragmentCount() {
        return lookAheadFragmentCount;
    }

    public SmoothStreamingMedia lookAheadFragmentCount(String lookAheadFragmentCount) {
        this.lookAheadFragmentCount = lookAheadFragmentCount;
        return this;
    }

    public void setLookAheadFragmentCount(String lookAheadFragmentCount) {
        this.lookAheadFragmentCount = lookAheadFragmentCount;
    }

    public Boolean isIsLive() {
        return isLive;
    }

    public SmoothStreamingMedia isLive(Boolean isLive) {
        this.isLive = isLive;
        return this;
    }

    public void setIsLive(Boolean isLive) {
        this.isLive = isLive;
    }

    public Boolean isCanSeek() {
        return canSeek;
    }

    public SmoothStreamingMedia canSeek(Boolean canSeek) {
        this.canSeek = canSeek;
        return this;
    }

    public void setCanSeek(Boolean canSeek) {
        this.canSeek = canSeek;
    }

    public Boolean isCanPause() {
        return canPause;
    }

    public SmoothStreamingMedia canPause(Boolean canPause) {
        this.canPause = canPause;
        return this;
    }

    public void setCanPause(Boolean canPause) {
        this.canPause = canPause;
    }

    public ZonedDateTime getDateManifest() {
        return dateManifest;
    }

    public SmoothStreamingMedia dateManifest(ZonedDateTime dateManifest) {
        this.dateManifest = dateManifest;
        return this;
    }

    public void setDateManifest(ZonedDateTime dateManifest) {
        this.dateManifest = dateManifest;
    }

    public Boolean isSmoothOk() {
        return smoothOk;
    }

    public SmoothStreamingMedia smoothOk(Boolean smoothOk) {
        this.smoothOk = smoothOk;
        return this;
    }

    public void setSmoothOk(Boolean smoothOk) {
        this.smoothOk = smoothOk;
    }

    public byte[] getManifestFile() {
        return manifestFile;
    }

    public SmoothStreamingMedia manifestFile(byte[] manifestFile) {
        this.manifestFile = manifestFile;
        return this;
    }

    public void setManifestFile(byte[] manifestFile) {
        this.manifestFile = manifestFile;
    }

    public String getManifestFileContentType() {
        return manifestFileContentType;
    }

    public SmoothStreamingMedia manifestFileContentType(String manifestFileContentType) {
        this.manifestFileContentType = manifestFileContentType;
        return this;
    }

    public void setManifestFileContentType(String manifestFileContentType) {
        this.manifestFileContentType = manifestFileContentType;
    }

    public String getTypeStreamIndex() {
        return typeStreamIndex;
    }

    public SmoothStreamingMedia typeStreamIndex(String typeStreamIndex) {
        this.typeStreamIndex = typeStreamIndex;
        return this;
    }

    public void setTypeStreamIndex(String typeStreamIndex) {
        this.typeStreamIndex = typeStreamIndex;
    }

    public String getNameVideo() {
        return nameVideo;
    }

    public SmoothStreamingMedia nameVideo(String nameVideo) {
        this.nameVideo = nameVideo;
        return this;
    }

    public void setNameVideo(String nameVideo) {
        this.nameVideo = nameVideo;
    }

    public String getNameAudio() {
        return nameAudio;
    }

    public SmoothStreamingMedia nameAudio(String nameAudio) {
        this.nameAudio = nameAudio;
        return this;
    }

    public void setNameAudio(String nameAudio) {
        this.nameAudio = nameAudio;
    }

    public Integer getNbrChunksVideo() {
        return nbrChunksVideo;
    }

    public SmoothStreamingMedia nbrChunksVideo(Integer nbrChunksVideo) {
        this.nbrChunksVideo = nbrChunksVideo;
        return this;
    }

    public void setNbrChunksVideo(Integer nbrChunksVideo) {
        this.nbrChunksVideo = nbrChunksVideo;
    }

    public Integer getNbrChunksAudio() {
        return nbrChunksAudio;
    }

    public SmoothStreamingMedia nbrChunksAudio(Integer nbrChunksAudio) {
        this.nbrChunksAudio = nbrChunksAudio;
        return this;
    }

    public void setNbrChunksAudio(Integer nbrChunksAudio) {
        this.nbrChunksAudio = nbrChunksAudio;
    }

    public String getChunksVideo() {
        return chunksVideo;
    }

    public SmoothStreamingMedia chunksVideo(String chunksVideo) {
        this.chunksVideo = chunksVideo;
        return this;
    }

    public void setChunksVideo(String chunksVideo) {
        this.chunksVideo = chunksVideo;
    }

    public String getChunksAudio() {
        return chunksAudio;
    }

    public SmoothStreamingMedia chunksAudio(String chunksAudio) {
        this.chunksAudio = chunksAudio;
        return this;
    }

    public void setChunksAudio(String chunksAudio) {
        this.chunksAudio = chunksAudio;
    }

    public String getQualityLevelsVideo() {
        return qualityLevelsVideo;
    }

    public SmoothStreamingMedia qualityLevelsVideo(String qualityLevelsVideo) {
        this.qualityLevelsVideo = qualityLevelsVideo;
        return this;
    }

    public void setQualityLevelsVideo(String qualityLevelsVideo) {
        this.qualityLevelsVideo = qualityLevelsVideo;
    }

    public String getQualityLevelsAudio() {
        return qualityLevelsAudio;
    }

    public SmoothStreamingMedia qualityLevelsAudio(String qualityLevelsAudio) {
        this.qualityLevelsAudio = qualityLevelsAudio;
        return this;
    }

    public void setQualityLevelsAudio(String qualityLevelsAudio) {
        this.qualityLevelsAudio = qualityLevelsAudio;
    }

    public String getMaxWidthVideo() {
        return maxWidthVideo;
    }

    public SmoothStreamingMedia maxWidthVideo(String maxWidthVideo) {
        this.maxWidthVideo = maxWidthVideo;
        return this;
    }

    public void setMaxWidthVideo(String maxWidthVideo) {
        this.maxWidthVideo = maxWidthVideo;
    }

    public String getMaxHeightVideo() {
        return maxHeightVideo;
    }

    public SmoothStreamingMedia maxHeightVideo(String maxHeightVideo) {
        this.maxHeightVideo = maxHeightVideo;
        return this;
    }

    public void setMaxHeightVideo(String maxHeightVideo) {
        this.maxHeightVideo = maxHeightVideo;
    }

    public String getMaxWidthAudio() {
        return maxWidthAudio;
    }

    public SmoothStreamingMedia maxWidthAudio(String maxWidthAudio) {
        this.maxWidthAudio = maxWidthAudio;
        return this;
    }

    public void setMaxWidthAudio(String maxWidthAudio) {
        this.maxWidthAudio = maxWidthAudio;
    }

    public String getMaxHeightAudio() {
        return maxHeightAudio;
    }

    public SmoothStreamingMedia maxHeightAudio(String maxHeightAudio) {
        this.maxHeightAudio = maxHeightAudio;
        return this;
    }

    public void setMaxHeightAudio(String maxHeightAudio) {
        this.maxHeightAudio = maxHeightAudio;
    }

    public String getDisplayWidthVideo() {
        return displayWidthVideo;
    }

    public SmoothStreamingMedia displayWidthVideo(String displayWidthVideo) {
        this.displayWidthVideo = displayWidthVideo;
        return this;
    }

    public void setDisplayWidthVideo(String displayWidthVideo) {
        this.displayWidthVideo = displayWidthVideo;
    }

    public String getDisplayHeightVideo() {
        return displayHeightVideo;
    }

    public SmoothStreamingMedia displayHeightVideo(String displayHeightVideo) {
        this.displayHeightVideo = displayHeightVideo;
        return this;
    }

    public void setDisplayHeightVideo(String displayHeightVideo) {
        this.displayHeightVideo = displayHeightVideo;
    }

    public String getUrlVideo() {
        return urlVideo;
    }

    public SmoothStreamingMedia urlVideo(String urlVideo) {
        this.urlVideo = urlVideo;
        return this;
    }

    public void setUrlVideo(String urlVideo) {
        this.urlVideo = urlVideo;
    }

    public String getUrlAudio() {
        return urlAudio;
    }

    public SmoothStreamingMedia urlAudio(String urlAudio) {
        this.urlAudio = urlAudio;
        return this;
    }

    public void setUrlAudio(String urlAudio) {
        this.urlAudio = urlAudio;
    }

    public String getIndexStrmAudio() {
        return indexStrmAudio;
    }

    public SmoothStreamingMedia indexStrmAudio(String indexStrmAudio) {
        this.indexStrmAudio = indexStrmAudio;
        return this;
    }

    public void setIndexStrmAudio(String indexStrmAudio) {
        this.indexStrmAudio = indexStrmAudio;
    }

    public String getLanguage() {
        return language;
    }

    public SmoothStreamingMedia language(String language) {
        this.language = language;
        return this;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getIndexQlityVideo() {
        return indexQlityVideo;
    }

    public SmoothStreamingMedia indexQlityVideo(String indexQlityVideo) {
        this.indexQlityVideo = indexQlityVideo;
        return this;
    }

    public void setIndexQlityVideo(String indexQlityVideo) {
        this.indexQlityVideo = indexQlityVideo;
    }

    public String getIndexQlityAudio() {
        return indexQlityAudio;
    }

    public SmoothStreamingMedia indexQlityAudio(String indexQlityAudio) {
        this.indexQlityAudio = indexQlityAudio;
        return this;
    }

    public void setIndexQlityAudio(String indexQlityAudio) {
        this.indexQlityAudio = indexQlityAudio;
    }

    public String getBitrateVideo() {
        return bitrateVideo;
    }

    public SmoothStreamingMedia bitrateVideo(String bitrateVideo) {
        this.bitrateVideo = bitrateVideo;
        return this;
    }

    public void setBitrateVideo(String bitrateVideo) {
        this.bitrateVideo = bitrateVideo;
    }

    public String getBitrateAudio() {
        return bitrateAudio;
    }

    public SmoothStreamingMedia bitrateAudio(String bitrateAudio) {
        this.bitrateAudio = bitrateAudio;
        return this;
    }

    public void setBitrateAudio(String bitrateAudio) {
        this.bitrateAudio = bitrateAudio;
    }

    public String getFourCcVideo() {
        return fourCcVideo;
    }

    public SmoothStreamingMedia fourCcVideo(String fourCcVideo) {
        this.fourCcVideo = fourCcVideo;
        return this;
    }

    public void setFourCcVideo(String fourCcVideo) {
        this.fourCcVideo = fourCcVideo;
    }

    public String getFourCcAudio() {
        return fourCcAudio;
    }

    public SmoothStreamingMedia fourCcAudio(String fourCcAudio) {
        this.fourCcAudio = fourCcAudio;
        return this;
    }

    public void setFourCcAudio(String fourCcAudio) {
        this.fourCcAudio = fourCcAudio;
    }

    public String getCodecPrivateDataVideo() {
        return codecPrivateDataVideo;
    }

    public SmoothStreamingMedia codecPrivateDataVideo(String codecPrivateDataVideo) {
        this.codecPrivateDataVideo = codecPrivateDataVideo;
        return this;
    }

    public void setCodecPrivateDataVideo(String codecPrivateDataVideo) {
        this.codecPrivateDataVideo = codecPrivateDataVideo;
    }

    public String getCodecPrivateDataAudio() {
        return codecPrivateDataAudio;
    }

    public SmoothStreamingMedia codecPrivateDataAudio(String codecPrivateDataAudio) {
        this.codecPrivateDataAudio = codecPrivateDataAudio;
        return this;
    }

    public void setCodecPrivateDataAudio(String codecPrivateDataAudio) {
        this.codecPrivateDataAudio = codecPrivateDataAudio;
    }

    public String getSamplingRate() {
        return samplingRate;
    }

    public SmoothStreamingMedia samplingRate(String samplingRate) {
        this.samplingRate = samplingRate;
        return this;
    }

    public void setSamplingRate(String samplingRate) {
        this.samplingRate = samplingRate;
    }

    public String getChannels() {
        return channels;
    }

    public SmoothStreamingMedia channels(String channels) {
        this.channels = channels;
        return this;
    }

    public void setChannels(String channels) {
        this.channels = channels;
    }

    public String getBitsPerSample() {
        return bitsPerSample;
    }

    public SmoothStreamingMedia bitsPerSample(String bitsPerSample) {
        this.bitsPerSample = bitsPerSample;
        return this;
    }

    public void setBitsPerSample(String bitsPerSample) {
        this.bitsPerSample = bitsPerSample;
    }

    public String getPacketSize() {
        return packetSize;
    }

    public SmoothStreamingMedia packetSize(String packetSize) {
        this.packetSize = packetSize;
        return this;
    }

    public void setPacketSize(String packetSize) {
        this.packetSize = packetSize;
    }

    public String getAudioTag() {
        return audioTag;
    }

    public SmoothStreamingMedia audioTag(String audioTag) {
        this.audioTag = audioTag;
        return this;
    }

    public void setAudioTag(String audioTag) {
        this.audioTag = audioTag;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        SmoothStreamingMedia smoothStreamingMedia = (SmoothStreamingMedia) o;
        if(smoothStreamingMedia.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, smoothStreamingMedia.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "SmoothStreamingMedia{" +
            "id=" + id +
            ", nameSmooth='" + nameSmooth + "'" +
            ", majorVersion='" + majorVersion + "'" +
            ", minorVersion='" + minorVersion + "'" +
            ", duration='" + duration + "'" +
            ", dvrWindowLength='" + dvrWindowLength + "'" +
            ", lookAheadFragmentCount='" + lookAheadFragmentCount + "'" +
            ", isLive='" + isLive + "'" +
            ", canSeek='" + canSeek + "'" +
            ", canPause='" + canPause + "'" +
            ", dateManifest='" + dateManifest + "'" +
            ", smoothOk='" + smoothOk + "'" +
            ", manifestFile='" + manifestFile + "'" +
            ", manifestFileContentType='" + manifestFileContentType + "'" +
            ", typeStreamIndex='" + typeStreamIndex + "'" +
            ", nameVideo='" + nameVideo + "'" +
            ", nameAudio='" + nameAudio + "'" +
            ", nbrChunksVideo='" + nbrChunksVideo + "'" +
            ", nbrChunksAudio='" + nbrChunksAudio + "'" +
            ", chunksVideo='" + chunksVideo + "'" +
            ", chunksAudio='" + chunksAudio + "'" +
            ", qualityLevelsVideo='" + qualityLevelsVideo + "'" +
            ", qualityLevelsAudio='" + qualityLevelsAudio + "'" +
            ", maxWidthVideo='" + maxWidthVideo + "'" +
            ", maxHeightVideo='" + maxHeightVideo + "'" +
            ", maxWidthAudio='" + maxWidthAudio + "'" +
            ", maxHeightAudio='" + maxHeightAudio + "'" +
            ", displayWidthVideo='" + displayWidthVideo + "'" +
            ", displayHeightVideo='" + displayHeightVideo + "'" +
            ", urlVideo='" + urlVideo + "'" +
            ", urlAudio='" + urlAudio + "'" +
            ", indexStrmAudio='" + indexStrmAudio + "'" +
            ", language='" + language + "'" +
            ", indexQlityVideo='" + indexQlityVideo + "'" +
            ", indexQlityAudio='" + indexQlityAudio + "'" +
            ", bitrateVideo='" + bitrateVideo + "'" +
            ", bitrateAudio='" + bitrateAudio + "'" +
            ", fourCcVideo='" + fourCcVideo + "'" +
            ", fourCcAudio='" + fourCcAudio + "'" +
            ", codecPrivateDataVideo='" + codecPrivateDataVideo + "'" +
            ", codecPrivateDataAudio='" + codecPrivateDataAudio + "'" +
            ", samplingRate='" + samplingRate + "'" +
            ", channels='" + channels + "'" +
            ", bitsPerSample='" + bitsPerSample + "'" +
            ", packetSize='" + packetSize + "'" +
            ", audioTag='" + audioTag + "'" +
            '}';
    }
}
