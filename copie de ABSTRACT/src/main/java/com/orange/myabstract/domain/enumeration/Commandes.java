package com.orange.myabstract.domain.enumeration;

/**
 * The Commandes enumeration.
 */
public enum Commandes {
    LIVE,CATCHUP,VODPLAYOUT,VODPREPACKED,NTC,NPVR,STARTOVER
}
