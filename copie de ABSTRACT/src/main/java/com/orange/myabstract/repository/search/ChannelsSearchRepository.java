package com.orange.myabstract.repository.search;

import com.orange.myabstract.domain.Channels;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the Channels entity.
 */
public interface ChannelsSearchRepository extends ElasticsearchRepository<Channels, Long> {
}
