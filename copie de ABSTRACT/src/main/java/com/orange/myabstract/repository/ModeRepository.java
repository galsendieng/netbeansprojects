package com.orange.myabstract.repository;

import com.orange.myabstract.domain.Mode;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the Mode entity.
 */
@SuppressWarnings("unused")
public interface ModeRepository extends JpaRepository<Mode,Long> {

}
