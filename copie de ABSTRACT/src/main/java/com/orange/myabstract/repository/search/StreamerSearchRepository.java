package com.orange.myabstract.repository.search;

import com.orange.myabstract.domain.Streamer;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the Streamer entity.
 */
public interface StreamerSearchRepository extends ElasticsearchRepository<Streamer, Long> {
}
