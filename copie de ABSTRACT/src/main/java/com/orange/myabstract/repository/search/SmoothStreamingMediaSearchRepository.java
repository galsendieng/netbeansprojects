package com.orange.myabstract.repository.search;

import com.orange.myabstract.domain.SmoothStreamingMedia;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the SmoothStreamingMedia entity.
 */
public interface SmoothStreamingMediaSearchRepository extends ElasticsearchRepository<SmoothStreamingMedia, Long> {
}
