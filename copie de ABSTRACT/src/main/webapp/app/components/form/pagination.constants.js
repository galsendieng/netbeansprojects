(function() {
    'use strict';

    angular
        .module('abstractApp')
        .constant('paginationConstants', {
            'itemsPerPage': 20
        });
})();
