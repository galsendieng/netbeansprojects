(function() {
    'use strict';

    angular
        .module('abstractApp')
        .factory('ModeSearch', ModeSearch);

    ModeSearch.$inject = ['$resource'];

    function ModeSearch($resource) {
        var resourceUrl =  'api/_search/modes/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
