(function() {
    'use strict';

    angular
        .module('abstractApp')
        .controller('ModeMyAbstractController', ModeMyAbstractController);

    ModeMyAbstractController.$inject = ['$scope', '$state', 'Mode', 'ModeSearch'];

    function ModeMyAbstractController ($scope, $state, Mode, ModeSearch) {
        var vm = this;

        vm.modes = [];
        vm.search = search;
        vm.loadAll = loadAll;

        loadAll();

        function loadAll() {
            Mode.query(function(result) {
                vm.modes = result;
            });
        }

        function search () {
            if (!vm.searchQuery) {
                return vm.loadAll();
            }
            ModeSearch.query({query: vm.searchQuery}, function(result) {
                vm.modes = result;
            });
        }    }
})();
