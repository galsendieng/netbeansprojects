(function() {
    'use strict';
    angular
        .module('abstractApp')
        .factory('SmoothStreamingMedia', SmoothStreamingMedia);

    SmoothStreamingMedia.$inject = ['$resource', 'DateUtils'];

    function SmoothStreamingMedia ($resource, DateUtils) {
        var resourceUrl =  'api/smooth-streaming-medias/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.dateManifest = DateUtils.convertDateTimeFromServer(data.dateManifest);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
