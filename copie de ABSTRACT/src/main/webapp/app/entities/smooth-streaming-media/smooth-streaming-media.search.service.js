(function() {
    'use strict';

    angular
        .module('abstractApp')
        .factory('SmoothStreamingMediaSearch', SmoothStreamingMediaSearch);

    SmoothStreamingMediaSearch.$inject = ['$resource'];

    function SmoothStreamingMediaSearch($resource) {
        var resourceUrl =  'api/_search/smooth-streaming-medias/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
