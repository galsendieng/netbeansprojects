(function() {
    'use strict';

    angular
        .module('abstractApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('smooth-streaming-mediamyAbstract', {
            parent: 'entity',
            url: '/smooth-streaming-mediamyAbstract?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'abstractApp.smoothStreamingMedia.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/smooth-streaming-media/smooth-streaming-mediasmyAbstract.html',
                    controller: 'SmoothStreamingMediaMyAbstractController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('smoothStreamingMedia');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('smooth-streaming-mediamyAbstract-detail', {
            parent: 'entity',
            url: '/smooth-streaming-mediamyAbstract/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'abstractApp.smoothStreamingMedia.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/smooth-streaming-media/smooth-streaming-mediamyAbstract-detail.html',
                    controller: 'SmoothStreamingMediaMyAbstractDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('smoothStreamingMedia');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'SmoothStreamingMedia', function($stateParams, SmoothStreamingMedia) {
                    return SmoothStreamingMedia.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'smooth-streaming-mediamyAbstract',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('smooth-streaming-mediamyAbstract-detail.edit', {
            parent: 'smooth-streaming-mediamyAbstract-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/smooth-streaming-media/smooth-streaming-mediamyAbstract-dialog.html',
                    controller: 'SmoothStreamingMediaMyAbstractDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['SmoothStreamingMedia', function(SmoothStreamingMedia) {
                            return SmoothStreamingMedia.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('smooth-streaming-mediamyAbstract.new', {
            parent: 'smooth-streaming-mediamyAbstract',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/smooth-streaming-media/smooth-streaming-mediamyAbstract-dialog.html',
                    controller: 'SmoothStreamingMediaMyAbstractDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                nameSmooth: null,
                                majorVersion: null,
                                minorVersion: null,
                                duration: null,
                                dvrWindowLength: null,
                                lookAheadFragmentCount: null,
                                isLive: null,
                                canSeek: null,
                                canPause: null,
                                dateManifest: null,
                                smoothOk: null,
                                manifestFile: null,
                                manifestFileContentType: null,
                                typeStreamIndex: null,
                                nameVideo: null,
                                nameAudio: null,
                                nbrChunksVideo: null,
                                nbrChunksAudio: null,
                                chunksVideo: null,
                                chunksAudio: null,
                                qualityLevelsVideo: null,
                                qualityLevelsAudio: null,
                                maxWidthVideo: null,
                                maxHeightVideo: null,
                                maxWidthAudio: null,
                                maxHeightAudio: null,
                                displayWidthVideo: null,
                                displayHeightVideo: null,
                                urlVideo: null,
                                urlAudio: null,
                                indexStrmAudio: null,
                                language: null,
                                indexQlityVideo: null,
                                indexQlityAudio: null,
                                bitrateVideo: null,
                                bitrateAudio: null,
                                fourCcVideo: null,
                                fourCcAudio: null,
                                codecPrivateDataVideo: null,
                                codecPrivateDataAudio: null,
                                samplingRate: null,
                                channels: null,
                                bitsPerSample: null,
                                packetSize: null,
                                audioTag: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('smooth-streaming-mediamyAbstract', null, { reload: 'smooth-streaming-mediamyAbstract' });
                }, function() {
                    $state.go('smooth-streaming-mediamyAbstract');
                });
            }]
        })
        .state('smooth-streaming-mediamyAbstract.edit', {
            parent: 'smooth-streaming-mediamyAbstract',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/smooth-streaming-media/smooth-streaming-mediamyAbstract-dialog.html',
                    controller: 'SmoothStreamingMediaMyAbstractDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['SmoothStreamingMedia', function(SmoothStreamingMedia) {
                            return SmoothStreamingMedia.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('smooth-streaming-mediamyAbstract', null, { reload: 'smooth-streaming-mediamyAbstract' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('smooth-streaming-mediamyAbstract.delete', {
            parent: 'smooth-streaming-mediamyAbstract',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/smooth-streaming-media/smooth-streaming-mediamyAbstract-delete-dialog.html',
                    controller: 'SmoothStreamingMediaMyAbstractDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['SmoothStreamingMedia', function(SmoothStreamingMedia) {
                            return SmoothStreamingMedia.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('smooth-streaming-mediamyAbstract', null, { reload: 'smooth-streaming-mediamyAbstract' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
