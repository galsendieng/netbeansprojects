(function() {
    'use strict';

    angular
        .module('abstractApp')
        .controller('SmoothStreamingMediaMyAbstractDeleteController',SmoothStreamingMediaMyAbstractDeleteController);

    SmoothStreamingMediaMyAbstractDeleteController.$inject = ['$uibModalInstance', 'entity', 'SmoothStreamingMedia'];

    function SmoothStreamingMediaMyAbstractDeleteController($uibModalInstance, entity, SmoothStreamingMedia) {
        var vm = this;

        vm.smoothStreamingMedia = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            SmoothStreamingMedia.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
