(function() {
    'use strict';

    angular
        .module('abstractApp')
        .controller('PlayingMyAbstractController', PlayingMyAbstractController);

    PlayingMyAbstractController.$inject = ['$scope', '$state', 'DataUtils', 'Playing', 'PlayingSearch'];

    function PlayingMyAbstractController ($scope, $state, DataUtils, Playing, PlayingSearch) {
        var vm = this;

        vm.playings = [];
        vm.openFile = DataUtils.openFile;
        vm.byteSize = DataUtils.byteSize;
        vm.search = search;
        vm.loadAll = loadAll;

        loadAll();

        function loadAll() {
            Playing.query(function(result) {
                vm.playings = result;
            });
        }

        function search () {
            if (!vm.searchQuery) {
                return vm.loadAll();
            }
            PlayingSearch.query({query: vm.searchQuery}, function(result) {
                vm.playings = result;
            });
        }    }
})();
