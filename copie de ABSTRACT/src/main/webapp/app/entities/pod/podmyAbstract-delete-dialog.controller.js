(function() {
    'use strict';

    angular
        .module('abstractApp')
        .controller('PodMyAbstractDeleteController',PodMyAbstractDeleteController);

    PodMyAbstractDeleteController.$inject = ['$uibModalInstance', 'entity', 'Pod'];

    function PodMyAbstractDeleteController($uibModalInstance, entity, Pod) {
        var vm = this;

        vm.pod = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            Pod.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
