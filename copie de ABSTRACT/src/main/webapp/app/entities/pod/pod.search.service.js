(function() {
    'use strict';

    angular
        .module('abstractApp')
        .factory('PodSearch', PodSearch);

    PodSearch.$inject = ['$resource'];

    function PodSearch($resource) {
        var resourceUrl =  'api/_search/pods/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
