package com.orange.myabstract.web.rest;

import com.orange.myabstract.AbstractApp;

import com.orange.myabstract.domain.Playing;
import com.orange.myabstract.repository.PlayingRepository;
import com.orange.myabstract.service.PlayingService;
import com.orange.myabstract.repository.search.PlayingSearchRepository;
import com.orange.myabstract.service.dto.PlayingDTO;
import com.orange.myabstract.service.mapper.PlayingMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.orange.myabstract.domain.enumeration.Commandes;
/**
 * Test class for the PlayingResource REST controller.
 *
 * @see PlayingResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = AbstractApp.class)
public class PlayingResourceIntTest {

    private static final Commandes DEFAULT_COMMANDE = Commandes.LIVE;
    private static final Commandes UPDATED_COMMANDE = Commandes.CATCHUP;

    private static final String DEFAULT_LINK = "AAAAAAAAAA";
    private static final String UPDATED_LINK = "BBBBBBBBBB";

    private static final byte[] DEFAULT_FILE_TEST = TestUtil.createByteArray(1, "0");
    private static final byte[] UPDATED_FILE_TEST = TestUtil.createByteArray(2, "1");
    private static final String DEFAULT_FILE_TEST_CONTENT_TYPE = "image/jpg";
    private static final String UPDATED_FILE_TEST_CONTENT_TYPE = "image/png";

    @Inject
    private PlayingRepository playingRepository;

    @Inject
    private PlayingMapper playingMapper;

    @Inject
    private PlayingService playingService;

    @Inject
    private PlayingSearchRepository playingSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Inject
    private EntityManager em;

    private MockMvc restPlayingMockMvc;

    private Playing playing;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        PlayingResource playingResource = new PlayingResource();
        ReflectionTestUtils.setField(playingResource, "playingService", playingService);
        this.restPlayingMockMvc = MockMvcBuilders.standaloneSetup(playingResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Playing createEntity(EntityManager em) {
        Playing playing = new Playing()
                .commande(DEFAULT_COMMANDE)
                .link(DEFAULT_LINK)
                .fileTest(DEFAULT_FILE_TEST)
                .fileTestContentType(DEFAULT_FILE_TEST_CONTENT_TYPE);
        return playing;
    }

    @Before
    public void initTest() {
        playingSearchRepository.deleteAll();
        playing = createEntity(em);
    }

    @Test
    @Transactional
    public void createPlaying() throws Exception {
        int databaseSizeBeforeCreate = playingRepository.findAll().size();

        // Create the Playing
        PlayingDTO playingDTO = playingMapper.playingToPlayingDTO(playing);

        restPlayingMockMvc.perform(post("/api/playings")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(playingDTO)))
                .andExpect(status().isCreated());

        // Validate the Playing in the database
        List<Playing> playings = playingRepository.findAll();
        assertThat(playings).hasSize(databaseSizeBeforeCreate + 1);
        Playing testPlaying = playings.get(playings.size() - 1);
        assertThat(testPlaying.getCommande()).isEqualTo(DEFAULT_COMMANDE);
        assertThat(testPlaying.getLink()).isEqualTo(DEFAULT_LINK);
        assertThat(testPlaying.getFileTest()).isEqualTo(DEFAULT_FILE_TEST);
        assertThat(testPlaying.getFileTestContentType()).isEqualTo(DEFAULT_FILE_TEST_CONTENT_TYPE);

        // Validate the Playing in ElasticSearch
        Playing playingEs = playingSearchRepository.findOne(testPlaying.getId());
        assertThat(playingEs).isEqualToComparingFieldByField(testPlaying);
    }

    @Test
    @Transactional
    public void checkCommandeIsRequired() throws Exception {
        int databaseSizeBeforeTest = playingRepository.findAll().size();
        // set the field null
        playing.setCommande(null);

        // Create the Playing, which fails.
        PlayingDTO playingDTO = playingMapper.playingToPlayingDTO(playing);

        restPlayingMockMvc.perform(post("/api/playings")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(playingDTO)))
                .andExpect(status().isBadRequest());

        List<Playing> playings = playingRepository.findAll();
        assertThat(playings).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkLinkIsRequired() throws Exception {
        int databaseSizeBeforeTest = playingRepository.findAll().size();
        // set the field null
        playing.setLink(null);

        // Create the Playing, which fails.
        PlayingDTO playingDTO = playingMapper.playingToPlayingDTO(playing);

        restPlayingMockMvc.perform(post("/api/playings")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(playingDTO)))
                .andExpect(status().isBadRequest());

        List<Playing> playings = playingRepository.findAll();
        assertThat(playings).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllPlayings() throws Exception {
        // Initialize the database
        playingRepository.saveAndFlush(playing);

        // Get all the playings
        restPlayingMockMvc.perform(get("/api/playings?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.[*].id").value(hasItem(playing.getId().intValue())))
                .andExpect(jsonPath("$.[*].commande").value(hasItem(DEFAULT_COMMANDE.toString())))
                .andExpect(jsonPath("$.[*].link").value(hasItem(DEFAULT_LINK.toString())))
                .andExpect(jsonPath("$.[*].fileTestContentType").value(hasItem(DEFAULT_FILE_TEST_CONTENT_TYPE)))
                .andExpect(jsonPath("$.[*].fileTest").value(hasItem(Base64Utils.encodeToString(DEFAULT_FILE_TEST))));
    }

    @Test
    @Transactional
    public void getPlaying() throws Exception {
        // Initialize the database
        playingRepository.saveAndFlush(playing);

        // Get the playing
        restPlayingMockMvc.perform(get("/api/playings/{id}", playing.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(playing.getId().intValue()))
            .andExpect(jsonPath("$.commande").value(DEFAULT_COMMANDE.toString()))
            .andExpect(jsonPath("$.link").value(DEFAULT_LINK.toString()))
            .andExpect(jsonPath("$.fileTestContentType").value(DEFAULT_FILE_TEST_CONTENT_TYPE))
            .andExpect(jsonPath("$.fileTest").value(Base64Utils.encodeToString(DEFAULT_FILE_TEST)));
    }

    @Test
    @Transactional
    public void getNonExistingPlaying() throws Exception {
        // Get the playing
        restPlayingMockMvc.perform(get("/api/playings/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePlaying() throws Exception {
        // Initialize the database
        playingRepository.saveAndFlush(playing);
        playingSearchRepository.save(playing);
        int databaseSizeBeforeUpdate = playingRepository.findAll().size();

        // Update the playing
        Playing updatedPlaying = playingRepository.findOne(playing.getId());
        updatedPlaying
                .commande(UPDATED_COMMANDE)
                .link(UPDATED_LINK)
                .fileTest(UPDATED_FILE_TEST)
                .fileTestContentType(UPDATED_FILE_TEST_CONTENT_TYPE);
        PlayingDTO playingDTO = playingMapper.playingToPlayingDTO(updatedPlaying);

        restPlayingMockMvc.perform(put("/api/playings")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(playingDTO)))
                .andExpect(status().isOk());

        // Validate the Playing in the database
        List<Playing> playings = playingRepository.findAll();
        assertThat(playings).hasSize(databaseSizeBeforeUpdate);
        Playing testPlaying = playings.get(playings.size() - 1);
        assertThat(testPlaying.getCommande()).isEqualTo(UPDATED_COMMANDE);
        assertThat(testPlaying.getLink()).isEqualTo(UPDATED_LINK);
        assertThat(testPlaying.getFileTest()).isEqualTo(UPDATED_FILE_TEST);
        assertThat(testPlaying.getFileTestContentType()).isEqualTo(UPDATED_FILE_TEST_CONTENT_TYPE);

        // Validate the Playing in ElasticSearch
        Playing playingEs = playingSearchRepository.findOne(testPlaying.getId());
        assertThat(playingEs).isEqualToComparingFieldByField(testPlaying);
    }

    @Test
    @Transactional
    public void deletePlaying() throws Exception {
        // Initialize the database
        playingRepository.saveAndFlush(playing);
        playingSearchRepository.save(playing);
        int databaseSizeBeforeDelete = playingRepository.findAll().size();

        // Get the playing
        restPlayingMockMvc.perform(delete("/api/playings/{id}", playing.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate ElasticSearch is empty
        boolean playingExistsInEs = playingSearchRepository.exists(playing.getId());
        assertThat(playingExistsInEs).isFalse();

        // Validate the database is empty
        List<Playing> playings = playingRepository.findAll();
        assertThat(playings).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchPlaying() throws Exception {
        // Initialize the database
        playingRepository.saveAndFlush(playing);
        playingSearchRepository.save(playing);

        // Search the playing
        restPlayingMockMvc.perform(get("/api/_search/playings?query=id:" + playing.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(playing.getId().intValue())))
            .andExpect(jsonPath("$.[*].commande").value(hasItem(DEFAULT_COMMANDE.toString())))
            .andExpect(jsonPath("$.[*].link").value(hasItem(DEFAULT_LINK.toString())))
            .andExpect(jsonPath("$.[*].fileTestContentType").value(hasItem(DEFAULT_FILE_TEST_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].fileTest").value(hasItem(Base64Utils.encodeToString(DEFAULT_FILE_TEST))));
    }
}
