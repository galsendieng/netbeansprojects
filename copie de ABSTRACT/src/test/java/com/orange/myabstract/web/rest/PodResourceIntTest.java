package com.orange.myabstract.web.rest;

import com.orange.myabstract.AbstractApp;

import com.orange.myabstract.domain.Pod;
import com.orange.myabstract.repository.PodRepository;
import com.orange.myabstract.service.PodService;
import com.orange.myabstract.repository.search.PodSearchRepository;
import com.orange.myabstract.service.dto.PodDTO;
import com.orange.myabstract.service.mapper.PodMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the PodResource REST controller.
 *
 * @see PodResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = AbstractApp.class)
public class PodResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_LINK = "AAAAAAAAAA";
    private static final String UPDATED_LINK = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    @Inject
    private PodRepository podRepository;

    @Inject
    private PodMapper podMapper;

    @Inject
    private PodService podService;

    @Inject
    private PodSearchRepository podSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Inject
    private EntityManager em;

    private MockMvc restPodMockMvc;

    private Pod pod;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        PodResource podResource = new PodResource();
        ReflectionTestUtils.setField(podResource, "podService", podService);
        this.restPodMockMvc = MockMvcBuilders.standaloneSetup(podResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Pod createEntity(EntityManager em) {
        Pod pod = new Pod()
                .name(DEFAULT_NAME)
                .link(DEFAULT_LINK)
                .description(DEFAULT_DESCRIPTION);
        return pod;
    }

    @Before
    public void initTest() {
        podSearchRepository.deleteAll();
        pod = createEntity(em);
    }

    @Test
    @Transactional
    public void createPod() throws Exception {
        int databaseSizeBeforeCreate = podRepository.findAll().size();

        // Create the Pod
        PodDTO podDTO = podMapper.podToPodDTO(pod);

        restPodMockMvc.perform(post("/api/pods")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(podDTO)))
                .andExpect(status().isCreated());

        // Validate the Pod in the database
        List<Pod> pods = podRepository.findAll();
        assertThat(pods).hasSize(databaseSizeBeforeCreate + 1);
        Pod testPod = pods.get(pods.size() - 1);
        assertThat(testPod.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testPod.getLink()).isEqualTo(DEFAULT_LINK);
        assertThat(testPod.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);

        // Validate the Pod in ElasticSearch
        Pod podEs = podSearchRepository.findOne(testPod.getId());
        assertThat(podEs).isEqualToComparingFieldByField(testPod);
    }

    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = podRepository.findAll().size();
        // set the field null
        pod.setName(null);

        // Create the Pod, which fails.
        PodDTO podDTO = podMapper.podToPodDTO(pod);

        restPodMockMvc.perform(post("/api/pods")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(podDTO)))
                .andExpect(status().isBadRequest());

        List<Pod> pods = podRepository.findAll();
        assertThat(pods).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkLinkIsRequired() throws Exception {
        int databaseSizeBeforeTest = podRepository.findAll().size();
        // set the field null
        pod.setLink(null);

        // Create the Pod, which fails.
        PodDTO podDTO = podMapper.podToPodDTO(pod);

        restPodMockMvc.perform(post("/api/pods")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(podDTO)))
                .andExpect(status().isBadRequest());

        List<Pod> pods = podRepository.findAll();
        assertThat(pods).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllPods() throws Exception {
        // Initialize the database
        podRepository.saveAndFlush(pod);

        // Get all the pods
        restPodMockMvc.perform(get("/api/pods?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.[*].id").value(hasItem(pod.getId().intValue())))
                .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
                .andExpect(jsonPath("$.[*].link").value(hasItem(DEFAULT_LINK.toString())))
                .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())));
    }

    @Test
    @Transactional
    public void getPod() throws Exception {
        // Initialize the database
        podRepository.saveAndFlush(pod);

        // Get the pod
        restPodMockMvc.perform(get("/api/pods/{id}", pod.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(pod.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.link").value(DEFAULT_LINK.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingPod() throws Exception {
        // Get the pod
        restPodMockMvc.perform(get("/api/pods/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePod() throws Exception {
        // Initialize the database
        podRepository.saveAndFlush(pod);
        podSearchRepository.save(pod);
        int databaseSizeBeforeUpdate = podRepository.findAll().size();

        // Update the pod
        Pod updatedPod = podRepository.findOne(pod.getId());
        updatedPod
                .name(UPDATED_NAME)
                .link(UPDATED_LINK)
                .description(UPDATED_DESCRIPTION);
        PodDTO podDTO = podMapper.podToPodDTO(updatedPod);

        restPodMockMvc.perform(put("/api/pods")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(podDTO)))
                .andExpect(status().isOk());

        // Validate the Pod in the database
        List<Pod> pods = podRepository.findAll();
        assertThat(pods).hasSize(databaseSizeBeforeUpdate);
        Pod testPod = pods.get(pods.size() - 1);
        assertThat(testPod.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testPod.getLink()).isEqualTo(UPDATED_LINK);
        assertThat(testPod.getDescription()).isEqualTo(UPDATED_DESCRIPTION);

        // Validate the Pod in ElasticSearch
        Pod podEs = podSearchRepository.findOne(testPod.getId());
        assertThat(podEs).isEqualToComparingFieldByField(testPod);
    }

    @Test
    @Transactional
    public void deletePod() throws Exception {
        // Initialize the database
        podRepository.saveAndFlush(pod);
        podSearchRepository.save(pod);
        int databaseSizeBeforeDelete = podRepository.findAll().size();

        // Get the pod
        restPodMockMvc.perform(delete("/api/pods/{id}", pod.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate ElasticSearch is empty
        boolean podExistsInEs = podSearchRepository.exists(pod.getId());
        assertThat(podExistsInEs).isFalse();

        // Validate the database is empty
        List<Pod> pods = podRepository.findAll();
        assertThat(pods).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchPod() throws Exception {
        // Initialize the database
        podRepository.saveAndFlush(pod);
        podSearchRepository.save(pod);

        // Search the pod
        restPodMockMvc.perform(get("/api/_search/pods?query=id:" + pod.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(pod.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].link").value(hasItem(DEFAULT_LINK.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())));
    }
}
