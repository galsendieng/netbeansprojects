#!/usr/bin/python2.7
#-*- coding: utf-8 -*-

import urllib
import urllib2
import datetime

class StaticProfil:
    abr=""
    fragment=""
    suffix=""
    suffix=""
    device=""
    
    def __init__(self,command = "LIVE",profil="MSS",period=None):
        
        self.command = command
        self.profil = profil
        self.period = period
        
    def getDateTimeUTC(self):
        
        fmt = '%Y-%m-%dT%H:%M:%S%ZZ'
        utc_datetime= datetime.datetime.utcnow()
        return utc_datetime.strftime(fmt)
    
    def getManifestLiveCatchup(self,channel="CH_3",typefile="Manifest", startM="LIVE", endM="END"):
        
        if self.profil=="MSS":
            StaticProfil.abr = "shss"
            StaticProfil.fragment=2
            StaticProfil.suffix="ism"
            StaticProfil.device="SMOOTH_2S"
        else:
            if self.profil=="DASH":
                StaticProfil.abr = "sdash"
                StaticProfil.fragment=2
                StaticProfil.suffix="mpd"
                StaticProfil.device="DASH_2S"
            else:
                StaticProfil.abr = "shls"
                StaticProfil.fragment=10
                StaticProfil.suffix="m3u8"
                typefile =""
                StaticProfil.device="HLS_LOW"
        try:
            fmt = '%Y-%m-%dT%H:%M:%S%ZZ'
            timeM= datetime.datetime.utcnow()
            manifest = "http://strm.podb.manager.cdvr.orange.fr:5555/"+str(StaticProfil.abr)+"/LIVE%24"+str(channel)+"/"+str(StaticProfil.fragment)+"."+str(StaticProfil.suffix)+"/"+str(typefile)+"?start="+str(startM)+"&end="+str(endM)+"&device="+str(StaticProfil.device)
            print manifest
            urllib.urlretrieve (manifest, "Manifest_"+str(StaticProfil.abr)+"_"+timeM.strftime(fmt))
        except Exception as x:
            return x
            
    def getManifestCatchup(self):
        return null
    
    def getManifestNtc(self):
        return null
    
    def getManifestStartOver(self):
        return null
    
    def getManifestVODPlayout(self):
        return null
    
    def getManifestVODPrepackage(self):
        return null 