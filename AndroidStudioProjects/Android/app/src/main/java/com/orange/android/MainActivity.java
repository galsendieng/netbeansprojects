package com.orange.android;
import android.app.AlertDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.appindexing.Thing;
import com.google.android.gms.common.api.GoogleApiClient;

public class MainActivity extends AppCompatActivity {

    String mdata;
    EditText nom;
    EditText prenom;
    EditText datenais;
    Spinner lieunais;
    //

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Le spinner
        Spinner spinner = (Spinner) findViewById(R.id.lieu);

// Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.departements_array, android.R.layout.simple_spinner_item);
// Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
// Apply the adapter to the spinner
        spinner.setAdapter(adapter);

        //Widget Date de naissance
        datenais = (EditText) findViewById(R.id.datenais);
        datenais.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();

        //Je recupère les données des champs du formulaire pour le second Activity
        mdata = getIntent().getExtras().getString("data");
        nom = (EditText)findViewById(R.id.nom);
        prenom = (EditText)findViewById(R.id.prenom);
        datenais = (EditText)findViewById(R.id.datenais);
        lieunais = (Spinner)findViewById(R.id.lieu);
    }

    public void validate(View v) {
        //on crée une boite de dialogue
        AlertDialog.Builder adb = new AlertDialog.Builder(MainActivity.this);
        nom = (EditText) findViewById(R.id.nom);
        prenom = (EditText) findViewById(R.id.prenom);
        datenais = (EditText) findViewById(R.id.datenais);
        lieunais = (Spinner)findViewById(R.id.lieu);
        //depts = findViewById(R.id.lieu);
        //on attribut un titre à notre boite de dialogue

        adb.setTitle("Mes données");
        //on insère un message à notre boite de dialogue, et ici on affiche le titre de l'item cliqué
        String nomStr = nom.getText().toString();
        String prenomStr = prenom.getText().toString();
        String datenaisStr = datenais.getText().toString();
        String lieunaisStr = lieunais.getSelectedItem().toString();
        adb.setMessage("Nom :  " + nomStr +
                "\nPrénom :  " + prenomStr +
                "\nDate de naissance :  " + datenaisStr +
                "\nLieu Naissance :  " + lieunaisStr);
        //on indique que l'on veut le bouton ok à notre boite de dialogue
        adb.setPositiveButton("Ok", null);
        //on affiche la boite de dialogue
        adb.show();
    }
    //Demmarer le second activity
    public boolean startSecondaryActivity(View v) {
        Intent intent = new Intent();
        intent.putExtra("returnNom", nom.getText().toString());
        intent.putExtra("returnPrenom", prenom.getText().toString());
        intent.putExtra("returnDaten", datenais.getText().toString());
        intent.putExtra("returnLieun", lieunais.getSelectedItem().toString());
        setResult(RESULT_OK,intent);
        finish();
        return true;
    }

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    public Action getIndexApiAction() {
        Thing object = new Thing.Builder()
                .setName("Main Page") // TODO: Define a title for the content shown.
                // TODO: Make sure this auto-generated URL is correct.
                .setUrl(Uri.parse("http://[ENTER-YOUR-URL-HERE]"))
                .build();
        return new Action.Builder(Action.TYPE_VIEW)
                .setObject(object)
                .setActionStatus(Action.STATUS_TYPE_COMPLETED)
                .build();
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        AppIndex.AppIndexApi.start(client, getIndexApiAction());
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        AppIndex.AppIndexApi.end(client, getIndexApiAction());
        client.disconnect();
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent intent){
        super.onActivityResult(requestCode, resultCode, intent);
        Bundle extras = intent.getExtras();
       // lieunais.toString().setText(extras != null ? extras.getString("returnKey") : "nothing returned");
    }

}
