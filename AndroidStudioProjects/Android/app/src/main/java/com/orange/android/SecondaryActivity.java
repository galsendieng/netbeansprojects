package com.orange.android;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.HashMap;

/**
 * Created by adama on 04/02/17.
 */

public class SecondaryActivity  extends Activity {

    private static final int RESULT_OK = 1;
    Spinner lieunais;

    private String name =  "";
    private String pren = "";
    private String daten = "";
    private String lieun = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_secondary);

    }

    //Fonction pour rechercher la ville du Client
    public boolean naviguer(View v) {
        String ville = lieunais.getSelectedItem().toString();
        Intent i = new Intent("android.intent.action.VIEW", Uri.parse("http://www.google.fr/search?q=" + ville));
        startActivity(i);
        return true;
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent intent){
        // Check which request we're responding to
        //if (requestCode == 1) {
        // Make sure the request was successful
        if (resultCode == RESULT_OK) {
            super.onActivityResult(requestCode, resultCode, intent);
            Bundle extras = intent.getExtras();
            if(extras != null ){
                name =  ""+extras.getString("returnNom");
                pren = ""+ extras.getString("returnPrenom");
                daten = ""+ extras.getString("returnDaten");
                lieun = ""+ extras.getString("returnLieun");

            }
            //}
        }
    }
}
