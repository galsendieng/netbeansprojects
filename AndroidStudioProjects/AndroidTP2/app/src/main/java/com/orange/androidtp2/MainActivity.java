package com.orange.androidtp2;

import android.app.AlertDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import java.util.ArrayList;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity {

    private ListView maListViewPerso;
    private SimpleAdapter mListAdapter;
    private ArrayList<HashMap<String, String>> listItem;
    private String name =  "";
    private String pren = "";
    private String daten = "";
    private String lieun = "";
    static final int PICK_CONTACT_REQUEST = 1;  // The request code
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        maListViewPerso = (ListView) findViewById(R.id.listviewperso);
        listItem = new ArrayList<HashMap<String, String>>();
        HashMap<String, String> map;
        //Création d'une HashMap pour insérer les informations du premier item de notre listView
        //map = new HashMap<String, String>();
        //map.put("nom", "Mon Nom");
        //map.put("prenom", "Mon prénom");
        //map.put("datenais", "14521111");
        //map.put("lieu", "Lieu de naissance");
        //listItem.add(map);
        mListAdapter = new SimpleAdapter(this.getBaseContext(), listItem, R.layout.item,
                new String[]{"nom", "prenom","datenais", "lieu"}, new int[]{
                R.id.nom, R.id.prenom, R.id.datenais, R.id.lieu
        });
        maListViewPerso.setAdapter(mListAdapter);
        maListViewPerso.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // TODO Auto-generated method stub
                HashMap<String, String> map = (HashMap<String, String>) maListViewPerso.getItemAtPosition(position);
                AlertDialog.Builder adb = new AlertDialog.Builder(MainActivity.this);
                adb.setTitle(map.get("nom") + "  "+ map.get("prenom"));
                adb.setMessage(map.get("datenais")
                                +"\n" + map.get("lieu"));
                adb.setPositiveButton("OK", null);
                adb.show();
            }
        });
    }

    public void addItem(View v) {
        HashMap<String, String> map;
        map = new HashMap<String, String>();
        map.put("nom", "DIENG");
        map.put("prenom", "Adama");
        map.put("datenais", "15621111");
        map.put("lieu", "Senegal");
        listItem.add(map);
        mListAdapter.notifyDataSetChanged();
    }

    public boolean startformActivity(View v) {
        //Intent du form_activity
        Intent i = new Intent(getApplicationContext(), form.class);
        i.putExtra("data", "CALL OK!");
        startActivityForResult(i, 0);
        return true;
        //define a new Intent for the second Activity
        //Intent intent = new Intent(this,SecondActivity.class);
        //start the second Activity
        //this.startActivity(intent);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent intent){
        // Check which request we're responding to
        //if (requestCode == 1) {
            // Make sure the request was successful
            if (resultCode == RESULT_OK) {
                super.onActivityResult(requestCode, resultCode, intent);
                Bundle extras = intent.getExtras();
                if(extras != null ){
                    name =  ""+extras.getString("returnNom");
                    pren = ""+ extras.getString("returnPrenom");
                    daten = ""+ extras.getString("returnDaten");
                    lieun = ""+ extras.getString("returnLieun");
                    HashMap<String, String> map;
                    map = new HashMap<String, String>();
                    map.put("nom", "Nom : "+name);
                    map.put("prenom", "Prénom(s) : "+pren);
                    map.put("datenais", "Naissance : "+daten );
                    map.put("lieu", "Lieu : "+lieun);
                    listItem.add(map);
                    mListAdapter.notifyDataSetChanged();
                }
            //}
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.activity_main) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}