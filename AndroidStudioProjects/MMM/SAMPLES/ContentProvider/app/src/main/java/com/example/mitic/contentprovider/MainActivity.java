package com.example.mitic.contentprovider;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // 1) create the URI
        Uri allBooks = LibraryContentProvider.CONTENT_URI;

        // 2) perform the query
        //Cursor c = getContentResolver().query(allBooks, null, null, null, null);

        // this performs the same query
        Cursor c = getContentResolver().query(Uri.parse("content://librarycontentprovider"), null, null, null, null);

        String s = new String();

        // 3) parse the data
        if (c.moveToFirst()) {
            do {

                // we get the data
                s = s + c.getString(c.getColumnIndex(
                        LibraryContentProvider._ID)) + ", " +
                        c.getString(c.getColumnIndex(
                                LibraryContentProvider.BOOK_TITLE)) + ", " +
                        c.getString(c.getColumnIndex(
                                LibraryContentProvider.BOOK_AUTHOR)) + ", " +
                        c.getString(c.getColumnIndex(
                                LibraryContentProvider.BOOK_YEAR)) + "\n";

            } while (c.moveToNext());

            TextView text = (TextView) findViewById(R.id.text);
            text.setText(s);
        }

    }

    public boolean startListView(View v) {
        // starts the second activity
        Intent i = new Intent(getApplicationContext(),MyListActivity.class);
        startActivity(i);
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu (Menu menu){
            // Inflate the menu; this adds items to the action bar if it is present.
            getMenuInflater().inflate(R.menu.menu_main, menu);
            return true;
    }

    @Override
    public boolean onOptionsItemSelected (MenuItem item){
            // Handle action bar item clicks here. The action bar will
            // automatically handle clicks on the Home/Up button, so long
            // as you specify a parent activity in AndroidManifest.xml.
            int id = item.getItemId();

            //noinspection SimplifiableIfStatement
            if (id == R.id.action_settings) {
                return true;
            }

            return super.onOptionsItemSelected(item);
    }
}
