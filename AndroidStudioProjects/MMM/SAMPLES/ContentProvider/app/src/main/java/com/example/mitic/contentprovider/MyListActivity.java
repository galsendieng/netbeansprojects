package com.example.mitic.contentprovider;

import android.app.Activity;
import android.app.ListActivity;
import android.database.Cursor;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

public class MyListActivity extends Activity {

    ListView myListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);


        // 1) get the listview
        myListView = (ListView) findViewById(R.id.listviewperso);


        // 2) create the URI
        Uri allBooks = LibraryContentProvider.CONTENT_URI;

        // 3) perform the query
        Cursor c = getContentResolver().query(allBooks, null, null, null, null);

        // 4.1) create a simple adapter to the listview
        SimpleCursorAdapter adapter = new SimpleCursorAdapter(this,
                android.R.layout.simple_list_item_1,
                c,
                new String[] { "title" },
                new int[] { android.R.id.text1 }, 0);

        // 4.2) create a simple adapter to the listview with two fields
        SimpleCursorAdapter adapter2 = new SimpleCursorAdapter(this,
                android.R.layout.simple_list_item_2,
                c,
                new String[] { "title", "author" },
                new int[] { android.R.id.text1, android.R.id.text2 }, 0);

        // 4.3) create your own adapter
        MyCursorAdapter adapter3 = new MyCursorAdapter(this,c, 0);

        // 5) link the adapter to the listview
        myListView.setAdapter(adapter3);


    }
}
