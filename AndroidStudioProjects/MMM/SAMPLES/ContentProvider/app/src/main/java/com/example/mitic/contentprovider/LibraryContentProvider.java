package com.example.mitic.contentprovider;

import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.net.Uri;


public class LibraryContentProvider extends ContentProvider {


	public static Book data[];
	
	public static final String _ID = "_ID";
	public static final String BOOK_TITLE = "title";
	public static final String BOOK_AUTHOR = "author";
	public static final String BOOK_YEAR = "year";

        // This must be the same as what as specified as the Content Provider authority
        // in the manifest file.
        static final String AUTHORITY = "librarycontentprovider";

	public static final String PROVIDER_NAME = "librarycontentprovider";

        public static final Uri CONTENT_URI = Uri.parse("content://"+ PROVIDER_NAME);


	// sample data to show the ContentProvider principle
	public class Book {
	      public Book(String string, String string2, String string3) {
			title = string;
			author = string2;
			year = string3;
		}
		public String title;
              public String author;
              public String year;
	}


        @Override
        public boolean onCreate() {
		// initialiser des données ici (on simule l'existence d'une BD)
		
    		data = new Book[3];
    		data[0] = new Book("Le viel homme et la mer", "E. Hemmingway","1951");

    		data[1] = new Book("Les travailleurs de la mer","V. Hugo","1866");
    		
    		data[2] = new Book("Moby-Dick","H. Melville","1851");
                
    		return true;
        }

        @Override
        public String getType(Uri uri) {
          // create a new MIME type "com.example.books" for the values which a returned
	  return ContentResolver.CURSOR_DIR_BASE_TYPE + '/' + "com.books";
        }

        @Override
        public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {


    


        	
                        MatrixCursor c = new MatrixCursor(new String[] {
                                        _ID,
                                        BOOK_TITLE,
                                        BOOK_AUTHOR,
					BOOK_YEAR
                        });

                        int row_index = 0;

                        // Add x-axis data
                        for (int i=0; i< data.length; i++) {


                                c.newRow()
                                .add( row_index )
                                .add( data[row_index].title )
                                .add( data[row_index].author )   // Only create data for the first series.
                                .add( data[row_index].year );

                                row_index++;
                        }

                        return c;
                }

        @Override
        public int update(Uri uri, ContentValues contentvalues, String s, String[] as) {
                throw new UnsupportedOperationException(" To be implemented");
        }


        @Override
        public Uri insert(Uri uri, ContentValues contentvalues) {
                throw new UnsupportedOperationException(" To be implemented");
        }

        @Override
        public int delete(Uri uri, String s, String[] as) {
                throw new UnsupportedOperationException(" To be implemented");
        }
}