package com.example.mitic.contentprovider;


import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

public class MyCursorAdapter extends CursorAdapter {

    private LayoutInflater mInflater;

    public MyCursorAdapter(Context context, Cursor c, int flags) {
        super(context, c, flags);
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return mInflater.inflate(R.layout.listitem, parent, false);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {

        if(cursor.getPosition()%2==1) {
            view.setBackgroundColor(context.getResources().getColor(android.R.color.holo_blue_dark));
        }
        else {
            view.setBackgroundColor(context.getResources().getColor(android.R.color.holo_blue_bright));
        }

        TextView year = (TextView) view.findViewById(R.id.year);
        year.setText(cursor.getString(cursor.getColumnIndex(LibraryContentProvider.BOOK_YEAR)));

        TextView title = (TextView) view.findViewById(R.id.title);
        title.setText(cursor.getString(cursor.getColumnIndex(LibraryContentProvider.BOOK_TITLE)));

        TextView author = (TextView) view.findViewById(R.id.author);
        author.setText(cursor.getString(cursor.getColumnIndex(LibraryContentProvider.BOOK_AUTHOR)));
    }
}