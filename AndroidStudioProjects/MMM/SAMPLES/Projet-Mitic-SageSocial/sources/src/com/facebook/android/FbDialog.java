/*
 * Copyright 2010 Facebook, Inc. Licensed under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with the
 * License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0 Unless required by applicable law
 * or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the specific language
 * governing permissions and limitations under the License.
 */

package com.facebook.android;


import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.facebook.android.Facebook.DialogListener;

import fr.istic.sagesocial.R;

public class FbDialog extends Dialog {

    private class FbWebViewClient extends WebViewClient {

	@Override
	public void onPageFinished(final WebView view, final String url) {
	    super.onPageFinished(view, url);
	    FbDialog.this.mSpinner.dismiss();
	    /*
	     * Once webview is fully loaded, set the mContent background to be
	     * transparent and make visible the 'x' image.
	     */
	    FbDialog.this.mContent.setBackgroundColor(Color.TRANSPARENT);
	    FbDialog.this.mWebView.setVisibility(View.VISIBLE);
	    FbDialog.this.mCrossImage.setVisibility(View.VISIBLE);
	}

	@Override
	public void onPageStarted(final WebView view, final String url,
	        final Bitmap favicon) {
	    Log.d("Facebook-WebView", "Webview loading URL: " + url);
	    super.onPageStarted(view, url, favicon);
	    FbDialog.this.mSpinner.show();
	}

	@Override
	public void onReceivedError(final WebView view, final int errorCode,
	        final String description, final String failingUrl) {
	    super.onReceivedError(view, errorCode, description, failingUrl);
	    FbDialog.this.mListener.onError(new DialogError(description, errorCode,
		    failingUrl));
	    FbDialog.this.dismiss();
	}

	@Override
	public boolean shouldOverrideUrlLoading(final WebView view, final String url) {
	    Log.d("Facebook-WebView", "Redirect URL: " + url);
	    if (url.startsWith(Facebook.REDIRECT_URI)) {
		final Bundle values = Util.parseUrl(url);

		String error = values.getString("error");
		if (error == null) {
		    error = values.getString("error_type");
		}

		if (error == null) {
		    FbDialog.this.mListener.onComplete(values);
		}
		else if (error.equals("access_denied")
		        || error.equals("OAuthAccessDeniedException")) {
		    FbDialog.this.mListener.onCancel();
		}
		else {
		    FbDialog.this.mListener.onFacebookError(new FacebookError(error));
		}

		FbDialog.this.dismiss();
		return true;
	    }
	    else if (url.startsWith(Facebook.CANCEL_URI)) {
		FbDialog.this.mListener.onCancel();
		FbDialog.this.dismiss();
		return true;
	    }
	    else if (url.contains(FbDialog.DISPLAY_STRING)) {
		return false;
	    }
	    // launch non-dialog URLs in a full browser
	    FbDialog.this.getContext().startActivity(
		    new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
	    return true;
	}
    }

    static final int FB_BLUE = 0xFF6D84B4;
    static final float[] DIMENSIONS_DIFF_LANDSCAPE = { 20, 60 };
    static final float[] DIMENSIONS_DIFF_PORTRAIT = { 40, 60 };
    static final FrameLayout.LayoutParams FILL = new FrameLayout.LayoutParams(
	    ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.FILL_PARENT);
    static final int MARGIN = 4;
    static final int PADDING = 2;
    static final String DISPLAY_STRING = "touch";

    static final String FB_ICON = "icon.png";
    private final String mUrl;
    private final DialogListener mListener;
    private ProgressDialog mSpinner;
    private ImageView mCrossImage;
    private WebView mWebView;

    private FrameLayout mContent;

    public FbDialog(final Context context, final String url, final DialogListener listener) {
	super(context, android.R.style.Theme_Translucent_NoTitleBar);
	this.mUrl = url;
	this.mListener = listener;
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	this.mSpinner = new ProgressDialog(this.getContext());
	this.mSpinner.requestWindowFeature(Window.FEATURE_NO_TITLE);
	this.mSpinner.setMessage("Loading...");

	this.requestWindowFeature(Window.FEATURE_NO_TITLE);
	this.mContent = new FrameLayout(this.getContext());

	/*
	 * Create the 'x' image, but don't add to the mContent layout yet at
	 * this point, we only need to know its drawable width and height to
	 * place the webview
	 */
	this.createCrossImage();

	/*
	 * Now we know 'x' drawable width and height, layout the webview and add
	 * it the mContent layout
	 */
	final int crossWidth = this.mCrossImage.getDrawable().getIntrinsicWidth();
	this.setUpWebView(crossWidth / 2);

	/*
	 * Finally add the 'x' image to the mContent layout and add mContent to
	 * the Dialog view
	 */
	this.mContent.addView(this.mCrossImage, new LayoutParams(
	        LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
	this.addContentView(this.mContent, new LayoutParams(LayoutParams.FILL_PARENT,
	        LayoutParams.FILL_PARENT));
    }

    private void createCrossImage() {
	this.mCrossImage = new ImageView(this.getContext());
	// Dismiss the dialog when user click on the 'x'
	this.mCrossImage.setOnClickListener(new View.OnClickListener() {
	    @Override
	    public void onClick(final View v) {
		FbDialog.this.mListener.onCancel();
		FbDialog.this.dismiss();
	    }
	});
	final Drawable crossDrawable = this.getContext().getResources()
	        .getDrawable(R.drawable.close);
	this.mCrossImage.setImageDrawable(crossDrawable);
	/*
	 * 'x' should not be visible while webview is loading make it visible
	 * only after webview has fully loaded
	 */
	this.mCrossImage.setVisibility(View.INVISIBLE);
    }

    private void setUpWebView(final int margin) {
	final LinearLayout webViewContainer = new LinearLayout(this.getContext());
	this.mWebView = new WebView(this.getContext());
	this.mWebView.setVerticalScrollBarEnabled(false);
	this.mWebView.setHorizontalScrollBarEnabled(false);
	this.mWebView.setWebViewClient(new FbDialog.FbWebViewClient());
	this.mWebView.getSettings().setJavaScriptEnabled(true);
	this.mWebView.loadUrl(this.mUrl);
	this.mWebView.setLayoutParams(FbDialog.FILL);
	this.mWebView.setVisibility(View.INVISIBLE);

	webViewContainer.setPadding(margin, margin, margin, margin);
	webViewContainer.addView(this.mWebView);
	this.mContent.addView(webViewContainer);
    }
}
