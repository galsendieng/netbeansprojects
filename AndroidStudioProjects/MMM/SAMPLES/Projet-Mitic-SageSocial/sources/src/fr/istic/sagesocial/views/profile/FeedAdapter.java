package fr.istic.sagesocial.views.profile;


import java.util.Date;
import java.util.HashMap;
import java.util.List;

import android.content.Context;
import android.net.Uri;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import fr.istic.sagesocial.R;
import fr.istic.sagesocial.engine.AbstractService;
import fr.istic.sagesocial.engine.bean.FeedBean;
import fr.istic.sagesocial.engine.bean.FriendBean;
import fr.istic.sagesocial.engine.bean.PhotoBean;
import fr.istic.sagesocial.engine.bean.ServiceBean;

public class FeedAdapter extends BaseAdapter {
    private final List<FeedBean> listFeed;
    private final LayoutInflater layoutInflater;
    private final Context context;
    private final FriendBean profile;

    private final HashMap<PhotoBean, ImageView> imageToUpdateFeeds;

    public FeedAdapter(Context context, List<FeedBean> listFeed, FriendBean profile) {
	layoutInflater = LayoutInflater.from(context);
	this.listFeed = listFeed;
	this.context = context;
	this.profile = profile;

	imageToUpdateFeeds = new HashMap<PhotoBean, ImageView>();
    }

    @Override
    public int getCount() {
	return listFeed.size();
    }

    @Override
    public Object getItem(int position) {
	return listFeed.get(position);
    }

    @Override
    public long getItemId(int position) {
	return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
	// Log.i("POSITION", position + "");
	convertView = layoutInflater.inflate(R.layout.profile_feed_item, null);
	FeedBean feedCourant = listFeed.get(position);

	TextView contentFeedName = (TextView) convertView
	        .findViewById(R.id.adapter_content_feed_name);
	TextView contentFeed = (TextView) convertView
	        .findViewById(R.id.adapter_content_feed);
	TextView timeAgo = (TextView) convertView
	        .findViewById(R.id.adapter_time_ago_feed);
	ImageView imageService = (ImageView) convertView
	        .findViewById(R.id.adapter_service_image_feed);
	ImageView imageFeed = (ImageView) convertView
	        .findViewById(R.id.adapter_image_feed);

	// calcul between current date and feed date
	String textTimeAgo = "";
	HashMap<String, Long> result = new HashMap<String, Long>();
	long differenceTime = 0;

	Date dateFeed = feedCourant.getCreatedAt();
	long dateFeedTime = dateFeed.getTime() * 1000;
	// Log.i("DATE_FEED", dateFeedTime + "");

	Date currentDate = new Date();
	long currentDateTime = currentDate.getTime();
	// Log.i("CURRENT_DATE", currentDateTime + "");

	differenceTime = currentDateTime - dateFeedTime;

	// Log.i("DIFFERRENCE_DATE", differenceTime + "");

	result.put("SECONDE", differenceTime / 1000);
	result.put("MINUTE", (differenceTime / 1000) / 60);
	result.put("HOUR", ((differenceTime / 1000) / 60) / 60);
	result.put("DAY", (((differenceTime / 1000) / 60) / 60) / 24);
	result.put("WEEK", (((differenceTime / 1000) / 60) / 60) / 24 / 7);
	result.put("MONTH", ((((differenceTime / 1000) / 60) / 60) / 24) / 30);

	if (result.get("MONTH") > 0) {
	    textTimeAgo = result.get("MONTH") + " ";
	    textTimeAgo += parent.getResources().getString(R.string.profile_months);
	}
	else if (result.get("WEEK") > 0) {
	    textTimeAgo = result.get("WEEK") + " ";
	    textTimeAgo += parent.getResources().getString(R.string.profile_weeks);
	}
	else if (result.get("DAY") > 0) {
	    textTimeAgo = result.get("DAY") + " ";
	    textTimeAgo += parent.getResources().getString(R.string.profile_days);
	}
	else if (result.get("HOUR") > 0) {
	    textTimeAgo = result.get("HOUR") + " ";
	    textTimeAgo += parent.getResources().getString(R.string.profile_hours);
	}
	else if (result.get("MINUTE") > 0) {
	    textTimeAgo = result.get("MINUTE") + " ";
	    textTimeAgo += parent.getResources().getString(R.string.profile_minutes);
	}
	else if (result.get("SECONDE") > 0) {
	    textTimeAgo = result.get("SECONDE") + " ";
	    textTimeAgo += parent.getResources().getString(R.string.profile_second);
	}
	textTimeAgo += " "
	        + parent.getResources().getString(R.string.profile_time_ago_message);

	String message = "";
	final FriendBean author = feedCourant.getAuthor();
	contentFeedName.setText(author.getName());

	if (feedCourant.getText() != null && !feedCourant.getText().contentEquals("")) {
	    message += feedCourant.getText();
	}

	if (feedCourant.getDataURI() != null
	        && !feedCourant.getDataURI().contentEquals("")) {
	    message += feedCourant.getDataURI();
	}

	if (feedCourant.getDescription() != null
	        && !feedCourant.getDescription().contentEquals("")) {
	    message += feedCourant.getDescription();
	}

	timeAgo.setText(textTimeAgo);

	final AbstractService service = feedCourant.getService();
	final ServiceBean serviceBean = service.getServiceBean();
	imageService.setImageResource(serviceBean.getDrawable());

	final PhotoBean photo = author.getPhoto();
	if (photo != null) {
	    final String imagePath = photo.getLocalRessource();
	    if (imagePath == null) {
		imageToUpdateFeeds.put(photo, imageFeed);
	    }
	    else {
		imageFeed.setScaleType(ImageView.ScaleType.FIT_XY);
		imageFeed.setImageURI(Uri.parse(imagePath));
	    }
	}

	contentFeed.setText(Html.fromHtml(message));

	return convertView;
    }

    public void update(final PhotoBean photoBean) {
	if (this.imageToUpdateFeeds.get(photoBean) != null) {
	    this.imageToUpdateFeeds.get(photoBean).setScaleType(
		    ImageView.ScaleType.FIT_XY);
	    this.imageToUpdateFeeds.get(photoBean).setImageURI(
		    Uri.parse(photoBean.getLocalRessource()));
	    this.imageToUpdateFeeds.remove(photoBean);
	}
    }

}
