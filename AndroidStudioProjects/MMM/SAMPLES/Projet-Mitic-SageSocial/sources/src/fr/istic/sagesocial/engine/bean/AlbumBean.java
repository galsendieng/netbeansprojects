package fr.istic.sagesocial.engine.bean;


import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class AlbumBean implements Parcelable {

    private ArrayList<PhotoBean> listPhotoBean;
    private PhotoBean coverPicture;
    private String lastUpdate;
    private String nameAlbum;
    private String url;
    private String id;

    public static final Parcelable.Creator<AlbumBean> CREATOR = new Parcelable.Creator<AlbumBean>() {

	@Override
	public AlbumBean createFromParcel(final Parcel source) {
	    return new AlbumBean(source);
	}

	@Override
	public AlbumBean[] newArray(final int size) {
	    return new AlbumBean[size];
	}
    };

    public AlbumBean() {
	super();
    }

    public AlbumBean(final ArrayList<PhotoBean> listPhotoBean,
	    final PhotoBean coverPicture, final String lastUpdate,
	    final String nameAlbum, final String url, final String id) {
	super();

	this.lastUpdate = lastUpdate;
	this.nameAlbum = nameAlbum;
	this.listPhotoBean = listPhotoBean;
	this.coverPicture = coverPicture;
	this.url = url;
	this.id = id;

    }

    public AlbumBean(final Parcel source) {

	this.lastUpdate = source.readString();
	this.nameAlbum = source.readString();
	this.url = source.readString();
	this.id = source.readString();
	this.listPhotoBean = source.readArrayList(PhotoBean.class.getClassLoader());
	this.coverPicture = source.readParcelable(PhotoBean.class.getClassLoader());

    }

    @Override
    public int describeContents() {
	return 5;
    }

    public PhotoBean getCoverPicture() {
	return this.coverPicture;
    }

    public String getId() {
	return this.id;
    }

    public String getLastUpdate() {
	return this.lastUpdate;
    }

    public ArrayList<PhotoBean> getListPhotoBean() {
	return this.listPhotoBean;
    }

    public String getNameAlbum() {
	return this.nameAlbum;
    }

    public String getUrl() {
	return this.url;
    }

    public void setCoverPicture(final PhotoBean coverPicture) {
	this.coverPicture = coverPicture;
    }

    public void setId(final String id) {
	this.id = id;
    }

    public void setLastUpdate(final String lastUpdate) {
	this.lastUpdate = lastUpdate;
    }

    public void setListPhotoBean(final ArrayList<PhotoBean> listPhotoBean) {
	this.listPhotoBean = listPhotoBean;
    }

    public void setNameAlbum(final String nameAlbum) {
	this.nameAlbum = nameAlbum;
    }

    public void setUrl(final String url) {
	this.url = url;
    }

    @Override
    public void writeToParcel(final Parcel dest, final int flags) {
	dest.writeString(this.lastUpdate);
	dest.writeString(this.nameAlbum);
	dest.writeString(this.id);
	dest.writeTypedList(this.listPhotoBean);
	dest.writeParcelable(this.coverPicture, flags);
	dest.writeString(this.url);
    }
}
