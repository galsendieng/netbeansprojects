package fr.istic.sagesocial.views.feedcomments;


import java.util.Collection;
import java.util.List;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import fr.istic.sagesocial.R;
import fr.istic.sagesocial.engine.bean.FeedBean;
import fr.istic.sagesocial.utils.DateUtils;

public class FeedCommentAdapter extends BaseAdapter {

    private Context mContext;
    private int mResource;
    private List<FeedBean> mFeedBeans;
    private LayoutInflater mInflater;

    private int mFragmentLayout;
    private int mFriendImageId;
    private int mTextViewId;
    private int mTimeAgoId;
    private int mServiceImageId;

    /** Constructor */
    public FeedCommentAdapter(Context context, int resource, Collection<FeedBean> feeds) {
	super();
	mContext = context;
	mResource = resource;
	mFeedBeans = (List) feeds;

	mInflater = (LayoutInflater) mContext
	        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	configureAdapter();
    }

    /** Retrieve View for One Item */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
	return createViewFromItem(position, convertView, parent);
    }

    /** Create a View for One Item */
    private View createViewFromItem(int position, View convertView, ViewGroup parent) {
	// Layout and Bean for a feed item
	View view;
	if (convertView == null)
	    view = mInflater.inflate(mResource, parent, false);
	else
	    view = convertView;

	FeedBean feed = (FeedBean) getItem(position);

	// Retrieve the item fields within the layout
	ImageView friendImage = (ImageView) view.findViewById(mFriendImageId);
	TextView text = (TextView) view.findViewById(mTextViewId);
	TextView timeAgo = (TextView) view.findViewById(mTimeAgoId);
	ImageView serviceImage = (ImageView) view.findViewById(mServiceImageId);

	// And fill them
	// Bitmap image =
	// BitmapFactory.decodeFile(feed.getAuthor().getPhoto().getLocalRessource());
	// friendImage.setImageBitmap(image);

	String message = "";
	if (feed.getText() != null && !feed.getText().contentEquals("")) {
	    message += "<p> " + feed.getText() + " </p>";
	    Log.i("FEEDLIST", "Text : " + feed.getText());
	}
	if (feed.getDataURI() != null && !feed.getDataURI().contentEquals("")) {
	    message += "<a href='" + feed.getDataURI() + "'> " + feed.getDataURI()
		    + " </a>";
	    Log.i("FEEDLIST", "URI : " + feed.getDataURI());
	}
	if (feed.getDescription() != null && !feed.getDescription().contentEquals("")) {
	    message += "<p> " + feed.getDescription() + " </p>";
	    Log.i("FEEDLIST", "description : " + feed.getDescription());
	}
	text.setText(Html.fromHtml(message));

	timeAgo.setText(DateUtils.timeAgo(mContext, feed.getCreatedAt()));

	serviceImage.setImageResource(feed.getService().getServiceBean().getDrawable());

	return view;
    }

    @Override
    public int getCount() {
	return mFeedBeans.size();
    }

    @Override
    public Object getItem(int position) {
	return mFeedBeans.get(position);
    }

    @Override
    public long getItemId(int position) {
	return 0;
    }

    /** Configure id's components for the fragment */
    private void configureAdapter() {
	mFragmentLayout = R.layout.comments_feed_selected_fragment;
	mFriendImageId = R.id.comments_identity_image;
	mTextViewId = R.id.comments_content_text;
	mTimeAgoId = R.id.comments_time_ago;
	mServiceImageId = R.id.comments_service_image;
    }

}
