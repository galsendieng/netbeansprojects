package fr.istic.sagesocial.views.add_account;


import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import fr.istic.sagesocial.engine.AbstractService;
import fr.istic.sagesocial.engine.ServiceFactory;

import java.util.Collection;

/**
 * This adapter binds the activated accounts to the list view. An activated
 * account is an account to which the user has already logged in.
 * 
 */
public class ActivatedAccountsListAdapter extends ArrayAdapter<AbstractService> {

    private final Collection<AbstractService> mServiceCollection;
    private final Activity mActivity;

    public ActivatedAccountsListAdapter(final Activity activity,
	    final int listViewResourceId) {
	super(activity, listViewResourceId);
	this.mActivity = activity;
	this.mServiceCollection = ServiceFactory.getAvailableServices(activity);
	for (final AbstractService s : this.mServiceCollection) {
	    this.add(s);
	}

    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {

	if (convertView == null) {
	    convertView = new ActivatedAccountView(this.mActivity, this.getItem(position));
	}

	return convertView;
    }

}
