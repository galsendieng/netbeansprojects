package fr.istic.sagesocial.engine.bean;


import android.os.Parcel;
import android.os.Parcelable;

public class FriendBean implements Parcelable {
    private String name;
    private final String id;
    private PhotoBean photo;

    /**
     * Create Creator object with a builder
     */
    public static final Creator<FriendBean> CREATOR = new Creator<FriendBean>() {
	@Override
	public FriendBean createFromParcel(final Parcel source) {
	    return new FriendBean(source);
	}

	@Override
	public FriendBean[] newArray(final int size) {
	    return new FriendBean[size];
	}
    };

    public FriendBean(final Parcel in) {
	this.id = in.readString();
	this.name = in.readString();
	this.photo = in.readParcelable(PhotoBean.class.getClassLoader());
    }

    public FriendBean(final String id) {
	this.id = id;
    }

    public FriendBean(final String id, final String name, final PhotoBean photo) {
	this.id = id;
	this.name = name;
	this.photo = photo;
    }

    /**
     * Return number of special object
     */
    @Override
    public int describeContents() {
	return 1;
    }

    @Override
    public boolean equals(final Object o) {
	if (o instanceof FriendBean) {
	    final FriendBean f = (FriendBean) o;
	    return (f.id == this.id) && (f.name == this.name);
	}
	return false;
    }

    public String getId() {
	return this.id;
    }

    public String getName() {
	return this.name;
    }

    public PhotoBean getPhoto() {
	return this.photo;
    }

    public void setName(final String name) {
	this.name = name;
    }

    public void setPhoto(final PhotoBean photo) {
	this.photo = photo;
    }

    public void setPhoto(final String photo) {
	this.photo = new PhotoBean(photo);
    }

    /**
     * Used for write object in to a Parcel
     */
    @Override
    public void writeToParcel(final Parcel dest, final int flags) {
	dest.writeString(this.id);
	dest.writeString(this.name);
	dest.writeParcelable(this.photo, flags);
    }
}
