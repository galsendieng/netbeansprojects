package fr.istic.sagesocial.views.allphotos;


import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Gallery;
import android.widget.ImageView;
import fr.istic.sagesocial.R;
import fr.istic.sagesocial.engine.bean.PhotoBean;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Class Adapter for component gallery
 */
public class PictureAdapter extends BaseAdapter {

    int GalItemBg;
    private final Context context;
    private final ArrayList<PhotoBean> listPhoto;
    private final Drawable noImage;
    private final HashMap<PhotoBean, ImageView> imageToUpdate = new HashMap<PhotoBean, ImageView>();

    // Constructor
    public PictureAdapter(final Context mContext, final ArrayList<PhotoBean> mlistPhoto) {
	listPhoto = mlistPhoto;

	context = mContext;
	noImage = context.getResources().getDrawable(R.drawable.no_photo);
	final TypedArray typArray = mContext
	        .obtainStyledAttributes(R.styleable.GalleryTheme);
	GalItemBg = typArray.getResourceId(
	        R.styleable.GalleryTheme_android_galleryItemBackground, 0);
	typArray.recycle();
    }

    // Number of elements
    @Override
    public int getCount() {
	return listPhoto.size();
    }

    // GetItem at index position
    @Override
    public Object getItem(final int position) {
	return listPhoto.get(position);
    }

    // Get index for current object
    @Override
    public long getItemId(final int position) {
	return position;
    }

    // Get view to display
    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {

	convertView = new View(context);
	ImageView imageView = null;

	// If component already exist

	imageView = new ImageView(context);
	final PhotoBean photo = listPhoto.get(position);
	final String imagePath = photo.getLocalRessource();

	if (imagePath == null) {
	    Log.e("Picture Adapter", "add imagePath " + imageView.toString());
	    imageToUpdate.put(photo, imageView);

	}
	else {
	    Log.e("Picture Adapter", "else imagePath " + imagePath);
	    imageView.setImageURI(Uri.parse(imagePath));
	}

	// Image size
	imageView.setLayoutParams(new Gallery.LayoutParams(150, 150));
	imageView.setScaleType(ImageView.ScaleType.FIT_XY);

	// Background
	imageView.setBackgroundResource(GalItemBg);

	return imageView;
    }

    public void update(final PhotoBean photoBean) {

	final ImageView imageView = imageToUpdate.get(photoBean);
	Log.d("Picture Adapter", "imageToUpdate " + imageToUpdate.toString());

	if (imageView != null) {
	    final String imagePath = photoBean.getLocalRessource();
	    Log.d("Picture Adapter", "if " + imagePath);
	    imageView.setImageURI(Uri.parse(imagePath));
	    imageToUpdate.remove(photoBean);
	}
    }
}
