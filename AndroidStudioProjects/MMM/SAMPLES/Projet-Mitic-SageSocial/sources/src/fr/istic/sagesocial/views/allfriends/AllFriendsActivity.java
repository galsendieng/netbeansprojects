package fr.istic.sagesocial.views.allfriends;


import android.app.FragmentManager;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import fr.istic.sagesocial.R;
import fr.istic.sagesocial.engine.AbstractService;
import fr.istic.sagesocial.engine.PhotoManager;
import fr.istic.sagesocial.engine.ServiceFactory;
import fr.istic.sagesocial.engine.bean.FriendBean;
import fr.istic.sagesocial.engine.bean.PhotoBean;
import fr.istic.sagesocial.engine.listener.PhotoManagerListener;
import fr.istic.sagesocial.engine.listener.ServiceCallback;
import fr.istic.sagesocial.utils.WidgetUtils;
import fr.istic.sagesocial.views.MenuManagerActivity;
import fr.istic.sagesocial.views.help.HelpActivity;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;

public class AllFriendsActivity extends MenuManagerActivity {

    private HashMap<String, FriendBean[]> mFriendsHashMap;
    private String[] mGroups;
    private PhotoManager photoManager;

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
	if (item.getItemId() == R.id.menu_item_help) {
	    final Intent intent = new Intent(this, HelpActivity.class);
	    intent.putExtra("helpView", "friends");
	    this.startActivity(intent);
	}
	return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);

	WidgetUtils.setProgressBarVisibility(this, true);

	FriendBean profil = null;
	final Bundle extras = this.getIntent().getExtras();
	if (extras != null) {
	    profil = extras.getParcelable("profile");
	}

	this.setContentView(R.layout.allfriends);
	final FragmentManager fm = this.getFragmentManager();
	this.mFriendsHashMap = new HashMap<String, FriendBean[]>();
	final Collection<AbstractService> services = ServiceFactory
	        .getRegisteredServices(this);

	// retrieve the list of groups
	final AllFriendsListFragment listeFragment = (AllFriendsListFragment) fm
	        .findFragmentById(R.id.allfriends_list_fragment);
	// in order to optimize the code, we put the service label into the
	// hashmap and init the fragment (dont do this in the callback!!)
	this.mGroups = new String[services.size()];
	int i = 0;
	for (final AbstractService service : services) {
	    this.mGroups[i++] = service.getServiceBean().getLabel();
	}
	listeFragment.init(this.mGroups);

	// retrieve the grid
	final AllFriendsGridFragment gridFragment = (AllFriendsGridFragment) fm
	        .findFragmentById(R.id.allfriends_grid_fragment);

	for (final AbstractService service : services) {

	    service.getFriends((profil == null ? "me" : profil.getId()), 0,
		    AbstractService.MAX_FRIENDS, new ServiceCallback() {
		        @Override
		        public void onUpdateData(final Object data) {

			    AllFriendsActivity.this.runOnUiThread(new Runnable() {
			        @Override
			        public void run() {

				    WidgetUtils.setProgressBarVisibility(
				            AllFriendsActivity.this, false);

				    final List<FriendBean> list = (List<FriendBean>) data;

				    AllFriendsActivity.this.mFriendsHashMap.put(service
				            .getServiceBean().getLabel(), list
				            .toArray(new FriendBean[list.size()]));

				    if (AllFriendsActivity.this.mFriendsHashMap.size() == services
				            .size()) {

				        gridFragment.init(
				                AllFriendsActivity.this.mFriendsHashMap,
				                AllFriendsActivity.this.mGroups);

				    }

			        }
			    });
		        }
		    });
	}

	this.photoManager = PhotoManager.getInstance(this);

	// listeners
	this.photoManager.addListener(new PhotoManagerListener() {

	    @Override
	    public void dataChanged(final PhotoBean photoBean) {
		if (AllFriendsActivity.this.mFriendsHashMap != null) {
		    gridFragment.update(photoBean);
		}
	    }
	});

    }

    @Override
    protected void onDestroy() {
	PhotoManager.getInstance(this).destroy();
	super.onDestroy();
    }

    @Override
    protected void onPause() {
	super.onPause();
	this.photoManager.destroy();
    }

    @Override
    protected void onResume() {
	super.onResume();
    }

}
