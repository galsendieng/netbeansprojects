package fr.istic.sagesocial.views.feedcomments;


import java.util.ArrayList;
import java.util.Collection;

import android.app.FragmentManager;
import android.os.Bundle;
import android.util.Log;
import fr.istic.sagesocial.ApplicationConfig;
import fr.istic.sagesocial.R;
import fr.istic.sagesocial.SageSocialActivity;
import fr.istic.sagesocial.engine.AbstractService;
import fr.istic.sagesocial.engine.ServiceFactory;
import fr.istic.sagesocial.engine.bean.FeedBean;
import fr.istic.sagesocial.engine.listener.ServiceCallback;
import fr.istic.sagesocial.views.MenuManagerActivity;

/** Activity for the feed comments view */
public class CommentsActivity extends MenuManagerActivity {

    private String mProfileId; // Given by profileActivity
    private FeedBean mSelectedFeed; // Given by ProfileActivity
    private Collection<FeedBean> mProfileFeeds; // linked to mProfile
    private Collection<FeedBean> mFeedComments; // linked to mSelectedFeed
    private final Collection<AbstractService> mServices = ServiceFactory
	    .getRegisteredServices(this); // Available services list

    private FeedsFragment feedsListFragment;
    private CommentsFragment commentsListFragment;

    private int mFeedsId;
    private int mCommentsId;
    private int serviceCpt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	setContentView(R.layout.comments);
	configureActivity();
	// XXX remove when this is done in the first view
	ApplicationConfig.installAppDirectory(this);

	final FragmentManager fm = this.getFragmentManager();
	// Left side fragment of the view
	feedsListFragment = (FeedsFragment) fm.findFragmentById(mFeedsId);
	// Right side fragments of the view
	commentsListFragment = (CommentsFragment) fm.findFragmentById(mCommentsId);
    }

    @Override
    protected void onStart() {
	super.onStart();

	// TODO Ajout listener FeedListener, CommentListener

	// Retrieve the profile FriendBean
	// TODO init profile in ProfileActivity before retrieve feedList
	// mProfileId = this.getIntent().getExtras().getParcelable("profile");
	mProfileId = this.getIntent().getExtras().getString("profile");
	// for (final AbstractService currentService : mServices) {
	// mProfileId = SageSocialActivity.mySelf.get(currentService).getId();
	// }
	mProfileFeeds = new ArrayList<FeedBean>();
	mFeedComments = new ArrayList<FeedBean>();

	refreshProfileFeeds();
	// mProfileFeeds = FeedsNFriendsTest.initializeFeedsTest(this); //XXX
	// initTest
	// feedsListFragment.fill(CommentsActivity.this, mProfileFeeds); //XXX
	// initTest

	// Retrieve the selected feedId parcelabled & look for a FeedBean

	// mFeedComments = FeedsNFriendsTest.initializeCommentsTest(this); //XXX
	// initTest
	// commentsListFragment.fill(CommentsActivity.this, mFeedComments);
	// //XXX initTest

    }

    @Override
    protected void onStop() {
	super.onStop();
    }

    protected void refreshSelectedFeed() {
	String selectedFeedId = this.getIntent().getExtras().getString("selectedFeed");
	mSelectedFeed = searchFeedById(selectedFeedId);
	setSelectFeed(mSelectedFeed);
	refreshAssociatedComments();
    }

    /** Refresh the comments list associated to the profile */
    protected void refreshProfileFeeds() {
	serviceCpt = 0;
	for (final AbstractService service : mServices) {
	    service.getMessages(mProfileId, 15, new ServiceCallback() {
		@Override
		public void onUpdateData(final Object data) {
		    CommentsActivity.this.runOnUiThread(new Runnable() {
			@Override
			public void run() {
			    final Collection<FeedBean> listFeeds = (Collection<FeedBean>) data;
			    mProfileFeeds.addAll(listFeeds);
			    Log.i("CommentsActivity", "Feeds found, size = "
				    + mProfileFeeds.size() + "");
			    if (serviceCpt == mServices.size()) {
				feedsListFragment.fill(CommentsActivity.this,
				        mProfileFeeds);
			    }
			}
		    });
		}
	    });
	    serviceCpt++;
	}
    }

    /** Refresh the comments list associated to the selected Feed */
    protected void refreshAssociatedComments() {
	serviceCpt = 0;
	for (final AbstractService service : mServices) {
	    service.getComments(mSelectedFeed.getId(), 0, 15, new ServiceCallback() {
		@Override
		public void onUpdateData(final Object data) {
		    CommentsActivity.this.runOnUiThread(new Runnable() {
			@Override
			public void run() {
			    final Collection<FeedBean> listComments = (Collection<FeedBean>) data;
			    mFeedComments.addAll(listComments);
			    Log.i("Comments found for the selected feed, size = ",
				    mFeedComments.size() + "");
			    if (serviceCpt == mServices.size()) {
				commentsListFragment.fill(CommentsActivity.this,
				        mFeedComments);
			    }
			}
		    });
		}
	    });
	    serviceCpt++;
	}
    }

    public FeedBean getSelectedFeed() {
	return mSelectedFeed;
    }

    /**
     * Update the selected feed within fragments
     * 
     * @param feed
     *            The Selected FeedBean
     */
    public void setSelectFeed(final FeedBean feed) {
	feedsListFragment.setSelectFeed(feed);
	commentsListFragment.setSelectFeed(feed);
    }

    /** Retrieve the registered services */
    public Collection<AbstractService> getServices() {
	return mServices;
    }

    /* PRIVATE ACTIONS */

    /**
     * Retrieve the selected feed
     * 
     * @param feedId
     *            the feed String Id
     */
    private FeedBean getSelectedFeed(final String feedId) {
	mSelectedFeed = searchFeedById(feedId);
	return mSelectedFeed;
    }

    /**
     * Looking for a FeedBean by Id field
     * 
     * @param feedId
     *            the feed String Id
     */
    private FeedBean searchFeedById(final String feedId) {
	FeedBean result = null;
	// for (FeedBean aFeed : mProfileFeeds) {
	// result = (aFeed.getId().equals(feedId)) ? aFeed : null;
	// }
	result = (FeedBean) ((ArrayList) mProfileFeeds).get(0);
	return result;
    }

    /** Configure the Activity resources */
    private void configureActivity() {
	mFeedsId = R.id.comments_feeds_list_fragment;
	mCommentsId = R.id.comments_comments_list_fragment;
    }

}