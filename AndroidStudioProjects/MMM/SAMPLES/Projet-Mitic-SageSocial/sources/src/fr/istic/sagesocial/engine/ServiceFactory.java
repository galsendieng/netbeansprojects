/**
 * ServiceFactory.java - Feb 10, 2012
 * 
 * @author Wassim Chegham
 */
package fr.istic.sagesocial.engine;


import android.app.Activity;
import fr.istic.sagesocial.R;
import fr.istic.sagesocial.engine.bean.ServiceBean;
import fr.istic.sagesocial.utils.ParserUtils;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;

/**
 * This is a factory that
 * 
 * @author Project M2 MITIC
 * 
 */
public class ServiceFactory {

    private static final Collection<AbstractService> xmlServices = new ArrayList<AbstractService>();
    private static final Collection<AbstractService> registeredServices = new ArrayList<AbstractService>();
    private static boolean xmlServicesInitialized = false;

    /**
     * This method returns all the available social services in the xml file. An
     * available social service is a social service which has been declared by
     * the developer in the xml file and set correctly.
     * 
     * @return All available services.
     */
    public static Collection<AbstractService> getAvailableServices(final Activity activity) {

	// initialize the list of services
	if (!ServiceFactory.xmlServicesInitialized) {
	    final Collection<ServiceBean> serviceBeans = ParserUtils
		    .parseServicesConfigFile(activity, R.xml.services);
	    final ClassLoader classLoader = ServiceBean.class.getClassLoader();
	    Class<?> aClass;

	    for (final ServiceBean serviceBean : serviceBeans) {

		try {

		    aClass = classLoader.loadClass(serviceBean.getClassName());
		    final AbstractService service = (AbstractService) aClass
			    .getConstructor(Activity.class).newInstance(activity);
		    service.setServiceBean(serviceBean);
		    ServiceFactory.xmlServices.add(service);

		}
		catch (final IllegalArgumentException e) {
		    e.printStackTrace();
		}
		catch (final InstantiationException e) {
		    e.printStackTrace();
		}
		catch (final IllegalAccessException e) {
		    e.printStackTrace();
		}
		catch (final InvocationTargetException e) {
		    e.printStackTrace();
		}
		catch (final ClassNotFoundException e) {
		    e.printStackTrace();
		}
		catch (final SecurityException e) {
		    e.printStackTrace();
		}
		catch (final NoSuchMethodException e) {
		    e.printStackTrace();
		}
	    }
	    ServiceFactory.xmlServicesInitialized = true;
	}
	else {
	    for (final AbstractService service : ServiceFactory.xmlServices) {
		service.setActivity(activity);
	    }

	}

	return ServiceFactory.xmlServices;
    }

    /**
     * This convenient method returns all registered social services from those
     * declared in the xml file. A registered social service is a social service
     * which has been activated (linked) by the user, and whose state is stored
     * in the sharedPreferences.
     * 
     * @return All registered services.
     */
    public static Collection<AbstractService> getRegisteredServices(
	    final Activity activity) {

	final Collection<AbstractService> availableServices = ServiceFactory
	        .getAvailableServices(activity);

	ServiceFactory.registeredServices.clear();

	for (final AbstractService service : availableServices) {
	    if ((service != null) && service.isSessionValid()) {
		ServiceFactory.registeredServices.add(service);
	    }
	}

	return ServiceFactory.registeredServices;
    }
}
