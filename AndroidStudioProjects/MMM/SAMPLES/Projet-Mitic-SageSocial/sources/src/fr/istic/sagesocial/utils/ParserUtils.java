package fr.istic.sagesocial.utils;


import android.content.Context;
import android.util.Log;
import fr.istic.sagesocial.engine.bean.ServiceBean;

import org.xmlpull.v1.XmlPullParser;

import java.util.ArrayList;
import java.util.Collection;

public class ParserUtils {

    public static Collection<ServiceBean> parseServicesConfigFile(final Context context,
	    final int xmlRessourceId) {
	final ArrayList<ServiceBean> services = new ArrayList<ServiceBean>();

	try {

	    final XmlPullParser xpp = context.getResources().getXml(xmlRessourceId);

	    int e = xpp.getEventType();
	    while (e != XmlPullParser.END_DOCUMENT) {
		if (e == XmlPullParser.START_TAG) {

		    if (xpp.getName().equals("service")) {

			final String serviceId = xpp.getAttributeValue(null, "id");
			final String serviceClass = xpp.getAttributeValue(null, "class");
			final String serviceDrawable = xpp.getAttributeValue(null,
			        "drawable");
			final String serviceLabel = xpp.getAttributeValue(null, "label");

			final ServiceBean serviceBean = new ServiceBean();
			serviceBean.setId(serviceId);
			serviceBean.setLabel(serviceLabel);
			serviceBean.setClassName(serviceClass);
			serviceBean.setDrawable(context.getResources().getIdentifier(
			        serviceDrawable, "drawable", context.getPackageName()));

			services.add(serviceBean);

			Log.i("ParserUtils", "Found service: id=" + serviceId
			        + ", class=" + serviceClass + ", label=" + serviceLabel
			        + ", drawable=" + serviceDrawable);

		    }
		}
		e = xpp.next();
	    } // while

	}
	catch (final Throwable t) {
	    t.printStackTrace();
	}

	return services;

    }
}
