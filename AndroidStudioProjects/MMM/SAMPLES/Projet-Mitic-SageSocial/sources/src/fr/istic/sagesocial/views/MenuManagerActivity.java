package fr.istic.sagesocial.views;


import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import fr.istic.sagesocial.R;
import fr.istic.sagesocial.views.profile.ProfileActivity;

import java.util.HashMap;

public class MenuManagerActivity extends Activity {

    private HashMap<Integer, Intent> itemAction;

    /**
     * Create the menu.
     */
    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
	super.onCreateOptionsMenu(menu);

	final MenuInflater inflater = this.getMenuInflater();
	inflater.inflate(R.menu.menu, menu);

	this.itemAction = new HashMap<Integer, Intent>();
	// add here intent for all menu items
	this.itemAction.put(R.id.menu_item_prefs, new Intent(
	        this.getApplicationContext(), PreferencesActivity.class));

	this.itemAction.put(R.id.menu_item_myprofile,
	        new Intent(this.getApplicationContext(), ProfileActivity.class));

	this.itemAction.put(android.R.id.home, new Intent(this.getApplicationContext(),
	        ProfileActivity.class));

	return true;
    }

    /**
     * Method called when the user clicked on a menu item. Choose what to do
     * with the clicked item.
     */
    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {

	final Intent intent = this.itemAction.get(item.getItemId());

	if (intent != null) {
	    this.startActivity(intent);
	}

	return true;
    }
}
