package fr.istic.sagesocial.views;


import android.content.Intent;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceChangeListener;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import fr.istic.sagesocial.ApplicationConfig;
import fr.istic.sagesocial.R;
import fr.istic.sagesocial.SageSocialActivity;
import fr.istic.sagesocial.utils.WidgetUtils;
import fr.istic.sagesocial.views.profile.ProfileActivity;

public class PreferencesActivity extends PreferenceActivity {

    @Override
    public void onCreate(final Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	this.addPreferencesFromResource(R.xml.preferences);
	String versionName;
	try {
	    versionName = this.getPackageManager().getPackageInfo(this.getPackageName(),
		    0).versionName;
	    this.findPreference("app_version").setSummary(versionName);
	}
	catch (final NameNotFoundException e) {
	}

	this.findPreference("switch_to_addaccount").setOnPreferenceClickListener(
	        new OnPreferenceClickListener() {

		    @Override
		    public boolean onPreferenceClick(final Preference preference) {

		        PreferencesActivity.this.startActivity(new Intent(
		                PreferencesActivity.this, SageSocialActivity.class));
		        return true;
		    }

	        });

	this.findPreference("app_about").setOnPreferenceClickListener(
	        new OnPreferenceClickListener() {

		    @Override
		    public boolean onPreferenceClick(final Preference preference) {

		        WidgetUtils.showDialog(PreferencesActivity.this,
		                PreferencesActivity.this
		                        .getString(R.string.prefs_about_app),
		                PreferencesActivity.this
		                        .getString(R.string.prefs_about_app_text));

		        return true;
		    }
	        });

	this.findPreference("clear_cache").setOnPreferenceClickListener(
	        new OnPreferenceClickListener() {

		    @Override
		    public boolean onPreferenceClick(final Preference preference) {
		        ApplicationConfig.clearCache(PreferencesActivity.this);
		        WidgetUtils.showToast(PreferencesActivity.this,
		                PreferencesActivity.this
		                        .getString(R.string.clear_cache_done));
		        return true;
		    }
	        });

	this.findPreference("refresh_feed_timer").setOnPreferenceChangeListener(
	        new OnPreferenceChangeListener() {

		    @Override
		    public boolean onPreferenceChange(final Preference preference,
		            final Object newValue) {
		        final String timer = PreferenceManager
		                .getDefaultSharedPreferences(PreferencesActivity.this)
		                .getString("refresh_feed_timer", "1");
		        final int timerPrefs = Integer.parseInt(timer);
		        return ProfileActivity.updateFeedUpdateTimerPrefs(timerPrefs);
		    }
	        });

    }
}