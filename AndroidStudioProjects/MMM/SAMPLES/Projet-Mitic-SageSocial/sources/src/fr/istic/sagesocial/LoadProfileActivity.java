package fr.istic.sagesocial;


import android.app.Activity;
import android.os.Bundle;
import fr.istic.sagesocial.engine.PhotoManager;

public class LoadProfileActivity extends Activity {

    @Override
    public void onCreate(final Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	this.setContentView(R.layout.load_profile);
	ExecuteTaskLoadProfile.loadProfile(this);
    }

    @Override
    protected void onDestroy() {
	PhotoManager.getInstance(this).destroy();
	super.onDestroy();
    }

    @Override
    protected void onPause() {
	super.onPause();
	PhotoManager.getInstance(this).destroy();
	this.finish();
    }

}
