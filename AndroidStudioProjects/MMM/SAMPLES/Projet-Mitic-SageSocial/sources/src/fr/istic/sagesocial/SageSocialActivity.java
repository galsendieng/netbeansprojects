package fr.istic.sagesocial;


import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ListView;
import fr.istic.sagesocial.engine.AbstractService;
import fr.istic.sagesocial.engine.ServiceFactory;
import fr.istic.sagesocial.engine.bean.FriendBean;
import fr.istic.sagesocial.engine.listener.OnClickStartActivity;
import fr.istic.sagesocial.utils.WidgetUtils;
import fr.istic.sagesocial.views.MenuManagerActivity;
import fr.istic.sagesocial.views.add_account.ActivatedAccountsListAdapter;
import fr.istic.sagesocial.views.help.HelpActivity;
import fr.istic.sagesocial.views.profile.ProfileActivity;

import java.util.Collection;
import java.util.Map;

public class SageSocialActivity extends MenuManagerActivity {

    public static Map<AbstractService, FriendBean> mySelf;
    public static ProfileActivity currentProfileActivity;

    private ListView mAccountList;

    private Collection<AbstractService> mRegisteredServices;
    private Button mStartButton;

    public void disableStartButton() {

	this.runOnUiThread(new Runnable() {

	    @Override
	    public void run() {
		SageSocialActivity.this.mStartButton
		        .setBackgroundResource(R.color.gray_normal_normal);
		SageSocialActivity.this.mStartButton
		        .setOnClickListener(new OnClickListener() {
			    @Override
			    public void onClick(final View v) {
			        WidgetUtils.showDialog(SageSocialActivity.this,
			                "Message", "Please connect your accounts first!");
			    }
		        });
	    }
	});
    }

    public void enableStartButton() {
	this.runOnUiThread(new Runnable() {

	    @Override
	    public void run() {
		SageSocialActivity.this.mStartButton
		        .setBackgroundResource(R.color.green_normal_start);
		SageSocialActivity.this.mStartButton
		        .setOnClickListener(new OnClickStartActivity(
		                SageSocialActivity.this, LoadProfileActivity.class));
	    }
	});
    }

    @Override
    public void onActivityResult(final int requestCode, final int resultCode,
	    final Intent data) {
	super.onActivityResult(requestCode, resultCode, data);

	for (final AbstractService abstractService : this.mRegisteredServices) {
	    abstractService.authorizeCallback(requestCode, resultCode, data);
	}
	this.updateStartButton();
    }

    @Override
    public void onCreate(final Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	this.setContentView(R.layout.link);

	this.mRegisteredServices = ServiceFactory.getRegisteredServices(this);

	this.mAccountList = (ListView) this.findViewById(R.id.link_listviewlink);
	this.mAccountList.setAdapter(new ActivatedAccountsListAdapter(this,
	        R.layout.listview_link));

	this.mStartButton = (Button) this.findViewById(R.id.link_button_start);
	this.updateStartButton();
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
	final boolean res = super.onCreateOptionsMenu(menu);
	if (this.mRegisteredServices.size() == 0) {
	    menu.removeItem(R.id.menu_item_myprofile);
	}
	return res;
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
	if (item.getItemId() == R.id.menu_item_help) {
	    final Intent intent = new Intent(this, HelpActivity.class);
	    intent.putExtra("helpView", "accounts");
	    this.startActivity(intent);
	}
	return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume() {
	super.onResume();
	for (final AbstractService abstractService : this.mRegisteredServices) {
	    abstractService.extendAccessTokenIfNeeded();
	}
    }

    private void updateStartButton() {
	if (this.mRegisteredServices.size() > 0) {
	    this.enableStartButton();
	}
	else {
	    this.disableStartButton();
	}
    }
}
