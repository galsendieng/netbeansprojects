package fr.istic.sagesocial.views;


import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import fr.istic.sagesocial.R;
import fr.istic.sagesocial.engine.AbstractService;
import fr.istic.sagesocial.engine.ServiceFactory;
import fr.istic.sagesocial.engine.bean.FriendBean;
import fr.istic.sagesocial.engine.listener.ServiceCallback;

import java.util.ArrayList;
import java.util.List;

public class FacebookActivity_test extends Activity {

    ArrayList<AbstractService> mRegisteredServices;
    private SharedPreferences mPrefs;
    private List<FriendBean> friends;

    @Override
    public void onActivityResult(final int requestCode, final int resultCode,
	    final Intent data) {
	super.onActivityResult(requestCode, resultCode, data);

	for (int i = 0; i < this.mRegisteredServices.size(); i++) {
	    this.mRegisteredServices.get(i).authorizeCallback(requestCode, resultCode,
		    data);
	}
    }

    @Override
    public void onCreate(final Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	this.setContentView(R.layout.facebook_test);
	this.mRegisteredServices = (ArrayList<AbstractService>) ServiceFactory
	        .getRegisteredServices(this);

	this.findViewById(R.id.button1).setOnClickListener(new OnClickListener() {
	    @Override
	    public void onClick(final View v) {
		FacebookActivity_test.this.mRegisteredServices.get(0).login(
		        new ServiceCallback() {

			    @Override
			    public void onUpdateData(final Object data) {
			        // TODO Auto-generated method stub

			    }
		        });
	    }
	});

	this.findViewById(R.id.button2).setOnClickListener(new OnClickListener() {
	    @Override
	    public void onClick(final View v) {
		FacebookActivity_test.this.mRegisteredServices.get(0).logout(
		        new ServiceCallback() {

			    @Override
			    public void onUpdateData(final Object data) {
			        // TODO Auto-generated method stub

			    }
		        });
	    }
	});

	this.findViewById(R.id.button3).setOnClickListener(new OnClickListener() {

	    @Override
	    public void onClick(final View v) {
		FacebookActivity_test.this.mRegisteredServices.get(0).getFriends(0, 10,
		        new ServiceCallback() {

			    @Override
			    public void onUpdateData(final Object data) {
			        // TODO Auto-generated method stub
			        final List<FriendBean> friends = (List<FriendBean>) data;
			    }
		        });
	    }
	});

    }

    @Override
    public void onResume() {
	super.onResume();
	for (int i = 0; i < this.mRegisteredServices.size(); i++) {
	    this.mRegisteredServices.get(i).extendAccessTokenIfNeeded();
	}
    }
}
