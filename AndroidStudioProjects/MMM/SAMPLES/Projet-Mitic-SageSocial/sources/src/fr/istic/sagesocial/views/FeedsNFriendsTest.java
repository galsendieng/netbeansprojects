package fr.istic.sagesocial.views;


import android.app.Activity;
import android.content.Context;
import fr.istic.sagesocial.ApplicationConfig;
import fr.istic.sagesocial.engine.bean.FeedBean;
import fr.istic.sagesocial.engine.bean.FriendBean;
import fr.istic.sagesocial.engine.bean.PhotoBean;
import fr.istic.sagesocial.models.FacebookService;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

public class FeedsNFriendsTest {

    public static Collection<FeedBean> initializeCommentsTest(final Activity activity) {

	final Collection<FeedBean> initialFeeds = new ArrayList<FeedBean>();

	@SuppressWarnings({ "unchecked", "rawtypes" })
	final Collection<FriendBean> someFriends = FeedsNFriendsTest
	        .initializeFriendsTest(activity);

	final FacebookService facebook = new FacebookService(activity);

	final String aFeed = "Commentarum est una porca sit amet, consectetuer adipiscing elit.Aenean comodo ligula1.";

	int day = 10;
	for (final FriendBean aFriend : someFriends) {
	    initialFeeds.add(new FeedBean("", aFeed + aFriend.getName(), new Date(
		    "2012/01/" + day + " 17:00"), aFriend, facebook));
	    day++;
	}

	return initialFeeds;
    }

    public static Collection<FeedBean> initializeFeedsTest(final Activity activity) {

	final Collection<FeedBean> initialFeeds = new ArrayList<FeedBean>();

	@SuppressWarnings({ "unchecked", "rawtypes" })
	final Collection<FriendBean> someFriends = FeedsNFriendsTest
	        .initializeFriendsTest(activity);

	final FacebookService facebook = new FacebookService(activity);

	final String aFeed = "Lorem ipsum dolor sit amet, consectetuer adipiscing elit.Aenean comodo ligula1.";

	int day = 10;
	for (final FriendBean aFriend : someFriends) {
	    initialFeeds.add(new FeedBean("", aFeed + aFriend.getName(), new Date(
		    "2012/01/" + day + " 17:00"), aFriend, facebook));
	    day++;
	}

	return initialFeeds;
    }

    public static Collection<FriendBean> initializeFriendsTest(final Context context) {
	final List<FriendBean> initialFriends = new ArrayList<FriendBean>();

	initialFriends.add(new FriendBean("1", "Eken",
	        new PhotoBean(
	                "http://upload.wikimedia.org/wikipedia/commons/d/d2/Babar.jpg",
	                ApplicationConfig.getAppDirectory(context) + File.separator
	                        + "Babar.jpg")));
	initialFriends.add(new FriendBean("2", "Tux",
	        new PhotoBean(
	                "http://upload.wikimedia.org/wikipedia/commons/d/d2/Babar.jpg",
	                ApplicationConfig.getAppDirectory(context) + File.separator
	                        + "Babar.jpg")));
	initialFriends.add(new FriendBean("3", "Yoshi",
	        new PhotoBean(
	                "http://upload.wikimedia.org/wikipedia/commons/d/d2/Babar.jpg",
	                ApplicationConfig.getAppDirectory(context) + File.separator
	                        + "Babar.jpg")));
	initialFriends.add(new FriendBean("4", "Robert",
	        new PhotoBean(
	                "http://upload.wikimedia.org/wikipedia/commons/d/d2/Babar.jpg",
	                ApplicationConfig.getAppDirectory(context) + File.separator
	                        + "Babar.jpg")));
	initialFriends.add(new FriendBean("5", "Babar",
	        new PhotoBean(
	                "http://upload.wikimedia.org/wikipedia/commons/d/d2/Babar.jpg",
	                ApplicationConfig.getAppDirectory(context) + File.separator
	                        + "Babar.jpg")));

	return initialFriends;
    }

}
