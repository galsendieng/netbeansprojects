package fr.istic.sagesocial;


import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;
import fr.istic.sagesocial.engine.ServiceFactory;
import fr.istic.sagesocial.engine.listener.OnClickStartActivity;
import fr.istic.sagesocial.utils.WidgetUtils;

import java.io.File;

public class ApplicationConfig {

    public static void checkRegisteredServices(final Activity activity) {
	if (ServiceFactory.getRegisteredServices(activity).size() <= 0) {

	    WidgetUtils.showToast(activity,
		    "You need to be connected to your account first.");

	    (new OnClickStartActivity(activity, SageSocialActivity.class)).onClick(null);
	}
    }

    public static void clearCache(final Context context) {
	final File cache = new File(Environment.getExternalStorageDirectory()
	        + File.separator + context.getString(R.string.app_name) + File.separator
	        + "cache");
	for (final File f : cache.listFiles()) {
	    f.delete();
	}
	cache.mkdirs();
    }

    public static String getAppDirectory(final Context context) {
	return Environment.getExternalStorageDirectory() + File.separator
	        + context.getString(R.string.app_name);
    }

    public static void installAppDirectory(final Context context) {
	if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
	    final File directory = new File(Environment.getExternalStorageDirectory()
		    + File.separator + context.getString(R.string.app_name));
	    directory.mkdirs();

	}
    }

    public static boolean isNetworkAvailable(final Context context) {
	final ConnectivityManager connectivityManager = (ConnectivityManager) context
	        .getSystemService(Context.CONNECTIVITY_SERVICE);
	final NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
	return activeNetworkInfo != null;
    }
}
