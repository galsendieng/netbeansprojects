package fr.istic.sagesocial.views.allfriends;


import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import fr.istic.sagesocial.R;
import fr.istic.sagesocial.engine.bean.FriendBean;
import fr.istic.sagesocial.engine.bean.PhotoBean;

import java.util.HashMap;
import java.util.Map;

public class AllFriendsAdapter extends BaseAdapter {
    private final LayoutInflater layoutInflater;

    private final FriendBean[] friends;
    private final OnClickListener onClickViewListener;
    private final Map<PhotoBean, ImageView> imageToUpdate;

    /**
     * Create a BaseAdapter for the AllFriendsGridView. It is used to fill all
     * case of the grid with a view. This view contains the picture of a friend,
     * and it name.
     * 
     * @param context
     * @param friends
     *            the array containing all friends to show in the grid
     * @param onClickViewListener
     */
    public AllFriendsAdapter(final Context context, final FriendBean[] friends,
	    final OnClickListener onClickViewListener) {

	this.friends = friends;
	this.onClickViewListener = onClickViewListener;

	// create the inflater to create the view in getView
	this.layoutInflater = (LayoutInflater) context
	        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

	this.imageToUpdate = new HashMap<PhotoBean, ImageView>();

    }

    @Override
    public int getCount() {
	return this.friends.length;
    }

    @Override
    public Object getItem(final int position) {
	return this.friends[position];
    }

    @Override
    public long getItemId(final int position) {
	return 0;
    }

    /**
     * Return a view that contains the picture of a friend and it's name
     */
    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {

	if (convertView == null) {
	    // set up the view
	    return createView(position, convertView, parent);

	}
	else {
	    final TextView textView = (TextView) convertView
		    .findViewById(R.id.allfriends_grid_item_text);
	    final String text = textView.getText().toString();
	    if (!text.equals(this.friends[position].getName())) {
		return createView(position, convertView, parent);
	    }
	}
	return convertView;
    }

    private View createView(int position, View convertView, final ViewGroup parent) {

	convertView = this.layoutInflater.inflate(R.layout.allfriends_grid_item, parent,
	        false);
	convertView.setTag(this.friends[position]);

	final ImageView imageView = (ImageView) convertView
	        .findViewById(R.id.allfriends_grid_item_image);

	// create the bitmap from the image
	final PhotoBean photo = this.friends[position].getPhoto();
	final String imagePath = photo.getLocalRessource();

	// the picture is null, it's because the file is not yet downloaded
	// by the PhotoManager. We add it to an array to add the bitmap
	// later.
	if (imagePath == null) {
	    this.imageToUpdate.put(photo, imageView);
	}
	else {
	    imageView.setImageURI(Uri.parse(imagePath));
	}

	// create the text
	final TextView textView = (TextView) convertView
	        .findViewById(R.id.allfriends_grid_item_text);
	textView.append(this.friends[position].getName());

	// add the action listener (it is declared in the
	// AllFriendsGridFragment class)
	convertView.setOnClickListener(this.onClickViewListener);

	return convertView;
    }

    public void update(final PhotoBean photoBean) {
	final ImageView imageView = this.imageToUpdate.get(photoBean);
	// the image is not in a view that is not displayed yet
	if (imageView != null) {
	    final String imagePath = photoBean.getLocalRessource();
	    imageView.setImageURI(Uri.parse(imagePath));
	    this.imageToUpdate.remove(photoBean);
	}
    }
}