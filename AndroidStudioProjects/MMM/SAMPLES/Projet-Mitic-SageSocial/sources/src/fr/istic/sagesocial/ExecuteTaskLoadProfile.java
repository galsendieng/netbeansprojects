package fr.istic.sagesocial;


import android.app.Activity;
import android.content.Intent;
import fr.istic.sagesocial.engine.AbstractService;
import fr.istic.sagesocial.engine.ServiceFactory;
import fr.istic.sagesocial.engine.bean.FriendBean;
import fr.istic.sagesocial.engine.listener.ServiceCallback;
import fr.istic.sagesocial.views.profile.ProfileActivity;

import java.util.Collection;
import java.util.HashMap;

public class ExecuteTaskLoadProfile {

    protected static int currentTask = 0;

    static public void loadProfile(final Activity activity) {

	final Collection<AbstractService> services = ServiceFactory
	        .getRegisteredServices(activity);
	SageSocialActivity.mySelf = new HashMap<AbstractService, FriendBean>();

	// recovery my profile
	for (final AbstractService currentService : services) {

	    ExecuteTaskLoadProfile.currentTask++;
	    currentService.getMyself(new ServiceCallback() {
		@Override
		public void onUpdateData(final Object data) {
		    activity.runOnUiThread(new Runnable() {
			@Override
			public void run() {
			    SageSocialActivity.mySelf.put(currentService,
				    (FriendBean) data);

			    if (SageSocialActivity.mySelf.size() == services.size()) {
				ExecuteTaskLoadProfile.currentTask--;
				ExecuteTaskLoadProfile.checkTasksEnd(activity);
			    }
			}
		    });
		}
	    });
	}
    }

    protected static void checkTasksEnd(final Activity activity) {
	if (ExecuteTaskLoadProfile.currentTask == 0) {
	    activity.startActivity(new Intent(activity.getApplicationContext(),
		    ProfileActivity.class));
	}
    }
}
