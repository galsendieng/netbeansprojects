package fr.istic.sagesocial.views.profile;


import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.TextView;
import fr.istic.sagesocial.R;
import fr.istic.sagesocial.engine.AbstractService;
import fr.istic.sagesocial.engine.ServiceFactory;
import fr.istic.sagesocial.engine.bean.AlbumBean;
import fr.istic.sagesocial.engine.bean.PhotoBean;
import fr.istic.sagesocial.engine.listener.PhotoManagerListener;
import fr.istic.sagesocial.engine.listener.ServiceCallback;
import fr.istic.sagesocial.views.allphotos.AlbumAdapter;
import fr.istic.sagesocial.views.allphotos.AllPhotosActivity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

public class AlbumFragment extends Fragment {

    GridView albums_gridview;
    private AlbumAdapter albumAdapter;
    ArrayList<AlbumBean> listMyAlbums;
    private Collection<AbstractService> services;
    private HashMap<String, ArrayList<PhotoBean>> photoMap;
    public ProgressDialog mProgressDialog;
    private PhotoManagerListener photoManager;
    private View mainView;
    private TextView noAlbumFinded;

    // Initialisation of the fragment
    public void init(final ArrayList<AlbumBean> albums) {

	listMyAlbums = albums;
	albumAdapter = new AlbumAdapter(getActivity().getBaseContext(), albums);
	albums_gridview.setAdapter(albumAdapter);
	albums_gridview.setVisibility(View.VISIBLE);
	noAlbumFinded.setVisibility(View.INVISIBLE);
	services = ServiceFactory.getRegisteredServices(getActivity());

    }

    public void noAlbum() {
	noAlbumFinded.setVisibility(View.VISIBLE);
	albums_gridview.setVisibility(View.INVISIBLE);
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container,
	    final Bundle savedInstanceState) {

	// Get component from layout
	mainView = inflater.inflate(R.layout.album_fragment, container, false);
	albums_gridview = (GridView) mainView.findViewById(R.id.albums_gridview);

	noAlbumFinded = (TextView) mainView.findViewById(R.id.no_album_text_view);

	// Listener
	albums_gridview.setOnItemClickListener(new OnItemClickListener() {

	    @Override
	    public void onItemClick(final AdapterView<?> parent, final View view,
		    final int position, final long id) {

		final AlbumBean selectedAlbum = listMyAlbums.get(position);

		photoMap = new HashMap<String, ArrayList<PhotoBean>>();

		for (final AbstractService service : services) {
		    mProgressDialog = ProgressDialog.show(
			    AlbumFragment.this.getActivity(),
			    "",
			    AlbumFragment.this.getResources()
			            .getString(R.string.comments_dialog_onclick)
			            .replace("%s", service.getServiceBean().getLabel()),
			    true);

		    final ArrayList<PhotoBean> listPhoto = new ArrayList<PhotoBean>();
		    service.getPhotosOfAlbum(selectedAlbum.getId(),
			    new ServiceCallback() {

			        @Override
			        public void onUpdateData(final Object data) {
				    AlbumFragment.this.getActivity().runOnUiThread(
				            new Runnable() {

				                @Override
				                public void run() {

					            @SuppressWarnings("unchecked")
					            final ArrayList<PhotoBean> listData = (ArrayList<PhotoBean>) data;

					            for (final PhotoBean photo : listData) {

					                listPhoto.add(photo);
					                photoMap.put(service
					                        .getServiceBean()
					                        .getLabel(), listPhoto);

					            }

					            if (photoMap.size() == services
					                    .size()) {
					                updateGallery(listPhoto,
					                        selectedAlbum);
					            }

					            mProgressDialog.dismiss();

				                }

				            });

			        }
			    });

		}

	    }
	});

	return mainView;
    }

    public void update(final PhotoBean photoBean) {

	if (albumAdapter != null) {
	    albumAdapter.update(photoBean);
	}
    }

    public void updateGallery(final ArrayList<PhotoBean> listPhoto,
	    final AlbumBean selectedAlbum) {

	final Bundle b = new Bundle();
	final Intent intent = new Intent(AlbumFragment.this.getActivity()
	        .getApplicationContext(), AllPhotosActivity.class);

	selectedAlbum.setListPhotoBean(listPhoto);
	final int sizeList = selectedAlbum.getListPhotoBean().size();
	b.putInt("sizeList", sizeList);

	for (int i = 0; i < sizeList; i++) {
	    b.putParcelable("PhotoBean" + i, selectedAlbum.getListPhotoBean().get(i));
	}
	b.putString("albumName", selectedAlbum.getNameAlbum());
	b.putInt("positionCursor", 0);
	intent.putExtras(b);

	AlbumFragment.this.startActivity(intent);

    }

}
