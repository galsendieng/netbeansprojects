package fr.istic.sagesocial;


import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;
import fr.istic.sagesocial.engine.PhotoManager;
import fr.istic.sagesocial.engine.bean.PhotoBean;
import fr.istic.sagesocial.engine.listener.PhotoManagerListener;

public class PhotoManagerTestActivity extends Activity {

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	ApplicationConfig.installAppDirectory(this);
	// Initialisation du PhotoManager, normalement fait une seule fois pour
	// toute l'application.
	PhotoManager.getInstance(this);
	this.setContentView(R.layout.photomanager_test);
	final TextView text = (TextView) this.findViewById(R.id.photomanager_test_text);

	// Cr�ation du listener. Il permet d'effectuer une op�ration quand la
	// photo est pr�te.
	final PhotoManagerListener listener = new PhotoManagerListener() {
	    @Override
	    public void dataChanged(final PhotoBean p) {
		// Mise � jour de l'UI
		text.setText(p.getLocalRessource());
	    }
	};

	// On enregistre le listener aupr�s du PhotoManager
	PhotoManager.getInstance(this).addListener(listener);
	// Pour le test on enregistre une photo!
	PhotoManager
	        .getInstance(this)
	        .register(
	                new PhotoBean(
	                        "http://www.ballajack.com/wp-content/uploads/2011/02/android.jpg"));
    }

    @Override
    protected void onDestroy() {
	PhotoManager.getInstance(this).destroy();
	super.onDestroy();
    }
}
