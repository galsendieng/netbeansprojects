package fr.istic.sagesocial.views.allfriends;


import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.GridView;
import fr.istic.sagesocial.R;
import fr.istic.sagesocial.engine.bean.FriendBean;
import fr.istic.sagesocial.engine.bean.PhotoBean;
import fr.istic.sagesocial.engine.listener.OnClickStartActivity;
import fr.istic.sagesocial.utils.WidgetUtils;
import fr.istic.sagesocial.views.profile.ProfileActivity;

import java.util.HashMap;

public class AllFriendsGridFragment extends Fragment {

    private GridView gridView;
    private HashMap<String, FriendBean[]> friendsByGroup;
    private OnClickListener onClickViewListener;
    private AllFriendsAdapter adapter;
    private Context mContext;

    public void fill(final String text) {
	if ((text != null) && (this.friendsByGroup != null)) {
	    this.adapter = new AllFriendsAdapter(this.gridView.getContext(),
		    this.friendsByGroup.get(text), this.onClickViewListener);
	    this.gridView.setAdapter(this.adapter);
	}
    }

    public void init(final HashMap<String, FriendBean[]> friendsByGroup,
	    final String[] groups) {
	this.friendsByGroup = friendsByGroup;

	for (final String grp : groups) {
	    this.adapter = new AllFriendsAdapter(this.gridView.getContext(),
		    friendsByGroup.get(grp), this.onClickViewListener);
	    this.gridView.setAdapter(this.adapter);
	}

    }

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container,
	    final Bundle savedInstanceState) {

	this.mContext = this.getActivity().getBaseContext();

	final View view = inflater.inflate(R.layout.allfriends_grid_fragment, container,
	        false);

	this.gridView = (GridView) view.findViewById(R.id.allfriends_gridview);

	this.onClickViewListener = new OnClickListener() {

	    @Override
	    public void onClick(final View v) {
		final FriendBean friend = (FriendBean) v.getTag();
		if (friend != null) {

		    WidgetUtils.showToast(AllFriendsGridFragment.this.getActivity(),
			    "Launching " + friend.getName() + "'s profile...");

		    final OnClickStartActivity startActivity = new OnClickStartActivity(
			    AllFriendsGridFragment.this.getActivity(),
			    ProfileActivity.class);
		    startActivity.addParamParcelable("profileFriendBean", friend);
		    startActivity.onClick(v);
		}
	    }
	};

	return view;
    }

    public void update(final PhotoBean photoBean) {
	if (this.adapter != null) {
	    this.adapter.update(photoBean);
	}
    }
}
