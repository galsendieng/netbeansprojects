/**
 * AbstractService.java - Feb 10, 2012
 * 
 * @author Wassim Chegham
 */
package fr.istic.sagesocial.engine;


import android.app.Activity;
import android.content.Intent;
import fr.istic.sagesocial.engine.bean.FriendBean;
import fr.istic.sagesocial.engine.bean.ServiceBean;
import fr.istic.sagesocial.engine.listener.ServiceCallback;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This is a abstract class that handles all the operations of the various
 * social services. All new services must extends this class and implements.
 * 
 * @author Project M2 MITIC
 * 
 */
public abstract class AbstractService {

    /**
     * The maximum number of friends to retrieve from the remote service on each
     * request.
     */
    public static final int MAX_FRIENDS = 5000;

    /**
     * A reference to the current context of an activity.
     */
    protected Activity mActivity;

    /**
     * A static mapping of the friends list with the current service. The key of
     * this map is of type {@link AbstractService} and this because the key must
     * be kept unique. The value of this map is a list of {@link FriendBean}
     * which contains all the information about a given friend.
     * 
     */
    public static Map<AbstractService, List<FriendBean>> friends = new HashMap<AbstractService, List<FriendBean>>();

    /**
     * The current service bean associated with the current service.
     */
    protected ServiceBean mServiceBean;

    /**
     * Public constructor which must be called in each service extending this
     * class.
     * 
     * @param activity
     *            The current context of an activity.
     */
    public AbstractService(final Activity activity) {
	this.mActivity = activity;
    }

    /**
     * This method adds a friend to friends list
     * 
     * @param id
     *            User ID
     */
    public abstract void addFriend(String id, ServiceCallback callback);

    /**
     * This method is meant to be executed each time to service has been
     * authorized by the user. It MUST be called in the onActivityResult()
     * method of an activity.
     * 
     * @param requestCode
     *            The request code returned by the activity (in the
     *            onActivityResult)
     * @param resultCode
     *            The result code returned by the activity (in the
     *            onActivityResult)
     * @param data
     *            The intent returned by the activity (in onActivityResult)
     */
    public abstract void authorizeCallback(final int requestCode, final int resultCode,
	    final Intent data);

    /**
     * This method allows the service to extend its access token. This method
     * should be called in the OnResume() method of an activity. This method in
     * not abstract because this process my vary from service to another.
     */
    public void extendAccessTokenIfNeeded() {
    }

    /**
     * This method is used to retrieve an album information of the current user
     * with a idAlbim.
     * 
     * @param callback
     *            A ServiceCallback that will be executed when the response has
     *            returned.
     */
    public abstract void getAlbumInformation(String UserId, ServiceCallback callback);

    /**
     * This method is used to retrieve the all the photos of the current user.
     * 
     * @param callback
     *            A ServiceCallback that will be executed when the response has
     *            returned.
     */
    public abstract void getAllPhotos(ServiceCallback callback);

    /**
     * This method is used to retrieve a list of the identified message or
     * statuses's comments , ending at a given limit, which its more
     * semantically understandable when dealing with some social services like
     * Facebook...etc.
     * 
     * @param feedID
     *            The feed id.
     * @param limit
     *            The limit index where to end.
     * @param callback
     *            A ServiceCallback that will be executed when the response has
     *            returned.
     */
    public abstract void getComments(String feedId, final int offset, int limit,
	    ServiceCallback callback);

    /**
     * This method retrieves a list of friends of the current user, starting at
     * a given offset and ending at a given limit.
     * 
     * @param offset
     *            The offset index where to start.
     * @param limit
     *            The limit index where to end.
     * @param callback
     *            A ServiceCallback that will be executed when the response has
     *            returned.
     */
    public abstract void getFriends(int offset, int limit, ServiceCallback callback);

    /**
     * This method retrieves a list of friends of a given user, starting at a
     * given offset and ending at a given limit.
     * 
     * @param userId
     *            The user id whose friends must be retrieved.
     * @param offset
     *            The offset index where to start.
     * @param limit
     *            The limit index where to end.
     * @param callback
     *            A ServiceCallback that will be executed when the response has
     *            returned.
     */
    public abstract void getFriends(String userId, int offset, int limit,
	    ServiceCallback callback);

    /**
     * This method retrieves a list of friends of a given user, starting at a
     * given offset and ending at a given limit.
     * 
     * @param userId
     *            The user id whose friends must be retrieved.
     * @param offset
     *            The offset index where to start.
     * @param limit
     *            The limit index where to end.
     * @param criteria
     *            A given string that is used when searching/filtering a
     *            friend's name of the given user.
     * @param callback
     *            A ServiceCallback that will be executed when the response has
     *            returned.
     */
    public abstract void getFriends(String userId, int offset, int limit,
	    final String criteria, ServiceCallback callback);

    /**
     * This method is used to retrieve a list of the identified user's messages
     * or statuses, ending at a given limit, which its more semantically
     * understandable when dealing with some social services like
     * Facebook...etc.
     * 
     * @param limit
     *            The limit index where to end.
     * @param callback
     *            A ServiceCallback that will be executed when the response has
     *            returned.
     */
    public abstract void getMessages(String userId, int limit, ServiceCallback callback);

    /**
     * This method gets the information about the current user. The returned
     * result is of the callback is of type FriendBean.
     * 
     * @param callback
     *            A ServiceCallback that will be executed when the response has
     *            returned.
     */
    public abstract void getMyself(ServiceCallback callback);

    /**
     * This method is used to retrieve all the current user's messages or
     * statuses which its more semantically understandable when dealing with
     * some social services like Facebook...etc.
     * 
     * @param callback
     *            A ServiceCallback that will be executed when the response has
     *            returned.
     */
    public abstract void getNews(ServiceCallback callback);

    /**
     * This method is used to retrieve an album information of the current user.
     * 
     * @param albumID
     *            The album id.
     * 
     * @param callback
     *            A ServiceCallback that will be executed when the response has
     *            returned.
     */
    public abstract void getPhotosOfAlbum(String albumID, ServiceCallback callback);

    /**
     * @return The service bean associated with the current service.
     */
    public ServiceBean getServiceBean() {
	return this.mServiceBean;
    }

    /**
     * This method is used to grant the user's permissions. This operation is
     * usually called in conjuntion with the login operation.
     * 
     * @param callback
     *            A ServiceCallback that will be executed when the response has
     *            returned.
     */
    public abstract void grantUserPermissions(ServiceCallback callback);

    /**
     * @return boolean whether this current service has an non-expired session
     *         token.
     */
    public boolean isSessionValid() {
	return false;
    }

    /**
     * This method is used to login the user to the specific service.
     * 
     * @param callback
     *            A ServiceCallback that will be executed when the response has
     *            returned.
     */
    public abstract void login(ServiceCallback serviceCallback);

    /**
     * This method is used to logout the user from a specific service.
     * 
     * @param callback
     *            A ServiceCallback that will be executed when the response has
     *            returned.
     */
    public abstract void logout(ServiceCallback serviceCallback);

    /**
     * This method is used to post a binary content (pictures, movies ...)
     * 
     * @param filepath
     *            The file path to the binary file that is being sent/posted to
     *            the service.
     * @param description
     *            A message to send along with the file.
     * @param callback
     *            A ServiceCallback that will be executed when the response has
     *            returned.
     */
    public abstract void postBinaryContent(String filepath, String description,
	    ServiceCallback serviceCallback);

    /**
     * This method is used to post a comment on status or to another comment.
     * 
     * @param postId
     *            The ID of post that must be addressed.
     * @param message
     *            Message of comment.
     * @param callback
     *            A ServiceCallback that will be executed when the response has
     *            returned.
     */
    public abstract void postComment(String postId, String message,
	    ServiceCallback callback);

    /**
     * This method is used to post a message which is a string content. A
     * message is intended to be posted after a comment. To post a profile
     * status use {@link AbstractService.postStatus()}. To post a binary content
     * use {@link AbstractService.postBinaryContent()}
     * 
     * @param message
     *            The message to be posted.
     * @param callback
     *            A ServiceCallback that will be executed when the response has
     *            returned.
     */
    public abstract void postMessage(String userId, String message,
	    ServiceCallback callback);

    /**
     * This method is used to post a profile status which is a string content. A
     * status is intended to be posted as a profile status. To post a message
     * after a comment use {@link AbstractService.postMessage()}. To post a
     * binary content use {@link AbstractService.postBinaryContent()}
     * 
     * @param status
     *            The status to be posted.
     */
    public abstract void postStatus(String status);

    /**
     * This method removes a friend from a the current user's friends list.
     * 
     * @param id
     *            The id of the friend to be removed.
     * 
     * @param callback
     *            A ServiceCallback that will be executed when the response has
     *            returned.
     */
    public abstract void removeFriend(String id, ServiceCallback callback);

    /**
     * This method is used to revoke the user's permissions. This operation is
     * usually called in conjunction with the logout operation.
     * 
     * @param callback
     *            A ServiceCallback that will be executed when the response has
     *            returned.
     */
    public abstract void revokeUserPermissions(final ServiceCallback serviceCallback);

    /**
     * Update the activity
     * 
     * @param activity
     */
    public void setActivity(final Activity activity) {
	this.mActivity = activity;
    }

    /**
     * @param mServiceBean
     *            the ServiceBean to set
     */
    public void setServiceBean(final ServiceBean serviceBean) {
	this.mServiceBean = serviceBean;
    }

}
