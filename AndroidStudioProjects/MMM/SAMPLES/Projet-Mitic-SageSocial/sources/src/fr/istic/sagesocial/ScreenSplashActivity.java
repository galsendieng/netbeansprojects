package fr.istic.sagesocial;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import fr.istic.sagesocial.engine.PhotoManager;
import fr.istic.sagesocial.engine.ServiceFactory;
import fr.istic.sagesocial.utils.WidgetUtils;

public class ScreenSplashActivity extends Activity {

    @Override
    public void onCreate(final Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);

	if (!ApplicationConfig.isNetworkAvailable(this)) {
	    WidgetUtils.showSettingDialog(this, "Network Access",
		    "Your Internet connection seems to be off. Please turn it on.");
	}
	else {
	    this.setContentView(R.layout.screen_splash);
	    ScreenSplashActivity.this.launchActivity();
	}

    }

    protected void launchActivity() {
	if (ServiceFactory.getRegisteredServices(this).size() > 0) {
	    this.doSomeAdditionalWork();
	}
	else {
	    this.startActivity(new Intent(this, SageSocialActivity.class));
	}
    }

    @Override
    protected void onDestroy() {
	super.onDestroy();
	PhotoManager.getInstance(this).destroy();
	this.finish();
    }

    @Override
    protected void onPause() {
	super.onPause();
	PhotoManager.getInstance(this).destroy();
	this.finish();
    }

    @Override
    protected void onResume() {
	super.onResume();
	ScreenSplashActivity.this.launchActivity();
    }

    private void doSomeAdditionalWork() {
	// TODO preload all photos, albums, friend list, feeds... here before
	// calling the profile acitvity
	ExecuteTaskLoadProfile.loadProfile(this);
    }

}
