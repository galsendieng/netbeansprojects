package fr.istic.sagesocial.models;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;

import com.facebook.android.AsyncFacebookRunner;
import com.facebook.android.AsyncFacebookRunner.RequestListener;
import com.facebook.android.DialogError;
import com.facebook.android.Facebook;
import com.facebook.android.Facebook.DialogListener;
import com.facebook.android.FacebookError;

import fr.istic.sagesocial.R;
import fr.istic.sagesocial.SageSocialActivity;
import fr.istic.sagesocial.engine.AbstractService;
import fr.istic.sagesocial.engine.PhotoManager;
import fr.istic.sagesocial.engine.bean.AlbumBean;
import fr.istic.sagesocial.engine.bean.FeedBean;
import fr.istic.sagesocial.engine.bean.FeedBean.MIME;
import fr.istic.sagesocial.engine.bean.FriendBean;
import fr.istic.sagesocial.engine.bean.PhotoBean;
import fr.istic.sagesocial.engine.listener.ServiceCallback;
import fr.istic.sagesocial.utils.StringUtils;
import fr.istic.sagesocial.utils.WidgetUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class FacebookService extends AbstractService {

    private static final String APP_TAG = "FacebookService";
    private final Facebook mFacebook = new Facebook("342392572458644");
    private final AsyncFacebookRunner mAsyncRunner = new AsyncFacebookRunner(
	    this.mFacebook);

    private final SharedPreferences mPrefs;

    // https://developers.facebook.com/docs/reference/api/permissions/
    private final String[] mPermissions = new String[] { "user_activities",
	    "user_photos", "user_about_me", "user_status", "read_stream",
	    "offline_access", "user_online_presence", "friends_online_presence",
	    "publish_stream" };

    public FacebookService(final Activity activity) {
	super(activity);
	this.mActivity = activity;

	this.mPrefs = activity.getSharedPreferences("facebook_service",
	        Context.MODE_PRIVATE);
	final String access_token = this.mPrefs.getString("facebook_access_token", null);
	final long expires = this.mPrefs.getLong("facebook_access_expires", 0);
	if (access_token != null) {
	    this.mFacebook.setAccessToken(access_token);
	}
	if (expires != 0) {
	    this.mFacebook.setAccessExpires(expires);
	}

    }

    @Override
    public void addFriend(final String id, final ServiceCallback callback) {
	Log.d(FacebookService.APP_TAG, "addFriend() not implemented yet.");
    }

    @Override
    public void authorizeCallback(final int requestCode, final int resultCode,
	    final Intent data) {
	this.mFacebook.authorizeCallback(requestCode, resultCode, data);
    }

    @Override
    public void extendAccessTokenIfNeeded() {
	this.mFacebook.extendAccessTokenIfNeeded(this.mActivity, null);
    }

    @Override
    public void getAlbumInformation(final String UserId, final ServiceCallback callback) {

	final String path = UserId + "/albums";
	final Bundle param = new Bundle();
	param.putString("fields", "id,name,cover_photo,link,updated_time");
	this.mAsyncRunner.request(path, param, new RequestListener() {

	    @Override
	    public void onComplete(final String response, final Object state) {

		final List<AlbumBean> albumList = new ArrayList<AlbumBean>();

		try {

		    final JSONObject json = new JSONObject(response);
		    final JSONArray data = json.getJSONArray("data");
		    final int length = data != null ? data.length() : 0;

		    for (int i = 0; i < length; i++) {

			final ArrayList<PhotoBean> listPhoto = new ArrayList<PhotoBean>();
			final JSONObject album = (JSONObject) data.get(i);
			final AlbumBean albumbean = new AlbumBean();
			albumbean.setNameAlbum(album.getString("name"));

			if (album.has("cover_photo") && album.has("link")) {

			    final PhotoBean coverBean = FacebookService.this
				    .getPhoto(album.getString("cover_photo"));

			    albumbean.setCoverPicture(coverBean);
			    albumbean.setUrl(album.getString("link"));
			    albumbean.setLastUpdate(album.getString("updated_time"));
			    albumbean.setId(album.getString("id"));
			    albumbean.setListPhotoBean(listPhoto);

			    albumList.add(albumbean);

			}

		    }

		    callback.onUpdateData(albumList);

		}
		catch (final JSONException e) {
		    Log.e(FacebookService.APP_TAG, e.getMessage());
		}

	    }

	    @Override
	    public void onFacebookError(final FacebookError e, final Object state) {
		Log.d(FacebookService.APP_TAG, e.getMessage());
		WidgetUtils.showDialog(FacebookService.this.mActivity, "Facebook",
		        e.getMessage());

	    }

	    @Override
	    public void onFileNotFoundException(final FileNotFoundException e,
		    final Object state) {
		Log.d(FacebookService.APP_TAG, e.getMessage());
		WidgetUtils.showDialog(FacebookService.this.mActivity, "Facebook",
		        e.getMessage());

	    }

	    @Override
	    public void onIOException(final IOException e, final Object state) {
		Log.d(FacebookService.APP_TAG, e.getMessage());
		WidgetUtils.showDialog(FacebookService.this.mActivity, "Facebook",
		        e.getMessage());

	    }

	    @Override
	    public void onMalformedURLException(final MalformedURLException e,
		    final Object state) {
		Log.d(FacebookService.APP_TAG, e.getMessage());
		WidgetUtils.showDialog(FacebookService.this.mActivity, "Facebook",
		        e.getMessage());

	    }

	});

    }

    @Override
    public void getAllPhotos(final ServiceCallback callback) {
	Log.d(FacebookService.APP_TAG, "getAllPhotos() not implemented yet.");
    }

    @Override
    public void getComments(final String feedId, final int offset, final int limit,
	    final ServiceCallback callback) {
	if (this.mAsyncRunner != null) {
	    final Bundle parameters = new Bundle();
	    parameters.putString("limit", limit + "");
	    parameters.putString("offset", offset + "");
	    parameters.putString("date_format", "U");
	    this.mAsyncRunner.request(feedId + "/comments", parameters,
		    new RequestListener() {

		        @Override
		        public void onComplete(final String response, final Object state) {
			    try {
			        final List<FeedBean> list = new ArrayList<FeedBean>();

			        final JSONObject json = new JSONObject(response);
			        final JSONArray comments = json.getJSONArray("data");
			        for (int i = 0; i < comments.length(); i++) {
				    final JSONObject comment = comments.getJSONObject(i);
				    final FeedBean feed = new FeedBean(comment
				            .getString("id"), FacebookService.this);
				    feed.setCreatedAt(new Date(comment
				            .getString("created_time")));
				    feed.setText(comment.getString("message"));
				    final JSONObject from = json.getJSONObject("from");
				    final FriendBean author = new FriendBean(from
				            .getString("id"), from.getString("name"),
				            FacebookService.this.getPhoto(from
				                    .getString("id")));
				    feed.setAuthor(author);
				    list.add(feed);
			        }
			        callback.onUpdateData(list);
			    }
			    catch (final JSONException e) {
			        Log.e(FacebookService.APP_TAG, e.getMessage());
			    }
		        }

		        @Override
		        public void onFacebookError(final FacebookError e,
		                final Object state) {
			    Log.d(FacebookService.APP_TAG, e.getMessage());
			    WidgetUtils.showDialog(FacebookService.this.mActivity,
			            "Facebook", e.getMessage());
		        }

		        @Override
		        public void onFileNotFoundException(
		                final FileNotFoundException e, final Object state) {
			    Log.d(FacebookService.APP_TAG, e.getMessage());
			    WidgetUtils.showDialog(FacebookService.this.mActivity,
			            "Facebook", e.getMessage());
		        }

		        @Override
		        public void onIOException(final IOException e, final Object state) {
			    Log.d(FacebookService.APP_TAG, e.getMessage());
			    WidgetUtils.showDialog(FacebookService.this.mActivity,
			            "Facebook", e.getMessage());
		        }

		        @Override
		        public void onMalformedURLException(
		                final MalformedURLException e, final Object state) {
			    Log.d(FacebookService.APP_TAG, e.getMessage());
			    WidgetUtils.showDialog(FacebookService.this.mActivity,
			            "Facebook", e.getMessage());
		        }
		    });
	}
    }

    @Override
    public void getFriends(final int offset, final int limit,
	    final ServiceCallback callback) {
	final String path = "me/friends";
	final Bundle parameters = new Bundle();
	parameters.putString("limit", limit + "");
	parameters.putString("offset", offset + "");
	this.getFriends(path, parameters, callback);
    }

    @Override
    public void getFriends(final String userId, final int offset, final int limit,
	    final ServiceCallback callback) {
	final String path = userId + "/friends";
	final Bundle parameters = new Bundle();
	parameters.putString("limit", limit + "");
	parameters.putString("offset", offset + "");
	this.getFriends(path, parameters, callback);
    }

    @Override
    public void getFriends(final String userId, final int offset, final int limit,
	    final String criteria, final ServiceCallback callback) {
	final String path = userId + "/friends";
	final Bundle parameters = new Bundle();
	parameters.putString("limit", limit + "");
	parameters.putString("offset", offset + "");
	this.getFriends(path, parameters, new ServiceCallback() {

	    @Override
	    public void onUpdateData(final Object data) {
		final List<FriendBean> friends = (List<FriendBean>) data;
		final List<FriendBean> result = new ArrayList<FriendBean>();
		for (final FriendBean friend : friends) {
		    if (friend.getName().toLowerCase().contains(criteria.toLowerCase())) {
			result.add(friend);
		    }
		}
		callback.onUpdateData(result);
	    }
	});
    }

    @Override
    public void getMessages(final String userId, final int limit,
	    final ServiceCallback callback) {
	if (this.mAsyncRunner != null) {
	    final Bundle parameters = new Bundle();
	    parameters.putString("date_format", "U");
	    parameters.putString("limit", limit + "");
	    this.mAsyncRunner.request(userId + "/feed", parameters,
		    new RequestListener() {

		        @Override
		        public void onComplete(final String response, final Object state) {
			    try {
			        final List<FeedBean> list = new ArrayList<FeedBean>();
			        final JSONObject json = new JSONObject(response);
			        final JSONArray table = json.getJSONArray("data");
			        String s;
			        for (int i = 0; i < table.length(); i++) {
				    s = table.getJSONObject(i).getString("type");
				    if (s.equals("status")) {
				        final FeedBean feed = FacebookService.this
				                .extractFeed(table.getJSONObject(i));
				        if (feed != null) {
					    if ((feed.getText() != null)
					            && !feed.getText().equals("")) {
					        list.add(feed);
					    }
				        }
				    }
			        }
			        callback.onUpdateData(list);
			    }
			    catch (final JSONException e) {
			        Log.e(FacebookService.APP_TAG, e.getMessage());
			    }
		        }

		        @Override
		        public void onFacebookError(final FacebookError e,
		                final Object state) {
			    Log.d(FacebookService.APP_TAG, e.getMessage());
			    WidgetUtils.showDialog(FacebookService.this.mActivity,
			            "Facebook", e.getMessage());
		        }

		        @Override
		        public void onFileNotFoundException(
		                final FileNotFoundException e, final Object state) {
			    Log.d(FacebookService.APP_TAG, e.getMessage());
			    WidgetUtils.showDialog(FacebookService.this.mActivity,
			            "Facebook", e.getMessage());
		        }

		        @Override
		        public void onIOException(final IOException e, final Object state) {
			    Log.d(FacebookService.APP_TAG, e.getMessage());
			    WidgetUtils.showDialog(FacebookService.this.mActivity,
			            "Facebook", e.getMessage());
		        }

		        @Override
		        public void onMalformedURLException(
		                final MalformedURLException e, final Object state) {
			    Log.d(FacebookService.APP_TAG, e.getMessage());
			    WidgetUtils.showDialog(FacebookService.this.mActivity,
			            "Facebook", e.getMessage());
		        }
		    });
	}
    }

    @Override
    public void getMyself(final ServiceCallback callback) {
	if (this.mAsyncRunner != null) {
	    final Bundle parameters = new Bundle();
	    parameters.putString("fields", new String("id,name,picture"));
	    parameters.putString("type", "large");
	    this.mAsyncRunner.request("me", parameters, new RequestListener() {

		@Override
		public void onComplete(final String response, final Object state) {
		    try {
			final JSONObject json = new JSONObject(response);
			final PhotoBean photo = new PhotoBean(json.getString("picture"));
			PhotoManager.getInstance(FacebookService.this.mActivity)
			        .register(photo);
			final FriendBean me = new FriendBean(json.getString("id"), json
			        .getString("name"), photo);
			callback.onUpdateData(me);
		    }
		    catch (final JSONException e) {
			Log.e(FacebookService.APP_TAG, e.getMessage());
		    }

		}

		@Override
		public void onFacebookError(final FacebookError e, final Object state) {
		    Log.d(FacebookService.APP_TAG, e.getMessage());
		    WidgetUtils.showDialog(FacebookService.this.mActivity, "Facebook",
			    e.getMessage());
		}

		@Override
		public void onFileNotFoundException(final FileNotFoundException e,
		        final Object state) {
		    Log.d(FacebookService.APP_TAG, e.getMessage());
		    WidgetUtils.showDialog(FacebookService.this.mActivity, "Facebook",
			    e.getMessage());
		}

		@Override
		public void onIOException(final IOException e, final Object state) {
		    Log.d(FacebookService.APP_TAG, e.getMessage());
		    WidgetUtils.showDialog(FacebookService.this.mActivity, "Facebook",
			    e.getMessage());
		}

		@Override
		public void onMalformedURLException(final MalformedURLException e,
		        final Object state) {
		    Log.d(FacebookService.APP_TAG, e.getMessage());
		    WidgetUtils.showDialog(FacebookService.this.mActivity, "Facebook",
			    e.getMessage());
		}
	    });
	}
    }

    @Override
    public void getNews(final ServiceCallback callback) {
	if (this.mAsyncRunner != null) {
	    final Bundle parameters = new Bundle();
	    parameters.putString("date_format", "U");
	    this.mAsyncRunner.request("me/statuses", new RequestListener() {

		@Override
		public void onComplete(final String response, final Object state) {
		    try {
			final List<FeedBean> list = new ArrayList<FeedBean>();
			final JSONObject json = new JSONObject(response);
			final JSONArray table = json.getJSONArray("data");
			for (int i = 0; i < table.length(); i++) {
			    final FeedBean feed = FacebookService.this.extractFeed(table
				    .getJSONObject(i));
			    if (feed != null) {
				if ((feed.getText() != null)
				        && !feed.getText().equals("")) {
				    list.add(feed);
				}
			    }
			}
			callback.onUpdateData(list);
		    }
		    catch (final JSONException e) {
			Log.e(FacebookService.APP_TAG, e.getMessage());
		    }
		}

		@Override
		public void onFacebookError(final FacebookError e, final Object state) {
		    Log.d(FacebookService.APP_TAG, e.getMessage());
		    WidgetUtils.showDialog(FacebookService.this.mActivity, "Facebook",
			    e.getMessage());
		}

		@Override
		public void onFileNotFoundException(final FileNotFoundException e,
		        final Object state) {
		    Log.d(FacebookService.APP_TAG, e.getMessage());
		    WidgetUtils.showDialog(FacebookService.this.mActivity, "Facebook",
			    e.getMessage());
		}

		@Override
		public void onIOException(final IOException e, final Object state) {
		    Log.d(FacebookService.APP_TAG, e.getMessage());
		    WidgetUtils.showDialog(FacebookService.this.mActivity, "Facebook",
			    e.getMessage());
		}

		@Override
		public void onMalformedURLException(final MalformedURLException e,
		        final Object state) {
		    Log.d(FacebookService.APP_TAG, e.getMessage());
		    WidgetUtils.showDialog(FacebookService.this.mActivity, "Facebook",
			    e.getMessage());
		}
	    });
	}
    }

    @Override
    public void getPhotosOfAlbum(final String albumID, final ServiceCallback callback) {
	final String path = albumID + "/photos";

	final Bundle param = new Bundle();
	param.putString("fields", "source");
	this.mAsyncRunner.request(path, new RequestListener() {

	    @Override
	    public void onComplete(final String response, final Object state) {

		JSONObject photoJSON;
		PhotoBean pb = null;
		final ArrayList<PhotoBean> listPhoto = new ArrayList<PhotoBean>();
		try {
		    photoJSON = new JSONObject(response);
		    final JSONArray data = photoJSON.getJSONArray("data");
		    final int length = data != null ? data.length() : 0;

		    for (int i = 0; i < length; i++) {

			final JSONObject photo = (JSONObject) data.get(i);
			pb = new PhotoBean(photo.getString("source"));
			PhotoManager.getInstance(FacebookService.this.mActivity)
			        .register(pb);
			listPhoto.add(pb);

		    }
		}
		catch (final JSONException e) {
		    Log.e(FacebookService.APP_TAG, e.getMessage());
		}

		callback.onUpdateData(listPhoto);

	    }

	    @Override
	    public void onFacebookError(final FacebookError e, final Object state) {
		Log.d(FacebookService.APP_TAG, e.getMessage());
		WidgetUtils.showDialog(FacebookService.this.mActivity, "Facebook",
		        e.getMessage());
	    }

	    @Override
	    public void onFileNotFoundException(final FileNotFoundException e,
		    final Object state) {
		Log.d(FacebookService.APP_TAG, e.getMessage());
		WidgetUtils.showDialog(FacebookService.this.mActivity, "Facebook",
		        e.getMessage());

	    }

	    @Override
	    public void onIOException(final IOException e, final Object state) {
		Log.d(FacebookService.APP_TAG, e.getMessage());
		WidgetUtils.showDialog(FacebookService.this.mActivity, "Facebook",
		        e.getMessage());

	    }

	    @Override
	    public void onMalformedURLException(final MalformedURLException e,
		    final Object state) {
		Log.d(FacebookService.APP_TAG, e.getMessage());
		WidgetUtils.showDialog(FacebookService.this.mActivity, "Facebook",
		        e.getMessage());

	    }
	});

    }

    @Override
    public void grantUserPermissions(final ServiceCallback serviceCallback) {
	Log.d(FacebookService.APP_TAG, "grantUserPermissions() not implemented yet.");
    }

    @Override
    public boolean isSessionValid() {
	return this.mFacebook.isSessionValid();
    }

    @Override
    public void login(final ServiceCallback serviceCallback) {

	FacebookService.this.mFacebook.authorize(FacebookService.this.mActivity,
	        this.mPermissions, new DialogListener() {

		    @Override
		    public void onCancel() {

		        final String label = FacebookService.this.getServiceBean()
		                .getLabel();

		        WidgetUtils.showDialog(
		                FacebookService.this.mActivity,
		                label,
		                label
		                        + " "
		                        + FacebookService.this.mActivity
		                                .getString(R.string.service_connection_canceled));

		        serviceCallback.onUpdateData(false);
		    }

		    @Override
		    public void onComplete(final Bundle values) {

		        final String label = FacebookService.this.getServiceBean()
		                .getLabel();

		        WidgetUtils.showDialog(
		                FacebookService.this.mActivity,
		                label,
		                FacebookService.this.mActivity
		                        .getString(R.string.service_connection)
		                        + " "
		                        + label);

		        final SharedPreferences.Editor editor = FacebookService.this.mPrefs
		                .edit();
		        editor.putString("facebook_access_token",
		                FacebookService.this.mFacebook.getAccessToken());
		        editor.putLong("facebook_access_expires",
		                FacebookService.this.mFacebook.getAccessExpires());
		        editor.commit();

		        ((SageSocialActivity) FacebookService.this.mActivity)
		                .enableStartButton();

		        serviceCallback.onUpdateData(true);

		    }

		    @Override
		    public void onError(final DialogError e) {
		        Log.d(FacebookService.APP_TAG, e.getMessage());
		        WidgetUtils.showDialog(FacebookService.this.mActivity,
		                "Facebook", e.getMessage());
		    }

		    @Override
		    public void onFacebookError(final FacebookError e) {
		        Log.d(FacebookService.APP_TAG, e.getMessage());
		        WidgetUtils.showDialog(FacebookService.this.mActivity,
		                "Facebook", e.getMessage());
		    }
	        });
    }

    @Override
    public void logout(final ServiceCallback serviceCallback) {

	if (this.mAsyncRunner != null) {

	    this.mAsyncRunner.logout(this.mActivity, new RequestListener() {

		@Override
		public void onComplete(final String response, final Object state) {

		    final SharedPreferences.Editor editor = FacebookService.this.mPrefs
			    .edit();
		    editor.putString("facebook_access_token", null);
		    editor.putLong("facebook_access_expires", 0);
		    editor.commit();

		    FacebookService.this.mFacebook.setAccessToken("");

		    final String label = FacebookService.this.getServiceBean().getLabel();

		    WidgetUtils.showDialog(
			    FacebookService.this.mActivity,
			    label,
			    FacebookService.this.mActivity
			            .getString(R.string.service_disconnection)
			            + " "
			            + label);

		    ((SageSocialActivity) FacebookService.this.mActivity)
			    .disableStartButton();

		    serviceCallback.onUpdateData(true);

		}

		@Override
		public void onFacebookError(final FacebookError e, final Object state) {

		    Log.d(FacebookService.APP_TAG, e.getMessage());
		    WidgetUtils.showDialog(FacebookService.this.mActivity, "Facebook",
			    e.getMessage());

		}

		@Override
		public void onFileNotFoundException(final FileNotFoundException e,
		        final Object state) {
		    Log.d(FacebookService.APP_TAG, e.getMessage());
		    WidgetUtils.showDialog(FacebookService.this.mActivity, "Facebook",
			    e.getMessage());

		}

		@Override
		public void onIOException(final IOException e, final Object state) {
		    Log.d(FacebookService.APP_TAG, e.getMessage());
		    WidgetUtils.showDialog(FacebookService.this.mActivity, "Facebook",
			    e.getMessage());
		}

		@Override
		public void onMalformedURLException(final MalformedURLException e,
		        final Object state) {
		    Log.d(FacebookService.APP_TAG, e.getMessage());
		    WidgetUtils.showDialog(FacebookService.this.mActivity, "Facebook",
			    e.getMessage());
		}
	    });

	}
    }

    @Override
    public void postBinaryContent(final String filepath, final String description,
	    final ServiceCallback callback) {
	if (this.mAsyncRunner != null) {

	    byte[] data = null;

	    final Bitmap bi = BitmapFactory.decodeFile(filepath);
	    final ByteArrayOutputStream baos = new ByteArrayOutputStream();
	    bi.compress(Bitmap.CompressFormat.JPEG, 100, baos);
	    data = baos.toByteArray();

	    final String httpMethod = "POST";
	    final Bundle parameters = new Bundle();
	    parameters.putString(Facebook.TOKEN, this.mFacebook.getAccessToken());

	    // we are forcing photos for now
	    parameters.putString("method", "photos.upload");
	    parameters.putByteArray("picture", data);

	    parameters.putString("caption", description);

	    this.mAsyncRunner.request(null, parameters, httpMethod,
		    new RequestListener() {

		        @Override
		        public void onComplete(final String response, final Object state) {
			    callback.onUpdateData(true);
		        }

		        @Override
		        public void onFacebookError(final FacebookError e,
		                final Object state) {
			    Log.d(FacebookService.APP_TAG, e.getMessage());
			    WidgetUtils.showDialog(FacebookService.this.mActivity,
			            "Facebook", e.getMessage());
		        }

		        @Override
		        public void onFileNotFoundException(
		                final FileNotFoundException e, final Object state) {
			    Log.d(FacebookService.APP_TAG, e.getMessage());
			    WidgetUtils.showDialog(FacebookService.this.mActivity,
			            "Facebook", e.getMessage());
		        }

		        @Override
		        public void onIOException(final IOException e, final Object state) {
			    Log.d(FacebookService.APP_TAG, e.getMessage());
			    WidgetUtils.showDialog(FacebookService.this.mActivity,
			            "Facebook", e.getMessage());
		        }

		        @Override
		        public void onMalformedURLException(
		                final MalformedURLException e, final Object state) {
			    Log.d(FacebookService.APP_TAG, e.getMessage());
			    WidgetUtils.showDialog(FacebookService.this.mActivity,
			            "Facebook", e.getMessage());
		        }
		    }, null);
	}
    }

    @Override
    public void postComment(final String postId, final String message,
	    final ServiceCallback callback) {
	if (this.mAsyncRunner != null) {
	    final String httpMethod = "POST";
	    final Bundle parameters = new Bundle();
	    parameters.putString("message", message);
	    this.mAsyncRunner.request(postId + "/comments", parameters, httpMethod,
		    new RequestListener() {

		        @Override
		        public void onComplete(final String response, final Object state) {
			    callback.onUpdateData(true);
		        }

		        @Override
		        public void onFacebookError(final FacebookError e,
		                final Object state) {
			    Log.d(FacebookService.APP_TAG, e.getMessage());
			    WidgetUtils.showDialog(FacebookService.this.mActivity,
			            "Facebook", e.getMessage());
		        }

		        @Override
		        public void onFileNotFoundException(
		                final FileNotFoundException e, final Object state) {
			    Log.d(FacebookService.APP_TAG, e.getMessage());
			    WidgetUtils.showDialog(FacebookService.this.mActivity,
			            "Facebook", e.getMessage());
		        }

		        @Override
		        public void onIOException(final IOException e, final Object state) {
			    Log.d(FacebookService.APP_TAG, e.getMessage());
			    WidgetUtils.showDialog(FacebookService.this.mActivity,
			            "Facebook", e.getMessage());
		        }

		        @Override
		        public void onMalformedURLException(
		                final MalformedURLException e, final Object state) {
			    Log.d(FacebookService.APP_TAG, e.getMessage());
			    WidgetUtils.showDialog(FacebookService.this.mActivity,
			            "Facebook", e.getMessage());
		        }
		    }, null);
	}
    }

    @Override
    public void postMessage(final String userId, final String message,
	    final ServiceCallback callback) {
	if (this.mAsyncRunner != null) {
	    final String httpMethod = "POST";
	    final Bundle parameters = new Bundle();
	    parameters.putString("message", message);
	    this.mAsyncRunner.request(userId + "/feed", parameters, httpMethod,
		    new RequestListener() {

		        @Override
		        public void onComplete(final String response, final Object state) {
			    callback.onUpdateData(true);
		        }

		        @Override
		        public void onFacebookError(final FacebookError e,
		                final Object state) {
			    Log.d(FacebookService.APP_TAG, e.getMessage());
			    WidgetUtils.showDialog(FacebookService.this.mActivity,
			            "Facebook", e.getMessage());
		        }

		        @Override
		        public void onFileNotFoundException(
		                final FileNotFoundException e, final Object state) {
			    Log.d(FacebookService.APP_TAG, e.getMessage());
			    WidgetUtils.showDialog(FacebookService.this.mActivity,
			            "Facebook", e.getMessage());
		        }

		        @Override
		        public void onIOException(final IOException e, final Object state) {
			    Log.d(FacebookService.APP_TAG, e.getMessage());
			    WidgetUtils.showDialog(FacebookService.this.mActivity,
			            "Facebook", e.getMessage());
		        }

		        @Override
		        public void onMalformedURLException(
		                final MalformedURLException e, final Object state) {
			    Log.d(FacebookService.APP_TAG, e.getMessage());
			    WidgetUtils.showDialog(FacebookService.this.mActivity,
			            "Facebook", e.getMessage());
		        }
		    }, null);
	}
    }

    @Override
    public void postStatus(final String status) {
	Log.d(FacebookService.APP_TAG, "postStatus() not implemented yet.");
    }

    @Override
    public void removeFriend(final String id, final ServiceCallback callback) {
	Log.d(FacebookService.APP_TAG, "removeFriend() not implemented yet.");
    }

    @Override
    public void revokeUserPermissions(final ServiceCallback serviceCallback) {
	if (this.mAsyncRunner != null) {
	    final String method = "DELETE";
	    final Bundle params = new Bundle();
	    params.putString("permission", StringUtils.join(this.mPermissions, ","));
	    this.mAsyncRunner.request("me/permissions", params, method,
		    new RequestListener() {

		        @Override
		        public void onComplete(final String response, final Object state) {
			    Log.d(FacebookService.APP_TAG, response);
			    serviceCallback.onUpdateData(true);
		        }

		        @Override
		        public void onFacebookError(final FacebookError e,
		                final Object state) {
			    Log.d(FacebookService.APP_TAG, e.getMessage());
			    WidgetUtils.showDialog(FacebookService.this.mActivity,
			            "Facebook", e.getMessage());
		        }

		        @Override
		        public void onFileNotFoundException(
		                final FileNotFoundException e, final Object state) {
			    Log.d(FacebookService.APP_TAG, e.getMessage());
			    WidgetUtils.showDialog(FacebookService.this.mActivity,
			            "Facebook", e.getMessage());
		        }

		        @Override
		        public void onIOException(final IOException e, final Object state) {
			    Log.d(FacebookService.APP_TAG, e.getMessage());
			    WidgetUtils.showDialog(FacebookService.this.mActivity,
			            "Facebook", e.getMessage());
		        }

		        @Override
		        public void onMalformedURLException(
		                final MalformedURLException e, final Object state) {
			    Log.d(FacebookService.APP_TAG, e.getMessage());
			    WidgetUtils.showDialog(FacebookService.this.mActivity,
			            "Facebook", e.getMessage());
		        }
		    }, null);
	}
    }

    private FeedBean extractFeed(final JSONObject jsonObject) {

	FeedBean feed = null;
	try {
	    feed = new FeedBean(jsonObject.getString("id"), this);
	    feed.setCreatedAt(new Date(jsonObject.getInt("created_time")));

	    final JSONObject from = jsonObject.getJSONObject("from");
	    final FriendBean author = new FriendBean(from.getString("id"),
		    from.getString("name"), this.getPhoto(from.getString("id")));
	    feed.setAuthor(author);

	    final String type = jsonObject.getString("type");
	    if (type.equalsIgnoreCase("photo")) {
		Log.i(FacebookService.APP_TAG, "Feed type : Photo");
		feed.setText(jsonObject.getString("message"));
		feed.setDataURI(jsonObject.getString("source"));
		feed.setMime(MIME.PHOTO);
	    }
	    else if (type.equalsIgnoreCase("video")) {
		Log.i(FacebookService.APP_TAG, "Feed type : Video");
		feed.setText(jsonObject.getString("message"));
		feed.setDataURI(jsonObject.getString("source"));
		feed.setMime(MIME.VIDEO);
	    }
	    else if (type.equalsIgnoreCase("link")) {
		Log.i(FacebookService.APP_TAG, "Feed type : Link");
		feed.setText(jsonObject.getString("message"));
		feed.setDataURI(jsonObject.getString("link"));
		feed.setMime(MIME.LINK);
	    }
	    else if (type.equalsIgnoreCase("story")) {
		Log.i(FacebookService.APP_TAG, "Feed type : Story");
		if (jsonObject.has("story")) {
		    feed.setText(jsonObject.getString("story"));
		}
		else if (jsonObject.has("message")) {
		    feed.setText(jsonObject.getString("message"));
		}
	    }
	    else if (type.equalsIgnoreCase("status")) {
		Log.i(FacebookService.APP_TAG, "Feed type : Status");
		if (jsonObject.has("story")) {
		    feed.setText(jsonObject.getString("story"));
		}
		else if (jsonObject.has("message")) {
		    feed.setText(jsonObject.getString("message"));
		}
	    }
	    else if (type.equalsIgnoreCase("checkin")) {
		Log.i(FacebookService.APP_TAG, "Feed type : Checkin");
		feed.setText(jsonObject.getString("message"));
	    }
	    else if (type.equalsIgnoreCase("question")) {
		Log.i(FacebookService.APP_TAG, "Feed type : Question");
		feed.setText(jsonObject.getString("question"));
	    }
	    else if (type.equalsIgnoreCase("review")) {
		Log.i(FacebookService.APP_TAG, "Feed type : Review");
		feed.setText(jsonObject.getString("message"));
	    }
	    else {
		Log.e(FacebookService.APP_TAG, "Unmanaged type : " + type);
		Log.d(FacebookService.APP_TAG, jsonObject.toString());
		feed = null;
	    }
	}
	catch (final JSONException e) {
	    Log.e(FacebookService.APP_TAG, e.getMessage());
	}
	return feed;
    }

    private void getFriends(final String path, final Bundle parameters,
	    final ServiceCallback callback) {

	if (this.mAsyncRunner != null) {

	    // this will optimize the request: get the friend id, name and
	    // profile picture at once. And force the image size to "large"
	    if (parameters != null) {
		parameters.putString("fields", "id,name,picture");
		parameters.putString("type", "large");
	    }
	    //

	    this.mAsyncRunner.request(path, parameters, new RequestListener() {

		@Override
		public void onComplete(final String response, final Object state) {
		    try {
			final List<FriendBean> list = new ArrayList<FriendBean>();
			final JSONObject json = new JSONObject(response);
			final JSONArray data = json.getJSONArray("data");
			for (int i = 0; i < data.length(); i++) {
			    final JSONObject friend = data.getJSONObject(i);

			    final PhotoBean pb = new PhotoBean(friend
				    .getString("picture"));
			    PhotoManager.getInstance(FacebookService.this.mActivity)
				    .register(pb);

			    list.add(new FriendBean(friend.getString("id"), friend
				    .getString("name"), pb));
			}
			callback.onUpdateData(list);
		    }
		    catch (final JSONException e) {
			Log.e(FacebookService.APP_TAG, e.getMessage());
		    }
		}

		@Override
		public void onFacebookError(final FacebookError e, final Object state) {
		    Log.d(FacebookService.APP_TAG, e.getMessage());
		    WidgetUtils.showDialog(FacebookService.this.mActivity, "Facebook",
			    e.getMessage());
		}

		@Override
		public void onFileNotFoundException(final FileNotFoundException e,
		        final Object state) {
		    Log.d(FacebookService.APP_TAG, e.getMessage());
		    WidgetUtils.showDialog(FacebookService.this.mActivity, "Facebook",
			    e.getMessage());
		}

		@Override
		public void onIOException(final IOException e, final Object state) {
		    Log.d(FacebookService.APP_TAG, e.getMessage());
		    WidgetUtils.showDialog(FacebookService.this.mActivity, "Facebook",
			    e.getMessage());
		}

		@Override
		public void onMalformedURLException(final MalformedURLException e,
		        final Object state) {
		    Log.d(FacebookService.APP_TAG, e.getMessage());
		    WidgetUtils.showDialog(FacebookService.this.mActivity, "Facebook",
			    e.getMessage());
		}
	    });
	}
    }

    /**
     * @param userId
     * @return
     */
    private PhotoBean getPhoto(final String userId) {
	PhotoBean pb = null;
	try {
	    final Bundle photoParameters = new Bundle();
	    photoParameters.putString("fields", "picture");
	    photoParameters.putString("type", "large");
	    final String url = FacebookService.this.mFacebook.request(userId,
		    photoParameters);
	    final JSONObject urlJSON = new JSONObject(url);
	    pb = new PhotoBean(urlJSON.getString("picture"));
	    PhotoManager.getInstance(this.mActivity).register(pb);
	}
	catch (final MalformedURLException e) {

	    Log.e(FacebookService.APP_TAG, e.getMessage());
	}
	catch (final IOException e) {

	    Log.e(FacebookService.APP_TAG, e.getMessage());
	}
	catch (final JSONException e) {

	    Log.e(FacebookService.APP_TAG, e.getMessage());
	}
	return pb;
    }
}