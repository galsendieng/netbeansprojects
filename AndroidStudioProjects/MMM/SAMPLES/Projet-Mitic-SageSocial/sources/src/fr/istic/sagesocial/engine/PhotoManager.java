package fr.istic.sagesocial.engine;


import android.app.DownloadManager;
import android.app.DownloadManager.Query;
import android.app.DownloadManager.Request;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.util.Log;
import fr.istic.sagesocial.ApplicationConfig;
import fr.istic.sagesocial.engine.bean.PhotoBean;
import fr.istic.sagesocial.engine.listener.PhotoManagerListener;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PhotoManager {
    private static String TAG = "PhotoManager";

    private static PhotoManager instance = null;

    private final File directory;
    private final DownloadManager dm;
    private final BroadcastReceiver receiver;
    private final Context context;

    private List<PhotoManagerListener> listeners = null;
    private final Map<Long, PhotoBean> inDownload;

    /**
     * This method initialize all variables to PhotoManager
     * 
     * @param context
     *            Context application
     */
    private PhotoManager(final Context context) {
	this.context = context;
	this.listeners = new ArrayList<PhotoManagerListener>();
	this.inDownload = new HashMap<Long, PhotoBean>();

	this.directory = new File(ApplicationConfig.getAppDirectory(context), "cache");
	this.dm = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);

	this.receiver = new BroadcastReceiver() {

	    @Override
	    public void onReceive(final Context context, final Intent intent) {
		final String action = intent.getAction();
		if (DownloadManager.ACTION_DOWNLOAD_COMPLETE.equals(action)) {
		    final long downloadId = intent.getLongExtra(
			    DownloadManager.EXTRA_DOWNLOAD_ID, 0);
		    if (PhotoManager.this.inDownload.containsKey(downloadId)) {

			final Query query = new Query();
			query.setFilterById(downloadId);
			final Cursor c = PhotoManager.this.dm.query(query);
			if (c.moveToFirst()) {
			    final int columnIndex = c
				    .getColumnIndex(DownloadManager.COLUMN_STATUS);
			    if (DownloadManager.STATUS_SUCCESSFUL == c
				    .getInt(columnIndex)) {
				Log.i(PhotoManager.TAG, "New Download finished.");
				final PhotoBean p;
				synchronized (this) {
				    p = PhotoManager.this.inDownload.remove(downloadId);
				}
				p.setLocalRessource(c.getString(c
				        .getColumnIndex(DownloadManager.COLUMN_LOCAL_URI)));
				PhotoManager.this.dataHasChanged(p);
			    }
			}
			c.close();
		    }
		}
	    }
	};

	context.registerReceiver(this.receiver, new IntentFilter(
	        DownloadManager.ACTION_DOWNLOAD_COMPLETE));
    }

    public static PhotoManager getInstance(final Context context) {
	if (PhotoManager.instance == null) {
	    PhotoManager.instance = new PhotoManager(context);
	}
	return PhotoManager.instance;
    }

    /**
     * This method register a new listener, which call when photo is in local
     * storage.
     * 
     * @param listener
     * @see PhotoManagerListener
     */
    public void addListener(final PhotoManagerListener listener) {
	synchronized (this) {
	    this.listeners.add(listener);
	}
    }

    public void destroy() {
	try {
	    this.context.unregisterReceiver(this.receiver);
	}
	catch (final IllegalArgumentException e) {
	    Log.e(PhotoManager.TAG, e.getStackTrace().toString());
	}
	synchronized (this) {
	    this.listeners.clear();
	}
	PhotoManager.instance = null;
    }

    /**
     * This method register a photo in PhotoManager. PhotoManager callback
     * listener when photo is in local storage
     * 
     * @param photo
     * @see PhotoBean
     */
    public void register(final PhotoBean photo) {
	final int index = photo.getUrl().lastIndexOf("/");
	final File destination = new File(this.directory, photo.getUrl().substring(
	        index + 1));
	if (destination.exists()) {
	    Log.i(PhotoManager.TAG, "Photo already exists. Skipping download.");
	    photo.setLocalRessource(destination.getAbsolutePath());
	    this.dataHasChanged(photo);
	}
	else {
	    Log.i(PhotoManager.TAG, "Photo does not exist. Enqueuing the download...");
	    final Request request = new Request(Uri.parse(photo.getUrl()));
	    request.setDestinationUri(Uri.fromFile(destination));
	    request.setNotificationVisibility(Request.VISIBILITY_HIDDEN);
	    request.setVisibleInDownloadsUi(false);
	    final long id = this.dm.enqueue(request);
	    synchronized (this) {
		this.inDownload.put(id, photo);
	    }
	}
    }

    /**
     * This method unregister listener.
     * 
     * @param listener
     * @see PhotoManagerListener
     */
    public void removeListener(final PhotoManagerListener listener) {
	synchronized (this) {
	    this.listeners.remove(listener);
	}
    }

    private void dataHasChanged(final PhotoBean p) {
	synchronized (this) {
	    for (final PhotoManagerListener l : this.listeners) {
		l.dataChanged(p);
	    }
	}
    }

}
