package fr.istic.sagesocial.engine.bean;


public class ServiceBean {

    private String mId;
    private String mClass;
    private int mDrawable;
    private String mLabel;

    public ServiceBean() {
    }

    /**
     * @return the classname of the service as it was set in the XML file.
     */
    public String getClassName() {
	return this.mClass;
    }

    /**
     * @return the drawable value ready to be used by Android resources manager.
     */
    public int getDrawable() {
	return this.mDrawable;
    }

    /**
     * @return the id of the service as it was set in the XML file.
     */
    public String getId() {
	return this.mId;
    }

    /**
     * @return the label of the service bean as it was set in the XML file.
     */
    public String getLabel() {
	return this.mLabel;
    }

    /**
     * @param mClass
     *            the mClass to set
     */
    public void setClassName(final String mClass) {
	this.mClass = mClass;
    }

    /**
     * @param drawable
     *            the drawable resource ID to set
     */
    public void setDrawable(final int drawable) {
	this.mDrawable = drawable;
    }

    /**
     * @param mId
     *            the mId to set
     */
    public void setId(final String id) {
	this.mId = id;
    }

    /**
     * @param mLabel
     *            the mLabel to set
     */
    public void setLabel(final String Label) {
	this.mLabel = Label;
    }

}
