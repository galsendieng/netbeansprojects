package fr.istic.sagesocial.views.profile;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import fr.istic.sagesocial.SageSocialActivity;

public class UpdateFeedListBroadcast extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent i) {
	Log.i("ALARME", "Test	Rafraichissement");
	if (SageSocialActivity.currentProfileActivity != null) {
	    SageSocialActivity.currentProfileActivity.refreshFeedsList();
	    Log.i("ALARME", "Rafraichissement");
	}
    }
}
