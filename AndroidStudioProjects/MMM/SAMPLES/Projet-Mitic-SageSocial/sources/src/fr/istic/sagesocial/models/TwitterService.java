package fr.istic.sagesocial.models;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.net.Uri;
import android.util.Log;
import android.webkit.WebView;
import fr.istic.sagesocial.engine.AbstractService;
import fr.istic.sagesocial.engine.bean.FeedBean;
import fr.istic.sagesocial.engine.bean.FriendBean;
import fr.istic.sagesocial.engine.listener.ServiceCallback;
import fr.istic.sagesocial.utils.WidgetUtils;
import twitter4j.IDs;
import twitter4j.ResponseList;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.User;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;

import java.util.ArrayList;
import java.util.List;

public class TwitterService extends AbstractService {

    private static final String TAG = "TwitterService";
    private static final String PREF_ACCESS_TOKEN = "accessToken";
    private static final String PREF_ACCESS_TOKEN_SECRET = "accessTokenSecret";
    private static final String CONSUMER_KEY = "RRfHT50c6Ql9DKaXx0jdw";
    private static final String CONSUMER_SECRET = "i9AqApqilbwIRmY47QnLTevG1Dauq8oRlEIjCKftwQ";
    private static final String CALLBACK_URL = "sagesocial-android:///";
    private static final List<FriendBean> TFriends = new ArrayList<FriendBean>();

    private final Twitter mTwitter;
    private RequestToken mReqToken;

    private final SharedPreferences mPrefs;

    public TwitterService(final Activity activity) {
	super(activity);
	AbstractService.friends.put(this, TwitterService.TFriends);
	this.mTwitter = new TwitterFactory().getInstance();
	this.mTwitter.setOAuthConsumer(TwitterService.CONSUMER_KEY,
	        TwitterService.CONSUMER_SECRET);
	this.mPrefs = activity.getSharedPreferences("twitterPrefs", Context.MODE_PRIVATE);
	if (this.mPrefs.contains(TwitterService.PREF_ACCESS_TOKEN)) {
	    this.load();
	}
    }

    @Override
    public void addFriend(final String id, final ServiceCallback callback) {
	Log.i(TwitterService.TAG, "addFriend() not implemented yet.");
    }

    @Override
    public void authorizeCallback(final int requestCode, final int resultCode,
	    final Intent intent) {
	final Uri uri = intent.getData();
	if ((uri != null) && uri.toString().startsWith(TwitterService.CALLBACK_URL)) {
	    final String oauthVerifier = uri.getQueryParameter("oauth_verifier");

	    try {
		final AccessToken at = this.mTwitter.getOAuthAccessToken(this.mReqToken,
		        oauthVerifier);
		this.mTwitter.setOAuthAccessToken(at);

		this.saveAccessToken(at);

	    }
	    catch (final TwitterException e) {
		WidgetUtils.showToast(this.mActivity,
		        "Twitter auth error x01, try agin later");
	    }
	}
    }

    @Override
    public void getAlbumInformation(final String UserId, final ServiceCallback callback) {
	// TODO Auto-generated method stub

    }

    @Override
    public void getAllPhotos(final ServiceCallback callback) {
	Log.i(TwitterService.TAG, "getAllPhotos() not implemented.");
    }

    @Override
    public void getComments(final String feedId, final int offset, final int limit,
	    final ServiceCallback callback) {
	// TODO Auto-generated method stub

    }

    @Override
    public void getFriends(final int offset, int limit, final ServiceCallback callback) {
	final List<FriendBean> friends = new ArrayList<FriendBean>(limit);
	if ((limit == -1) || (limit > 5000)) {
	    limit = 5000;
	}
	try {
	    final IDs list = this.mTwitter.getFriendsIDs(offset);
	    final long[] ids = list.getIDs();
	    final long[] users = new long[Math.max(limit, ids.length)];
	    for (int i = 0; (i < limit) && (i < ids.length); i++) {
		users[i] = ids[i];
	    }
	    final ResponseList<User> userList = this.mTwitter.lookupUsers(users);
	    for (final User user : userList) {
		final FriendBean friend = new FriendBean(user.getId() + "");
		friend.setName(user.getName());
		friend.setPhoto(user.getProfileImageURL().toExternalForm());
		friends.add(friend);
	    }
	}
	catch (final TwitterException e) {
	    Log.e(TwitterService.TAG, "Erreur de connection.");
	}
	callback.onUpdateData(friends);
    }

    @Override
    public void getFriends(final String userId, final int offset, int limit,
	    final ServiceCallback callback) {
	final List<FriendBean> friends = new ArrayList<FriendBean>(limit);
	if ((limit == -1) || (limit > 5000)) {
	    limit = 5000;
	}
	try {
	    final IDs list = this.mTwitter.getFriendsIDs(userId, offset);
	    final long[] ids = list.getIDs();
	    final long[] users = new long[Math.max(limit, ids.length)];
	    for (int i = 0; (i < limit) && (i < ids.length); i++) {
		users[i] = ids[i];
	    }
	    final ResponseList<User> userList = this.mTwitter.lookupUsers(users);
	    for (final User user : userList) {
		final FriendBean friend = new FriendBean(user.getId() + "");
		friend.setName(user.getName());
		friend.setPhoto(user.getProfileImageURL().toExternalForm());
		friends.add(friend);
	    }
	}
	catch (final TwitterException e) {
	    Log.e(TwitterService.TAG, "Erreur de connection.");
	}
	callback.onUpdateData(friends);
    }

    @Override
    public void getFriends(final String userId, final int offset, final int limit,
	    final String criteria, final ServiceCallback callback) {
	Log.i(TwitterService.TAG, "getFriends() not implemented yet.");
    }

    @Override
    public void getMessages(final String userId, final int limit,
	    final ServiceCallback callback) {
	// TODO
	Log.i(TwitterService.TAG,
	        "getMessages(userId, limit, callback) not implemented yet.");
    }

    @Override
    public void getMyself(final ServiceCallback callback) {
	Log.i(TwitterService.TAG, "getMySelf() not implemented yet.");
    }

    @Override
    public void getNews(final ServiceCallback callback) {
	final List<FeedBean> newsList = new ArrayList<FeedBean>();
	try {
	    final ResponseList<Status> statusList = this.mTwitter.getHomeTimeline();
	    for (final Status status : statusList) {
		final FeedBean news = new FeedBean(status.getId() + "", this);
		final FriendBean author = new FriendBean(status.getUser().getId() + "");
		author.setName(status.getUser().getName());
		author.setPhoto(status.getUser().getProfileImageURL().toExternalForm());
		news.setAuthor(author);
		news.setCreatedAt(status.getCreatedAt());
		news.setText(status.getText());
		newsList.add(news);
	    }
	}
	catch (final TwitterException e) {
	    Log.e(TwitterService.TAG, "Erreur de connection.");
	}
	callback.onUpdateData(newsList);
    }

    @Override
    public void getPhotosOfAlbum(final String albumID, final ServiceCallback callback) {
	Log.i(TwitterService.TAG, "getPhotosOfAlbum() not implemented yet.");
    }

    @Override
    public void grantUserPermissions(final ServiceCallback callback) {
	Log.i(TwitterService.TAG, "grantUserPermissions() not implemented yet.");
    }

    @Override
    public void login(final ServiceCallback serviceCallback) {
	try {
	    this.mReqToken = this.mTwitter
		    .getOAuthRequestToken(TwitterService.CALLBACK_URL);

	    final WebView twitterSite = new WebView(this.mActivity);
	    twitterSite.loadUrl(this.mReqToken.getAuthenticationURL());
	    this.mActivity.setContentView(twitterSite);

	}
	catch (final TwitterException e) {
	    Log.e(TwitterService.TAG, e.getMessage());
	    WidgetUtils.showToast(this.mActivity, "Twitter login error, try again later");
	}
    }

    @Override
    public void logout(final ServiceCallback serviceCallback) {
	this.mPrefs.edit().clear();
    }

    @Override
    public void postBinaryContent(final String filepath, final String description,
	    final ServiceCallback callback) {
	Log.i(TwitterService.TAG, "postBinaryContent() not implemented yet.");
    }

    @Override
    public void postComment(final String postId, final String message,
	    final ServiceCallback callback) {
	Log.i(TwitterService.TAG, "postComment() not implemented yet.");
    }

    @Override
    public void postMessage(final String userId, final String message,
	    final ServiceCallback callback) {
	Log.i(TwitterService.TAG, "postMessage() not implemented yet.");
    }

    @Override
    public void postStatus(final String status) {
	Log.i(TwitterService.TAG, "postStatus() not implemented yet.");
    }

    @Override
    public void removeFriend(final String id, final ServiceCallback callback) {
	Log.i(TwitterService.TAG, "removeFriend() not implemented yet.");
    }

    @Override
    public void revokeUserPermissions(final ServiceCallback serviceCallback) {
	Log.i(TwitterService.TAG, "revokeUserPersmissions() not implemented yet.");
    }

    private void load() {
	final String token = this.mPrefs
	        .getString(TwitterService.PREF_ACCESS_TOKEN, null);
	final String secret = this.mPrefs.getString(
	        TwitterService.PREF_ACCESS_TOKEN_SECRET, null);

	final AccessToken at = new AccessToken(token, secret);
	this.mTwitter.setOAuthAccessToken(at);
    }

    private void saveAccessToken(final AccessToken at) {
	final String token = at.getToken();
	final String secret = at.getTokenSecret();
	final Editor editor = this.mPrefs.edit();
	editor.putString(TwitterService.PREF_ACCESS_TOKEN, token);
	editor.putString(TwitterService.PREF_ACCESS_TOKEN_SECRET, secret);
	editor.commit();
    }

}
