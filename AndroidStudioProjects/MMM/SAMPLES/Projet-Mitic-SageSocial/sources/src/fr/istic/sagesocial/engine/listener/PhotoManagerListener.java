package fr.istic.sagesocial.engine.listener;


import fr.istic.sagesocial.engine.bean.PhotoBean;

public interface PhotoManagerListener {
    public void dataChanged(PhotoBean p);
}
