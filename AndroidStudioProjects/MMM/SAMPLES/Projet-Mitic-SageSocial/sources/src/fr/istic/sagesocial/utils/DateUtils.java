package fr.istic.sagesocial.utils;


import android.content.Context;
import fr.istic.sagesocial.R;

import java.util.Date;
import java.util.HashMap;

public class DateUtils {

    public static int[] typeTimeAgo = { R.string.feedcomments_minutes,
	    R.string.feedcomments_hours, R.string.feedcomments_days,
	    R.string.feedcomments_weeks, R.string.feedcomments_months };

    public static String timeAgo(final Context context, final Date editedDate) {

	if (context == null) {
	    return "";
	}

	final Date currentDate = new Date();
	String textTimeAgo = "";
	final HashMap<String, Long> diffTimeAgo = new HashMap<String, Long>();

	final long difference = Math.abs(currentDate.getTime() - editedDate.getTime());

	diffTimeAgo.put("" + DateUtils.typeTimeAgo[0], (difference / 1000) / 60);
	diffTimeAgo.put("" + DateUtils.typeTimeAgo[1], ((difference / 1000) / 60) / 60);
	diffTimeAgo.put("" + DateUtils.typeTimeAgo[2],
	        (((difference / 1000) / 60) / 60) / 24);
	diffTimeAgo.put("" + DateUtils.typeTimeAgo[3],
	        (((difference / 1000) / 60) / 60) / 24 / 7);
	diffTimeAgo.put("" + DateUtils.typeTimeAgo[4],
	        ((((difference / 1000) / 60) / 60) / 24) / 30);

	for (final int tta : DateUtils.typeTimeAgo) {
	    final long diff = diffTimeAgo.get("" + tta);
	    if (diff > 0) {
		final String type = context.getResources().getString(tta);
		textTimeAgo = diff + " " + type;
	    }
	}

	return textTimeAgo;
    }
}
