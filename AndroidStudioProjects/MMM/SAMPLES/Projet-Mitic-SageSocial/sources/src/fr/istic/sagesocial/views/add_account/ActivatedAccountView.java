package fr.istic.sagesocial.views.add_account;


import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import fr.istic.sagesocial.R;
import fr.istic.sagesocial.engine.AbstractService;
import fr.istic.sagesocial.engine.bean.ServiceBean;
import fr.istic.sagesocial.engine.listener.ServiceCallback;

/**
 * 
 * This class handles the activated accounts creation. This is actually a view
 * which contain a service logo, a service label and a toggle button for logging
 * in and logging out.
 * 
 */
public class ActivatedAccountView extends LinearLayout {

    private final TextView mTitle;
    private final ImageView mImage, mImage2;
    private final Context mContext;
    private final AbstractService mAbstractService;
    private final ServiceBean mServiceBean;
    private final ServiceCallback mLogoutCallback = new ServiceCallback() {

	@Override
	public void onUpdateData(final Object data) {

	    ((Activity) ActivatedAccountView.this.mContext)
		    .runOnUiThread(ActivatedAccountView.this
		            .updateUIAfterLogout((Boolean) data));

	}
    };
    private final ServiceCallback mLoginCallback = new ServiceCallback() {

	@Override
	public void onUpdateData(final Object data) {

	    ((Activity) ActivatedAccountView.this.mContext)
		    .runOnUiThread(ActivatedAccountView.this
		            .updateUIAfterLogin((Boolean) data));

	}
    };

    public ActivatedAccountView(final Context context,
	    final AbstractService abstractService) {
	super(context);

	this.mContext = context;
	this.mAbstractService = abstractService;
	this.mServiceBean = this.mAbstractService.getServiceBean();

	this.setOrientation(LinearLayout.HORIZONTAL);
	this.setPadding(20, 20, 20, 20);

	this.mImage = new ImageView(context);
	this.mImage.setBackgroundResource(this.mServiceBean.getDrawable());
	this.addView(this.mImage);

	this.mTitle = new TextView(context);
	this.mTitle.setTextSize(25);
	this.mTitle.setWidth(300);
	this.mTitle.setPadding(30, 10, 70, 10);
	this.mTitle.setText(this.mServiceBean.getLabel());
	this.addView(this.mTitle);

	this.mImage2 = new ImageView(context);

	final int btnState = this.getBtnState();
	this.mImage2.setTag(btnState);
	this.mImage2.setBackgroundResource(btnState);
	this.addView(this.mImage2);
	this.mImage2.setOnClickListener(new OnClickListener() {
	    @Override
	    public void onClick(final View v) {
		ActivatedAccountView.this.toggleButtonState();
	    }
	});

    }

    protected Runnable updateUIAfterLogin(final Boolean data) {
	return new Runnable() {

	    @Override
	    public void run() {
		ActivatedAccountView.this.mImage2
		        .setBackgroundResource(data ? R.drawable.btn_toggle_on
		                : R.drawable.btn_toggle_off);
		ActivatedAccountView.this.mImage2.setTag(data ? R.drawable.btn_toggle_on
		        : R.drawable.btn_toggle_off);

	    }
	};
    }

    protected Runnable updateUIAfterLogout(final Boolean data) {
	return new Runnable() {

	    @Override
	    public void run() {
		ActivatedAccountView.this.mImage2
		        .setBackgroundResource(data ? R.drawable.btn_toggle_off
		                : R.drawable.btn_toggle_on);
		ActivatedAccountView.this.mImage2.setTag(data ? R.drawable.btn_toggle_off
		        : R.drawable.btn_toggle_on);
	    }
	};
    }

    private int getBtnState() {
	return this.mAbstractService.isSessionValid() ? R.drawable.btn_toggle_on
	        : R.drawable.btn_toggle_off;
    }

    private void toggleButtonState() {

	if (ActivatedAccountView.this.mImage2.getTag().equals(R.drawable.btn_toggle_on)) {
	    this.mAbstractService.logout(this.mLogoutCallback);
	}
	else if (ActivatedAccountView.this.mImage2.getTag().equals(
	        R.drawable.btn_toggle_off)) {
	    this.mAbstractService.login(this.mLoginCallback);
	}
    }
}
