package fr.istic.sagesocial.views.profile;


import android.app.Activity;
import android.app.AlarmManager;
import android.app.FragmentManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.QuickContactBadge;
import android.widget.TextView;
import fr.istic.sagesocial.ApplicationConfig;
import fr.istic.sagesocial.R;
import fr.istic.sagesocial.SageSocialActivity;
import fr.istic.sagesocial.engine.AbstractService;
import fr.istic.sagesocial.engine.PhotoManager;
import fr.istic.sagesocial.engine.ServiceFactory;
import fr.istic.sagesocial.engine.bean.AlbumBean;
import fr.istic.sagesocial.engine.bean.FriendBean;
import fr.istic.sagesocial.engine.bean.PhotoBean;
import fr.istic.sagesocial.engine.listener.OnClickStartActivity;
import fr.istic.sagesocial.engine.listener.PhotoManagerListener;
import fr.istic.sagesocial.engine.listener.ServiceCallback;
import fr.istic.sagesocial.utils.WidgetUtils;
import fr.istic.sagesocial.views.MenuManagerActivity;
import fr.istic.sagesocial.views.allfriends.AllFriendsActivity;
import fr.istic.sagesocial.views.help.HelpActivity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

public class ProfileActivity extends MenuManagerActivity {

    public static int numberCurrentTask = 0;
    private static int UPDATE_FEED_TIMER = 1000 * 60; // default 1min
    private static final int START_CHECKING_AFTER_1_MIN = 1000 * 60;

    static public FriendBean profileFriendBean;
    final FragmentManager fm = this.getFragmentManager();
    private TextView name;
    private Button allMyFriendButton;
    private OnClickStartActivity allFriendOnClick;
    private HashMap<PhotoBean, QuickContactBadge> imageToUpdateProfil;
    private QuickContactBadge profileImage;
    private Collection<AbstractService> services;
    private HashMap<String, ArrayList<AlbumBean>> albumMap;

    private boolean stateAlarm, alarmInit = false;
    private AlarmManager alarmManager;
    private PendingIntent pendingIntentProfile;

    /**
     * Method to assign feed refresh timer prefs.
     * 
     * @return
     */
    public static boolean updateFeedUpdateTimerPrefs(final int timerPrefs) {
	switch (timerPrefs) {
	case 1:
	    ProfileActivity.UPDATE_FEED_TIMER = 60000; // 1 min
	    break;
	case 5:
	    ProfileActivity.UPDATE_FEED_TIMER = 300000; // 5 min
	    break;
	case 10:
	    ProfileActivity.UPDATE_FEED_TIMER = 600000; // 10 min
	    break;
	}
	Log.d("", "New feed update interval: " + ProfileActivity.UPDATE_FEED_TIMER);
	return true;
    }

    public void checkPhotoLoaded() {
	if (ProfileActivity.profileFriendBean.getPhoto() != null) {
	    if (ProfileActivity.profileFriendBean.getPhoto().getLocalRessource() == null) {
		this.imageToUpdateProfil.put(
		        ProfileActivity.profileFriendBean.getPhoto(), this.profileImage);
	    }
	    else {
		this.profileImage.setScaleType(ImageView.ScaleType.FIT_XY);
		this.profileImage.setImageURI(Uri.parse(ProfileActivity.profileFriendBean
		        .getPhoto().getLocalRessource()));
	    }
	}
    }

    public void checkTasksEnd(final Activity activity) {
	if (ProfileActivity.numberCurrentTask == 0) {
	    WidgetUtils.setProgressBarVisibility(activity, false);
	    this.startAlarmRefreshFeedsList();
	}
    }

    public void initLauchPeriodiqueTask() {
	Log.i("ALARME", "Init alarm");
	this.stateAlarm = false;
	this.alarmManager = (AlarmManager) this.getSystemService(Context.ALARM_SERVICE);
	final Intent intentProfile = new Intent(this, UpdateFeedListBroadcast.class);
	this.pendingIntentProfile = PendingIntent.getBroadcast(this, 0, intentProfile, 0);
	this.alarmInit = true;
    }

    @Override
    public void onActivityResult(final int requestCode, final int resultCode,
	    final Intent data) {
	super.onActivityResult(requestCode, resultCode, data);

	for (final AbstractService abstractService : this.services) {
	    abstractService.authorizeCallback(requestCode, resultCode, data);
	}
    }

    /** Called when the activity is first created. */
    @Override
    public void onCreate(final Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);

	WidgetUtils.setProgressBarVisibility(this, true);
	this.initLauchPeriodiqueTask();

	// TODO create directory in sdcard
	ApplicationConfig.installAppDirectory(this);

	// recovery all service
	this.services = ServiceFactory.getRegisteredServices(this);

	SageSocialActivity.currentProfileActivity = this;

	this.recoveryProfile();

	// recovery and create view
	this.setContentView(R.layout.profile);
	this.profileImage = (QuickContactBadge) this
	        .findViewById(R.id.identity_quick_contact_badge);
	this.name = (TextView) this.findViewById(R.id.identity_name_text_view);
	this.allMyFriendButton = (Button) this.findViewById(R.id.all_friend_button);
	this.allFriendOnClick = new OnClickStartActivity(this, AllFriendsActivity.class);
	this.imageToUpdateProfil = new HashMap<PhotoBean, QuickContactBadge>();

	if (this.getIntent().getExtras() != null) {
	    // update text title for friend profile
	    ((TextView) this.findViewById(R.id.photo_title_text_view)).setText(this
		    .getResources().getString(R.string.profile_photo_title));
	    ((TextView) this.findViewById(R.id.feeds_title_title_text_view)).setText(this
		    .getResources().getString(R.string.profile_feeds_title));

	    final String friendName = this.getResources()
		    .getString(R.string.profile_find_friend_title1)
		    .replace("%s", ProfileActivity.profileFriendBean.getName());
	    ((TextView) this.findViewById(R.id.find_friend_title_text_view))
		    .setText(friendName);
	    ((Button) this.findViewById(R.id.all_friend_button))
		    .setText(ProfileActivity.profileFriendBean.getName()
		            + " "
		            + this.getResources().getString(
		                    R.string.profile_all_friends_title_button));
	}

	this.checkPhotoLoaded();
	// listeners of PhotoManager
	// PhotoManager.load(this);
	PhotoManager.getInstance(this).addListener(new PhotoManagerListener() {
	    @Override
	    public void dataChanged(final PhotoBean photoBean) {
		ProfileActivity.this.updateImageProfil(photoBean);
		ProfileActivity.this.updateImageFeeds(photoBean);
		ProfileActivity.this.updateImageFriends(photoBean);
		ProfileActivity.this.updateAlbum(photoBean);
	    }
	});

	this.updateAlbum();
	this.updateProfile();

	this.handleLargeNewFeed();
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {

	if (item.getItemId() == R.id.menu_item_help) {
	    final Intent intent = new Intent(this, HelpActivity.class);
	    intent.putExtra("helpView", "profile");
	    this.startActivity(intent);
	}
	return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRestoreInstanceState(final Bundle savedInstanceState) {
	super.onRestoreInstanceState(savedInstanceState);
	final String profile_message = savedInstanceState.getString("profile_message");
	((EditText) this.findViewById(R.id.new_message_edit_text))
	        .setText(profile_message);
    }

    @Override
    public void onResume() {
	super.onResume();
	SageSocialActivity.currentProfileActivity = this;
	this.recoveryProfile();
	for (final AbstractService abstractService : this.services) {
	    abstractService.extendAccessTokenIfNeeded();
	}
	this.refreshFeedsList();
	if (ProfileActivity.numberCurrentTask == 0) {
	    this.startAlarmRefreshFeedsList();
	}
    }

    @Override
    public void onSaveInstanceState(final Bundle savedInstanceState) {
	final String profile_message = ((EditText) this
	        .findViewById(R.id.new_message_edit_text)).getText().toString();
	savedInstanceState.putString("profile_message", profile_message);
	super.onSaveInstanceState(savedInstanceState);
    }

    public void recoveryProfile() {
	// --------------- Recovery profile ---------------------------
	if (this.getIntent().getExtras() != null) {
	    // recovery friend profile
	    ProfileActivity.profileFriendBean = this.getIntent().getExtras()
		    .getParcelable("profileFriendBean");
	}
	else {
	    // recovery my profile in the first service when we have one service
	    // or the last service if we have many service
	    // TODO create an mixer for the profile
	    for (final AbstractService currentService : this.services) {
		ProfileActivity.profileFriendBean = SageSocialActivity.mySelf
		        .get(currentService);
	    }
	}
	// --------------- End Recovery profile ---------------------------
    }

    public void refreshFeedsList() {
	final FeedsFragment feedFragment = (FeedsFragment) this.fm
	        .findFragmentById(R.id.feed_list_fragment);
	feedFragment.loadFeedsList();
    }

    public void startAlarmRefreshFeedsList() {
	if (this.alarmInit && !this.stateAlarm) {
	    Log.i("ALARME", "Start");
	    this.stateAlarm = true;
	    this.alarmManager.setRepeating(AlarmManager.RTC_WAKEUP,
		    System.currentTimeMillis()
		            + ProfileActivity.START_CHECKING_AFTER_1_MIN,
		    ProfileActivity.UPDATE_FEED_TIMER, this.pendingIntentProfile);
	}
    }

    public void stopAlarmRefreshFeedsList() {
	if (this.alarmManager != null) {
	    Log.i("ALARME", "Stop");
	    this.stateAlarm = false;
	    this.alarmManager.cancel(this.pendingIntentProfile);
	}
    }

    public void updateAlbum(final PhotoBean photoBean) {

	final AlbumFragment albumFragment = (AlbumFragment) this.fm
	        .findFragmentById(R.id.album_fragment);
	albumFragment.update(photoBean);
    }

    public void updateImageFeeds(final PhotoBean photoBean) {
	final FeedsFragment feedFragment = (FeedsFragment) this.fm
	        .findFragmentById(R.id.feed_list_fragment);
	feedFragment.update(photoBean);
    }

    public void updateImageFriends(final PhotoBean photoBean) {
	final FindFriendFragment friendFragment = (FindFriendFragment) this.fm
	        .findFragmentById(R.id.find_friend_fragment);
	friendFragment.update(photoBean);
    }

    public void updateImageProfil(final PhotoBean photoBean) {
	if (this.imageToUpdateProfil.get(photoBean) != null) {
	    this.imageToUpdateProfil.get(photoBean).setImageURI(
		    Uri.parse(photoBean.getLocalRessource()));
	    this.imageToUpdateProfil.remove(photoBean);
	}
    }

    public void updateProfile() {
	this.name.setText(ProfileActivity.profileFriendBean.getName());
	this.allFriendOnClick.addParamParcelable("profile",
	        ProfileActivity.profileFriendBean);
	this.allMyFriendButton.setOnClickListener(this.allFriendOnClick);
    }

    @Override
    protected void onDestroy() {
	PhotoManager.getInstance(this).destroy();
	this.stopAlarmRefreshFeedsList();
	super.onDestroy();
    }

    @Override
    protected void onPause() {
	PhotoManager.getInstance(this).destroy();
	this.stopAlarmRefreshFeedsList();
	super.onPause();
    }

    private void handleLargeNewFeed() {
	this.findViewById(R.id.new_message_edit_text).setOnTouchListener(
	        new OnTouchListener() {

		    @Override
		    public boolean onTouch(final View v, final MotionEvent event) {
		        final OnClickStartActivity a = new OnClickStartActivity(
		                ProfileActivity.this, NewFeedLargeActivity.class);
		        a.onClick(v);
		        return true;
		    }
	        });
    }

    private void updateAlbum() {

	this.albumMap = new HashMap<String, ArrayList<AlbumBean>>();
	for (final AbstractService service : this.services) {

	    final FriendBean userId = ProfileActivity.profileFriendBean;
	    final ArrayList<AlbumBean> listMyAlbums = new ArrayList<AlbumBean>();

	    ProfileActivity.numberCurrentTask++;
	    service.getAlbumInformation(userId.getId(), new ServiceCallback() {

		@Override
		public void onUpdateData(final Object data) {

		    ProfileActivity.this.runOnUiThread(new Runnable() {
			@SuppressWarnings("unchecked")
			final ArrayList<AlbumBean> listdata = (ArrayList<AlbumBean>) data;

			@Override
			public void run() {
			    for (final AlbumBean album : this.listdata) {
				listMyAlbums.add(album);
				ProfileActivity.this.albumMap.put(service
				        .getServiceBean().getLabel(), listMyAlbums);

			    }

			    if (ProfileActivity.this.albumMap.size() == ProfileActivity.this.services
				    .size()) {

				final AlbumFragment albumFragment = (AlbumFragment) ProfileActivity.this.fm
				        .findFragmentById(R.id.album_fragment);

				if (listMyAlbums.size() == 0) {
				    albumFragment.noAlbum();

				}
				else {
				    albumFragment.init(listMyAlbums);
				}

			    }

			    ProfileActivity.numberCurrentTask--;
			    ProfileActivity.this.checkTasksEnd(ProfileActivity.this);
			}
		    });

		}
	    });

	}
    }
}
