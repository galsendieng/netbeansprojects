package fr.istic.sagesocial.views.feedcomments;


import java.util.Collection;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import fr.istic.sagesocial.engine.bean.FeedBean;

public abstract class FeedCommentFragment extends Fragment {

    public static final String FEED = "feed";
    public static final String COMMENT = "comment";

    protected CommentsActivity mParentActivity;

    protected int mFragmentLayout;
    protected int mItemLayout;
    protected int mViewId;

    protected ListView mListView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
	    Bundle savedInstanceState) {
	View view = inflater.inflate(mFragmentLayout, container, false);
	mListView = (ListView) view.findViewById(mViewId);
	mParentActivity = (CommentsActivity) getActivity();

	return view;
    }

    public abstract void setSelectFeed(FeedBean feed);

    /**
     * fill the view list within the fragment thanks the appropriate adapter
     * 
     * @param context
     *            Activity within the fragment is
     * @param list
     *            FeedBean list (comments or feed)
     */
    public abstract void fill(Context context, Collection<FeedBean> list);

    /** Configure appropriate id's components for the fragment */
    protected abstract void configureFragment();

}
