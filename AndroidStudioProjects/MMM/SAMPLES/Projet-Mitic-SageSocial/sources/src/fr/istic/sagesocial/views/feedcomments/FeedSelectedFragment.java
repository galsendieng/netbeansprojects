package fr.istic.sagesocial.views.feedcomments;


import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import android.app.Fragment;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import fr.istic.sagesocial.R;
import fr.istic.sagesocial.engine.bean.FeedBean;
import fr.istic.sagesocial.utils.DateUtils;

public class FeedSelectedFragment extends Fragment {

    private int mFragmentLayout;
    private int mFriendImageId;
    private int mTextViewId;
    private int mTimeAgoId;
    private int mServiceImageId;

    private ImageView mFriendImage;
    private TextView mFeedContent;
    private TextView mTimeAgo;
    private ImageView mServiceImage;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
	    Bundle savedInstanceState) {
	super.onCreateView(inflater, container, savedInstanceState);
	configureFragment();

	View view = inflater.inflate(mFragmentLayout, container, false);
	mFriendImage = (ImageView) view.findViewById(mFriendImageId);
	mFeedContent = (TextView) view.findViewById(mTextViewId);
	mTimeAgo = (TextView) view.findViewById(mTimeAgoId);
	mServiceImage = (ImageView) view.findViewById(mServiceImageId);

	return view;
    }

    public void fillSelectedFeed(FeedBean feed) {
	// Bitmap friendImage =
	// BitmapFactory.decodeFile(feed.getAuthor().getPhoto()
	// .getLocalRessource());
	// mFriendImage.setImageBitmap(friendImage);

	String message = "";
	if (feed.getText() != null && !feed.getText().contentEquals("")) {
	    message += "<p> " + feed.getText() + " </p>";
	    Log.i("FEEDLIST", "Text : " + feed.getText());
	}

	if (feed.getDataURI() != null && !feed.getDataURI().contentEquals("")) {
	    message += "<a href='" + feed.getDataURI() + "'> " + feed.getDataURI()
		    + " </a>";
	    Log.i("FEEDLIST", "URI : " + feed.getDataURI());
	}

	if (feed.getDescription() != null && !feed.getDescription().contentEquals("")) {
	    message += "<p> " + feed.getDescription() + " </p>";
	    Log.i("FEEDLIST", "description : " + feed.getDescription());
	}

	mFeedContent.setText(Html.fromHtml(message));

	mTimeAgo.setText(DateUtils.timeAgo(getActivity(), feed.getCreatedAt()));
	mServiceImage.setImageResource(feed.getService().getServiceBean().getDrawable());

    }

    /** Configure id's components for the fragment */
    private void configureFragment() {
	mFragmentLayout = R.layout.comments_feed_selected_fragment;
	mFriendImageId = R.id.comments_identity_image;
	mTextViewId = R.id.comments_content_text;
	mTimeAgoId = R.id.comments_time_ago;
	mServiceImageId = R.id.comments_service_image;
    }
}
