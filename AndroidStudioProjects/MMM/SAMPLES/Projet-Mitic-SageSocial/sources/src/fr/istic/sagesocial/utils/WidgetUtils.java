package fr.istic.sagesocial.utils;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.view.Window;
import android.widget.Toast;
import fr.istic.sagesocial.R;

public class WidgetUtils extends Handler {

    public static final int DISPLAY_UI_TOAST = 0;
    public static final int DISPLAY_UI_DIALOG = 1;
    private final Activity mActivity;

    public WidgetUtils(final Activity activity) {
	this.mActivity = activity;
    }

    public static void setProgressBarVisibility(final Activity activity,
	    final boolean state) {
	if (state) {
	    activity.requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
	    activity.requestWindowFeature(Window.FEATURE_PROGRESS);
	}
	activity.setProgressBarIndeterminateVisibility(state);
    }

    public static void showDialog(final Activity activity, final String title,
	    final String message) {

	if (!activity.isFinishing()) {

	    activity.runOnUiThread(new Runnable() {

		@Override
		public void run() {
		    new AlertDialog.Builder(activity)
			    .setIcon(R.drawable.app_icon)
			    .setMessage(message)
			    .setTitle(title)
			    .setPositiveButton(android.R.string.ok,
			            new DialogInterface.OnClickListener() {
			                @Override
			                public void onClick(final DialogInterface dialog,
			                        final int whichButton) {
			                }
			            }).show();

		}
	    });
	}

    }

    public static void showSettingDialog(final Activity activity, final String title,
	    final String message) {

	if (!activity.isFinishing()) {

	    activity.runOnUiThread(new Runnable() {

		@Override
		public void run() {
		    new AlertDialog.Builder(activity)
			    .setIcon(R.drawable.app_icon)
			    .setMessage(message)
			    .setTitle(title)
			    .setNeutralButton(android.R.string.ok, null)
			    .setPositiveButton("Open settings",
			            new DialogInterface.OnClickListener() {

			                @Override
			                public void onClick(final DialogInterface dialog,
			                        final int which) {
				            final Intent intent = new Intent(
				                    Settings.ACTION_WIFI_SETTINGS);
				            activity.startActivity(intent);

			                }
			            }).show();

		}
	    });
	}

    }

    public static void showToast(final Activity activity, final String message) {

	activity.runOnUiThread(new Runnable() {

	    @Override
	    public void run() {
		Toast.makeText(activity, message, Toast.LENGTH_LONG).show();
	    }

	});
    }

    @Override
    public void handleMessage(final Message msg) {
	super.handleMessage(msg);

	switch (msg.what) {
	case WidgetUtils.DISPLAY_UI_TOAST:
	    WidgetUtils.showToast(this.mActivity, (String) msg.obj);
	case WidgetUtils.DISPLAY_UI_DIALOG:
	    WidgetUtils.showDialog(this.mActivity, "", (String) msg.obj);
	default:
	    break;
	}

    }
}
