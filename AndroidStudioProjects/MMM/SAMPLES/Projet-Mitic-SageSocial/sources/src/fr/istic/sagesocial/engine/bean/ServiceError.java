package fr.istic.sagesocial.engine.bean;


/**
 * Use this class as a listener (or callback) parameter.
 * 
 */
public class ServiceError {

    private final boolean status;
    private final String errorMessage;

    public ServiceError(final boolean status, final String errorMessage) {
	this.status = status;
	this.errorMessage = errorMessage;
    }

    /**
     * A message to get information on what happened ("could not connect",
     * "wrong id"...)
     * 
     * @return
     */
    public String getErrorMessage() {
	return this.errorMessage;
    }

    /**
     * Status of the callback (true if everything went good, false if it went
     * wrong).
     * 
     * @return
     */
    public boolean isStatus() {
	return this.status;
    }
}
