package fr.istic.sagesocial.views.feedcomments;


import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import fr.istic.sagesocial.R;
import fr.istic.sagesocial.engine.AbstractService;
import fr.istic.sagesocial.engine.listener.ServiceCallback;

import java.util.Collection;

public class CommentNewFragment extends Fragment {

    private CommentsActivity mParentActivity;

    private int mFragmentLayout;
    private int mButtonId;
    private int mEditTextId;

    private Button mNewCommentButton;
    private EditText mNewCommentEditText;
    private String newContentComment;

    /**
     * Configure the new comment button with the appropriate listener
     * 
     * @param services
     *            Services where the new comment will pasted
     */
    public void configureButton(final Collection<AbstractService> services) {
	this.mNewCommentButton.setOnClickListener(new OnClickListener() {
	    @Override
	    public void onClick(final View v) {

		CommentNewFragment.this.newContentComment = CommentNewFragment.this.mNewCommentEditText
		        .getText().toString();

		if (!CommentNewFragment.this.newContentComment
		        .equals(CommentNewFragment.this.getResources().getString(
		                R.string.comments_default_new_comment))) {
		    // add new comment in all services
		    for (final AbstractService currentService : services) {
			currentService.postComment(
			        CommentNewFragment.this.mParentActivity.getSelectedFeed()
			                .getId(),
			        CommentNewFragment.this.newContentComment,
			        new ServiceCallback() {
				    @Override
				    public void onUpdateData(final Object data) {
				        // Refresh the associated comments list
				        CommentNewFragment.this.mParentActivity
				                .refreshAssociatedComments();
				    }
			        });
		    }
		    CommentNewFragment.this.refreshListFeeds();
		}
	    }
	});
    }

    /**
     * Retrieve the content of the new comment
     * 
     * @return the content
     */
    public String getContentComment() {
	return this.newContentComment;
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container,
	    final Bundle savedInstanceState) {
	super.onCreateView(inflater, container, savedInstanceState);
	this.configureFragment();

	final View view = inflater.inflate(this.mFragmentLayout, container, false);
	this.mNewCommentEditText = (EditText) view.findViewById(this.mEditTextId);
	this.mNewCommentButton = (Button) view.findViewById(this.mButtonId);
	return view;
    }

    /** This method is use for refresh feeds list after a post */
    public void refreshListFeeds() {
	this.mNewCommentEditText.setText("");
	// feedsFragment.refreshLisFeeds();
    }

    /** Configure id's components for the fragment */
    private void configureFragment() {
	try {
	    this.mParentActivity = (CommentsActivity) this.getActivity();
	}
	catch (final Exception e) {
	    Log.e("CommentNewFragment", "PARENT " + e);
	}
	this.mFragmentLayout = R.layout.comments_comment_new_fragment;
	this.mButtonId = R.id.comments_new_message_button;
	this.mEditTextId = R.id.comments_new_comment_edit_text;
    }
}
