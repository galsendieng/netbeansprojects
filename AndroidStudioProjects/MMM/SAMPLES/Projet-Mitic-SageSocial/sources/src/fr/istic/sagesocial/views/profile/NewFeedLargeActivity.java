package fr.istic.sagesocial.views.profile;


import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.MediaStore.MediaColumns;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import fr.istic.sagesocial.ApplicationConfig;
import fr.istic.sagesocial.R;
import fr.istic.sagesocial.engine.AbstractService;
import fr.istic.sagesocial.engine.ServiceFactory;
import fr.istic.sagesocial.engine.listener.ServiceCallback;
import fr.istic.sagesocial.utils.WidgetUtils;
import fr.istic.sagesocial.views.MenuManagerActivity;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Collection;

public class NewFeedLargeActivity extends MenuManagerActivity {
    protected static final int CAMERA_PIC_REQUEST = 1;
    protected static final String TMP_PICTURE_NAME = "sagesocial_tmp_cam.jpg";
    private Collection<AbstractService> mServices;
    private Button mPostButton;
    private EditText mPostEditText;
    private ProgressDialog mProgressDialog;
    private final Bitmap mImageFromCamera = null;
    private Uri mOutputImageFromCamera;
    private static int mTotalServices = 0;
    private static int mServicesCounter = 0;

    @Override
    public void onActivityResult(final int requestCode, final int resultCode,
	    final Intent data) {
	super.onActivityResult(requestCode, resultCode, data);

	for (final AbstractService abstractService : this.mServices) {
	    abstractService.authorizeCallback(requestCode, resultCode, data);
	}

	if ((requestCode == NewFeedLargeActivity.CAMERA_PIC_REQUEST)) {

	    /**
	     * intent.getData() won't work. See bug :
	     * http://code.google.com/p/android/issues/detail?id=1480
	     */
	    if (data == null) {

		final String filename = Environment.getExternalStorageDirectory()
		        + File.separator + NewFeedLargeActivity.TMP_PICTURE_NAME;
		final File fi = new File(filename);

		/**
		 * see bug:
		 * groups.google.com/group/android-developers/browse_thread/
		 * thread/2352c776651b6f99
		 */
		Bitmap b = BitmapFactory.decodeFile(filename);
		b = Bitmap.createScaledBitmap(b, b.getWidth() / 2, b.getHeight() / 2,
		        false);
		try {
		    final FileOutputStream out = new FileOutputStream(filename);
		    b.compress(Bitmap.CompressFormat.JPEG, 100, out);
		}
		catch (final Exception e) {
		    e.printStackTrace();
		}
		//

		try {
		    this.mOutputImageFromCamera = Uri
			    .parse(android.provider.MediaStore.Images.Media.insertImage(
			            this.getContentResolver(), fi.getAbsolutePath(),
			            null, null));

		    if (!fi.delete()) {
			Log.d("", "Failed to delete " + fi);
		    }

		    // this must be called after we set the
		    // mOutputImageFromCamera
		    this.handleSendImage(data);

		}
		catch (final FileNotFoundException e) {
		    e.printStackTrace();
		}

	    }
	    else {
		this.handleNoIntentFilter();
	    }

	}
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
	final boolean res = super.onCreateOptionsMenu(menu);
	menu.removeItem(R.id.menu_item_help);
	return res;
    }

    @Override
    public void onRestoreInstanceState(final Bundle savedInstanceState) {
	super.onRestoreInstanceState(savedInstanceState);
	final String profile_message = savedInstanceState.getString("profile_message");
	((EditText) this.findViewById(R.id.new_message_edit_text))
	        .setText(profile_message);
    }

    @Override
    public void onResume() {
	super.onResume();
	for (final AbstractService abstractService : this.mServices) {
	    abstractService.extendAccessTokenIfNeeded();
	}
    }

    @Override
    public void onSaveInstanceState(final Bundle savedInstanceState) {
	final String profile_message = ((EditText) this
	        .findViewById(R.id.new_message_edit_text)).getText().toString();
	savedInstanceState.putString("profile_message", profile_message);
	super.onSaveInstanceState(savedInstanceState);
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);

	ApplicationConfig.checkRegisteredServices(this);

	this.setContentView(R.layout.profile_new_feed_fragment_large);

	this.mServices = ServiceFactory.getRegisteredServices(this);
	NewFeedLargeActivity.mTotalServices = this.mServices.size();
	NewFeedLargeActivity.mServicesCounter = 0;

	this.mPostEditText = (EditText) this.findViewById(R.id.new_message_edit_text);
	this.mPostButton = (Button) this.findViewById(R.id.new_message_button);

	this.handleBundleIfNeeded();

    }

    @Override
    protected void onPause() {
	super.onPause();

	// remove the previous image (if there is one)
	final View v = this.findViewById(1337);
	if (v != null) {
	    final LinearLayout container = ((LinearLayout) this
		    .findViewById(R.id.new_post_container));
	    container.removeView(v);
	}

    }

    private String getRealPathFromURI(final Uri contentUri) {
	if (contentUri != null) {

	    final String[] proj = { MediaColumns.DATA };
	    final Cursor cursor = this.managedQuery(contentUri, proj, null, null, null);
	    final int column_index = cursor.getColumnIndexOrThrow(MediaColumns.DATA);
	    cursor.moveToFirst();
	    return cursor.getString(column_index);

	}
	return null;
    }

    private void handleBundleIfNeeded() {

	final Bundle bundle = this.getIntent().getExtras();
	if (bundle != null) {
	    final String action = this.getIntent().getAction();
	    if (action != null) {
		this.handleIntentFilterAction(action);
	    }
	}
	else {
	    this.handleNoIntentFilter();
	}

    }

    private void handleIntentFilterAction(final String action) {
	final Intent intent = this.getIntent();
	final String type = intent.getType();

	if (Intent.ACTION_SEND.equals(action) && (type != null)) {
	    if (type.startsWith("text/")) {
		this.handleSendText(intent);
	    }
	    else if (type.startsWith("image/")) {
		this.handleSendImage(intent);
	    }

	}
	else if (Intent.ACTION_SEND_MULTIPLE.equals(action) && (type != null)) {
	    if (type.startsWith("image/")) {
		this.handleSendMultipleImages(intent);
	    }
	}
	else {
	    this.handleNoIntentFilter();
	}

    }

    private void handleNoIntentFilter() {

	Log.d("", "handleNoIntentFilter");

	this.mPostEditText.setHint(this.getString(R.string.profile_default_new_message));
	this.mPostButton.setText(this
	        .getString(R.string.profile_title_new_message_button));

	this.mPostButton.setOnClickListener(new OnClickListener() {

	    @Override
	    public void onClick(final View v) {
		// delete when we have many space
		final String contentPost = NewFeedLargeActivity.this.mPostEditText
		        .getText().toString();
		final String postContentModify = contentPost.replaceAll("\\s", "");

		// TODO add argument for id profile
		if (!(postContentModify.contentEquals(""))) {
		    for (final AbstractService currentService : NewFeedLargeActivity.this.mServices) {

			WidgetUtils.showToast(
			        NewFeedLargeActivity.this,
			        NewFeedLargeActivity.this
			                .getResources()
			                .getString(
			                        R.string.profile_toast_new_message_wait)
			                .replace(
			                        "%s",
			                        currentService.getServiceBean()
			                                .getLabel()));

			currentService.postMessage(
			        ProfileActivity.profileFriendBean.getId(), contentPost,
			        new ServiceCallback() {

				    @Override
				    public void onUpdateData(final Object data) {
				        NewFeedLargeActivity.this
				                .runOnUiThread(new Runnable() {

					            @Override
					            public void run() {

					                NewFeedLargeActivity.this.mPostEditText
					                        .setText("");

					                WidgetUtils
					                        .showToast(
					                                NewFeedLargeActivity.this,
					                                NewFeedLargeActivity.this
					                                        .getResources()
					                                        .getString(
					                                                R.string.profile_toast_new_message_success));

					                NewFeedLargeActivity.this
					                        .terminateActivityIfNoMoreServices();
					            }
				                });
				    }
			        });
		    }

		}
	    }
	});

	((ImageButton) this.findViewById(R.id.new_message_camera))
	        .setOnClickListener(new OnClickListener() {

		    @Override
		    public void onClick(final View v) {

		        final Intent cameraIntent = new Intent(
		                android.provider.MediaStore.ACTION_IMAGE_CAPTURE);

		        /**
		         * This file must exists before calling the camera. If
		         * the file is not created when the camera return a
		         * result the app will crash!!
		         * 
		         * see onResult()
		         */
		        final File file = new File(Environment
		                .getExternalStorageDirectory(),
		                NewFeedLargeActivity.TMP_PICTURE_NAME);

		        NewFeedLargeActivity.this.mOutputImageFromCamera = Uri
		                .fromFile(file);

		        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT,
		                NewFeedLargeActivity.this.mOutputImageFromCamera);

		        NewFeedLargeActivity.this.startActivityForResult(cameraIntent,
		                NewFeedLargeActivity.CAMERA_PIC_REQUEST);
		    }
	        });
    }

    private void handleSendImage(final Intent intent) {

	if (this.mOutputImageFromCamera == null) {
	    this.mOutputImageFromCamera = (Uri) intent
		    .getParcelableExtra(Intent.EXTRA_STREAM);
	}

	if (this.mOutputImageFromCamera != null) {

	    final String imagePath = this.getRealPathFromURI(this.mOutputImageFromCamera);

	    final View imageView = new ImageView(this);
	    final LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
		    ViewGroup.LayoutParams.WRAP_CONTENT, 0, 2);
	    imageView.setId(1337);
	    imageView.setLayoutParams(params);
	    imageView.setPadding(20, 20, 20, 20);
	    ((ImageView) imageView).setImageDrawable(Drawable.createFromPath(imagePath));

	    final LinearLayout container = ((LinearLayout) this
		    .findViewById(R.id.new_post_container));
	    container.addView(imageView, 0);

	    this.mPostEditText
		    .setHint(this.getString(R.string.profile_default_new_image));
	    this.mPostButton.setText(this
		    .getString(R.string.profile_title_new_image_button));

	    this.mPostButton.setOnClickListener(new OnClickListener() {

		@Override
		public void onClick(final View v) {

		    for (final AbstractService currentService : NewFeedLargeActivity.this.mServices) {

			NewFeedLargeActivity.this.mPostButton.setEnabled(false);

			NewFeedLargeActivity.this.mProgressDialog = ProgressDialog.show(
			        NewFeedLargeActivity.this,
			        "",
			        NewFeedLargeActivity.this
			                .getResources()
			                .getString(R.string.profile_toast_new_image_wait)
			                .replace(
			                        "%s",
			                        currentService.getServiceBean()
			                                .getLabel()), true);

			currentService.postBinaryContent(
			        NewFeedLargeActivity.this
			                .getRealPathFromURI(NewFeedLargeActivity.this.mOutputImageFromCamera),
			        NewFeedLargeActivity.this.mPostEditText.getText()
			                .toString(), new ServiceCallback() {
				    @Override
				    public void onUpdateData(final Object data) {
				        NewFeedLargeActivity.this
				                .runOnUiThread(new Runnable() {

					            @Override
					            public void run() {

					                NewFeedLargeActivity.this.mPostEditText
					                        .setText("");

					                WidgetUtils
					                        .showToast(
					                                NewFeedLargeActivity.this,
					                                NewFeedLargeActivity.this
					                                        .getResources()
					                                        .getString(
					                                                R.string.profile_toast_new_image_success));

					                NewFeedLargeActivity.this.mPostButton
					                        .setEnabled(true);

					                NewFeedLargeActivity.this.mProgressDialog
					                        .dismiss();

					                NewFeedLargeActivity.this
					                        .terminateActivityIfNoMoreServices();
					            }
				                });
				    }
			        });
		    }
		}
	    });
	}
    }

    private void handleSendMultipleImages(final Intent intent) {
	final ArrayList<Uri> imageUris = intent
	        .getParcelableArrayListExtra(Intent.EXTRA_STREAM);
	if (imageUris != null) {
	    // TODO Update UI to reflect multiple images being shared
	}
    }

    private void handleSendText(final Intent intent) {
	final String message = intent.getStringExtra(Intent.EXTRA_TEXT);
	if (message != null) {
	    this.mPostEditText.setText(message);
	}
    }

    private void sendImage() {

    }

    private void terminateActivityIfNoMoreServices() {
	NewFeedLargeActivity.mServicesCounter++;
	if (NewFeedLargeActivity.mServicesCounter == NewFeedLargeActivity.mTotalServices) {
	    NewFeedLargeActivity.this.finish();
	}
    }
}
