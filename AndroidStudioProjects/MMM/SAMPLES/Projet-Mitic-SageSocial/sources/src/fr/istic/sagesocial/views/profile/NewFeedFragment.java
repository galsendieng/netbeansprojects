package fr.istic.sagesocial.views.profile;


import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import fr.istic.sagesocial.R;

public class NewFeedFragment extends Fragment {

    private EditText newPostEditText;

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container,
	    final Bundle savedInstanceState) {
	final View mainView = inflater.inflate(R.layout.profile_new_feed_fragment,
	        container, false);
	return mainView;
    }

    @Override
    public void onResume() {
	super.onResume();
	((ProfileActivity) this.getActivity()).refreshFeedsList();
    }

    /**
     * This method is use for refresh feeds list after a post
     */
    public void refreshListFeeds() {
	this.newPostEditText.setText("");
	((ProfileActivity) this.getActivity()).refreshFeedsList();
    }

}
