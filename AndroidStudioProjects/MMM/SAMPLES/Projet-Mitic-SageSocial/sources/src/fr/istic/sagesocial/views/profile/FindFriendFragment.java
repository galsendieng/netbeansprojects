package fr.istic.sagesocial.views.profile;


import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.TextView;
import fr.istic.sagesocial.R;
import fr.istic.sagesocial.engine.AbstractService;
import fr.istic.sagesocial.engine.ServiceFactory;
import fr.istic.sagesocial.engine.bean.FriendBean;
import fr.istic.sagesocial.engine.bean.PhotoBean;
import fr.istic.sagesocial.engine.listener.ServiceCallback;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class FindFriendFragment extends Fragment {

    private View mainView;
    private Context context;
    private List<FriendBean> listMyFriends;// for search friend
    private EditText nameSearchEditText;

    private int currentServiceNumber;

    private FindFriendAdapter adapter;

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container,
	    final Bundle savedInstanceState) {
	this.mainView = inflater.inflate(R.layout.profile_find_friend_fragment,
	        container, false);
	this.context = this.getActivity().getApplicationContext();
	this.listMyFriends = new ArrayList<FriendBean>();
	this.nameSearchEditText = (EditText) this.mainView
	        .findViewById(R.id.find_friend_edit_text);

	final GridView friendsFinded = (GridView) this.mainView
	        .findViewById(R.id.list_friends_grid_view);

	this.adapter = new FindFriendAdapter(this.context, this.listMyFriends);
	friendsFinded.setAdapter(this.adapter);

	final Button searchButton = (Button) this.mainView
	        .findViewById(R.id.new_friend_research_button);
	searchButton.setOnClickListener(new OnClickListener() {

	    @Override
	    public void onClick(final View v) {
		// recovery edit text
		final String searchName = FindFriendFragment.this.nameSearchEditText
		        .getText().toString().replaceAll("\\s", "");
		if (!searchName.contentEquals("")) {
		    FindFriendFragment.this
			    .searchFriend(FindFriendFragment.this.nameSearchEditText
			            .getText().toString());
		}
	    }
	});

	return this.mainView;
    }

    /**
     * This method is used for refresh the friend gridView when the user click
     * to search.
     */
    public void refreshSearchLisFriend() {
	// recovery TextView and GridView
	final GridView friendsFinded = (GridView) this.mainView
	        .findViewById(R.id.list_friends_grid_view);
	final TextView noFriendsFinded = (TextView) this.mainView
	        .findViewById(R.id.no_friends_text_view);

	if (this.listMyFriends.isEmpty()) {
	    friendsFinded.setVisibility(View.INVISIBLE);
	    noFriendsFinded.setVisibility(View.VISIBLE);
	}
	else {
	    // recovery friend grid view
	    friendsFinded.setVisibility(View.VISIBLE);
	    noFriendsFinded.setVisibility(View.INVISIBLE);
	    this.adapter.notifyDataSetChanged();
	}
    }

    public void update(final PhotoBean photoBean) {
	this.adapter.update(photoBean);
    }

    /**
     * This method is used for load in gridView the list of friend which
     * correspond to the nameFriend
     * 
     * @param nameFriend
     */
    private void searchFriend(final String nameFriend) {

	final InputMethodManager imm = (InputMethodManager) this.getActivity()
	        .getSystemService(Context.INPUT_METHOD_SERVICE);
	imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);

	// clear the older search friends list
	this.listMyFriends.clear();

	// recovery profile in param
	final FriendBean profileFriendBean = ProfileActivity.profileFriendBean;

	final Collection<AbstractService> services = ServiceFactory
	        .getRegisteredServices(this.getActivity());

	this.currentServiceNumber = 0;
	for (final AbstractService currentService : services) {
	    // concat list friend

	    ProfileActivity.numberCurrentTask++;
	    currentService.getFriends(profileFriendBean.getId(), 0,
		    AbstractService.MAX_FRIENDS, nameFriend, new ServiceCallback() {

		        @Override
		        public void onUpdateData(final Object data) {
			    FindFriendFragment.this.getActivity().runOnUiThread(
			            new Runnable() {

			                @Override
			                public void run() {
				            final List<FriendBean> listFriendFounded = (List<FriendBean>) data;
				            Log.i("Friends founded",
				                    listFriendFounded.size() + "");
				            FindFriendFragment.this.listMyFriends
				                    .addAll(listFriendFounded);

				            FindFriendFragment.this.currentServiceNumber++;
				            ProfileActivity.numberCurrentTask--;
				            ((ProfileActivity) FindFriendFragment.this
				                    .getActivity())
				                    .checkTasksEnd(FindFriendFragment.this
				                            .getActivity());

				            if (FindFriendFragment.this.currentServiceNumber == services
				                    .size()) {
				                FindFriendFragment.this
				                        .refreshSearchLisFriend();
				            }
			                }

			            });
		        }
		    });
	}
    }

}
