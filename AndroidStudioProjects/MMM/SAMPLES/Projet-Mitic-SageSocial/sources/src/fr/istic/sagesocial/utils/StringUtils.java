package fr.istic.sagesocial.utils;


public class StringUtils {
    public static String join(final String[] strings, final String separator) {
	final StringBuffer sb = new StringBuffer();
	for (int i = 0; i < strings.length; i++) {
	    if (i != 0) {
		sb.append(separator);
	    }
	    sb.append(strings[i]);
	}
	return sb.toString();
    }
}
