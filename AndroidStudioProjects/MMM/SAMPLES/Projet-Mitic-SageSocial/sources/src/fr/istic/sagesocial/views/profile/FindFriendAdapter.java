package fr.istic.sagesocial.views.profile;


import java.util.HashMap;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import fr.istic.sagesocial.R;
import fr.istic.sagesocial.engine.bean.FriendBean;
import fr.istic.sagesocial.engine.bean.PhotoBean;
import fr.istic.sagesocial.engine.listener.OnClickStartActivity;

public class FindFriendAdapter extends BaseAdapter {

    private final Context context;
    private final List<FriendBean> listFriends;
    private final LayoutInflater layoutInflater;

    private final HashMap<PhotoBean, ImageView> imageToUpdateFriends;

    public FindFriendAdapter(final Context context, final List<FriendBean> listFriends) {
	this.context = context;
	this.listFriends = listFriends;
	this.layoutInflater = LayoutInflater.from(context);

	this.imageToUpdateFriends = new HashMap<PhotoBean, ImageView>();
    }

    @Override
    public int getCount() {
	return this.listFriends.size();
    }

    @Override
    public Object getItem(final int position) {
	return position;
    }

    @Override
    public long getItemId(final int position) {
	return position;
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {
	// set up the view
	Log.v("FindFriendAdapter", "position : " + position);
	Log.v("FindFriendAdapter", "ajout de : "
	        + this.listFriends.get(position).getName());
	convertView = this.layoutInflater.inflate(R.layout.profile_find_friend_item,
	        parent, false);
	convertView.setTag(this.listFriends.get(position));

	final ImageView imageView = (ImageView) convertView
	        .findViewById(R.id.find_friend_image_view);
	// create the image
	final PhotoBean photo = this.listFriends.get(position).getPhoto();
	final String imagePath = photo.getLocalRessource();
	if (imagePath == null) {
	    this.imageToUpdateFriends.put(photo, imageView);
	}
	else {
	    imageView.setImageURI(Uri.parse(imagePath));
	}

	// create the text
	final TextView textView = (TextView) convertView
	        .findViewById(R.id.find_friend_text_view);
	textView.append(this.listFriends.get(position).getName());

	// add the action listener
	final OnClickStartActivity onclickFriend = new OnClickStartActivity(this.context,
	        ProfileActivity.class);
	onclickFriend.addParamParcelable("profileFriendBean",
	        this.listFriends.get(position));
	onclickFriend.addFlag(Intent.FLAG_ACTIVITY_NEW_TASK);
	convertView.setOnClickListener(onclickFriend);

	return convertView;
    }

    public void update(final PhotoBean photoBean) {
	if (this.imageToUpdateFriends.get(photoBean) != null) {
	    this.imageToUpdateFriends.get(photoBean).setImageURI(
		    Uri.parse(photoBean.getLocalRessource()));
	    this.imageToUpdateFriends.remove(photoBean);
	}
    }

}
