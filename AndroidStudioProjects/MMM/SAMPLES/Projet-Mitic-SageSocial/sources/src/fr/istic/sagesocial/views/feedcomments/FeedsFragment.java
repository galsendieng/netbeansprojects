package fr.istic.sagesocial.views.feedcomments;


import java.util.Collection;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import fr.istic.sagesocial.R;
import fr.istic.sagesocial.engine.bean.FeedBean;

public class FeedsFragment extends FeedCommentFragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
	    Bundle savedInstanceState) {
	configureFragment();
	View view = super.onCreateView(inflater, container, savedInstanceState);
	return view;
    }

    @Override
    public void setSelectFeed(FeedBean feed) {

    }

    @Override
    public void fill(Context context, Collection<FeedBean> list) {
	FeedCommentAdapter adapter = new FeedCommentAdapter(context, mItemLayout, list);
	mListView.setAdapter(adapter);
	mParentActivity.refreshSelectedFeed();
    }

    @Override
    protected void configureFragment() {
	mFragmentLayout = R.layout.comments_feeds_fragment;
	mItemLayout = R.layout.comments_feed_item;
	mViewId = R.id.comments_feeds_view;
    }

}
