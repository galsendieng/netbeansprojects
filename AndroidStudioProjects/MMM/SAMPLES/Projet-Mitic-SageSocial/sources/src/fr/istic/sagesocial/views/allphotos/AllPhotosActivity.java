package fr.istic.sagesocial.views.allphotos;


import android.content.Context;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Gallery;
import android.widget.ImageView;
import android.widget.TextView;
import fr.istic.sagesocial.R;
import fr.istic.sagesocial.engine.PhotoManager;
import fr.istic.sagesocial.engine.bean.PhotoBean;
import fr.istic.sagesocial.engine.listener.PhotoManagerListener;
import fr.istic.sagesocial.views.MenuManagerActivity;

import java.util.ArrayList;
import java.util.HashMap;

public class AllPhotosActivity extends MenuManagerActivity {

    private Gallery gallery;
    private ImageView photo_viewer_imageView;
    private TextView number_photo_textView;
    private TextView album_name_textView;
    // Data
    private ArrayList<PhotoBean> listPhoto;
    private Drawable noImage;
    private int positionCursor;
    private PictureAdapter adapter;
    private String albumName;
    private static Context mContext;
    private HashMap<PhotoBean, ImageView> imageToUpdate;
    private PhotoManagerListener photoManager;

    @Override
    public void onCreate(final Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);

	this.setContentView(R.layout.allphotos);
	AllPhotosActivity.mContext = getBaseContext();
	imageToUpdate = new HashMap<PhotoBean, ImageView>();

	// Parameters from AlbumFragment
	final Bundle b = getIntent().getExtras();
	listPhoto = new ArrayList<PhotoBean>();
	final int size = b.getInt("sizeList");
	PhotoBean photoCurrent;
	for (int i = 0; i < size; i++) {
	    photoCurrent = b.getParcelable("PhotoBean" + i);

	    listPhoto.add(photoCurrent);
	}
	adapter = new PictureAdapter(this, listPhoto);
	albumName = b.getString("albumName");
	positionCursor = b.getInt("positionCursor");

	// Image used when no image was found
	noImage = getResources().getDrawable(R.drawable.no_photo);

	// Components
	photo_viewer_imageView = (ImageView) findViewById(R.id.imageview);
	number_photo_textView = (TextView) findViewById(R.id.photo_number_textview);
	album_name_textView = (TextView) findViewById(R.id.album_name_textview);

	// Initialize the first image
	album_name_textView.setText(albumName);
	if (listPhoto.size() <= 0) {
	    photo_viewer_imageView.setImageDrawable(noImage);
	}
	else {
	    if (positionCursor < listPhoto.size()) {
		setImage(photo_viewer_imageView, listPhoto.get(positionCursor));
		number_photo_textView
		        .setText(positionCursor + 1 + "/" + listPhoto.size());
	    }
	    else {
		Log.e("Gallery", "Wrong cursor position");
	    }
	}

	// Component Gallery
	gallery = (Gallery) findViewById(R.id.gallery);

	// Parameters

	gallery.setAdapter(adapter);

	PhotoManager.getInstance(AllPhotosActivity.mContext).addListener(
	        new PhotoManagerListener() {
		    @Override
		    public void dataChanged(final PhotoBean photoBean) {
		        Log.d("Picture Adapter", "all data changed");
		        AllPhotosActivity.this.update(photoBean);
		        AllPhotosActivity.this.updateGallery(photoBean);

		    }
	        });
	// Listener for component gallery
	gallery.setOnItemClickListener(new OnItemClickListener() {
	    @Override
	    public void onItemClick(final AdapterView<?> parent, final View v,
		    final int position, final long id) {
		AllPhotosActivity.this.setImage(photo_viewer_imageView,
		        listPhoto.get(position));
		number_photo_textView.setText(position + 1 + "/" + listPhoto.size());
	    }
	});
    }

    /**
     * SetImages
     * 
     * @param ImageView
     *            view
     * @param PhotoBean
     *            pb
     */
    public void setImage(final ImageView imageView, final PhotoBean photo) {

	final String path = photo.getLocalRessource();
	// Set image to the imageView component
	if (path == null) {
	    imageToUpdate.put(photo, imageView);
	}
	else {
	    imageView.setImageURI(Uri.parse(path));
	}
    }

    public void update(final PhotoBean photoBean) {

	final ImageView imageView = imageToUpdate.get(photoBean);

	if (imageView != null) {
	    final String imagePath = photoBean.getLocalRessource();
	    imageView.setImageURI(Uri.parse(imagePath));
	    imageToUpdate.remove(photoBean);
	}
	if (imageToUpdate.size() == 0) {

	}
    }

    public void updateGallery(final PhotoBean photoBean) {
	if (adapter != null) {
	    adapter.update(photoBean);
	}

    }

    @Override
    protected void onDestroy() {
	PhotoManager.getInstance(this).destroy();
	super.onDestroy();
    }

    @Override
    protected void onPause() {
	PhotoManager.getInstance(this).destroy();
	super.onPause();
    }
}
