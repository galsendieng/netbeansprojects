package fr.istic.sagesocial.engine.bean;


import fr.istic.sagesocial.engine.AbstractService;

import java.util.Date;

public class FeedBean {
    public enum MIME {
	LINK, VIDEO, PHOTO
    }

    private final String id;
    private String text;
    private Date createdAt;
    private FriendBean author;
    private String dataURI;
    private MIME mime;
    private final AbstractService service;
    private String description;

    public FeedBean(final String id, final AbstractService service) {
	this.id = id;
	this.service = service;
	this.description = "";
    }

    public FeedBean(final String id, final String text, final Date createdAt,
	    final FriendBean author, final AbstractService service) {
	this.id = id;
	this.text = text;
	this.createdAt = createdAt;
	this.author = author;
	this.service = service;
	this.description = "";
    }

    public FriendBean getAuthor() {
	return this.author;
    }

    public Date getCreatedAt() {
	return this.createdAt;
    }

    public String getDataURI() {
	return this.dataURI;
    }

    public String getDescription() {
	return this.description;
    }

    public String getId() {
	return this.id;
    }

    public MIME getMime() {
	return this.mime;
    }

    public AbstractService getService() {
	return this.service;
    }

    public String getText() {
	return this.text;
    }

    public void setAuthor(final FriendBean author) {
	this.author = author;
    }

    public void setCreatedAt(final Date date) {
	this.createdAt = date;
    }

    public void setDataURI(final String dataURI) {
	this.dataURI = dataURI;
    }

    public void setDescription(final String description) {
	this.description = description;
    }

    public void setMime(final MIME mime) {
	this.mime = mime;
    }

    public void setText(final String text) {
	this.text = text;
    }
}
