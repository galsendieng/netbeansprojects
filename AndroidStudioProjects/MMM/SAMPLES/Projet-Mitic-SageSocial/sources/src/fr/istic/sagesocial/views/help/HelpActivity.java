package fr.istic.sagesocial.views.help;


import android.app.Activity;
import android.os.Bundle;
import android.view.MotionEvent;
import fr.istic.sagesocial.R;

public class HelpActivity extends Activity {
    @Override
    public boolean onTouchEvent(final MotionEvent event) {
	HelpActivity.this.finish();
	return super.onTouchEvent(event);
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);

	final Bundle view = this.getIntent().getExtras();
	if (view != null) {

	    if (view.getString("helpView").equalsIgnoreCase("profile")) {
		this.setContentView(R.layout.help_profile);
	    }
	    else if (view.getString("helpView").equalsIgnoreCase("friends")) {
		this.setContentView(R.layout.help_friends);
	    }
	    else if (view.getString("helpView").equalsIgnoreCase("accounts")) {
		this.setContentView(R.layout.help_accounts);
	    }
	}

    }

    @Override
    protected void onPause() {
	super.onPause();
	this.finish();
    }
}
