package fr.istic.sagesocial.views.profile;


import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import fr.istic.sagesocial.R;
import fr.istic.sagesocial.engine.AbstractService;
import fr.istic.sagesocial.engine.ServiceFactory;
import fr.istic.sagesocial.engine.bean.FeedBean;
import fr.istic.sagesocial.engine.bean.PhotoBean;
import fr.istic.sagesocial.engine.listener.ServiceCallback;

public class FeedsFragment extends Fragment {

    private Context context;
    private List<FeedBean> listMyFeeds;
    private View mainView;

    private FeedAdapter adapter;
    private int currentServiceNumber;

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container,
	    final Bundle savedInstanceState) {

	this.context = this.getActivity().getApplicationContext();
	this.mainView = inflater.inflate(R.layout.profile_feeds_fragment, container,
	        false);

	final ListView listFeedView = (ListView) this.mainView
	        .findViewById(R.id.my_feed_list_view);

	// user list feeds
	this.listMyFeeds = new ArrayList<FeedBean>();

	adapter = new FeedAdapter(this.context, this.listMyFeeds,
	        ((ProfileActivity) this.getActivity()).profileFriendBean);
	listFeedView.setAdapter(adapter);

	return this.mainView;
    }

    public void update(final PhotoBean photoBean) {
	adapter.update(photoBean);
    }

    public void loadFeedsList() {
	final Collection<AbstractService> services = ServiceFactory
	        .getRegisteredServices(this.getActivity());

	this.currentServiceNumber = 0;
	for (final AbstractService currentService : services) {
	    final String idUser = ((ProfileActivity) getActivity()).profileFriendBean
		    .getId();

	    ProfileActivity.numberCurrentTask++;
	    currentService.getMessages(idUser, 20, new ServiceCallback() {

		@Override
		public void onUpdateData(final Object data) {
		    FeedsFragment.this.getActivity().runOnUiThread(new Runnable() {

			@Override
			public void run() {
			    final List<FeedBean> listFeeds = (List<FeedBean>) data;

			    if (currentServiceNumber == 0) {
				listMyFeeds.clear();
			    }

			    FeedsFragment.this.listMyFeeds.addAll(listFeeds);
			    FeedsFragment.this.refreshLisFeeds();
			    FeedsFragment.this.currentServiceNumber++;

			    ProfileActivity.numberCurrentTask--;
			    ((ProfileActivity) getActivity())
				    .checkTasksEnd(getActivity());

			    if (FeedsFragment.this.currentServiceNumber == services
				    .size()) {
				// FeedsFragment.this.refreshLisFeeds();
			    }
			}

		    });
		}

	    });
	}
    }

    public void refreshLisFeeds() {
	adapter.notifyDataSetChanged();
    }

}
