package fr.istic.sagesocial.engine.listener;


import android.app.Activity;

public interface ServiceCallback {

    /**
     * This method is called when the service method to retrieve information.
     * This information is contained in the data object. Warning: if the
     * callback is in an Activity, use runOnUiThread (...) {@link http
     * ://developer
     * .android.com/reference/android/app/Activity.html#runOnUiThread
     * (java.lang.Runnable)} and put your code in.
     * 
     * @param data
     * @see Activity
     */
    public void onUpdateData(Object data);
}
