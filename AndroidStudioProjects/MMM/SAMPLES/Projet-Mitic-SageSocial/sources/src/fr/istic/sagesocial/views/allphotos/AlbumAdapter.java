package fr.istic.sagesocial.views.allphotos;


import android.content.Context;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import fr.istic.sagesocial.R;
import fr.istic.sagesocial.engine.bean.AlbumBean;
import fr.istic.sagesocial.engine.bean.PhotoBean;

import java.util.ArrayList;
import java.util.HashMap;

public class AlbumAdapter extends BaseAdapter {

    private final ArrayList<AlbumBean> albumList;
    LayoutInflater layoutInflater;
    final String imagePath;

    private final HashMap<PhotoBean, ImageView> imageToUpdate;

    public AlbumAdapter(final Context c, final ArrayList<AlbumBean> albumList) {
	layoutInflater = LayoutInflater.from(c);
	this.albumList = albumList;
	imageToUpdate = new HashMap<PhotoBean, ImageView>();
	imagePath = "";
    }

    @Override
    public int getCount() {
	return albumList.size();
    }

    @Override
    public Object getItem(final int position) {
	return albumList.get(position);
    }

    @Override
    public long getItemId(final int position) {
	return position;
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {

	convertView = layoutInflater.inflate(R.layout.albums_item, parent, false);

	final PhotoBean photo = albumList.get(position).getCoverPicture();
	final String imagePath = photo.getLocalRessource();
	final ImageView imageView = (ImageView) convertView
	        .findViewById(R.id.adapter_album_picture);

	// the picture is null, it's because the file is not yet downloaded
	// by the PhotoManager. We add it to an array to add the bitmap
	// later.
	if (imagePath == null) {
	    imageToUpdate.put(photo, imageView);
	}
	else {
	    imageView.setImageURI(Uri.parse(imagePath));
	}

	convertView.setLayoutParams(new GridView.LayoutParams(150, 150));

	return convertView;
    }

    public void update(final PhotoBean photoBean) {

	final ImageView imageView = imageToUpdate.get(photoBean);

	if (imageView != null) {
	    final String imagePath = photoBean.getLocalRessource();
	    Log.d("AlbumAdapter update", imagePath);
	    imageView.setImageURI(Uri.parse(imagePath));
	    imageToUpdate.remove(photoBean);
	}
    }
}
