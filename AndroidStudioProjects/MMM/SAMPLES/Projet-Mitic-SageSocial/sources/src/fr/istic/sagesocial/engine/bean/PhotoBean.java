package fr.istic.sagesocial.engine.bean;


import android.os.Parcel;
import android.os.Parcelable;

public class PhotoBean implements Parcelable {

    public String url;
    public String localRessource;

    public static final Parcelable.Creator<PhotoBean> CREATOR = new Parcelable.Creator<PhotoBean>() {
	@Override
	public PhotoBean createFromParcel(final Parcel source) {
	    return new PhotoBean(source);
	}

	@Override
	public PhotoBean[] newArray(final int size) {
	    return new PhotoBean[size];
	}
    };

    public PhotoBean() {
    }

    public PhotoBean(final Parcel source) {
	this.url = source.readString();
	this.setLocalRessource(source.readString());
    }

    public PhotoBean(final String url) {
	this.url = this.sanitizeFacebookUrl(url);
    }

    public PhotoBean(final String url, final String localRessource) {
	this(url);
	this.localRessource = localRessource;
    }

    @Override
    public int describeContents() {
	return 2;
    }

    /**
     * Get the local resource of the file (ie:
     * /mnt/sdcard/SageSocial/cache/foo.jpg)
     * 
     * @return
     */
    public String getLocalRessource() {
	return this.localRessource;
    }

    /**
     * Get the URL of the file. It correspond to the online address of the file.
     * 
     * @return
     */
    public String getUrl() {
	return this.url;
    }

    /**
     * Set the local resource of the file (ie:
     * /mnt/sdcard/SageSocial/cache/foo.jpg)
     * 
     * @param localRessource
     */
    public void setLocalRessource(final String localRessource) {
	this.localRessource = localRessource;
    }

    public void setUrl(final String url) {
	this.url = url;
    }

    @Override
    public void writeToParcel(final Parcel dest, final int flags) {
	dest.writeString(this.url);
	dest.writeString(this.localRessource);
    }

    /**
     * Try to hack the image url to fore downloading a larger image.
     * 
     * @param url
     * @return
     */
    private String sanitizeFacebookUrl(final String url) {
	return url.replace("_q", "_n");
    }
}
