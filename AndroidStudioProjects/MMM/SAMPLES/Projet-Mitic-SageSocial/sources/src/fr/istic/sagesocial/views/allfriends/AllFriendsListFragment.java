package fr.istic.sagesocial.views.allfriends;


import android.app.Fragment;
import android.app.FragmentManager;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import fr.istic.sagesocial.R;

public class AllFriendsListFragment extends Fragment {

    private ListView mListView;
    private AllFriendsGridFragment mGridFragment;

    public void init(final String[] groups) {
	final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this.getActivity(),
	        android.R.layout.simple_list_item_1, groups);
	this.mListView.setAdapter(adapter);
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container,
	    final Bundle savedInstanceState) {

	final View view = inflater.inflate(R.layout.allfriends_list_fragment, container,
	        false);

	// create the listview
	this.mListView = (ListView) view.findViewById(R.id.allfriends_listview);

	// Improves scrolling performance
	this.mListView.setCacheColorHint(Color.TRANSPARENT);

	this.mListView.setOnItemClickListener(new OnItemClickListener() {
	    @Override
	    public void onItemClick(final AdapterView<?> parent, final View view,
		    final int position, final long id) {

		// When clicked, show a toast with the TextView text
		Toast.makeText(
		        AllFriendsListFragment.this.getActivity(),
		        AllFriendsListFragment.this.getString(R.string.show_group_friend)
		                .replace("%s", ((TextView) view).getText()),
		        Toast.LENGTH_SHORT).show();

		// retrieve the grid
		final FragmentManager fm = AllFriendsListFragment.this
		        .getFragmentManager();
		AllFriendsListFragment.this.mGridFragment = (AllFriendsGridFragment) fm
		        .findFragmentById(R.id.allfriends_grid_fragment);
		final String selectedGroup = ((TextView) view).getText().toString();
		AllFriendsListFragment.this.mGridFragment.fill(selectedGroup);
	    }
	});

	return view;
    }
}
