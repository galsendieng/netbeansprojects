package fr.istic.sagesocial.engine.listener;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;
import android.view.View;
import android.view.View.OnClickListener;

public class OnClickStartActivity implements OnClickListener {
    private final Context mActivity;
    private final Class<?> mClass;
    private final Intent mIntent;
    private boolean mForceClose = false;

    public OnClickStartActivity(final Context activity, final Class<?> myClass) {
	this.mActivity = activity;
	this.mClass = myClass;
	this.mIntent = new Intent(this.mActivity, this.mClass);
    }

    public OnClickStartActivity(final Context activity, final Class<?> myClass,
	    final boolean forceClose) {
	this(activity, myClass);
	this.mForceClose = forceClose;
    }

    public void addFlag(final int flags) {
	this.mIntent.addFlags(flags);
    }

    public void addParam(final String key, final String content) {
	this.mIntent.putExtra(key, content);
    }

    public void addParamParcelable(final String key, final Parcelable contentParcelable) {
	this.mIntent.putExtra(key, contentParcelable);
    }

    @Override
    public void onClick(final View arg0) {

	OnClickStartActivity.this.mActivity
	        .startActivity(OnClickStartActivity.this.mIntent);

	if (this.mForceClose) {
	    ((Activity) this.mActivity).finish();
	}
    }
}
