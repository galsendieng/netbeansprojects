package fr.istic.sagesocial.views.feedcomments;


import java.util.Collection;

import android.app.FragmentManager;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import fr.istic.sagesocial.R;
import fr.istic.sagesocial.engine.bean.FeedBean;

public class CommentsFragment extends FeedCommentFragment {

    private FeedSelectedFragment feedSelectedFragment;
    private CommentNewFragment commentNewFragment;

    private int mFeedSelectedId;
    private int mCommentNewId;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
	    Bundle savedInstanceState) {
	configureFragment();
	View view = super.onCreateView(inflater, container, savedInstanceState);

	final FragmentManager fm = this.getFragmentManager();
	feedSelectedFragment = (FeedSelectedFragment) fm
	        .findFragmentById(mFeedSelectedId);
	commentNewFragment = (CommentNewFragment) fm.findFragmentById(mCommentNewId);

	return view;
    }

    @Override
    public void onStart() {
	super.onStart();
	CommentsActivity activity = (CommentsActivity) getActivity();
	commentNewFragment.configureButton(activity.getServices());
    }

    @Override
    public void setSelectFeed(FeedBean feed) {
	feedSelectedFragment.fillSelectedFeed(feed);
    }

    @Override
    public void fill(Context context, Collection<FeedBean> list) {
	FeedCommentAdapter adapter = new FeedCommentAdapter(context, mItemLayout, list);
	mListView.setAdapter(adapter);
    }

    @Override
    protected void configureFragment() {
	mFragmentLayout = R.layout.comments_comments_fragment;
	mItemLayout = R.layout.comments_comment_item;
	mViewId = R.id.comments_comments_view;
	mFeedSelectedId = R.id.comments_feed_selected_fragment;
	mCommentNewId = R.id.comments_comment_new_fragment;
    }

}
