package mysql.test;

import java.io.InputStream;
import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class SageSocialActivity extends Activity{
	
	private static final String urlPage = "connection.php";
	private EditText mail;
	private EditText password;
	private TextView error;
	private OnClickListener listener_create_account_btn = new OnClickListener(){

		@Override
		public void onClick(View arg0) {
			Intent i = new Intent(SageSocialActivity.this, CreateAccount.class);
			startActivity(i);		
		}
	};
	private OnClickListener listener_login_btn = new OnClickListener(){
		
		@Override
		public void onClick(View v) {
			
			String entered_mail = mail.getText().toString().trim();
			String entered_password = password.getText().toString().trim();
			
			if((entered_password.compareTo("") != 0) &&(entered_mail.compareTo("") != 0)){			
				if(getSession(mail.getText().toString(), Tools.sha1(password.getText().toString()))){
					Intent i = new Intent(SageSocialActivity.this, HomeActivity.class);
					startActivity(i);
				}else{
					error.setText("Authentication failed");
					error.setVisibility(View.VISIBLE);
				}
			}else{
				error.setText("The fields were not entered correctly");
				error.setVisibility(View.VISIBLE);
			}
		}
		
	};	

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		
		mail = (EditText)this.findViewById(R.id.mail_input);
		password = (EditText)this.findViewById(R.id.password_input);
		error = (TextView)this.findViewById(R.id.error_text);
		error.setVisibility(View.INVISIBLE);
		
		Button create_account = (Button)this.findViewById(R.id.createAccount_btn);
		create_account.setOnClickListener(listener_create_account_btn);
		
		Button login = (Button)this.findViewById(R.id.login_btn);
		login.setOnClickListener(listener_login_btn);
	}

	private boolean getSession(String mail, String password){
		
		InputStream is = null;
		String result = "";		
		ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("mail",mail));
		nameValuePairs.add(new BasicNameValuePair("password",password));
		
		//HTTP Request
		is = Tools.httpRequest(nameValuePairs, urlPage);
		
		//Request conversion into string
		result = Tools.conversionToString(is);
		
		//Get JSON data
		try{
			
			JSONObject json_data = new JSONObject(result);
			//If session find in database
			if(json_data.getString("connection").compareTo("1")==0){
				return true;
			}
			
		}catch(JSONException e){
			Log.e("log_tag", "Error parsing data " + e.toString());
		}
		return false;
	}
	
}