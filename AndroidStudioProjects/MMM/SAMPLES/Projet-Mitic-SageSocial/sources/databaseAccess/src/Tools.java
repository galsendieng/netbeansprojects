package mysql.test;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;

import android.util.Log;

public class Tools {
	
	public static final String urlServer = "http://cheghamwassim.com/apps/android/sagesocial/";
	
	//Encoding password with sha1
	public static String sha1(String s){
		MessageDigest digest;
		try {
			digest = MessageDigest.getInstance("SHA-1");
			byte[] input = digest.digest(s.getBytes());
			StringBuilder sb = new StringBuilder(); 
			int v;
			for (int i = 0; i < input.length; i++) {
				v = input[i] & 0xFF; 
				if(v < 16) {
					sb.append("0");
				}
				sb.append(Integer.toString(v, 16)); 
			}  
			return sb.toString();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	//Converting InputStream into String
	public static String conversionToString(InputStream is){
		String result = "";
		try{
			BufferedReader reader = new BufferedReader(new InputStreamReader(is,"UTF-8"),8);
			StringBuilder sb = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
			is.close();
			result=sb.toString();
		}catch(Exception e){
			Log.e("log_tag", "Error converting result " + e.toString());
		}
		return result;
	}
	
	//HTTP Request
	public static InputStream httpRequest(ArrayList<NameValuePair> nameValuePairs, String urlPage){
		InputStream is = null;
		try{
			HttpClient httpclient = new DefaultHttpClient();
			HttpPost httppost = new HttpPost(Tools.urlServer+urlPage);
			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			HttpResponse response = httpclient.execute(httppost);
			HttpEntity entity = response.getEntity();
			is = entity.getContent();
		}catch(Exception e){
			Log.e("log_tag", "Error in http connection " + e.toString());
		}
		return is;
	}
	
}
