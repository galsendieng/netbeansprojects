package mysql.test;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class HomeActivity extends Activity implements OnClickListener{
	
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.home);

		Button return_btn = (Button)this.findViewById(R.id.disconnect_btn);
		return_btn.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		if(v.getId() == R.id.disconnect_btn){
			Intent i = new Intent(HomeActivity.this, SageSocialActivity.class);
			startActivity(i);
		}
	}
}
