package com.orange.tpfirebase;

/**
 * Created by adama on 08/02/17.
 */

public class Person {
    private String name;
    private String address;
    private String email;


    /***
     * Contructeur Vide
     */
    public Person() {

    }
    //Getters and setters
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}