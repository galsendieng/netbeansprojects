package com.orange.tpfirebase;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import java.util.ArrayList;
import java.util.HashMap;

public class ListActivity extends AppCompatActivity {

    //Liste des elements ajoutés
    private ListView maListViewPerso;
    private SimpleAdapter mListAdapter;
    private ArrayList<HashMap<String, String>> listItem;
    private String namen =  "";
    private String adressen = "";
    private String emailn = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        //Liste des items avec Btn Update

        maListViewPerso = (ListView) findViewById(R.id.listviewperso);
        listItem = new ArrayList<HashMap<String, String>>();
        HashMap<String, String> map;
        mListAdapter = new SimpleAdapter(this.getBaseContext(), listItem, R.layout.item,
                new String[]{"name", "adresse","email"}, new int[]{
                R.id.name, R.id.adresse, R.id.email
        });

    }

}
