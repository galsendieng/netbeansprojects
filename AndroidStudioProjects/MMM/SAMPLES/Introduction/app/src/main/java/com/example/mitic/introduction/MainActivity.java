package com.example.mitic.introduction;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //On doit lancer obligatoirement une activity
        setContentView(R.layout.activity_main);
       // setContentView(R.layout.activity_main);
        // on récupère l'instance du bouton (bouton 1)
        Button b =(Button)findViewById(R.id.button1);
        //on y ajoute un listener sur le bouton
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                Log.i("Click", "Button " + ((Button) arg0).getId() + "pressed");
                Toast.makeText(getApplicationContext(), "Hello Button1!", Toast.LENGTH_SHORT).show();

            }
        });

        /*
        TextView tv = new TextView(this);
        tv.setText("Hello World");
        setContentView(tv);
        */

        /*
        Button b = new Button(this);
        b.setText("Hello World");
        b.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Log.i("Click", "Button "+ ((Button)arg0).getId() + "pressed");
				Toast.makeText(getApplicationContext(), "Hello!", Toast.LENGTH_SHORT).show();
			}
		});
        setContentView(b);
        */
    }
    public void test(View v) {
        Toast.makeText(this, "Hello Button2", Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
