package com.example.mitic.intents;

/**
 * Created by mchristi on 9/18/15.
 */
        import android.net.Uri;
        import android.os.Bundle;
        import android.app.Activity;
        import android.content.Intent;
        import android.view.Menu;
        import android.view.View;
        import android.widget.Button;
        import android.widget.EditText;

public class SecondaryActivity extends Activity {

    String mdata;
    EditText mEditText1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_secondary);

        mdata = getIntent().getExtras().getString("data");

        mEditText1 = (EditText)findViewById(R.id.editText1);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    public boolean ok(View v) {

        Intent intent = new Intent();
        // ajout de données supplémentaires dans l'intent
        // attention au toString() à faire sur l'Editable de l'EditText!
        intent.putExtra("returnKey", "Hello first activity: "+ mdata + mEditText1.getText().toString());
        // envoi du resultat
        setResult(RESULT_OK,intent);
        // arret de l'activity ici
        finish();

        return true;
    }



}
