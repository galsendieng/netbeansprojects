package com.example.mitic.intents;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    protected EditText mEditText1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mEditText1 = (EditText)findViewById(R.id.editText1);
    }

    public boolean openBrowser(View v) {
        Intent i = new Intent("android.intent.action.VIEW", Uri.parse("http://www.google.fr/search?q=" + mEditText1.getText().toString()));
        startActivity(i);
        return true;
    }

    public boolean call(View v) {
        String url = "tel:3334444";
        Intent i = new Intent(Intent.ACTION_DIAL, Uri.parse(url));
        startActivity(i);
        return true;
    }

    public boolean where(View v) {
        String url = "geo:32,25";
        Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        startActivity(i);
        return true;
    }

    public boolean startSecondaryActivity(View v) {
        Intent i = new Intent(getApplicationContext(), SecondaryActivity.class);
        i.putExtra("data", "CALL OK!");
        startActivityForResult(i, 0); // 0 = requestcode de l'activity appelant
        return true;
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent intent){
        super.onActivityResult(requestCode, resultCode, intent);
        Bundle extras = intent.getExtras();
        mEditText1.setText(extras != null ? extras.getString("returnKey") : "nothing returned");
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
