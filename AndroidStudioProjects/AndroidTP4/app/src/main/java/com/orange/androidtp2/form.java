package com.orange.androidtp2;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class form extends Activity {

    String mdata;
    EditText mEditNom;
    EditText mEditPrenom;
    EditText mEditDatenais;
    EditText mEditLieu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form);

        mdata = getIntent().getExtras().getString("data");
        mEditNom = (EditText)findViewById(R.id.nom);
        mEditPrenom = (EditText)findViewById(R.id.prenom);
        mEditDatenais = (EditText)findViewById(R.id.datenais);
        mEditLieu = (EditText)findViewById(R.id.lieu);
    }

    public boolean ok(View v) {
        Intent intent = new Intent();
        intent.putExtra("returnNom", mEditNom.getText().toString());
        intent.putExtra("returnPrenom", mEditPrenom.getText().toString());
        intent.putExtra("returnDaten", mEditDatenais.getText().toString());
        intent.putExtra("returnLieun", mEditLieu.getText().toString());
        setResult(RESULT_OK,intent);
        finish();
        return true;
    }
}
