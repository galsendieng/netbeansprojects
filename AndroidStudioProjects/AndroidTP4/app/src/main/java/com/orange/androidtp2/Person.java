package com.orange.androidtp2;

/**
 * Created by adama on 08/02/17.
 */

public class Person {
    private String name;
   // private String address;
    private String prenom;
    private String datenais;
    private String lieunais;
    /***
     * Contructeur Vide
     */
    public Person() {

    }
    //Getters and setters
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getDatenais() {
        return datenais;
    }

    public void setDatenais(String datenais) {
        this.datenais = datenais;
    }

    public String getLieunais() {
        return lieunais;
    }

    public void setLieunais(String lieunais) {
        this.lieunais = lieunais;
    }
}